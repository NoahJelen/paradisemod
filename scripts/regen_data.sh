#!/bin/bash
if ./gradlew runData; then
    rm -rf src/generated/resources/data/minecraft/advancements
    rm -rf src/generated/resources/data/paradisemod/advancements/recipes
    rm -rf src/generated/resources/.cache
    echo "Paradise Mod data pack regeneration complete!"
fi
