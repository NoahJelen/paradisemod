#!/bin/bash
./gradlew runData
rm -rf src/generated/resources/data/minecraft/advancements
rm -rf src/generated/resources/data/paradisemod/advancements/recipes
rm -rf src/generated/resources/.cache
./gradlew runClient
