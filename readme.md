![For Minecraft 1.20.1](https://img.shields.io/badge/Minecraft-1.20.1-green)

![](logo.png)
![](front.png)

A mod that makes many improvements to Minecraft and adds lots of new content like new trees, wood types, 3 brand new dimensions to explore, and many more features!

[website](https://www.paradisemod.net/)

[Modrinth download](https://modrinth.com/mod/paradise-mod)

[Curse Forge download](https://www.curseforge.com/minecraft/mc-mods/paradise-mod)
