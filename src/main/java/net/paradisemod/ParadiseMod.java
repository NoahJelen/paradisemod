package net.paradisemod;

import java.util.EnumMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLDedicatedServerSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.paradisemod.automation.Automation;
import net.paradisemod.base.Events;
import net.paradisemod.base.PMConfig;
import net.paradisemod.base.PMPaintings;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.DataGen;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredCreativeTab;
import net.paradisemod.bonus.Bonus;
import net.paradisemod.building.Building;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.misc.Misc;
import net.paradisemod.monsters.Monsters;
import net.paradisemod.redstone.Redstone;
import net.paradisemod.world.PMWorld;

@Mod(ParadiseMod.ID)
public class ParadiseMod {
    public static final EnumMap<DyeColor, Item> DYES_BY_COLOR = new EnumMap<>(DyeColor.class);
    public static final String ID = "paradisemod";
    public static final Logger LOG = LogManager.getLogger(ID);

    static {
        DYES_BY_COLOR.put(DyeColor.WHITE, Items.WHITE_DYE);
        DYES_BY_COLOR.put(DyeColor.ORANGE, Items.ORANGE_DYE);
        DYES_BY_COLOR.put(DyeColor.MAGENTA, Items.MAGENTA_DYE);
        DYES_BY_COLOR.put(DyeColor.LIGHT_BLUE, Items.LIGHT_BLUE_DYE);
        DYES_BY_COLOR.put(DyeColor.YELLOW, Items.YELLOW_DYE);
        DYES_BY_COLOR.put(DyeColor.LIME, Items.LIME_DYE);
        DYES_BY_COLOR.put(DyeColor.PINK, Items.PINK_DYE);
        DYES_BY_COLOR.put(DyeColor.GRAY, Items.GRAY_DYE);
        DYES_BY_COLOR.put(DyeColor.LIGHT_GRAY, Items.LIGHT_GRAY_DYE);
        DYES_BY_COLOR.put(DyeColor.CYAN, Items.CYAN_DYE);
        DYES_BY_COLOR.put(DyeColor.PURPLE, Items.PURPLE_DYE);
        DYES_BY_COLOR.put(DyeColor.BLUE, Items.BLUE_DYE);
        DYES_BY_COLOR.put(DyeColor.BROWN, Items.BROWN_DYE);
        DYES_BY_COLOR.put(DyeColor.GREEN, Items.GREEN_DYE);
        DYES_BY_COLOR.put(DyeColor.RED, Items.RED_DYE);
        DYES_BY_COLOR.put(DyeColor.BLACK, Items.BLACK_DYE);
    }

    public ParadiseMod() {
        LOG.info("Setting up Paradise Mod");
        PMConfig.init(ModLoadingContext.get());
        var eventbus = FMLJavaModLoadingContext.get().getModEventBus();

        // creative inventory changes
        // put debug stick and knowledge book in tools tab of creative inventory
        Utils.addToCreativeTab(Items.DEBUG_STICK, CreativeModeTabs.TOOLS_AND_UTILITIES);
        Utils.addToCreativeTab(Items.KNOWLEDGE_BOOK, CreativeModeTabs.TOOLS_AND_UTILITIES);

        // add jigsaw, structure, and command blocks to the redstone tab
        Utils.addToCreativeTab(Blocks.JIGSAW, CreativeModeTabs.REDSTONE_BLOCKS);
        Utils.addToCreativeTab(Blocks.STRUCTURE_BLOCK, CreativeModeTabs.REDSTONE_BLOCKS);
        Utils.addToCreativeTab(Blocks.STRUCTURE_VOID, CreativeModeTabs.REDSTONE_BLOCKS);
        Utils.addToCreativeTab(Blocks.COMMAND_BLOCK, CreativeModeTabs.REDSTONE_BLOCKS);
        Utils.addToCreativeTab(Blocks.CHAIN_COMMAND_BLOCK, CreativeModeTabs.REDSTONE_BLOCKS);
        Utils.addToCreativeTab(Blocks.REPEATING_COMMAND_BLOCK, CreativeModeTabs.REDSTONE_BLOCKS);

        // add barrier to building blocks tab
        Utils.addToCreativeTab(Blocks.BARRIER, CreativeModeTabs.BUILDING_BLOCKS);

        // add command block minecarts to transportation tab
        Utils.addToCreativeTab(Items.COMMAND_BLOCK_MINECART, CreativeModeTabs.REDSTONE_BLOCKS);

        // modules
        PMPaintings.init(eventbus);
        Misc.init(eventbus);
        Bonus.init(eventbus);
        Redstone.init(eventbus);
        PMWorld.init(eventbus);
        Building.init();
        Decoration.init();
        Automation.init();
        Monsters.init();
        RegisteredCreativeTab.initTabs(eventbus);

        // register everything
        PMRegistries.init(eventbus);

        // client setup
        eventbus.addListener(
            (FMLClientSetupEvent event) -> {
                // init the client side of the modules
                Misc.initClient(event);
                Redstone.initClient();
                PMWorld.initClient(event);
                Monsters.initClient();

                // client side event handlers
                MinecraftForge.EVENT_BUS.register(Events.Client.class);
                LOG.info("Minecraft is in client mode");
            }
        );

        eventbus.addListener(Events.Client::colorBlocks);
        eventbus.addListener(Events.Client::colorItems);

        // server setup (currently only says minecraft is in server mode when launched on a server)
        eventbus.addListener((FMLDedicatedServerSetupEvent event) -> LOG.info("Minecraft is in server mode"));
        eventbus.addListener(Events.Common::populateCreativeTabs);

        // common event handlers
        MinecraftForge.EVENT_BUS.register(Events.Common.class);

        // data generation
        eventbus.addListener(DataGen::generateData);

        LOG.info("Setup complete");
    }
}