package net.paradisemod.decoration;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraftforge.client.model.generators.ModelFile;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.bonus.Bonus;
import net.paradisemod.world.CrystalClusters;
import net.paradisemod.world.DeepDarkBlocks;

public class FlowerPots {
    public static final RegisteredBlock POTTED_BLACK_ROSE = regPottedPlant(Decoration.BLACK_ROSE, 0, false);
    public static final RegisteredBlock POTTED_BLUE_ROSE = regPottedPlant(Decoration.BLUE_ROSE, 0, false);
    public static final RegisteredBlock POTTED_BROWN_ROSE = regPottedPlant(Decoration.BROWN_ROSE, 0, false);
    public static final RegisteredBlock POTTED_CYAN_ROSE = regPottedPlant(Decoration.CYAN_ROSE, 0, false);
    public static final RegisteredBlock POTTED_ENDER_ROSE = regPottedPlant(Decoration.ENDER_ROSE, 0, false);
    public static final RegisteredBlock POTTED_GRAY_ROSE = regPottedPlant(Decoration.GRAY_ROSE, 0, false);
    public static final RegisteredBlock POTTED_GREEN_ROSE = regPottedPlant(Decoration.GREEN_ROSE, 0, false);
    public static final RegisteredBlock POTTED_LIGHT_BLUE_ROSE = regPottedPlant(Decoration.LIGHT_BLUE_ROSE, 0, false);
    public static final RegisteredBlock POTTED_LIGHT_GRAY_ROSE = regPottedPlant(Decoration.LIGHT_GRAY_ROSE, 0, false);
    public static final RegisteredBlock POTTED_MAGENTA_ROSE = regPottedPlant(Decoration.MAGENTA_ROSE, 0, false);
    public static final RegisteredBlock POTTED_ORANGE_ROSE = regPottedPlant(Decoration.ORANGE_ROSE, 0, false);
    public static final RegisteredBlock POTTED_LIME_ROSE = regPottedPlant(Decoration.LIME_ROSE, 0, false);
    public static final RegisteredBlock POTTED_PINK_ROSE = regPottedPlant(Decoration.PINK_ROSE, 0, false);
    public static final RegisteredBlock POTTED_PURPLE_ROSE = regPottedPlant(Decoration.PURPLE_ROSE, 0, false);
    public static final RegisteredBlock POTTED_RED_ROSE = regPottedPlant(Decoration.RED_ROSE, 0, false);
    public static final RegisteredBlock POTTED_WHITE_ROSE = regPottedPlant(Decoration.WHITE_ROSE, 0, false);
    public static final RegisteredBlock POTTED_YELLOW_ROSE = regPottedPlant(Decoration.YELLOW_ROSE, 0, false);
    public static final RegisteredBlock POTTED_GLOWSHROOM = regPottedPlant(DeepDarkBlocks.GLOWSHROOM, 7, false);
    public static final RegisteredBlock POTTED_BLUE_AUTUMN_SAPLING = regPottedPlant(Decoration.BLUE_AUTUMN_SAPLING, 0, false);
    public static final RegisteredBlock POTTED_ORANGE_AUTUMN_SAPLING = regPottedPlant(Decoration.ORANGE_AUTUMN_SAPLING, 0, false);
    public static final RegisteredBlock POTTED_RED_AUTUMN_SAPLING = regPottedPlant(Decoration.RED_AUTUMN_SAPLING, 0, false);
    public static final RegisteredBlock POTTED_YELLOW_AUTUMN_SAPLING = regPottedPlant(Decoration.YELLOW_AUTUMN_SAPLING, 0, false);
    public static final RegisteredBlock POTTED_CHRISTMAS_TREE_SAPLING = regPottedPlant(Bonus.CHRISTMAS_TREE_SAPLING, 15, false);
    public static final RegisteredBlock POTTED_PALO_VERDE_SAPLING = regPottedPlant(Decoration.PALO_VERDE_SAPLING, 0, false);
    public static final RegisteredBlock POTTED_MESQUITE_SAPLING = regPottedPlant(Decoration.MESQUITE_SAPLING, 0, false);
    public static final RegisteredBlock POTTED_BLACKENED_OAK_SAPLING = regPottedPlant(DeepDarkBlocks.BLACKENED_OAK_SAPLING, 0, false);
    public static final RegisteredBlock POTTED_BLACKENED_SPRUCE_SAPLING = regPottedPlant(DeepDarkBlocks.BLACKENED_SPRUCE_SAPLING, 0, false);
    public static final RegisteredBlock POTTED_GLOWING_OAK_SAPLING = regPottedPlant(DeepDarkBlocks.GLOWING_OAK_SAPLING, 7, false);
    public static final RegisteredBlock POTTED_EMERALD_CRYSTAL_CLUSTER = regPottedPlant(CrystalClusters.EMERALD_CRYSTAL_CLUSTER, 10, false);
    public static final RegisteredBlock POTTED_DIAMOND_CRYSTAL_CLUSTER = regPottedPlant(CrystalClusters.DIAMOND_CRYSTAL_CLUSTER, 10, false);
    public static final RegisteredBlock POTTED_HONEY_CRYSTAL_CLUSTER = regPottedPlant(CrystalClusters.HONEY_CRYSTAL_CLUSTER, 10, false);
    public static final RegisteredBlock POTTED_LAPIS_CRYSTAL_CLUSTER = regPottedPlant(CrystalClusters.LAPIS_CRYSTAL_CLUSTER, 10, false);
    public static final RegisteredBlock POTTED_QUARTZ_CRYSTAL_CLUSTER = regPottedPlant(CrystalClusters.QUARTZ_CRYSTAL_CLUSTER, 10, false);
    public static final RegisteredBlock POTTED_REDSTONE_CRYSTAL_CLUSTER = regPottedPlant(CrystalClusters.REDSTONE_CRYSTAL_CLUSTER, 10, false);
    public static final RegisteredBlock POTTED_RUBY_CRYSTAL_CLUSTER = regPottedPlant(CrystalClusters.RUBY_CRYSTAL_CLUSTER, 10, false);
    public static final RegisteredBlock POTTED_SALT_CRYSTAL_CLUSTER = regPottedPlant(CrystalClusters.SALT_CRYSTAL_CLUSTER, 10, false);
    public static final RegisteredBlock POTTED_ENDER_PEARL_CLUSTER = regPottedPlant(CrystalClusters.ENDER_PEARL_CLUSTER, 10, false);
    public static final RegisteredBlock POTTED_SOUL_PUMPKIN = regPottedPlant(Decoration.SOUL_PUMPKIN, 0, true);
    public static final RegisteredBlock POTTED_GLOWNG_CACTUS = regPottedPlant(DeepDarkBlocks.GLOWING_CACTUS, 7, true);

    public static void init() { }

    private static RegisteredBlock regPottedPlant(RegisteredBlock plant, int light, boolean hasCustomModel) {
        var name = plant.shortName();
        var empty = (FlowerPotBlock) Blocks.FLOWER_POT;
        var pot = PMRegistries.regBlock(
            "potted_" + name,
            () -> (Block) new FlowerPotBlock(
                () -> empty, plant,
                Block.Properties.copy(Blocks.FLOWER_POT)
                    .instabreak()
                    .noOcclusion()
                    .lightLevel(s -> light)
            )
        )
            .tag(BlockTags.FLOWER_POTS)
            .blockStateGenerator(
                (block, generator) -> {
                    ModelFile model;
                    var texture = "block/" + name;

                    if(name.endsWith("_rose"))
                        texture = "block/rose/" + name.substring(0, name.length() - 5);
                    else if(name.endsWith("_crystal_cluster"))
                        texture = "block/crystal_cluster/" + name.substring(0, name.length() - 16);
                    else if(name == "ender_pearl_cluster")
                        texture = "block/crystal_cluster/ender_pearl";
                    else if(name.contains("autumn"))
                        texture = "block/autumn_sapling/" + name.substring(0, name.length() - 15);

                    if(hasCustomModel)
                        model = generator.existingModel("potted/" + name);
                    else if(name.endsWith("_crystal_cluster"))
                        model = generator.models()
                            .withExistingParent("block/potted/" + name, generator.modLoc("block/template/potted_crystal"))
                            .texture("texture", generator.modLoc(texture));
                    else 
                        model = generator.models()
                            .withExistingParent("block/potted/" + name, "block/flower_pot_cross")
                            .texture("plant", generator.modLoc(texture))
                            .renderType("cutout");

                    generator.simpleBlock(block.get(), model);
                }
            )
            .lootTable((block, generator) -> generator.dropPottedContents(block));

        empty.addPlant(new ResourceLocation(ParadiseMod.ID, name), pot);
        return pot;
    }
}