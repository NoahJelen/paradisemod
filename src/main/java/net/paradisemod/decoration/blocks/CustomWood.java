package net.paradisemod.decoration.blocks;

import com.google.common.collect.ImmutableMap;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.paradisemod.base.mixin.AxeItemAccessor;

public class CustomWood extends RotatedPillarBlock {
    public CustomWood(Block strippedWood) {
        super(Properties.copy(Blocks.OAK_LOG));
        setStrippedWood(strippedWood);
    }

    public CustomWood(Block strippedWood, boolean glows) {
        super(Properties.copy(Blocks.OAK_LOG).lightLevel(s -> glows ? 7 : 0));
        setStrippedWood(strippedWood);
    }

    private void setStrippedWood(Block strippedWood) {
        var strippedWoods = AxeItemAccessor.getStrippableWoods();
        var newMap = new ImmutableMap.Builder<Block, Block>();
        newMap.putAll(strippedWoods);
        newMap.put(this, strippedWood);
        AxeItemAccessor.setStrippableWoods(newMap.build());
    }
}