package net.paradisemod.decoration.blocks;

import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.damagesource.DamageSources;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ItemModelGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.world.PMWorld;


public class PricklyPear extends Block implements ModeledBlock {
    private static final VoxelShape SHAPE_X = Block.box(4, 0, 6.5, 12, 16, 9.5);
    private static final VoxelShape SHAPE_Z = Block.box(6.5, 0, 4, 9.5, 16, 12);
    public static final EnumProperty<Direction.Axis> AXIS = BlockStateProperties.HORIZONTAL_AXIS;
    public static final EnumProperty<TiltDir> TILT_DIR = EnumProperty.create("tilt_dir", TiltDir.class);
    public static final BooleanProperty HAS_FRUIT = BooleanProperty.create("has_fruit");

    public PricklyPear() {
        super(Block.Properties.copy(Blocks.CACTUS).sound(SoundType.GRASS).sound(SoundType.WOOL).randomTicks().noOcclusion());
        registerDefaultState(
            stateDefinition.any()
                .setValue(AXIS, Direction.Axis.X)
                .setValue(TILT_DIR, TiltDir.NONE)
                .setValue(HAS_FRUIT, false)
        );
    }

    @Override
    public void randomTick(BlockState state, ServerLevel world, BlockPos pos, RandomSource rand) {
        var blockY = pos.getY();
        var groundY = PMWorld.getGroundLevel(world, world.getMinBuildHeight(), blockY + 3, pos);

        if(groundY.isPresent())
            if(blockY - groundY.getAsInt() < 10) {
                BlockPos[] spreadTo = {pos.above().east(), pos.above().west(), pos.above()};

                if(state.getValue(AXIS) == Direction.Axis.Z) {
                    spreadTo[0] = pos.above().north();
                    spreadTo[1] = pos.above().south();
                }

                var newpos = spreadTo[rand.nextInt(3)];
                if(!world.getBlockState(newpos).isAir()) return;
                if(world.getBlockState(newpos).is(this)) return;
                world.setBlockAndUpdate(newpos, state.setValue(HAS_FRUIT, rand.nextBoolean()));
            }
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(AXIS, TILT_DIR, HAS_FRUIT);
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter world, BlockPos pos, CollisionContext context) {
        if(state.getValue(AXIS) == Direction.Axis.Z) return SHAPE_Z;
        else return SHAPE_X;
    }

    @Override
    public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor level, BlockPos pos, BlockPos facingPos) {
        if(level instanceof Level world) {
            if ((world.getBlockState(pos.east()).getBlock() == this && world.getBlockState(pos.west()).getBlock() == this) || (world.getBlockState(pos.north()).getBlock() == this && world.getBlockState(pos.south()).getBlock() == this))
                return state.setValue(TILT_DIR, TiltDir.NONE);
            else if (world.getBlockState(pos.below().east()).getBlock() == this) return state.setValue(TILT_DIR,TiltDir.WEST);
            else if (world.getBlockState(pos.below().west()).getBlock() == this) return state.setValue(TILT_DIR,TiltDir.EAST);
            else if (world.getBlockState(pos.below().north()).getBlock() == this) return state.setValue(TILT_DIR,TiltDir.SOUTH);
            else if (world.getBlockState(pos.below().south()).getBlock() == this) return state.setValue(TILT_DIR,TiltDir.NORTH);
        }
        return state;
    }

    @Override
    public VoxelShape getCollisionShape(BlockState state, BlockGetter world, BlockPos pos, CollisionContext ctx) { return this.getShape(state, world, pos, ctx); }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        var state = defaultBlockState();
        var facing = context.getNearestLookingDirection();
        var pos = context.getClickedPos();
        var world = context.getLevel();
        state = switch (facing.getAxis()) {
            case X -> state.setValue(AXIS, Direction.Axis.Z);
            case Y, Z -> state.setValue(AXIS, Direction.Axis.X);
        };

        if (world.getBlockState(pos.east()).getBlock() == this && world.getBlockState(pos.west()).getBlock() == this || world.getBlockState(pos.north()).getBlock() == this && world.getBlockState(pos.south()).getBlock() == this)
            state = state.setValue(TILT_DIR, TiltDir.NONE);

        else if (world.getBlockState(pos.below().east()).getBlock() == this) state = state.setValue(TILT_DIR,TiltDir.WEST);
        else if (world.getBlockState(pos.below().west()).getBlock() == this) state = state.setValue(TILT_DIR,TiltDir.EAST);
        else if (world.getBlockState(pos.below().north()).getBlock() == this) state = state.setValue(TILT_DIR,TiltDir.SOUTH);
        else if (world.getBlockState(pos.below().south()).getBlock() == this) state = state.setValue(TILT_DIR,TiltDir.NORTH);
        return state;
    }

    @Override
    public void neighborChanged(BlockState state, Level world, BlockPos pos, Block block, BlockPos fromPos, boolean isMoving) {
        if (canSurvive(state, world, pos)) return;
        dropResources(state, world, pos);
        world.setBlockAndUpdate(pos, Blocks.AIR.defaultBlockState());
    }

    @Override
    public boolean canSurvive(BlockState state, LevelReader world, BlockPos pos) {
        var sideBlocks = List.of(
            world.getBlockState(pos.below().east()),
            world.getBlockState(pos.below().west()),
            world.getBlockState(pos.below().north()),
            world.getBlockState(pos.below().south())
        );

        var blockBelow = world.getBlockState(pos.below());
        for (var sideBlock: sideBlocks) if (sideBlock.is(this)) return true;
        return blockBelow.is(PMTags.Blocks.GROUND_BLOCKS) || blockBelow.is(this);
    }

    @Override
    public void entityInside(BlockState state, Level world, BlockPos pos, Entity entity) {
        if(entity instanceof LivingEntity creature) {
            var damageSources = new DamageSources(world.registryAccess());
            creature.hurt(damageSources.cactus(), 2f);
        }
    }

    @Override
    public void genItemModel(ItemModelGenerator generator) {
        generator.parentBlockItem(this, "prickly_pear_up");
    }

    @Override
    public void genBlockState(BlockStateGenerator generator) {
        var empty = generator.existingMCModel("air");
        var up = generator.existingModel("prickly_pear_up");
        var tilted = generator.existingModel("prickly_pear_tilted");
        var fruitUp = generator.existingModel("prickly_pear_up_fruit");
        var fruitTilted = generator.existingModel("prickly_pear_tilted_fruit");

        generator.getVariantBuilder(this)
            .forAllStates(
                state -> {
                    Direction.Axis axis = state.getValue(AXIS);
                    TiltDir dir = state.getValue(TILT_DIR);
                    boolean hasFruit = state.getValue(HAS_FRUIT);

                    if(dir == TiltDir.NONE)
                        return generator.buildVariantModel(hasFruit ? fruitUp : up, 0, axis == Direction.Axis.Z ? 90 : 0);
                    else if(dir.isAxis(axis)) 
                        return generator.buildVariantModel(hasFruit ? fruitTilted : tilted, dir.toDirection(), true);
                    else return generator.buildVariantModel(empty);
                }
            );
    }

    // represents the direction the prickly pear leaf is tilting
    public enum TiltDir implements StringRepresentable {
        EAST,
        WEST,
        NORTH,
        SOUTH,
        NONE;

        @Override
        public String getSerializedName() { return name().toLowerCase(); }

        public Direction toDirection() {
            return switch(this) {
                case NONE, WEST -> Direction.WEST;
                case EAST -> Direction.EAST;
                case NORTH -> Direction.NORTH;
                case SOUTH -> Direction.SOUTH;
            };
        }

        public boolean isAxis(Direction.Axis axis) {
            return switch(axis) {
                case X -> this == EAST || this == WEST;
                case Z -> this == NORTH || this == SOUTH;
                default -> false;
            };
        }
    }
}