package net.paradisemod.decoration.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.CraftingTableBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.paradisemod.base.BlockType;
import net.paradisemod.decoration.ui.CustomTableMenu;

public class CustomTable extends CraftingTableBlock {
    public CustomTable(boolean glows) {
        super(BlockType.WOOD.getProperties().lightLevel((s -> glows ? 7 : 0)));
    }

    @Override
    public MenuProvider getMenuProvider(BlockState state, Level world, BlockPos pos) {
        return new SimpleMenuProvider(
            (id, inventory, player) ->
                new CustomTableMenu(id, inventory, this, ContainerLevelAccess.create(world, pos)),
            Component.translatable("container.crafting")
        );
    }
}