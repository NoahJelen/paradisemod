package net.paradisemod.decoration;

import java.util.EnumMap;

import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.LanternBlock;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.data.assets.PMTranslations;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;

public class ColoredLanterns {
    public static final EnumMap<DyeColor, RegisteredBlock> LANTERNS = new EnumMap<>(DyeColor.class);

    // colored lanterns
    public static final RegisteredBlock BLACK_LANTERN = regLantern(DyeColor.BLACK);
    public static final RegisteredBlock BLUE_LANTERN = regLantern(DyeColor.BLUE);
    public static final RegisteredBlock BROWN_LANTERN = regLantern(DyeColor.BROWN);
    public static final RegisteredBlock CYAN_LANTERN = regLantern(DyeColor.CYAN);
    public static final RegisteredBlock GRAY_LANTERN = regLantern(DyeColor.GRAY);
    public static final RegisteredBlock GREEN_LANTERN = regLantern(DyeColor.GREEN);
    public static final RegisteredBlock LIGHT_BLUE_LANTERN = regLantern(DyeColor.LIGHT_BLUE);
    public static final RegisteredBlock LIGHT_GRAY_LANTERN = regLantern(DyeColor.LIGHT_GRAY);
    public static final RegisteredBlock LIME_LANTERN = regLantern(DyeColor.LIME);
    public static final RegisteredBlock MAGENTA_LANTERN = regLantern(DyeColor.MAGENTA);
    public static final RegisteredBlock ORANGE_LANTERN = regLantern(DyeColor.ORANGE);
    public static final RegisteredBlock PINK_LANTERN = regLantern(DyeColor.PINK);
    public static final RegisteredBlock PURPLE_LANTERN = regLantern(DyeColor.PURPLE);
    public static final RegisteredBlock RED_LANTERN = regLantern(DyeColor.RED);
    public static final RegisteredBlock WHITE_LANTERN = regLantern(DyeColor.WHITE);
    public static final RegisteredBlock YELLOW_LANTERN = regLantern(DyeColor.YELLOW);

    public static void init() { }

    private static RegisteredBlock regLantern(DyeColor color) {
        var lantern = PMRegistries.regBlockItem(
            color.getName() + "_lantern", () -> new LanternBlock(Block.Properties.copy(Blocks.LANTERN))
        )
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item, 8)
                    .pattern("lll")
                    .pattern("ldl")
                    .pattern("lll")
                    .define('d', ParadiseMod.DYES_BY_COLOR.get(color))
                    .define('l', Blocks.LANTERN)
            )
            .tab(CreativeModeTabs.FUNCTIONAL_BLOCKS)
            .tags(
                PMTags.Blocks.BUOY_LANTERNS,
                PMTags.Blocks.LANTERNS
            )
            .itemModel((block, generator) -> generator.basicItem(block.asItem(), "lantern/" + color.getName()))
            .blockStateGenerator(
                (block, generator) -> {
                    var modelBuilder = generator.models();
                    var lanternModel = modelBuilder.withExistingParent("block/lantern/" + color.getName(), "block/template_lantern")
                        .renderType("cutout")
                        .texture("lantern", "paradisemod:block/lantern/" + color.getName());

                    var hangingModel = modelBuilder.withExistingParent("block/lantern/" + color.getName() + "_hanging", "block/template_hanging_lantern")
                        .renderType("cutout")
                        .texture("lantern", "paradisemod:block/lantern/" + color.getName());

                    generator.getVariantBuilder(block.get())
                        .forAllStatesExcept(
                            state -> {
                                if(state.getValue(LanternBlock.HANGING))
                                    return generator.buildVariantModel(hangingModel);
                                else
                                    return generator.buildVariantModel(lanternModel);
                            },
                            LanternBlock.WATERLOGGED
                        );
                }
            )
            .localizedName(
                PMTranslations.englishColor(color) + " Lantern",
                "Farol " + PMTranslations.spanishColor(color, false)
            );

        LANTERNS.put(color, lantern);
        return lantern;
    }
}