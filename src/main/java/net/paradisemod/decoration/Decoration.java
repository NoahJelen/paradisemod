package net.paradisemod.decoration;

import java.util.EnumMap;

import net.minecraft.advancements.critereon.StatePropertiesPredicate;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemNameBlockItem;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.*;
import net.minecraft.world.level.block.grower.AbstractTreeGrower;
import net.minecraft.world.level.block.state.BlockBehaviour.Properties;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.functions.ApplyBonusCount;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.predicates.LootItemBlockStatePropertyCondition;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.BlockTemplates;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.Events;
import net.paradisemod.base.Utils;
import net.paradisemod.base.blocks.CustomPlant;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.PMTranslations;
import net.paradisemod.base.data.loot.BlockLootGenerator;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.base.registry.RegisteredItem;
import net.paradisemod.decoration.blocks.PricklyPear;
import net.paradisemod.decoration.blocks.SoulPumpkin;
import net.paradisemod.misc.Misc;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.trees.AutumnTree;
import net.paradisemod.world.trees.MesquiteTree;
import net.paradisemod.world.trees.PaloVerdeTree;

public class Decoration {
    // metal bars
    public static final RegisteredBlock NETHERITE_BARS = regMetalBars("netherite", SoundType.NETHERITE_BLOCK);
    public static final RegisteredBlock SILVER_BARS = regMetalBars("silver", SoundType.METAL);
    public static final RegisteredBlock RUSTED_IRON_BARS = regMetalBars("rusted_iron", SoundType.METAL);

    // glass
    public static final RegisteredBlock BLASTPROOF_GLASS = PMRegistries.regBlockItem(
        "blastproof_glass",
        () -> new GlassBlock(
            Block.Properties.copy(Blocks.GLASS)
                .sound(SoundType.GLASS)
                .noOcclusion()
                .strength(0.3F, 1200F)
        )
    )
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item, 8)
                .pattern("ooo")
                .pattern("ogo")
                .pattern("ooo")
                .define('g', Blocks.GLASS)
                .define('o', Blocks.OBSIDIAN)
        )
        .tab(CreativeModeTabs.COLORED_BLOCKS)
        .renderType("translucent")
        .localizedName("Blastproof Glass", "Vidrio resistente a explosiones");

    public static final RegisteredBlock SOUL_GLASS = PMRegistries.regBlockItem(
        "soul_glass",
        () -> new GlassBlock(
            Block.Properties.copy(Blocks.GLASS)
                .sound(SoundType.GLASS)
                .lightLevel(s -> 15)
                .noOcclusion()
                .strength(0.3F)
                .friction(1.2f)
        )
    )
        .oreRecipes(Blocks.SOUL_SAND)
        .tab(CreativeModeTabs.COLORED_BLOCKS)
        .tag(BlockTags.SOUL_FIRE_BASE_BLOCKS)
        .renderType("translucent")
        .dropsWithSilkTouch()
        .localizedName("Soul Glass", "Vidrio de almas");

    public static final EnumMap<DyeColor, RegisteredBlock> TINTED_BLASTPROOF_GLASS = new EnumMap<>(DyeColor.class);

    public static final RegisteredBlock BLACK_TINTED_BLASTPROOF_GLASS = regTintedBlastproofGlass(DyeColor.BLACK);
    public static final RegisteredBlock BLUE_TINTED_BLASTPROOF_GLASS = regTintedBlastproofGlass(DyeColor.BLUE);
    public static final RegisteredBlock BROWN_TINTED_BLASTPROOF_GLASS = regTintedBlastproofGlass(DyeColor.BROWN);
    public static final RegisteredBlock CYAN_TINTED_BLASTPROOF_GLASS = regTintedBlastproofGlass(DyeColor.CYAN);
    public static final RegisteredBlock GRAY_TINTED_BLASTPROOF_GLASS = regTintedBlastproofGlass(DyeColor.GRAY);
    public static final RegisteredBlock GREEN_TINTED_BLASTPROOF_GLASS = regTintedBlastproofGlass(DyeColor.GREEN);
    public static final RegisteredBlock LIGHT_BLUE_TINTED_BLASTPROOF_GLASS = regTintedBlastproofGlass(DyeColor.LIGHT_BLUE);
    public static final RegisteredBlock LIGHT_GRAY_TINTED_BLASTPROOF_GLASS = regTintedBlastproofGlass(DyeColor.LIGHT_GRAY);
    public static final RegisteredBlock LIME_TINTED_BLASTPROOF_GLASS = regTintedBlastproofGlass(DyeColor.LIME);
    public static final RegisteredBlock MAGENTA_TINTED_BLASTPROOF_GLASS = regTintedBlastproofGlass(DyeColor.MAGENTA);
    public static final RegisteredBlock ORANGE_TINTED_BLASTPROOF_GLASS = regTintedBlastproofGlass(DyeColor.ORANGE);
    public static final RegisteredBlock PINK_TINTED_BLASTPROOF_GLASS = regTintedBlastproofGlass(DyeColor.PINK);
    public static final RegisteredBlock PURPLE_TINTED_BLASTPROOF_GLASS = regTintedBlastproofGlass(DyeColor.PURPLE);
    public static final RegisteredBlock RED_TINTED_BLASTPROOF_GLASS = regTintedBlastproofGlass(DyeColor.RED);
    public static final RegisteredBlock WHITE_TINTED_BLASTPROOF_GLASS = regTintedBlastproofGlass(DyeColor.WHITE);
    public static final RegisteredBlock YELLOW_TINTED_BLASTPROOF_GLASS = regTintedBlastproofGlass(DyeColor.YELLOW);

    // glass panes
    public static final RegisteredBlock BLASTPROOF_GLASS_PANE = PMRegistries.regBlockItem(
        "blastproof_glass_pane",
        () -> new IronBarsBlock(
            Block.Properties.copy(Blocks.GLASS)
                .sound(SoundType.GLASS)
                .noOcclusion()
                .strength( 0.3F, 1200F)
        )
    )
        .tab(CreativeModeTabs.COLORED_BLOCKS)
        .blockStateGenerator((block, generator) -> genPaneBlockState("blastproof_glass", "blastproof_glass", block, generator))
        .itemModel((block, generator) -> generator.glassPaneItem("blastproof_glass_pane", "blastproof_glass"))
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item, 16)
                .pattern("ooo")
                .pattern("ooo")
                .define('o', BLASTPROOF_GLASS)
        )
        .localizedName("Blastproof Glass Pane", "Panel de vidrio resistente a explosiones");

    public static final RegisteredBlock SOUL_GLASS_PANE = PMRegistries.regBlockItem(
        "soul_glass_pane",
        () -> new IronBarsBlock(
            Block.Properties.copy(Blocks.GLASS)
                .sound(SoundType.GLASS)
                .lightLevel(s -> 15)
                .noOcclusion()
                .strength(0.3F)
                .friction(1.2F)
        )
    )
        .tab(CreativeModeTabs.COLORED_BLOCKS)
        .blockStateGenerator((block, generator) -> genPaneBlockState("soul_glass", "soul_glass",block, generator))
        .itemModel((block, generator) -> generator.glassPaneItem("soul_glass_pane", "soul_glass"))
        .dropsWithSilkTouch()
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item, 16)
                .pattern("ooo")
                .pattern("ooo")
                .define('o', SOUL_GLASS)
        )
        .localizedName("Soul Glass Pane", "Panel de vidrio de almas");

    public static final RegisteredBlock BLACK_TINTED_BLASTPROOF_GLASS_PANE = regTintedBlastproofGlassPane(DyeColor.BLACK);
    public static final RegisteredBlock BLUE_TINTED_BLASTPROOF_GLASS_PANE = regTintedBlastproofGlassPane(DyeColor.BLUE);
    public static final RegisteredBlock BROWN_TINTED_BLASTPROOF_GLASS_PANE = regTintedBlastproofGlassPane(DyeColor.BROWN);
    public static final RegisteredBlock CYAN_TINTED_BLASTPROOF_GLASS_PANE = regTintedBlastproofGlassPane(DyeColor.CYAN);
    public static final RegisteredBlock GRAY_TINTED_BLASTPROOF_GLASS_PANE = regTintedBlastproofGlassPane(DyeColor.GRAY);
    public static final RegisteredBlock GREEN_TINTED_BLASTPROOF_GLASS_PANE = regTintedBlastproofGlassPane(DyeColor.GREEN);
    public static final RegisteredBlock LIGHT_BLUE_TINTED_BLASTPROOF_GLASS_PANE = regTintedBlastproofGlassPane(DyeColor.LIGHT_BLUE);
    public static final RegisteredBlock LIGHT_GRAY_TINTED_BLASTPROOF_GLASS_PANE = regTintedBlastproofGlassPane(DyeColor.LIGHT_GRAY);
    public static final RegisteredBlock LIME_TINTED_BLASTPROOF_GLASS_PANE = regTintedBlastproofGlassPane(DyeColor.LIME);
    public static final RegisteredBlock MAGENTA_TINTED_BLASTPROOF_GLASS_PANE = regTintedBlastproofGlassPane(DyeColor.MAGENTA);
    public static final RegisteredBlock ORANGE_TINTED_BLASTPROOF_GLASS_PANE = regTintedBlastproofGlassPane(DyeColor.ORANGE);
    public static final RegisteredBlock PINK_TINTED_BLASTPROOF_GLASS_PANE = regTintedBlastproofGlassPane(DyeColor.PINK);
    public static final RegisteredBlock PURPLE_TINTED_BLASTPROOF_GLASS_PANE = regTintedBlastproofGlassPane(DyeColor.PURPLE);
    public static final RegisteredBlock RED_TINTED_BLASTPROOF_GLASS_PANE = regTintedBlastproofGlassPane(DyeColor.RED);
    public static final RegisteredBlock WHITE_TINTED_BLASTPROOF_GLASS_PANE = regTintedBlastproofGlassPane(DyeColor.WHITE);
    public static final RegisteredBlock YELLOW_TINTED_BLASTPROOF_GLASS_PANE = regTintedBlastproofGlassPane(DyeColor.YELLOW);

    // prickly pear cactus
    public static final RegisteredBlock PRICKLY_PEAR = PMRegistries.regBlockItem("prickly_pear", PricklyPear::new)
        .tab(CreativeModeTabs.NATURAL_BLOCKS)
        .localizedName("Prickly Pear Cactus", "Cactus de nopal")
        .lootTable(
            (block, generator) -> {
                generator.add(
                    block,
                    blockIn -> LootTable.lootTable()
                        .withPool(
                            LootPool.lootPool()
                                .setRolls(ConstantValue.exactly(1))
                                .add(
                                    LootItem.lootTableItem(blockIn)
                                        .when(BlockLootGenerator.hasSilkTouch())
                                        .otherwise(
                                            generator.applyExplosionDecay(
                                                blockIn,
                                                LootItem.lootTableItem(Misc.NOPAL)
                                                    .apply(
                                                        SetItemCountFunction.setCount(ConstantValue.exactly(1))
                                                    )
                                                    .apply(ApplyBonusCount.addUniformBonusCount(Enchantments.BLOCK_FORTUNE))
                                            )
                                        )
                                )
                        )
                        .withPool(
                            LootPool.lootPool()
                                .setRolls(ConstantValue.exactly(1))
                                .add(LootItem.lootTableItem(Misc.PRICKLY_PEAR_FRUIT))
                                .when(
                                    LootItemBlockStatePropertyCondition.hasBlockStateProperties(blockIn)
                                        .setProperties(
                                            StatePropertiesPredicate.Builder.properties()
                                                .hasProperty(PricklyPear.HAS_FRUIT, true)
                                            )
                                )
                        )
                );
            }
        );

    // roses
    public static final EnumMap<DyeColor, RegisteredBlock> ROSES = new EnumMap<>(DyeColor.class);
    public static final RegisteredBlock BLACK_ROSE = regRose(DyeColor.BLACK);
    public static final RegisteredBlock BLUE_ROSE = regRose(DyeColor.BLUE);
    public static final RegisteredBlock BROWN_ROSE = regRose(DyeColor.BROWN);
    public static final RegisteredBlock CYAN_ROSE = regRose(DyeColor.CYAN);
    public static final RegisteredBlock GRAY_ROSE = regRose(DyeColor.GRAY);
    public static final RegisteredBlock GREEN_ROSE = regRose(DyeColor.GREEN);
    public static final RegisteredBlock LIGHT_BLUE_ROSE = regRose(DyeColor.LIGHT_BLUE);
    public static final RegisteredBlock LIGHT_GRAY_ROSE = regRose(DyeColor.LIGHT_GRAY);
    public static final RegisteredBlock LIME_ROSE = regRose(DyeColor.LIME);
    public static final RegisteredBlock MAGENTA_ROSE = regRose(DyeColor.MAGENTA);
    public static final RegisteredBlock ORANGE_ROSE = regRose(DyeColor.ORANGE);
    public static final RegisteredBlock PINK_ROSE = regRose(DyeColor.PINK);
    public static final RegisteredBlock PURPLE_ROSE = regRose(DyeColor.PURPLE);
    public static final RegisteredBlock RED_ROSE = regRose(DyeColor.RED);
    public static final RegisteredBlock WHITE_ROSE = regRose(DyeColor.WHITE);
    public static final RegisteredBlock YELLOW_ROSE = regRose(DyeColor.YELLOW);

    public static final RegisteredBlock ENDER_ROSE = PMRegistries.regBlockItem(
        "ender_rose",
        () -> new CustomPlant(false, false, CustomPlant.Type.END)
    )
        .tab(CreativeModeTabs.NATURAL_BLOCKS)
        .blockStateGenerator((block, generator) -> generator.genSimplePlant(block.get(), "rose/ender"))
        .tag(PMTags.Blocks.END_FOLIAGE)
        .localizedName("Ender Rose", "Rosa del Fin");

    // soul pumpkins
    public static final RegisteredBlock SOUL_PUMPKIN = PMRegistries.regBlockItem(
        "soul_pumpkin",
        SoulPumpkin::new
    )
        .tab(CreativeModeTabs.NATURAL_BLOCKS)
        .tag(BlockTags.MINEABLE_WITH_AXE)
        .blockStateGenerator(
            (block, generator) -> {
                var top = generator.modLoc("block/soul_pumpkin_top");
                var side = generator.modLoc("block/soul_pumpkin_side");
                var model = generator.models().cubeBottomTop("soul_pumpkin", side, top, top);
                generator.simpleBlock(block, model);
            }
        )
        .localizedName("Soul Pumpkin", "Calabaza de almas");

    public static final RegisteredBlock CARVED_SOUL_PUMPKIN = PMRegistries.regBlockItem(
        "carved_soul_pumpkin",
        () -> new CarvedPumpkinBlock(Block.Properties.copy(Blocks.CARVED_PUMPKIN))
    )
        .tab(CreativeModeTabs.NATURAL_BLOCKS)
        .blockStateGenerator(
            (block, generator) -> {
                var top = generator.modLoc("block/soul_pumpkin_top");
                var front = generator.modLoc("block/carved_soul_pumpkin");
                var side = generator.modLoc("block/soul_pumpkin_side");
                var model = generator.models().orientable("carved_soul_pumpkin", side, front, top);

                generator.getVariantBuilder(block.get())
                    .forAllStates(
                        state -> {
                            Direction facing = state.getValue(HorizontalDirectionalBlock.FACING);
                            return generator.buildVariantModel(model, facing, true);
                        }
                    );
            }
        )
        .tag(BlockTags.MINEABLE_WITH_AXE)
        .localizedName("Carved Soul Pumpkin", "Calabaza tallada de almas");

    public static final RegisteredBlock SOUL_JACK_O_LANTERN = PMRegistries.regBlockItem(
        "soul_jack_o_lantern",
        () -> new CarvedPumpkinBlock(Block.Properties.copy(Blocks.JACK_O_LANTERN))
    )
        .tab(CreativeModeTabs.NATURAL_BLOCKS)
        .blockStateGenerator(
            (block, generator) -> {
                var top = generator.modLoc("block/soul_pumpkin_top");
                var front = generator.modLoc("block/soul_jack_o_lantern");
                var side = generator.modLoc("block/soul_pumpkin_side");
                var model = generator.models().orientable("soul_jack_o_lantern", side, front, top);

                generator.getVariantBuilder(block.get())
                    .forAllStates(
                        state -> {
                            Direction facing = state.getValue(HorizontalDirectionalBlock.FACING);
                            return generator.buildVariantModel(model, facing, true);
                        }
                    );
            }
        )
        .tags(
            BlockTags.MINEABLE_WITH_AXE,
            BlockTags.PIGLIN_REPELLENTS
        )
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.MISC, item, CARVED_SOUL_PUMPKIN, Blocks.TORCH))
        .localizedName("Soul Jack o'Lantern ", "Calabaza de almas de Halloween");

    public static final RegisteredItem SOUL_PUMPKIN_SEEDS = PMRegistries.regItem(
        "soul_pumpkin_seeds",
        () -> new ItemNameBlockItem(Decoration.SOUL_PUMPKIN_STEM.get(), new Item.Properties())
    )
        .tab(CreativeModeTabs.NATURAL_BLOCKS)
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.MISC, item, SOUL_PUMPKIN))
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.MISC, item, CARVED_SOUL_PUMPKIN))
        .localizedName("Soul Pumpkin Seeds", "Semillas de calabaza de almas");

    public static final RegisteredBlock SOUL_PUMPKIN_STEM = PMRegistries.regBlock("soul_pumpkin_stem",
        () -> new StemBlock((StemGrownBlock) SOUL_PUMPKIN.get(), SOUL_PUMPKIN_SEEDS, Block.Properties.copy(Blocks.PUMPKIN_STEM)) {
            @Override
            public boolean mayPlaceOn(BlockState state, BlockGetter world, BlockPos pos) { return state.is(Blocks.SOUL_SAND) || state.is(Blocks.SOUL_SOIL); }
        }
    )
        .blockStateGenerator(
            (block, generator) -> {
                generator.getVariantBuilder(block.get())
                    .forAllStates(
                        state -> {
                            int age = state.getValue(StemBlock.AGE);
                            var model = generator.models().withExistingParent("soul_pumpkin_stem" + age, "block/stem_growth" + age)
                                .texture("stem", "paradisemod:block/soul_pumpkin_stem");

                            return generator.buildVariantModel(model);
                        }
                    );
            }
        )
        .dropsItem(SOUL_PUMPKIN_SEEDS);

    public static final RegisteredBlock ATTACHED_SOUL_PUMPKIN_STEM = PMRegistries.regBlock( "attached_soul_pumpkin_stem",
        () -> new AttachedStemBlock((StemGrownBlock) SOUL_PUMPKIN.get(), SOUL_PUMPKIN_SEEDS, Block.Properties.copy(Blocks.PUMPKIN_STEM)) {
            @Override
            public boolean mayPlaceOn(BlockState state, BlockGetter world, BlockPos pos) { return state.is(Blocks.SOUL_SAND) || state.is(Blocks.SOUL_SOIL); }
        }
    )
        .blockStateGenerator(
            (block, generator) -> {
                var model = generator.models().withExistingParent("attached_soul_pumpkin_stem", "block/stem_fruit")
                    .texture("stem", "paradisemod:block/soul_pumpkin_stem")
                    .texture("upperstem", "paradisemod:block/attached_soul_pumpkin_stem");

                generator.getVariantBuilder(block.get())
                    .forAllStates(
                        state -> {
                            Direction facing = state.getValue(AttachedStemBlock.FACING);
                            return generator.buildVariantModel(model, facing, true);
                        }
                    );
            }
        )
        .dropsItem(SOUL_PUMPKIN_SEEDS);

    // autumn saplings
    public static final RegisteredBlock BLUE_AUTUMN_SAPLING = regAutumnSapling(AutumnTree.Color.BLUE);
    public static final RegisteredBlock ORANGE_AUTUMN_SAPLING = regAutumnSapling(AutumnTree.Color.ORANGE);
    public static final RegisteredBlock RED_AUTUMN_SAPLING = regAutumnSapling(AutumnTree.Color.RED);
    public static final RegisteredBlock YELLOW_AUTUMN_SAPLING = regAutumnSapling(AutumnTree.Color.YELLOW);

    // saplings
    public static final RegisteredBlock PALO_VERDE_SAPLING = regSapling("palo_verde", new PaloVerdeTree(), false, false);
    public static final RegisteredBlock MESQUITE_SAPLING = regSapling("mesquite", new MesquiteTree(), false, false);

    // autumn leaves
    public static RegisteredBlock BLUE_AUTUMN_LEAVES = autumnLeaves(AutumnTree.Color.BLUE, BLUE_AUTUMN_SAPLING);
    public static RegisteredBlock ORANGE_AUTUMN_LEAVES = autumnLeaves(AutumnTree.Color.ORANGE, ORANGE_AUTUMN_SAPLING);
    public static RegisteredBlock RED_AUTUMN_LEAVES = autumnLeaves(AutumnTree.Color.RED, RED_AUTUMN_SAPLING);
    public static RegisteredBlock YELLOW_AUTUMN_LEAVES = autumnLeaves(AutumnTree.Color.YELLOW, YELLOW_AUTUMN_SAPLING);

    // leaves
    public static RegisteredBlock PALO_VERDE_LEAVES = BlockTemplates.leaves("palo_verde", "acacia", PALO_VERDE_SAPLING, false);
    public static RegisteredBlock MESQUITE_LEAVES = BlockTemplates.leaves("mesquite", "oak", MESQUITE_SAPLING, false);

    // logs
    public static final RegisteredBlock STRIPPED_PALO_VERDE_LOG = BlockTemplates.strippedLog("palo_verde", false, false);
    public static final RegisteredBlock PALO_VERDE_LOG = BlockTemplates.log("palo_verde", false, STRIPPED_PALO_VERDE_LOG, false);
    public static final RegisteredBlock STRIPPED_PALO_VERDE_WOOD = BlockTemplates.strippedLog("palo_verde", true, false);
    public static final RegisteredBlock PALO_VERDE_WOOD = BlockTemplates.log("palo_verde", true, STRIPPED_PALO_VERDE_WOOD, false);

    public static final RegisteredBlock STRIPPED_MESQUITE_LOG = BlockTemplates.strippedLog("mesquite", false, false);
    public static final RegisteredBlock MESQUITE_LOG = BlockTemplates.log("mesquite", false, STRIPPED_MESQUITE_LOG, false);
    public static final RegisteredBlock STRIPPED_MESQUITE_WOOD = BlockTemplates.strippedLog("mesquite", true, false);
    public static final RegisteredBlock MESQUITE_WOOD = BlockTemplates.log("mesquite", true, STRIPPED_MESQUITE_WOOD, false);

    // bookshelves
    public static final RegisteredBlock CACTUS_BOOKSHELF = regBookshelf("cactus", false);
    public static final RegisteredBlock BAMBOO_BOOKSHELF = regBookshelf("bamboo", false);
    public static final RegisteredBlock PALO_VERDE_BOOKSHELF = regBookshelf("palo_verde", false);
    public static final RegisteredBlock MESQUITE_BOOKSHELF = regBookshelf("mesquite", false);

    public static final RegisteredBlock BLACKENED_OAK_BOOKSHELF = regBookshelf("blackened_oak", false)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_SPRUCE_BOOKSHELF = regBookshelf("blackened_spruce", false)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_OAK_BOOKSHELF = regBookshelf("glowing_oak", true)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_CACTUS_BOOKSHELF = regBookshelf("glowing_cactus", true)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static void init() {
        // submodules
        Tables.init();
        ColoredLanterns.init();
        FlowerPots.init();
        ParadiseMod.LOG.info("Loaded Decoration module");
        Events.registerBasicColoredBlocks(0xC2DB3B, PALO_VERDE_LEAVES);
        Events.registerBasicColoredBlocks(0x53801E, MESQUITE_LEAVES);
    }

    public static RegisteredBlock regSapling(String name, AbstractTreeGrower tree, boolean deepDarkOverride, boolean glows) {
        return regSapling(
            name,
            name + "_sapling",
            tree,
            deepDarkOverride,
            glows
        );
    }

    private static RegisteredBlock regAutumnSapling(AutumnTree.Color color) {
        var modelName = "autumn_sapling/" + color.name().toLowerCase();
        return regSapling(
            color.name().toLowerCase() + "_autumn",
            modelName,
            new AutumnTree(color),
            false, false
        )
            .localizedName(
                color.localizedSaplingColor(false) + " Autumn Sapling", 
                "Retoño " + color.localizedSaplingColor(true) + " de otoño"
            );
    }

    public static RegisteredBlock regSapling(String name, String modelName, AbstractTreeGrower tree, boolean deepDarkOverride, boolean glows) {
        return PMRegistries.regBlockItem(
            name + "_sapling",
            () -> new SaplingBlock(
                tree,
                Properties.copy(Blocks.OAK_SAPLING)
                    .lightLevel(s -> glows ? 7 : 0)
            ) {
                @Override
                protected boolean mayPlaceOn(BlockState state, BlockGetter world, BlockPos pos) {
                    if(
                        state.is(DeepDarkBlocks.DARKSTONE.get()) ||
                        state.is(DeepDarkBlocks.GLOWING_NYLIUM.get()) ||
                        state.is(DeepDarkBlocks.OVERGROWN_DARKSTONE.get()) &&
                        deepDarkOverride
                    )
                        return true;
                    else return super.mayPlaceOn(state, world, pos);
                }
            }
        )
            .tab(CreativeModeTabs.NATURAL_BLOCKS)
            .blockStateGenerator((block, generator) -> generator.genSimplePlant(block.get(), modelName))
            .itemModelAlreadyExists()
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Sapling", 
                "Retoño de" + Utils.localizedMaterialName(name, true)
            );
    }

    private static RegisteredBlock regRose(DyeColor color) {
        RegisteredBlock rose = PMRegistries.regBlockItem(
            color.name().toLowerCase() + "_rose",
            () -> new CustomPlant(false, false, CustomPlant.Type.NORMAL)
        )
            .tab(CreativeModeTabs.NATURAL_BLOCKS)
            .blockStateGenerator((block, generator) -> generator.genSimplePlant(block.get(), "rose/" + color.getName()))
            .itemModelAlreadyExists()
            .tag(PMTags.Blocks.ROSES)
            .localizedName(PMTranslations.englishColor(color) + " Rose", "Rosa " + PMTranslations.spanishColor(color, true));

        ROSES.put(color, rose);
        return rose;
    }

    private static RegisteredBlock regTintedBlastproofGlass(DyeColor color) {
        var glass = PMRegistries.regBlockItem(
            color.getName() + "_tinted_blastproof_glass",
            () -> new GlassBlock(
                Block.Properties.copy(Blocks.GLASS)
                    .sound(SoundType.GLASS)
                    .noOcclusion()
                    .strength( 0.3F, 1200F)
            )
        )
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.DECORATIONS, item,8)
                    .pattern("ggg")
                    .pattern("gdg")
                    .pattern("ggg")
                    .define('g', BLASTPROOF_GLASS)
                    .define('d', ParadiseMod.DYES_BY_COLOR.get(color))
            )
            .tab(CreativeModeTabs.COLORED_BLOCKS)
            .itemModel((block, generator) -> generator.parentBlockItem(block, "tinted_blastproof_glass/" + color.getName()))
            .blockStateGenerator((block, generator) -> generator.simpleBlockWithRenderType(block.get(), "tinted_blastproof_glass/" + color.getName(), "translucent"))
            .localizedName(
                PMTranslations.englishColor(color) + " Blastproof Glass",
                "Vidrio " + PMTranslations.spanishColor(color, false) + " resistente a explosiones"
            );

        TINTED_BLASTPROOF_GLASS.put(color, glass);
        return glass;
    }

    private static RegisteredBlock regTintedBlastproofGlassPane(DyeColor color) {
        return PMRegistries.regBlockItem(
            color.getName() + "_tinted_blastproof_glass_pane",
            () -> new IronBarsBlock(
                Block.Properties.copy(Blocks.GLASS)
                    .sound(SoundType.GLASS)
                    .noOcclusion()
                    .strength(0.3F, 1200F)
            )
        )
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.DECORATIONS, item, 16)
                    .pattern("iii")
                    .pattern("iii")
                    .define('i', TINTED_BLASTPROOF_GLASS.get(color))
            )
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.DECORATIONS, item,8)
                    .pattern("ggg")
                    .pattern("gdg")
                    .pattern("ggg")
                    .define('g', BLASTPROOF_GLASS_PANE)
                    .define('d', ParadiseMod.DYES_BY_COLOR.get(color))
            )
            .tab(CreativeModeTabs.COLORED_BLOCKS)
            .itemModel(
                (block, generator) -> generator.glassPaneItem(
                    color.getName() + "_tinted_blastproof_glass_pane",
                    "tinted_blastproof_glass/" + color.getName()
                )
            )
            .blockStateGenerator(
                (block, generator) -> genPaneBlockState(
                    "tinted_blastproof_glass_pane/" + color.getName(),
                    "tinted_blastproof_glass/" + color.getName(),
                    block, generator
                )
            )
            .localizedName(
                PMTranslations.englishColor(color) + " Blastproof Glass Pane",
                "Panel de vidrio " + PMTranslations.spanishColor(color, false) + " resistente a explosiones"
            );
    }

    private static RegisteredBlock regBookshelf(String name, boolean glows) {
        return PMRegistries.regBlockItem(name + "_bookshelf",
            () -> new Block(BlockType.WOOD.getProperties().lightLevel(s -> glows ? 7 : 0))
        )
            .tab(CreativeModeTabs.FUNCTIONAL_BLOCKS)
            .tag(BlockTags.MINEABLE_WITH_AXE)
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.BUILDING_BLOCKS, item)
                    .pattern("ppp")
                    .pattern("bbb")
                    .pattern("ppp")
                    .define('p', Utils.getPlanks(name))
                    .define('b', Items.BOOK)
            )
            .blockStateGenerator(
                (block, generator) -> {
                    var topTexture = generator.modLoc("block/" + name + "_planks");

                    if(name.contains("cactus"))
                        topTexture = generator.modLoc("block/"+ name +"_block");
                    else if(name == "bamboo")
                        topTexture = generator.mcLoc("block/bamboo_planks");

                    var sideTexture = generator.modLoc("block/bookshelf/" + name);
                    var model = generator.models()
                        .cubeColumn("block/bookshelf/" + name, sideTexture, topTexture);

                    generator.simpleBlock(block, model);
                }
            )
            .itemModel((block, generator) -> generator.parentBlockItem(block.get(), "bookshelf/" + name))
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Bookshelf",
                "Estante de " + Utils.localizedMaterialName(name, true)
            );
    }

    private static RegisteredBlock regMetalBars(String name, SoundType sound) {
        return PMRegistries.regBlockItem(
            name + "_bars",
            () -> new IronBarsBlock(
                BlockType.METAL.getProperties()
                    .sound(sound)
            )
        )
            .recipe(
                (item, generator) -> {
                    ItemLike ingot = switch(name) {
                        case "rusted_iron" -> Misc.RUSTED_IRON_INGOT;
                        case "netherite" -> Items.NETHERITE_INGOT;
                        case "silver" -> Misc.SILVER_INGOT;
                        default -> Utils.modErrorTyped("The metal bars type " + name + " has no ingot specified!");
                    };

                    return generator.getShapedBuilder(RecipeCategory.MISC, item, 16)
                        .pattern("iii")
                        .pattern("iii")
                        .define('i', ingot);
                }
            )
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .itemModel((block, generator) -> generator.flatBlockItem(name + "_bars", "bars/" + name))
            .blockStateGenerator(
                (block, generator) -> {
                    var barName = "block/bars/" + name;
                    var texture = generator.modLoc(barName);
                    var modelBuilder = generator.models();

                    var post = modelBuilder.withExistingParent(barName + "_post", generator.modLoc("block/template/custom_bars_post"))
                        .texture("texture", texture);

                    var side = modelBuilder.withExistingParent(barName + "_side", generator.modLoc("block/template/custom_bars_side"))
                        .texture("texture", texture);

                    var sideAlt = modelBuilder.withExistingParent(barName + "_side_alt", generator.modLoc("block/template/custom_bars_side_alt"))
                        .texture("texture", texture);

                    generator.getMultipartBuilder(block.get())
                        .part()
                            .modelFile(post)
                            .addModel()
                            .condition(CrossCollisionBlock.NORTH, false)
                            .condition(CrossCollisionBlock.SOUTH, false)
                            .condition(CrossCollisionBlock.EAST, false)
                            .condition(CrossCollisionBlock.WEST, false)
                            .end()
                        .part()
                            .modelFile(side)
                            .addModel()
                            .condition(CrossCollisionBlock.NORTH, true)
                            .end()
                        .part()
                            .modelFile(side)
                            .rotationY(90)
                            .addModel()
                            .condition(CrossCollisionBlock.EAST, true)
                            .end()
                        .part()
                            .modelFile(sideAlt)
                            .addModel()
                            .condition(CrossCollisionBlock.SOUTH, true)
                            .end()
                        .part()
                            .modelFile(sideAlt)
                            .rotationY(90)
                            .addModel()
                            .condition(CrossCollisionBlock.WEST, true)
                            .end();
                }
            )
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Bars",
                "Barras de " + Utils.localizedMaterialName(name, true)
            );
    }

    private static RegisteredBlock autumnLeaves(AutumnTree.Color color, RegisteredBlock sapling) {
        var regBlock = BlockTemplates.leaves(color.name().toLowerCase() + "_autumn", "oak", sapling, false)
            .localizedName(
                color.localizedLeavesColor(false) + " Autumn Leaves", 
                "Hojas " + color.localizedLeavesColor(true) + " de otoño"
            );

        Events.registerBasicColoredBlocks(color.leafColor(), regBlock);
        return regBlock;
    }

    private static void genPaneBlockState(String name, String texturePath, RegisteredBlock block, BlockStateGenerator generator) {
        var texture = generator.modLoc("block/" + texturePath);
        var modelBuilder = generator.models();

        var post = modelBuilder.panePost("block/" + name + "_post", texture, texture)
            .renderType("translucent");

        var side = modelBuilder.paneSide("block/" + name + "_side", texture, texture)
            .renderType("translucent");

        var sideAlt = modelBuilder.paneSideAlt("block/" + name + "_side_alt", texture, texture)
            .renderType("translucent");

        var noSide = modelBuilder.paneNoSide("block/" + name + "_noside", texture)
            .renderType("translucent");

        var noSideAlt = modelBuilder.paneNoSideAlt("block/" + name + "_noside_alt", texture)
            .renderType("translucent");

        generator.paneBlock((IronBarsBlock) block.get(), post, side, sideAlt, noSide, noSideAlt);
    }
}