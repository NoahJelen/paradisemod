package net.paradisemod.decoration.ui;

import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.inventory.CraftingMenu;
import net.paradisemod.decoration.blocks.CustomTable;

public class CustomTableMenu extends CraftingMenu {
    private final CustomTable table;
    private final ContainerLevelAccess accessor;

    public CustomTableMenu(int id, Inventory inventory, CustomTable table, ContainerLevelAccess accessor) {
        super(id, inventory, accessor);
        this.table = table;
        this.accessor = accessor;
    }

    public boolean stillValid(Player player) { return stillValid(accessor, player, table); }
}