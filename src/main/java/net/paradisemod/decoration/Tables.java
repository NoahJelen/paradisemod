package net.paradisemod.decoration;

import java.util.List;

import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.item.CreativeModeTabs;
import net.paradisemod.base.Utils;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.decoration.blocks.CustomTable;
import net.paradisemod.world.DeepDarkBlocks;

public class Tables {
    private static List<String> VANILLA_NAMES = List.of("acacia", "birch", "cherry", "mangrove", "bamboo", "crimson", "warped", "dark_oak", "jungle", "spruce");

    // crafting tables
    public static final RegisteredBlock ACACIA_CRAFTING_TABLE = regTable("acacia", false);
    public static final RegisteredBlock BIRCH_CRAFTING_TABLE = regTable("birch", false);
    public static final RegisteredBlock CACTUS_CRAFTING_TABLE = regTable("cactus", false);
    public static final RegisteredBlock BAMBOO_CRAFTING_TABLE = regTable("bamboo", false);
    public static final RegisteredBlock CRIMSON_CRAFTING_TABLE = regTable("crimson", false);
    public static final RegisteredBlock DARK_OAK_CRAFTING_TABLE = regTable("dark_oak", false);
    public static final RegisteredBlock JUNGLE_CRAFTING_TABLE = regTable("jungle", false);
    public static final RegisteredBlock PALO_VERDE_CRAFTING_TABLE = regTable("palo_verde", false);
    public static final RegisteredBlock MESQUITE_CRAFTING_TABLE = regTable("mesquite", false);
    public static final RegisteredBlock SPRUCE_CRAFTING_TABLE = regTable("spruce", false);
    public static final RegisteredBlock WARPED_CRAFTING_TABLE = regTable("warped", false);
    public static final RegisteredBlock CHERRY_CRAFTING_TABLE = regTable("cherry", false);
    public static final RegisteredBlock MANGROVE_CRAFTING_TABLE = regTable("mangrove", false);

    public static final RegisteredBlock BLACKENED_OAK_CRAFTING_TABLE = regTable("blackened_oak", false)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_SPRUCE_CRAFTING_TABLE = regTable("blackened_spruce", false)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_OAK_CRAFTING_TABLE = regTable("glowing_oak", true)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_CACTUS_CRAFTING_TABLE = regTable("glowing_cactus", true)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static void init() { }

    private static RegisteredBlock regTable(String name, boolean glows) {
        return PMRegistries.regBlockItem(
            name + "_crafting_table",
            () -> new CustomTable(glows)
        )
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item)
                    .pattern("pp")
                    .pattern("pp")
                    .define('p', Utils.getPlanks(name))
            )
            .tab(CreativeModeTabs.FUNCTIONAL_BLOCKS)
            .itemModel((block, generator) -> generator.parentBlockItem(block.get(), "crafting_table/" + name))
            .blockStateGenerator(
                (block, generator) -> {
                    var planksTexture = "paradisemod:block/" + name + "_planks";

                    if(name.contains("cactus"))
                        planksTexture = "paradisemod:block/" + name + "_block";

                    if(VANILLA_NAMES.contains(name))
                        planksTexture = "minecraft:block/" + name + "_planks";

                    var frontTexture = "paradisemod:block/crafting_table/" + name + "_front";
                    var sideTexture = "paradisemod:block/crafting_table/" + name + "_side";
                    var topTexture = "paradisemod:block/crafting_table/" + name + "_top";

                    var model = generator.models()
                        .cube(
                            "block/crafting_table/" + name,
                            new ResourceLocation(planksTexture),
                            new ResourceLocation(topTexture),
                            new ResourceLocation(frontTexture),
                            new ResourceLocation(frontTexture),
                            new ResourceLocation(sideTexture),
                            new ResourceLocation(sideTexture)
                        )
                        .texture("particle", planksTexture);

                    generator.simpleBlock(block.get(), model);
                }
            )
            .tag(BlockTags.MINEABLE_WITH_AXE)
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Crafting Table",
                "Mesa de trabajo " + (name != "crimson" && name != "warped" ? "de " : "") + Utils.localizedMaterialName(name, true)
            );
    }
}