package net.paradisemod.base;

import java.util.ArrayList;
import java.util.HashMap;

import net.minecraft.world.entity.decoration.PaintingVariant;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.registry.PMRegistries;

public class PMPaintings {
    private static final DeferredRegister<PaintingVariant> PAINTINGS = PMRegistries.createRegistry(ForgeRegistries.PAINTING_VARIANTS);
    public static ArrayList<RegistryObject<PaintingVariant>> ALL_PAINTINGS = new ArrayList<>();
    public static HashMap<String, String> PAINTING_NAMES = new HashMap<>();
    public static ArrayList<String> AUTHOR_KEYS = new ArrayList<>();

    public static void init(IEventBus eventBus) {
        regPainting("aercloud", 64, 48, "Aercloud Sytems");
        regPainting("blind_guardian", 128, 128, "Blind Guardian Logo");
        regPainting("breaking_benjamin", 32, 16, "Breaking Benjamin Logo");
        regPainting("chaos_and_nether", 64, 48, "ChaosDog and NetherNoah777");
        regPainting("death_punch", 32, 32, "5 Finger Death Punch Logo");
        regPainting("linkin_park", 32, 32, "Linkin Park Logo");
        regPainting("maiden", 64, 32, "Iron Maiden Logo");
        regPainting("manowar", 64, 64, "Manowar Logo");
        regPainting("metallica1", 32, 16, "Metallica Logo 1");
        regPainting("metallica2", 32, 16, "Metallica Logo 2");
        regPainting("rebels", 32, 16, "Outsider Alliance");
        regPainting("skillet", 32, 16, "Skillet Logo");
        regPainting("hamas_is_evil", 32, 16, "Hamas is Evil!");
        regPainting("aercloud_empire", 32, 32, "Aercloud Empire");
        PAINTINGS.register(eventBus);
        ParadiseMod.LOG.info("Loaded Paintings");
    }

    private static void regPainting(String name, int width, int height, String displayName) {
        ALL_PAINTINGS.add(
            Utils.handlePossibleException(
                () -> PAINTINGS.register(name, () -> new PaintingVariant(width, height))
            )
        );

        var translationKey = "painting.paradisemod." + name + ".title";
        PAINTING_NAMES.put(translationKey, displayName);
        AUTHOR_KEYS.add("painting.paradisemod." + name + ".author");
    }
}