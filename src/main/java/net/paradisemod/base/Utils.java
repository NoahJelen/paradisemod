package net.paradisemod.base;

import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.ArrayListMultimap;

import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.tags.ITag;
import net.paradisemod.ParadiseMod;
import net.paradisemod.building.Building;
import net.paradisemod.world.DeepDarkBlocks;

public class Utils {
    protected static final ArrayListMultimap<ResourceKey<CreativeModeTab>, Item> CREATIVE_TAB_ITEMS = ArrayListMultimap.create();

    // get a block tag
    public static ITag<Block> getBlockTag(TagKey<Block> key) {
        var tags = ForgeRegistries.BLOCKS.tags();
        return tags.getTag(key);
    }

    // Gets the maximum value of an integer array
    public static int getMaxValue(List<Integer> numbers){
        int maxValue = numbers.get(0);
        for(var number : numbers)
            if(number > maxValue && number != 0)
                maxValue = number;

        return maxValue;
    }

    // Gets the minimum value of an integer array
    public static int getMinValue(List<Integer> numbers){
        var minValue = numbers.get(0);
        for(var number : numbers)
            if(number < minValue && number != 0)
                minValue = number;

        return minValue;
    }

    public static void addToCreativeTab(ItemLike item, ResourceKey<CreativeModeTab> tab) {
        CREATIVE_TAB_ITEMS.put(tab, item.asItem());
    }

    public static ItemLike getPlanks(String name) {
        return switch(name) {
            case "mangrove" -> Blocks.MANGROVE_PLANKS;
            case "cherry" -> Blocks.CHERRY_PLANKS;
            case "acacia" -> Blocks.ACACIA_PLANKS;
            case "birch" -> Blocks.BIRCH_PLANKS;
            case "cactus" -> Building.CACTUS_BLOCK;
            case "glowing_cactus" -> DeepDarkBlocks.GLOWING_CACTUS_BLOCK;
            case "bamboo" -> Blocks.BAMBOO_PLANKS;
            case "crimson" -> Blocks.CRIMSON_PLANKS;
            case "dark_oak" -> Blocks.DARK_OAK_PLANKS;
            case "jungle" -> Blocks.JUNGLE_PLANKS;
            case "palo_verde" -> Building.PALO_VERDE_PLANKS;
            case "mesquite" -> Building.MESQUITE_PLANKS;
            case "spruce" -> Blocks.SPRUCE_PLANKS;
            case "warped" -> Blocks.WARPED_PLANKS;
            case "blackened_oak" -> DeepDarkBlocks.BLACKENED_OAK_PLANKS;
            case "blackened_spruce" -> DeepDarkBlocks.BLACKENED_SPRUCE_PLANKS;
            case "glowing_oak" -> DeepDarkBlocks.GLOWING_OAK_PLANKS;
            default -> modErrorTyped("The wood type " + name + " needs a planks definition!");
        };
    }

    public static String localizedMaterialName(String materialName, boolean spanish) {
        if(spanish)
            return switch(materialName) {
                case "smooth_stone" -> "piedra lisa";
                case "mangrove" -> "mangle";
                case "cherry" -> "cerezo";
                case "basalt" -> "basalto";
                case "netherrack" -> "netherrack";
                case "ender_pearl" -> "perla de Fin";
                case "honey" -> "miel";
                case "lapis" -> "lapislázuli";
                case "quartz" -> "cuarzo";
                case "salt" -> "sal";
                case "deepslate" -> "pizarra profunda";
                case "polished_deepslate" -> "pizarra profunda pulida";
                case "cobbled_deepslate" -> "pizarra profunda labrada";
                case "blackstone" -> "piedra negra";
                case "polished_andesite" -> "andesita pulida";
                case "polished_diorite" -> "diorita pulida";
                case "polished_granite" -> "granito pulido";
                case "sandstone" -> "arenisca";
                case "red_sandstone" -> "arenisca roja";
                case "dirt" -> "tierra";
                case "grass" -> "pasto";
                case "acacia" -> "acacia";
                case "birch" -> "abedul";
                case "crimson" -> "carmesí";
                case "dark_oak" -> "roble oscuro";
                case "jungle" -> "jungla";
                case "spruce" -> "abeto";
                case "warped" -> "distorsionado";
                case "enderite" -> "metal del Fin";
                case "bamboo" -> "bambú";
                case "brick" -> "ladrillos";
                case "andesite" -> "andesita";
                case "bedrock" -> "piedra madre";
                case "cobblestone" -> "piedra labrada";
                case "diorite" -> "diorita";
                case "end_stone" -> "piedra del Fin";
                case "granite" -> "granito";
                case "mossy_cobblestone" -> "piedra labrada musgosa";
                case "obsidian" -> "obsidiana";
                case "stone" -> "piedra";
                case "diamond" -> "diamante";
                case "gold" -> "oro";
                case "emerald" -> "esmeralda";
                case "ruby" -> "rubí";
                case "rusted_iron" -> "hierro oxidado";
                case "silver" -> "plata";
                case "redstone" -> "piedra roja";
                case "cactus" -> "cactus";
                case "glowing_cactus" -> "cactus brillante";
                case "palo_verde" -> "palo verde";
                case "mesquite" -> "mezquite";
                case "glass" -> "vidrio";
                case "blackened_oak" -> "roble ennegrecido";
                case "blackened_spruce" -> "abeto ennegrecido";
                case "glowing_oak" -> "roble brillante";
                case "polished_end_stone" -> "piedra pulida del Fin";
                case "dripstone" -> "caliza";
                case "polished_dripstone" -> "caliza pulida";
                case "polished_dripstone_bricks" -> "ladrillos de caliza pulida";
                case "calcite" -> "calcita";
                case "polished_calcite" -> "calcita pulida";
                case "polished_calcite_bricks" -> "ladrillos de calcita pulida";
                case "tuff" -> "toba";
                case "polished_tuff" -> "toba pulida";
                case "polished_tuff_bricks" -> "ladrillos de toba pulida";
                case "polished_asphalt" -> "asfalto pulido";
                case "polished_asphalt_bricks" -> "ladrillos de asfalto pulido";
                case "darkstone" -> "piedra oscura";
                case "polished_darkstone" -> "piedra oscura pulida";
                case "polished_darkstone_bricks" -> "ladrillos de piedra oscura pulida";
                case "netherite" -> "netherita";
                case "blackened_sandstone" -> "arenisca ennegrecida";
                case "smooth_blackened_sandstone" -> "arenisca ennegrecida lisa";
                case "cut_blackened_sandstone" -> "arenisca ennegrecida cortada";
                default -> "";
            };

        return switch(materialName) {
            case "smooth_stone" -> "Smooth Stone";
            case "mangrove" -> "Mangrove";
            case "cherry" -> "Cherry";
            case "basalt" -> "Basalt";
            case "netherrack" -> "Netherrack";
            case "ender_pearl" -> "Ender Pearl";
            case "honey" -> "Honey";
            case "lapis" -> "Lapis Lazuli";
            case "quartz" -> "Quartz";
            case "salt" -> "Salt";
            case "deepslate" -> "Deepslate";
            case "polished_deepslate" -> "Polished Deepslate";
            case "cobbled_deepslate" -> "Cobbled Deepslate";
            case "blackstone" -> "Blackstone";
            case "polished_andesite" -> "Polished Andesite";
            case "polished_diorite" -> "Polished Diorite";
            case "polished_granite" -> "Polished Granite";
            case "sandstone" -> "Sandstone";
            case "red_sandstone" -> "Red Sandstone";
            case "dirt" -> "Dirt";
            case "grass" -> "Grass";
            case "acacia" -> "Acacia";
            case "birch" -> "Birch";
            case "crimson" -> "Crimson";
            case "dark_oak" -> "Dark Oak";
            case "jungle" -> "Jungle";
            case "spruce" -> "Spruce";
            case "warped" -> "Warped";
            case "enderite" -> "Enderite";
            case "bamboo" -> "Bamboo";
            case "brick" -> "Brick";
            case "andesite" -> "Andesite";
            case "bedrock" -> "Bedrock";
            case "cobblestone" -> "Cobblestone";
            case "diorite" -> "Diorite";
            case "end_stone" -> "End Stone";
            case "granite" -> "Granite";
            case "mossy_cobblestone" -> "Mossy Cobblestone";
            case "obsidian" -> "Obsidian";
            case "stone" -> "Stone";
            case "diamond" -> "Diamond";
            case "gold" -> "Golden";
            case "emerald" -> "Emerald";
            case "ruby" -> "Ruby";
            case "rusted_iron" -> "Rusted Iron";
            case "silver" -> "Silver";
            case "redstone" -> "Redstone";
            case "cactus" -> "Cactus";
            case "glowing_cactus" -> "Glowing Cactus";
            case "palo_verde" -> "Palo Verde";
            case "mesquite" -> "Mesquite";
            case "glass" -> "Glass";
            case "blackened_oak" -> "Blackened Oak";
            case "blackened_spruce" -> "Blackened Spruce";
            case "glowing_oak" -> "Glowing Oak";
            case "polished_end_stone" -> "Polished End Stone";
            case "dripstone" -> "Dripstone";
            case "polished_dripstone" -> "Polished Dripstone";
            case "polished_dripstone_bricks" -> "Polished Dripstone Bricks";
            case "calcite" -> "Calcite";
            case "polished_calcite" -> "Polished Calcite";
            case "polished_calcite_bricks" -> "Polished Calcite Bricks";
            case "tuff" -> "Tuff";
            case "polished_tuff" -> "Polished Tuff";
            case "polished_tuff_bricks" -> "Polished Tuff Bricks";
            case "polished_asphalt" -> "Polished Ashphalt";
            case "polished_asphalt_bricks" -> "Polished Asphalt Bricks";
            case "darkstone" -> "Darkstone";
            case "polished_darkstone" -> "Polished Darkstone";
            case "polished_darkstone_bricks" -> "Polished Darkstone Bricks";
            case "netherite" -> "Netherite";
            case "blackened_sandstone" -> "Blackened Sandstone";
            case "smooth_blackened_sandstone" -> "Smooth Blackened Sandstone";
            case "cut_blackened_sandstone" -> "Cut Blackened Sandstone";
            default -> "";
        };
    }

    /* Intentionally throw an exception for the specified reason */
    public static <T> T modErrorTyped(String message) throws RuntimeException {
        throw handleError(new IllegalArgumentException(message));
    }

    /* Intentionally throw an exception for the specified reason */
    public static void modError(String message) throws RuntimeException {
        throw handleError(new IllegalArgumentException(message));
    }

    /* Very similar to try...catch, but handles the exception more gracefully */
    public static <T> T handlePossibleException(Supplier<T> code) {
        try {
            return code.get();
        }
        catch (Exception e) {
            throw handleError(e);
        }
    }

    /* Very similar to try...catch, but handles the exception more gracefully */
    public static void handlePossibleException(FallibleCode code) {
        try {
            code.run();
        }
        catch (Exception e) {
            throw handleError(e);
        }
    }

    // this should hopefully make it easier for me and other Minecraft players
    // to report and fix crashes
    private static RuntimeException handleError(Exception e) {
        ParadiseMod.LOG.fatal("Paradise Mod has encountered a fatal error!");
        ParadiseMod.LOG.fatal("Error: " + e.getMessage());
        ParadiseMod.LOG.fatal("Report it here: https://gitlab.com/NoahJelen/paradisemod/-/issues");
        ParadiseMod.LOG.fatal("Stack Trace:", e);
        return new RuntimeException("Paradise Mod has encountered a fatal error! Please look at your Minecraft log for more info.");
    }

    @FunctionalInterface
    public interface FallibleCode {
        void run();
    }
}