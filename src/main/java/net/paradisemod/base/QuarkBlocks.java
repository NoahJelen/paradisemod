package net.paradisemod.base;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.registries.ObjectHolder;

public class QuarkBlocks {
    // Quark's chests
    @ObjectHolder(registryName = "block", value = "quark:oak_chest")
    public static Block OAK_CHEST = Blocks.CHEST;

    @ObjectHolder(registryName = "block", value = "quark:birch_chest")
    public static Block BIRCH_CHEST = Blocks.CHEST;

    @ObjectHolder(registryName = "block", value = "quark:dark_oak_chest")
    public static Block DARK_OAK_CHEST = Blocks.CHEST;

    @ObjectHolder(registryName = "block", value = "quark:jungle_chest")
    public static Block JUNGLE_CHEST = Blocks.CHEST;

    @ObjectHolder(registryName = "block", value = "quark:spruce_chest")
    public static Block SPRUCE_CHEST = Blocks.CHEST;

    @ObjectHolder(registryName = "block", value = "quark:acacia_chest")
    public static Block ACACIA_CHEST = Blocks.CHEST;

    @ObjectHolder(registryName = "block", value = "quark:cherry_chest")
    public static Block CHERRY_CHEST = Blocks.CHEST;

    @ObjectHolder(registryName = "block", value = "quark:mangrove_chest")
    public static Block MANGROVE_CHEST = Blocks.CHEST;

    // Quark's bookshelves
    @ObjectHolder(registryName = "block", value = "quark:birch_bookshelf")
    public static Block BIRCH_BOOKSHELF = Blocks.BOOKSHELF;

    @ObjectHolder(registryName = "block", value = "quark:dark_oak_bookshelf")
    public static Block DARK_OAK_BOOKSHELF = Blocks.BOOKSHELF;

    @ObjectHolder(registryName = "block", value = "quark:jungle_bookshelf")
    public static Block JUNGLE_BOOKSHELF = Blocks.BOOKSHELF;

    @ObjectHolder(registryName = "block", value = "quark:spruce_bookshelf")
    public static Block SPRUCE_BOOKSHELF = Blocks.BOOKSHELF;

    @ObjectHolder(registryName = "block", value = "quark:acacia_bookshelf")
    public static Block ACACIA_BOOKSHELF = Blocks.BOOKSHELF;

    @ObjectHolder(registryName = "block", value = "quark:cherry_bookshelf")
    public static Block CHERRY_BOOKSHELF = Blocks.BOOKSHELF;

    @ObjectHolder(registryName = "block", value = "quark:mangrove_bookshelf")
    public static Block MANGROVE_BOOKSHELF = Blocks.BOOKSHELF;
}