package net.paradisemod.base;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.HangingSignItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.SignItem;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BarrelBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.state.BlockBehaviour.Properties;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.WoodType;
import net.minecraftforge.common.Tags;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.base.registry.RegisteredItem;
import net.paradisemod.decoration.blocks.CustomWood;
import net.paradisemod.misc.blocks.CustomBarrel;
import net.paradisemod.misc.blocks.CustomHangingSign;
import net.paradisemod.misc.blocks.CustomSign;
import net.paradisemod.misc.blocks.CustomWallHangingSign;
import net.paradisemod.misc.blocks.CustomWallSign;

/** Predefined templates for common block types */
public class BlockTemplates {
    public static RegisteredBlock planks(String woodName, boolean glows) {
        return PMRegistries.regBlockItem(
            woodName + "_planks",
            () -> new Block(BlockType.WOOD.getProperties().lightLevel(s -> glows ? 7 : 0))
        )
            .tag(BlockTags.PLANKS)
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.BUILDING_BLOCKS, item, 4)
                    .pattern("LL")
                    .pattern("LL")
                    .define('L', logTagItem(woodName))
            )
            .localizedName(
                Utils.localizedMaterialName(woodName, false) + " Planks",
                "Tablones de " + Utils.localizedMaterialName(woodName, true)
            );
    }

    public static RegisteredBlock rock(String name) {
        return PMRegistries.regBlockItem(
            name,
            () -> new Block(BlockType.STONE.getProperties())
        )
            .tags(
                BlockTags.MINEABLE_WITH_PICKAXE,
                Tags.Blocks.STONE
            )
            .tab(CreativeModeTabs.BUILDING_BLOCKS);
    }

    public static RegisteredBlock rock(String name, ItemLike craftFromBlock) {
        return rockInternal(name, craftFromBlock, BlockType.STONE.getProperties());
    }

    public static RegisteredBlock rock(String name, Block vanillaRockToCopy) {
        return rockInternal(name, vanillaRockToCopy, Block.Properties.copy(vanillaRockToCopy));
    }

    public static RegisteredBlock cutRock(String name, ItemLike craftFromBlock, ItemLike... blocksToStoneCutFrom) {
        return cutRockInternal(name, craftFromBlock, BlockType.STONE.getProperties(), blocksToStoneCutFrom);
    }

    public static RegisteredBlock cutRock(String name, Block vanillaRockToCopy, ItemLike... blocksToStoneCutFrom) {
        return cutRockInternal(name, vanillaRockToCopy, Block.Properties.copy(vanillaRockToCopy), blocksToStoneCutFrom);
    }

    public static RegisteredBlock improvedRock(String name) {
        return PMRegistries.regBlockItem(
            name,
            () -> new Block(BlockType.ENHANCED_STONE.getProperties())
        )
            .tags(
                BlockTags.MINEABLE_WITH_PICKAXE,
                Tags.Blocks.STONE
            )
            .tab(CreativeModeTabs.BUILDING_BLOCKS);
    }

    public static RegisteredBlock improvedRock(String name, ItemLike craftFromBlock) {
        return rockInternal(name, craftFromBlock, BlockType.ENHANCED_STONE.getProperties());
    }

    public static RegisteredBlock cutImprovedRock(String name, ItemLike craftFromBlock, ItemLike... blocksToStoneCutFrom) {
        return cutRockInternal(name, craftFromBlock, BlockType.ENHANCED_STONE.getProperties());
    }

    private static RegisteredBlock rockInternal(String name, ItemLike craftFromBlock, Block.Properties props) {
        var block = PMRegistries.regBlockItem(
            name,
            () -> new Block(props)
        )
            .tags(
                BlockTags.MINEABLE_WITH_PICKAXE,
                Tags.Blocks.STONE
            )
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .recipe((item, generator) -> generator.stonecutterRecipe(craftFromBlock, item));

        if(!name.contains("chiseled"))
            block = block.recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.BUILDING_BLOCKS, item)
                    .pattern("rr")
                    .pattern("rr")
                    .define('r', craftFromBlock)
            );

        return block;
    }

    public static RegisteredBlock cutRockInternal(String name, ItemLike craftFromBlock, Block.Properties props, ItemLike... blocksToStoneCutFrom) {
        var stonecutterBlocks = new ArrayList<>(List.of(blocksToStoneCutFrom));
        stonecutterBlocks.add(craftFromBlock);
        return PMRegistries.regBlockItem(
            name,
            () -> new Block(BlockType.STONE.getProperties())
        )
            .tags(
                BlockTags.MINEABLE_WITH_PICKAXE,
                BlockTags.FEATURES_CANNOT_REPLACE
            )
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.BUILDING_BLOCKS, item)
                    .pattern("rr")
                    .pattern("rr")
                    .define('r', craftFromBlock)
            )
            .stonecutterRecipes(stonecutterBlocks.toArray(ItemLike[]::new));
    }

    public static RegisteredBlock log(String name, boolean isWood, Supplier<Block> strippedLog, boolean glows) {
        var logName = name + (isWood ? "_wood" : "_log");

        var log = PMRegistries.regBlockItem(
            logName,
            () -> new CustomWood(strippedLog.get(), glows)
        )
            .blockStateGenerator((block, generator) -> logBlockState(name, isWood, false, block, generator))
            .tag(logTag(name))
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .localizedName(
                Utils.localizedMaterialName(name, false) + " " + (isWood ? "Wood" : "Log"),
                (isWood ? "Leño" : "Tronco") + " de " + Utils.localizedMaterialName(name, true)
            );
            
        if(!isWood)
            log = log.tab(CreativeModeTabs.NATURAL_BLOCKS);

        return log;
    }

    public static RegisteredBlock strippedLog(String name, boolean isWood, boolean glows) {
        var logName = "stripped_" + name + (isWood ? "_wood" : "_log");

        return PMRegistries.regBlockItem(
            logName,
            () -> new RotatedPillarBlock(Properties.copy(Blocks.OAK_LOG).lightLevel(s -> glows ? 7 : 0))
        )
            .tag(logTag(name))
            .blockStateGenerator((block, generator) -> logBlockState(name, isWood, true, block, generator))
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .localizedName(
                "Stripped " + Utils.localizedMaterialName(name, false) + " " + (isWood ? "Wood" : "Log"),
                (isWood ? "Leño" : "Tronco") + " de " + Utils.localizedMaterialName(name, true) + " sin corteza"
            );
    }

    public static RegisteredBlock leaves(String name, String vanillaLeavesName, RegisteredBlock sapling, boolean glows) {
        return PMRegistries.regBlockItem(
            name + "_leaves",
            () -> new LeavesBlock(Block.Properties.copy(Blocks.OAK_LEAVES).lightLevel(s -> glows ? 7 : 0)) {
                @Override
                public void animateTick(BlockState state, Level world, BlockPos pos, RandomSource rand) {
                    if(name == "glowing_oak")
                        world.sendBlockUpdated(pos, state, state, 8);
                };
            }
        )
            .tag(BlockTags.LEAVES)
            .renderType("cutout")
            .lootTable((block, generator) -> generator.leavesDrop(block, sapling.get()))
            .tab(CreativeModeTabs.NATURAL_BLOCKS)
            .blockStateGenerator((block, generator) -> generator.vanillaTintedLeaves(block, vanillaLeavesName))
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Leaves",
                "Hojas de " + Utils.localizedMaterialName(name, true)
            );
    }

    public static RegisteredBlock barrel(String name, boolean glows, ItemLike planks, ItemLike woodSlab) {
        return PMRegistries.regBlockItem(
            name + "_barrel",
            () -> new CustomBarrel(glows ? 7 : 0)
        )
            .itemModel((block, generator) -> generator.parentBlockItem(block, "barrel/" + name))
            .blockStateGenerator(
                (block, generator) -> {
                    var modelBuilder = generator.models();
                    var topTexture = "block/barrel/" + name + "_top";
                    var topOpenTexture = "block/barrel/" + name + "_top_open";
                    var bottomTexture = "block/barrel/" + name + "_bottom";
                    var sideTexture = "block/barrel/" + name + "_side";
                    var barrel = modelBuilder.getBuilder("block/barrel/" + name)
                        .parent(generator.existingMCModel("cube_bottom_top"))
                        .texture("top", topTexture)
                        .texture("bottom", bottomTexture)
                        .texture("side", sideTexture);

                    var open = modelBuilder.getBuilder("block/barrel/" + name + "_open")
                        .parent(generator.existingMCModel("cube_bottom_top"))
                        .texture("top", topOpenTexture)
                        .texture("bottom", bottomTexture)
                        .texture("side", sideTexture);

                    generator.getVariantBuilder(block.get())
                        .forAllStates(
                            state -> {
                                Direction facing = state.getValue(BarrelBlock.FACING);
                                boolean isOpen = state.getValue(BarrelBlock.OPEN);

                                if(isOpen)
                                    return generator.buildVariantModel(open, facing);
                                else
                                    return generator.buildVariantModel(barrel, facing);
                            }
                        );
                }
            )
            .tags(
                BlockTags.MINEABLE_WITH_AXE,
                Tags.Blocks.BARRELS_WOODEN
            )
            .tab(CreativeModeTabs.FUNCTIONAL_BLOCKS)
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item)
                    .pattern("PSP")
                    .pattern("P P")
                    .pattern("PSP")
                    .define('P', planks)
                    .define('S', woodSlab)
            )
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Barrel",
                (name == "crimson" || name == "warped" ? "Barril " : "Barril de ") + Utils.localizedMaterialName(name, true)
            );
    }

    public static RegisteredBlock sign(String name, WoodType woodType, boolean isWallSign, boolean glows) {
        var lightLevel = glows ? 7 : 0;
        Supplier<Block> sign = isWallSign ? () -> new CustomWallSign(woodType, lightLevel) : () -> new CustomSign(woodType, lightLevel);
        var signName = name + (isWallSign ? "_wall"  : "") + "_sign";
        var signTag = isWallSign ? BlockTags.WALL_SIGNS : BlockTags.STANDING_SIGNS;
        var particleTex = name.contains("cactus") ? "block/" + name + "_block" : "block/" + name + "_planks";

        return PMRegistries.regBlock(signName, sign)
            .tag(signTag)
            .blockStateGenerator(
                (block, generator) -> {
                    var model = generator.models()
                        .getBuilder("block/sign/" + name)
                        .texture("particle", particleTex);

                    generator.simpleBlock(block, model);
                }
            );
    }

    public static RegisteredItem signItem(String name, Supplier<Block> sign, Supplier<Block> wallSign, ItemLike planks) {
        return PMRegistries.regItem(
            name + "_sign",
            () -> new SignItem(new Item.Properties(), sign.get(), wallSign.get())
        )
            .model((item, generator) -> generator.basicItem(item.get(), "sign/" + name))
            .tab(CreativeModeTabs.FUNCTIONAL_BLOCKS)
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item, 3)
                    .pattern("PPP")
                    .pattern("PPP")
                    .pattern(" S ")
                    .define('P', planks)
                    .define('S', Items.STICK)

            )
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Sign",
                "Letrero de " + Utils.localizedMaterialName(name, true)
            );
    }

    public static RegisteredBlock hangingSign(String name, WoodType woodType, boolean isWallSign, boolean glows) {
        var lightLevel = glows ? 7 : 0;
        Supplier<Block> sign = isWallSign ? () -> new CustomWallHangingSign(woodType, lightLevel) : () -> new CustomHangingSign(woodType, lightLevel);
        var signName = name + (isWallSign ? "_wall"  : "") + "_hanging_sign";
        var signTag = isWallSign ? BlockTags.WALL_HANGING_SIGNS : BlockTags.CEILING_HANGING_SIGNS;
        var particleTex = name.contains("cactus") ? "block/" + name + "_block" : "block/" + name + "_planks";

        return PMRegistries.regBlock(signName, sign)
            .tag(signTag)
            .blockStateGenerator(
                (block, generator) -> {
                    var model = generator.models()
                        .getBuilder("block/hanging_sign/" + name)
                        .texture("particle", particleTex);

                    generator.simpleBlock(block, model);
                }
            );
    }

    public static RegisteredItem hangingSignItem(String name, Supplier<Block> sign, Supplier<Block> wallSign, ItemLike planks) {
        return PMRegistries.regItem(
            name + "_hanging_sign",
            () -> new HangingSignItem(sign.get(), wallSign.get(), new Item.Properties())
        )
            .model((item, generator) -> generator.basicItem(item.get(), "hanging_sign/" + name))
            .tab(CreativeModeTabs.FUNCTIONAL_BLOCKS)
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item, 6)
                    .pattern("C C")
                    .pattern("PPP")
                    .pattern("PPP")
                    .define('P', planks)
                    .define('C', Blocks.CHAIN)

            )
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Hanging Sign",
                "Letrero colgante de " + Utils.localizedMaterialName(name, true)
            );
    }

    public static TagKey<Block> logTag(String logName) {
        return PMTags.createModTag(logName + "_logs", Registries.BLOCK);
    }

    public static TagKey<Item> logTagItem(String logName) {
        return PMTags.createModTag(logName + "_logs", Registries.ITEM);
    }

    private static void logBlockState(String name, boolean isWood, boolean stripped, RegisteredBlock block, BlockStateGenerator generator) {
        var logName = (stripped ? "stripped_" : "") + name;
        var texture = generator.modLoc("block/" + logName + "_log");
        var topTexture = generator.modLoc("block/" + logName + "_log_top");
        var modelBuilder = generator.models();

        if(isWood) {
            var model = modelBuilder.cubeColumn(logName + "_wood", texture, texture);
            generator.axisBlock((RotatedPillarBlock) block.get(), model, model);
        }
        else {
            var vertical = modelBuilder.cubeColumn(logName + "_log", texture, topTexture);
            var horizontal = modelBuilder.cubeColumnHorizontal(name + "_log_horizontal", texture, topTexture);
            generator.axisBlock((RotatedPillarBlock) block.get(), vertical, horizontal);
        }
    }
}