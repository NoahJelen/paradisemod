package net.paradisemod.base;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.lang3.tuple.Pair;

import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import com.electronwill.nightconfig.core.io.WritingMode;

import net.minecraft.util.RandomSource;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.config.ModConfig;
import net.paradisemod.ParadiseMod;

public class PMConfig {
    public static final PMConfig SETTINGS;
    private static final Path PATH = Paths.get("config", ParadiseMod.ID + "-common.toml");
    private static final ForgeConfigSpec SPEC;

    static {
        Pair<PMConfig, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(PMConfig::new);
        SETTINGS = specPair.getLeft();
        SPEC = specPair.getRight();

        CommentedFileConfig configFile = CommentedFileConfig.builder(PATH)
            .sync()
            .autoreload()
            .writingMode(WritingMode.REPLACE)
            .build();

        configFile.load();
        configFile.save();
        SPEC.setConfig(configFile);
    }

    public final WorldGenOption caveCrystals;
    public final Structures structures;
    public final Ores ores;
    public final BetterEnd betterEnd;
    public final Redstone redstone;

    PMConfig(ForgeConfigSpec.Builder builder) {
        builder.comment("Paradise Mod Settings\nMinecraft might need to be restarted if anything is changed").push("config");
        caveCrystals = new WorldGenOption(builder, true, 150, "Cave crystal generation settings", "crystals");
        structures = new Structures(builder);
        ores = new Ores(builder);
        betterEnd = new BetterEnd(builder);
        redstone = new Redstone(builder);
    }

    public static class Structures {
        public final WorldGenOption blackCross;
        public final WorldGenOption goldCross;
        public final WorldGenOption buoys;
        public final WorldGenOption landmines;
        public final WorldGenOption runways;
        public final WorldGenOption eastereggs;
        public final WorldGenOption blackAndGold;
        public final WorldGenOption ruins;
        public final WorldGenOption smallStronghold;
        public final WorldGenOption creatorMonuments;
        public final WorldGenOption homes;
        public final WorldGenOption bases;
        public final WorldGenOption smallDarkDungeon;
        public final ForgeConfigSpec.BooleanValue traderTent;
        public final ForgeConfigSpec.BooleanValue mediumDarkDungeon;
        public final ForgeConfigSpec.BooleanValue darkTower;
        public final ForgeConfigSpec.BooleanValue brickPyramids;
        public final ForgeConfigSpec.BooleanValue terrariums;
        public final ForgeConfigSpec.BooleanValue minerBases;
        public final ForgeConfigSpec.BooleanValue largeDarkDungeon;
        public final ForgeConfigSpec.BooleanValue badlandsPyramids;
        public final ForgeConfigSpec.BooleanValue darkDesertPyramids;
        public final ForgeConfigSpec.BooleanValue skyWheels;
        public final ForgeConfigSpec.BooleanValue enderOutposts;
        public final ForgeConfigSpec.BooleanValue wickerMan;
        public final ForgeConfigSpec.BooleanValue monastery;
        public final ForgeConfigSpec.BooleanValue conduit;
        public final ForgeConfigSpec.BooleanValue debugMode;

        Structures(ForgeConfigSpec.Builder builder) {
            builder.comment("Structure Settings").push("structures");
            builder.comment("Small structure settings");
            blackCross = new WorldGenOption(builder, false, 7777, "Generate the Black Cross Structure?", "blackcross");
            goldCross = new WorldGenOption(builder, false, 7777, "Generate the Gold Cross Structure?", "goldcross");
            buoys = new WorldGenOption(builder, true, 200, "Generate Buoys in the ocean?", "buoys");
            landmines = new WorldGenOption(builder, true, 20, "Generate Landmines?", "landMines");
            traderTent = builder.comment("Generate Wandering Trader Tents?").define("traderTent", true);
            runways = new WorldGenOption(builder, true, 1000, "Generate Elytra Runways?", "runways");
            eastereggs = new WorldGenOption(builder, true, 1000, "Generate bonus structures?", "eastereggs");
            blackAndGold = new WorldGenOption(builder, false, 750, "Generate black and gold bonus structures?", "blackAndGold");
            wickerMan = builder.comment("Generate Wicker Man structure?").define("wickerMan", true);
            ruins = new WorldGenOption(builder, true, 125, "Generate the ruins of old villages?", "ruins");
            smallStronghold = new WorldGenOption(builder, true, 1000, "Generate small strongholds?", "smallstronghold");
            creatorMonuments = new WorldGenOption(builder, true, 250, "Generate monuments of the mod's creators?", "creatormonuments");
            homes = new WorldGenOption(builder, true, 100, "Generate Starter Homes?", "homes");
            bases = new WorldGenOption(builder, true, 100, "Generate Research Bases?", "bases");
            smallDarkDungeon = new WorldGenOption(builder, true, 4, "Generate Small Dark Dungeons?", "smalldarkdungeon");
            builder.comment("Big structure settings");
            mediumDarkDungeon = builder.comment("Generate Medium Dark Dungeons?").define("mediumDarkDungeon", true);
            darkTower = builder.comment("Generate Dark Towers?").define("darktower", true);
            brickPyramids = builder.comment("Generate Brick Pyramids?").define("brickPyramids", true);
            terrariums = builder.comment("Generate Terraria?").define("terrariums", true);
            minerBases = builder.comment("Generate abandoned miner bases?").define("minerBases", true);
            largeDarkDungeon = builder.comment("Generate Large Dark Dungeons?").define("largeDarkDungeon", true);
            badlandsPyramids = builder.comment("Generate Badlands Pyramids?").define("badlandsPyramid", true);
            darkDesertPyramids = builder.comment("Generate Dark Desert Pyramids?").define("darkDesertPyramid", true);
            skyWheels = builder.comment("Generate wheels in the sky?").define("skyWheels", true);
            enderOutposts = builder.comment("Generate ender outposts?").define("enderOutposts", true);
            monastery = builder.comment("Generate monasteries?").define("monastery", true);
            conduit = builder.comment("Generate conduit structures in the ocean?").define("conduit", true);
            debugMode = builder.comment("Enable debug mode for structure generation? Recommended for use only by the developers of this mod.").define("debugMode", false);
            builder.pop();
        }
    }

    public static class Ores {
        public final ForgeConfigSpec.BooleanValue overworld;
        public final ForgeConfigSpec.BooleanValue netherSilver;
        public final ForgeConfigSpec.BooleanValue end;
        public final ForgeConfigSpec.BooleanValue deepDark;

        Ores(ForgeConfigSpec.Builder builder) {
            builder.comment("Ore settings").push("ores");
            overworld = builder.comment("Generate overworld ores").define("overworld", true);
            netherSilver = builder.comment("Generate nether silver ore").define("netherSilver", true);
            end = builder.comment("Generate end ores").define("end", true);
            deepDark = builder.comment("Generate deep dark ores").define("deepDark", true);
            builder.pop();
        }
    }

    public static class BetterEnd {
        public final WorldGenOption endCrystals;
        public final WorldGenOption rogueSpike;
        public final ForgeConfigSpec.BooleanValue enderOutpost;
        public final ForgeConfigSpec.BooleanValue endFoliage;
        public final ForgeConfigSpec.BooleanValue enderAcid;
        public final ForgeConfigSpec.BooleanValue enderCaves;

        BetterEnd(ForgeConfigSpec.Builder builder) {
            builder.comment("Improvements to the End dimension").push("betterEnd");
            endCrystals = new WorldGenOption(builder, true, 13, "Crystal generation settings", "endCrystals");
            enderOutpost = builder.comment("Generate ender outposts?").define("enderOutpost", true);
            rogueSpike = new WorldGenOption(builder, true, 250, "Rogue Obsidian Spike settings", "rogueSpike");
            endFoliage = builder.comment("Generate Ender Foliage?").define("endFoliage", true);
            enderAcid = builder.comment("Generate Ender Acid lakes and springs?").define("enderAcid", true);
            enderCaves = builder.comment("Generate Ender Caves?").define("enderCaves", true);
            builder.pop();
        }
    }

    public static class Redstone {
        public final ForgeConfigSpec.IntValue superPistonPushLimit;
        public final ForgeConfigSpec.IntValue transmitterRange;

        Redstone(ForgeConfigSpec.Builder builder) {
            builder.comment("Redstone improvements").push("redstone");
            superPistonPushLimit = builder.comment("How many blocks should the super piston be allowed to push?").defineInRange("superPistonPushLimit", 36, 12, Integer.MAX_VALUE);
            transmitterRange = builder.comment("Range of Transmitter").defineInRange("transmitterRange", 16, 8, Integer.MAX_VALUE);
            builder.pop();
        }
    }

    public static class WorldGenOption {
        private final ForgeConfigSpec.BooleanValue enabled;
        private final ForgeConfigSpec.IntValue chance;

        WorldGenOption(ForgeConfigSpec.Builder builder, boolean enabled, int chance, String comment, String name) {
            builder.comment(comment).push(name);
            this.enabled = builder.define("enabled", enabled);
            this.chance =  builder.defineInRange("chance", chance, 0, Integer.MAX_VALUE);
            builder.pop();
        }

        public boolean shouldGenerate(RandomSource rand) {
            return enabled.get() && rand.nextInt(chance.get()) == 0;
        }
    }

    public static void init(ModLoadingContext context) {
        context.registerConfig(ModConfig.Type.COMMON, SPEC);
        ParadiseMod.LOG.info("Loaded Config");
    }
}