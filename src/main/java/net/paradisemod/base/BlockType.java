package net.paradisemod.base;

import java.util.Arrays;
import java.util.List;

import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SoundType;

/** Preset properties for different types of blocks */
public enum BlockType {
    GLASS(SoundType.GLASS, 0.3f, 0.3f),
    SOIL(SoundType.GRAVEL, 0.5f, 0.5f, BlockTags.MINEABLE_WITH_SHOVEL),
    WOOD(SoundType.WOOD, 2.5f, 2.5f, BlockTags.MINEABLE_WITH_AXE),
    WEAK_METAL(SoundType.METAL, 6, 6, BlockTags.MINEABLE_WITH_PICKAXE, BlockTags.NEEDS_STONE_TOOL),
    STONE(SoundType.STONE, 1.5f, 6, BlockTags.MINEABLE_WITH_PICKAXE),
    METAL(SoundType.METAL, 6, 6, BlockTags.MINEABLE_WITH_PICKAXE, BlockTags.NEEDS_IRON_TOOL),
    NETHERITE_LIKE(SoundType.NETHERITE_BLOCK, 50, 1200, BlockTags.MINEABLE_WITH_PICKAXE, BlockTags.NEEDS_DIAMOND_TOOL),
    ENHANCED_STONE(SoundType.STONE, 3, 9, BlockTags.MINEABLE_WITH_PICKAXE),
    STRONG_STONE(SoundType.STONE, 50, 1200, BlockTags.MINEABLE_WITH_PICKAXE, BlockTags.NEEDS_DIAMOND_TOOL),
    INDESTRUCTIBLE(SoundType.STONE, -1, 6000000);

    private final SoundType sound;
    private final float hardness;
    private final float resistance;
    private final List<TagKey<Block>> tags;

    @SafeVarargs
    BlockType(SoundType sound, float hardness, float resistance, TagKey<Block>... tags) {
        this.sound = sound;
        this.hardness = hardness;
        this.resistance = resistance;
        this.tags = Arrays.asList(tags);
    }

    /** Get properties of current block type */
    public Block.Properties getProperties() {
        var properties = Block.Properties.copy(getBlock())
            .sound(sound)
            .strength(hardness, resistance);

        return switch(this) {
            case STRONG_STONE, WEAK_METAL, METAL, STONE, ENHANCED_STONE -> properties.requiresCorrectToolForDrops();
            default -> properties;
        };
    }

    /** Get a vanilla block that corresponds to current block type */
    public Block getBlock() {
        return switch (this) {
            default -> Blocks.DIRT;
            case WOOD -> Blocks.OAK_PLANKS;
            case GLASS -> Blocks.GLASS;
            case METAL -> Blocks.GOLD_BLOCK;
            case STONE -> Blocks.STONE;
            case WEAK_METAL -> Blocks.IRON_BLOCK;
            case STRONG_STONE -> Blocks.OBSIDIAN;
            case ENHANCED_STONE -> Blocks.END_STONE;
            case INDESTRUCTIBLE -> Blocks.BEDROCK;
        };
    }

    public List<TagKey<Block>> tags() {
        return tags;
    }

    public boolean isStone() {
        return switch(this) {
            case STONE, ENHANCED_STONE, STRONG_STONE, INDESTRUCTIBLE -> true;
            default -> false;
        };
    }

    public boolean isMetal() {
        return switch(this) {
            case METAL, WEAK_METAL, NETHERITE_LIKE -> true;
            default -> false;
        };
    }
}