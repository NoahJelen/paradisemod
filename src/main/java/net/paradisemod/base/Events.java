package net.paradisemod.base;

import java.util.ArrayList;

import com.google.common.collect.ArrayListMultimap;

import net.minecraft.ChatFormatting;
import net.minecraft.client.renderer.BiomeColors;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.BannerItem;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.GrassColor;
import net.minecraft.world.level.block.RedStoneWireBlock;
import net.minecraft.world.level.block.entity.BannerPattern;
import net.minecraftforge.client.event.RegisterColorHandlersEvent;
import net.minecraftforge.client.extensions.common.IClientFluidTypeExtensions;
import net.minecraftforge.event.BuildCreativeModeTabContentsEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.furnace.FurnaceFuelBurnTimeEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.registries.ForgeRegistries;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.base.registry.RegisteredItem;
import net.paradisemod.building.Building;
import net.paradisemod.misc.Misc;
import net.paradisemod.redstone.Redstone;
import net.paradisemod.redstone.blocks.RedstoneWire;
import net.paradisemod.world.blocks.CustomCauldron;
import net.paradisemod.world.fluid.PMFluids;

public class Events {
    public static RainbowColor RAINBOW_COLOR = new RainbowColor();
    private static ArrayList<RegisteredBlock> RAINBOW_BLOCKS = new ArrayList<>();
    private static ArrayList<RegisteredBlock> GRASS_COLORED_BLOCKS = new ArrayList<>();
    private static ArrayListMultimap<Integer, RegisteredBlock> COLORED_BLOCKS = ArrayListMultimap.create();

    public static void registerBasicColoredBlocks(int color, RegisteredBlock... blocks) {
        for(var block : blocks)
            COLORED_BLOCKS.put(color, block);
    }

    public static void registerGrassColoredBlocks(RegisteredBlock... blocks) {
        for(var block : blocks)
            GRASS_COLORED_BLOCKS.add(block);
    }

    public static void registerRainbowBlocks(RegisteredBlock... blocks) {
        for(var block : blocks)
            RAINBOW_BLOCKS.add(block);
    }

    public static class Common {
        // make some of my items furnace fuels
        @SubscribeEvent
        public static void addFurnaceFuels(FurnaceFuelBurnTimeEvent event) {
            var item = event.getItemStack().getItem();
            if(item == Misc.ASPHALT_SHARD.get()) event.setBurnTime(3200);
            else if(item == Building.ASPHALT.get().asItem()) event.setBurnTime(32000);
            else if(item == PMFluids.TAR_BUCKET.get()) event.setBurnTime(3200);
            else if(item == PMFluids.DARK_LAVA_BUCKET.get()) event.setBurnTime(20000);
            else if(item == PMFluids.MOLTEN_SALT_BUCKET.get()) event.setBurnTime(20000);
            else if(item == PMFluids.PSYCHEDELIC_LAVA_BUCKET.get()) event.setBurnTime(20000);
            else if(item == PMFluids.LIQUID_REDSTONE_BUCKET.get()) event.setBurnTime(20000);
        }

        // unlocks all recipes if Quark is not present
        @SubscribeEvent
        public static void unlockAllRecipes(PlayerEvent.PlayerLoggedInEvent event) {
            // let Quark take care of the recipe unlocking if it is installed
            if(!ModList.get().isLoaded("quark")) {
                var perro = event.getEntity(); // get player to unlock recipes for
                var server = perro.getServer();
                var recipes = server.getRecipeManager().getRecipes(); // get the list of recipes
                perro.awardRecipes(recipes); // unlock them
                ParadiseMod.LOG.info("Unlocked " + recipes.size() + " recipes for " + perro.getDisplayName().getString());
            }
        }

        public static void populateCreativeTabs(BuildCreativeModeTabContentsEvent event) {
            ParadiseMod.LOG.debug("Populating creative tabs...");
            var curTab = event.getTabKey();

            for(var block : RegisteredBlock.ALL_BLOCKS)
                if(block.hasTab(curTab))
                    event.accept(block.asItem());

            for(var item : RegisteredItem.ALL_ITEMS)
                if(item.hasTab(curTab))
                    event.accept(item.asItem());

            for(var entry : Utils.CREATIVE_TAB_ITEMS.entries()) {
                var tab = entry.getKey();
                var item = entry.getValue();

                if(tab == curTab)
                    event.accept(item);
            }
        }
    }

    public static class Client {
        // new way of displaying banner patterns since banners can basically have infinite patterns now
        @SubscribeEvent
        public static void bannerInfo(ItemTooltipEvent event) {
            ItemStack stack = event.getItemStack();
            if(!(stack.getItem() instanceof BannerItem)) return;
            var flags = event.getFlags();
            var name = stack.getHoverName();
            var tooltip = event.getToolTip();
            tooltip.clear();
            tooltip.add(name);
            var compoundtag = stack.getTagElement("BlockEntityTag");

            if (compoundtag != null && compoundtag.contains("Patterns")) {
                var patterns = compoundtag.getList("Patterns", 10);

                for(int i = 0; i < patterns.size(); i++) {
                    var patternTag = patterns.getCompound(i);
                    var dyecolor = DyeColor.byId(patternTag.getInt("Color"));
                    var pattern = BannerPattern.byHash(patternTag.getString("Pattern"));

                    if(pattern != null)
                        pattern.unwrapKey()
                            .map(pat -> pat.location().toShortLanguageKey())
                            .ifPresent(
                                pat -> {
                                    var fileLoc = new ResourceLocation(pat);
                                    tooltip.add(Component.translatable("block." + fileLoc.getNamespace() + ".banner." + fileLoc.getPath() + "." + dyecolor.getName()).withStyle(ChatFormatting.GRAY));
                                }
                            );
                }
            }

            if(flags.isAdvanced())
                tooltip.add(Component.literal(ForgeRegistries.ITEMS.getKey(stack.getItem()).toString()).withStyle(ChatFormatting.DARK_GRAY));
        }

        public static void colorItems(RegisterColorHandlersEvent.Item event) {
            for(var block : RAINBOW_BLOCKS)
                event.register((stack, index) -> RAINBOW_COLOR.color(), block.asItem());

            for(var block : GRASS_COLORED_BLOCKS)
                event.register(
                    (stack, index) -> GrassColor.get(0.5D, 1),
                    block.asItem()
                );

            for(var entry : COLORED_BLOCKS.entries()) {
                var color = entry.getKey();
                var block = entry.getValue();

                event.register(
                    (stack, index) -> color,
                    block.get()
                );
            }

            for(var entry : PMFluids.COLORED_BUCKETS.entrySet()) {
                var fluid = entry.getKey().get();
                var bucket = entry.getValue().get();
                var bucketKey = ForgeRegistries.ITEMS.getResourceKey(bucket).get();
                event.register(
                    (stack, index) -> {
                        if(bucketKey == PMFluids.PSYCHEDELIC_LAVA_BUCKET.getKey() || bucketKey == PMFluids.PSYCHEDELIC_FLUID_BUCKET.getKey())
                            return index == 1 ? Events.RAINBOW_COLOR.color() : 0xFFFFFFFF;
                        else {
                            var color = IClientFluidTypeExtensions.of(fluid).getTintColor();
                            return index == 1 ? color : 0xFFFFFFFF;
                        }
                    },
                    bucket
                );
            }
        }

        public static void colorBlocks(RegisterColorHandlersEvent.Block event) {
            for(var block : RAINBOW_BLOCKS)
                event.register((state, reader, pos, color) -> RAINBOW_COLOR.color(), block.get());

            for(var block : GRASS_COLORED_BLOCKS)
                event.register(
                    (state, reader, pos, color) -> reader != null && pos != null ? BiomeColors.getAverageGrassColor(reader, pos) : GrassColor.get(0.5D, 1),
                    block.get()
                );

            for(var entry : COLORED_BLOCKS.entries()) {
                var blockColor = entry.getKey();
                var block = entry.getValue();

                event.register(
                    (state, reader, pos, color) -> blockColor,
                    block.get()
                );
            }

            for(var block : PMFluids.COLORED_CAULDRONS) {
                var cauldron = (CustomCauldron) block.get();

                event.register(
                    (state, reader, pos, color) -> cauldron.fluidTintColor(),
                    cauldron
                );
            }

            event.register(
                (state, reader, pos, color) -> RedStoneWireBlock.getColorForPower(state.getValue(RedstoneWire.POWER)),
                Redstone.REDSTONE_WIRE.get() 
            );
        }
    }
}