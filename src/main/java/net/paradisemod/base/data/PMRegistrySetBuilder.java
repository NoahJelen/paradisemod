package net.paradisemod.base.data;

import java.util.HashMap;

import net.minecraft.core.Registry;
import net.minecraft.core.RegistrySetBuilder;
import net.minecraft.resources.ResourceKey;
import net.paradisemod.ParadiseMod;
import net.paradisemod.world.PMWorld.WorldgenFactory;

public class PMRegistrySetBuilder extends RegistrySetBuilder {
    public <T> PMRegistrySetBuilder add(ResourceKey<? extends Registry<T>> key, HashMap<ResourceKey<T>, ? extends WorldgenFactory<T>> registry) {
        return add(
            key,
            context -> {
                var keyPath = key.location().getPath();
                var logMessage = "yes";

                if(keyPath == "worldgen/biome")
                    logMessage = "Generating biomes...";
                else if(keyPath == "worldgen/configured_carver")
                    logMessage = "Generating carvers...";
                else if(keyPath == "worldgen/configured_feature")
                    logMessage = "Generating configured features...";
                else if(keyPath == "worldgen/placed_feature")
                    logMessage = "Generating placed features...";
                else if(keyPath == "worldgen/structure")
                    logMessage = "Generating structures...";

                ParadiseMod.LOG.debug(logMessage);

                for(var entry : registry.entrySet())
                    context.register(entry.getKey(), entry.getValue().generate(context));
            }
        );
    }

    public <T> PMRegistrySetBuilder add(ResourceKey<? extends Registry<T>> key, RegistryBootstrap<T> bootstrap) {
        return (PMRegistrySetBuilder) super.add(key, bootstrap);
    }
}