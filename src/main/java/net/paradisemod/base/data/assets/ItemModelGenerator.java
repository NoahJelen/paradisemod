package net.paradisemod.base.data.assets;

import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.client.model.generators.ModelFile.ExistingModelFile;
import net.minecraftforge.client.model.generators.ModelFile.UncheckedModelFile;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.Utils;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.base.registry.RegisteredItem;

public class ItemModelGenerator extends ItemModelProvider {
    public ItemModelGenerator(PackOutput output, ExistingFileHelper existingFileHelper) {
        super(output, ParadiseMod.ID, existingFileHelper);
    }

    @Override
    protected void registerModels() {
        var blockItemCount = 0;
        for(var block : RegisteredBlock.ALL_BLOCKS)
            if(block.hasItem()) {
                try {
                    block.genItemModel(this);
                }
                catch (Exception e) {
                    ParadiseMod.LOG.error("Error generating block item model for " + block.shortName() + ": " + e.getMessage());
                    ParadiseMod.LOG.error("Stack trace:", e);
                }

                blockItemCount++;
            }

        ParadiseMod.LOG.debug("Finished generating block item models for " + blockItemCount + " blocks");

        for(var item : RegisteredItem.ALL_ITEMS) {
            try {
                item.genModel(this);
            }
            catch (Exception e) {
                ParadiseMod.LOG.error("Error generating block item model for " + item.shortName() + ": " + e.getMessage());
                ParadiseMod.LOG.error("Stack trace:", e);
            }
        }

        ParadiseMod.LOG.debug("Finished generating item models for " + RegisteredItem.ALL_ITEMS.size() + " items");
    }

    /** Generate an item model that is a flat version of the block texture
    * (useful for rail item models)
    */
    public void flatBlockItem(RegisteredBlock block) {
        flatBlockItem(block.shortName());
    }

    /** Generate an item model that is a flat version of the block texture
     * (useful for rail item models)
     */
    public void flatBlockItem(String modelName) {
        flatBlockItem(modelName, modelName);
    }

    public void flatBlockItem(String modelName, String texturePath) {
        getBuilder("item/" +  modelName)
            .parent(new UncheckedModelFile("minecraft:item/generated"))
            .texture("layer0", modLoc("block/" + texturePath));
    }

    public void glassPaneItem(String modelName, String texturePath) {
        getBuilder("item/" +  modelName)
            .parent(new UncheckedModelFile("minecraft:item/generated"))
            .texture("layer0", modLoc("block/" + texturePath))
            .renderType("translucent");
    }

    /** Generate an item model that is a child of a block model */
    public void parentBlockItem(RegisteredBlock block) {
        parentBlockItem(block.get());
    }

    /** Generate an item model that is a child of a block model */
    public void parentBlockItem(Block block) {
        var location = ForgeRegistries.BLOCKS.getKey(block);
        parentBlockItem(block, location.getPath());
    }

    /** Generate an item model that is a child of a block model */
    public void parentBlockItem(Block block, String modelName) {
        var location = ForgeRegistries.BLOCKS.getKey(block);
        getBuilder(location.toString())
            .parent(uncheckedExistingModel("block/" + modelName));
    }

    /** Generate an item model that is a child of a block model */
    public void parentBlockItem(RegisteredBlock block, String modelName) {
        parentBlockItem(block.get(), modelName);
    }

    /** Generate a basic item model with specified texture path */
    public void basicItem(Item item, String texturePath) {
        basicItem(item, modLoc("item/" + texturePath));
    }

    /** Generate a basic item model with specified texture path */
    public void basicItem(Item item, ResourceLocation texturePath) {
        var location = ForgeRegistries.ITEMS.getKey(item);
        getBuilder(location.toString())
            .parent(new UncheckedModelFile("minecraft:item/generated"))
            .texture("layer0", texturePath);
    }

    /** Generate a basic item model with specified texture path */
    public void basicItem(Item item, String texturePath, String renderType) {
        var location = ForgeRegistries.ITEMS.getKey(item);
        getBuilder(location.toString())
            .parent(new UncheckedModelFile("minecraft:item/generated"))
            .texture("layer0", modLoc("item/" + texturePath))
            .renderType(renderType);
    }

    /** Get a reference to an existing model file */
    public ExistingModelFile existingModel(String modelName) {
        var file = new ExistingModelFile(modLoc(modelName), existingFileHelper);

        try {
            file.assertExistence();
        }
        catch (Exception e) {
            Utils.modError("The model \"paradisemod:" + modelName + "\" doesn't seem to exist!");
        }

        return file;
    }

    public UncheckedModelFile uncheckedExistingModel(ResourceLocation modelName) {
        ParadiseMod.LOG.warn("Not checking for the existence of \"" + modelName + "\". Please verify it exists if it is being generated!");
        return new UncheckedModelFile(modelName);
    }

    public UncheckedModelFile uncheckedExistingModel(String modelName) {
        return uncheckedExistingModel(modLoc(modelName));
    }
}