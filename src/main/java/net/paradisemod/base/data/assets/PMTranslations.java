package net.paradisemod.base.data.assets;

import java.util.EnumMap;

import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BannerPattern;
import net.minecraftforge.common.data.LanguageProvider;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.PMPaintings;
import net.paradisemod.base.data.PMAdvancements;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.base.registry.RegisteredCreativeTab;
import net.paradisemod.base.registry.RegisteredItem;
import net.paradisemod.bonus.PMBannerPatterns;
import net.paradisemod.world.biome.PMBiomes;

public class PMTranslations extends LanguageProvider {
    private static final EnumMap<DyeColor, String> ENGLISH_DYE_NAMES = new EnumMap<>(DyeColor.class);
    private static final EnumMap<DyeColor, String> SPANISH_DYE_NAMES = new EnumMap<>(DyeColor.class);

    static {
        ENGLISH_DYE_NAMES.put(DyeColor.WHITE, "White");
        ENGLISH_DYE_NAMES.put(DyeColor.ORANGE, "Orange");
        ENGLISH_DYE_NAMES.put(DyeColor.MAGENTA, "Magenta");
        ENGLISH_DYE_NAMES.put(DyeColor.LIGHT_BLUE, "Light Blue");
        ENGLISH_DYE_NAMES.put(DyeColor.YELLOW, "Yellow");
        ENGLISH_DYE_NAMES.put(DyeColor.LIME, "Lime");
        ENGLISH_DYE_NAMES.put(DyeColor.PINK, "Pink");
        ENGLISH_DYE_NAMES.put(DyeColor.GRAY, "Gray");
        ENGLISH_DYE_NAMES.put(DyeColor.LIGHT_GRAY, "Light Gray");
        ENGLISH_DYE_NAMES.put(DyeColor.CYAN, "Cyan");
        ENGLISH_DYE_NAMES.put(DyeColor.PURPLE, "Purple");
        ENGLISH_DYE_NAMES.put(DyeColor.BLUE, "Blue");
        ENGLISH_DYE_NAMES.put(DyeColor.BROWN, "Brown");
        ENGLISH_DYE_NAMES.put(DyeColor.GREEN, "Green");
        ENGLISH_DYE_NAMES.put(DyeColor.RED, "Red");
        ENGLISH_DYE_NAMES.put(DyeColor.BLACK, "Black");
        SPANISH_DYE_NAMES.put(DyeColor.WHITE, "blanco");
        SPANISH_DYE_NAMES.put(DyeColor.ORANGE, "naranja");
        SPANISH_DYE_NAMES.put(DyeColor.MAGENTA, "magenta");
        SPANISH_DYE_NAMES.put(DyeColor.LIGHT_BLUE, "azul claro");
        SPANISH_DYE_NAMES.put(DyeColor.YELLOW, "amarillo");
        SPANISH_DYE_NAMES.put(DyeColor.LIME, "verde lima");
        SPANISH_DYE_NAMES.put(DyeColor.PINK, "rosado");
        SPANISH_DYE_NAMES.put(DyeColor.GRAY, "gris");
        SPANISH_DYE_NAMES.put(DyeColor.LIGHT_GRAY, "gris claro");
        SPANISH_DYE_NAMES.put(DyeColor.CYAN, "cian");
        SPANISH_DYE_NAMES.put(DyeColor.PURPLE, "morado");
        SPANISH_DYE_NAMES.put(DyeColor.BLUE, "azul");
        SPANISH_DYE_NAMES.put(DyeColor.BROWN, "café");
        SPANISH_DYE_NAMES.put(DyeColor.GREEN, "verde");
        SPANISH_DYE_NAMES.put(DyeColor.RED, "rojo");
        SPANISH_DYE_NAMES.put(DyeColor.BLACK, "negro");
    }

    private final boolean isSpanish;

    public PMTranslations(PackOutput output, boolean isSpanish) {
        super(output, ParadiseMod.ID, isSpanish ? "es_mx" : "en_us");
        this.isSpanish = isSpanish;
    }

    @Override
    protected void addTranslations() {
        var langName = isSpanish ? "spanish" : "english";
        var numblocks = 0;
        for(var block : RegisteredBlock.ALL_BLOCKS) {
            var name = block.getLocalizedName(isSpanish);
            if(!name.isEmpty()) {
                add(block.get(), name);
                numblocks++;
            }
            else ParadiseMod.LOG.warn("Block " + block.shortName() + " doesn't have a translation key in " + langName + "!");
        }

        ParadiseMod.LOG.debug("Created " + langName + " names for " + numblocks + " blocks");

        var numitems = 0;
        for(var item : RegisteredItem.ALL_ITEMS) {
            var name = item.getLocalizedName(isSpanish);
            if(!name.isEmpty()) {
                add(item.get(), name);
                numitems++;
            }
            else ParadiseMod.LOG.warn("Item " + item.shortName() + " doesn't have a translation key in " + langName + "!");
        }

        for(var tab : RegisteredCreativeTab.ALL_TABS)
            tab.genLocalizedName(this, isSpanish);

        ParadiseMod.LOG.debug("Created " + langName + " names for " + numitems + " items");
        PMBannerPatterns.translations(this);
        PMBiomes.translations(this);
        PMAdvancements.translations(this, isSpanish);
        add(Blocks.CRAFTING_TABLE, isSpanish ? "Mesa de trabajo de roble" : "Oak Crafting Table");
        add(Blocks.CHEST, isSpanish ? "Cofre de roble" : "Oak Chest");
        add(Blocks.TRAPPED_CHEST, isSpanish ? "Cofre de roble con trampa" : "Trapped Oak Chest");
        add(Blocks.LEVER, isSpanish ? "Palanca de piedra labrada" : "Cobblestone Lever");
        add(Blocks.LIGHT_WEIGHTED_PRESSURE_PLATE, isSpanish ? "Placa de presión de oro (ligero)" : "Gold Pressure Plate (Light)");
        add(Blocks.HEAVY_WEIGHTED_PRESSURE_PLATE, isSpanish ? "Placa de presión de hierro (pesado)" : "Iron Pressure Plate (Heavy)");
        add(Blocks.FURNACE, isSpanish ? "Horno de piedra labrada" : "Cobblestone Furnace");
        add(Blocks.HOPPER, isSpanish ? "Tolva de hierro" : "Iron Hopper");
        add("paradisemod.name", isSpanish ? "Paraíso Mod": "Paradise Mod");
        add("paradisemod.no_more_school", isSpanish ? "¡Gracias a Dios he terminado escuela para siempre!" : "Thank God I'm done with school for good!");

        for(var authorKey : PMPaintings.AUTHOR_KEYS)
            add(authorKey, "NetherNoah777");

        for(var entry : PMPaintings.PAINTING_NAMES.entrySet())
            add(entry.getKey(), entry.getValue());

        if(isSpanish) {
            add(Blocks.END_STONE, "Piedra del Fin");
            add(Blocks.END_STONE_BRICKS, "Ladrillos de piedra del Fin");
            add(Blocks.END_STONE_BRICK_STAIRS, "Escaleras de ladrillos de piedra del Fin");
            add(Blocks.END_STONE_BRICK_SLAB, "Losa de ladrillos de piedra del Fin");
            add(Blocks.REDSTONE_BLOCK, "Bloque de piedra roja");
            add(Blocks.REDSTONE_TORCH, "Antorcha de piedra roja");
            add(Items.REDSTONE, "Polvo de piedra roja");
        }
    }

    public void addBanner(RegistryObject<BannerPattern> pattern, String englishName, String spanishName) {
        var patternName = pattern.getKey().location().getPath();
        var key = "item.paradisemod." + patternName + "_pattern.desc";
        add(key, isSpanish ? spanishName : englishName);
        for(var color : DyeColor.values()) {
            var colorKey = "block.minecraft.banner.paradisemod." + patternName + "." + color.getName();

            if(isSpanish)
                add(colorKey, spanishName + " " + spanishColor(color, patternName == "david_star"));
            else add(colorKey, englishColor(color) + " " + englishName);
        }
    }

    public void addBiome(ResourceKey<Biome> biome, String englishName, String spanishName) {
        var key = "biome.paradisemod." + biome.location().getPath();
        add(key, isSpanish ? spanishName : englishName);
    }

    public static String englishColor(DyeColor color) {
        return ENGLISH_DYE_NAMES.get(color);
    }

    public static String spanishColor(DyeColor color, boolean isFeminine) {
        var colorName = SPANISH_DYE_NAMES.get(color);

        // if color name is supposed to be feminine, replace the ending 'o' with 'a'
        if(isFeminine && color != DyeColor.LIGHT_BLUE && color != DyeColor.LIGHT_GRAY)
            if(colorName.endsWith("o"))
                return colorName.substring(0, colorName.length() - 1) + "a";

        return colorName;
    }
}