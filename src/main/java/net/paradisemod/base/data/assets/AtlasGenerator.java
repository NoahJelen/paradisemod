package net.paradisemod.base.data.assets;

import net.minecraft.client.renderer.texture.atlas.sources.DirectoryLister;
import net.minecraft.data.PackOutput;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.common.data.SpriteSourceProvider;
import net.paradisemod.ParadiseMod;

public class AtlasGenerator extends SpriteSourceProvider {
    public AtlasGenerator(PackOutput output, ExistingFileHelper fileHelper) {
        super(output, fileHelper, ParadiseMod.ID);
    }

    @Override
    protected void addSources() {
        atlas(BANNER_PATTERNS_ATLAS).addSource(new DirectoryLister("entity/banner", "paradisemod"));
        atlas(SHIELD_PATTERNS_ATLAS).addSource(new DirectoryLister("entity/shield", "paradisemod"));
        atlas(CHESTS_ATLAS).addSource(new DirectoryLister("entity/chest", "paradisemod"));
    }
}