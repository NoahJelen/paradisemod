package net.paradisemod.base.data.assets;

import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.ForgeRegistries;

/** This interface can be implemented on a
* class extending `Block` to allow its
* blockstate definition, block models, and item
* models to be created during data generation
*/
public interface ModeledBlock {
    /** Create the item model */
    default void genItemModel(ItemModelGenerator generator) { }

    /** Create the blockstate definition and the block models */
    void genBlockState(BlockStateGenerator generator);

    /** If this interface is implemented on anything that
    * doesn't extend Block, this will throw a ClassCastException
    */
    default Block block() {
        return (Block) this;
    }

    default String registeredName() {
        return ForgeRegistries.BLOCKS.getKey(block()).toString();
    }

    default String shortName() {
        return ForgeRegistries.BLOCKS.getKey(block()).getPath();
    }
}
