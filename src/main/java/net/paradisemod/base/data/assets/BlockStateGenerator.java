package net.paradisemod.base.data.assets;

import net.minecraft.core.Direction;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.client.model.generators.ConfiguredModel;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.client.model.generators.ModelFile.ExistingModelFile;
import net.minecraftforge.client.model.generators.ModelFile.UncheckedModelFile;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.world.blocks.SpreadableBlock;
public class BlockStateGenerator extends BlockStateProvider {
    private final ItemModelGenerator itemModelGenerator;

    public BlockStateGenerator(PackOutput output, ExistingFileHelper fileHelper, ItemModelGenerator itemModelGenerator) {
        super(output, ParadiseMod.ID, fileHelper);
        this.itemModelGenerator = itemModelGenerator;
    }

    @Override
    protected void registerStatesAndModels() {
        for(var block : RegisteredBlock.ALL_BLOCKS) {
            try {
                block.genBlockState(this);
            }
            catch (Exception e) {
                ParadiseMod.LOG.error("Error generating block state definition for " + block.shortName() + ": " + e.getMessage());
                ParadiseMod.LOG.error("Stack trace:", e);
            }
        }

        ParadiseMod.LOG.debug("Finished generating block state definitions for " + RegisteredBlock.ALL_BLOCKS.size() + " blocks");
    }

    @Override
    public ItemModelGenerator itemModels() {
        return itemModelGenerator;
    }

    /** Get a reference to an existing model file */
    public ExistingModelFile existingModel(String modelName) throws IllegalStateException {
        return existingModel(modLoc("block/" + modelName));
    }

    /** Get a reference to an existing Minecraft model file */
    public ExistingModelFile existingMCModel(String modelName) {
        return existingModel(mcLoc("block/" + modelName));
    }

    /** Get a reference to an existing model file */
    public ExistingModelFile existingModel(ResourceLocation modelName) {
        var file = new ExistingModelFile(modelName, models().existingFileHelper);

        try {
            file.assertExistence();
        }
        catch (Exception e) {
            throw new IllegalStateException("The model \"" + modelName + "\" doesn't seem to exist!");
        }

        return file;
    }

    public UncheckedModelFile uncheckedExistingModel(ResourceLocation modelName) {
        ParadiseMod.LOG.warn("Not checking for the existence of \"" + modelName + "\". Please verify it exists if it is being generated!");
        return new UncheckedModelFile(modelName);
    }

    public UncheckedModelFile uncheckedExistingModel(String modelName) {
        return uncheckedExistingModel(modLoc("block/" + modelName));
    }

    /** Build and configure a block model for a block state variant */
    public ConfiguredModel[] buildVariantModel(ModelFile model, Direction facing, boolean fourWay) {
        var xRot = calcXRot(facing);
        if(fourWay) xRot = 0;
        var yRot = calcYRot(facing);
        return buildVariantModel(model, xRot, yRot);
    }

    /** Build and configure a block model for a block state variant */
    public ConfiguredModel[] buildVariantModel(ModelFile model, Direction facing) {
        return buildVariantModel(model, facing, false);
    }

    /** Build and configure a block model for a block state variant 
    * where the default model is facing up */
    public ConfiguredModel[] buildVariantModelFacingUp(ModelFile model, Direction facing) {
        var xRot = calcXRot(facing) + 90;
        var yRot = calcYRot(facing);
        return buildVariantModel(model, xRot, yRot);
    }

    public static int calcXRot(Direction facing) {
        return switch(facing) {
            case DOWN -> 90;
            case UP -> 270;
            default -> 0;
        };
    }

    public static int calcYRot(Direction facing) {
        return switch(facing) {
            case EAST -> 90;
            case WEST -> 270;
            default -> 0;
            case SOUTH -> 180;
        };
    }

    /** Build and configure a block model for a block state variant */
    public ConfiguredModel[] buildVariantModel(ModelFile model) {
        return buildVariantModel(model, Direction.NORTH, true);
    }

    /** Build and configure a block model for a block state variant */
    public ConfiguredModel[] buildVariantModel(ModelFile model, int xRot, int yRot) {
        var builder = ConfiguredModel.builder()
            .modelFile(model);

        if(yRot > 0) builder = builder.rotationY(yRot);
        if(xRot > 0 && xRot < 360) builder = builder.rotationX(xRot);
        return builder.build();
    }

    /** Generate a blockstate definition for a simple plant */
    public void genSimplePlant(Block block, String modelName) {
        var model = models()
            .cross("block/" + modelName, modLoc("block/" + modelName))
            .renderType("cutout");

        var key = ForgeRegistries.BLOCKS.getKey(block);

        itemModels()
            .flatBlockItem(key.getPath(), modelName);

        simpleBlock(block, model);
    }

    /** Generate a blockstate definition for a simple plant */
    public void genSimplePlant(Block block) {
        var key = ForgeRegistries.BLOCKS.getKey(block);
        genSimplePlant(block, key.getPath());
    }

    public void grassBlockLike(RegisteredBlock block, String bottomTexture, boolean isSnowy) {
        var name = block.shortName();

        if(isSnowy)
            grassBlockLike(block, bottomTexture, "paradisemod:block/snowy_" + name + "_side");
        else {
            var modelBuilder = models();
            var model = modelBuilder
                .cubeBottomTop(block.shortName(), modLoc("block/" + name + "_side"), new ResourceLocation(bottomTexture), modLoc("block/" + name + "_top"));

            simpleBlock(block, model);
        }
    }

    public void grassBlockLike(RegisteredBlock block, String bottomTexture, String snowySideTexture) {
        var name = block.shortName();
        var modelBuilder = models();

        var model = modelBuilder
            .cubeBottomTop(block.shortName(),  modLoc("block/" + name + "_side"), new ResourceLocation(bottomTexture), modLoc("block/" + name + "_top"));

        var snowy = modelBuilder
            .cubeBottomTop("snowy_" + block.shortName(), new ResourceLocation(snowySideTexture), new ResourceLocation(bottomTexture), mcLoc("block/grass_block_top"));

        getVariantBuilder(block.get())
            .forAllStates(
                state -> {
                    boolean isBlockSnowy = state.getValue(SpreadableBlock.SNOWY);

                    if(isBlockSnowy)
                        return buildVariantModel(snowy);
                    else return buildVariantModel(model);
                }
            );
    }

    /** Generate a blockstate definition for a simple cube block */
    public void simpleBlock(RegisteredBlock block, String modelName) {
        simpleBlock(block.get(), modelName);
    }

    /** Generate a blockstate definition for a simple cube block */
    public void simpleBlock(RegisteredBlock block, ModelFile model) {
        simpleBlock(block.get(), model);
    }

    /** Generate a blockstate definition for a simple cube block */
    public void simpleBlock(RegisteredBlock block) {
        simpleBlock(block.get());
    }

    /** Generate a blockstate definition for a simple cube block */
    public void simpleBlock(Block block, String modelName) {
        var model = models().cubeAll("block/" + modelName, modLoc("block/" + modelName));
        simpleBlock(block, model);
    }

    /** Generate a blockstate definition for a simple cube block */
    @Override
    public void simpleBlock(Block block, ModelFile model) {
        getVariantBuilder(block)
            .partialState()
            .setModels(buildVariantModel(model));
    }

    /** Generate a blockstate definition for a simple cube block */
    @Override
    public void simpleBlock(Block block) {
        var key = ForgeRegistries.BLOCKS.getKey(block);
        simpleBlock(block, key.getPath());
    }

    /** Generate a blockstate definition for a simple cube block */
    public void simpleBlockWithRenderType(Block block, String renderType) {
        var key = ForgeRegistries.BLOCKS.getKey(block);
        simpleBlockWithRenderType(block, key.getPath(), renderType);
    }

    /** Generate a blockstate definition for a simple cube block */
    public void simpleBlockWithRenderType(Block block, String modelName, String renderType) {
        var model = models()
            .cubeAll("block/" + modelName, modLoc("block/" + modelName))
            .renderType(renderType);

        simpleBlock(block, model);
    }

    public void vanillaTintedLeaves(RegisteredBlock block, String vanillaLeavesName) {
        var blockName = block.shortName();

        var model = models().withExistingParent("block/" + blockName, "leaves")
            .texture("all", "minecraft:block/" + vanillaLeavesName + "_leaves")
            .renderType("cutout");

        simpleBlock(block, model);
    }

    public void tintedGrass(RegisteredBlock block) {
        var blockName = block.shortName();

        var model = models()
            .singleTexture(
                "block/" + blockName,
                new ResourceLocation("block/tinted_cross"),
                "cross", mcLoc("block/grass")
            )
            .renderType("cutout");

        itemModels()
            .getBuilder("item/" + blockName)
            .parent(new UncheckedModelFile("minecraft:item/generated"))
            .texture("layer0", mcLoc("block/grass"));

        simpleBlock(block, model);
    }
}
