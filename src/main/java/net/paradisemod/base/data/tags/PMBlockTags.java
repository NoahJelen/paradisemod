package net.paradisemod.base.data.tags;

import java.util.concurrent.CompletableFuture;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.data.BlockTagsProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.BlockTemplates;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.worldgen.structures.processors.DarkDungeonProcessor;

@SuppressWarnings("unchecked")
public class PMBlockTags extends BlockTagsProvider {
    public PMBlockTags(PackOutput output, CompletableFuture<HolderLookup.Provider> lookupProvider, ExistingFileHelper existingFileHelper) {
        super(output, lookupProvider, ParadiseMod.ID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider provider) {
        for(var entry : RegisteredBlock.TAGGED_BLOCKS.entries()) {
            var tag = entry.getKey();
            var block = entry.getValue();
            tag(tag).add(block.get());
        }

        tag(BlockTags.LOGS_THAT_BURN)
            .addTags(
                BlockTemplates.logTag("palo_verde"),
                BlockTemplates.logTag("mesquite")
            );

        tag(BlockTags.LOGS)
            .addTags(
                BlockTemplates.logTag("blackened_oak"),
                BlockTemplates.logTag("blackened_spruce"),
                BlockTemplates.logTag("glowing_oak")
            );

        tag(BlockTags.MINEABLE_WITH_PICKAXE)
            .addTags(
                PMTags.Blocks.GLOWING_OBSIDIAN,
                PMTags.Blocks.GLOWING_OBSIDIAN_STAIRS,
                PMTags.Blocks.GLOWING_OBSIDIAN_SLABS
            );

        tag(BlockTags.SLABS)
            .addTag(PMTags.Blocks.GLOWING_OBSIDIAN_SLABS);

        tag(BlockTags.STAIRS)
            .addTag(PMTags.Blocks.GLOWING_OBSIDIAN_STAIRS);

        tag(BlockTags.NEEDS_DIAMOND_TOOL)
            .addTags(
                PMTags.Blocks.GLOWING_OBSIDIAN,
                PMTags.Blocks.GLOWING_OBSIDIAN_STAIRS,
                PMTags.Blocks.GLOWING_OBSIDIAN_SLABS,
                PMTags.Blocks.GLOWING_OBSIDIAN_WALLS
            );

        tag(BlockTags.BEACON_BASE_BLOCKS)
            .addTag(PMTags.Blocks.GLOWING_OBSIDIAN);

        tag(BlockTags.DIRT)
            .add(Blocks.SOUL_SAND, Blocks.SOUL_SOIL);

        tag(Tags.Blocks.STONE)
            .add(Blocks.DRIPSTONE_BLOCK, Blocks.CALCITE);

        tag(BlockTags.SMALL_FLOWERS)
            .addTag(PMTags.Blocks.ROSES);

        tag(BlockTags.WALLS)
            .addTag(PMTags.Blocks.GLOWING_OBSIDIAN_WALLS);

        tag(BlockTags.STAIRS)
            .addTag(PMTags.Blocks.GLOWING_OBSIDIAN_STAIRS);

        tag(BlockTags.SLABS)
            .addTag(PMTags.Blocks.GLOWING_OBSIDIAN_SLABS);

        tag(PMTags.Blocks.BLACKENED_FOLIAGE)
            .add(
                Blocks.RED_MUSHROOM,
                Blocks.BROWN_MUSHROOM
            );

        tag(PMTags.Blocks.BUOY_LANTERNS)
            .add(
                Blocks.LANTERN,
                Blocks.SOUL_LANTERN
            )
            .addOptional(new ResourceLocation("charm:gold_lantern"))
            .addOptional(new ResourceLocation("charm:gold_soul_lantern"))
            .addOptional(new ResourceLocation("byg:therium_lantern"))
            .addOptional(new ResourceLocation("byg:glowstone_lantern"));

        tag(PMTags.Blocks.LANTERNS)
            .add(
                Blocks.LANTERN,
                Blocks.SOUL_LANTERN
            )
            .addOptional(new ResourceLocation("charm:gold_lantern"))
            .addOptional(new ResourceLocation("charm:gold_soul_lantern"))
            .addOptional(new ResourceLocation("byg:therium_lantern"))
            .addOptional(new ResourceLocation("byg:glowstone_lantern"))
            .addOptional(new ResourceLocation("byg:redstone_lantern"));

        tag(PMTags.Blocks.CARVABLES)
            .addTags(
                Tags.Blocks.STONE,
                Tags.Blocks.COBBLESTONE
            )
            .add(
                Blocks.SNOW,
                Blocks.PACKED_ICE,
                Blocks.GLOWSTONE,
                Blocks.SEA_LANTERN,
                Blocks.GRASS,
                Blocks.TALL_GRASS,
                Blocks.FERN,
                Blocks.LARGE_FERN,
                Blocks.VINE,
                Blocks.BAMBOO,
                Blocks.BAMBOO_SAPLING,
                Blocks.SEAGRASS,
                Blocks.TALL_SEAGRASS
            );

        tag(BlockTags.FEATURES_CANNOT_REPLACE)
            .addTags(
                PMTags.Blocks.GLOWING_OBSIDIAN,
                BlockTags.WOOL,
                BlockTags.SLABS,
                BlockTags.PLANKS,
                BlockTags.STONE_BRICKS,
                BlockTags.WOOL_CARPETS,
                BlockTags.STAIRS,
                BlockTags.FENCES,
                BlockTags.FENCE_GATES
            )
            .add(
                Blocks.DIRT_PATH,
                Blocks.BRICKS,
                Blocks.GLASS
            );

        tag(PMTags.Blocks.GLOWING_FOLIAGE)
            .add(
                Blocks.RED_MUSHROOM,
                Blocks.BROWN_MUSHROOM
            );

        tag(PMTags.Blocks.GROUND_BLOCKS)
            .addTags(
                BlockTags.DIRT,
                BlockTags.TERRACOTTA,
                Tags.Blocks.STONE,
                Tags.Blocks.END_STONES,
                BlockTags.SAND,
                Tags.Blocks.SANDSTONE
            )
            .add(
                Blocks.GRAVEL,
                Blocks.SNOW_BLOCK,
                Blocks.BLUE_ICE,
                Blocks.OBSIDIAN,
                Blocks.MAGMA_BLOCK,
                Blocks.BASALT
            );

        tag(BlockTags.SCULK_REPLACEABLE_WORLD_GEN)
            .add(Blocks.SCULK_CATALYST);

        tag(BlockTags.SCULK_REPLACEABLE)
            .add(DarkDungeonProcessor.ALL_CONCRETE.toArray(Block[]::new));
    }
}