package net.paradisemod.base.data.tags;

import java.util.HashMap;

import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Block;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.BlockTemplates;

public class PMTags {
    public class Blocks {
        public static final TagKey<Block> GLOWING_OBSIDIAN = createModTag("glowing_obsidian", Registries.BLOCK);
        public static final TagKey<Block> GLOWING_OBSIDIAN_STAIRS = createModTag("glowing_obsidian_stairs", Registries.BLOCK);
        public static final TagKey<Block> GLOWING_OBSIDIAN_SLABS = createModTag("glowing_obsidian_slabs", Registries.BLOCK);
        public static final TagKey<Block> GLOWING_OBSIDIAN_WALLS = createModTag("glowing_obsidian_walls", Registries.BLOCK);
        public static final TagKey<Block> ROSES = createModTag("roses", Registries.BLOCK);
        public static final TagKey<Block> BLACKENED_FOLIAGE = createModTag("blackened_foliage", Registries.BLOCK);
        public static final TagKey<Block> BUOY_LANTERNS = createModTag("buoy_lanterns", Registries.BLOCK);
        public static final TagKey<Block> LANTERNS = createModTag("lanterns", Registries.BLOCK);
        public static final TagKey<Block> BUOY_LIGHTS = createModTag("buoy_lights", Registries.BLOCK);
        public static final TagKey<Block> CARVABLES = createModTag("carvables", Registries.BLOCK);
        public static final TagKey<Block> CRYSTAL_CLUSTERS = createModTag("crystal_clusters", Registries.BLOCK);
        public static final TagKey<Block> END_FOLIAGE = createModTag("end_foliage", Registries.BLOCK);
        public static final TagKey<Block> GLOWING_FOLIAGE = createModTag("glowing_foliage", Registries.BLOCK);
        public static final TagKey<Block> METAL_FENCES = createModTag("metal_fences", Registries.BLOCK);
        public static final TagKey<Block> GROUND_BLOCKS = createModTag("ground_blocks", Registries.BLOCK);
    }

    public class Biomes {
        public static TagKey<Biome> DARK_DESERTS = createModTag("dark_deserts", Registries.BIOME);
        public static TagKey<Biome> ROCKY_DESERTS = createModTag("rocky_deserts", Registries.BIOME);
        public static TagKey<Biome> COLD_ROCKY_DESERTS = createModTag("cold_rocky_deserts", Registries.BIOME);
        public static TagKey<Biome> DEEP_DARK = createModTag("deep_dark", Registries.BIOME);
        public static TagKey<Biome> ELYSIUM = createModTag("elysium", Registries.BIOME);
        public static TagKey<Biome> OVERWORLD_CORE = createModTag("overworld_core", Registries.BIOME);
        public static TagKey<Biome> MINER_BASE_BIOMES = createModTag("miner_base_biomes", Registries.BIOME);
        public static TagKey<Biome> SALT = createModTag("salt", Registries.BIOME);
        public static TagKey<Biome> VOLCANIC = createModTag("volcanic", Registries.BIOME);
        public static TagKey<Biome> ROSE_FIELDS = createModTag("rose_fields", Registries.BIOME);
    }

    public class Items {
        public static final HashMap<TagKey<Block>, TagKey<Item>> MIRRORED_BLOCK_ITEM_TAGS = new HashMap<>();

        static {
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.WOOL, ItemTags.WOOL);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.PLANKS, ItemTags.PLANKS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.STONE_BRICKS, ItemTags.STONE_BRICKS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.WOODEN_BUTTONS, ItemTags.WOODEN_BUTTONS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.STONE_BUTTONS, ItemTags.STONE_BUTTONS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.BUTTONS, ItemTags.BUTTONS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.WOOL_CARPETS, ItemTags.WOOL_CARPETS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.WOODEN_DOORS, ItemTags.WOODEN_DOORS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.WOODEN_STAIRS, ItemTags.WOODEN_STAIRS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.WOODEN_SLABS, ItemTags.WOODEN_SLABS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.WOODEN_FENCES, ItemTags.WOODEN_FENCES);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.FENCE_GATES, ItemTags.FENCE_GATES);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.WOODEN_PRESSURE_PLATES, ItemTags.WOODEN_PRESSURE_PLATES);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.WOODEN_TRAPDOORS, ItemTags.WOODEN_TRAPDOORS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.DOORS, ItemTags.DOORS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.SAPLINGS, ItemTags.SAPLINGS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.LOGS_THAT_BURN, ItemTags.LOGS_THAT_BURN);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.LOGS, ItemTags.LOGS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.BANNERS, ItemTags.BANNERS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.SAND, ItemTags.SAND);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.SMELTS_TO_GLASS, ItemTags.SMELTS_TO_GLASS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.STAIRS, ItemTags.STAIRS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.SLABS, ItemTags.SLABS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.WALLS, ItemTags.WALLS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.ANVIL, ItemTags.ANVIL);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.RAILS, ItemTags.RAILS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.LEAVES, ItemTags.LEAVES);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.TRAPDOORS, ItemTags.TRAPDOORS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.SMALL_FLOWERS, ItemTags.SMALL_FLOWERS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.BEDS, ItemTags.BEDS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.FENCES, ItemTags.FENCES);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.TALL_FLOWERS, ItemTags.TALL_FLOWERS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.FLOWERS, ItemTags.FLOWERS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.PIGLIN_REPELLENTS, ItemTags.PIGLIN_REPELLENTS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.GOLD_ORES, ItemTags.GOLD_ORES);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.IRON_ORES, ItemTags.IRON_ORES);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.DIAMOND_ORES, ItemTags.DIAMOND_ORES);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.REDSTONE_ORES, ItemTags.REDSTONE_ORES);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.LAPIS_ORES, ItemTags.LAPIS_ORES);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.COAL_ORES, ItemTags.COAL_ORES);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.EMERALD_ORES, ItemTags.EMERALD_ORES);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.COPPER_ORES, ItemTags.COPPER_ORES);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.SOUL_FIRE_BASE_BLOCKS, ItemTags.SOUL_FIRE_BASE_BLOCKS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.CANDLES, ItemTags.CANDLES);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.DIRT, ItemTags.DIRT);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.TERRACOTTA, ItemTags.TERRACOTTA);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.COMPLETES_FIND_TREE_TUTORIAL, ItemTags.COMPLETES_FIND_TREE_TUTORIAL);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.SIGNS, ItemTags.SIGNS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTags.DAMPENS_VIBRATIONS, ItemTags.DAMPENS_VIBRATIONS);
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTemplates.logTag("blackened_oak"), BlockTemplates.logTagItem("blackened_oak"));
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTemplates.logTag("blackened_spruce"), BlockTemplates.logTagItem("blackened_spruce"));
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTemplates.logTag("glowing_oak"), BlockTemplates.logTagItem("glowing_oak"));
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTemplates.logTag("mesquite"), BlockTemplates.logTagItem("mesquite"));
            MIRRORED_BLOCK_ITEM_TAGS.put(BlockTemplates.logTag("palo_verde"), BlockTemplates.logTagItem("palo_verde"));
            MIRRORED_BLOCK_ITEM_TAGS.put(Blocks.GLOWING_OBSIDIAN, Items.GLOWING_OBSIDIAN);
        }

        public static final TagKey<Item> SALT_DUSTS = createForgeItemTag("dusts/salt");
        public static final TagKey<Item> RUBY_GEMS = createForgeItemTag("gems/ruby");
        public static final TagKey<Item> ENDERITE_INGOTS = createForgeItemTag("ingots/enderite");
        public static final TagKey<Item> RUSTED_IRON_INGOTS = createForgeItemTag("ingots/rusted_iron");
        public static final TagKey<Item> SILVER_INGOTS = createForgeItemTag("ingots/silver_iron");
        public static final TagKey<Item> RUSTED_IRON_NUGGETS = createForgeItemTag("nuggets/rusted_iron");
        public static final TagKey<Item> SILVER_NUGGETS = createForgeItemTag("nuggets/silver_iron");

        public static final TagKey<Item> ANTENNAS = createModTag("antennas", Registries.ITEM);
        public static final TagKey<Item> TRANSMITTERS = createModTag("transmitters", Registries.ITEM);
        public static final TagKey<Item> GLOWING_OBSIDIAN = createMirroredBlockItemTag(Blocks.GLOWING_OBSIDIAN);
        public static final TagKey<Item> PRESENTS = createModTag("presents", Registries.ITEM);
        public static final TagKey<Item> SILVER_RECYCLABLES = createModTag("silver_recyclables", Registries.ITEM);

        private static TagKey<Item> createMirroredBlockItemTag(TagKey<Block> blockTag) {
            var location = blockTag.location();
            var itemTag = ItemTags.create(location);
            MIRRORED_BLOCK_ITEM_TAGS.put(blockTag, itemTag);
            return itemTag;
        }

        private static TagKey<Item> createForgeItemTag(String name) {
            return TagKey.create(Registries.ITEM, new ResourceLocation("forge", name));
        }
    }

    public static <T> TagKey<T> createModTag(String name, ResourceKey<Registry<T>> registryKey) {
        return TagKey.create(registryKey, new ResourceLocation(ParadiseMod.ID, name));
    }
}