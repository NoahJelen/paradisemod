package net.paradisemod.base.data.tags;

import java.util.concurrent.CompletableFuture;

import net.minecraft.core.HolderLookup;
import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.BannerPatternTagsProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.paradisemod.ParadiseMod;
import net.paradisemod.bonus.PMBannerPatterns;

public class PMBannerTags extends BannerPatternTagsProvider {
    public PMBannerTags(PackOutput output, CompletableFuture<Provider> provider, ExistingFileHelper existingFileHelper) {
        super(output, provider, ParadiseMod.ID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider provider) {
        for(var entry : PMBannerPatterns.REGISTERED_BANNERS.entrySet()) {
            var tag = entry.getKey();
            var pattern = entry.getValue().getKey();
            tag(tag).add(pattern);
        }
    }
}
