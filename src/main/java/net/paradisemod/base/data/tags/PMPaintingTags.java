package net.paradisemod.base.data.tags;

import java.util.concurrent.CompletableFuture;

import javax.annotation.Nullable;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.PaintingVariantTagsProvider;
import net.minecraft.tags.PaintingVariantTags;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.PMPaintings;

public class PMPaintingTags extends PaintingVariantTagsProvider {
    public PMPaintingTags(PackOutput output, CompletableFuture<HolderLookup.Provider> provider, @Nullable ExistingFileHelper existingFileHelper) {
        super(output, provider, ParadiseMod.ID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider provider) {
        for(var painting : PMPaintings.ALL_PAINTINGS)
            tag(PaintingVariantTags.PLACEABLE).add(painting.getKey());
    }
}
