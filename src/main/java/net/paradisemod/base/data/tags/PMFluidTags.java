package net.paradisemod.base.data.tags;

import java.util.concurrent.CompletableFuture;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.FluidTagsProvider;
import net.minecraft.tags.FluidTags;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.paradisemod.ParadiseMod;
import net.paradisemod.world.fluid.PMFluids;

public class PMFluidTags extends FluidTagsProvider {
    public PMFluidTags(PackOutput output, CompletableFuture<HolderLookup.Provider> provider, ExistingFileHelper existingFileHelper) {
        super(output, provider, ParadiseMod.ID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider provider) {
        tag(FluidTags.LAVA)
            .add(
                PMFluids.LIQUID_REDSTONE.get(),
                PMFluids.FLOWING_LIQUID_REDSTONE.get(),
                PMFluids.MOLTEN_SALT.get(),
                PMFluids.FLOWING_MOLTEN_SALT.get(),
                PMFluids.PSYCHEDELIC_LAVA.get(),
                PMFluids.FLOWING_PSYCHEDELIC_LAVA.get(),
                PMFluids.DARK_LAVA.get(),
                PMFluids.FLOWING_DARK_LAVA.get(),
                PMFluids.MOLTEN_GOLD.get(),
                PMFluids.FLOWING_MOLTEN_GOLD.get(),
                PMFluids.MOLTEN_SILVER.get(),
                PMFluids.FLOWING_MOLTEN_SILVER.get(),
                PMFluids.MOLTEN_IRON.get(),
                PMFluids.FLOWING_MOLTEN_IRON.get(),
                PMFluids.MOLTEN_COPPER.get(),
                PMFluids.FLOWING_MOLTEN_COPPER.get(),
                PMFluids.TAR.get(),
                PMFluids.FLOWING_TAR.get()
            );

        tag(FluidTags.WATER)
            .add(
                PMFluids.ENDER_ACID.get(),
                PMFluids.FLOWING_ENDER_ACID.get(),
                PMFluids.GLOWING_WATER.get(),
                PMFluids.FLOWING_GLOWING_WATER.get(),
                PMFluids.PSYCHEDELIC_FLUID.get(),
                PMFluids.FLOWING_PSYCHEDELIC_FLUID.get(),
                PMFluids.HONEY.get(),
                PMFluids.FLOWING_HONEY.get()
            );
    }
}