package net.paradisemod.base.data.tags;

import java.util.concurrent.CompletableFuture;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.BiomeTagsProvider;
import net.minecraft.tags.BiomeTags;
import net.minecraft.world.level.biome.Biomes;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.paradisemod.ParadiseMod;
import net.paradisemod.world.biome.PMBiomes;

@SuppressWarnings("unchecked")
public class PMBiomeTags extends BiomeTagsProvider {
    public PMBiomeTags(PackOutput output, CompletableFuture<HolderLookup.Provider> provider, ExistingFileHelper existingFileHelper) {
        super(output, provider, ParadiseMod.ID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider provider) {
        for(var entry : PMBiomes.TAGGED_BIOMES.entries()) {
            var tag = entry.getKey();
            var biomeKey = entry.getValue();

            tag(tag).add(biomeKey);
        }

        tag(Tags.Biomes.IS_COLD)
            .addTag(PMTags.Biomes.COLD_ROCKY_DESERTS);

        tag(Tags.Biomes.IS_DRY)
            .addTags(
                PMTags.Biomes.ROCKY_DESERTS,
                PMTags.Biomes.SALT
            );

        tag(Tags.Biomes.IS_HOT)
            .addTags(
                PMTags.Biomes.ROCKY_DESERTS,
                PMTags.Biomes.SALT
            );

        tag(Tags.Biomes.IS_PLAINS)
            .addTag(PMTags.Biomes.ROSE_FIELDS);

        tag(BiomeTags.HAS_RUINED_PORTAL_STANDARD)
            .addTags(
                PMTags.Biomes.ROSE_FIELDS,
                PMTags.Biomes.ROCKY_DESERTS,
                PMTags.Biomes.COLD_ROCKY_DESERTS,
                PMTags.Biomes.SALT,
                PMTags.Biomes.VOLCANIC
            );

        tag(BiomeTags.HAS_STRONGHOLD)
            .addTags(
                PMTags.Biomes.ROSE_FIELDS,
                PMTags.Biomes.ROCKY_DESERTS,
                PMTags.Biomes.COLD_ROCKY_DESERTS,
                PMTags.Biomes.SALT,
                PMTags.Biomes.VOLCANIC
            );

        tag(PMTags.Biomes.DEEP_DARK).add(Biomes.DEEP_DARK);

        tag(PMTags.Biomes.MINER_BASE_BIOMES)
            .addTags(
                BiomeTags.IS_OVERWORLD,
                PMTags.Biomes.OVERWORLD_CORE,
                PMTags.Biomes.ELYSIUM
            );

        tag(BiomeTags.IS_OVERWORLD)
            .remove(Biomes.DEEP_DARK);

        tag(Tags.Biomes.IS_CONIFEROUS)
            .add(Biomes.OLD_GROWTH_SPRUCE_TAIGA);

        tag(BiomeTags.HAS_ANCIENT_CITY)
            .addTag(PMTags.Biomes.DEEP_DARK);
    }
}