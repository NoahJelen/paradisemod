package net.paradisemod.base.data.tags;

import java.util.concurrent.CompletableFuture;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.data.tags.TagsProvider;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.base.registry.RegisteredItem;

public class PMItemTags extends ItemTagsProvider {
    public PMItemTags(PackOutput output, CompletableFuture<HolderLookup.Provider> provider, CompletableFuture<TagsProvider.TagLookup<Block>> tagProvider, ExistingFileHelper existingFileHelper) {
        super(output, provider, tagProvider, ParadiseMod.ID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider provider) {
        for(var entry : PMTags.Items.MIRRORED_BLOCK_ITEM_TAGS.entrySet()) {
            var blockTag = entry.getKey();
            var itemTag = entry.getValue();

            tag(itemTag)
                .add(
                    RegisteredBlock.TAGGED_BLOCKS.get(blockTag)
                        .stream()
                        .map(ItemLike::asItem)
                        .toArray(Item[]::new)
                );
        }

        for(var entry : RegisteredItem.TAGGED_ITEMS.entries()) {
            var tag = entry.getKey();
            var item = entry.getValue();
            tag(tag).add(item.get());
        }

        tag(ItemTags.STONE_CRAFTING_MATERIALS)
            .add(
                Blocks.STONE.asItem(),
                Blocks.END_STONE.asItem(),
                Blocks.GRANITE.asItem(),
                Blocks.DIORITE.asItem(),
                Blocks.ANDESITE.asItem(),
                Blocks.DRIPSTONE_BLOCK.asItem(),
                Blocks.CALCITE.asItem(),
                Blocks.TUFF.asItem(),
                Blocks.DEEPSLATE.asItem(),
                Blocks.COBBLED_DEEPSLATE.asItem()
            );

        tag(ItemTags.STONE_TOOL_MATERIALS)
            .add(
                Blocks.STONE.asItem(),
                Blocks.END_STONE.asItem(),
                Blocks.GRANITE.asItem(),
                Blocks.DIORITE.asItem(),
                Blocks.ANDESITE.asItem()
            );

        tag(PMTags.Items.PRESENTS)
            .add(
                Blocks.OBSIDIAN.asItem(),
                Blocks.REDSTONE_WIRE.asItem(),
                Items.NETHERITE_SWORD,
                Items.NETHERITE_PICKAXE,
                Items.NETHERITE_AXE,
                Items.NETHERITE_SHOVEL,
                Items.NETHERITE_HOE,
                Items.NETHERITE_INGOT,
                Items.DIAMOND_SWORD,
                Items.DIAMOND_PICKAXE,
                Items.DIAMOND_AXE,
                Items.DIAMOND_SHOVEL,
                Items.DIAMOND_HOE,
                Items.DIAMOND,
                Items.GOLDEN_SWORD,
                Items.GOLDEN_PICKAXE,
                Items.GOLDEN_AXE,
                Items.GOLDEN_SHOVEL,
                Items.GOLDEN_HOE,
                Items.GOLD_INGOT,
                Items.IRON_SWORD,
                Items.IRON_PICKAXE,
                Items.IRON_AXE,
                Items.IRON_SHOVEL,
                Items.IRON_HOE,
                Items.IRON_INGOT,
                Items.DIAMOND_HELMET,
                Items.DIAMOND_CHESTPLATE,
                Items.DIAMOND_LEGGINGS,
                Items.DIAMOND_BOOTS,
                Items.IRON_HELMET,
                Items.IRON_CHESTPLATE,
                Items.IRON_LEGGINGS,
                Items.IRON_BOOTS,
                Items.GOLDEN_HELMET,
                Items.GOLDEN_CHESTPLATE,
                Items.GOLDEN_LEGGINGS,
                Items.GOLDEN_BOOTS,
                Items.TRIDENT,
                Items.HEART_OF_THE_SEA,
                Blocks.CONDUIT.asItem()
            );
    }
}