package net.paradisemod.base.data;

import java.util.Set;

import net.minecraft.core.registries.Registries;
import net.minecraftforge.common.data.DatapackBuiltinEntriesProvider;
import net.minecraftforge.data.event.GatherDataEvent;
import net.minecraftforge.registries.ForgeRegistries;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.data.assets.AtlasGenerator;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ItemModelGenerator;
import net.paradisemod.base.data.assets.PMTranslations;
import net.paradisemod.base.data.loot.PMLootTables;
import net.paradisemod.base.data.tags.PMBannerTags;
import net.paradisemod.base.data.tags.PMBiomeTags;
import net.paradisemod.base.data.tags.PMBlockTags;
import net.paradisemod.base.data.tags.PMFluidTags;
import net.paradisemod.base.data.tags.PMItemTags;
import net.paradisemod.base.data.tags.PMPaintingTags;
import net.paradisemod.world.biome.PMBiomes;
import net.paradisemod.world.dimension.PMDimensions;
import net.paradisemod.worldgen.carver.PMCarvers;
import net.paradisemod.worldgen.features.PMFeatures;
import net.paradisemod.worldgen.structures.PMStructures;
import net.paradisemod.worldgen.PMBiomeModifier;

//TODO: ask the NeoForge devs about adding my data generation API to Forge
public class DataGen {
    private static final PMRegistrySetBuilder WORLDGEN_DATA = new PMRegistrySetBuilder()
        .add(Registries.BIOME, PMBiomes.BIOMES)
        .add(Registries.CONFIGURED_CARVER , PMCarvers.CFG_CARVERS)
        .add(Registries.CONFIGURED_FEATURE, PMFeatures.CFG_FEATURES)
        .add(Registries.PLACED_FEATURE, PMFeatures.PLACED_FEATURES)
        .add(Registries.STRUCTURE, PMStructures.STRUCTURES)
        .add(Registries.TEMPLATE_POOL, PMStructures::buildStructPools)
        .add(Registries.STRUCTURE_SET, PMStructures::buildStructSets)
        .add(Registries.DIMENSION_TYPE, PMDimensions::buildDimensionTypes)
        .add(Registries.LEVEL_STEM, PMDimensions::buildDimensions)
        .add(ForgeRegistries.Keys.BIOME_MODIFIERS, PMBiomeModifier::buildBiomeModifier);

    public static void generateData(GatherDataEvent event) {
        var generator = event.getGenerator();
        var output = generator.getPackOutput();
        var fileHelper = event.getExistingFileHelper();
        var blankLookup = event.getLookupProvider();

        // server data
        var dataRegistries = new DatapackBuiltinEntriesProvider(output, blankLookup, WORLDGEN_DATA, Set.of(ParadiseMod.ID));
        var registryProvider = dataRegistries.getRegistryProvider();
        generator.addProvider(true, dataRegistries);
        generator.addProvider(true, new PMLootTables(output));
        generator.addProvider(true, new RecipeGenerator(output));
        generator.addProvider(true, new PMAdvancements(output, registryProvider, fileHelper));

        // tags
        var blockTags = new PMBlockTags(output, registryProvider, fileHelper);
        generator.addProvider(true, blockTags);
        generator.addProvider(true, new PMItemTags(output, registryProvider, blockTags.contentsGetter(), fileHelper));
        generator.addProvider(true, new PMBiomeTags(output, registryProvider, fileHelper));
        generator.addProvider(true, new PMFluidTags(output, registryProvider, fileHelper));
        generator.addProvider(true, new PMBannerTags(output, registryProvider, fileHelper));
        generator.addProvider(true, new PMPaintingTags(output, registryProvider, fileHelper));

        // client assets
        var itemModelGenerator = new ItemModelGenerator(output, fileHelper);
        generator.addProvider(true, new AtlasGenerator(output, fileHelper));
        generator.addProvider(true, new BlockStateGenerator(output, fileHelper, itemModelGenerator));
        generator.addProvider(true, itemModelGenerator);
        generator.addProvider(true, new PMTranslations(output, true));
        generator.addProvider(true, new PMTranslations(output, false));
    }
}