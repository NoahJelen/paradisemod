package net.paradisemod.base.data;

import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.critereon.ImpossibleTrigger;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.PackOutput;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.RecipeProvider;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.data.recipes.ShapelessRecipeBuilder;
import net.minecraft.data.recipes.SimpleCookingRecipeBuilder;
import net.minecraft.data.recipes.SingleItemRecipeBuilder;
import net.minecraft.data.recipes.SmithingTransformRecipeBuilder;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.Tags;
import net.minecraftforge.registries.ForgeRegistries;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.base.registry.RegisteredItem;
import net.paradisemod.building.Building;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.misc.Armor;
import net.paradisemod.misc.Misc;
import net.paradisemod.misc.Tools;
import net.paradisemod.world.Ores;

public class RecipeGenerator extends RecipeProvider {
    public RecipeGenerator(PackOutput output) {
        super(output);
    }

    @Override
    protected void buildRecipes(Consumer<FinishedRecipe> output) {
        // build the recipes for all the blocks
        var numblocks = 0;
        for(var block : RegisteredBlock.ALL_BLOCKS) {
            if(block.hasRecipes()) numblocks++;

            try {
                block.genRecipes(this, output);
            }
            catch (Exception e) {
                ParadiseMod.LOG.error("Error generating recipes for " + block.shortName() + ": " + e.getMessage());
                ParadiseMod.LOG.error("Stack trace:", e);
            }
        }

        ParadiseMod.LOG.debug("Finished generating recipes for " + numblocks + " blocks");

        // build the recipes for all the items
        var numitems = 0;
        for(var item : RegisteredItem.ALL_ITEMS) {
            if(item.hasRecipes()) numitems++;

            try {
                item.genRecipes(this, output);
            }
            catch (Exception e) {
                ParadiseMod.LOG.error("Error generating recipes for " + item.shortName() + ": " + e.getMessage());
                ParadiseMod.LOG.error("Stack trace:", e);
            }
        }

        ParadiseMod.LOG.debug("Finished generating recipes for " + numitems + " items");

        // create a colored rose -> colored dye recipe for every single rose
        for(var entry : Decoration.ROSES.entrySet()) {
            var color = entry.getKey();
            var rose = entry.getValue();

            shapelessRecipe(RecipeCategory.MISC, ParadiseMod.DYES_BY_COLOR.get(color), rose)
                .save(output, "paradisemod:" + color.getName() + "_dye_from_rose");
        }

        // why can't 9 nether wart come from a nether wart block?
        shapelessRecipe(RecipeCategory.DECORATIONS, Items.NETHER_WART, 9, Blocks.NETHER_WART_BLOCK)
            .save(output, "paradisemod:nether_wart_from_nether_wart_block");

        // why can't 4 quartz come from a quartz block?
        shapelessRecipe(RecipeCategory.DECORATIONS, Items.QUARTZ, 4, Blocks.QUARTZ_BLOCK)
            .save(output, "paradisemod:quartz_from_quartz_block");

        // the notch apple recipe is back!
        getShapedBuilder(RecipeCategory.MISC, Items.ENCHANTED_GOLDEN_APPLE)
            .pattern("GGG")
            .pattern("GAG")
            .pattern("GGG")
            .define('G', Blocks.GOLD_BLOCK)
            .define('A', Items.APPLE)
            .save(output, "paradisemod:notch_apple");

        // the crafting table is only craftable from oak planks
        getShapedBuilder(RecipeCategory.MISC, Blocks.CRAFTING_TABLE)
            .pattern("pp")
            .pattern("pp")
            .define('p', Blocks.OAK_PLANKS)
            .save(output);

        getShapedBuilder(RecipeCategory.BUILDING_BLOCKS, Blocks.END_STONE_BRICKS)
            .pattern("ss")
            .pattern("ss")
            .define('s', Building.POLISHED_END_STONE)
            .save(output);

        stonecutterRecipe(Building.POLISHED_END_STONE, Blocks.END_STONE_BRICKS)
            .save(output, "paradisemod:stonecutter/end_stone_bricks");

        getShapedBuilder(RecipeCategory.REDSTONE, Blocks.LEVER)
            .pattern("s")
            .pattern("S")
            .define('S', Blocks.COBBLESTONE)
            .define('s', Items.STICK)
            .save(output);

        getShapedBuilder(RecipeCategory.REDSTONE, Blocks.IRON_DOOR, 3)
            .pattern("##")
            .pattern("##")
            .pattern("##")
            .define('#', Tags.Items.INGOTS_IRON)
            .group("metal_doors")
            .save(output);

        getShapedBuilder(RecipeCategory.REDSTONE, Blocks.IRON_TRAPDOOR)
            .pattern("##")
            .pattern("##")
            .define('#', Tags.Items.INGOTS_IRON)
            .group("metal_trapdoors")
            .save(output);
        
        SimpleCookingRecipeBuilder.smelting(Ingredient.of(PMTags.Items.SILVER_RECYCLABLES), RecipeCategory.MISC, Misc.SILVER_NUGGET, 0.1F, 200)
            .unlockedBy("impossible", new ImpossibleTrigger.TriggerInstance())
            .save(output, "paradisemod:smelting/silver_nugget_from_silver");

        SimpleCookingRecipeBuilder.blasting(Ingredient.of(PMTags.Items.SILVER_RECYCLABLES), RecipeCategory.MISC, Misc.SILVER_NUGGET, 0.1F, 100)
            .unlockedBy("impossible", new ImpossibleTrigger.TriggerInstance())
            .save(output, "paradisemod:blasting/silver_nugget_from_silver");

        furnaceRecipe(Building.GOLD_GLOWING_OBSIDIAN, Blocks.GOLD_BLOCK, RecipeCategory.MISC)
            .save(output, "paradisemod:smelting/gold_from_glowing_obsidian");

        furnaceRecipe(Building.SILVER_GLOWING_OBSIDIAN, Ores.SILVER_BLOCK.get(), RecipeCategory.MISC)
            .save(output, "paradisemod:smelting/silver_from_glowing_obsidian");

        furnaceRecipe(Building.IRON_GLOWING_OBSIDIAN, Blocks.IRON_BLOCK, RecipeCategory.MISC)
            .save(output, "paradisemod:smelting/iron_from_glowing_obsidian");

        furnaceRecipe(Building.COPPER_GLOWING_OBSIDIAN, Blocks.COPPER_BLOCK, RecipeCategory.MISC)
            .save(output, "paradisemod:smelting/copper_from_glowing_obsidian");

        furnaceRecipe(Ores.DARKSTONE_COAL_ORE, Items.COAL, RecipeCategory.MISC)
            .save(output, "paradisemod:smelting/coal_from_darkstone");

        furnaceRecipe(Ores.DARKSTONE_IRON_ORE, Items.IRON_INGOT, RecipeCategory.MISC)
            .save(output, "paradisemod:smelting/iron_from_darkstone");

        furnaceRecipe(Ores.DARKSTONE_GOLD_ORE, Items.GOLD_INGOT, RecipeCategory.MISC)
            .save(output, "paradisemod:smelting/gold_from_darkstone");

        furnaceRecipe(Ores.DARKSTONE_SILVER_ORE, Misc.SILVER_INGOT, RecipeCategory.MISC)
            .save(output, "paradisemod:smelting/silver_from_darkstone");

        blastingRecipe(Ores.DARKSTONE_COAL_ORE, Items.COAL, RecipeCategory.MISC)
            .save(output, "paradisemod:blasting/coal_from_darkstone");

        blastingRecipe(Ores.DARKSTONE_IRON_ORE, Items.IRON_INGOT, RecipeCategory.MISC)
            .save(output, "paradisemod:blasting/iron_from_darkstone");

        blastingRecipe(Ores.DARKSTONE_GOLD_ORE, Items.GOLD_INGOT, RecipeCategory.MISC)
            .save(output, "paradisemod:blasting/gold_from_darkstone");

        blastingRecipe(Ores.DARKSTONE_SILVER_ORE, Misc.SILVER_INGOT, RecipeCategory.MISC)
            .save(output, "paradisemod:blasting/silver_from_darkstone");

        buildNetheriteFromEmeraldRubySmithingRecipe(output, Tools.EMERALD_AXE, Tools.RUBY_AXE, Items.NETHERITE_AXE);
        buildNetheriteFromEmeraldRubySmithingRecipe(output, Tools.EMERALD_HOE, Tools.RUBY_HOE, Items.NETHERITE_HOE);
        buildNetheriteFromEmeraldRubySmithingRecipe(output, Tools.EMERALD_SHOVEL, Tools.RUBY_SHOVEL, Items.NETHERITE_SHOVEL);
        buildNetheriteFromEmeraldRubySmithingRecipe(output, Tools.EMERALD_PICKAXE, Tools.RUBY_PICKAXE, Items.NETHERITE_PICKAXE);
        buildNetheriteFromEmeraldRubySmithingRecipe(output, Tools.EMERALD_SWORD, Tools.RUBY_SWORD, Items.NETHERITE_SWORD);
        buildNetheriteFromEmeraldRubySmithingRecipe(output, Armor.EMERALD_HELMET, Armor.RUBY_HELMET, Items.NETHERITE_HELMET);
        buildNetheriteFromEmeraldRubySmithingRecipe(output, Armor.EMERALD_CHESTPLATE, Armor.RUBY_CHESTPLATE, Items.NETHERITE_CHESTPLATE);
        buildNetheriteFromEmeraldRubySmithingRecipe(output, Armor.EMERALD_LEGGINGS, Armor.RUBY_LEGGINGS, Items.NETHERITE_LEGGINGS);
        buildNetheriteFromEmeraldRubySmithingRecipe(output, Armor.EMERALD_BOOTS, Armor.RUBY_BOOTS, Items.NETHERITE_BOOTS);
    }

    @Override
    public CompletableFuture<?> run(CachedOutput output) {
        return CompletableFuture.allOf(super.run(output), this.buildAdvancement(output, new ResourceLocation("paradisemod:recipes/root"), Advancement.Builder.recipeAdvancement().addCriterion("impossible", new ImpossibleTrigger.TriggerInstance())));
    }

    public ShapedRecipeBuilder getShapedBuilder(RecipeCategory category, ItemLike result, int count) {
        return ShapedRecipeBuilder.shaped(category, result, count)
            .unlockedBy("impossible", new ImpossibleTrigger.TriggerInstance());
    }

    public ShapedRecipeBuilder getShapedBuilder(RecipeCategory category, ItemLike result) {
        return getShapedBuilder(category, result, 1);
    }

    public ShapedRecipeBuilder getShapedBuilder(RecipeCategory category, ItemLike result, String wholePattern) {
        var builder = getShapedBuilder(category, result);

        for(var pattern : wholePattern.lines().toList())
            builder = builder.pattern(pattern);

        return builder;
    }

    public ShapelessRecipeBuilder shapelessRecipe(RecipeCategory category, ItemLike result, int count, ItemLike... inputItems) {
        var builder = ShapelessRecipeBuilder.shapeless(category, result, count);
        for(var item : inputItems)
            builder = builder.requires(Ingredient.of(item));

        return builder.unlockedBy("impossible", new ImpossibleTrigger.TriggerInstance());
    }

    public ShapelessRecipeBuilder shapelessRecipe(RecipeCategory category, ItemLike result, ItemLike... inputItems) {
        return shapelessRecipe(category, result, 1, inputItems);
    }

    public SingleItemRecipeBuilder stonecutterRecipe(ItemLike input, ItemLike result, int count) {
        return SingleItemRecipeBuilder.stonecutting(Ingredient.of(input), RecipeCategory.BUILDING_BLOCKS, result, count)
            .unlockedBy("impossible", new ImpossibleTrigger.TriggerInstance());
    }

    public SingleItemRecipeBuilder stonecutterRecipe(ItemLike input, ItemLike result) {
        return stonecutterRecipe(input, result, 1);
    }

    public SimpleCookingRecipeBuilder campfireRecipe(ItemLike input, ItemLike result, RecipeCategory category, float exp, int cookTime) {
        return SimpleCookingRecipeBuilder.campfireCooking(Ingredient.of(input), category, result, exp, cookTime)
            .unlockedBy("impossible", new ImpossibleTrigger.TriggerInstance());
    }

    public SimpleCookingRecipeBuilder campfireRecipe(ItemLike input, ItemLike result, RecipeCategory category) {
        return campfireRecipe(input, result, category, 0.1F, 200);
    }

    public SimpleCookingRecipeBuilder blastingRecipe(ItemLike input, ItemLike result, RecipeCategory category, float exp, int cookTime) {
        return SimpleCookingRecipeBuilder.blasting(Ingredient.of(input), category, result, exp, cookTime)
            .unlockedBy("impossible", new ImpossibleTrigger.TriggerInstance());
    }

    public SimpleCookingRecipeBuilder blastingRecipe(ItemLike inputItem, ItemLike resultItem, RecipeCategory category) {
        return blastingRecipe(inputItem, resultItem, category, 0.1F, 100);
    }

    public SimpleCookingRecipeBuilder smokingRecipe(ItemLike inputItem, ItemLike resultItem, RecipeCategory category, float exp, int cookTime) {
        return SimpleCookingRecipeBuilder.smoking(Ingredient.of(inputItem), category, resultItem, exp, cookTime)
            .unlockedBy("impossible", new ImpossibleTrigger.TriggerInstance());
    }

    public SimpleCookingRecipeBuilder smokingRecipe(ItemLike inputItem, ItemLike resultItem, RecipeCategory category) {
        return smokingRecipe(inputItem, resultItem, category, 0.1F, 200);
    }

    public SimpleCookingRecipeBuilder furnaceRecipe(ItemLike inputItem, ItemLike resultItem, RecipeCategory category, float exp, int cookTime) {
        return SimpleCookingRecipeBuilder.smelting(Ingredient.of(inputItem), category, resultItem, exp, cookTime)
            .unlockedBy("impossible", new ImpossibleTrigger.TriggerInstance());
    }

    public SimpleCookingRecipeBuilder furnaceRecipe(ItemLike inputItem, ItemLike resultItem, RecipeCategory category) {
        return furnaceRecipe(inputItem, resultItem, category, 0.1F, 200);
    }

    public SmithingTransformRecipeBuilder smithingRecipe(ItemLike base, ItemLike addition, ItemLike template, ItemLike result, RecipeCategory category) {
        return SmithingTransformRecipeBuilder.smithing(Ingredient.of(template), Ingredient.of(base), Ingredient.of(addition), category, result.asItem())
            .unlocks("impossible", new ImpossibleTrigger.TriggerInstance());
    }

    private void buildNetheriteFromEmeraldRubySmithingRecipe(Consumer<FinishedRecipe> output, ItemLike emeraldTool, ItemLike rubyTool, Item netheriteTool) {
        var name = ForgeRegistries.ITEMS.getKey(netheriteTool.asItem()).getPath();

        SmithingTransformRecipeBuilder.smithing(
            Ingredient.of(Items.NETHERITE_UPGRADE_SMITHING_TEMPLATE),
            Ingredient.of(emeraldTool, rubyTool),
            Ingredient.of(Items.NETHERITE_INGOT),
            name.endsWith("sword") ? RecipeCategory.COMBAT : RecipeCategory.TOOLS,
            netheriteTool
        )
            .unlocks("impossible", new ImpossibleTrigger.TriggerInstance())
            .save(output, "paradisemod:smithing/" + name + "_from_emerald_and_ruby");
    }
}