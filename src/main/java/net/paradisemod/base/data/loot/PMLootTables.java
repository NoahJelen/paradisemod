package net.paradisemod.base.data.loot;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import net.minecraft.data.CachedOutput;
import net.minecraft.data.PackOutput;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.paradisemod.base.Utils;

public class PMLootTables extends LootTableProvider {
    public PMLootTables(PackOutput output) {
        super(
            output, 
            Collections.emptySet(),
            List.of(
                new SubProviderEntry(BlockLootGenerator::new, LootContextParamSets.BLOCK),
                new SubProviderEntry(ChestLootGenerator::new, LootContextParamSets.CHEST),
                new SubProviderEntry(EntityLootGenerator::new, LootContextParamSets.ENTITY)
            )
        );
    }

    public CompletableFuture<?> run(CachedOutput output) {
        return Utils.handlePossibleException(() -> super.run(output));
    }
}