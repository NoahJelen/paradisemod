package net.paradisemod.base.data.loot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.function.Function;

import net.minecraft.data.loot.BlockLootSubProvider;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.functions.ApplyBonusCount;
import net.minecraft.world.level.storage.loot.functions.FunctionUserBuilder;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.predicates.BonusLevelTableCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.misc.Misc;

public class BlockLootGenerator extends BlockLootSubProvider {
    private final ArrayList<Block> blocks = new ArrayList<>();

    protected BlockLootGenerator() {
        super(Collections.emptySet(), FeatureFlags.REGISTRY.allFlags());
    }

    @Override
    protected void generate() {
        var numblocks = 0;
        for(var block : RegisteredBlock.ALL_BLOCKS) {
            if(block.hasLootTable()) numblocks++;

            try {
                block.genLootTable(this);
            }
            catch (Exception e) {
                ParadiseMod.LOG.error("Error generating block loot table for " + block.shortName() + ": " + e.getMessage());
                ParadiseMod.LOG.error("Stack trace:", e);
            }
        }

        ParadiseMod.LOG.debug("Finished generating block loot tables for " + numblocks + " blocks");

        add(
            Blocks.OAK_LEAVES,
            block -> createLeavesDrops(
                block,
                Blocks.OAK_SAPLING,
                0.05F, 0.0625F, 0.083333336F, 0.1F
            )
                .withPool(
                    LootPool.lootPool()
                        .setRolls(ConstantValue.exactly(1.0F))
                        .when(HAS_SHEARS.or(HAS_SILK_TOUCH).invert())
                        .add(
                            applyExplosionCondition(block, LootItem.lootTableItem(Items.APPLE))
                                .when(BonusLevelTableCondition.bonusLevelFlatChance(Enchantments.BLOCK_FORTUNE, 0.005F, 0.0055555557F, 0.00625F, 0.008333334F, 0.025F))
                        )
                )
                .withPool(
                    LootPool.lootPool()
                        .setRolls(ConstantValue.exactly(1.0F))
                        .when(HAS_SHEARS.or(HAS_SILK_TOUCH).invert())
                        .add(
                            applyExplosionCondition(block, LootItem.lootTableItem(Misc.ACORN))
                                .when(BonusLevelTableCondition.bonusLevelFlatChance(Enchantments.BLOCK_FORTUNE, 0.005F, 0.0055555557F, 0.00625F, 0.008333334F, 0.025F))
                        )
                )
        );

        add(
            Blocks.DARK_OAK_LEAVES,
            block -> createLeavesDrops(
                block,
                Blocks.DARK_OAK_SAPLING,
                0.05F, 0.0625F, 0.083333336F, 0.1F
            )
                .withPool(
                    LootPool.lootPool()
                        .setRolls(ConstantValue.exactly(1.0F))
                        .when(HAS_SHEARS.or(HAS_SILK_TOUCH).invert())
                        .add(
                            applyExplosionCondition(block, LootItem.lootTableItem(Items.APPLE))
                                .when(BonusLevelTableCondition.bonusLevelFlatChance(Enchantments.BLOCK_FORTUNE, 0.005F, 0.0055555557F, 0.00625F, 0.008333334F, 0.025F))
                        )
                )
                .withPool(
                    LootPool.lootPool()
                        .setRolls(ConstantValue.exactly(1.0F))
                        .when(HAS_SHEARS.or(HAS_SILK_TOUCH).invert())
                        .add(
                            applyExplosionCondition(block, LootItem.lootTableItem(Misc.ACORN))
                                .when(BonusLevelTableCondition.bonusLevelFlatChance(Enchantments.BLOCK_FORTUNE, 0.005F, 0.0055555557F, 0.00625F, 0.008333334F, 0.025F))
                        )
                )
        );

        add(
            Blocks.CHERRY_LEAVES,
            block -> createLeavesDrops(
                block,
                Blocks.CHERRY_SAPLING,
                0.05F, 0.0625F, 0.083333336F, 0.1F
            )
                .withPool(
                    LootPool.lootPool()
                        .setRolls(ConstantValue.exactly(1.0F))
                        .when(HAS_SHEARS.or(HAS_SILK_TOUCH).invert())
                        .add(
                            applyExplosionCondition(block, LootItem.lootTableItem(Misc.CHERRY))
                                .when(BonusLevelTableCondition.bonusLevelFlatChance(Enchantments.BLOCK_FORTUNE, 0.005F, 0.0055555557F, 0.00625F, 0.008333334F, 0.025F))
                        )
                )
        );
    }

    @Override
    public void dropSelf(Block block) {
        dropOther(block, block);
    }

    @Override
    public void dropWhenSilkTouch(Block block) {
        blocks.add(block);
        otherWhenSilkTouch(block, block);
    }

    @Override
    public void dropOther(Block block, ItemLike item) {
        blocks.add(block);
        add(block, createSingleItemTable(item));
    }

    @Override
    public void dropPottedContents(Block pot) {
        add(pot, block -> createPotFlowerItemTable(((FlowerPotBlock) block).getContent()));
    }

    public void leavesDrop(Block leaves, Block sapling) {
        add(
            leaves,
            block -> createLeavesDrops(block, sapling, NORMAL_LEAVES_SAPLING_CHANCES)
        );
    }

    public void doorDrop(Block door) {
        add(
            door,
            block -> createDoorTable(block)
        );
    }

    public void slabDrop(Block slab) {
        add(
            slab,
            block -> createSlabItemTable(block)
        );
    }

    public void randomItemDrop(Block block, ItemLike item, int minDrops, int maxDrops) {
        add(
            block,
            blockIn ->
                createSilkTouchDispatchTable(
                    blockIn,
                    applyExplosionDecay(
                        blockIn,
                        LootItem.lootTableItem(item)
                            .apply(
                                SetItemCountFunction.setCount(UniformGenerator.between(minDrops, maxDrops))
                            )
                            .apply(ApplyBonusCount.addUniformBonusCount(Enchantments.BLOCK_FORTUNE))
                    )
                )
        );
    }

    public void multipleItemDrop(Block block, ItemLike item, int count) {
        add(
            block,
            blockIn ->
                createSilkTouchDispatchTable(
                    blockIn,
                    applyExplosionDecay(
                        blockIn,
                        LootItem.lootTableItem(item)
                            .apply(
                                SetItemCountFunction.setCount(ConstantValue.exactly(count))
                            )
                            .apply(ApplyBonusCount.addUniformBonusCount(Enchantments.BLOCK_FORTUNE))
                    )
                )
        );
    }

    public void randomItemDrop(Block block, ItemLike item, int maxDrops) { randomItemDrop(block, item, 1, maxDrops); }

    public void dropUnlessSilkTouch(Block block, ItemLike drop) {
        add(
            block,
            blockIn -> createSingleItemTableWithSilkTouch(blockIn, drop)
        );
    }

    public static LootItemCondition.Builder hasSilkTouch() {
        return HAS_SILK_TOUCH;
    }

    @Override
    public <T extends FunctionUserBuilder<T>> T applyExplosionDecay(ItemLike item, FunctionUserBuilder<T> functionBuilder) {
        return super.applyExplosionDecay(item, functionBuilder);
    }

    @Override
    public void add(Block block, Function<Block, LootTable.Builder> factory) {
        blocks.add(block);
        super.add(block, factory);
    }

    @Override
    protected ArrayList<Block> getKnownBlocks() {
        return blocks;
    }
}