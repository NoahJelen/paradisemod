package net.paradisemod.base.data.loot;

import java.util.function.BiConsumer;

import net.minecraft.data.loot.LootTableSubProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.alchemy.Potions;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.LootTable.Builder;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.entries.LootPoolSingletonContainer;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.functions.SetPotionFunction;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;
import net.paradisemod.ParadiseMod;
import net.paradisemod.building.Doors;
import net.paradisemod.misc.Misc;
import net.paradisemod.misc.Tools;
import net.paradisemod.world.Ores;

public class ChestLootGenerator implements LootTableSubProvider {
    @Override
    public void generate(BiConsumer<ResourceLocation, Builder> output) {
        output.accept(
            new ResourceLocation("minecraft:chests/buried_treasure"),
            LootTable.lootTable()
                .withPool(
                    LootPool.lootPool()
                        .setRolls(ConstantValue.exactly(1))
                        .add(LootItem.lootTableItem(Items.HEART_OF_THE_SEA))
                )
                .withPool(
                    LootPool.lootPool()
                        .setRolls(UniformGenerator.between(5, 8))
                        .add(
                            weightedItem(Items.IRON_INGOT, 20)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(1, 4)))
                        )
                        .add(
                            weightedItem(Items.GOLD_INGOT, 10)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(1, 4)))
                        )
                        .add(
                            weightedItem(Blocks.TNT, 5)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(1, 2)))
                        )
                )
                .withPool(
                    LootPool.lootPool()
                        .setRolls(UniformGenerator.between(1, 3))
                        .add(
                            weightedItem(Items.EMERALD, 5)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(4, 8)))
                        )
                        .add(
                            weightedItem(Items.DIAMOND, 5)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(1, 2)))
                        )
                        .add(
                            weightedItem(Items.PRISMARINE_CRYSTALS, 5)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(1, 5)))
                        )
                )
                .withPool(
                    LootPool.lootPool()
                        .setRolls(UniformGenerator.between(0, 1))
                        .add(LootItem.lootTableItem(Items.LEATHER_CHESTPLATE))
                        .add(LootItem.lootTableItem(Items.IRON_SWORD))
                )
                .withPool(
                    LootPool.lootPool()
                        .setRolls(ConstantValue.exactly(2))
                        .add(
                            LootItem.lootTableItem(Items.COOKED_COD)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(2, 4)))
                        )
                        .add(
                            LootItem.lootTableItem(Items.COOKED_SALMON)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(2, 4)))
                        )
                )
                .withPool(
                    LootPool.lootPool()
                        .setRolls(UniformGenerator.between(0, 2))
                        .add(LootItem.lootTableItem(Items.POTION))
                        .apply(SetPotionFunction.setPotion(Potions.WATER_BREATHING))
                )

                .withPool(
                    LootPool.lootPool()
                        .setRolls(UniformGenerator.between(1, 128))
                        .setBonusRolls(UniformGenerator.between(0, 256))
                        .add(
                            weightedItem(Items.DIAMOND, 4)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(1, 32)))
                        )
                        .add(
                            weightedItem(Items.IRON_INGOT, 8)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(1, 32)))
                        )
                        .add(
                            weightedItem(Items.GOLD_INGOT, 7)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(1, 32)))
                        )
                        .add(
                            weightedItem(Items.REDSTONE, 5)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(1, 32)))
                        )
                        .add(
                            weightedItem(Items.EMERALD, 2)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(1, 32)))
                        )
                        .add(
                            weightedItem(Misc.RUBY, 3)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(1, 32)))
                        )
                        .add(
                            weightedItem(Misc.SILVER_INGOT, 6)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(1, 32)))
                        )
                        .add(
                            weightedItem(Blocks.OBSIDIAN, 6)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(1, 32)))
                        )
                )
        );

        modChestTable(
            "mining_chest",
            output,
            64,
            weightedItem(Items.WOODEN_PICKAXE, 5),
            weightedItem(Items.STONE_PICKAXE, 4),
            weightedItem(Items.IRON_PICKAXE, 3),
            weightedItem(Items.DIAMOND_PICKAXE, 2),
            weightedItem(Items.NETHERITE_PICKAXE, 2),
            weightedItem(Tools.RUBY_PICKAXE, 1),
            weightedItem(Tools.EMERALD_PICKAXE, 1),
            weightedItem(Tools.OBSIDIAN_PICKAXE, 1),
            weightedItem(Tools.SILVER_PICKAXE, 1),
            weightedItem(Items.WOODEN_SHOVEL, 5),
            weightedItem(Items.STONE_SHOVEL, 4),
            weightedItem(Items.IRON_SHOVEL, 3),
            weightedItem(Items.DIAMOND_SHOVEL, 2),
            weightedItem(Items.NETHERITE_SHOVEL, 2),
            weightedItem(Tools.RUBY_SHOVEL, 1),
            weightedItem(Tools.EMERALD_SHOVEL, 1),
            weightedItem(Tools.OBSIDIAN_SHOVEL, 1),
            weightedItem(Tools.SILVER_SHOVEL, 1),
            weightedItem(Items.DIAMOND, 1),
            weightedItem(Items.IRON_INGOT, 3),
            weightedItem(Items.GOLD_INGOT, 2),
            weightedItem(Items.REDSTONE, 3),
            weightedItem(Items.EMERALD, 1),
            weightedItem(Misc.RUBY, 1),
            weightedItem(Misc.SILVER_INGOT, 3),
            weightedItem(Blocks.COBBLESTONE, 6),
            weightedItem(Blocks.GRAVEL, 6),
            weightedItem(Blocks.DIRT, 6),
            weightedItem(Blocks.IRON_ORE, 4),
            weightedItem(Blocks.GOLD_ORE, 4),
            weightedItem(Ores.SILVER_ORE, 2)
        );

        modChestTable(
            "starter_chest",
            output,
            10,
            weightedItem(Items.WOODEN_PICKAXE, 5),
            weightedItem(Items.WOODEN_SHOVEL, 5),
            weightedItem(Items.WOODEN_AXE, 5),
            weightedItem(Items.WOODEN_HOE, 5),
            weightedItem(Items.WOODEN_SWORD, 5),
            weightedItem(Items.APPLE, 5),
            weightedItem(Items.WHEAT, 5),
            weightedItem(Items.COD, 5),
            weightedItem(Items.SALMON, 5),
            weightedItem(Items.BEEF, 5),
            weightedItem(Items.CHICKEN, 5),
            weightedItem(Items.PORKCHOP, 5),
            weightedItem(Items.RABBIT, 5),
            weightedItem(Items.MUTTON, 5),
            weightedItem(Blocks.OAK_PLANKS, 5),
            weightedItem(Blocks.OAK_SAPLING, 5),
            weightedItem(Blocks.COBBLESTONE, 6),
            weightedItem(Blocks.SAND, 6),
            weightedItem(Blocks.DIRT, 6),
            weightedItem(Items.STICK, 5),
            weightedItem(Blocks.OAK_DOOR, 5),
            weightedItem(Blocks.BIRCH_DOOR, 5),
            weightedItem(Blocks.ACACIA_DOOR, 5),
            weightedItem(Blocks.SPRUCE_DOOR, 5),
            weightedItem(Blocks.JUNGLE_DOOR, 5),
            weightedItem(Blocks.DARK_OAK_DOOR, 5),
            weightedItem(Doors.CACTUS_DOOR, 5),
            weightedItem(Doors.PALO_VERDE_DOOR, 5),
            weightedItem(Doors.MESQUITE_DOOR, 5),
            weightedItem(Blocks.BAMBOO_DOOR, 5)
        );
    }

    private static void modChestTable(String name, BiConsumer<ResourceLocation, Builder> output, int maxRolls, LootPoolSingletonContainer.Builder<?>... items) {
        var table = LootTable.lootTable();
        var pool = LootPool.lootPool()
            .name(name)
            .setRolls(UniformGenerator.between(1, maxRolls))
            .setBonusRolls(UniformGenerator.between(0, 6));

        for(var item : items)
            pool = pool.add(item);

        output.accept(
            new ResourceLocation(ParadiseMod.ID, "chests/" + name), 
            table.withPool(pool)
        );
    }

    private static LootPoolSingletonContainer.Builder<?> weightedItem(ItemLike item, int weight) {
        return LootItem.lootTableItem(item)
            .setWeight(weight);
    }
}