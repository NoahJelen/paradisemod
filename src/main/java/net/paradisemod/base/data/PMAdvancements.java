package net.paradisemod.base.data;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.FrameType;
import net.minecraft.advancements.RequirementsStrategy;
import net.minecraft.advancements.critereon.ChangeDimensionTrigger;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.InventoryChangeTrigger;
import net.minecraft.advancements.critereon.KilledTrigger;
import net.minecraft.advancements.critereon.LocationPredicate;
import net.minecraft.advancements.critereon.NbtPredicate;
import net.minecraft.advancements.critereon.PlayerTrigger;
import net.minecraft.advancements.critereon.SummonedEntityTrigger;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.data.PackOutput;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.common.data.ForgeAdvancementProvider;
import net.paradisemod.base.data.assets.PMTranslations;
import net.paradisemod.building.Building;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.misc.Tools;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.PMWorld;
import net.paradisemod.world.biome.PMBiomes;
import net.paradisemod.world.dimension.PMDimensions;

public class PMAdvancements extends ForgeAdvancementProvider {
    //TODO: when all the monsters are created, create a separate advancement tree for defeating them
    public PMAdvancements(PackOutput output, CompletableFuture<Provider> registries, ExistingFileHelper existingFileHelper) {
        super(output, registries, existingFileHelper, List.of(PMAdvancements::buildAdvancements));
    }

    public static void translations(PMTranslations translator, boolean spanish) {
        translator.add(
            advancementDescKey("root"),
            spanish ? "Minecraft puede ser lo que quieres" : "Minecraft can be whatever you want it to be!"
        );

        translator.add(
            advancementKey("going_even_deeper"),
            spanish ? "¡Necesitamos viajar aún más profundo!" : "We need to go even deeper!"
        );

        translator.add(
            advancementDescKey("going_even_deeper"),
            spanish ? "Viajar al Centro del sobremundo" : "Travel to the Overworld Core"
        );

        translator.add(
            advancementKey("deep_dark"),
            spanish ? "La dimensión de oscuridad total" : "The Pitch Black Dimension"
        );

        translator.add(
            advancementDescKey("deep_dark"),
            spanish ? "Viajar al Oscuro profundo" : "Travel to the Deep Dark"
        );

        translator.add(
            advancementKey("elysium"),
            spanish ? "El sueño de Notch" : "Notch's Dream"
        );

        translator.add(
            advancementDescKey("elysium"),
            spanish ? "Viajar a Elíseo" : "Travel to Elysium"
        );

        translator.add(
            advancementKey("regen_stone"),
            spanish ? "¡Soy inmortal!" : "I'm immortal!!"
        );

        translator.add(
            advancementDescKey("regen_stone"),
            spanish ? "Minar una piedra de regeneración" : "Mine a Regeneration Stone"
        );

        translator.add(
            advancementKey("killer_rabbit"),
            spanish ? "¿Es la bestia? ¡Solamente es un conejo!" : "That's the beast? 'Tis but a rabbit!"
        );

        translator.add(
            advancementDescKey("killer_rabbit"),
            spanish ? "Matar un conejo mortal" : "Kill a Killer Bunny"
        );

        translator.add(
            advancementKey("spawn_giant"),
            spanish ? "¡Ay caramba! ¡¿Qué es éso?!" : "WHAT THE HECK IS THAT?!"
        );

        translator.add(
            advancementDescKey("spawn_giant"),
            spanish ? "Generar un gigante" : "Summon a Giant"
        );

        translator.add(
            advancementKey("rose_field"),
            spanish ? "¿Por qué no pueden flores gustar a un hombre?" : "Why can't a dude like flowers?"
        );

        translator.add(
            advancementDescKey("rose_field"),
            spanish ? "Encontar un campo de rosas" : "Find a Rose Field biome"
        );

        translator.add(
            advancementKey("seasons"),
            spanish ? "Cuando las estaciones se cambian" :  "When the Seasons Change"
        );

        translator.add(
            advancementDescKey("seasons"),
            spanish ? "Encontrar el bioma de bosque de otoño" : "Find The Autumn Forest biome"
        );

        translator.add(
            advancementKey("arizona"),
            spanish ? "¡Bienvenidos a Arizona!" : "Welcome to Arizona!"
        );

        translator.add(
            advancementDescKey("arizona"),
            spanish ? "Encontrar un bioma de desierto rocoso" : "Find a Rocky Desert biome"
        );

        translator.add(
            advancementKey("rusted_weapon"),
            spanish ? "Un ataque oxidado" : "A Rusted Attack"
        );

        translator.add(
            advancementDescKey("rusted_weapon"),
            spanish ? "Construir una espada de hierro oxidado" : "Craft a Rusted Iron Sword"
        );

        translator.add(
            advancementKey("redstone_sword"),
            spanish ? "Una espada de fuego" : "A Fiery Sword"
        );

        translator.add(
            advancementDescKey("redstone_sword"),
            spanish ? "Construir una espada de piedra roja" : "Craft a Redstone Sword"
        );

        translator.add(
            advancementKey("the_origin"),
            spanish ? "El principio de tiempo" : "The Beginning of Time"
        );

        translator.add(
            advancementDescKey("the_origin"),
            spanish ? "Encontrar el bioma del origen" : "Find The Origin biome"
        );

        translator.add(
            advancementKey("prison_breakout"),
            spanish ? "La fuga de cárcel" : "Prison Breakout"
        );

        translator.add(
            advancementDescKey("prison_breakout"),
            spanish ? "Matar el monstruo de warden" : "Kill the Warden"
        );
    }

    private static void buildAdvancements(HolderLookup.Provider registries, Consumer<Advancement> saver, ExistingFileHelper existingFileHelper) {
        var root = Advancement.Builder.advancement()
            .display(
                Building.RED_GLOWING_OBSIDIAN,
                Component.translatable("paradisemod.name"),
                Component.translatable(advancementDescKey("root")),
                new ResourceLocation("paradisemod:textures/block/blaze_block.png"),
                FrameType.TASK,
                false, false, false
            )
            .addCriterion(
                "changed_dimension",
                ChangeDimensionTrigger.TriggerInstance.changedDimension(
                    Level.OVERWORLD,
                    PMDimensions.Type.OVERWORLD_CORE.getKey()
                )
            )
            .save(saver, "paradisemod:paradisemod/root");

        var goingEvenDeeper = Advancement.Builder.advancement()
            .display(
                PMWorld.OVERWORLD_CORE_PORTAL,
                Component.translatable(advancementKey("going_even_deeper")),
                Component.translatable(advancementDescKey("going_even_deeper")),
                null, FrameType.TASK,
                true, true, false
            )
            .addCriterion(
                "changed_dimension",
                ChangeDimensionTrigger.TriggerInstance.changedDimensionTo(
                    PMDimensions.Type.OVERWORLD_CORE.getKey()
                )
            )
            .parent(root)
            .save(saver, "paradisemod:paradisemod/going_even_deeper");

        var deepDark = Advancement.Builder.advancement()
            .display(
                DeepDarkBlocks.DARKSTONE,
                Component.translatable(advancementKey("deep_dark")),
                Component.translatable(advancementDescKey("deep_dark")),
                null, FrameType.TASK,
                true, true, false
            )
            .addCriterion(
                "changed_dimension",
                ChangeDimensionTrigger.TriggerInstance.changedDimensionTo(
                    PMDimensions.Type.DEEP_DARK.getKey()
                )
            )
            .parent(goingEvenDeeper)
            .save(saver, "paradisemod:paradisemod/deep_dark");

        Advancement.Builder.advancement()
            .display(
                PMWorld.ELYSIUM_PORTAL,
                Component.translatable(advancementKey("elysium")),
                Component.translatable(advancementDescKey("elysium")),
                null, FrameType.TASK,
                true, true, false
            )
            .addCriterion(
                "changed_dimension",
                ChangeDimensionTrigger.TriggerInstance.changedDimensionTo(
                    PMDimensions.Type.ELYSIUM.getKey()
                )
            )
            .parent(deepDark)
            .save(saver, "paradisemod:paradisemod/elysium");

        Advancement.Builder.advancement()
            .display(
                DeepDarkBlocks.REGENERATION_STONE,
                Component.translatable(advancementKey("regen_stone")),
                Component.translatable(advancementDescKey("regen_stone")),
                null, FrameType.TASK,
                true, true, false
            )
            .addCriterion(
                "regen_stone",
                InventoryChangeTrigger.TriggerInstance.hasItems(DeepDarkBlocks.REGENERATION_STONE)
            )
            .parent(deepDark)
            .save(saver, "paradisemod:paradisemod/regen_stone");

        var rabbitTag = new CompoundTag();
        rabbitTag.putInt("RabbitType", 99);

        var killerRabbit = Advancement.Builder.advancement()
            .display(
                PMWorld.ELYSIUM_PORTAL,
                Component.translatable(advancementKey("killer_rabbit")),
                Component.translatable(advancementDescKey("killer_rabbit")),
                null, FrameType.CHALLENGE,
                true, true, false
            )
            .addCriterion(
                "kill_rabbit",
                KilledTrigger.TriggerInstance.playerKilledEntity(
                    EntityPredicate.Builder.entity()
                        .of(EntityType.RABBIT)
                        .nbt(new NbtPredicate(rabbitTag))
                )
            )
            .parent(root)
            .save(saver, "paradisemod:paradisemod/killer_rabbit");

        //TODO: create a giant spawner block for this advancement
        //TODO: a child advancement for defeating the said giant
        var spawnGiant = Advancement.Builder.advancement()
            .display(
                Blocks.ZOMBIE_HEAD,
                Component.translatable(advancementKey("spawn_giant")),
                Component.translatable(advancementDescKey("spawn_giant")),
                null, FrameType.CHALLENGE,
                true, true, false
            )
            .addCriterion(
                "summon_giant",
                SummonedEntityTrigger.TriggerInstance.summonedEntity(
                    EntityPredicate.Builder.entity()
                        .of(EntityType.GIANT)
                )
            )
            .parent(killerRabbit)
            .save(saver, "paradisemod:paradisemod/spawn_giant");

        Advancement.Builder.advancement()
            .display(
                Blocks.IRON_BARS,
                Component.translatable(advancementKey("prison_breakout")),
                Component.translatable(advancementDescKey("prison_breakout")),
                null, FrameType.CHALLENGE,
                true, true, false
            )
            .addCriterion(
                "kill_warden",
                KilledTrigger.TriggerInstance.playerKilledEntity(
                    EntityPredicate.Builder.entity()
                        .of(EntityType.WARDEN)
                )
            )
            .parent(spawnGiant)
            .save(saver, "paradisemod:paradisemod/prison_breakout");

        var roseFields = createRoseFieldAdvancement(root, saver);

        var theOrigin = Advancement.Builder.advancement()
            .display(
                Blocks.GRASS_BLOCK,
                Component.translatable(advancementKey("the_origin")),
                Component.translatable(advancementDescKey("the_origin")),
                null, FrameType.TASK,
                true, true, false
            )
            .addCriterion(
                "the_origin",
                PlayerTrigger.TriggerInstance.located(LocationPredicate.inBiome(PMBiomes.THE_ORIGIN))
            )
            .parent(roseFields)
            .save(saver, "paradisemod:paradisemod/the_origin");

        var seasons = Advancement.Builder.advancement()
            .display(
                Decoration.ORANGE_AUTUMN_SAPLING,
                Component.translatable(advancementKey("seasons")),
                Component.translatable(advancementDescKey("seasons")),
                null, FrameType.TASK,
                true, true, false
            )
            .addCriterion(
                "autumn_forest",
                PlayerTrigger.TriggerInstance.located(LocationPredicate.inBiome(PMBiomes.AUTUMN_FOREST))
            )
            .parent(theOrigin)
            .save(saver, "paradisemod:paradisemod/seasons");

        Advancement.Builder.advancement()
            .display(
                Blocks.CACTUS,
                Component.translatable(advancementKey("arizona")),
                Component.translatable(advancementDescKey("arizona")),
                null, FrameType.TASK,
                true, true, false
            )
            .addCriterion(
                "regular",
                PlayerTrigger.TriggerInstance.located(LocationPredicate.inBiome(PMBiomes.ROCKY_DESERT))
            )
            .addCriterion(
                "high",
                PlayerTrigger.TriggerInstance.located(LocationPredicate.inBiome(PMBiomes.HIGH_ROCKY_DESERT))
            )
            .addCriterion(
                "snowy",
                PlayerTrigger.TriggerInstance.located(LocationPredicate.inBiome(PMBiomes.SNOWY_ROCKY_DESERT))
            )
            .addCriterion(
                "palo_verde",
                PlayerTrigger.TriggerInstance.located(LocationPredicate.inBiome(PMBiomes.PALO_VERDE_FOREST))
            )
            .addCriterion(
                "mesquite",
                PlayerTrigger.TriggerInstance.located(LocationPredicate.inBiome(PMBiomes.MESQUITE_FOREST    ))
            )
            .requirements(RequirementsStrategy.OR)
            .parent(seasons)
            .save(saver, "paradisemod:paradisemod/arizona");

        var rustedWeapon = Advancement.Builder.advancement()
            .display(
                Tools.RUSTED_IRON_SWORD,
                Component.translatable(advancementKey("rusted_weapon")),
                Component.translatable(advancementDescKey("rusted_weapon")),
                null, FrameType.TASK,
                true, true, false
            )
            .addCriterion(
                "rusted_sword",
                InventoryChangeTrigger.TriggerInstance.hasItems(Tools.RUSTED_IRON_SWORD)
            )
            .parent(root)
            .save(saver, "paradisemod:paradisemod/rusted_weapon");

        Advancement.Builder.advancement()
            .display(
                Tools.REDSTONE_SWORD,
                Component.translatable(advancementKey("redstone_sword")),
                Component.translatable(advancementDescKey("redstone_sword")),
                null, FrameType.TASK,
                true, true, false
            )
            .addCriterion(
                "redstone_sword",
                InventoryChangeTrigger.TriggerInstance.hasItems(Tools.REDSTONE_SWORD)
            )
            .parent(rustedWeapon)
            .save(saver, "paradisemod:paradisemod/redstone_sword");
    }

    private static Advancement createRoseFieldAdvancement(Advancement root, Consumer<Advancement> saver) {
        var roseFields = Advancement.Builder.advancement()
            .display(
                Decoration.RED_ROSE,
                Component.translatable(advancementKey("rose_field")),
                Component.translatable(advancementDescKey("rose_field")),
                null, FrameType.TASK,
                true, true, false
            );

        var roseFileReqs = new ArrayList<>();
        for(var entry : PMBiomes.ROSE_FIELDS_BY_COLOR.entrySet()) {
            var roseField = entry.getValue();
            var colorName = entry.getKey().getName();
            roseFileReqs.add(colorName);

            roseFields = roseFields.addCriterion(
                colorName,
                PlayerTrigger.TriggerInstance.located(LocationPredicate.inBiome(roseField))
            );
        }

        return roseFields.parent(root)
            .requirements(RequirementsStrategy.OR)
            .save(saver, "paradisemod:paradisemod/rose_field");
    }

    private static String advancementKey(String name) {
        return "advancement.paradisemod.self." + name;
    }

    private static String advancementDescKey(String name) {
        return "advancement.paradisemod.self." + name + ".desc";
    }
}