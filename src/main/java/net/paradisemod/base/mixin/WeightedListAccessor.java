package net.paradisemod.base.mixin;

import com.google.common.collect.ImmutableList;
import net.minecraft.util.random.WeightedEntry;
import net.minecraft.util.random.WeightedRandomList;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(WeightedRandomList.class)
public interface WeightedListAccessor<E extends WeightedEntry> {
    @Accessor("totalWeight")
    int totalWeight();

    @Accessor("items")
    ImmutableList<E> items();
}