package net.paradisemod.base.mixin;

import net.minecraft.world.item.AxeItem;
import net.minecraft.world.level.block.Block;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;

import java.util.Map;

@Mixin(AxeItem.class)
public interface AxeItemAccessor {
    // get the table containing the wood blocks and their strippable variants
    @Accessor("STRIPPABLES")
    static Map<Block, Block> getStrippableWoods() { throw new Error("Failed to apply Mixin!"); }

    // set the table containing the wood blocks and their strippable variants
    @Accessor("STRIPPABLES")
    @Mutable
    static void setStrippableWoods(Map<Block, Block> newMap) { throw new Error("Failed to apply Mixin!"); }
}