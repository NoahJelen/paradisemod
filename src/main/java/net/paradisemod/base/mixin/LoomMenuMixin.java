package net.paradisemod.base.mixin;

import net.minecraft.core.Holder;
import net.minecraft.world.Container;
import net.minecraft.world.inventory.*;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BannerPattern;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import java.util.List;

@Mixin(value = LoomMenu.class, priority = 900)
public abstract class LoomMenuMixin extends AbstractContainerMenu {
    @Shadow
    @Final
    Slot bannerSlot;

    @Shadow
    @Final
    Slot dyeSlot;

    @Shadow
    @Final
    private Slot patternSlot;

    @Shadow
    @Final
    private Slot resultSlot;

    @Shadow
    @Final
    DataSlot selectedBannerPatternIndex;

    @Shadow
    private List<Holder<BannerPattern>> selectablePatterns;

    private LoomMenuMixin(@Nullable MenuType<?> menuType, int containerId) { super(menuType, containerId); }

    @Shadow
    protected abstract void setupResultSlot(Holder<BannerPattern> pat);

    @Shadow
    protected abstract boolean isValidPatternIndex(int index);

    @Shadow
    protected abstract List<Holder<BannerPattern>> getSelectablePatterns(ItemStack stack);

    @Inject(method = "slotsChanged", at = @At("HEAD"), cancellable = true)
    public void slotsChangedWithInfintePatterns(Container inventory, CallbackInfo ci) {
        var bannerItem = bannerSlot.getItem();
        var dyeItem = dyeSlot.getItem();
        var patternItem = patternSlot.getItem();

        if (!bannerItem.isEmpty() && !dyeItem.isEmpty()) {
            var i = selectedBannerPatternIndex.get();
            var flag = isValidPatternIndex(i);
            selectablePatterns = getSelectablePatterns(patternItem);
            Holder<BannerPattern> pattern;
            if (selectablePatterns.size() == 1) {
                selectedBannerPatternIndex.set(0);
                pattern = selectablePatterns.get(0);
            }
            else if (!flag) {
                selectedBannerPatternIndex.set(-1);
                pattern = null;
            }
            else {
                var pattern1 = selectablePatterns.get(i);
                int j = selectablePatterns.indexOf(pattern1);
                if (j != -1) {
                    pattern = pattern1;
                    selectedBannerPatternIndex.set(j);
                }
                else {
                    pattern = null;
                    selectedBannerPatternIndex.set(-1);
                }
            }

            if (pattern != null) setupResultSlot(pattern);
            else resultSlot.set(ItemStack.EMPTY);

            broadcastChanges();
        }
        else {
            resultSlot.set(ItemStack.EMPTY);
            selectablePatterns = List.of();
            selectedBannerPatternIndex.set(-1);
        }

        ci.cancel();
    }
}