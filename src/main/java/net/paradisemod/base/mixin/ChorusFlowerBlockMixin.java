package net.paradisemod.base.mixin;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.ChorusFlowerBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.Tags;
import net.paradisemod.base.PMConfig;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

// makes it possible to plant chorus flowers on any forge:end_stones block, instead of just End Stone
@Mixin(ChorusFlowerBlock.class)
public abstract class ChorusFlowerBlockMixin {
    @Inject(method = "canSurvive", at = @At("HEAD"), cancellable = true)
    private void connectToAllEndStone(BlockState state, LevelReader world, BlockPos pos, CallbackInfoReturnable<Boolean> retVal) {
        if(PMConfig.SETTINGS.betterEnd.endFoliage.get()) {
            var ground = world.getBlockState(pos.below());
            if (ground.is(Tags.Blocks.END_STONES)) retVal.setReturnValue(true);
        }
    }
}