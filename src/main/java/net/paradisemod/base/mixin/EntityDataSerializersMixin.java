package net.paradisemod.base.mixin;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.network.syncher.EntityDataSerializer;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.util.CrudeIncrementalIntIdentityHashBiMap;

@Mixin(EntityDataSerializers.class)
public class EntityDataSerializersMixin {
    @Shadow
    @Final
    private static CrudeIncrementalIntIdentityHashBiMap<EntityDataSerializer<?>> SERIALIZERS;

    @Inject(method = "registerSerializer", at = @At("HEAD"), cancellable = true)
    private static void improvedRegistration(EntityDataSerializer<?> serializer, CallbackInfo ci) {
        SERIALIZERS.add(serializer);
        ci.cancel();
    }
}