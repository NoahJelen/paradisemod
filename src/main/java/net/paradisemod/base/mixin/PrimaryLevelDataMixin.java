package net.paradisemod.base.mixin;

import net.minecraft.world.level.storage.PrimaryLevelData;
import net.minecraftforge.fml.ModList;
import net.paradisemod.ParadiseMod;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PrimaryLevelData.class)
public abstract class PrimaryLevelDataMixin {
    // if Shut Up Experimental Settings is not in use,
    // keep the experimental settings warning screen from ever being shown
    @Inject(method = "hasConfirmedExperimentalWarning", at = @At("HEAD"), cancellable = true, remap = false)
    private void ignoreExperimentalSettingsScreen(CallbackInfoReturnable<Boolean> cir)
    {
        if(!ModList.get().isLoaded("shutupexperimentalsettings")) {
            ParadiseMod.LOG.debug("NetherNoah777: Dear Mojang, if we are using mods, we are already aware that experimental world settings are unsupported because mods are unsupported by you guys as well!");
            cir.setReturnValue(true);
        }
    }
}