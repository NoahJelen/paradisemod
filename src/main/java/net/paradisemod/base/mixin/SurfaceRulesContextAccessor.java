package net.paradisemod.base.mixin;

import net.minecraft.world.level.levelgen.SurfaceRules.Context;
import net.minecraft.world.level.levelgen.SurfaceSystem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(Context.class)
public interface SurfaceRulesContextAccessor {
    @Accessor("system")
    SurfaceSystem surfaceSystem();
}