package net.paradisemod.base.mixin;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.core.LayeredRegistryAccess;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.RegistryLayer;
import net.minecraft.server.level.progress.ChunkProgressListener;
import net.minecraft.world.level.dimension.LevelStem;
import net.minecraft.world.level.levelgen.NoiseBasedChunkGenerator;
import net.minecraft.world.level.levelgen.SurfaceRules;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.PMConfig;
import net.paradisemod.world.dimension.PMDimensions;
import net.paradisemod.worldgen.surfacerules.PMSurfaceRules;

@Mixin(MinecraftServer.class)
public abstract class MinecraftServerMixin {
    @Shadow
    @Final
    private LayeredRegistryAccess<RegistryLayer> registries;

    // is there an easier way to add hardcoded surface rules to noise settings JSONs?
    @Inject(method = "createLevels", at = @At("HEAD"))
    private void hackyAddSurfaceRules(ChunkProgressListener chunkProgressListener, CallbackInfo ci) {
        var registry = registries.compositeAccess().registryOrThrow(Registries.LEVEL_STEM);
        if(PMConfig.SETTINGS.betterEnd.endFoliage.get())
            applySurfaceRule(registry, LevelStem.END, PMSurfaceRules.buildEndRule());

        applySurfaceRule(registry, PMDimensions.Type.OVERWORLD_CORE.getLevelStemKey(), PMSurfaceRules.buildOverworldCoreRules());
        applySurfaceRule(registry, PMDimensions.Type.DEEP_DARK.getLevelStemKey(), PMSurfaceRules.buildDeepDarkRules());
        applySurfaceRule(registry, PMDimensions.Type.ELYSIUM.getLevelStemKey(), PMSurfaceRules.buildElysiumRules());
        ParadiseMod.LOG.debug("Custom surface rules applied to the End and custom dimensions");
    }

    private static void applySurfaceRule(Registry<LevelStem> dimRegistry, ResourceKey<LevelStem> dimKey, SurfaceRules.RuleSource rule) {
        var dim = dimRegistry.get(dimKey);
        var chunkGenerator = dim.generator();

        if(chunkGenerator instanceof NoiseBasedChunkGenerator noiseChunkGenerator) {
            var noise = ((NoiseBasedChunkGeneratorAccessor) noiseChunkGenerator).getSettings().value();
            ((NoiseGeneratorSettingsAccessor) (Object) noise).setSurfaceRule(rule);
        }
    }
}