package net.paradisemod.base.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.ChorusPlantBlock;
import net.minecraft.world.level.block.PipeBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.Tags;
import net.paradisemod.base.PMConfig;

// makes it possible to plant chorus on any forge:end_stones block, instead of just End Stone
@Mixin(ChorusPlantBlock.class)
public abstract class ChorusPlantBlockMixin {
    @Inject(method = "getStateForPlacement(Lnet/minecraft/world/level/BlockGetter;Lnet/minecraft/core/BlockPos;)Lnet/minecraft/world/level/block/state/BlockState;", at = @At("RETURN"), locals = LocalCapture.CAPTURE_FAILSOFT, cancellable = true)
    private void getStateForPlacement(BlockGetter world, BlockPos pos, CallbackInfoReturnable<BlockState> retVal, BlockState block, BlockState block1, BlockState block2, BlockState block3, BlockState block4, BlockState block5) {
        if(PMConfig.SETTINGS.betterEnd.endFoliage.get()) {
            var plant = self();
            var newState = plant.defaultBlockState()
                .setValue(PipeBlock.DOWN, block.is(plant) || block.is(Blocks.CHORUS_FLOWER) || block.is(Tags.Blocks.END_STONES))
                .setValue(PipeBlock.UP, block1.is(plant) || block1.is(Blocks.CHORUS_FLOWER))
                .setValue(PipeBlock.NORTH, block2.is(plant) || block2.is(Blocks.CHORUS_FLOWER))
                .setValue(PipeBlock.EAST, block3.is(plant) || block3.is(Blocks.CHORUS_FLOWER))
                .setValue(PipeBlock.SOUTH, block4.is(plant) || block4.is(Blocks.CHORUS_FLOWER))
                .setValue(PipeBlock.WEST, block5.is(plant) || block5.is(Blocks.CHORUS_FLOWER));

            retVal.setReturnValue(newState);
        }
    }

    @Inject(method = "canSurvive", at = @At("HEAD"), cancellable = true)
    private void canSurvive(BlockState state, LevelReader world, BlockPos pos, CallbackInfoReturnable<Boolean> retVal) {
        if(PMConfig.SETTINGS.betterEnd.endFoliage.get()) {
            var ground = world.getBlockState(pos.below());
            if (ground.is(Tags.Blocks.END_STONES)) retVal.setReturnValue(true);
        }
    }

    @Inject(method = "updateShape", at = @At("HEAD"), cancellable = true)
    private void updateShape(BlockState curState, Direction facing, BlockState facingState, LevelAccessor world, BlockPos pos, BlockPos facingPos, CallbackInfoReturnable<BlockState> retVal) {
        if(PMConfig.SETTINGS.betterEnd.endFoliage.get()) {
            var ground = world.getBlockState(pos.below());
            if (ground.is(Tags.Blocks.END_STONES)) {
                var flag = facingState.getBlock() == self() || facingState.is(Blocks.CHORUS_FLOWER) || facing == Direction.DOWN && ground.is(Tags.Blocks.END_STONES);
                retVal.setReturnValue(curState.setValue(PipeBlock.PROPERTY_BY_DIRECTION.get(facing), flag));
            }
        }
    }

    private ChorusPlantBlock self() {
        return ((ChorusPlantBlock) (Object) this);
    }
}