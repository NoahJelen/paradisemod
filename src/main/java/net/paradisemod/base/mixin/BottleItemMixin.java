package net.paradisemod.base.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.AreaEffectCloud;
import net.minecraft.world.entity.boss.enderdragon.EnderDragon;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.BottleItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.phys.HitResult;
import net.paradisemod.world.fluid.PMFluids;

@Mixin(BottleItem.class)
public abstract class BottleItemMixin extends Item {
    BottleItemMixin(Item.Properties properties) {
        super(properties);
    }

    @Shadow
    protected abstract ItemStack turnBottleIntoItem(ItemStack bottleStack, Player player, ItemStack filledBottleStack);

    @Inject(method = "use", at = @At("HEAD"), cancellable = true)
    private void myBottledFluids(Level world, Player player, InteractionHand hand, CallbackInfoReturnable<InteractionResultHolder<ItemStack>> retVal) {
        var dragonBreathClouds = world.getEntitiesOfClass(
            AreaEffectCloud.class,
            player.getBoundingBox().inflate(2),
            cloud -> cloud != null && cloud.isAlive() && cloud.getOwner() instanceof EnderDragon
        );

        if(dragonBreathClouds.isEmpty()) {
            var bottleStack = player.getItemInHand(hand);
            var hitResult = getPlayerPOVHitResult(world, player, ClipContext.Fluid.SOURCE_ONLY);

            if(hitResult.getType() == HitResult.Type.MISS)
                retVal.setReturnValue(InteractionResultHolder.pass(bottleStack));
            else {
                if(hitResult.getType() == HitResult.Type.BLOCK) {
                    var blockpos = hitResult.getBlockPos();
                    var fluid = world.getFluidState(blockpos);
                    var curState = world.getBlockState(blockpos);

                    if(!world.mayInteract(player, blockpos))
                        retVal.setReturnValue(InteractionResultHolder.pass(bottleStack));

                    if(fluid.is(PMFluids.ENDER_ACID.get()) || fluid.is(PMFluids.FLOWING_ENDER_ACID.get()) || curState.is(PMFluids.ENDER_ACID_CAULDRON.get())) {
                        world.playSound(player, player.getX(), player.getY(), player.getZ(), SoundEvents.BOTTLE_FILL, SoundSource.NEUTRAL, 1, 1);
                        world.gameEvent(player, GameEvent.FLUID_PICKUP, blockpos);
                        retVal.setReturnValue(InteractionResultHolder.sidedSuccess(turnBottleIntoItem(bottleStack, player, new ItemStack(Items.DRAGON_BREATH)), world.isClientSide()));
                    }
                    else if(fluid.is(PMFluids.HONEY.get()) || fluid.is(PMFluids.FLOWING_HONEY.get()) || curState.is(PMFluids.HONEY_CAULDRON.get())) {
                        world.playSound(player, player.getX(), player.getY(), player.getZ(), SoundEvents.BOTTLE_FILL, SoundSource.NEUTRAL, 1, 1);
                        world.gameEvent(player, GameEvent.FLUID_PICKUP, blockpos);
                        retVal.setReturnValue(InteractionResultHolder.sidedSuccess(turnBottleIntoItem(bottleStack, player, new ItemStack(Items.HONEY_BOTTLE)), world.isClientSide()));
                    }
                }
            }
        }
    }
}