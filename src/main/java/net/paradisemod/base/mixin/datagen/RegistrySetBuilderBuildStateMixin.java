package net.paradisemod.base.mixin.datagen;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import net.paradisemod.ParadiseMod;

@Mixin(targets = "net.minecraft.core.RegistrySetBuilder$BuildState")
public class RegistrySetBuilderBuildStateMixin {
    @Inject(at = @At("HEAD"), method = "reportRemainingUnreferencedValues", cancellable = true)
    public void noErrorsForUnreferencedValues(CallbackInfo ci) {
        ParadiseMod.LOG.debug("NetherNoah777: Seriously Mojang?! Why are hand-written JSON files considered a problem?");
        ci.cancel();
    }
}