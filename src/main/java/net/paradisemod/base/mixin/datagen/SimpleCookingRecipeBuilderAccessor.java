package net.paradisemod.base.mixin.datagen;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import net.minecraft.data.recipes.SimpleCookingRecipeBuilder;
import net.minecraft.world.item.crafting.AbstractCookingRecipe;
import net.minecraft.world.item.crafting.RecipeSerializer;

@Mixin(SimpleCookingRecipeBuilder.class)
public interface SimpleCookingRecipeBuilderAccessor {
    @Accessor("serializer")
    RecipeSerializer<? extends AbstractCookingRecipe> getSerializer();
}
