package net.paradisemod.base.mixin.client;

import java.util.List;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.gui.screens.inventory.LoomScreen;
import net.minecraft.core.Holder;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.LoomMenu;
import net.minecraft.world.item.BannerItem;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BannerBlockEntity;
import net.minecraft.world.level.block.entity.BannerPattern;
import com.mojang.datafixers.util.Pair;

@Mixin(LoomScreen.class)
public abstract class LoomScreenMixin extends AbstractContainerScreen<LoomMenu> {
    @Shadow
    private List<Pair<Holder<BannerPattern>, DyeColor>> resultBannerPatterns;

    @Shadow
    private boolean hasMaxPatterns;

    @Shadow
    private ItemStack bannerStack;

    @Shadow
    private ItemStack dyeStack;

    @Shadow
    private boolean displayPatterns;

    @Shadow
    private ItemStack patternStack;

    @Shadow
    private int startRow;

    @Shadow
    private float scrollOffs;

    private LoomScreenMixin(LoomMenu menu, Inventory playerInventory, Component title) { super(menu, playerInventory, title); }

    @Shadow
    protected abstract int totalRowCount();

    @Inject(method = "containerChanged", at = @At("HEAD"), cancellable = true)
    private void containerChangedWithInfinitePatterns(CallbackInfo ci) {
        var itemstack = menu.getResultSlot().getItem();
        if (itemstack.isEmpty()) resultBannerPatterns = null;
        else {
            var bannerColor = ((BannerItem) itemstack.getItem()).getColor();
            resultBannerPatterns = BannerBlockEntity.createPatterns(bannerColor, BannerBlockEntity.getItemPatterns(itemstack));
        }

        var itemstack1 = menu.getBannerSlot().getItem();
        var itemstack2 = menu.getDyeSlot().getItem();
        var itemstack3 = menu.getPatternSlot().getItem();
        hasMaxPatterns = false;

        if (!ItemStack.matches(itemstack1, bannerStack) || !ItemStack.matches(itemstack2, this.dyeStack) || !ItemStack.matches(itemstack3, patternStack))
            displayPatterns = !itemstack1.isEmpty() && !itemstack2.isEmpty() && !menu.getSelectablePatterns().isEmpty();
        
        if (startRow >= totalRowCount()) {
            startRow = 0;
            scrollOffs = 0.0F;
        }

        bannerStack = itemstack1.copy();
        dyeStack = itemstack2.copy();
        patternStack = itemstack3.copy();

        ci.cancel();
    }
}
