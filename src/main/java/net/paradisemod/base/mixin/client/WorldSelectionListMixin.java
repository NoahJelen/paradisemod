package net.paradisemod.base.mixin.client;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.worldselection.WorldSelectionList;
import net.minecraftforge.fml.ModList;

@Mixin(WorldSelectionList.WorldListEntry.class)
public abstract class WorldSelectionListMixin {
    // if Shut Up Experimental Settings is not in use,
    // this hides the experimental settings warning icon
    @Inject(method = "renderExperimentalWarning", at = @At("HEAD"), cancellable = true, remap = false)
    private void ignoreExperimentalWarningIcon(GuiGraphics guiGraphics, int mouseX, int mouseY, int top, int left, CallbackInfo ci) {
        if(!ModList.get().isLoaded("shutupexperimentalsettings"))
            ci.cancel();
    }
}