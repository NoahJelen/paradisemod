package net.paradisemod.base.mixin.client;

import java.util.HashMap;
import java.util.Map;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.paradisemod.monsters.Monsters;

@Mixin(EntityModelSet.class)
public class EntityModelSetMixin {
    @Shadow
    private Map<ModelLayerLocation, LayerDefinition> roots;

    @Inject(method = "onResourceManagerReload", at = @At("TAIL"), cancellable = true)
    private void customEntityModels(CallbackInfo ci) {
        var newMap = new HashMap<>(roots);
        newMap.putAll(Monsters.buildLayerDefinitions());
        roots = newMap;
    }
}