package net.paradisemod.base.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.core.Direction;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.level.block.FenceBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.building.FenceGates;

// is there an easier way to make fences connect to my mod's redstone fence gate block?
@Mixin(FenceBlock.class)
public abstract class FenceBlockMixin {
    @Inject(method = "connectsTo", at = @At("HEAD"), cancellable = true)
    public void connectsTo(BlockState state, boolean isSideSolid, Direction direction, CallbackInfoReturnable<Boolean> retVal) {
        if(
            state.is(FenceGates.REDSTONE_FENCE_GATE.get()) ||
            state.is(PMTags.Blocks.METAL_FENCES) ||
            (
                self().defaultBlockState().is(PMTags.Blocks.METAL_FENCES) &&
                (state.is(BlockTags.FENCES) || state.is(BlockTags.WOODEN_FENCES))
            )
        )
            retVal.setReturnValue(true);
    }

    private FenceBlock self() {
        return ((FenceBlock) (Object) this);
    }
}