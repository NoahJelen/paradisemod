package net.paradisemod.base.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

import net.minecraft.server.commands.LocateCommand;

@Mixin(LocateCommand.class)
public class LocateCommandMixin {
    @ModifyConstant(method = "locateBiome", constant = @Constant(intValue = 6400))
    private static int fartherBiomeSearch(int value) {
        return 51200;
    }
}