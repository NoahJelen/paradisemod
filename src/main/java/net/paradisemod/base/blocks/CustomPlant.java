package net.paradisemod.base.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.BushBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.common.Tags;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;
import net.paradisemod.world.DeepDarkBlocks;

/** Base class for plant blocks (ex: roses) */
public class CustomPlant extends BushBlock implements ModeledBlock {
    protected static final VoxelShape SHAPE = Block.box(5, 0, 5, 11, 10, 11);

    // is the plant 1 or 2 blocks tall?
    private final boolean large;

    // plant type
    private final Type type;

    public CustomPlant(boolean isLarge, boolean replaceable, Type type) {
        super(
            Properties.copy(replaceable ? Blocks.GRASS : Blocks.POPPY)
                .sound(SoundType.GRASS)
                .noCollission()
        );

        this.type = type;
        large = isLarge;
    }

    public CustomPlant(boolean isLarge, Type type, int light) {
        super(
            Properties.copy(Blocks.POPPY)
                .sound(SoundType.GRASS)
                .noCollission()
                .lightLevel(s -> light)
        );

        this.type = type;
        large = isLarge;
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter world, BlockPos pos, CollisionContext context) {
        if (large) return Shapes.block();
        else return SHAPE;
    }

    @Override
    protected boolean mayPlaceOn(BlockState state, BlockGetter world, BlockPos pos) {
        if(super.mayPlaceOn(state, world, pos)) return true;
        switch(type) {
            case DEEP_DARK -> {
                return state.is(DeepDarkBlocks.DARKSTONE.get()) || state.is(DeepDarkBlocks.GLOWING_NYLIUM.get()) || state.is(DeepDarkBlocks.OVERGROWN_DARKSTONE.get());
            }

            case END -> {
                return state.is(Tags.Blocks.END_STONES);
            }
        }

        return false;
    }

    @Override
    public boolean canSurvive(BlockState state, LevelReader world, BlockPos pos) {
        if(state.is(this)) {
            if(super.canSurvive(state, world, pos)) return true;
            switch(type) {
                case DEEP_DARK -> {
                    return state.is(DeepDarkBlocks.DARKSTONE.get()) || state.is(DeepDarkBlocks.GLOWING_NYLIUM.get()) || state.is(DeepDarkBlocks.OVERGROWN_DARKSTONE.get());
                }

                case END -> {
                    return state.is(Tags.Blocks.END_STONES);
                }
            }

            return false;
        }

        return mayPlaceOn(world.getBlockState(pos), world, pos);
    }

    @Override
    public void genBlockState(BlockStateGenerator generator) {
        generator.genSimplePlant(this);
    }

    public enum Type {
        NORMAL,
        END,
        DEEP_DARK
    }
}