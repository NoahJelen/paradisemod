package net.paradisemod.base.registry;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import com.google.common.collect.ArrayListMultimap;

import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ItemModelGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;
import net.paradisemod.base.data.loot.BlockLootGenerator;

/** A block in my mod registered to Forge */
@SuppressWarnings("unchecked")
public class RegisteredBlock extends RegisteredObject<RegisteredBlock> implements Supplier<Block> {
    /** blocks with tags */
    public static final ArrayListMultimap<TagKey<Block>, RegisteredBlock> TAGGED_BLOCKS = ArrayListMultimap.create();

    /** all the blocks in this mod */
    public static final ArrayList<RegisteredBlock> ALL_BLOCKS = new ArrayList<>();

    private final RegistryObject<Block> block;
    private final boolean hasItem;
    private String renderType = "solid";
    private @Nullable BiConsumer<RegisteredBlock, ItemModelGenerator> itemModel;
    private @Nullable BiConsumer<RegisteredBlock, BlockStateGenerator> blockState;
    private BiConsumer<Block, BlockLootGenerator> lootTable = (block, generator) -> generator.dropSelf(block);
    private boolean hasLootTable = true;

    protected RegisteredBlock(RegistryObject<Block> block, boolean hasItem) {
        this.hasItem = hasItem;
        this.block = block;
        ALL_BLOCKS.add(this);
    }

    public boolean hasLootTable() { return hasLootTable; }

    @Override
    public Block get() {
        return block.get();
    }

    @Override
    public Item asItem() {
        if(hasItem)
            return get().asItem();
        else
            return Utils.modErrorTyped(registeredName() + " doesn't have an item!");
    }

    /** Does this block have an item? */
    public boolean hasItem() { return hasItem; }

    /** Add a tag to this block */
    public RegisteredBlock tag(TagKey<Block> tag) {
        TAGGED_BLOCKS.put(tag, this);
        return this;
    }

    /** Add many tags to this block */
    @SafeVarargs
    public final RegisteredBlock tags(TagKey<Block>... tags) {
        return tags(List.of(tags));
    }

    /** Add many tags to this block */
    public RegisteredBlock tags(List<TagKey<Block>> tags) {
        for(var tag : tags)
            TAGGED_BLOCKS.put(tag, this);

        return this;
    }

    /** Add an item tag to this block (if it has an item)*/
    public RegisteredBlock itemTag(TagKey<Item> tag) {
        if(!hasItem)
            return Utils.modErrorTyped(registeredName() + " doesn't have an item!");

        RegisteredItem.TAGGED_ITEMS.put(tag, this::asItem);
        return this;
    }

    /** Add many item tags to this block (if it has an item)*/
    public RegisteredBlock itemTags(TagKey<Item>... tags) {
        if(!hasItem) return Utils.modErrorTyped(registeredName() + " doesn't have an item!");

        for(var tag : tags)
            RegisteredItem.TAGGED_ITEMS.put(tag, this::asItem);

        return this;
    }

    /** The full name of this block (ex: "paradisemod:darkstone") */
    @Override
    public String registeredName() {
        return block.getKey().location().toString();
    }

    /** The name of this block (ex: "darkstone") */
    @Override
    public String shortName() {
        return block.getKey().location().getPath();
    }

    /** The block has an existing, custom made item model */
    public RegisteredBlock itemModelAlreadyExists() {
        itemModel = (block, generator) -> { };
        return this;
    }

    /** Generator function to create an item model during data generation */
    public RegisteredBlock itemModel(BiConsumer<RegisteredBlock, ItemModelGenerator> model) {
        this.itemModel = model;
        return this;
    }

    /** Generator function to create a block state definition and block models during data generation */
    public RegisteredBlock blockStateGenerator(BiConsumer<RegisteredBlock, BlockStateGenerator> blockState) {
        this.blockState = blockState;
        return this;
    }

    /** Render type for the default block model */
    public RegisteredBlock renderType(String renderType) {
        this.renderType = renderType;
        return this;
    }

    /** Generator function to create a loot table for this block */
    public RegisteredBlock lootTable(BiConsumer<Block, BlockLootGenerator> lootTable) {
        this.lootTable = lootTable;
        return this;
    }

    /** The block only drops itself when mined with a silk touch tool */
    public RegisteredBlock dropsWithSilkTouch() {
        lootTable = (block, generator) -> generator.dropWhenSilkTouch(block);
        return this;
    }

    /** The block has no item drop */
    public RegisteredBlock noDrops() {
        hasLootTable = false;
        lootTable = (block, generator) -> { };
        return this;
    }

    /** The block drops this item */
    public RegisteredBlock dropsItem(ItemLike item) {
        lootTable = (block, generator) -> generator.dropOther(block, item);
        return this;
    }

    /** Run the item model generator function at datagen time (if it exists) */
    public void genItemModel(ItemModelGenerator generator) {
        var block = get();
        if(itemModel != null) itemModel.accept(this, generator);
        else if(block instanceof ModeledBlock modeledBlock)
            modeledBlock.genItemModel(generator);
        else generator.parentBlockItem(this);
    }

    /** Run the block state definition generator function at datagen time (if it exists) */
    public void genBlockState(BlockStateGenerator generator) {
        var block = get();
        if(blockState != null)
            blockState.accept(this, generator);
        else if(block instanceof ModeledBlock modeledBlock) 
            modeledBlock.genBlockState(generator);
        else if(renderType != "solid")
            generator.simpleBlockWithRenderType(get(), renderType);
        else generator.simpleBlock(this);
    }

    public void genLootTable(BlockLootGenerator generator) { lootTable.accept(get(), generator); }
}
