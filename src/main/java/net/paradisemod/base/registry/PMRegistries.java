package net.paradisemod.base.registry;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.entity.BlockEntityType.BlockEntitySupplier;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.Utils;

public class PMRegistries {
    private static final DeferredRegister<Block> BLOCKS = PMRegistries.createRegistry(ForgeRegistries.BLOCKS);
    private static final DeferredRegister<Item> ITEMS = PMRegistries.createRegistry(ForgeRegistries.ITEMS);
    private static final DeferredRegister<BlockEntityType<?>> TILES = PMRegistries.createRegistry(ForgeRegistries.BLOCK_ENTITY_TYPES);
    private static final DeferredRegister<EntityType<?>> ENTITIES = PMRegistries.createRegistry(ForgeRegistries.ENTITY_TYPES);
    private static final ArrayList<Consumer<EntityAttributeCreationEvent>> ENTITY_ATTRIBUTES = new ArrayList<>();

    /** Register a block (no item) */
    public static RegisteredBlock regBlock(String name, Supplier<? extends Block> block) {
        return new RegisteredBlock(
            Utils.handlePossibleException(
                () -> BLOCKS.register(name, block)
            ),
            false
        );
    }

    /** Register a block with an item */
    public static RegisteredBlock regBlockItem(String name, Supplier<? extends Block> block) {
        var regBlock = new RegisteredBlock(BLOCKS.register(name, block), true);

        Utils.handlePossibleException(
            () -> ITEMS.register(
                name,
                () -> new BlockItem(regBlock.get(), new Item.Properties())
            )
        );

        return regBlock;
    }

    /** Register an item */
    public static RegisteredItem regItem(String name, Supplier<Item> item) {
        return new RegisteredItem(
            Utils.handlePossibleException(
                () -> ITEMS.register(name, item)
            ),
            false
        );
    }

    /** Create and register a tile entity */
    @SafeVarargs
    public static <T extends BlockEntity> RegistryObject<BlockEntityType<T>> createTile(String name, BlockEntitySupplier<T> constructor, Supplier<Block>... validBlocks) {
        return createTile(name, constructor, List.of(validBlocks));
    }

    /** Create and register a tile entity */
    public static <T extends BlockEntity> RegistryObject<BlockEntityType<T>> createTile(String name, BlockEntitySupplier<T> constructor, List<? extends Supplier<Block>> validBlocks) {
        return Utils.handlePossibleException(
            () -> TILES.register(
                name,
                () -> {
                    var blocks = validBlocks.stream()
                        .map(Supplier::get)
                        .toArray(Block[]::new);

                    return BlockEntityType.Builder.of(constructor, blocks).build(null);
                }
            )
        );
    }

    /** Create and register an entity */
    public static <T extends Entity> RegistryObject<EntityType<T>> createEntity(String name, EntityType.Builder<T> builder) {
        return Utils.handlePossibleException(
            () -> ENTITIES.register(name, () -> builder.build("paradisemod:" + name))
        );
    }

    public static <T extends LivingEntity> RegistryObject<EntityType<T>> createCreature(String name, EntityType.Builder<T> builder, Supplier<AttributeSupplier.Builder> attributesBuilder) {
        var regEntity = createEntity(name, builder);
        ENTITY_ATTRIBUTES.add(event -> event.put(regEntity.get(), attributesBuilder.get().build()));
        return regEntity;
    }

    public static void init(IEventBus eventbus) {
        BLOCKS.register(eventbus);
        ITEMS.register(eventbus);
        TILES.register(eventbus);
        ENTITIES.register(eventbus);
        eventbus.addListener(PMRegistries::genEntityAttributes);
        ParadiseMod.LOG.debug("Registration success!");
    }

    /** Create a new forge registry */
    public static <R> DeferredRegister<R> createRegistry(IForgeRegistry<R> registry) { return DeferredRegister.create(registry, ParadiseMod.ID); }

    /** Create a new forge registry */
    public static <R> DeferredRegister<R> createRegistry(ResourceKey<? extends Registry<R>> key) { return DeferredRegister.create(key, ParadiseMod.ID); }

    /** Convenience method for creating `ResourceKey`s for this mod */
    public static <T> ResourceKey<T> createModResourceKey(ResourceKey<? extends Registry<T>> registryKey, String name) {
        return createResourceKey(registryKey, ParadiseMod.ID, name);
    }

    /** Convenience method for creating `ResourceKey`s */
    public static <T> ResourceKey<T> createResourceKey(ResourceKey<? extends Registry<T>> registryKey, String namespace, String name) {
        return ResourceKey.create(registryKey, new ResourceLocation(namespace, name));
    }

    /** Convenience method for creating `ResourceKey`s */
    public static <T> ResourceKey<T> createResourceKey(ResourceKey<? extends Registry<T>> registryKey, String fullName) {
        return ResourceKey.create(registryKey, new ResourceLocation(fullName));
    }

    private static void genEntityAttributes(EntityAttributeCreationEvent event) {
        for(var attrConsumer : ENTITY_ATTRIBUTES)
            attrConsumer.accept(event);
    }
}