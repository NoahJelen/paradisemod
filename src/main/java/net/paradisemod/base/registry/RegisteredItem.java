package net.paradisemod.base.registry;

import java.util.ArrayList;
import java.util.function.BiConsumer;
import java.util.function.Supplier;
import javax.annotation.Nullable;
import com.google.common.collect.ArrayListMultimap;

import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.base.data.assets.ItemModelGenerator;

/** An item in my mod registered to Forge */
@SuppressWarnings("unchecked")
public class RegisteredItem extends RegisteredObject<RegisteredItem> implements Supplier<Item> {
    /** items with tags */
    public static final ArrayListMultimap<TagKey<Item>, Supplier<? extends Item>> TAGGED_ITEMS = ArrayListMultimap.create();

    /** all the non-block items in this mod */
    public static final ArrayList<RegisteredItem> ALL_ITEMS = new ArrayList<>();

    private final RegistryObject<Item> item;
    private @Nullable BiConsumer<RegisteredItem, ItemModelGenerator> model = null;

    protected RegisteredItem(RegistryObject<Item> item, boolean forBlock) {
        this.item = item;
        if(!forBlock) ALL_ITEMS.add(this);
    }

    @Override
    public Item get() {
        return item.get();
    }

    /** Add a tag to this item */
    public RegisteredItem tag(TagKey<Item> tag) {
        TAGGED_ITEMS.put(tag, this);
        return this;
    }

    /** Add many tags to this item */
    public RegisteredItem tags(TagKey<Item>... tags) {
        for(var tag : tags)
            TAGGED_ITEMS.put(tag, this);

        return this;
    }

    /** The item has an existing, custom made model */
    public RegisteredItem modelAlreadyExists() {
        this.model = (item, provider) -> { };
        return this;
    }

    /** Generator function to create the model during data generation */
    public RegisteredItem model(BiConsumer<RegisteredItem, ItemModelGenerator> model) {
        this.model = model;
        return this;
    }

    /** run the item model generator function at datagen time (if it exists) */
    public void genModel(ItemModelGenerator generator) {
        if(model != null) model.accept(this, generator);
        else generator.basicItem(get());
    }

    /** The full name of this item (ex: "paradisemod:redstone_door") */
    @Override
    public String registeredName() {
        return item.getKey().location().toString();
    }

    /** The name of this block (ex: "redstone_door") */
    @Override
    public String shortName() {
        return item.getKey().location().getPath();
    }

    @Override
    public Item asItem() {
        return get();
    }

    public ResourceKey<Item> getKey() {
        return item.getKey();
    }
}
