package net.paradisemod.base.registry;

import java.util.ArrayList;
import java.util.function.Supplier;

import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.data.assets.PMTranslations;

/** A creative tab in my mod registered to Forge */
public class RegisteredCreativeTab {
    private static final DeferredRegister<CreativeModeTab> REGISTERED_TABS = PMRegistries.createRegistry(Registries.CREATIVE_MODE_TAB);
    public static final ArrayList<RegisteredCreativeTab> ALL_TABS = new ArrayList<>();

    private ResourceKey<CreativeModeTab> key;
    private final String name;
    private String englishName;
    private String spanishName;

    public RegisteredCreativeTab(String name, Supplier<? extends ItemLike> icon, String englishName, String spanishName) {
        this.name = name;
        this.englishName = englishName;
        this.spanishName = spanishName;
        this.key = ResourceKey.create(Registries.CREATIVE_MODE_TAB, new ResourceLocation(ParadiseMod.ID, name));
        ALL_TABS.add(this);

        REGISTERED_TABS.register(
            name,
            () -> CreativeModeTab.builder()
                .icon(() -> new ItemStack(icon.get()))
                .title(Component.translatable("itemGroup." + name))
                .build()
        );
    }

    /** Get the `ResourceKey` of this creative tab */
    public ResourceKey<CreativeModeTab> key() {
        return key;
    }

    public static void initTabs(IEventBus eventbus) {
        REGISTERED_TABS.register(eventbus);
    }

    public void genLocalizedName(PMTranslations translator, boolean spanish) {
        if(spanish)
            translator.add("itemGroup." + name, spanishName);
        else translator.add("itemGroup." + name, englishName);
    }
}