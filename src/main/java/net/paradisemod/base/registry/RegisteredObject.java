package net.paradisemod.base.registry;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;

import com.google.common.collect.ArrayListMultimap;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.data.recipes.RecipeBuilder;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.SimpleCookingRecipeBuilder;
import net.minecraft.data.recipes.SingleItemRecipeBuilder;
import net.minecraft.data.recipes.SmithingTransformRecipeBuilder;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.ItemLike;
import net.minecraftforge.registries.ForgeRegistries;
import net.paradisemod.base.data.RecipeGenerator;
import net.paradisemod.base.mixin.datagen.SimpleCookingRecipeBuilderAccessor;

/* the abstraction for blocks and items registered to my mod */
public abstract class RegisteredObject<T extends RegisteredObject<T>> implements ItemLike {
    private final ArrayList<BiFunction<Item, RecipeGenerator, ? extends RecipeBuilder>> recipes = new ArrayList<>();
    private final ArrayList<BiFunction<Item, RecipeGenerator, SmithingTransformRecipeBuilder>> smithingRecipes = new ArrayList<>();
    private final ArrayList<ResourceKey<CreativeModeTab>> creativeTabs = new ArrayList<>();
    private String englishName = "";
    private String spanishName = "";

    @SuppressWarnings("unchecked")
    private T self() { return (T) this; }

    public abstract String shortName();
    public abstract String registeredName();

    /** The recipe (crafting, smelting, stonecutting, etc.) to create this block.
    * This can be called multiple times if the block/item has several recipes */
    public T recipe(BiFunction<Item, RecipeGenerator, ? extends RecipeBuilder> recipe) {
        recipes.add(recipe);
        return self();
    }

    /** Smithing recipe for the block/item. Smithing recipes must be done separately from
    * other recipe types since they don't implement RecipeBuilder */
    public T smithingRecipe(BiFunction<Item, RecipeGenerator, SmithingTransformRecipeBuilder> recipe) {
        smithingRecipes.add(recipe);
        return self();
    }

    public T oreRecipes(ItemLike oreBlock) {
        return recipe((item, generator) -> generator.furnaceRecipe(oreBlock, item, RecipeCategory.MISC))
            .recipe((item, generator) -> generator.blastingRecipe(oreBlock, item, RecipeCategory.MISC));
    }

    public T cookedFoodRecipes(ItemLike rawFood) {
        return recipe((item, generator) -> generator.campfireRecipe(rawFood, item, RecipeCategory.MISC))
            .recipe((item, generator) -> generator.smokingRecipe(rawFood, item, RecipeCategory.MISC))
            .recipe((item, generator) -> generator.furnaceRecipe(rawFood, item, RecipeCategory.MISC));
    }

    public T stonecutterRecipes(int count, ItemLike... inputs) {
        for(var input : inputs)
            recipes.add((item, generator) -> generator.stonecutterRecipe(input, asItem(), count));

        return self();
    }

    public T stonecutterRecipes(ItemLike... inputs) {
        return stonecutterRecipes(1, inputs);
    }

    public T tab(ResourceKey<CreativeModeTab> tab) {
        creativeTabs.add(tab);
        return self();
    }

    @SafeVarargs
    public final T tabs(ResourceKey<CreativeModeTab>... tabs) {
        creativeTabs.addAll(List.of(tabs));
        return self();
    }

    public T tab(RegisteredCreativeTab tab) { return tab(tab.key());}

    public T localizedName(String englishName, String spanishName) {
        this.englishName = englishName;
        this.spanishName = spanishName;
        return self();
    }

    public String getLocalizedName(boolean isSpanish) {
        return isSpanish ? spanishName : englishName;
    }

    public boolean hasTab(ResourceKey<CreativeModeTab> key) { return creativeTabs.contains(key); }

    public boolean hasRecipes() {
        return !recipes.isEmpty() || !smithingRecipes.isEmpty();
    }

    public void genRecipes(RecipeGenerator generator, Consumer<FinishedRecipe> output) {
        ArrayListMultimap<String, RecipeBuilder> sortedRecipes = ArrayListMultimap.create();
        var name = shortName();

        for(var recipe : recipes) {
            var recipeBuilder = recipe.apply(asItem(), generator);

            if(recipeBuilder instanceof SimpleCookingRecipeBuilder furnaceRecipe) {
                var serializer = ((SimpleCookingRecipeBuilderAccessor) furnaceRecipe).getSerializer();
                var key = ForgeRegistries.RECIPE_SERIALIZERS.getKey(serializer);
                sortedRecipes.put(key.getPath(), furnaceRecipe);
            }
            else if(recipeBuilder instanceof SingleItemRecipeBuilder stonecutterRecipe)
                sortedRecipes.put("stonecutter", stonecutterRecipe);
            else
                sortedRecipes.put("crafting", recipeBuilder);
        }

        for(var recipeList : sortedRecipes.asMap().entrySet()) {
            int idx = 0;

            var recipeType = recipeList.getKey();
            for(var recipe : recipeList.getValue()) {
                var outputName = name;

                if(recipeType != "crafting")
                    outputName = recipeType + "/" + outputName;

                if(idx > 0)
                    outputName = outputName + "_" + (idx + 1);

                if(outputName == name)
                    recipe.save(output);
                else
                    recipe.save(output, "paradisemod:" + outputName);

                idx++;
            }
        }

        int idx = 0;
        for(var recipe : smithingRecipes) {
            var outputName = "paradisemod:smithing/" + name;
            var builder = recipe.apply(asItem(), generator);

            if(idx > 0)
                outputName = outputName + "_" + (idx + 1);

            builder.save(output, outputName);
            idx++;
        }
    }
}