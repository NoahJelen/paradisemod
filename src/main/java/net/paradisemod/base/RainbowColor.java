package net.paradisemod.base;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class RainbowColor {
    private static int RED = 0xFF0000;
    public static List<Integer> RAINBOW_COLORS = List.of(
        RED,
        0xF43545,
        0xFA8901,
        0xFAD717,
        0x00BA71,
        0x00FF00,
        0x00C2DE,
        0x0000FF, 
        0x00418D,
        0x5F2879,
        RED
    );

    private final ArrayList<Integer> gradient = new ArrayList<>();
    private int colorIndex = 0;
    private Instant time = Instant.now();

    public RainbowColor() {
        for(int idx = 0; idx < RAINBOW_COLORS.size() - 1; idx++) {
            var firstColor = RAINBOW_COLORS.get(idx);
            var secondColor = RAINBOW_COLORS.get(idx + 1);

            gradient.add(firstColor);
            for(int numerator = 1; numerator < 20; numerator++)
                gradient.add(
                    mixColors(firstColor, secondColor,  numerator / 20F)
                );

            gradient.add(secondColor);
        }
    }

    public int color() {
        var color = gradient.get(colorIndex);
        incIndex();
        return color;
    }

    private void incIndex() {
        var now = Instant.now();

        if(now.toEpochMilli() - time.toEpochMilli() >= 100) {
            time = now;
            if(colorIndex < gradient.size() - 1)
                colorIndex++;
            else colorIndex = 0;
        }
    }

    private static int mixColors(int firstColor, int secondColor, float ratio) {
        int mask1 = 0x00ff00ff;
        int mask2 = 0xff00ff00;

        int f2 = (int) (256 * ratio);
        int f1 = 256 - f2;

        return (((((firstColor & mask1) * f1) + ((secondColor & mask1) * f2)) >> 8) & mask1) 
             | (((((firstColor & mask2) * f1) + ((secondColor & mask2) * f2)) >> 8) & mask2);
    }
}
