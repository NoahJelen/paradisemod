package net.paradisemod.monsters;

import com.google.common.collect.ImmutableMap;

import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.renderer.entity.EntityRenderers;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraftforge.client.model.generators.ModelFile.UncheckedModelFile;
import net.minecraftforge.common.ForgeSpawnEggItem;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredItem;
import net.paradisemod.monsters.gargoyle.Gargoyle;
import net.paradisemod.monsters.gargoyle.GargoyleModel;
import net.paradisemod.monsters.gargoyle.GargoyleRenderer;

public class Monsters {
    static {
        EntityDataSerializers.registerSerializer(Gargoyle.STATE_SERIALIZER);
        EntityDataSerializers.registerSerializer(Gargoyle.VARIANT_SERIALIZER);
    }

    public static final RegistryObject<EntityType<Gargoyle>> GARGOYLE = PMRegistries.createCreature(
        "gargoyle",
        EntityType.Builder.of(Gargoyle::new, MobCategory.MONSTER)
            .sized(1, 1)
            .fireImmune()
            .updateInterval(1),
        Monster::createMonsterAttributes
    );

    public static final RegisteredItem GARGOYLE_SPAWN_EGG = createSpawnEgg(
        GARGOYLE,
        0x747474,
        0xFF9C59,
        "Gargoyle Spawn Egg",
        "Generar gárgola"
    );

    public static void init() { ParadiseMod.LOG.info("Loaded Monsters module"); }

    public static void initClient() {
        EntityRenderers.register(GARGOYLE.get(), GargoyleRenderer::new);
    }

    public static ImmutableMap<ModelLayerLocation, LayerDefinition> buildLayerDefinitions() {
        return new ImmutableMap.Builder<ModelLayerLocation, LayerDefinition>()
            .put(GargoyleModel.LAYER_LOCATION, GargoyleModel.createBodyLayer())
            .build();
    }

    public static RegisteredItem createSpawnEgg(RegistryObject<? extends EntityType<? extends Mob>> entityType, int backgroundColor, int highlightColor, String englishName, String spanishName) {
        var name = entityType.getKey().location().getPath() + "_spawn_egg";
        
        return PMRegistries.regItem(name, () -> new ForgeSpawnEggItem(entityType, backgroundColor, highlightColor, new Item.Properties()))
            .tab(CreativeModeTabs.SPAWN_EGGS)
            .model(
                (item, generator) -> {
                    generator.getBuilder(name)
                        .parent(new UncheckedModelFile("minecraft:item/template_spawn_egg"));
                }
            )
            .localizedName(englishName, spanishName);
    }
}