package net.paradisemod.monsters.gargoyle;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;
import net.paradisemod.ParadiseMod;

public class GargoyleRenderer extends MobRenderer<Gargoyle, GargoyleModel> {
    public GargoyleRenderer(EntityRendererProvider.Context context) {
        super(context, new GargoyleModel(context), 0.5F);
    }

    @Override
    public ResourceLocation getTextureLocation(Gargoyle gargoyle) {
        return new ResourceLocation(ParadiseMod.ID, "textures/entity/gargoyle/" + gargoyle.getVariant().getSerializedName() + ".png");
    }
}