package net.paradisemod.monsters.gargoyle;

import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.paradisemod.ParadiseMod;

public class GargoyleModel extends HierarchicalModel<Gargoyle> {
    public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(ParadiseMod.ID, "gargoyle"), "main");

    private final ModelPart root;
	private final ModelPart leg1;
	private final ModelPart leg2;
	private final ModelPart body;
	private final ModelPart head;
	private final ModelPart arms;
	private final ModelPart wing1;
	private final ModelPart wing2;

	public GargoyleModel(EntityRendererProvider.Context context) {
        var root = context.bakeLayer(LAYER_LOCATION);
		this.root = root.getChild("gargoyle");
		leg1 = this.root.getChild("leg1");
		leg2 = this.root.getChild("leg2");
		body = this.root.getChild("body");
		head = this.root.getChild("head");
		arms = this.root.getChild("arms");
		wing1 = this.root.getChild("wing1");
		wing2 = this.root.getChild("wing2");
	}

    @Override
    public void setupAnim(Gargoyle gargoyle, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        root.getAllParts().forEach(ModelPart::resetPose);
        animate(gargoyle.attackingState, GargoyleAnimations.ATTACK, ageInTicks);
        animate(gargoyle.flyingState, GargoyleAnimations.FLYING, ageInTicks);
        animateWalk(GargoyleAnimations.WALK, limbSwing, limbSwingAmount, 1, 2.5F);
    }

    public static LayerDefinition createBodyLayer() {
		var meshdefinition = new MeshDefinition();
        var root = meshdefinition.getRoot()
            .addOrReplaceChild("gargoyle", CubeListBuilder.create(), PartPose.offset(-3, 16, 0));

		root.addOrReplaceChild(
            "leg1",
            CubeListBuilder.create()
                .texOffs(16, 24)
                .addBox(-1, 0, -2, 3, 8, 3, new CubeDeformation(0)),
            PartPose.offset(0, 0, 0)
        );

        root.addOrReplaceChild(
            "leg2",
            CubeListBuilder.create()
                .texOffs(16, 24)
                .addBox(-2, 0, -2, 3, 8, 3, new CubeDeformation(0)),
            PartPose.offset(6, 0, 0)
        );

        root.addOrReplaceChild(
            "body",
            CubeListBuilder.create()
                .texOffs(26, 12)
                .addBox(-3, 0, -2, 6, 2, 3, new CubeDeformation(0))
                .texOffs(0, 0)
                .addBox(-5, -7, -3, 10, 7, 5, new CubeDeformation(0)),
            PartPose.offset(3, -2, 0)
        );

        root.addOrReplaceChild(
            "head",
            CubeListBuilder.create()
                .texOffs(26, 17)
                .addBox(0, 2, -3.5F, 1, 2, 1, new CubeDeformation(0))
                .texOffs(0, 24)
                .addBox(-1.5F, -1, -2.5F, 4, 4, 4, new CubeDeformation(0))
                .texOffs(30, 0)
                .addBox(1, -3, -1, 1, 3, 1, new CubeDeformation(0))
                .texOffs(30, 0)
                .addBox(-1, -3, -1, 1, 3, 1, new CubeDeformation(0)),
            PartPose.offset(2.5F, -12, -0.5F)
        );

        root.addOrReplaceChild(
            "arms",
            CubeListBuilder.create()
                .texOffs(0, 32)
                .addBox(11, -2, -1, 2, 12, 2, new CubeDeformation(0))
                .texOffs(0, 32)
                .addBox(-1, -2, -1, 2, 12, 2, new CubeDeformation(0)), PartPose.offset(-3, -7, 0)
        );

        root.addOrReplaceChild(
            "wing1",
            CubeListBuilder.create()
                .texOffs(0, 12)
                .addBox(-11, -2, 0, 11, 2, 2, new CubeDeformation(0))
                .texOffs(0, 20)
                .addBox(-11, 0, 1, 11, 4, 0, new CubeDeformation(0)),
            PartPose.offset(2, -5, 2)
        );

        root.addOrReplaceChild(
            "wing2",
            CubeListBuilder.create()
                .texOffs(0, 12)
                .addBox(0, -2, 0, 11, 2, 2, new CubeDeformation(0))
                .texOffs(0, 20)
                .addBox(0, 0, 1, 11, 4, 0, new CubeDeformation(0)),
            PartPose.offset(4, -5, 2)
        );

		return LayerDefinition.create(meshdefinition, 64, 64);
	}

    @Override
    public ModelPart root() {
        return root;
    }
}