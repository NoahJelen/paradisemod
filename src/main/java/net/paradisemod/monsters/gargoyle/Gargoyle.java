package net.paradisemod.monsters.gargoyle;

import java.util.UUID;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializer;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.util.StringRepresentable;
import net.minecraft.util.TimeUtil;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.AnimationState;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.NeutralMob;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.VariantHolder;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.paradisemod.world.DeepDarkBlocks;

public class Gargoyle extends Monster implements NeutralMob, VariantHolder<Gargoyle.Variant> {
    public static final EntityDataSerializer<State> STATE_SERIALIZER = EntityDataSerializer.simpleEnum(State.class);
    public static final EntityDataSerializer<Variant> VARIANT_SERIALIZER = EntityDataSerializer.simpleEnum(Variant.class);

    private static final EntityDataAccessor<Integer> REMAINING_ANGER_TIME = SynchedEntityData.defineId(Gargoyle.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Variant> VARIANT = SynchedEntityData.defineId(Gargoyle.class, VARIANT_SERIALIZER);
    private static final EntityDataAccessor<State> STATE = SynchedEntityData.defineId(Gargoyle.class, STATE_SERIALIZER);

    private static final UniformInt PERSISTENT_ANGER_TIME = TimeUtil.rangeOfSeconds(14, 49);

    protected AnimationState flyingState = new AnimationState();
    protected AnimationState attackingState = new AnimationState();

    @Nullable
    private UUID persistentAngerTarget;

    public Gargoyle(EntityType<Gargoyle> entityType, Level world) {
        super(entityType, world);
    }

    @Override
    public void setVariant(Variant variant) {
        entityData.set(VARIANT, variant);
    }

    @Override
    public Variant getVariant() {
        return entityData.get(VARIANT);
    }

    @Override
    public int getRemainingPersistentAngerTime() {
        return entityData.get(REMAINING_ANGER_TIME);
    }

    @Override
    public void setRemainingPersistentAngerTime(int time) {
        entityData.set(REMAINING_ANGER_TIME, time);
    }

    @Override
    @Nullable
    public UUID getPersistentAngerTarget() {
        return persistentAngerTarget;
    }

    @Override
    public void setPersistentAngerTarget(@Nullable UUID target) {
        this.persistentAngerTarget = target;
    }

    @Override
    public void startPersistentAngerTimer() {
        setRemainingPersistentAngerTime(PERSISTENT_ANGER_TIME.sample(random));
    }

    @Override
    public boolean isPreventingPlayerRest(Player player) {
        return isAngryAt(player);
    }

    @Override
    public void setTarget(@Nullable LivingEntity creature) {
        if(creature instanceof Player player)
            setLastHurtByPlayer(player);

        super.setTarget(creature);
    }

    @Override
    public void addAdditionalSaveData(CompoundTag nbt) {
        super.addAdditionalSaveData(nbt);
        nbt.putString("variant", getVariant().getSerializedName());
    }

    @Override
    public void readAdditionalSaveData(CompoundTag nbt) {
        super.readAdditionalSaveData(nbt);
        entityData.set(VARIANT, Variant.checkedValueOf(nbt.getString("variant")));
    }

    @Override
    public void onSyncedDataUpdated(EntityDataAccessor<?> key) {
        if(key == STATE) {
            var state = entityData.get(STATE);

            if(state.isFlying())
                flyingState.start(tickCount);
            else flyingState.stop();

            if(state.isAttacking())
                attackingState.start(tickCount);
            else attackingState.stop();
        }
    }

    @Override
    public SpawnGroupData finalizeSpawn(ServerLevelAccessor world, DifficultyInstance difficulty, MobSpawnType reason, @Nullable SpawnGroupData spawnData, @Nullable CompoundTag nbt) {
        var blockBelow = world.getBlockState(blockPosition().below());
        setVariant(Variant.fromBlock(blockBelow));
        return super.finalizeSpawn(world, difficulty, reason, spawnData, nbt);
    }

    @Override
    protected SoundEvent getHurtSound(DamageSource damageSource) {
        return SoundEvents.DEEPSLATE_BREAK;
    }

    @Override
    protected SoundEvent getDeathSound() {
        return SoundEvents.DEEPSLATE_BREAK;
    }

    @Override
    protected void defineSynchedData() {
        super.defineSynchedData();
        entityData.define(REMAINING_ANGER_TIME, 0);
        entityData.define(VARIANT, Variant.STONE);
        entityData.define(STATE, State.NONE);
    }

    @Override
    protected void registerGoals() {
        goalSelector.addGoal(0, new LookAtPlayerGoal(this, Player.class, 8));
        goalSelector.addGoal(1, new RandomLookAroundGoal(this));
    }

    public enum State {
        NONE,
        ATTACKING,
        FLYING,
        ATTACKING_IN_FLIGHT;

        public boolean isFlying() {
            return this == FLYING || this == ATTACKING_IN_FLIGHT;
        }

        public boolean isAttacking() {
            return this == ATTACKING || this == ATTACKING_IN_FLIGHT;
        }
    }

    public enum Variant implements StringRepresentable {
        ANDESITE(Blocks.ANDESITE),
        BLACKENED_SANDSTONE(DeepDarkBlocks.BLACKENED_SANDSTONE),
        CALCITE(Blocks.CALCITE),
        DARKSTONE(DeepDarkBlocks.DARKSTONE),
        DEEPSLATE(Blocks.DEEPSLATE),
        DIORITE(Blocks.DIORITE),
        DRIPSTONE(Blocks.DRIPSTONE_BLOCK),
        END_STONE(Blocks.END_STONE),
        GRANITE(Blocks.GRANITE),
        RED_SANDSTONE(Blocks.RED_SANDSTONE),
        SANDSTONE(Blocks.SANDSTONE),
        STONE(Blocks.STONE),
        TUFF(Blocks.TUFF);

        private final Supplier<Block> matchingBlock;

        Variant(Supplier<Block> matchingBlock) {
            this.matchingBlock = matchingBlock;
        }

        Variant(Block matchingBlock) {
            this.matchingBlock = () -> matchingBlock;
        }

        @Override
        public String getSerializedName() {
            return name().toLowerCase();
        }

        public static Variant fromBlock(BlockState block) {
            for(var type : values())
                if(block.is(type.matchingBlock.get()))
                    return type;

            return STONE;
        }

        public static Variant checkedValueOf(String name) {
            try {
                return valueOf(name.toUpperCase());
            }
            catch (Exception yes) {
                return Variant.STONE;
            }
        }
    }
}