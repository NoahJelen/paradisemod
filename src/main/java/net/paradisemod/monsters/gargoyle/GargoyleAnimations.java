package net.paradisemod.monsters.gargoyle;

import net.minecraft.client.animation.AnimationChannel;
import net.minecraft.client.animation.AnimationDefinition;
import net.minecraft.client.animation.Keyframe;
import net.minecraft.client.animation.KeyframeAnimations;

public class GargoyleAnimations {
    public static final AnimationDefinition ATTACK = AnimationDefinition.Builder.withLength(0.68F).looping()
		.addAnimation(
            "arms",
            new AnimationChannel(
                AnimationChannel.Targets.ROTATION, 
                new Keyframe(0, KeyframeAnimations.degreeVec(0, 0, 0), AnimationChannel.Interpolations.LINEAR),
                new Keyframe(0.36F, KeyframeAnimations.degreeVec(-142.5F, 0, 0), AnimationChannel.Interpolations.LINEAR),
                new Keyframe(0.68F, KeyframeAnimations.degreeVec(0, 0, 0), AnimationChannel.Interpolations.LINEAR)
            )
        )
		.addAnimation(
            "head",
            new AnimationChannel(
                AnimationChannel.Targets.ROTATION, 
                new Keyframe(0, KeyframeAnimations.degreeVec(0, 0, 0), AnimationChannel.Interpolations.LINEAR)
            )
        )
		.build();

	public static final AnimationDefinition WALK = AnimationDefinition.Builder.withLength(1.08F).looping()
		.addAnimation(
            "leg1",
            new AnimationChannel(
                AnimationChannel.Targets.ROTATION, 
                new Keyframe(0, KeyframeAnimations.degreeVec(-35, 0, 0), AnimationChannel.Interpolations.LINEAR),
                new Keyframe(0.48F, KeyframeAnimations.degreeVec(35, 0, 0), AnimationChannel.Interpolations.LINEAR),
                new Keyframe(1.08F, KeyframeAnimations.degreeVec(-35, 0, 0), AnimationChannel.Interpolations.LINEAR)
            )
        )
		.addAnimation(
            "leg2",
            new AnimationChannel(
                AnimationChannel.Targets.ROTATION, 
                new Keyframe(0, KeyframeAnimations.degreeVec(35, 0, 0), AnimationChannel.Interpolations.LINEAR),
                new Keyframe(0.48F, KeyframeAnimations.degreeVec(-35, 0, 0), AnimationChannel.Interpolations.LINEAR),
                new Keyframe(1.08F, KeyframeAnimations.degreeVec(35, 0, 0), AnimationChannel.Interpolations.LINEAR)
            )
        )
		.build();

	public static final AnimationDefinition FLYING = AnimationDefinition.Builder.withLength(1).looping()
		.addAnimation(
            "wing1",
            new AnimationChannel(
                AnimationChannel.Targets.ROTATION, 
                new Keyframe(0, KeyframeAnimations.degreeVec(0, 0, 0), AnimationChannel.Interpolations.LINEAR),
                new Keyframe(0.48F, KeyframeAnimations.degreeVec(0, 25, 0), AnimationChannel.Interpolations.LINEAR),
                new Keyframe(0.96F, KeyframeAnimations.degreeVec(0, 0, 0), AnimationChannel.Interpolations.LINEAR)
            )
        )
		.addAnimation(
            "wing2",
            new AnimationChannel(
                AnimationChannel.Targets.ROTATION, 
                new Keyframe(0, KeyframeAnimations.degreeVec(0, 0, 0), AnimationChannel.Interpolations.LINEAR),
                new Keyframe(0.48F, KeyframeAnimations.degreeVec(0, -25, 0), AnimationChannel.Interpolations.LINEAR),
                new Keyframe(0.96F, KeyframeAnimations.degreeVec(0, 2.5F, 0), AnimationChannel.Interpolations.LINEAR)
            )
        )
		.addAnimation(
            "head",
            new AnimationChannel(
                AnimationChannel.Targets.ROTATION, 
                new Keyframe(0, KeyframeAnimations.degreeVec(-25, 0, 0), AnimationChannel.Interpolations.LINEAR)
            )
        )
		.addAnimation(
            "gargoyle",
            new AnimationChannel(
                AnimationChannel.Targets.ROTATION, 
                new Keyframe(0, KeyframeAnimations.degreeVec(27.5F, 0, 0), AnimationChannel.Interpolations.LINEAR)
            )
        )
		.build();
}