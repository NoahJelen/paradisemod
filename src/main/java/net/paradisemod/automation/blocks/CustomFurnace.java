package net.paradisemod.automation.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FurnaceBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.paradisemod.automation.tile.CustomFurnaceEntity;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ItemModelGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;

public class CustomFurnace extends FurnaceBlock implements ModeledBlock {
    private final boolean dark;
    public CustomFurnace(boolean isDark) {
        super(Properties.copy(Blocks.FURNACE));
        dark = isDark;
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) { return new CustomFurnaceEntity(pos, state, dark); }

    @Override
    public void openContainer(Level world, BlockPos pos, Player player) {
        var tile = world.getBlockEntity(pos);
        if (tile instanceof CustomFurnaceEntity furnace) {
            player.openMenu(furnace);
            player.awardStat(Stats.INTERACT_WITH_FURNACE);
        }
    }

    @Override
    public void animateTick(BlockState state, Level world, BlockPos pos, RandomSource rand) {
        if (state.getValue(LIT)) {
            var d0 = pos.getX() + 0.5D;
            var d1 = pos.getY();
            var d2 = pos.getZ() + 0.5D;
            if (rand.nextDouble() < 0.1D)
                world.playLocalSound(d0, d1, d2, SoundEvents.FURNACE_FIRE_CRACKLE, SoundSource.BLOCKS, 1.0F, 1.0F, false);

            var direction = state.getValue(FACING);
            var axis = direction.getAxis();
            var d4 = rand.nextDouble() * 0.6D - 0.3D;
            var d5 = axis == Direction.Axis.X ? (double) direction.getStepX() * 0.52D : d4;
            var d6 = rand.nextDouble() * 6.0D / 16.0D;
            var d7 = axis == Direction.Axis.Z ? (double) direction.getStepZ() * 0.52D : d4;
            world.addParticle(ParticleTypes.SMOKE, d0 + d5, d1 + d6, d2 + d7, 0.0D, 0.0D, 0.0D);
            if (dark) world.addParticle(ParticleTypes.PORTAL, d0 + d5, d1 + d6, d2 + d7, 0.0D, 0.0D, 0.0D);
            else world.addParticle(ParticleTypes.FLAME, d0 + d5, d1 + d6, d2 + d7, 0.0D, 0.0D, 0.0D);
        }
    }

    @Override
    public void genItemModel(ItemModelGenerator generator) {
        var name = shortName();
        var furnaceName = name.substring(0, name.length() - 8);
        generator.getBuilder(name)
            .parent(generator.existingModel("block/furnace/" + furnaceName));
    }

    @Override
    public void genBlockState(BlockStateGenerator generator) {
        var name = shortName();
        var furnaceName = name.substring(0, name.length() - 8);
        var model = generator.existingModel("furnace/" + furnaceName);
        var litModel = generator.existingModel("furnace/" + furnaceName + "_on");

        generator.getVariantBuilder(this)
            .forAllStates(
                state ->
                    generator.buildVariantModel(state.getValue(LIT) ? litModel : model, state.getValue(FACING), true)
            );
    }
}