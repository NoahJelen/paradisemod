package net.paradisemod.automation.blocks;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.stats.Stats;
import net.minecraft.world.Containers;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.HopperBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.client.model.generators.ConfiguredModel;
import net.paradisemod.automation.Automation;
import net.paradisemod.automation.tile.CustomHopperEntity;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ItemModelGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;

public class CustomHopper extends HopperBlock implements ModeledBlock {
    private final int rate;
    public CustomHopper(int itemRate) {
        super(Block.Properties.copy(Blocks.HOPPER));
        rate = itemRate;
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) { return new CustomHopperEntity(pos, state, rate); }

    @Override
    public void setPlacedBy(Level world, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack) {
        if (stack.hasCustomHoverName()) {
            var tile = world.getBlockEntity(pos);
            if (tile instanceof CustomHopperEntity hopper)
                hopper.setCustomName(stack.getHoverName());
        }
    }

    @Override
    public InteractionResult use(BlockState state, Level world, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hit) {
        if (world.isClientSide) return InteractionResult.SUCCESS;
        else {
            var tile = world.getBlockEntity(pos);
            if (tile instanceof CustomHopperEntity hopper) {
                player.openMenu(hopper);
                player.awardStat(Stats.INSPECT_HOPPER);
            }

            return InteractionResult.CONSUME;
        }
    }

    @Override
    public void onRemove(BlockState state, Level world, BlockPos pos, BlockState newState, boolean isMoving) {
        if (!state.is(newState.getBlock())) {
            var tile = world.getBlockEntity(pos);
            if (tile instanceof CustomHopperEntity hopper) {
                Containers.dropContents(world, pos, hopper);
                world.updateNeighbourForOutputSignal(pos, this);
            }

            super.onRemove(state, world, pos, newState, isMoving);
        }
    }

    @Override
    public void entityInside(BlockState state, Level world, BlockPos pPos, Entity pEntity) {
        var tile = world.getBlockEntity(pPos);
        if (tile instanceof CustomHopperEntity hopper)
            CustomHopperEntity.entityInside(world, pPos, state, pEntity, hopper);
    }

    @Override
    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level world, BlockState state, BlockEntityType<T> tileType) {
        return world.isClientSide ? null : createTickerHelper(tileType, Automation.CUSTOM_HOPPER_TILE.get(), CustomHopperEntity::pushItemsTick);
    }

    @Override
    public void genItemModel(ItemModelGenerator generator) {
        generator.basicItem(asItem());
    }

    @Override
    public void genBlockState(BlockStateGenerator generator) {
        var name = shortName();
        var hopperName = name.substring(0, name.length() - 7);
        var downModel = generator.existingModel("hopper/" + hopperName);
        var sideModel = generator.existingModel("hopper/" + hopperName + "_side");

        generator.getVariantBuilder(this)
            .forAllStatesExcept(
                state -> {
                    Direction dir = state.getValue(FACING);
                    var builder = ConfiguredModel.builder();

                    if(dir == Direction.DOWN)
                        builder = builder.modelFile(downModel);
                    else
                        builder = builder.modelFile(sideModel)
                            .rotationY(BlockStateGenerator.calcYRot(dir));

                    return builder.build();
                },
                ENABLED
            );
    }
}