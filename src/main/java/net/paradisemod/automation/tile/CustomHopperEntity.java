package net.paradisemod.automation.tile;

import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.Container;
import net.minecraft.world.ContainerHelper;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.WorldlyContainerHolder;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntitySelector;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.HopperMenu;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.ChestBlock;
import net.minecraft.world.level.block.HopperBlock;
import net.minecraft.world.level.block.entity.ChestBlockEntity;
import net.minecraft.world.level.block.entity.Hopper;
import net.minecraft.world.level.block.entity.RandomizableContainerBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.Shapes;
import net.paradisemod.automation.Automation;

public class CustomHopperEntity extends RandomizableContainerBlockEntity implements Hopper {
    private final int rate;
    private NonNullList<ItemStack> items = NonNullList.withSize(5, ItemStack.EMPTY);
    private int cooldownTime = -1;
    private long tickedGameTime;

    public CustomHopperEntity(BlockPos pos, BlockState state, int itemRate) {
        super(Automation.CUSTOM_HOPPER_TILE.get(), pos, state);
        rate = itemRate;
    }

    public CustomHopperEntity(BlockPos pos, BlockState state) { this(pos, state, 64); }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        items = NonNullList.withSize(this.getContainerSize(), ItemStack.EMPTY);
        if (!tryLoadLootTable(tag))
            ContainerHelper.loadAllItems(tag, items);

        cooldownTime = tag.getInt("TransferCooldown");
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        if (!trySaveLootTable(tag)) ContainerHelper.saveAllItems(tag, items);
        tag.putInt("TransferCooldown", cooldownTime);
    }

    @Override
    public int getContainerSize() { return 5; }

    @Override
    public ItemStack removeItem(int index, int count) {
        unpackLootTable(null);
        return ContainerHelper.removeItem(getItems(), index, count);
    }

    @Override
    public void setItem(int index, ItemStack stack) {
        unpackLootTable(null);
        items.set(index, stack);
        if (stack.getCount() > getMaxStackSize())
            stack.setCount(getMaxStackSize());
    }

    @Override
    protected Component getDefaultName() { return Component.translatable("container.hopper"); }

    public static void pushItemsTick(Level world, BlockPos pos, BlockState state, CustomHopperEntity hopper) {
        hopper.cooldownTime--;
        hopper.tickedGameTime = world.getGameTime();
        if (!hopper.isOnCooldown()) {
            hopper.setCooldown(0);
            tryMoveItems(world, pos, state, hopper, () -> suckInItems(world, hopper));
        }
    }

    private static boolean tryMoveItems(Level world, BlockPos pos, BlockState state, CustomHopperEntity hopper, BooleanSupplier validator) {
        if (!world.isClientSide) {
            if (!hopper.isOnCooldown() && state.getValue(HopperBlock.ENABLED)) {
                var flag = false;
                if (!hopper.isEmpty()) flag = ejectItems(world, pos, state, hopper);
                if (!hopper.inventoryFull()) flag |= validator.getAsBoolean();

                if (flag) {
                    hopper.setCooldown(hopper.rate);
                    setChanged(world, pos, state);
                    return true;
                }
            }
        }

        return false;
    }

    private boolean inventoryFull() {
        for(var itemstack : items)
            if (itemstack.isEmpty() || itemstack.getCount() != itemstack.getMaxStackSize()) return false;

        return true;
    }

    private static boolean ejectItems(Level world, BlockPos pos, BlockState state, CustomHopperEntity sourceHopper) {
        var container = getAttachedContainer(world, pos, state);
        if (container != null) {
            var direction = state.getValue(HopperBlock.FACING).getOpposite();
            if (!isFullContainer(container, direction)) {
                for(int i = 0; i < sourceHopper.getContainerSize(); ++i) {
                    if (!sourceHopper.getItem(i).isEmpty()) {
                        var itemstack = sourceHopper.getItem(i).copy();
                        var itemstack1 = addItem(sourceHopper, container, sourceHopper.removeItem(i, 1), direction);
                        if (itemstack1.isEmpty()) {
                            container.setChanged();
                            return true;
                        }

                        sourceHopper.setItem(i, itemstack);
                    }
                }
            }
        }

        return false;
    }

    private static IntStream getSlots(Container container, Direction direction) {
        return container instanceof WorldlyContainer ? IntStream.of(((WorldlyContainer)container).getSlotsForFace(direction)) : IntStream.range(0, container.getContainerSize());
    }

    private static boolean isFullContainer(Container container, Direction direction) {
        return getSlots(container, direction).allMatch(slot -> {
            var itemstack = container.getItem(slot);
            return itemstack.getCount() >= itemstack.getMaxStackSize();
        });
    }

    private static boolean isEmptyContainer(Container container, Direction direction) {
        return getSlots(container, direction).allMatch(slot -> container.getItem(slot).isEmpty());
    }

    public static boolean suckInItems(Level world, Hopper hopper) {
        var ret = net.minecraftforge.items.VanillaInventoryCodeHooks.extractHook(world, hopper);
        if (ret != null) return ret;
        var container = getSourceContainer(world, hopper);
        if (container != null) {
            var direction = Direction.DOWN;
            return !isEmptyContainer(container, direction) && getSlots(container, direction).anyMatch(slot -> tryTakeInItemFromSlot(hopper, container, slot, direction));
        }
        else {
            for(var itementity : getItemsAtAndAbove(world, hopper))
                if (addItem(hopper, itementity))
                    return true;

            return false;
        }
    }

    private static boolean tryTakeInItemFromSlot(Hopper hopper, Container container, int slot, Direction direction) {
        var itemstack = container.getItem(slot);
        if (!itemstack.isEmpty() && canTakeItemFromContainer(container, itemstack, slot, direction)) {
            var itemstack1 = itemstack.copy();
            var itemstack2 = addItem(container, hopper, container.removeItem(slot, 1), null);
            if (itemstack2.isEmpty()) {
                container.setChanged();
                return true;
            }

            container.setItem(slot, itemstack1);
        }

        return false;
    }

    public static boolean addItem(Container container, ItemEntity item) {
        var flag = false;
        var itemstack = item.getItem().copy();
        var itemstack1 = addItem(null, container, itemstack, null);
        if (itemstack1.isEmpty()) {
            flag = true;
            item.discard();
        }
        else item.setItem(itemstack1);

        return flag;
    }

    public static ItemStack addItem(@Nullable Container source, Container destination, ItemStack stack, @Nullable Direction direction) {
        if (destination instanceof WorldlyContainer container && direction != null) {
            for(var slot : container.getSlotsForFace(direction))
                stack = tryMoveInItem(source, destination, stack, slot, direction);
        }
        else {
            int i = destination.getContainerSize();

            for(int j = 0; j < i && !stack.isEmpty(); ++j)
                stack = tryMoveInItem(source, destination, stack, j, direction);
        }

        return stack;
    }

    private static boolean canPlaceItemInContainer(Container container, ItemStack stack, int pslot, @Nullable Direction direction) {
        if (!container.canPlaceItem(pslot, stack)) return false;
        else
            return !(container instanceof WorldlyContainer) || ((WorldlyContainer) container).canPlaceItemThroughFace(pslot, stack, direction);
    }

    private static boolean canTakeItemFromContainer(Container container, ItemStack stack, int slot, Direction direction) {
        return !(container instanceof WorldlyContainer) || ((WorldlyContainer) container).canTakeItemThroughFace(slot, stack, direction);
    }

    private static ItemStack tryMoveInItem(@Nullable Container source, Container destination, ItemStack stack, int slot, @Nullable Direction direction) {
        var itemstack = destination.getItem(slot);
        if (canPlaceItemInContainer(destination, stack, slot, direction)) {
            var flag = false;
            var flag1 = destination.isEmpty();
            if (itemstack.isEmpty()) {
                destination.setItem(slot, stack);
                stack = ItemStack.EMPTY;
                flag = true;
            }
            else if (canMergeItems(itemstack, stack)) {
                var i = stack.getMaxStackSize() - itemstack.getCount();
                var j = Math.min(stack.getCount(), i);
                stack.shrink(j);
                itemstack.grow(j);
                flag = j > 0;
            }

            if (flag) {
                if (flag1 && destination instanceof CustomHopperEntity hopperblockentity1) {
                    if (!hopperblockentity1.isOnCustomCooldown()) {
                        int k = 0;
                        if (source instanceof CustomHopperEntity hopperblockentity)
                            if (hopperblockentity1.tickedGameTime >= hopperblockentity.tickedGameTime)
                                k = 1;

                        hopperblockentity1.setCooldown(8 - k);
                    }
                }

                destination.setChanged();
            }
        }

        return stack;
    }

    @Nullable
    private static Container getAttachedContainer(Level world, BlockPos pos, BlockState state) {
        var direction = state.getValue(HopperBlock.FACING);
        return getContainerAt(world, pos.relative(direction));
    }

    @Nullable
    private static Container getSourceContainer(Level world, Hopper hopper) {
        return getContainerAt(world, hopper.getLevelX(), hopper.getLevelY() + 1.0D, hopper.getLevelZ());
    }

    public static List<ItemEntity> getItemsAtAndAbove(Level world, Hopper hopper) {
        return hopper.getSuckShape().toAabbs().stream().flatMap(
            bounds -> world.getEntitiesOfClass(ItemEntity.class, bounds.move(hopper.getLevelX() - 0.5D, hopper.getLevelY() - 0.5D, hopper.getLevelZ() - 0.5D), EntitySelector.ENTITY_STILL_ALIVE).stream()
        )
            .collect(Collectors.toList());
    }

    @Nullable
    public static Container getContainerAt(Level world, BlockPos pos) {
        return getContainerAt(world, pos.getX() + 0.5D, pos.getY() + 0.5D, pos.getZ() + 0.5D);
    }

    @Nullable
    private static Container getContainerAt(Level world, double x, double y, double z) {
        Container container = null;
        var blockpos = new BlockPos((int) x, (int) y, (int) z);
        var blockstate = world.getBlockState(blockpos);
        var block = blockstate.getBlock();
        if (block instanceof WorldlyContainerHolder containerBlock)
            container = containerBlock.getContainer(blockstate, world, blockpos);
        else if (blockstate.hasBlockEntity()) {
            var tile = world.getBlockEntity(blockpos);
            if (tile instanceof Container containerTile) {
                container = containerTile;
                if (container instanceof ChestBlockEntity && block instanceof ChestBlock chest)
                    container = ChestBlock.getContainer(chest, blockstate, world, blockpos, true);
            }
        }

        if (container == null) {
            var entities = world.getEntities((Entity) null, new AABB(x - 0.5D, y - 0.5D, z - 0.5D, x + 0.5D, y + 0.5D, z + 0.5D), EntitySelector.CONTAINER_ENTITY_SELECTOR);
            if (!entities.isEmpty()) container = (Container) entities.get(world.random.nextInt(entities.size()));
        }

        return container;
    }

    private static boolean canMergeItems(ItemStack stack1, ItemStack stack2) {
        if (!stack1.is(stack2.getItem())) return false;
        else if (stack1.getDamageValue() != stack2.getDamageValue()) return false;
        else if (stack1.getCount() > stack1.getMaxStackSize()) return false;
        else return ItemStack.isSameItemSameTags(stack1, stack2);
    }

    @Override
    public double getLevelX() { return (double)this.worldPosition.getX() + 0.5D; }

    @Override
    public double getLevelY() { return (double)this.worldPosition.getY() + 0.5D; }

    @Override
    public double getLevelZ() { return (double)this.worldPosition.getZ() + 0.5D; }

    public void setCooldown(int time) { cooldownTime = time; }
    private boolean isOnCooldown() { return cooldownTime > 0; }
    public boolean isOnCustomCooldown() { return cooldownTime > 8; }

    @Override
    protected NonNullList<ItemStack> getItems() { return items; }

    @Override
    protected void setItems(NonNullList<ItemStack> items) { this.items = items; }

    public static void entityInside(Level world, BlockPos pos, BlockState state, Entity entity, CustomHopperEntity hopper) {
        if (entity instanceof ItemEntity item && Shapes.joinIsNotEmpty(Shapes.create(entity.getBoundingBox().move(-pos.getX(), -pos.getY(), -pos.getZ())), hopper.getSuckShape(), BooleanOp.AND))
            tryMoveItems(world, pos, state, hopper, () -> addItem(hopper, item));
    }

    @Override
    protected AbstractContainerMenu createMenu(int id, Inventory inventory) {
        return new HopperMenu(id, inventory, this);
    }
}