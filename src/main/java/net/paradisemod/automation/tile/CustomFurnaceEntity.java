package net.paradisemod.automation.tile;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.FurnaceMenu;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.block.entity.AbstractFurnaceBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.paradisemod.automation.Automation;

public class CustomFurnaceEntity extends AbstractFurnaceBlockEntity {
    public final boolean dark;
    public CustomFurnaceEntity(BlockPos pos, BlockState state) {
        super(Automation.CUSTOM_FURNACE_TILE.get(), pos, state, RecipeType.SMELTING);
        dark = false;
    }

    public CustomFurnaceEntity(BlockPos pos, BlockState state, boolean isDark) {
        super(Automation.CUSTOM_FURNACE_TILE.get(), pos, state, RecipeType.SMELTING);
        dark = isDark;
    }

    @Override
    protected Component getDefaultName() {
        return Component.translatable("container.furnace");
    }

    @Override
    protected FurnaceMenu createMenu(int id, Inventory inventory) {
        return new FurnaceMenu(id, inventory, this, this.dataAccess);
    }

    @Override
    protected int getBurnDuration(ItemStack fuel) {
        if(dark) return super.getBurnDuration(fuel) / 2;
        else return super.getBurnDuration(fuel);
    }
}
