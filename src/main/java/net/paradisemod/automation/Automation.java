package net.paradisemod.automation;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.vehicle.AbstractMinecart;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.PoweredRailBlock;
import net.minecraft.world.level.block.RailBlock;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.RailShape;
import net.minecraftforge.common.Tags;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.ParadiseMod;
import net.paradisemod.automation.blocks.CustomFurnace;
import net.paradisemod.automation.blocks.CustomHopper;
import net.paradisemod.automation.tile.CustomFurnaceEntity;
import net.paradisemod.automation.tile.CustomHopperEntity;
import net.paradisemod.base.data.RecipeGenerator;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ItemModelGenerator;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.world.DeepDarkBlocks;

public class Automation {
    // furnaces
    public static final RegisteredBlock MOSSY_COBBLESTONE_FURNACE = PMRegistries.regBlockItem(
        "mossy_cobblestone_furnace",
        () -> new CustomFurnace(false)
    )
        .tabs(
            CreativeModeTabs.FUNCTIONAL_BLOCKS,
            CreativeModeTabs.REDSTONE_BLOCKS
        )
        .tag(BlockTags.MINEABLE_WITH_PICKAXE)
        .recipe((item, generator) -> furnaceRecipe(generator, Blocks.MOSSY_COBBLESTONE, item))
        .localizedName("Mossy Cobblestone Furnace", "Horno de piedra labrada musgosa");

    public static final RegisteredBlock DARKSTONE_FURNACE = PMRegistries.regBlockItem(
        "darkstone_furnace",
        () -> new CustomFurnace(true)
    )
        .tabs(
            DeepDarkBlocks.DEEP_DARK_TAB.key(),
            CreativeModeTabs.FUNCTIONAL_BLOCKS,
            CreativeModeTabs.REDSTONE_BLOCKS
        )
        .tag(BlockTags.MINEABLE_WITH_PICKAXE)
        .recipe((item, generator) -> furnaceRecipe(generator, DeepDarkBlocks.DARKSTONE, item))
        .localizedName("Darkstone Furnace", "Horno de piedra oscura");

    // hoppers
    public static final RegisteredBlock GOLD_HOPPER = PMRegistries.regBlockItem(
        "gold_hopper",
        () -> new CustomHopper(1)
    )
        .tab(CreativeModeTabs.REDSTONE_BLOCKS)
        .tags(
            BlockTags.MINEABLE_WITH_PICKAXE,
            BlockTags.NEEDS_IRON_TOOL
        )
        .recipe((item, generator) -> hopperRecipe(generator, Tags.Items.INGOTS_GOLD, item))
        .localizedName("Golden Hopper", "Tolva dorada");

    public static final RegisteredBlock SILVER_HOPPER = PMRegistries.regBlockItem(
        "silver_hopper",
        () -> new CustomHopper(2)
    )
        .tab(CreativeModeTabs.REDSTONE_BLOCKS)
        .tags(
            BlockTags.MINEABLE_WITH_PICKAXE,
            BlockTags.NEEDS_STONE_TOOL
        )
        .recipe((item, generator) -> hopperRecipe(generator, PMTags.Items.SILVER_INGOTS, item))
        .localizedName("Silver Hopper", "Tolva plateada");

    // rails
    public static final RegisteredBlock EMERALD_RAIL = PMRegistries.regBlockItem("emerald_rail",
        () -> new RailBlock(Block.Properties.copy(Blocks.RAIL)) {
            @Override
            public float getRailMaxSpeed(BlockState state, Level level, BlockPos pos, AbstractMinecart cart) { return 1.2f; }
        }
    )
        .tabs(
            CreativeModeTabs.REDSTONE_BLOCKS,
            CreativeModeTabs.TOOLS_AND_UTILITIES
        )
        .itemModel(Automation::genRailItemModel)
        .blockStateGenerator(Automation::genRailBlockState)
        .tag(BlockTags.RAILS)
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item, 16)
                .pattern("e e")
                .pattern("ese")
                .pattern("e e")
                .define('e', Items.EMERALD)
                .define('s', Items.STICK)
        )
        .localizedName("Emerald Rail", "Vía de esmeralda");

    public static final RegisteredBlock POWERED_EMERALD_RAIL = PMRegistries.regBlockItem("powered_emerald_rail",
        () -> new PoweredRailBlock(Block.Properties.copy(Blocks.POWERED_RAIL), true) {
            @Override
            public float getRailMaxSpeed(BlockState state, Level level, BlockPos pos, AbstractMinecart cart) { return 1.2f; }
        }
    )
        .tabs(
            CreativeModeTabs.REDSTONE_BLOCKS,
            CreativeModeTabs.TOOLS_AND_UTILITIES
        )
        .itemModel((block, generator) -> generator.flatBlockItem(block))
        .blockStateGenerator(Automation::genRailBlockState)
        .tag(BlockTags.RAILS)
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item, 6)
                .pattern("gsg")
                .pattern("geg")
                .pattern("grg")
                .define('g', Tags.Items.INGOTS_GOLD)
                .define('s', Items.STICK)
                .define('r', Items.REDSTONE)
                .define('e', Items.EMERALD)
        )
        .localizedName("Powered Emerald Rail", "Vía propulsora de esmeralda");

    // tile entities
    public static final RegistryObject<BlockEntityType<CustomFurnaceEntity>> CUSTOM_FURNACE_TILE = PMRegistries.createTile("custom_furnace", CustomFurnaceEntity::new, MOSSY_COBBLESTONE_FURNACE, DARKSTONE_FURNACE);
    public static final RegistryObject<BlockEntityType<CustomHopperEntity>> CUSTOM_HOPPER_TILE = PMRegistries.createTile("custom_hopper", CustomHopperEntity::new, GOLD_HOPPER, SILVER_HOPPER);

    public static void init() { ParadiseMod.LOG.info("Loaded Automation module"); }

    private static void genRailItemModel(RegisteredBlock railBlock, ItemModelGenerator generator) {
        generator.flatBlockItem(railBlock);
    }

    private static void genRailBlockState(RegisteredBlock railBlock, BlockStateGenerator generator) {
        if(railBlock.shortName().contains("powered")) {
            generator.getVariantBuilder(railBlock.get())
                .forAllStates(state -> {
                    boolean powered = state.getValue(PoweredRailBlock.POWERED);
                    RailShape shape = state.getValue(PoweredRailBlock.SHAPE);
                    var shortName = railBlock.shortName() + (powered ? "_on" : "");

                    var name = switch(shape) {
                        case ASCENDING_EAST, ASCENDING_NORTH -> shortName + "_raised_ne";
                        case ASCENDING_SOUTH, ASCENDING_WEST -> shortName + "_raised_sw";
                        default -> shortName;
                    };

                    var dir = switch(shape) {
                        case ASCENDING_EAST, ASCENDING_WEST, EAST_WEST -> Direction.EAST;
                        default -> Direction.NORTH;
                    };

                    return generator.buildVariantModel(generator.existingModel(name), dir, true);
                });
        }
        else {
            generator.getVariantBuilder(railBlock.get())
                .forAllStates(state -> {
                    RailShape shape = state.getValue(RailBlock.SHAPE);
                    var modelName =  switch(shape) {
                        case ASCENDING_EAST, ASCENDING_NORTH ->
                            railBlock.shortName() + "_raised_ne";

                        case ASCENDING_SOUTH, ASCENDING_WEST ->
                            railBlock.shortName() + "_raised_sw";

                        case NORTH_EAST, NORTH_WEST, SOUTH_EAST, SOUTH_WEST ->
                            railBlock.shortName() + "_corner";

                        default -> railBlock.shortName();
                    };

                    var dir = switch(shape) {
                        case ASCENDING_EAST, ASCENDING_WEST, EAST_WEST, SOUTH_WEST -> Direction.EAST;
                        case NORTH_EAST -> Direction.WEST;
                        case NORTH_WEST -> Direction.SOUTH;
                        default -> Direction.NORTH;
                    };

                    return generator.buildVariantModel(generator.existingModel(modelName), dir, true);
                });
        }
    }

    private static ShapedRecipeBuilder furnaceRecipe(RecipeGenerator generator, ItemLike input, ItemLike result) {
        return generator.getShapedBuilder(RecipeCategory.MISC, result)
            .pattern("###")
            .pattern("# #")
            .pattern("###")
            .define('#', input);
    }

    private static ShapedRecipeBuilder hopperRecipe(RecipeGenerator generator, TagKey<Item> input, ItemLike result) {
        return generator.getShapedBuilder(RecipeCategory.MISC, result)
            .pattern("I I")
            .pattern("ICI")
            .pattern(" I ")
            .define('I', input)
            .define('C', Tags.Items.CHESTS);
    }
}