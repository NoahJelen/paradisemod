package net.paradisemod.misc;

import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.ItemLike;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredItem;

public class Armor {
    // emerald armor
    public static final RegisteredItem EMERALD_HELMET = regArmorItem(PMArmorMaterial.EMERALD, ArmorItem.Type.HELMET);
    public static final RegisteredItem EMERALD_CHESTPLATE = regArmorItem(PMArmorMaterial.EMERALD, ArmorItem.Type.CHESTPLATE);
    public static final RegisteredItem EMERALD_LEGGINGS = regArmorItem(PMArmorMaterial.EMERALD, ArmorItem.Type.LEGGINGS);
    public static final RegisteredItem EMERALD_BOOTS = regArmorItem(PMArmorMaterial.EMERALD, ArmorItem.Type.BOOTS);

    // ruby armor
    public static final RegisteredItem RUBY_HELMET = regArmorItem(PMArmorMaterial.RUBY, ArmorItem.Type.HELMET);
    public static final RegisteredItem RUBY_CHESTPLATE = regArmorItem(PMArmorMaterial.RUBY, ArmorItem.Type.CHESTPLATE);
    public static final RegisteredItem RUBY_LEGGINGS = regArmorItem(PMArmorMaterial.RUBY, ArmorItem.Type.LEGGINGS);
    public static final RegisteredItem RUBY_BOOTS = regArmorItem(PMArmorMaterial.RUBY, ArmorItem.Type.BOOTS);

    // obsidian armor
    public static final RegisteredItem OBSIDIAN_HELMET = regArmorItem(PMArmorMaterial.OBSIDIAN, ArmorItem.Type.HELMET);
    public static final RegisteredItem OBSIDIAN_CHESTPLATE = regArmorItem(PMArmorMaterial.OBSIDIAN, ArmorItem.Type.CHESTPLATE);
    public static final RegisteredItem OBSIDIAN_LEGGINGS = regArmorItem(PMArmorMaterial.OBSIDIAN, ArmorItem.Type.LEGGINGS);
    public static final RegisteredItem OBSIDIAN_BOOTS = regArmorItem(PMArmorMaterial.OBSIDIAN, ArmorItem.Type.BOOTS);

    // redstone armor
    public static final RegisteredItem REDSTONE_HELMET = regArmorItem(PMArmorMaterial.REDSTONE, ArmorItem.Type.HELMET);
    public static final RegisteredItem REDSTONE_CHESTPLATE = regArmorItem(PMArmorMaterial.REDSTONE, ArmorItem.Type.CHESTPLATE);
    public static final RegisteredItem REDSTONE_LEGGINGS = regArmorItem(PMArmorMaterial.REDSTONE, ArmorItem.Type.LEGGINGS);
    public static final RegisteredItem REDSTONE_BOOTS = regArmorItem(PMArmorMaterial.REDSTONE, ArmorItem.Type.BOOTS);

    // rusted iron armor
    public static final RegisteredItem RUSTED_IRON_HELMET = regArmorItem(PMArmorMaterial.RUSTED_IRON, ArmorItem.Type.HELMET);
    public static final RegisteredItem RUSTED_IRON_CHESTPLATE = regArmorItem(PMArmorMaterial.RUSTED_IRON, ArmorItem.Type.CHESTPLATE);
    public static final RegisteredItem RUSTED_IRON_LEGGINGS = regArmorItem(PMArmorMaterial.RUSTED_IRON, ArmorItem.Type.LEGGINGS);
    public static final RegisteredItem RUSTED_IRON_BOOTS = regArmorItem(PMArmorMaterial.RUSTED_IRON, ArmorItem.Type.BOOTS);

    // silver armor
    public static final RegisteredItem SILVER_HELMET = regArmorItem(PMArmorMaterial.SILVER, ArmorItem.Type.HELMET);
    public static final RegisteredItem SILVER_CHESTPLATE = regArmorItem(PMArmorMaterial.SILVER, ArmorItem.Type.CHESTPLATE);
    public static final RegisteredItem SILVER_LEGGINGS = regArmorItem(PMArmorMaterial.SILVER, ArmorItem.Type.LEGGINGS);
    public static final RegisteredItem SILVER_BOOTS = regArmorItem(PMArmorMaterial.SILVER, ArmorItem.Type.BOOTS);

    // enderite armor
    public static final RegisteredItem ENDERITE_HELMET = regArmorItem(PMArmorMaterial.ENDERITE, ArmorItem.Type.HELMET);
    public static final RegisteredItem ENDERITE_CHESTPLATE = regArmorItem(PMArmorMaterial.ENDERITE, ArmorItem.Type.CHESTPLATE);
    public static final RegisteredItem ENDERITE_LEGGINGS = regArmorItem(PMArmorMaterial.ENDERITE, ArmorItem.Type.LEGGINGS);
    public static final RegisteredItem ENDERITE_BOOTS = regArmorItem(PMArmorMaterial.ENDERITE, ArmorItem.Type.BOOTS);

    public static void init() {
        // armor events
        MinecraftForge.EVENT_BUS.register(Events.class);
    }

    private static RegisteredItem regArmorItem(PMArmorMaterial material, ArmorItem.Type type, ItemLike... upgradeFromItems) {
        var props = new Item.Properties();
        if (material == PMArmorMaterial.OBSIDIAN) props.fireResistant();

        var slotName = switch (type) {
            case BOOTS -> "boots";
            case LEGGINGS -> "leggings";
            case CHESTPLATE -> "chestplate";
            case HELMET -> "helmet";
        };

        var englishName = switch (type) {
            case BOOTS -> "Boots";
            case LEGGINGS -> "Leggings";
            case CHESTPLATE -> "Chestplate";
            case HELMET -> "Helmet";
        };

        var spanishName = switch (type) {
            case BOOTS -> "Botas";
            case LEGGINGS -> "Grebas";
            case CHESTPLATE -> "Pechera";
            case HELMET -> "Yelmo";
        };

        var itemTexture = "armor/" + material.getShortName() + "_" + slotName;

        var armor = PMRegistries.regItem(
            material.getShortName() + "_" + slotName,
            () -> new ArmorItem(material, type, new Item.Properties())
        )
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.COMBAT, item, armorRecipePattern(type))
                    .define('x', material.craftingItem())
            )
            .tab(CreativeModeTabs.COMBAT)
            .tag(PMTags.Items.PRESENTS)
            .model((item, generator) -> generator.basicItem(item.get(), itemTexture))
            .localizedName(
                Utils.localizedMaterialName(material.getShortName(), false) + " " + englishName,
                spanishName + " de " + Utils.localizedMaterialName(material.getShortName(), true)
            );

        if(material == PMArmorMaterial.SILVER)
            armor = armor.tag(PMTags.Items.SILVER_RECYCLABLES);

        var upgradeItem = material.upgradableFrom(type);

        if(upgradeItem != null)
            armor = armor.smithingRecipe(
                (item, generator) -> generator.smithingRecipe(upgradeItem, material.getRepairItem(), Tools.SMITHING_UPGRADE, item, RecipeCategory.COMBAT)
            );

        return armor;
    }

    private static String armorRecipePattern(ArmorItem.Type armorType) {
        return switch(armorType) {
            case HELMET -> "xxx\nx x";
            case CHESTPLATE -> "x x\nxxx\nxxx";
            case LEGGINGS -> "xxx\nx x\nx x";
            case BOOTS ->"x x\nx x";
        };
    }

    private static class Events {
        // give a player XP at random if he/she is wearing 4 pieces of emerald armor and gives the player indefinite luck
        @SubscribeEvent
        public static void giveXPAndLuckforEmeraldArmor(PlayerEvent event) {
            int numEmeraldArmorPieces = 0;
            var player = event.getEntity();
            if (player == null) return;
            for (var stack : player.getArmorSlots())
                if(stack.getItem() instanceof ArmorItem armor)
                    if (armor.getMaterial() == PMArmorMaterial.EMERALD) numEmeraldArmorPieces ++;

            if (numEmeraldArmorPieces == 4) {
                var playerWorld = player.level();

                if (playerWorld.random.nextInt(500) == 0)
                    player.giveExperiencePoints(2);

                player.addEffect(new MobEffectInstance(MobEffects.LUCK, 100));
            }
        }

        // gives a player indefinite fire resistance if he/she is a wearing all obsidian armor
        @SubscribeEvent
        public static void obsidianArmorFireResistance(PlayerEvent event) {
            int numObsidianArmorPieces = 0;
            var player = event.getEntity();
            if (player == null) return;
            for (var stack : player.getArmorSlots())
                if(stack.getItem() instanceof ArmorItem armor)
                    if(armor.getMaterial() == PMArmorMaterial.OBSIDIAN) numObsidianArmorPieces++;

            if (numObsidianArmorPieces == 4) player.addEffect(new MobEffectInstance(MobEffects.FIRE_RESISTANCE, 100));
        }
    }
}