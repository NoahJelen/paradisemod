package net.paradisemod.misc;

import java.util.function.Supplier;

import javax.annotation.Nullable;

import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Blocks;
import net.paradisemod.ParadiseMod;

public enum PMArmorMaterial implements ArmorMaterial {
    EMERALD(35, new int[] {4, 6, 8, 4}, 40, SoundEvents.ARMOR_EQUIP_DIAMOND, 3f, 0f, () -> Items.EMERALD),
    RUBY(35, new int[] {4, 6, 8, 4}, 40, SoundEvents.ARMOR_EQUIP_DIAMOND, 3f, 0f, () -> Items.EMERALD),
    OBSIDIAN(40, new int[] {4, 7, 9, 4}, 15, SoundEvents.ARMOR_EQUIP_NETHERITE, 3f, 0.5f, Blocks.OBSIDIAN::asItem),
    REDSTONE(3, new int[] {8, 10, 12, 7}, 40, SoundEvents.ARMOR_EQUIP_NETHERITE, 0f, 1f, () -> Items.REDSTONE),
    RUSTED_IRON(15, new int[] {2, 5, 6, 2}, 9, SoundEvents.ARMOR_EQUIP_IRON, 0f, 0f, Misc.RUSTED_IRON_INGOT),
    SILVER(15, new int[] {1, 3, 5, 2}, 25, SoundEvents.ARMOR_EQUIP_GOLD, 0f, 0f, Misc.SILVER_INGOT),
    ENDERITE(74, new int[]{6, 12, 16, 6}, 30, SoundEvents.ARMOR_EQUIP_NETHERITE, 6, 0.2f, Misc.ENDERITE_INGOT);

    private static final int[] MAX_DAMAGE = {13, 15, 16, 11};
    private final int maxDamageFactor;
    private final int[] damageReductionAmountArray;
    private final int enchantability;
    private final SoundEvent soundEvent;
    private final float toughness;
    private final float knockbackResistance;
    private final Supplier<Item> repairItem;

    PMArmorMaterial(int maxDamage, int[] damageReduction, int enchantLevel, SoundEvent sound, float toughness, float knockbackResistance, Supplier<Item> repairItem) {
        maxDamageFactor = maxDamage;
        damageReductionAmountArray = damageReduction;
        enchantability = enchantLevel;
        this.toughness = toughness;
        this.knockbackResistance = knockbackResistance;
        this.repairItem = repairItem;
        soundEvent = sound;
    }

    @Override
    public int getDurabilityForType(ArmorItem.Type type) {
        return MAX_DAMAGE[type.ordinal()] * maxDamageFactor;
    }

    @Override
    public int getDefenseForType(ArmorItem.Type type) {
        return this.damageReductionAmountArray[type.ordinal()];
    }

    @Override
    public int getEnchantmentValue() { return enchantability; }

    @Override
    public SoundEvent getEquipSound() { return soundEvent; }

    @Override
    public Ingredient getRepairIngredient() { return Ingredient.of(repairItem.get()); }

    @Override
    public String getName() { return ParadiseMod.ID + ":" + getShortName(); }

    @Override
    public float getToughness() { return toughness; }

    @Override
    public float getKnockbackResistance() { return knockbackResistance; }

    public String getShortName() { return name().toLowerCase(); }

    public ItemLike craftingItem() {
        return switch(this) {
            case EMERALD -> Items.EMERALD;
            case RUBY -> Misc.RUBY;
            case OBSIDIAN -> Blocks.OBSIDIAN;
            case REDSTONE -> Items.REDSTONE;
            case RUSTED_IRON -> Misc.RUSTED_IRON_INGOT;
            case SILVER -> Misc.SILVER_INGOT;
            case ENDERITE -> Misc.ENDERITE_INGOT;
        };
    }

    public @Nullable Item upgradableFrom(ArmorItem.Type type) {
        return switch (type) {
            case BOOTS -> switch(this) {
                case EMERALD, RUBY, OBSIDIAN -> Items.DIAMOND_BOOTS;
                case ENDERITE -> Items.NETHERITE_BOOTS;
                default -> null;
            };

            case LEGGINGS -> switch(this) {
                case EMERALD, RUBY, OBSIDIAN -> Items.DIAMOND_LEGGINGS;
                case ENDERITE -> Items.NETHERITE_LEGGINGS;
                default -> null;
            };

            case CHESTPLATE -> switch(this) {
                case EMERALD, RUBY, OBSIDIAN -> Items.DIAMOND_CHESTPLATE;
                case ENDERITE -> Items.NETHERITE_CHESTPLATE;
                default -> null;
            };

            case HELMET -> switch(this) {
                case EMERALD, RUBY, OBSIDIAN -> Items.DIAMOND_HELMET;
                case ENDERITE -> Items.NETHERITE_HELMET;
                default -> null;
            };
        };
    }

    public Item getRepairItem() { return repairItem.get(); }
}