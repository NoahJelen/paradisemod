package net.paradisemod.misc;

import java.util.ArrayList;

import javax.annotation.Nullable;

import net.minecraft.client.renderer.Sheets;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderers;
import net.minecraft.client.renderer.blockentity.HangingSignRenderer;
import net.minecraft.client.renderer.blockentity.SignRenderer;
import net.minecraft.core.BlockPos;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.food.FoodProperties;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.GlassBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.WoodType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.BlockTemplates;
import net.paradisemod.base.PMBlockSetTypes;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.base.registry.RegisteredItem;
import net.paradisemod.building.Building;
import net.paradisemod.building.Slabs;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.misc.tile.CustomBarrelEntity;
import net.paradisemod.misc.tile.CustomHangingSignEntity;
import net.paradisemod.misc.tile.CustomSignEntity;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.Ores;
import net.paradisemod.world.fluid.PMFluids;

@SuppressWarnings("unchecked")
public class Misc {
    private static final ArrayList<WoodType> SIGN_WOOD_TYPES = new ArrayList<>();

    // basic material items
    public static final RegisteredItem ENDERITE_INGOT = regBasicItem("enderite_ingot")
        .tags(
            PMTags.Items.ENDERITE_INGOTS,
            PMTags.Items.PRESENTS
        )
        .oreRecipes(Ores.ENDERITE_ORE)
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.MISC, item, 9, Ores.ENDERITE_BLOCK))
        .localizedName("Enderite Ingot", "Lingote de metal del Fin");

    public static final RegisteredItem RUBY = regBasicItem("ruby")
        .tags(
            PMTags.Items.RUBY_GEMS,
            PMTags.Items.PRESENTS
        )
        .oreRecipes(Ores.RUBY_ORE)
        .oreRecipes(Ores.END_RUBY_ORE)
        .oreRecipes(Ores.DEEPSLATE_RUBY_ORE)
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.MISC, item, 9, Ores.RUBY_BLOCK))
        .localizedName("Ruby", "Rubí");

    public static final RegisteredItem RUSTED_IRON_NUGGET = regBasicItem("rusted_iron_nugget")
        .tag(PMTags.Items.RUSTED_IRON_NUGGETS)
        .localizedName("Rusted Iron Nugget", "Pepita de hierro oxidado")
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.MISC, item, 9, Misc.RUSTED_IRON_INGOT));

    public static final RegisteredItem RUSTED_IRON_INGOT = regBasicItem("rusted_iron_ingot")
        .tags(
            PMTags.Items.RUSTED_IRON_INGOTS,
            PMTags.Items.PRESENTS
        )
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.MISC, item, 9, Ores.RUSTED_IRON_BLOCK))
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item)
                .pattern("iii")
                .pattern("iii")
                .pattern("iii")
                .define('i', RUSTED_IRON_NUGGET)
        )
        .localizedName("Rusted Iron Ingot", "Lingote de hierro oxidado");

    public static final RegisteredItem SILVER_NUGGET = regBasicItem("silver_nugget")
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.MISC, item, 9, Misc.SILVER_INGOT))
        .tag(PMTags.Items.SILVER_NUGGETS)
        .localizedName("Silver Nugget", "Pepita de plata");

    public static RegisteredItem SILVER_INGOT = regBasicItem("silver_ingot")
        .tags(
            PMTags.Items.SILVER_INGOTS,
            PMTags.Items.PRESENTS
        )
        .oreRecipes(Ores.SILVER_ORE)
        .oreRecipes(Ores.DEEPSLATE_SILVER_ORE)
        .oreRecipes(Ores.DARKSTONE_SILVER_ORE)
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.MISC, item, 9, Ores.SILVER_BLOCK))
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item)
                .pattern("iii")
                .pattern("iii")
                .pattern("iii")
                .define('i', SILVER_NUGGET)
        )
        .localizedName("Silver Ingot", "Lingote de plata");

    public static final RegisteredItem ASPHALT_SHARD = regBasicItem("asphalt_shard")
        .tag(ItemTags.COALS)
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.MISC, item, 4, Building.ASPHALT))
        .localizedName("Asphalt Shard", "Fragmento de asfalto");

    public static final RegisteredItem SALT_ITEM = regBasicItem("salt")
        .tag(PMTags.Items.SALT_DUSTS)
        .oreRecipes(Ores.SALT_ORE)
        .oreRecipes(Ores.DEEPSLATE_SALT_ORE)
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.MISC, item, 9, Ores.COMPACT_SALT_BLOCK))
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.MISC, item, 4, Ores.SALT_BLOCK))
        .localizedName("Salt", "Sal");

    // food
    public static final RegisteredItem COOKED_EGG = regFood("cooked_egg", 8, 4, true)
        .cookedFoodRecipes(Items.EGG)
        .localizedName("Cooked Egg", "Huevo cocinado");

    public static final RegisteredItem BEEF_JERKY = regFood("beef_jerky", 5, 2.5F, true)
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.FOOD, item, Items.BEEF, SALT_ITEM))
        .localizedName("Beef Jerky", "Carne seca");

    public static final RegisteredItem CHICKEN_JERKY = regFood("chicken_jerky", 4, 2, true)
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.FOOD, item, Items.CHICKEN, SALT_ITEM))
        .localizedName("Chicken Jerky", "Pollo seco");

    public static final RegisteredItem MUTTON_JERKY = regFood("mutton_jerky", 4, 2, true)
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.FOOD, item, Items.MUTTON, SALT_ITEM))
        .localizedName("Mutton Jerky", "Carne ovina seca");

    public static final RegisteredItem PORK_JERKY = regFood("pork_jerky", 5, 2.5F, true)
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.FOOD, item, Items.PORKCHOP, SALT_ITEM))
        .localizedName("Pork Jerky", "Puerco seco");

    public static final RegisteredItem SQUID = regFood("squid", 2, 1, true)
        .localizedName("Squid", "Calamar");

    public static final RegisteredItem COOKED_SQUID = regFood("cooked_squid", 6, 3, true)
        .cookedFoodRecipes(SQUID)
        .localizedName("Cooked Squid", "Calamar cocinado");

    public static final RegisteredItem NOPAL = regFood("nopal", 1, .5F, false)
        .localizedName("Nopal", "Nopal");

    public static final RegisteredItem PRICKLY_PEAR_FRUIT = regFood("prickly_pear_fruit", 4, 2, false)
        .localizedName("Prickly Pear Fruit", "Fruta de nopal");

    public static final RegisteredItem SOUL_PUMPKIN_PIE = regFood("soul_pumpkin_pie", 8, 3, false)
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.FOOD, item, Decoration.SOUL_PUMPKIN, Items.SUGAR, Items.EGG))
        .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.FOOD, item, Decoration.CARVED_SOUL_PUMPKIN, Items.SUGAR, Items.EGG))
        .localizedName("Soul Pumpkin Pie", "Pastel de calabaza de almas");

    public static final RegisteredItem CHERRY = regFood("cherry", 2, 1, false)
        .localizedName("Cherry", "Cereza");

    public static final RegisteredItem ACORN = regFood("acorn", 1, .5F, false)
        .localizedName("Acorn", "Bellota");

    public static final RegisteredItem COOKED_ACORN = regFood("cooked_acorn", 4, 2, false)
        .cookedFoodRecipes(ACORN)
        .localizedName("Cooked Acorn", "Bellota cocinada");

    public static final RegisteredItem HONEY_CRYSTAL = regFood("honey_crystal", 2, .1F, false)
        .localizedName("Honey Crystal", "Cristal de miel");

    // barrels
    // who knows when the charm mod will be ported to the recent version of Forge/NeoForge
    public static final RegisteredBlock ACACIA_BARREL = BlockTemplates.barrel("acacia", false, Blocks.ACACIA_PLANKS, Blocks.ACACIA_SLAB);
    public static final RegisteredBlock BIRCH_BARREL = BlockTemplates.barrel("birch", false, Blocks.BIRCH_PLANKS, Blocks.BIRCH_SLAB);
    public static final RegisteredBlock CHERRY_BARREL = BlockTemplates.barrel("cherry", false, Blocks.CHERRY_PLANKS, Blocks.CHERRY_SLAB);
    public static final RegisteredBlock CRIMSON_BARREL = BlockTemplates.barrel("crimson", false, Blocks.CRIMSON_PLANKS, Blocks.CRIMSON_SLAB);
    public static final RegisteredBlock DARK_OAK_BARREL = BlockTemplates.barrel("dark_oak", false, Blocks.DARK_OAK_PLANKS, Blocks.DARK_OAK_SLAB);
    public static final RegisteredBlock JUNGLE_BARREL = BlockTemplates.barrel("jungle", false, Blocks.JUNGLE_PLANKS, Blocks.JUNGLE_SLAB);
    public static final RegisteredBlock MANGROVE_BARREL = BlockTemplates.barrel("mangrove", false, Blocks.MANGROVE_PLANKS, Blocks.MANGROVE_SLAB);
    public static final RegisteredBlock SPRUCE_BARREL = BlockTemplates.barrel("spruce", false, Blocks.SPRUCE_PLANKS, Blocks.SPRUCE_SLAB);
    public static final RegisteredBlock WARPED_BARREL = BlockTemplates.barrel("warped", false, Blocks.WARPED_PLANKS, Blocks.WARPED_SLAB);
    public static final RegisteredBlock BAMBOO_BARREL = BlockTemplates.barrel("bamboo", false, Blocks.BAMBOO_PLANKS, Blocks.BAMBOO_SLAB);
    public static final RegisteredBlock CACTUS_BARREL = BlockTemplates.barrel("cactus", false, Building.CACTUS_BLOCK, Slabs.CACTUS_SLAB);
    public static final RegisteredBlock PALO_VERDE_BARREL = BlockTemplates.barrel("palo_verde", false, Building.PALO_VERDE_PLANKS, Slabs.PALO_VERDE_SLAB);
    public static final RegisteredBlock MESQUITE_BARREL = BlockTemplates.barrel("mesquite", false, Building.MESQUITE_PLANKS, Slabs.MESQUITE_SLAB);

    public static final RegisteredBlock GLOWING_CACTUS_BARREL = BlockTemplates.barrel("glowing_cactus", false, DeepDarkBlocks.GLOWING_CACTUS_BLOCK, Slabs.GLOWING_CACTUS_SLAB)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_OAK_BARREL = BlockTemplates.barrel("blackened_oak", false, DeepDarkBlocks.BLACKENED_OAK_PLANKS, Slabs.BLACKENED_OAK_SLAB)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_SPRUCE_BARREL = BlockTemplates.barrel("blackened_spruce", false, DeepDarkBlocks.BLACKENED_SPRUCE_PLANKS, Slabs.BLACKENED_SPRUCE_SLAB)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_OAK_BARREL = BlockTemplates.barrel("glowing_oak", true, DeepDarkBlocks.GLOWING_OAK_PLANKS, Slabs.GLOWING_OAK_SLAB)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    // wood types for signs
    public static final WoodType CACTUS_TYPE = createWoodType("cactus");
    public static final WoodType GLOWING_CACTUS_TYPE = createWoodType("glowing_cactus");
    public static final WoodType PALO_VERDE_TYPE = createWoodType("palo_verde");
    public static final WoodType MESQUITE_TYPE = createWoodType("mesquite");
    public static final WoodType BLACKENED_OAK_TYPE = createWoodType("blackened_oak");
    public static final WoodType BLACKENED_SPRUCE_TYPE = createWoodType("blackened_spruce");
    public static final WoodType GLOWING_OAK_TYPE = createWoodType("glowing_oak");

    // signs
    public static final RegisteredBlock CACTUS_SIGN = BlockTemplates.sign("cactus", CACTUS_TYPE, false, false);
    public static final RegisteredBlock CACTUS_WALL_SIGN = BlockTemplates.sign("cactus", CACTUS_TYPE, true, false);
    public static final RegisteredItem CACTUS_SIGN_ITEM = BlockTemplates.signItem("cactus", CACTUS_SIGN, CACTUS_WALL_SIGN, Building.CACTUS_BLOCK);
    public static final RegisteredBlock CACTUS_HANGING_SIGN = BlockTemplates.hangingSign("cactus", CACTUS_TYPE, false, false);
    public static final RegisteredBlock CACTUS_WALL_HANGING_SIGN = BlockTemplates.hangingSign("cactus", CACTUS_TYPE, true, false);
    public static final RegisteredItem CACTUS_HANGING_SIGN_ITEM = BlockTemplates.hangingSignItem("cactus", CACTUS_HANGING_SIGN, CACTUS_WALL_HANGING_SIGN, Building.CACTUS_BLOCK);

    public static final RegisteredBlock GLOWING_CACTUS_SIGN = BlockTemplates.sign("glowing_cactus", GLOWING_CACTUS_TYPE, false, false);
    public static final RegisteredBlock GLOWING_CACTUS_WALL_SIGN = BlockTemplates.sign("glowing_cactus", GLOWING_CACTUS_TYPE, true, false);

    public static final RegisteredItem GLOWING_CACTUS_SIGN_ITEM = BlockTemplates.signItem("glowing_cactus", GLOWING_CACTUS_SIGN, GLOWING_CACTUS_WALL_SIGN, DeepDarkBlocks.GLOWING_CACTUS_BLOCK)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_CACTUS_HANGING_SIGN = BlockTemplates.hangingSign("glowing_cactus", GLOWING_CACTUS_TYPE, false, false);
    public static final RegisteredBlock GLOWING_CACTUS_WALL_HANGING_SIGN = BlockTemplates.hangingSign("glowing_cactus", GLOWING_CACTUS_TYPE, true, false);

    public static final RegisteredItem GLOWING_CACTUS_HANGING_SIGN_ITEM = BlockTemplates.hangingSignItem("glowing_cactus", GLOWING_CACTUS_HANGING_SIGN, GLOWING_CACTUS_WALL_HANGING_SIGN, DeepDarkBlocks.GLOWING_CACTUS_BLOCK)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock PALO_VERDE_SIGN = BlockTemplates.sign("palo_verde", PALO_VERDE_TYPE, false, false);
    public static final RegisteredBlock PALO_VERDE_WALL_SIGN = BlockTemplates.sign("palo_verde", PALO_VERDE_TYPE, true, false);
    public static final RegisteredItem PALO_VERDE_SIGN_ITEM = BlockTemplates.signItem("palo_verde", PALO_VERDE_SIGN, PALO_VERDE_WALL_SIGN, Building.PALO_VERDE_PLANKS);
    public static final RegisteredBlock PALO_VERDE_HANGING_SIGN = BlockTemplates.hangingSign("palo_verde", PALO_VERDE_TYPE, false, false);
    public static final RegisteredBlock PALO_VERDE_WALL_HANGING_SIGN = BlockTemplates.hangingSign("palo_verde", PALO_VERDE_TYPE, true, false);
    public static final RegisteredItem PALO_VERDE_HANGING_SIGN_ITEM = BlockTemplates.hangingSignItem("palo_verde", PALO_VERDE_HANGING_SIGN, PALO_VERDE_WALL_HANGING_SIGN, Building.PALO_VERDE_PLANKS);

    public static final RegisteredBlock MESQUITE_SIGN = BlockTemplates.sign("mesquite", MESQUITE_TYPE, false, false);
    public static final RegisteredBlock MESQUITE_WALL_SIGN = BlockTemplates.sign("mesquite", MESQUITE_TYPE, true, false);
    public static final RegisteredItem MESQUITE_SIGN_ITEM = BlockTemplates.signItem("mesquite", MESQUITE_SIGN, MESQUITE_WALL_SIGN, Building.MESQUITE_PLANKS);
    public static final RegisteredBlock MESQUITE_HANGING_SIGN = BlockTemplates.hangingSign("mesquite", MESQUITE_TYPE, false, false);
    public static final RegisteredBlock MESQUITE_WALL_HANGING_SIGN = BlockTemplates.hangingSign("mesquite", MESQUITE_TYPE, true, false);
    public static final RegisteredItem MESQUITE_HANGING_SIGN_ITEM = BlockTemplates.hangingSignItem("mesquite", MESQUITE_HANGING_SIGN, MESQUITE_WALL_HANGING_SIGN, Building.MESQUITE_PLANKS);

    public static final RegisteredBlock BLACKENED_OAK_SIGN = BlockTemplates.sign("blackened_oak", BLACKENED_OAK_TYPE, false, true);
    public static final RegisteredBlock BLACKENED_OAK_WALL_SIGN = BlockTemplates.sign("blackened_oak", BLACKENED_OAK_TYPE, true, true);

    public static final RegisteredItem BLACKENED_OAK_SIGN_ITEM = BlockTemplates.signItem("blackened_oak", BLACKENED_OAK_SIGN, BLACKENED_OAK_WALL_SIGN, DeepDarkBlocks.BLACKENED_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_OAK_HANGING_SIGN = BlockTemplates.hangingSign("blackened_oak", BLACKENED_OAK_TYPE, false, true);
    public static final RegisteredBlock BLACKENED_OAK_WALL_HANGING_SIGN = BlockTemplates.hangingSign("blackened_oak", BLACKENED_OAK_TYPE, true, true);

    public static final RegisteredItem BLACKENED_OAK_HANGING_SIGN_ITEM = BlockTemplates.hangingSignItem("blackened_oak", BLACKENED_OAK_HANGING_SIGN, BLACKENED_OAK_WALL_HANGING_SIGN, DeepDarkBlocks.BLACKENED_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_SPRUCE_SIGN = BlockTemplates.sign("blackened_spruce", BLACKENED_SPRUCE_TYPE, false, false);
    public static final RegisteredBlock BLACKENED_SPRUCE_WALL_SIGN = BlockTemplates.sign("blackened_spruce", BLACKENED_SPRUCE_TYPE, true, false);

    public static final RegisteredItem BLACKENED_SPRUCE_SIGN_ITEM = BlockTemplates.signItem("blackened_spruce", BLACKENED_SPRUCE_SIGN, BLACKENED_SPRUCE_WALL_SIGN, DeepDarkBlocks.BLACKENED_SPRUCE_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_SPRUCE_HANGING_SIGN = BlockTemplates.hangingSign("blackened_spruce", BLACKENED_SPRUCE_TYPE, false, false);
    public static final RegisteredBlock BLACKENED_SPRUCE_WALL_HANGING_SIGN = BlockTemplates.hangingSign("blackened_spruce", BLACKENED_SPRUCE_TYPE, true, false);

    public static final RegisteredItem BLACKENED_SPRUCE_HANGING_SIGN_ITEM = BlockTemplates.hangingSignItem("blackened_spruce", BLACKENED_SPRUCE_HANGING_SIGN, BLACKENED_SPRUCE_WALL_HANGING_SIGN, DeepDarkBlocks.BLACKENED_SPRUCE_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_OAK_SIGN = BlockTemplates.sign("glowing_oak", GLOWING_OAK_TYPE, false, true);
    public static final RegisteredBlock GLOWING_OAK_WALL_SIGN = BlockTemplates.sign("glowing_oak", GLOWING_OAK_TYPE, true, true);

    public static final RegisteredItem GLOWING_OAK_SIGN_ITEM = BlockTemplates.signItem("glowing_oak", GLOWING_OAK_SIGN, GLOWING_OAK_WALL_SIGN, DeepDarkBlocks.GLOWING_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_OAK_HANGING_SIGN = BlockTemplates.hangingSign("glowing_oak", GLOWING_OAK_TYPE, false, true);
    public static final RegisteredBlock GLOWING_OAK_WALL_HANGING_SIGN = BlockTemplates.hangingSign("glowing_oak", GLOWING_OAK_TYPE, true, true);

    public static final RegisteredItem GLOWING_OAK_HANGING_SIGN_ITEM = BlockTemplates.hangingSignItem("glowing_oak", GLOWING_OAK_HANGING_SIGN, GLOWING_OAK_WALL_HANGING_SIGN, DeepDarkBlocks.GLOWING_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    // tile entities
    public static final RegistryObject<BlockEntityType<CustomBarrelEntity>> CUSTOM_BARREL_TILE = PMRegistries.createTile("custom_barrel", CustomBarrelEntity::new,
        CACTUS_BARREL,
        GLOWING_CACTUS_BARREL,
        BAMBOO_BARREL,
        PALO_VERDE_BARREL,
        MESQUITE_BARREL,
        BLACKENED_OAK_BARREL,
        BLACKENED_SPRUCE_BARREL,
        GLOWING_OAK_BARREL,
        ACACIA_BARREL,
        BIRCH_BARREL,
        CHERRY_BARREL,
        CRIMSON_BARREL,
        DARK_OAK_BARREL,
        JUNGLE_BARREL,
        MANGROVE_BARREL,
        SPRUCE_BARREL,
        WARPED_BARREL
    );

    public static final RegistryObject<BlockEntityType<CustomSignEntity>> CUSTOM_SIGN_TILE = PMRegistries.createTile("custom_sign", CustomSignEntity::new,
        CACTUS_SIGN, CACTUS_WALL_SIGN,
        GLOWING_CACTUS_SIGN, GLOWING_CACTUS_WALL_SIGN,
        PALO_VERDE_SIGN, PALO_VERDE_WALL_SIGN,
        MESQUITE_SIGN, MESQUITE_WALL_SIGN,
        BLACKENED_OAK_SIGN, BLACKENED_OAK_WALL_SIGN,
        BLACKENED_SPRUCE_SIGN, BLACKENED_SPRUCE_WALL_SIGN,
        GLOWING_OAK_SIGN, GLOWING_OAK_WALL_SIGN
    );

    public static final RegistryObject<BlockEntityType<CustomHangingSignEntity>> CUSTOM_HANGING_SIGN_TILE = PMRegistries.createTile("custom_hanging_sign", CustomHangingSignEntity::new,
        CACTUS_HANGING_SIGN, CACTUS_WALL_HANGING_SIGN,
        GLOWING_CACTUS_HANGING_SIGN, GLOWING_CACTUS_WALL_HANGING_SIGN,
        PALO_VERDE_HANGING_SIGN, PALO_VERDE_WALL_HANGING_SIGN,
        MESQUITE_HANGING_SIGN, MESQUITE_WALL_HANGING_SIGN,
        BLACKENED_OAK_HANGING_SIGN, BLACKENED_OAK_WALL_HANGING_SIGN,
        BLACKENED_SPRUCE_HANGING_SIGN, BLACKENED_SPRUCE_WALL_HANGING_SIGN,
        GLOWING_OAK_HANGING_SIGN, GLOWING_OAK_WALL_HANGING_SIGN
    );

    // other blocks
    public static final RegisteredBlock PRISMARINE_CRYSTAL_BLOCK = PMRegistries.regBlockItem(
        "prismarine_crystal_block",
        () -> new GlassBlock(
            Block.Properties.copy(Blocks.GLASS)
                .sound(SoundType.GLASS)
                .lightLevel(s -> 15)
                .noOcclusion()
                .strength(0.3F)
        )
    )
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.DECORATIONS, item)
                .pattern("cc")
                .pattern("cc")
                .define('c', Items.PRISMARINE_CRYSTALS)
        )
        .tab(CreativeModeTabs.NATURAL_BLOCKS)
        .renderType("translucent")
        .lootTable((block, generator) -> generator.multipleItemDrop(block, Items.PRISMARINE_CRYSTALS, 4))
        .localizedName("Prismarine Crystal Block", "Bloque de cristales de prismarina");

    public static final RegisteredBlock HONEY_CRYSTAL_BLOCK = PMRegistries.regBlockItem(
        "honey_crystal_block",
        () -> new GlassBlock(
            Block.Properties.copy(Blocks.GLASS)
                .sound(SoundType.GLASS)
                .lightLevel(s -> 15)
                .noOcclusion()
                .strength(0.3F)
        )
    )
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.DECORATIONS, item)
                .pattern("hh")
                .pattern("hh")
                .define('h', HONEY_CRYSTAL)
        )
        .tab(CreativeModeTabs.NATURAL_BLOCKS)
        .renderType("translucent")
        .tag(PMTags.Blocks.CARVABLES)
        .lootTable((block, generator) -> generator.multipleItemDrop(block, HONEY_CRYSTAL, 4))
        .localizedName("Honey Crystal Block", "Bloque de cristales de miel");

    public static RegisteredBlock GLOWING_ICE = PMRegistries.regBlockItem(
        "glowing_ice",
        () -> new GlassBlock(Block.Properties.copy(Blocks.ICE).sound(SoundType.GLASS).lightLevel(state -> 7).friction(1f).noOcclusion().strength(0.3F)) {
            @Override
            public void playerDestroy(Level world, Player player, BlockPos pos, BlockState state, @Nullable BlockEntity te, ItemStack stack) {
                var enchantments = stack.getAllEnchantments().keySet();
                if(!enchantments.contains(Enchantments.SILK_TOUCH))
                    world.setBlockAndUpdate(pos, PMFluids.GLOWING_WATER_BLOCK.get().defaultBlockState());
            }
        }
    )
        .tab(CreativeModeTabs.NATURAL_BLOCKS)
        .renderType("translucent")
        .tag(PMTags.Blocks.CARVABLES)
        .localizedName("Glowing Ice", "Hielo brillante");

    public static final RegisteredBlock SALT_LAMP = PMRegistries.regBlockItem(
        "salt_lamp",
        () -> new GlassBlock(
            Block.Properties.copy(Blocks.GLASS)
                .sound(SoundType.GLASS)
                .lightLevel(s -> 15)
                .noOcclusion()
                .strength(0.3F)
        )
    )
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.DECORATIONS, item)
                .pattern("sss")
                .pattern("sbs")
                .pattern("sss")
                .define('s', SALT_ITEM)
                .define('b', Items.BLAZE_POWDER)
        )
        .tabs(
            CreativeModeTabs.BUILDING_BLOCKS,
            CreativeModeTabs.FUNCTIONAL_BLOCKS
        )
        .tag(PMTags.Blocks.CARVABLES)
        .localizedName("Salt Lamp", "Luz de sal");

    public static void init(IEventBus eventbus) {
        createSignLootTable(CACTUS_SIGN, CACTUS_WALL_SIGN, CACTUS_SIGN_ITEM);
        createSignLootTable(GLOWING_CACTUS_SIGN, GLOWING_CACTUS_WALL_SIGN, GLOWING_CACTUS_SIGN_ITEM);
        createSignLootTable(PALO_VERDE_SIGN, PALO_VERDE_WALL_SIGN, PALO_VERDE_SIGN_ITEM);
        createSignLootTable(MESQUITE_SIGN, MESQUITE_WALL_SIGN, MESQUITE_SIGN_ITEM);
        createSignLootTable(BLACKENED_OAK_SIGN, BLACKENED_OAK_WALL_SIGN, BLACKENED_OAK_SIGN_ITEM);
        createSignLootTable(BLACKENED_SPRUCE_SIGN, BLACKENED_SPRUCE_WALL_SIGN, BLACKENED_SPRUCE_SIGN_ITEM);
        createSignLootTable(GLOWING_OAK_SIGN, GLOWING_OAK_WALL_SIGN, GLOWING_OAK_SIGN_ITEM);
        createSignLootTable(CACTUS_HANGING_SIGN, CACTUS_WALL_HANGING_SIGN, CACTUS_HANGING_SIGN_ITEM);
        createSignLootTable(GLOWING_CACTUS_HANGING_SIGN, GLOWING_CACTUS_WALL_HANGING_SIGN, GLOWING_CACTUS_HANGING_SIGN_ITEM);
        createSignLootTable(PALO_VERDE_HANGING_SIGN, PALO_VERDE_WALL_HANGING_SIGN, PALO_VERDE_HANGING_SIGN_ITEM);
        createSignLootTable(MESQUITE_HANGING_SIGN, MESQUITE_WALL_HANGING_SIGN, MESQUITE_HANGING_SIGN_ITEM);
        createSignLootTable(BLACKENED_OAK_HANGING_SIGN, BLACKENED_OAK_WALL_HANGING_SIGN, BLACKENED_OAK_HANGING_SIGN_ITEM);
        createSignLootTable(BLACKENED_SPRUCE_HANGING_SIGN, BLACKENED_SPRUCE_WALL_HANGING_SIGN, BLACKENED_SPRUCE_HANGING_SIGN_ITEM);
        createSignLootTable(GLOWING_OAK_HANGING_SIGN, GLOWING_OAK_WALL_HANGING_SIGN, GLOWING_OAK_HANGING_SIGN_ITEM);

        // submodules
        Chests.init(eventbus);
        Armor.init();
        Tools.init();

        eventbus.addListener(Misc::regSignTypes);

        SILVER_INGOT = SILVER_INGOT.recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.DECORATIONS, item)
                .pattern("iii")
                .pattern("iii")
                .pattern("iii")
                .define('i', SILVER_NUGGET)
        );

        ParadiseMod.LOG.info("Loaded Miscellaneous module");
    }

    private static WoodType createWoodType(String name) {
        var woodType = new WoodType("paradisemod:" + name, PMBlockSetTypes.VARIANT_WOOD);
        SIGN_WOOD_TYPES.add(woodType);
        return woodType;
    }

    private static void createSignLootTable(RegisteredBlock sign, RegisteredBlock wallSign, RegisteredItem signItem) {
        sign = sign.dropsItem(signItem);
        wallSign = wallSign.dropsItem(signItem);
    }

    private static void regSignTypes(final FMLCommonSetupEvent event) {
        event.enqueueWork(
            () -> {
                for(var woodType : SIGN_WOOD_TYPES)
                    WoodType.register(woodType);
            }
        );
    }

    public static void initClient(FMLClientSetupEvent event) {
        BlockEntityRenderers.register(CUSTOM_SIGN_TILE.get(), SignRenderer::new);
        BlockEntityRenderers.register(CUSTOM_HANGING_SIGN_TILE.get(), HangingSignRenderer::new);

        event.enqueueWork(
            () -> {
                for(var woodType : SIGN_WOOD_TYPES)
                    Sheets.addWoodType(woodType);
            }
        );
    }

    // generates and registers a food item
    private static RegisteredItem regFood(String name, int hungerPoints, float saturation, boolean wolfFood) {
        var foodProps = new FoodProperties.Builder().nutrition(hungerPoints).saturationMod(saturation);
        if(wolfFood) foodProps = foodProps.meat();
        var properties = new Item.Properties().food(foodProps.build()).stacksTo(64);
        return PMRegistries.regItem(name, () -> new Item(properties))
            .tab(CreativeModeTabs.FOOD_AND_DRINKS);
    }

    protected static RegisteredItem regBasicItem(String name) {
        return PMRegistries.regItem(name, () -> new Item(new Item.Properties()))
            .tab(CreativeModeTabs.INGREDIENTS);
    }
}