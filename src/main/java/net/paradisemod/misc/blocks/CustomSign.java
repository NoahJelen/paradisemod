package net.paradisemod.misc.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.StandingSignBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.WoodType;
import net.paradisemod.misc.tile.CustomSignEntity;

public class CustomSign extends StandingSignBlock {
    public CustomSign(WoodType woodType) { super(Block.Properties.copy(Blocks.OAK_SIGN), woodType); }

    public CustomSign(WoodType woodType, int light) { super(Block.Properties.copy(Blocks.OAK_SIGN).lightLevel(s -> light), woodType); }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) { return new CustomSignEntity(pos, state); }
}