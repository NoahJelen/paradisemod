package net.paradisemod.misc.blocks;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.stats.Stats;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.monster.piglin.PiglinAi;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BarrelBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.paradisemod.base.BlockType;
import net.paradisemod.misc.tile.CustomBarrelEntity;

public class CustomBarrel extends BarrelBlock {
    public CustomBarrel() { super(BlockType.WOOD.getProperties()); }

    public CustomBarrel(int light) { super(BlockType.WOOD.getProperties().lightLevel(s -> light)); }

    @Override
    public InteractionResult use(BlockState state, Level world, BlockPos pos, Player player, InteractionHand handIn, BlockHitResult hit) {
        if (world.isClientSide) return InteractionResult.SUCCESS;

        else {
            var tile = world.getBlockEntity(pos);
            if (tile instanceof CustomBarrelEntity barrel) {
                player.openMenu(barrel);
                player.awardStat(Stats.OPEN_BARREL);
                PiglinAi.angerNearbyPiglins(player, true);
            }

            return InteractionResult.CONSUME;
        }
    }

    @Override
    public void tick(BlockState state, ServerLevel world, BlockPos pos, RandomSource rand) {
        var tile = world.getBlockEntity(pos);
        if (tile instanceof CustomBarrelEntity barrel) barrel.recheckOpen();
    }

    @Override
    public void setPlacedBy(Level world, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack stack) {
        if (stack.hasCustomHoverName()) {
            var tile = world.getBlockEntity(pos);
            if (tile instanceof CustomBarrelEntity barrel) barrel.setCustomName(stack.getHoverName());
        }
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) { return new CustomBarrelEntity(pos, state); }
}