package net.paradisemod.misc.blocks;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.ChestBlock;
import net.minecraft.world.level.block.entity.ChestBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;
import net.paradisemod.building.Building;
import net.paradisemod.misc.Chests;
import net.paradisemod.misc.tile.CustomChestEntity;
import net.paradisemod.world.DeepDarkBlocks;

public class CustomChest extends ChestBlock implements ModeledBlock {
    private final Type chestType;
    private final boolean trapped;

    public CustomChest(Type chestType, boolean isTrapped) {
        super(BlockType.WOOD.getProperties().lightLevel(s -> chestType.getLightLevel()), Chests.CHEST_TILE::get);
        this.chestType = chestType;
        trapped = isTrapped;
    }

    @Override
    public CustomChestEntity newBlockEntity(BlockPos pos, BlockState state) { return new CustomChestEntity(pos, state, chestType, trapped); }

    @Override
    public boolean isSignalSource(@Nullable BlockState state) { return trapped; }

    @Override
    public int getSignal(BlockState blockState, BlockGetter world, BlockPos pos, Direction side) {
        if(trapped) {
            var numPlayers = ChestBlockEntity.getOpenCount(world, pos);
            return Math.min(numPlayers, 15);
        }
        else return 0;
    }

    @Override
    public int getDirectSignal(BlockState blockState, BlockGetter blockAccess, BlockPos pos, Direction side) {
        if (trapped) return side == Direction.UP ? blockState.getSignal(blockAccess, pos, side) : 0;
        else return 0;
    }

    @Override
    public void genBlockState(BlockStateGenerator generator) {
        var model = generator.models()
            .getBuilder("block/chest/" + chestType.getName() + (trapped ? "_trapped" : ""))
            .texture("particle", chestType.particleTexture());

        generator.simpleBlock(this, model);
    }

    // in order to add new chests in future, all I have to do is add a value to this enum type
    // and I will get that chest and a trapped variation of that chest
    public enum Type {
        DUMMY1(false, 0),
        DUMMY2(false, 0),
        DUMMY3(false, 0),
        CACTUS(false, 0),
        PALO_VERDE(false, 0),
        MESQUITE(false, 0),
        BLACKENED_OAK(true, 0),
        BLACKENED_SPRUCE(true, 0),
        GLOWING_OAK(true, 7),
        GLOWING_CACTUS(true, 7);

        private final boolean deepDarkChest;
        private final int light;

        Type(boolean deepDarkChest, int light) {
            this.deepDarkChest = deepDarkChest;
            this.light = light;
        }

        public boolean isDeepDarkChest() { return deepDarkChest; }
        public int getLightLevel() { return light; }

        public String getName() {
            return name().toLowerCase();
        }

        public String localizedName(boolean spanish, boolean trapped) {
            return switch(this) {
                case DUMMY1 -> spanish ? "Cofre falso 1" + (trapped ? " con trampa" : "") : (trapped ? "Trapped " : "") + "Dummy Chest 1";
                case DUMMY2 -> spanish ? "Cofre falso 2" + (trapped ? " con trampa" : "") : (trapped ? "Trapped " : "") + "Dummy Chest 2";
                case DUMMY3 -> spanish ? "Cofre falso 3" + (trapped ? " con trampa" : "") : (trapped ? "Trapped " : "") + "Dummy Chest 3";
                default ->
                    spanish ? "Cofre de " + Utils.localizedMaterialName(getName(), true) + (trapped ? " con trampa" : "") : (trapped ? "Trapped " : "") + Utils.localizedMaterialName(getName(), false) + " Chest";
            };
        }

        public String particleTexture() {
            return switch(this) {
                default -> "paradisemod:block/" + getName() + "_planks";
                case CACTUS, GLOWING_CACTUS -> "paradisemod:block/" + getName() + "_block";
                case DUMMY1 -> "minecraft:block/blue_wool";
                case DUMMY2 -> "minecraft:block/red_wool";
                case DUMMY3 -> "minecraft:block/yellow_wool";
            };
        }

        public @Nullable ItemLike getCraftItem(boolean trapped) {
            return switch(this) {
                case CACTUS -> trapped ? Chests.CACTUS_CHEST : Building.CACTUS_BLOCK;
                case PALO_VERDE -> trapped ? Chests.PALO_VERDE_CHEST : Building.PALO_VERDE_PLANKS;
                case MESQUITE -> trapped ? Chests.MESQUITE_CHEST : Building.MESQUITE_PLANKS;
                case BLACKENED_OAK -> trapped ? Chests.BLACKENED_OAK_CHEST : DeepDarkBlocks.BLACKENED_OAK_PLANKS;
                case BLACKENED_SPRUCE -> trapped ? Chests.BLACKENED_SPRUCE_CHEST : DeepDarkBlocks.BLACKENED_SPRUCE_PLANKS;
                case GLOWING_OAK -> trapped ? Chests.GLOWING_OAK_CHEST : DeepDarkBlocks.GLOWING_OAK_PLANKS;
                case GLOWING_CACTUS -> trapped ? Chests.GLOWING_CACTUS_CHEST : DeepDarkBlocks.GLOWING_CACTUS_BLOCK;
                default -> null;
            };
        }
    }
}
