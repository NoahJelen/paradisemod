package net.paradisemod.misc.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.WallHangingSignBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.WoodType;
import net.paradisemod.misc.Misc;
import net.paradisemod.misc.tile.CustomHangingSignEntity;

public class CustomWallHangingSign extends WallHangingSignBlock {
    public CustomWallHangingSign(WoodType type, int light) {
        super(Block.Properties.copy(Blocks.OAK_SIGN).lightLevel(s -> light), type);
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return new CustomHangingSignEntity(pos, state);
    }

    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level world, BlockState state, BlockEntityType<T> blockEntityType) {
        return createTickerHelper(blockEntityType, Misc.CUSTOM_HANGING_SIGN_TILE.get(), CustomHangingSignEntity::tick);
    }
}