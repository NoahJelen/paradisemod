package net.paradisemod.misc.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SaplingBlock;
import net.minecraft.world.level.block.grower.AbstractTreeGrower;
import net.minecraft.world.level.block.state.BlockState;
import net.paradisemod.world.DeepDarkBlocks;

public class DeepDarkSapling extends SaplingBlock {
    public DeepDarkSapling(AbstractTreeGrower tree, boolean glows) {
        super(tree, Properties.copy(Blocks.OAK_SAPLING).lightLevel(s -> glows ? 7 : 0));
    }

    @Override
    protected boolean mayPlaceOn(BlockState state, BlockGetter world, BlockPos pos) {
        if(state.is(DeepDarkBlocks.DARKSTONE.get()) || state.is(DeepDarkBlocks.GLOWING_NYLIUM.get()) || state.is(DeepDarkBlocks.OVERGROWN_DARKSTONE.get())) return true;
        return super.mayPlaceOn(state, world, pos);
    }
}