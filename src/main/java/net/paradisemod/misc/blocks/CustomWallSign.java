package net.paradisemod.misc.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.WallSignBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.WoodType;
import net.paradisemod.misc.tile.CustomSignEntity;

public class CustomWallSign extends WallSignBlock {
    public CustomWallSign(WoodType type) { super(Block.Properties.copy(Blocks.OAK_SIGN), type); }

    public CustomWallSign(WoodType type, int light) { super(Block.Properties.copy(Blocks.OAK_SIGN).lightLevel(s -> light), type); }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) { return new CustomSignEntity(pos, state); }
}
