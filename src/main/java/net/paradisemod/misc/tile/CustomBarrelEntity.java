package net.paradisemod.misc.tile;

import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.ContainerHelper;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ChestMenu;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BarrelBlock;
import net.minecraft.world.level.block.entity.ContainerOpenersCounter;
import net.minecraft.world.level.block.entity.RandomizableContainerBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.paradisemod.misc.Misc;

public class CustomBarrelEntity extends RandomizableContainerBlockEntity {
    private NonNullList<ItemStack> items = NonNullList.withSize(27, ItemStack.EMPTY);

    public CustomBarrelEntity(BlockPos pos, BlockState state) { super(Misc.CUSTOM_BARREL_TILE.get(), pos, state); }

    private final ContainerOpenersCounter openersCounter = new ContainerOpenersCounter() {
        @Override
        protected void onOpen(Level world, BlockPos pos, BlockState state) {
            playSound(state, SoundEvents.BARREL_OPEN);
            updateBlockState(state, true);
        }

        @Override
        protected void onClose(Level world, BlockPos pos, BlockState state) {
            playSound(state, SoundEvents.BARREL_CLOSE);
            updateBlockState(state, false);
        }

        @Override
        protected void openerCountChanged(Level world, BlockPos pos, BlockState state, int count, int openCount) { }

        @Override
        protected boolean isOwnContainer(Player player) {
            if (player.containerMenu instanceof ChestMenu menu) {
                var container = menu.getContainer();
                return container == CustomBarrelEntity.this;
            }
            else return false;
        }
    };

    @Override
    protected void saveAdditional(CompoundTag nbt) {
        super.saveAdditional(nbt);
        if (!trySaveLootTable(nbt)) ContainerHelper.saveAllItems(nbt, this.items);
    }

    @Override
    public void load(CompoundTag nbt) {
        super.load(nbt);
        items = NonNullList.withSize(getContainerSize(), ItemStack.EMPTY);
        if (!tryLoadLootTable(nbt)) ContainerHelper.loadAllItems(nbt, this.items);
    }

    @Override
    public int getContainerSize() { return 27; }

    @Override
    protected NonNullList<ItemStack> getItems() { return items; }

    @Override
    protected void setItems(NonNullList<ItemStack> items) { this.items = items; }

    @Override
    protected Component getDefaultName() { return Component.translatable("container.barrel"); }

    @Override
    protected ChestMenu createMenu(int id, Inventory inventory) { return ChestMenu.threeRows(id, inventory, this); }

    @Override
    public void startOpen(Player player) {
        if (!remove && !player.isSpectator())
            openersCounter.incrementOpeners(player, level, getBlockPos(), getBlockState());
    }

    @Override
    public void stopOpen(Player player) {
        if (!remove && !player.isSpectator())
            openersCounter.decrementOpeners(player, level, getBlockPos(), getBlockState());
    }

    
    public void recheckOpen() {
        if (!remove)
            openersCounter.recheckOpeners(level, getBlockPos(), getBlockState());
    }

    private void updateBlockState(BlockState state, boolean open) {
        level.setBlock(getBlockPos(), state.setValue(BarrelBlock.OPEN, open), 3);
    }

    private void playSound(BlockState state, SoundEvent sound) {
        var vec3D = state.getValue(BarrelBlock.FACING).getNormal();
        var d0 = worldPosition.getX() + 0.5 + vec3D.getX() / 2.;
        var d1 = worldPosition.getY() + 0.5 + vec3D.getY() / 2.;
        var d2 = worldPosition.getZ() + 0.5 + vec3D.getZ() / 2.;
        level.playSound(null, d0, d1, d2, sound, SoundSource.BLOCKS, 0.5F, level.random.nextFloat() * 0.1F + 0.9F);
    }
}