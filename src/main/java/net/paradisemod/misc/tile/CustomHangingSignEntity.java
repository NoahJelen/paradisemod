package net.paradisemod.misc.tile;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.entity.HangingSignBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.paradisemod.misc.Misc;

public class CustomHangingSignEntity extends HangingSignBlockEntity {
    public CustomHangingSignEntity(BlockPos pos, BlockState blockState) { super(pos, blockState); }

    @Override
    public BlockEntityType<CustomHangingSignEntity> getType() {
        return Misc.CUSTOM_HANGING_SIGN_TILE.get();
    }
}