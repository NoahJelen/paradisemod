package net.paradisemod.misc.tile;

import java.util.EnumMap;

import net.minecraft.client.renderer.Sheets;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.client.renderer.blockentity.ChestRenderer;
import net.minecraft.client.resources.model.Material;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.state.properties.ChestType;
import net.paradisemod.ParadiseMod;
import net.paradisemod.misc.blocks.CustomChest;

public class CustomChestRenderer extends ChestRenderer<CustomChestEntity> {
    private static EnumMap<CustomChest.Type, EnumMap<ChestType, Material>> NORMAL_TEXTURES = new EnumMap<>(CustomChest.Type.class);
    private static EnumMap<CustomChest.Type, EnumMap<ChestType, Material>> TRAPPED_TEXTURES = new EnumMap<>(CustomChest.Type.class);

    static {
        for(var chest : CustomChest.Type.values()) {
            EnumMap<ChestType, Material> normalTextures = new EnumMap<>(ChestType.class);
            EnumMap<ChestType, Material> trappedTextures = new EnumMap<>(ChestType.class);
            for(var type : ChestType.values()) {
                normalTextures.put(type, createChesMaterial(chest, type, false));
                trappedTextures.put(type, createChesMaterial(chest, type, true));
            }

            NORMAL_TEXTURES.put(chest, normalTextures);
            TRAPPED_TEXTURES.put(chest, trappedTextures);
        }
    }

    public CustomChestRenderer(BlockEntityRendererProvider.Context context) {
        super(context);
    }

    @Override
    protected Material getMaterial(CustomChestEntity chest, ChestType type) {
        var chestType = chest.getChestType();
        var normalTextures = NORMAL_TEXTURES.get(chestType);
        var trappedTextures = TRAPPED_TEXTURES.get(chestType);
        return (chest.isTrapped()) ? trappedTextures.get(type) : normalTextures.get(type);
    }

    private static Material createChesMaterial(CustomChest.Type chest, ChestType type, boolean trapped) {
        var texName = chest.name().toLowerCase() + (trapped ? "_trapped" : "") + (type == ChestType.SINGLE ? "" : "_" + type.name().toLowerCase());
        return new Material(Sheets.CHEST_SHEET, new ResourceLocation(ParadiseMod.ID, "entity/chest/" + texName));
    }
}