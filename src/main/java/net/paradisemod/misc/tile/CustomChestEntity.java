package net.paradisemod.misc.tile;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.ChestBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.paradisemod.misc.Chests;
import net.paradisemod.misc.blocks.CustomChest;

public class CustomChestEntity extends ChestBlockEntity {
    private final CustomChest.Type chestType;
    private final boolean trapped;

    public CustomChestEntity(BlockPos pos, BlockState state) { this(pos, state, CustomChest.Type.DUMMY1, false); }

    public CustomChestEntity(BlockPos pos, BlockState state, CustomChest.Type chestType, boolean isTrapped) {
        super(Chests.CHEST_TILE.get(), pos, state);
        this.chestType = chestType;
        trapped = isTrapped;
    }

    public CustomChest.Type getChestType() { return chestType; }
    public boolean isTrapped() { return trapped; }

    @Override
    protected void signalOpenCount(Level world, BlockPos pos, BlockState state, int eventId, int eventParam) {
        if(trapped) {
            super.signalOpenCount(world, pos, state, eventId, eventParam);
            if (eventId != eventParam) {
                var block = state.getBlock();
                world.updateNeighborsAt(pos, block);
                world.updateNeighborsAt(pos.below(), block);
            }
        }
        else super.signalOpenCount(world, pos, state, eventId, eventParam);
    }
}