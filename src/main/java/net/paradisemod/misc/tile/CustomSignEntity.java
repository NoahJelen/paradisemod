package net.paradisemod.misc.tile;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.entity.SignBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.paradisemod.misc.Misc;

public class CustomSignEntity extends SignBlockEntity {
    public CustomSignEntity(BlockPos pos, BlockState blockState) { super(pos, blockState); }

    @Override
    public BlockEntityType<CustomSignEntity> getType() { return Misc.CUSTOM_SIGN_TILE.get(); }
}