package net.paradisemod.misc;

import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Supplier;

import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockEntityWithoutLevelRenderer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderers;
import net.minecraft.core.BlockPos;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.client.extensions.common.IClientItemExtensions;
import net.minecraftforge.common.Tags;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.loading.FMLEnvironment;
import net.minecraftforge.registries.ObjectHolder;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.misc.blocks.CustomChest;
import net.paradisemod.misc.tile.CustomChestEntity;
import net.paradisemod.misc.tile.CustomChestRenderer;
import net.paradisemod.world.DeepDarkBlocks;

public class Chests { 
    @ObjectHolder(registryName = "block", value = "paradisemod:cactus_chest")
    public static Block CACTUS_CHEST = Blocks.CHEST;

    @ObjectHolder(registryName = "block", value = "paradisemod:trapped_cactus_chest")
    public static Block TRAPPED_CACTUS_CHEST = Blocks.TRAPPED_CHEST;

    @ObjectHolder(registryName = "block", value = "paradisemod:palo_verde_chest")
    public static Block PALO_VERDE_CHEST = Blocks.CHEST;

    @ObjectHolder(registryName = "block", value = "paradisemod:trapped_palo_verde_chest")
    public static Block TRAPPED_PALO_VERDE_CHEST = Blocks.TRAPPED_CHEST;

    @ObjectHolder(registryName = "block", value = "paradisemod:mesquite_chest")
    public static Block MESQUITE_CHEST = Blocks.CHEST;

    @ObjectHolder(registryName = "block", value = "paradisemod:trapped_mesquite_chest")
    public static Block TRAPPED_MESQUITE_CHEST = Blocks.TRAPPED_CHEST;

    @ObjectHolder(registryName = "block", value = "paradisemod:bamboo_chest")
    public static Block BAMBOO_CHEST = Blocks.CHEST;

    @ObjectHolder(registryName = "block", value = "paradisemod:trapped_bamboo_chest")
    public static Block TRAPPED_BAMBOO_CHEST = Blocks.TRAPPED_CHEST;

    @ObjectHolder(registryName = "block", value = "paradisemod:blackened_oak_chest")
    public static Block BLACKENED_OAK_CHEST = Blocks.CHEST;

    @ObjectHolder(registryName = "block", value = "paradisemod:trapped_blackened_oak_chest")
    public static Block TRAPPED_BLACKENED_OAK_CHEST = Blocks.TRAPPED_CHEST;

    @ObjectHolder(registryName = "block", value = "paradisemod:blackened_spruce_chest")
    public static Block BLACKENED_SPRUCE_CHEST = Blocks.CHEST;

    @ObjectHolder(registryName = "block", value = "paradisemod:trapped_blackened_spruce_chest")
    public static Block TRAPPED_BLACKENED_SPRUCE_CHEST = Blocks.TRAPPED_CHEST;

    @ObjectHolder(registryName = "block", value = "paradisemod:glowing_oak_chest")
    public static Block GLOWING_OAK_CHEST = Blocks.CHEST;

    @ObjectHolder(registryName = "block", value = "paradisemod:trapped_glowing_oak_chest")
    public static Block TRAPPED_GLOWING_OAK_CHEST = Blocks.TRAPPED_CHEST;

    @ObjectHolder(registryName = "block", value = "paradisemod:glowing_cactus_chest")
    public static Block GLOWING_CACTUS_CHEST = Blocks.CHEST;

    @ObjectHolder(registryName = "block", value = "paradisemod:trapped_glowing_cactus_chest")
    public static Block TRAPPED_GLOWING_CACTUS_CHEST = Blocks.TRAPPED_CHEST;

    public static RegistryObject<BlockEntityType<CustomChestEntity>> CHEST_TILE;

    public static void init(IEventBus eventbus) {
        ArrayList<RegisteredBlock> chests = new ArrayList<>();

        for(var type : CustomChest.Type.values()) {
            chests.add(createChest(type, false));
            chests.add(createChest(type, true));
        }

        CHEST_TILE = PMRegistries.createTile("custom_chest", CustomChestEntity::new, chests);
        if (FMLEnvironment.dist.isClient())
            eventbus.addListener(
                (FMLClientSetupEvent event) -> BlockEntityRenderers.register(Chests.CHEST_TILE.get(), CustomChestRenderer::new)
            );
    }

    private static RegisteredBlock createChest(CustomChest.Type chestType, boolean trapped) {
        var name = chestType.name().toLowerCase() + "_chest";
        if(trapped) name = "trapped_" + name;

        var chestBlock = PMRegistries.regBlock(
            name,
            () -> new CustomChest(chestType, trapped)
        )
            .tags(
                Tags.Blocks.CHESTS,
                BlockTags.MINEABLE_WITH_AXE,
                BlockTags.GUARDED_BY_PIGLINS
            );

        Supplier<Item> chestItem = () -> new BlockItem(chestBlock.get(), new Item.Properties()) {
            @Override
            public void initializeClient(Consumer<IClientItemExtensions> consumer) {
                consumer.accept(
                    new IClientItemExtensions() {
                        public BlockEntityWithoutLevelRenderer getCustomRenderer() {
                            var mc = Minecraft.getInstance();

                            return new BlockEntityWithoutLevelRenderer(mc.getBlockEntityRenderDispatcher(), mc.getEntityModels()) {
                                private final BlockEntity tile = new CustomChestEntity(BlockPos.ZERO, getBlock().defaultBlockState(), chestType, trapped);

                                @Override
                                public void renderByItem(ItemStack stack, ItemDisplayContext displayContext, PoseStack poseStack, MultiBufferSource buffer, int packedLight, int packedOverlay) {
                                    mc.getBlockEntityRenderDispatcher()
                                        .renderItem(tile, poseStack, buffer, packedLight, packedOverlay);
                                }
                            };
                        }
                    }
                );
            }
        };

        var regItem = PMRegistries.regItem(name, chestItem)
            .model(
                (item, generator) -> generator.withExistingParent((trapped ? "trapped_" : "") + chestType.getName() + "_chest", "item/chest")
            )
            .tab(trapped ? CreativeModeTabs.REDSTONE_BLOCKS : CreativeModeTabs.FUNCTIONAL_BLOCKS)
            .localizedName(chestType.localizedName(false, trapped), chestType.localizedName(true, trapped));

        if(!chestType.getName().startsWith("dummy")) {
            regItem = regItem.recipe(
                (item, generator) -> {
                    var craftItem = chestType.getCraftItem(trapped);

                    if(trapped)
                        return generator.shapelessRecipe(trapped ? RecipeCategory.REDSTONE : RecipeCategory.DECORATIONS, item, Blocks.TRIPWIRE_HOOK, craftItem);
                    else return generator.getShapedBuilder(trapped ? RecipeCategory.REDSTONE : RecipeCategory.DECORATIONS, item)
                        .pattern("xxx")
                        .pattern("x x")
                        .pattern("xxx")
                        .define('x', craftItem);
                }
            );
        }

        if(chestType.isDeepDarkChest())
            regItem = regItem.tab(DeepDarkBlocks.DEEP_DARK_TAB);

        return chestBlock;
    }
}