package net.paradisemod.misc;

import java.util.function.Supplier;

import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.AxeItem;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.HoeItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.PickaxeItem;
import net.minecraft.world.item.ShovelItem;
import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.TieredItem;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.client.model.generators.ModelFile.UncheckedModelFile;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredItem;

public class Tools {
    public static final RegisteredItem SMITHING_UPGRADE = Misc.regBasicItem("smithing_upgrade")
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.COMBAT, item, 2)
                .pattern("gug")
                .pattern("gsg")
                .pattern("ggg")
                .define('u', item)
                .define('g', Items.GOLD_INGOT)
                .define('s', Blocks.STONE)
        )
        .localizedName("Smithing Upgrade", "Mejora de herrería");

    // emerald tools
    public static final RegisteredItem EMERALD_SWORD = regTool(ToolType.SWORD, PMToolTier.EMERALD);
    public static final RegisteredItem EMERALD_PICKAXE = regTool(ToolType.PICKAXE, PMToolTier.EMERALD);
    public static final RegisteredItem EMERALD_AXE = regTool(ToolType.AXE, PMToolTier.EMERALD);
    public static final RegisteredItem EMERALD_SHOVEL = regTool(ToolType.SHOVEL, PMToolTier.EMERALD);
    public static final RegisteredItem EMERALD_HOE = regTool(ToolType.HOE, PMToolTier.EMERALD);

    // ruby tools
    public static final RegisteredItem RUBY_SWORD = regTool(ToolType.SWORD, PMToolTier.RUBY);
    public static final RegisteredItem RUBY_PICKAXE = regTool(ToolType.PICKAXE, PMToolTier.RUBY);
    public static final RegisteredItem RUBY_AXE = regTool(ToolType.AXE, PMToolTier.RUBY);
    public static final RegisteredItem RUBY_SHOVEL = regTool(ToolType.SHOVEL, PMToolTier.RUBY);
    public static final RegisteredItem RUBY_HOE = regTool(ToolType.HOE, PMToolTier.RUBY);

    // obsidian tools
    public static final RegisteredItem OBSIDIAN_SWORD = regTool(ToolType.SWORD, PMToolTier.OBSIDIAN);
    public static final RegisteredItem OBSIDIAN_PICKAXE = regTool(ToolType.PICKAXE, PMToolTier.OBSIDIAN);
    public static final RegisteredItem OBSIDIAN_AXE = regTool(ToolType.AXE, PMToolTier.OBSIDIAN);
    public static final RegisteredItem OBSIDIAN_SHOVEL = regTool(ToolType.SHOVEL, PMToolTier.OBSIDIAN);
    public static final RegisteredItem OBSIDIAN_HOE = regTool(ToolType.HOE, PMToolTier.OBSIDIAN);

    // redstone tools
    public static final RegisteredItem REDSTONE_SWORD = regTool(ToolType.SWORD, PMToolTier.REDSTONE);
    public static final RegisteredItem REDSTONE_PICKAXE = regTool(ToolType.PICKAXE, PMToolTier.REDSTONE);
    public static final RegisteredItem REDSTONE_AXE = regTool(ToolType.AXE, PMToolTier.REDSTONE);
    public static final RegisteredItem REDSTONE_SHOVEL = regTool(ToolType.SHOVEL, PMToolTier.REDSTONE);
    public static final RegisteredItem REDSTONE_HOE = regTool(ToolType.HOE, PMToolTier.REDSTONE);

    // rusted iron tools
    public static final RegisteredItem RUSTED_IRON_SWORD = regTool(ToolType.SWORD, PMToolTier.RUSTED_IRON);
    public static final RegisteredItem RUSTED_IRON_PICKAXE = regTool(ToolType.PICKAXE, PMToolTier.RUSTED_IRON);
    public static final RegisteredItem RUSTED_IRON_AXE = regTool(ToolType.AXE, PMToolTier.RUSTED_IRON);
    public static final RegisteredItem RUSTED_IRON_SHOVEL = regTool(ToolType.SHOVEL, PMToolTier.RUSTED_IRON);
    public static final RegisteredItem RUSTED_IRON_HOE = regTool(ToolType.HOE, PMToolTier.RUSTED_IRON);

    // silver tools
    public static final RegisteredItem SILVER_SWORD = regTool(ToolType.SWORD, PMToolTier.SILVER);
    public static final RegisteredItem SILVER_PICKAXE = regTool(ToolType.PICKAXE, PMToolTier.SILVER);
    public static final RegisteredItem SILVER_AXE = regTool(ToolType.AXE, PMToolTier.SILVER);
    public static final RegisteredItem SILVER_SHOVEL = regTool(ToolType.SHOVEL, PMToolTier.SILVER);
    public static final RegisteredItem SILVER_HOE = regTool(ToolType.HOE, PMToolTier.SILVER);

    // enderite tools
    public static final RegisteredItem ENDERITE_SWORD = regTool(ToolType.SWORD, PMToolTier.ENDERITE);
    public static final RegisteredItem ENDERITE_PICKAXE = regTool(ToolType.PICKAXE, PMToolTier.ENDERITE);
    public static final RegisteredItem ENDERITE_AXE = regTool(ToolType.AXE, PMToolTier.ENDERITE);
    public static final RegisteredItem ENDERITE_SHOVEL = regTool(ToolType.SHOVEL, PMToolTier.ENDERITE);
    public static final RegisteredItem ENDERITE_HOE = regTool(ToolType.HOE, PMToolTier.ENDERITE);

    public static void init() {
        // tool events
        MinecraftForge.EVENT_BUS.register(ToolEvents.class);
    }

    private static RegisteredItem regTool(ToolType type, PMToolTier tier) {
        var props = new Item.Properties();
        if (tier == PMToolTier.OBSIDIAN)
            props = props.fireResistant();

        Supplier<Item> tool = switch (type) {
            case AXE ->
                () -> new AxeItem(tier, (int) tier.getAttackDamageBonus() + 4, 1f, new Item.Properties());

            case HOE ->
                () -> new HoeItem(tier, 0, 4f, new Item.Properties());

            case SWORD ->
                () -> new SwordItem(tier, (int) tier.getAttackDamageBonus(), 3f, new Item.Properties());

            case SHOVEL ->
                () -> new ShovelItem(tier, (int) tier.getAttackDamageBonus() - 4, 1f, new Item.Properties());

            case PICKAXE ->
                () -> new PickaxeItem(tier, (int) tier.getAttackDamageBonus() - 2, 1f, new Item.Properties());
        };

        var texture = "paradisemod:item/tool/" + tier.getShortName() + "_" + type.getName();

        var englishMaterialName = Utils.localizedMaterialName(tier.getShortName(), false);
        englishMaterialName = englishMaterialName.substring(0, 1).toUpperCase() + englishMaterialName.substring(1);

        var toolItem = PMRegistries.regItem(
            tier.getShortName() + "_" + type.getName(),
            tool
        )
            .recipe(
                (item, generator) -> generator.getShapedBuilder(
                    type == ToolType.SWORD ? RecipeCategory.COMBAT : RecipeCategory.TOOLS,
                    item,
                    type.recipePattern()
                )
                    .define('x', tier.getRepairItem())
                    .define('y', tier.getStickItem())
            )
            .tab(type == ToolType.SWORD ? CreativeModeTabs.COMBAT : CreativeModeTabs.TOOLS_AND_UTILITIES)
            .model(
                (item, generator) ->
                    generator.getBuilder(tier.getShortName() + "_" + type.getName())
                        .parent(new UncheckedModelFile("item/handheld"))
                        .texture("layer0", texture)
            )
            .tag(PMTags.Items.PRESENTS)
            .localizedName(
                englishMaterialName + " " + type.localizedName(false),
                type.localizedName(true) + " de " + Utils.localizedMaterialName(tier.getShortName(), true)
            );

        if(type == ToolType.AXE) toolItem = toolItem.tab(CreativeModeTabs.COMBAT);

        if(tier == PMToolTier.SILVER)
            toolItem = toolItem.tag(PMTags.Items.SILVER_RECYCLABLES);

        var upgradeItem = tier.upgradableFrom(type);

        if(upgradeItem != null) {
            toolItem = toolItem.smithingRecipe(
                (item, generator) -> generator.smithingRecipe(upgradeItem, tier.getRepairItem(), SMITHING_UPGRADE, item, type == ToolType.SWORD ? RecipeCategory.COMBAT : RecipeCategory.TOOLS)
            );
        }

        return toolItem;
    }

    protected enum ToolType {
        PICKAXE,
        AXE,
        HOE,
        SWORD,
        SHOVEL;

        String getName() { return name().toLowerCase(); }

        private String recipePattern() {
            return switch(this) {
                case PICKAXE -> "xxx\n y \n y ";
                case AXE -> " xx\n yx\n y ";
                case HOE -> " xx\n y \n y ";
                case SWORD -> " x \n x \n y ";
                case SHOVEL -> " x \n y \n y ";
            };
        }

        private String localizedName(boolean spanish) {
            if(spanish)
                return switch(this) {
                    case PICKAXE -> "Pico";
                    case AXE -> "Hacha";
                    case HOE -> "Azada";
                    case SWORD -> "Espada";
                    case SHOVEL -> "Pala";
                };

            return switch(this) {
                case PICKAXE -> "Shovel";
                case AXE -> "Axe";
                case HOE -> "Hoe";
                case SWORD -> "Sword";
                case SHOVEL -> "Shovel";
            };
        }
    }

    private static class ToolEvents {
        // inflicts wither and slowness on a living entity for 20 seconds if it is attacked with a rusted iron tool
        @SubscribeEvent
        public static void attackedByRustedIronTool(LivingAttackEvent event) {
            var entity = event.getSource().getDirectEntity();
            if(entity instanceof LivingEntity attacker) {
                var victim = event.getEntity();
                if (victim == null) return;

                if (attacker.getItemInHand(InteractionHand.MAIN_HAND).getItem() instanceof TieredItem tool) {
                    if (tool.getTier() == PMToolTier.RUSTED_IRON) {
                        victim.addEffect(new MobEffectInstance(MobEffects.WITHER, 400));
                        victim.addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SLOWDOWN, 400));
                    }
                }
            }
        }
    }
}