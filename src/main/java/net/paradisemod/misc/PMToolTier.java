package net.paradisemod.misc;

import java.util.function.Supplier;

import javax.annotation.Nullable;

import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.paradisemod.misc.Tools.ToolType;

public enum PMToolTier implements Tier {
    RUSTED_IRON(2, 125, 12, 2, 22, Misc.RUSTED_IRON_INGOT, BlockTags.NEEDS_IRON_TOOL),
    SILVER(2, 64, 12, 2, 22, Misc.SILVER_INGOT, BlockTags.NEEDS_IRON_TOOL),
    EMERALD(4, 1700, 9, 4, 30, () -> Items.EMERALD, BlockTags.NEEDS_DIAMOND_TOOL),
    RUBY(4, 1700, 9, 4, 30, Misc.RUBY, BlockTags.NEEDS_DIAMOND_TOOL),
    OBSIDIAN(4, 3122, 10, 5, 30, Blocks.OBSIDIAN::asItem, BlockTags.NEEDS_DIAMOND_TOOL),
    REDSTONE(6, 16, 100, 10, 45, () -> Items.REDSTONE, BlockTags.NEEDS_DIAMOND_TOOL),
    ENDERITE(8, 4062, 18, 8, 30, Misc.ENDERITE_INGOT, BlockTags.NEEDS_DIAMOND_TOOL);

    private final int harvestLevel;
    private final int maxUses;
    private final float efficiency;
    private final float attackDamage;
    private final int enchantability;
    private final Supplier<Item> repairItem;
    private final TagKey<Block> harvestTag;

    PMToolTier(int harvestLevel, int maxUses, float efficiency, float attackDamage, int enchantLevel, Supplier<Item> repairItem, TagKey<Block> harvestTag) {
        this.harvestLevel = harvestLevel;
        this.maxUses = maxUses;
        this.efficiency = efficiency;
        this.attackDamage = attackDamage;
        enchantability = enchantLevel;
        this.repairItem = repairItem;
        this.harvestTag = harvestTag;
    }

    @Override
    public int getUses() { return maxUses; }

    @Override
    public float getSpeed() { return efficiency; }

    @Override
    public float getAttackDamageBonus() { return attackDamage; }

    @Override
    public int getLevel() { return harvestLevel; }

    @Override
    public int getEnchantmentValue() { return enchantability; }

    public Item getRepairItem() { return repairItem.get(); }

    public Item getStickItem() {
        return switch(this) {
            case EMERALD, RUBY -> Items.IRON_INGOT;
            case REDSTONE -> Blocks.REDSTONE_TORCH.asItem();
            case OBSIDIAN, ENDERITE -> Items.DIAMOND;
            default -> Items.STICK;
        };
    }

    public @Nullable Item upgradableFrom(ToolType type) {
        return switch(type) {
            case AXE -> switch(this) {
                case EMERALD, RUBY, OBSIDIAN -> Items.DIAMOND_AXE;
                case ENDERITE -> Items.NETHERITE_AXE;
                default -> null;
            };

            case HOE -> switch(this) {
                case EMERALD, RUBY, OBSIDIAN -> Items.DIAMOND_HOE;
                case ENDERITE -> Items.NETHERITE_HOE;
                default -> null;
            };

            case PICKAXE -> switch(this) {
                case EMERALD, RUBY, OBSIDIAN -> Items.DIAMOND_PICKAXE;
                case ENDERITE -> Items.NETHERITE_PICKAXE;
                default -> null;
            };

            case SHOVEL -> switch(this) {
                case EMERALD, RUBY, OBSIDIAN -> Items.DIAMOND_SHOVEL;
                case ENDERITE -> Items.NETHERITE_SHOVEL;
                default -> null;
            };

            case SWORD -> switch(this) {
                case EMERALD, RUBY, OBSIDIAN -> Items.DIAMOND_SWORD;
                case ENDERITE -> Items.NETHERITE_SWORD;
                default -> null;
            };
        };
    }

    @Override
    public Ingredient getRepairIngredient() { return Ingredient.of(getRepairItem()); }

    @Override
    public TagKey<Block> getTag() { return harvestTag; }

    public String getShortName() { return name().toLowerCase(); }
}