package net.paradisemod.building;

import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.StairBlock;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.world.DeepDarkBlocks;

public class Stairs {
    // wooden stairs
    public static final RegisteredBlock CACTUS_STAIRS = regWoodStairs("cactus", false, Building.CACTUS_BLOCK);
    public static final RegisteredBlock PALO_VERDE_STAIRS = regWoodStairs("palo_verde", false, Building.PALO_VERDE_PLANKS);
    public static final RegisteredBlock MESQUITE_STAIRS = regWoodStairs("mesquite", false, Building.MESQUITE_PLANKS);

    public static final RegisteredBlock BLACKENED_OAK_STAIRS = regWoodStairs("blackened_oak", false, DeepDarkBlocks.BLACKENED_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_SPRUCE_STAIRS = regWoodStairs("blackened_spruce", false, DeepDarkBlocks.BLACKENED_SPRUCE_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_OAK_STAIRS = regWoodStairs("glowing_oak", true, DeepDarkBlocks.GLOWING_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_CACTUS_STAIRS = regWoodStairs("glowing_cactus", true, Building.CACTUS_BLOCK)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    // stone stairs
    public static final RegisteredBlock BEDROCK_STAIRS = regStairs("bedrock", BlockType.INDESTRUCTIBLE, false, Blocks.BEDROCK);
    public static final RegisteredBlock END_STONE_STAIRS = regStoneStairs("end_stone", SoundType.STONE, Blocks.END_STONE);
    public static final RegisteredBlock DRIPSTONE_STAIRS = regStoneStairs("dripstone", SoundType.DRIPSTONE_BLOCK, Blocks.DRIPSTONE_BLOCK);
    public static final RegisteredBlock CALCITE_STAIRS = regStoneStairs("calcite", SoundType.CALCITE, Blocks.CALCITE);
    public static final RegisteredBlock TUFF_STAIRS = regStoneStairs("tuff", SoundType.TUFF, Blocks.TUFF);
    public static final RegisteredBlock POLISHED_END_STONE_STAIRS = regStoneStairs("polished_end_stone", SoundType.STONE, Building.POLISHED_END_STONE, Blocks.END_STONE);
    public static final RegisteredBlock POLISHED_DRIPSTONE_STAIRS = regStoneStairs("polished_dripstone", SoundType.DRIPSTONE_BLOCK, Building.POLISHED_DRIPSTONE, Blocks.DRIPSTONE_BLOCK);
    public static final RegisteredBlock POLISHED_CALCITE_STAIRS = regStoneStairs("polished_calcite", SoundType.CALCITE, Building.POLISHED_CALCITE, Blocks.CALCITE);
    public static final RegisteredBlock POLISHED_TUFF_STAIRS = regStoneStairs("polished_tuff", SoundType.TUFF, Building.POLISHED_TUFF, Blocks.TUFF);

    public static final RegisteredBlock DARKSTONE_STAIRS = regStoneStairs("darkstone", SoundType.STONE, DeepDarkBlocks.DARKSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock POLISHED_DARKSTONE_STAIRS = regStoneStairs("polished_darkstone", SoundType.STONE, DeepDarkBlocks.POLISHED_DARKSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock POLISHED_DARKSTONE_BRICKS_STAIRS = regStoneStairs("polished_darkstone_bricks", SoundType.STONE, DeepDarkBlocks.POLISHED_DARKSTONE_BRICKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock POLISHED_DRIPSTONE_BRICKS_STAIRS = regStoneStairs("polished_dripstone_bricks", SoundType.STONE, Building.POLISHED_DRIPSTONE_BRICKS, Building.POLISHED_DRIPSTONE, Blocks.DRIPSTONE_BLOCK);
    public static final RegisteredBlock POLISHED_CALCITE_BRICKS_STAIRS = regStoneStairs("polished_calcite_bricks", SoundType.STONE, Building.POLISHED_CALCITE_BRICKS, Building.POLISHED_CALCITE, Blocks.CALCITE);
    public static final RegisteredBlock POLISHED_TUFF_BRICKS_STAIRS = regStoneStairs("polished_tuff_bricks", SoundType.STONE, Building.POLISHED_TUFF_BRICKS, Building.POLISHED_TUFF, Blocks.TUFF);
    public static final RegisteredBlock OBSIDIAN_STAIRS = regStairs("obsidian", BlockType.STRONG_STONE, false, Blocks.OBSIDIAN);
    public static final RegisteredBlock POLISHED_ASPHALT_STAIRS = regStoneStairs("polished_asphalt", SoundType.STONE, Building.POLISHED_ASPHALT);
    public static final RegisteredBlock POLISHED_ASPHALT_BRICKS_STAIRS = regStoneStairs("polished_asphalt_bricks", SoundType.STONE, Building.POLISHED_ASPHALT_BRICKS, Building.POLISHED_ASPHALT);

    public static final RegisteredBlock BLACKENED_SANDSTONE_STAIRS = regSandstonetoneStairs("blackened", false, DeepDarkBlocks.BLACKENED_SANDSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock SMOOTH_BLACKENED_SANDSTONE_STAIRS = regSandstonetoneStairs("blackened", true, DeepDarkBlocks.SMOOTH_BLACKENED_SANDSTONE, DeepDarkBlocks.BLACKENED_SANDSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    // glowing obsidian stairs
    public static final RegisteredBlock BLACK_GLOWING_OBSIDIAN_STAIRS = regGlowingObsidianStairs(GlowingObsidianColor.BLACK);
    public static final RegisteredBlock BLUE_GLOWING_OBSIDIAN_STAIRS = regGlowingObsidianStairs(GlowingObsidianColor.BLUE);
    public static final RegisteredBlock GREEN_GLOWING_OBSIDIAN_STAIRS = regGlowingObsidianStairs(GlowingObsidianColor.GREEN);
    public static final RegisteredBlock INDIGO_GLOWING_OBSIDIAN_STAIRS = regGlowingObsidianStairs(GlowingObsidianColor.INDIGO);
    public static final RegisteredBlock ORANGE_GLOWING_OBSIDIAN_STAIRS = regGlowingObsidianStairs(GlowingObsidianColor.ORANGE);
    public static final RegisteredBlock RED_GLOWING_OBSIDIAN_STAIRS = regGlowingObsidianStairs(GlowingObsidianColor.RED);
    public static final RegisteredBlock VIOLET_GLOWING_OBSIDIAN_STAIRS = regGlowingObsidianStairs(GlowingObsidianColor.VIOLET);
    public static final RegisteredBlock WHITE_GLOWING_OBSIDIAN_STAIRS = regGlowingObsidianStairs(GlowingObsidianColor.WHITE);
    public static final RegisteredBlock YELLOW_GLOWING_OBSIDIAN_STAIRS = regGlowingObsidianStairs(GlowingObsidianColor.YELLOW);
    public static final RegisteredBlock GOLD_GLOWING_OBSIDIAN_STAIRS = regGlowingObsidianStairs(GlowingObsidianColor.GOLD);
    public static final RegisteredBlock SILVER_GLOWING_OBSIDIAN_STAIRS = regGlowingObsidianStairs(GlowingObsidianColor.SILVER);
    public static final RegisteredBlock IRON_GLOWING_OBSIDIAN_STAIRS = regGlowingObsidianStairs(GlowingObsidianColor.IRON);
    public static final RegisteredBlock COPPER_GLOWING_OBSIDIAN_STAIRS = regGlowingObsidianStairs(GlowingObsidianColor.COPPER);

    public static void init() { }

    private static RegisteredBlock regStairs(String name, BlockType type, boolean glows, ItemLike craftItem) {
        return regStairsInternal(name, type, glows, craftItem)
            .tag(BlockTags.STAIRS);
    }

    private static RegisteredBlock regWoodStairs(String name, boolean glows, ItemLike planks) {
        return regStairs(name, BlockType.WOOD, glows, planks)
            .tag(BlockTags.WOODEN_STAIRS);
    }

    private static RegisteredBlock regStairsInternal(String name, BlockType type, boolean glows, ItemLike craftItem) {
        return PMRegistries.regBlockItem(name + "_stairs",
            () -> new StairBlock(type.getBlock()::defaultBlockState, type.getProperties())
        )
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .blockStateGenerator((block, generator) -> genStairsBlockState(name, block, Slabs.slabTexture(name, false), generator))
            .itemModelAlreadyExists()
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.BUILDING_BLOCKS, item, 4)
                    .pattern("I  ")
                    .pattern("II ")
                    .pattern("III")
                    .define('I', craftItem)
            )
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Stairs",
                "Escaleras de " + Utils.localizedMaterialName(name, true)
            );
    }

    private static RegisteredBlock regSandstonetoneStairs(String name, boolean isSmooth, ItemLike stone, ItemLike... stonecuttingBlocks) {
        return regStairsInternal((isSmooth ? "smooth_" : "") + name + "_sandstone", BlockType.STONE, false, stone)
            .recipe((item, generator) -> generator.stonecutterRecipe(stone, item))
            .stonecutterRecipes(stonecuttingBlocks)
            .blockStateGenerator((block, generator) -> genSandstoneStairsBlockState(name, block, isSmooth, generator));
    }

    private static RegisteredBlock regStoneStairs(String name, SoundType stoneSound, ItemLike stone, ItemLike... stonecuttingBlocks) {
        return PMRegistries.regBlockItem(
            name + "_stairs",
            () -> new StairBlock(Blocks.STONE_STAIRS::defaultBlockState, Block.Properties.copy(Blocks.STONE_STAIRS).sound(stoneSound))
        )
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .tags(
                BlockTags.STAIRS,
                BlockTags.MINEABLE_WITH_PICKAXE
            )
            .blockStateGenerator((block, generator) -> genStairsBlockState(name, block, Slabs.slabTexture(name, false), generator))
            .itemModelAlreadyExists()
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.BUILDING_BLOCKS, item, 4)
                    .pattern("I  ")
                    .pattern("II ")
                    .pattern("III")
                    .define('I', stone)
            )
            .recipe((item, generator) -> generator.stonecutterRecipe(stone, item))
            .stonecutterRecipes(stonecuttingBlocks)
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Stairs",
                "Escaleras de " + Utils.localizedMaterialName(name, true)
            );
    }

    private static RegisteredBlock regGlowingObsidianStairs(GlowingObsidianColor color) {
        var glowingObsidian = Building.GLOWING_OBSIDIAN.get(color);
        return PMRegistries.regBlockItem(
            color.getName() + "_glowing_obsidian_stairs",
            () -> new StairBlock(
                Blocks.OBSIDIAN::defaultBlockState,
                BlockType.STRONG_STONE
                    .getProperties()
                    .lightLevel(s -> 7)
            )
        )
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.BUILDING_BLOCKS, item, 4)
                    .pattern("I  ")
                    .pattern("II ")
                    .pattern("III")
                    .define('I', glowingObsidian)
            )
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .tag(PMTags.Blocks.GLOWING_OBSIDIAN_STAIRS)
            .blockStateGenerator((block, generator) -> genGlowingObsidianStairsBlockState(color.getName(), block, generator))
            .itemModelAlreadyExists()
            .localizedName(
                color.localizedName(false) + " Glowing Obsidian Stairs",
                "Escaleras de obsidiana brillante " + color.localizedName(true)
            );
    }

    private static void genGlowingObsidianStairsBlockState(String color, RegisteredBlock stairsBlock, BlockStateGenerator generator) {
        var texture = new ResourceLocation("paradisemod:block/glowing_obsidian/" + color);
        genStairsBlockState(color + "_glowing_obsidian", stairsBlock, texture, generator);
    }

    private static void genStairsBlockState(String name, RegisteredBlock stairsBlock, ResourceLocation texture, BlockStateGenerator generator) {
        var modelBuilder = generator.models();
        var stairs = modelBuilder.stairs("block/stairs/" + name, texture, texture, texture);
        var stairsInner = modelBuilder.stairsInner("block/stairs/" + name + "_inner", texture, texture, texture);
        var stairsOuter = modelBuilder.stairsOuter("block/stairs/" + name + "_outer", texture, texture, texture);
        generator.stairsBlock((StairBlock) stairsBlock.get(), stairs, stairsInner, stairsOuter);
        generator.itemModels().parentBlockItem(stairsBlock.get(), "stairs/" + name);
    }

    private static void genSandstoneStairsBlockState(String name, RegisteredBlock stairsBlock, boolean isSmooth, BlockStateGenerator generator) {
        var bottomTexture = new ResourceLocation(ParadiseMod.ID, "block/" + name + "_sandstone" + (isSmooth ? "_top" : "_bottom"));
        var sideTexture = new ResourceLocation(ParadiseMod.ID, "block/" + name + "_sandstone" + (isSmooth ? "_top" : ""));
        var topTexture = new ResourceLocation(ParadiseMod.ID, "block/" + name + "_sandstone_top");
        var modelBuilder = generator.models();
        var stairs = modelBuilder.stairs("block/stairs/" + (isSmooth ? "smooth_" : "") + name + "_sandstone", sideTexture, bottomTexture, topTexture);
        var stairsInner = modelBuilder.stairsInner("block/stairs/" + (isSmooth ? "smooth_" : "") + name + "_sandstone_inner", sideTexture, bottomTexture, topTexture);
        var stairsOuter = modelBuilder.stairsOuter("block/stairs/" + (isSmooth ? "smooth_" : "") + name + "_sandstone_outer", sideTexture, bottomTexture, topTexture);
        generator.stairsBlock((StairBlock) stairsBlock.get(), stairs, stairsInner, stairsOuter);
        generator.itemModels().parentBlockItem(stairsBlock.get(), "stairs/" + (isSmooth ? "smooth_" : "") + name + "_sandstone");
    }
}