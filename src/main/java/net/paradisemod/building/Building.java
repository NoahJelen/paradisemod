package net.paradisemod.building;

import java.util.EnumMap;

import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.BlockTemplates;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.misc.Misc;

public class Building {
    // glowing obsidian (it is different colors now)
    public static final EnumMap<GlowingObsidianColor, RegisteredBlock> GLOWING_OBSIDIAN = new EnumMap<>(GlowingObsidianColor.class);

    public static final RegisteredBlock BLACK_GLOWING_OBSIDIAN = regGlowingObsidian(GlowingObsidianColor.BLACK);
    public static final RegisteredBlock BLUE_GLOWING_OBSIDIAN = regGlowingObsidian(GlowingObsidianColor.BLUE);
    public static final RegisteredBlock GREEN_GLOWING_OBSIDIAN = regGlowingObsidian(GlowingObsidianColor.GREEN);
    public static final RegisteredBlock INDIGO_GLOWING_OBSIDIAN = regGlowingObsidian(GlowingObsidianColor.INDIGO);
    public static final RegisteredBlock ORANGE_GLOWING_OBSIDIAN = regGlowingObsidian(GlowingObsidianColor.ORANGE);
    public static final RegisteredBlock RED_GLOWING_OBSIDIAN = regGlowingObsidian(GlowingObsidianColor.RED);
    public static final RegisteredBlock VIOLET_GLOWING_OBSIDIAN = regGlowingObsidian(GlowingObsidianColor.VIOLET);
    public static final RegisteredBlock WHITE_GLOWING_OBSIDIAN = regGlowingObsidian(GlowingObsidianColor.WHITE);
    public static final RegisteredBlock YELLOW_GLOWING_OBSIDIAN = regGlowingObsidian(GlowingObsidianColor.YELLOW);
    public static final RegisteredBlock GOLD_GLOWING_OBSIDIAN = regGlowingObsidian(GlowingObsidianColor.GOLD);
    public static final RegisteredBlock SILVER_GLOWING_OBSIDIAN = regGlowingObsidian(GlowingObsidianColor.SILVER);
    public static final RegisteredBlock IRON_GLOWING_OBSIDIAN = regGlowingObsidian(GlowingObsidianColor.IRON);
    public static final RegisteredBlock COPPER_GLOWING_OBSIDIAN = regGlowingObsidian(GlowingObsidianColor.COPPER);

    // other building blocks
    public static final RegisteredBlock CACTUS_BLOCK = PMRegistries.regBlockItem(
        "cactus_block",
        () -> new Block(BlockType.WOOD.getProperties())
    )
        .tab(CreativeModeTabs.BUILDING_BLOCKS)
        .tag(BlockTags.PLANKS)
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.BUILDING_BLOCKS, item)
                .pattern("cc")
                .pattern("cc")
                .define('c', Blocks.CACTUS)
        )
        .localizedName("Cactus Block", "Bloque de cactus");

    public static final RegisteredBlock POLISHED_END_STONE = BlockTemplates.cutImprovedRock("polished_end_stone", Blocks.END_STONE)
        .localizedName("Polished End Stone", "Piedra pulida del Fin");

    public static final RegisteredBlock ASPHALT = BlockTemplates.rock("asphalt")
        .tag(BlockTags.INFINIBURN_OVERWORLD)
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.BUILDING_BLOCKS, item)
                .pattern("aa")
                .pattern("aa")
                .define('a', Misc.ASPHALT_SHARD)
        )
        .tab(CreativeModeTabs.NATURAL_BLOCKS)
        .localizedName("Asphalt", "Asfalto");

    public static final RegisteredBlock POLISHED_ASPHALT = BlockTemplates.rock("polished_asphalt")
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.BUILDING_BLOCKS, item, 4)
                .pattern("aS")
                .pattern("Sa")
                .define('a', ASPHALT)
                .define('S', Blocks.STONE)
        )
        .localizedName("Polished Asphalt", "Asfalto pulido");

    public static final RegisteredBlock POLISHED_ASPHALT_BRICKS = BlockTemplates.cutRock("polished_asphalt_bricks", POLISHED_ASPHALT)
        .localizedName("Polished Asphalt Bricks", "Ladrillos de asfalto pulido");

    public static final RegisteredBlock POLISHED_DRIPSTONE = BlockTemplates.rock("polished_dripstone", Blocks.DRIPSTONE_BLOCK)
        .localizedName("Polished Dripstone", "Caliza pulida");

    public static final RegisteredBlock POLISHED_CALCITE = BlockTemplates.rock("polished_calcite", Blocks.CALCITE)
        .localizedName("Polished Calcite", "Calcita pulida");

    public static final RegisteredBlock POLISHED_TUFF = BlockTemplates.rock("polished_tuff", Blocks.TUFF)
        .localizedName("Polished Tuff", "Toba pulida");

    public static final RegisteredBlock CHISELED_POLISHED_DRIPSTONE = BlockTemplates.cutRock("chiseled_polished_dripstone", POLISHED_DRIPSTONE, Blocks.DRIPSTONE_BLOCK)
        .localizedName("Chiseled Polished Dripstone", "Caliza pulida grabada");

    public static final RegisteredBlock CHISELED_POLISHED_CALCITE = BlockTemplates.cutRock("chiseled_polished_calcite", POLISHED_CALCITE, Blocks.CALCITE)
        .localizedName("Chiseled Polished Calcite", "Calcita pulida grabada");

    public static final RegisteredBlock CHISELED_POLISHED_TUFF = BlockTemplates.cutRock("chiseled_polished_tuff", POLISHED_TUFF, Blocks.TUFF)
        .localizedName("Chiseled Polished Tuff", "Toba pulida grabada");

    public static final RegisteredBlock POLISHED_DRIPSTONE_BRICKS = BlockTemplates.cutRock("polished_dripstone_bricks", POLISHED_DRIPSTONE, Blocks.DRIPSTONE_BLOCK)
        .localizedName("Polished Dripstone Bricks", "Ladrillos de caliza pulida");

    public static final RegisteredBlock POLISHED_CALCITE_BRICKS = BlockTemplates.cutRock("polished_calcite_bricks", POLISHED_CALCITE, Blocks.CALCITE)
        .localizedName("Polished Calcite Bricks", "Ladrillos de calcita pulida");

    public static final RegisteredBlock POLISHED_TUFF_BRICKS = BlockTemplates.cutRock("polished_tuff_bricks", POLISHED_TUFF, Blocks.TUFF)
        .localizedName("Polished Tuff Bricks", "Ladrillos de toba pulida");

    // planks
    public static final RegisteredBlock PALO_VERDE_PLANKS = BlockTemplates.planks("palo_verde", false);
    public static final RegisteredBlock MESQUITE_PLANKS = BlockTemplates.planks("mesquite", false);

    public static void init() {
        // submodules
        Doors.init();
        Fences.init();
        FenceGates.init();
        Slabs.init();
        Stairs.init();
        Trapdoors.init();
        Walls.init();
        ParadiseMod.LOG.info("Loaded Building module");
    }

    private static RegisteredBlock regGlowingObsidian(GlowingObsidianColor color) {
        var obsidian = PMRegistries.regBlockItem(
            color.getName() + "_glowing_obsidian",
            () -> new Block(BlockType.STRONG_STONE.getProperties().lightLevel(s -> 7))
        )
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .tag(PMTags.Blocks.GLOWING_OBSIDIAN)
            .blockStateGenerator((block, generator) -> generator.simpleBlock(block, "glowing_obsidian/" + color.getName()))
            .itemModel((block, generator) -> generator.parentBlockItem(block.get(), "glowing_obsidian/" + color.getName()))
            .localizedName(
                color.localizedName(false) + " Glowing Obsidian",
                "Obsidiana brillante " + color.localizedName(true)
            );

        GLOWING_OBSIDIAN.put(color, obsidian);
        return obsidian;
    }
}