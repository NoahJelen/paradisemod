package net.paradisemod.building;

import java.util.ArrayList;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FenceBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.Tags;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.misc.Misc;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.Ores;

public class Fences {
    // fences
    public static final RegisteredBlock BRICK_FENCE = regFence(BlockType.STONE, "brick", false, Items.BRICK, Blocks.BRICKS);
    public static final RegisteredBlock CACTUS_FENCE = regFence(BlockType.WOOD, "cactus", false, Building.CACTUS_BLOCK);
    public static final RegisteredBlock PALO_VERDE_FENCE = regFence(BlockType.WOOD, "palo_verde", false, Building.PALO_VERDE_PLANKS);
    public static final RegisteredBlock MESQUITE_FENCE = regFence(BlockType.WOOD, "mesquite", false, Building.MESQUITE_PLANKS);
    public static final RegisteredBlock DIAMOND_FENCE = regFence(BlockType.METAL, "diamond", false, Items.DIAMOND, Blocks.DIAMOND_BLOCK);
    public static final RegisteredBlock EMERALD_FENCE = regFence(BlockType.METAL, "emerald", false, Items.EMERALD, Blocks.EMERALD_BLOCK);
    public static final RegisteredBlock GOLD_FENCE = regFence(BlockType.METAL, "gold", false, Items.GOLD_INGOT, Blocks.GOLD_BLOCK);
    public static final RegisteredBlock IRON_FENCE = regFence(BlockType.WEAK_METAL, "iron", false, Items.IRON_INGOT, Blocks.IRON_BLOCK);

    public static final RegisteredBlock BLACKENED_OAK_FENCE = regFence(BlockType.WOOD, "blackened_oak", false, DeepDarkBlocks.BLACKENED_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_SPRUCE_FENCE = regFence(BlockType.WOOD, "blackened_spruce", false, DeepDarkBlocks.BLACKENED_SPRUCE_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_OAK_FENCE = regFence(BlockType.WOOD, "glowing_oak", true, DeepDarkBlocks.GLOWING_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_CACTUS_FENCE = regFence(BlockType.WOOD, "glowing_cactus", false, DeepDarkBlocks.GLOWING_CACTUS_BLOCK)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock REDSTONE_FENCE = PMRegistries.regBlockItem("redstone_fence",
        () -> new FenceBlock(BlockType.METAL.getProperties().noOcclusion()) {
            @Override
            public int getSignal(BlockState blockState, BlockGetter blockAccess, BlockPos pos, Direction side) { return 15; }
        }
    )
        .tab(CreativeModeTabs.BUILDING_BLOCKS)
        .tags(
            Tags.Blocks.FENCES,
            PMTags.Blocks.METAL_FENCES,
            BlockTags.FENCES,
            BlockTags.MINEABLE_WITH_PICKAXE,
            BlockTags.NEEDS_IRON_TOOL
        )
        .itemModel((block, generator) -> generator.fenceInventory("redstone_fence", fenceTexture("redstone")))
        .blockStateGenerator((block, generator) -> genFenceBlockState("redstone", block, generator))
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item, 3)
                .pattern("PSP")
                .pattern("PSP")
                .define('S', Items.REDSTONE_TORCH)
                .define('P', Blocks.REDSTONE_BLOCK)
        )
        .localizedName("Redstone Fence", "Valla de piedra roja");

    public static final RegisteredBlock RUBY_FENCE = regFence(BlockType.METAL, "ruby", false, Misc.RUBY, Ores.RUBY_BLOCK);
    public static final RegisteredBlock RUSTED_IRON_FENCE = regFence(BlockType.WEAK_METAL, "rusted_iron", false, Misc.RUSTED_IRON_INGOT, Ores.RUSTED_IRON_BLOCK);
    public static final RegisteredBlock SILVER_FENCE = regFence(BlockType.METAL, "silver", false, Misc.SILVER_INGOT, Ores.SILVER_BLOCK);

    public static void init() { }

    private static RegisteredBlock regFence(BlockType type, String name, boolean glows, ItemLike planks) {
        return regFence(type, name, glows, Items.STICK, planks);
    }

    private static RegisteredBlock regFence(BlockType type, String name, boolean glows, ItemLike craftItem, ItemLike craftBlock) {
        ArrayList<TagKey<Block>> tags = new ArrayList<>();
        var texture = fenceTexture(name);

        if(type == BlockType.METAL || type == BlockType.WEAK_METAL)
            tags.add(PMTags.Blocks.METAL_FENCES);

        if(type == BlockType.WOOD) {
            tags.add(Tags.Blocks.FENCES_WOODEN);
            tags.add(BlockTags.WOODEN_FENCES);
        }
        else {
            tags.add(Tags.Blocks.FENCES);
            tags.addAll(type.tags());
        }

        return PMRegistries.regBlockItem(
            name + "_fence",
            () -> new FenceBlock(type.getProperties().lightLevel(s -> glows ? 7 : 0))
        )
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .tags(tags)
            .tags(BlockTags.FENCES)
            .itemModel((block, generator) -> generator.fenceInventory(name + "_fence", texture))
            .blockStateGenerator((block, generator) -> genFenceBlockState(name, block, generator))
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item, 3)
                    .pattern("PSP")
                    .pattern("PSP")
                    .define('S', craftItem)
                    .define('P', craftBlock)
            )
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Fence",
                "Valla de " + Utils.localizedMaterialName(name, true)
            );
    }

    public static ResourceLocation fenceTexture(String name) {
        return new ResourceLocation(
            switch(name) {
                case "brick" -> "minecraft:block/bricks";
                case "cactus" -> "paradisemod:block/cactus_block";

                case "palo_verde", "mesquite", "blackened_oak", "blackened_spruce", "glowing_oak" ->
                    "paradisemod:block/" + name + "_planks";

                case "diamond", "emerald", "gold", "iron", "redstone" -> "minecraft:block/" + name + "_block";
                default -> "paradisemod:block/" + name + "_block";
            }
        );
    }

    private static void genFenceBlockState(String name, RegisteredBlock fence, BlockStateGenerator generator) {
        var texture = fenceTexture(name);
        var modelBuilder = generator.models();
        var modelName = "block/fence/" + name;
        var post = modelBuilder.fencePost(modelName + "_post", texture);
        var side = modelBuilder.fenceSide(modelName + "_side", texture);
        generator.fourWayBlock((FenceBlock) fence.get(), post, side);
    }
}