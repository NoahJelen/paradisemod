package net.paradisemod.building;

import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.SoundType;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.world.DeepDarkBlocks;

public class Slabs {
    // wooden slabs
    public static final RegisteredBlock CACTUS_SLAB = regWoodSlab("cactus", false, Building.CACTUS_BLOCK);
    public static final RegisteredBlock PALO_VERDE_SLAB = regWoodSlab("palo_verde", false, Building.PALO_VERDE_PLANKS);
    public static final RegisteredBlock MESQUITE_SLAB = regWoodSlab("mesquite", false, Building.MESQUITE_PLANKS);

    public static final RegisteredBlock BLACKENED_OAK_SLAB = regWoodSlab("blackened_oak", false, DeepDarkBlocks.BLACKENED_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_SPRUCE_SLAB = regWoodSlab("blackened_spruce", false, DeepDarkBlocks.BLACKENED_SPRUCE_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_OAK_SLAB = regWoodSlab("glowing_oak", true, DeepDarkBlocks.GLOWING_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_CACTUS_SLAB = regWoodSlab("glowing_cactus", true, DeepDarkBlocks.GLOWING_CACTUS_BLOCK)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    // stone slabs
    public static final RegisteredBlock BEDROCK_SLAB = regSlab("bedrock", BlockType.INDESTRUCTIBLE, Blocks.BEDROCK);
    public static final RegisteredBlock END_STONE_SLAB = regStoneSlab("end_stone", SoundType.STONE, Blocks.END_STONE);
    public static final RegisteredBlock DRIPSTONE_SLAB = regStoneSlab("dripstone", SoundType.DRIPSTONE_BLOCK, Blocks.DRIPSTONE_BLOCK);
    public static final RegisteredBlock CALCITE_SLAB = regStoneSlab("calcite", SoundType.CALCITE, Blocks.CALCITE);
    public static final RegisteredBlock TUFF_SLAB = regStoneSlab("tuff", SoundType.TUFF, Blocks.TUFF);
    public static final RegisteredBlock POLISHED_END_STONE_SLAB = regStoneSlab("polished_end_stone", SoundType.STONE, Building.POLISHED_END_STONE, Blocks.END_STONE);
    public static final RegisteredBlock POLISHED_DRIPSTONE_SLAB = regStoneSlab("polished_dripstone", SoundType.DRIPSTONE_BLOCK, Building.POLISHED_DRIPSTONE, Blocks.DRIPSTONE_BLOCK);
    public static final RegisteredBlock POLISHED_CALCITE_SLAB = regStoneSlab("polished_calcite", SoundType.CALCITE, Building.POLISHED_CALCITE, Blocks.CALCITE);
    public static final RegisteredBlock POLISHED_TUFF_SLAB = regStoneSlab("polished_tuff", SoundType.TUFF, Building.POLISHED_TUFF, Blocks.TUFF);

    public static final RegisteredBlock DARKSTONE_SLAB = regStoneSlab("darkstone", SoundType.STONE, DeepDarkBlocks.DARKSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock POLISHED_DARKSTONE_SLAB = regStoneSlab("polished_darkstone", SoundType.STONE, DeepDarkBlocks.POLISHED_DARKSTONE, DeepDarkBlocks.DARKSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock POLISHED_DARKSTONE_BRICKS_SLAB = regStoneSlab("polished_darkstone_bricks", SoundType.STONE, DeepDarkBlocks.POLISHED_DARKSTONE_BRICKS, DeepDarkBlocks.POLISHED_DARKSTONE, DeepDarkBlocks.DARKSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock POLISHED_DRIPSTONE_BRICKS_SLAB = regStoneSlab("polished_dripstone_bricks", SoundType.DRIPSTONE_BLOCK, Building.POLISHED_DRIPSTONE_BRICKS, Building.POLISHED_DRIPSTONE, Blocks.DRIPSTONE_BLOCK);
    public static final RegisteredBlock POLISHED_CALCITE_BRICKS_SLAB = regStoneSlab("polished_calcite_bricks", SoundType.CALCITE, Building.POLISHED_CALCITE_BRICKS, Building.POLISHED_CALCITE, Blocks.CALCITE);
    public static final RegisteredBlock POLISHED_TUFF_BRICKS_SLAB = regStoneSlab("polished_tuff_bricks", SoundType.TUFF, Building.POLISHED_TUFF_BRICKS, Building.POLISHED_TUFF, Blocks.TUFF);
    public static final RegisteredBlock OBSIDIAN_SLAB = regSlab("obsidian", BlockType.STRONG_STONE, Blocks.OBSIDIAN);
    public static final RegisteredBlock POLISHED_ASPHALT_SLAB = regStoneSlab("polished_asphalt", SoundType.STONE, Building.POLISHED_ASPHALT);
    public static final RegisteredBlock POLISHED_ASPHALT_BRICKS_SLAB = regStoneSlab("polished_asphalt_bricks", SoundType.STONE, Building.POLISHED_ASPHALT_BRICKS, Building.POLISHED_ASPHALT);

    public static final RegisteredBlock BLACKENED_SANDSTONE_SLAB = regSandstoneSlab("blackened", SandstoneType.NORMAL, DeepDarkBlocks.BLACKENED_SANDSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock CUT_BLACKENED_SANDSTONE_SLAB = regSandstoneSlab("blackened", SandstoneType.CUT, DeepDarkBlocks.CUT_BLACKENED_SANDSTONE, DeepDarkBlocks.BLACKENED_SANDSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock SMOOTH_BLACKENED_SANDSTONE_SLAB = regSandstoneSlab("blackened", SandstoneType.SMOOTH, DeepDarkBlocks.CUT_BLACKENED_SANDSTONE, DeepDarkBlocks.BLACKENED_SANDSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    // glowing obsidian slabs
    public static final RegisteredBlock BLACK_GLOWING_OBSIDIAN_SLAB = regGlowingObsidianSlab(GlowingObsidianColor.BLACK);
    public static final RegisteredBlock BLUE_GLOWING_OBSIDIAN_SLAB = regGlowingObsidianSlab(GlowingObsidianColor.BLUE);
    public static final RegisteredBlock GREEN_GLOWING_OBSIDIAN_SLAB = regGlowingObsidianSlab(GlowingObsidianColor.GREEN);
    public static final RegisteredBlock INDIGO_GLOWING_OBSIDIAN_SLAB = regGlowingObsidianSlab(GlowingObsidianColor.INDIGO);
    public static final RegisteredBlock ORANGE_GLOWING_OBSIDIAN_SLAB = regGlowingObsidianSlab(GlowingObsidianColor.ORANGE);
    public static final RegisteredBlock RED_GLOWING_OBSIDIAN_SLAB = regGlowingObsidianSlab(GlowingObsidianColor.RED);
    public static final RegisteredBlock VIOLET_GLOWING_OBSIDIAN_SLAB = regGlowingObsidianSlab(GlowingObsidianColor.VIOLET);
    public static final RegisteredBlock WHITE_GLOWING_OBSIDIAN_SLAB = regGlowingObsidianSlab(GlowingObsidianColor.WHITE);
    public static final RegisteredBlock YELLOW_GLOWING_OBSIDIAN_SLAB = regGlowingObsidianSlab(GlowingObsidianColor.YELLOW);
    public static final RegisteredBlock GOLD_GLOWING_OBSIDIAN_SLAB = regGlowingObsidianSlab(GlowingObsidianColor.GOLD);
    public static final RegisteredBlock SILVER_GLOWING_OBSIDIAN_SLAB = regGlowingObsidianSlab(GlowingObsidianColor.SILVER);
    public static final RegisteredBlock IRON_GLOWING_OBSIDIAN_SLAB = regGlowingObsidianSlab(GlowingObsidianColor.IRON);
    public static final RegisteredBlock COPPER_GLOWING_OBSIDIAN_SLAB = regGlowingObsidianSlab(GlowingObsidianColor.COPPER);

    public static void init() { }

    private static RegisteredBlock regSlab(String name, BlockType type, ItemLike craftItem) {
        return regSlabInternal(name, type.getProperties(), craftItem)
            .tags(type.tags())
            .tag(BlockTags.SLABS);
    }

    private static RegisteredBlock regWoodSlab(String name, boolean glows, ItemLike planks) {
        return regSlabInternal(
            name,
            Block.Properties.copy(Blocks.OAK_SLAB)
                .lightLevel(s -> glows ? 7 : 0),
            planks
        )
            .tag(BlockTags.WOODEN_SLABS);
    }

    private static RegisteredBlock regStoneSlab(String name, SoundType stoneSound, ItemLike stone, ItemLike... stoneCuttingBlocks) {
        return regSlabInternal(
            name,

            Block.Properties.copy(Blocks.STONE_SLAB)
                .sound(stoneSound),
            stone
        )
            .recipe((item, generator) -> generator.stonecutterRecipe(stone, item, 2))
            .tags(
                BlockTags.SLABS,
                BlockTags.MINEABLE_WITH_PICKAXE
            )
            .stonecutterRecipes(2, stoneCuttingBlocks);
    }

    private static RegisteredBlock regSandstoneSlab(String name, SandstoneType type, ItemLike stone, ItemLike... stoneCuttingBlocks) {
        return regSlabInternal(
            type.getName() + name + "_sandstone",
            Block.Properties.copy(Blocks.SANDSTONE_SLAB),
            stone
        )
            .recipe((item, generator) -> generator.stonecutterRecipe(stone, item, 2))
            .tags(
                BlockTags.SLABS,
                BlockTags.MINEABLE_WITH_PICKAXE
            )
            .stonecutterRecipes(2, stoneCuttingBlocks)
            .blockStateGenerator((block, generator) -> genSandstoneSlabBlockState(name, type, block, generator));
    }

    // registers a glowing obsidian slab block
    private static RegisteredBlock regGlowingObsidianSlab(GlowingObsidianColor color) {
        var glowingObsidian = Building.GLOWING_OBSIDIAN.get(color);

        return regSlabInternal(
            color.getName() + "_glowing_obsidian",
            BlockType.STRONG_STONE
                .getProperties()
                .lightLevel(s -> 7),
                glowingObsidian
        )
            .recipe((item, generator) -> generator.stonecutterRecipe(glowingObsidian, item, 2))
            .tag(PMTags.Blocks.GLOWING_OBSIDIAN_SLABS)
            .blockStateGenerator((block, generator) -> genGlowingObsidianSlabBlockState(color, block, generator))
            .localizedName(
                color.localizedName(false) + " Glowing Obsidian Slab",
                "Losa de obsidiana brillante " + color.localizedName(true)
            );
    }

    private static RegisteredBlock regSlabInternal(String name, Block.Properties props, ItemLike craftItem) {
        return PMRegistries.regBlockItem(
            name + "_slab",
            () -> new SlabBlock(props)
        )
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .blockStateGenerator((block, generator) -> genSlabBlockState(name, block, generator))
            .itemModelAlreadyExists()
            .lootTable((block, generator) -> generator.slabDrop(block))
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.BUILDING_BLOCKS, item, 6)
                    .pattern("III")
                    .define('I', craftItem)
            )
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Slab",
                "Losa de " + Utils.localizedMaterialName(name, true)
            );
    }

    private static void genGlowingObsidianSlabBlockState(GlowingObsidianColor color, RegisteredBlock slabBlock, BlockStateGenerator generator) {
        var colorName = color.getName();
        var topTexture = new ResourceLocation("paradisemod:block/glowing_obsidian/" + colorName);
        var sideTexture = new ResourceLocation("paradisemod:block/glowing_obsidian/" + colorName + "_slab");
        genSlabBlockState(colorName + "_glowing_obsidian", slabBlock, topTexture, topTexture, sideTexture, generator);
    }

    private static void genSandstoneSlabBlockState(String name, SandstoneType type, RegisteredBlock slabBlock, BlockStateGenerator generator) {
        var sandstoneName = type.getName() + name + "_sandstone";
        var topTexture = new ResourceLocation("paradisemod:block/" + name + "_sandstone_top");
        var bottomTexture = new ResourceLocation("paradisemod:block/" + name + "_sandstone" + (type == SandstoneType.NORMAL ? "_bottom" : "_top"));
        var sideTexture = new ResourceLocation("paradisemod:block/" + sandstoneName);
        if(type == SandstoneType.SMOOTH)
            sideTexture = new ResourceLocation("paradisemod:block/" + name + "_sandstone_top");

        genSlabBlockState(sandstoneName, slabBlock, topTexture, bottomTexture, sideTexture, generator);
    }

    private static void genSlabBlockState(String name, RegisteredBlock slabBlock, BlockStateGenerator generator) {
        var topTexture = slabTexture(name, false);
        var sideTexture = slabTexture(name, true);
        genSlabBlockState(name, slabBlock, topTexture, topTexture, sideTexture, generator);
    }

    private static void genSlabBlockState(String name, RegisteredBlock slabBlock, ResourceLocation topTexture, ResourceLocation bottomTexture, ResourceLocation sideTexture, BlockStateGenerator generator) {
        var modelBuilder = generator.models();
        var slab = modelBuilder.slab("block/slab/" + name, sideTexture, bottomTexture, topTexture);
        var slabTop = modelBuilder.slabTop("block/slab/" + name + "_top", sideTexture, bottomTexture, topTexture);
        var doubleSlab = generator.uncheckedExistingModel(topTexture);

        if(name.contains("sandstone"))
            doubleSlab = generator.uncheckedExistingModel(slabTexture(name, false));

        generator.slabBlock((SlabBlock) slabBlock.get(), slab, slabTop, doubleSlab);
        generator.itemModels().parentBlockItem(slabBlock.get(), "slab/" + name);
    }

    protected static ResourceLocation slabTexture(String name, boolean side) {
        if(side && name.contains("polished") && !name.contains("bricks"))
            return new ResourceLocation(
                switch(name) {
                    case "polished_end_stone", "polished_dripstone", "polished_calcite",
                    "polished_tuff", "polished_darkstone", "polished_asphalt" ->
                        "paradisemod:block/" + name + "_slab";

                    default -> "";
                }
            );
        else return new ResourceLocation(
            switch(name) {
                case "cactus" -> "paradisemod:block/cactus_block";
                case "glowing_cactus" -> "paradisemod:block/glowing_cactus_block";

                case "palo_verde", "mesquite", "blackened_oak", "blackened_spruce", "glowing_oak" ->
                    "paradisemod:block/" + name + "_planks";

                case "dripstone" -> "minecraft:block/dripstone_block";
                case "obsidian", "bedrock", "end_stone", "calcite", "tuff" -> "minecraft:block/" + name;

                default -> "paradisemod:block/" + name;
            }
        );
    }

    private enum SandstoneType {
        SMOOTH,
        CUT,
        NORMAL;

        private String getName() {
            return this == NORMAL ? "" : name().toLowerCase() + "_";
        }
    }
}