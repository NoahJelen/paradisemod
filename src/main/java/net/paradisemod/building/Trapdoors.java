package net.paradisemod.building;

import java.util.ArrayList;

import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.TrapDoorBlock;
import net.minecraftforge.client.model.generators.BlockModelBuilder;
import net.minecraftforge.client.model.generators.ModelFile;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.PMBlockSetTypes;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.building.blocks.RedstoneTrapdoor;
import net.paradisemod.misc.Misc;
import net.paradisemod.world.DeepDarkBlocks;

public class Trapdoors {
    // stone trapdoors
    public static final RegisteredBlock ANDESITE_TRAPDOOR = regTrapdoor("andesite", BlockType.STONE, 0, true, Blocks.ANDESITE);
    public static final RegisteredBlock BEDROCK_TRAPDOOR = regTrapdoor("bedrock", BlockType.INDESTRUCTIBLE, 0, true, Blocks.BEDROCK);
    public static final RegisteredBlock COBBLESTONE_TRAPDOOR = regTrapdoor("cobblestone", BlockType.STONE, 0, true, Blocks.COBBLESTONE);
    public static final RegisteredBlock DIORITE_TRAPDOOR = regTrapdoor("diorite", BlockType.STONE, 0, true, Blocks.DIORITE);
    public static final RegisteredBlock END_STONE_TRAPDOOR = regTrapdoor("end_stone", BlockType.STONE, 0, true, Blocks.END_STONE);
    public static final RegisteredBlock GRANITE_TRAPDOOR = regTrapdoor("granite", BlockType.STONE, 0, true, Blocks.GRANITE);
    public static final RegisteredBlock MOSSY_COBBLESTONE_TRAPDOOR = regTrapdoor("mossy_cobblestone", BlockType.STONE, 0, true, Blocks.MOSSY_COBBLESTONE);
    public static final RegisteredBlock OBSIDIAN_TRAPDOOR = regTrapdoor("obsidian", BlockType.STRONG_STONE, 0, true, Blocks.OBSIDIAN);
    public static final RegisteredBlock STONE_TRAPDOOR = regTrapdoor("stone", BlockType.STONE, 0, true, Blocks.STONE);

    // metal trapdoors
    public static final RegisteredBlock DIAMOND_TRAPDOOR = regTrapdoor("diamond", BlockType.METAL, 0, true, Items.DIAMOND);
    public static final RegisteredBlock GOLD_TRAPDOOR = regTrapdoor("gold", BlockType.METAL, 0, true, Items.GOLD_INGOT);
    public static final RegisteredBlock EMERALD_TRAPDOOR = regTrapdoor("emerald", BlockType.WEAK_METAL, 0, true, Items.EMERALD);
    public static final RegisteredBlock RUBY_TRAPDOOR = regTrapdoor("ruby", BlockType.WEAK_METAL, 0, true, Misc.RUBY);
    public static final RegisteredBlock RUSTED_IRON_TRAPDOOR = regTrapdoor("rusted_iron", BlockType.METAL, 0, true, Misc.RUSTED_IRON_INGOT);
    public static final RegisteredBlock SILVER_TRAPDOOR = regTrapdoor("silver", BlockType.METAL, 0, true, Misc.SILVER_INGOT);

    public static final RegisteredBlock REDSTONE_TRAPDOOR = PMRegistries.regBlockItem(
        "redstone_trapdoor",
        RedstoneTrapdoor::new
    )
        .tab(CreativeModeTabs.BUILDING_BLOCKS)
        .tag(BlockTags.TRAPDOORS)
        .localizedName("Redstone Trapdoor", "Trampilla de piedra roja");

    // wooden (and glass) trapdoors
    public static final RegisteredBlock CACTUS_TRAPDOOR = regTrapdoor("cactus", BlockType.WOOD, 0, true, Building.CACTUS_BLOCK);
    public static final RegisteredBlock PALO_VERDE_TRAPDOOR = regTrapdoor("palo_verde", BlockType.WOOD, 0, false, Building.PALO_VERDE_PLANKS);
    public static final RegisteredBlock MESQUITE_TRAPDOOR = regTrapdoor("mesquite", BlockType.WOOD, 0, false, Building.MESQUITE_PLANKS);
    public static final RegisteredBlock GLASS_TRAPDOOR = regTrapdoor("glass", BlockType.GLASS, 0, false, Blocks.GLASS);

    public static final RegisteredBlock BLACKENED_OAK_TRAPDOOR = regTrapdoor("blackened_oak", BlockType.WOOD, 0, false, DeepDarkBlocks.BLACKENED_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_SPRUCE_TRAPDOOR = regTrapdoor("blackened_spruce", BlockType.WOOD, 0, false, DeepDarkBlocks.BLACKENED_SPRUCE_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_OAK_TRAPDOOR = regTrapdoor("glowing_oak", BlockType.WOOD, 7, true, DeepDarkBlocks.GLOWING_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_CACTUS_TRAPDOOR = regTrapdoor("glowing_cactus", BlockType.WOOD, 7, true, DeepDarkBlocks.GLOWING_CACTUS_BLOCK)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    // glowing obsidian trapdoors
    public static final RegisteredBlock BLACK_GLOWING_OBSIDIAN_TRAPDOOR = regGlowingObsidianTrapdoor(GlowingObsidianColor.BLACK);
    public static final RegisteredBlock BLUE_GLOWING_OBSIDIAN_TRAPDOOR = regGlowingObsidianTrapdoor(GlowingObsidianColor.BLUE);
    public static final RegisteredBlock GREEN_GLOWING_OBSIDIAN_TRAPDOOR = regGlowingObsidianTrapdoor(GlowingObsidianColor.GREEN);
    public static final RegisteredBlock INDIGO_GLOWING_OBSIDIAN_TRAPDOOR = regGlowingObsidianTrapdoor(GlowingObsidianColor.INDIGO);
    public static final RegisteredBlock ORANGE_GLOWING_OBSIDIAN_TRAPDOOR = regGlowingObsidianTrapdoor(GlowingObsidianColor.ORANGE);
    public static final RegisteredBlock RED_GLOWING_OBSIDIAN_TRAPDOOR = regGlowingObsidianTrapdoor(GlowingObsidianColor.RED);
    public static final RegisteredBlock VIOLET_GLOWING_OBSIDIAN_TRAPDOOR = regGlowingObsidianTrapdoor(GlowingObsidianColor.VIOLET);
    public static final RegisteredBlock WHITE_GLOWING_OBSIDIAN_TRAPDOOR = regGlowingObsidianTrapdoor(GlowingObsidianColor.WHITE);
    public static final RegisteredBlock YELLOW_GLOWING_OBSIDIAN_TRAPDOOR = regGlowingObsidianTrapdoor(GlowingObsidianColor.YELLOW);
    public static final RegisteredBlock GOLD_GLOWING_OBSIDIAN_TRAPDOOR = regGlowingObsidianTrapdoor(GlowingObsidianColor.GOLD);
    public static final RegisteredBlock SILVER_GLOWING_OBSIDIAN_TRAPDOOR = regGlowingObsidianTrapdoor(GlowingObsidianColor.SILVER);
    public static final RegisteredBlock IRON_GLOWING_OBSIDIAN_TRAPDOOR = regGlowingObsidianTrapdoor(GlowingObsidianColor.IRON);
    public static final RegisteredBlock COPPER_GLOWING_OBSIDIAN_TRAPDOOR = regGlowingObsidianTrapdoor(GlowingObsidianColor.COPPER);

    public static void init() { }

    private static RegisteredBlock regGlowingObsidianTrapdoor(GlowingObsidianColor color) {
        var glowingObsidian = Building.GLOWING_OBSIDIAN.get(color);
        return regTrapdoor(color.getName() + "_glowing_obsidian", BlockType.STRONG_STONE, 7, true, glowingObsidian)
            .localizedName(
                color.localizedName(false) + " Glowing Obsidian Trapdoor",
                "Trampilla de obsidiana brillante " + color.localizedName(true)
            );
    }

    private static RegisteredBlock regTrapdoor(String name, BlockType type, int lightLevel, boolean fancy, ItemLike craftItem) {
        var blockSetType = switch(type) {
            case WEAK_METAL -> PMBlockSetTypes.WEAK_METAL;
            case METAL -> PMBlockSetTypes.METAL;
            case GLASS -> PMBlockSetTypes.GLASS;
            case WOOD -> PMBlockSetTypes.VARIANT_WOOD;
            default -> PMBlockSetTypes.VARIANT_STONE;
        };

        ArrayList<TagKey<Block>> tags = new ArrayList<>();

        if(type == BlockType.WOOD) tags.add(BlockTags.WOODEN_TRAPDOORS);
        else {
            tags.addAll(type.tags());
            tags.add(BlockTags.TRAPDOORS);
        }

        var trapdoor = PMRegistries.regBlockItem(name + "_trapdoor",
            () -> new TrapDoorBlock(
                type.getProperties()
                    .noOcclusion()
                    .lightLevel(s -> lightLevel),
                    blockSetType
            )
        )
            .recipe(
                (item, generator) -> {
                    var recipe = generator.getShapedBuilder(RecipeCategory.REDSTONE, item, 2)
                        .pattern("###")
                        .pattern("###")
                        .define('#', craftItem);

                    if(type.isMetal()) {
                        recipe = generator.getShapedBuilder(RecipeCategory.REDSTONE, item)
                            .pattern("##")
                            .pattern("##")
                            .define('#', craftItem)
                            .group("metal_trapdoors");
                    }

                    return recipe;
                }
            )
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .tags(tags)
            .itemModel((block, generator) -> generator.parentBlockItem(block, "trapdoor/" + name + "_bottom"))
            .blockStateGenerator((block, generator) -> genTrapdoorBlockState(name, type, fancy, block, generator))
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Trapdoor",
                "Trampilla de " + Utils.localizedMaterialName(name, true)
            );

        if(type.isStone())
            trapdoor = trapdoor.recipe((item, generator) -> generator.stonecutterRecipe(craftItem, item));

        return trapdoor;
    }

    private static void genTrapdoorBlockState(String name, BlockType type, boolean fancy, RegisteredBlock trapdoor, BlockStateGenerator generator) {
        var renderType = type == BlockType.GLASS ? "cutout" : "solid";
        var texture = "paradisemod:block/trapdoor/" + name;

        if(name.endsWith("_glowing_obsidian")) {
            var color = name.substring(0, name.length() - 17);
            texture = "paradisemod:block/glowing_obsidian/" + color;
        }
        else if(type == BlockType.STONE || type == BlockType.ENHANCED_STONE || type == BlockType.STRONG_STONE|| type == BlockType.INDESTRUCTIBLE)
            texture = "minecraft:block/" + name;
        else if(name == "glass") texture = "minecraft:block/glass";
        else if(name == "redstone" || name == "emerald" || name == "diamond" || name == "gold")
            texture = "minecraft:block/" + name + "_block";
        else if(name == "ruby" || name == "rusted_iron" || name == "silver")
            texture = "paradisemod:block/" + name + "_block";

        var modelBuilder = generator.models();
        var bottom = genTrapdoorModel(name, texture, "bottom", renderType, fancy, generator, modelBuilder::trapdoorOrientableBottom);
        var top = genTrapdoorModel(name, texture, "top", renderType, fancy, generator, modelBuilder::trapdoorOrientableTop);
        var open = genTrapdoorModel(name, texture, "open", renderType, fancy, generator, modelBuilder::trapdoorOrientableOpen);
        generator.trapdoorBlock((TrapDoorBlock) trapdoor.get(), bottom, top, open, true);
    }

    public static ModelFile genTrapdoorModel(
        String name,
        String texture,
        String suffix,
        String renderType,
        boolean fancy,
        BlockStateGenerator generator,
        TrapdoorModelGenerator modelGenerator
    ) {
        var modelBuilder = generator.models();
        var modelName = "trapdoor/" + name + "_" + suffix;

        if(fancy) {
            try {
                return generator.existingModel(modelName);
            }
            catch(Exception e) {
                return modelBuilder.withExistingParent("block/" + modelName, generator.modLoc("block/template/custom_trapdoor_" + suffix))
                    .texture("texture", new ResourceLocation(texture))
                    .renderType(renderType);
            }
        }
        else return modelGenerator.generate("block/" + modelName, new ResourceLocation(texture))
            .renderType(renderType);
    }

    @FunctionalInterface
    public interface TrapdoorModelGenerator {
        BlockModelBuilder generate(String name, ResourceLocation texture);
    }
}