package net.paradisemod.building;

import java.util.ArrayList;

import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FenceGateBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.properties.BlockSetType;
import net.minecraft.world.level.block.state.properties.WoodType;
import net.minecraftforge.common.Tags;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.PMBlockSetTypes;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.building.blocks.RedstoneFenceGate;
import net.paradisemod.misc.Misc;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.Ores;

public class FenceGates {
    private static WoodType METAL_GATE = fenceGateType("metal_gate", PMBlockSetTypes.WEAK_METAL);
    private static WoodType BRICK_GATE = fenceGateType("brick_gate", PMBlockSetTypes.VARIANT_STONE);

    // fence gates
    public static final RegisteredBlock BRICK_FENCE_GATE = regFenceGate(BlockType.STONE, "brick", false, Items.BRICK, Blocks.BRICKS);
    public static final RegisteredBlock CACTUS_FENCE_GATE = regFenceGate(BlockType.WOOD, "cactus", false, Building.CACTUS_BLOCK);
    public static final RegisteredBlock PALO_VERDE_FENCE_GATE = regFenceGate(BlockType.WOOD, "palo_verde", false, Building.PALO_VERDE_PLANKS);
    public static final RegisteredBlock MESQUITE_FENCE_GATE = regFenceGate(BlockType.WOOD, "mesquite", false, Building.MESQUITE_PLANKS);
    public static final RegisteredBlock DIAMOND_FENCE_GATE = regFenceGate(BlockType.METAL, "diamond", false, Items.DIAMOND, Blocks.DIAMOND_BLOCK);
    public static final RegisteredBlock EMERALD_FENCE_GATE = regFenceGate(BlockType.METAL, "emerald", false, Items.EMERALD, Blocks.EMERALD_BLOCK);
    public static final RegisteredBlock GOLD_FENCE_GATE = regFenceGate(BlockType.METAL, "gold", false, Items.GOLD_INGOT, Blocks.GOLD_BLOCK);
    public static final RegisteredBlock IRON_FENCE_GATE = regFenceGate(BlockType.WEAK_METAL, "iron", false, Items.IRON_INGOT, Blocks.IRON_BLOCK);

    public static final RegisteredBlock REDSTONE_FENCE_GATE = PMRegistries.regBlockItem("redstone_fence_gate", RedstoneFenceGate::new)
        .tags(
            Tags.Blocks.FENCE_GATES,
            BlockTags.MINEABLE_WITH_PICKAXE,
            BlockTags.NEEDS_IRON_TOOL
        )
        .tab(CreativeModeTabs.BUILDING_BLOCKS)
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item)
                .pattern("SPS")
                .pattern("SPS")
                .define('S', Items.REDSTONE_TORCH)
                .define('P', Blocks.REDSTONE_BLOCK)
        )
        .localizedName("Redstone Fence Gate", "Puerta de valla de piedra roja");

    public static final RegisteredBlock RUBY_FENCE_GATE = regFenceGate(BlockType.METAL, "ruby", false, Misc.RUBY, Ores.RUBY_BLOCK);
    public static final RegisteredBlock RUSTED_IRON_FENCE_GATE = regFenceGate(BlockType.WEAK_METAL, "rusted_iron", false, Misc.RUSTED_IRON_INGOT, Ores.RUSTED_IRON_BLOCK);
    public static final RegisteredBlock SILVER_FENCE_GATE = regFenceGate(BlockType.METAL, "silver", false, Misc.SILVER_INGOT, Ores.SILVER_BLOCK);

    public static final RegisteredBlock BLACKENED_OAK_FENCE_GATE = regFenceGate(BlockType.WOOD, "blackened_oak", false, DeepDarkBlocks.BLACKENED_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_SPRUCE_FENCE_GATE = regFenceGate(BlockType.WOOD, "blackened_spruce", false, DeepDarkBlocks.BLACKENED_SPRUCE_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_OAK_FENCE_GATE = regFenceGate(BlockType.WOOD, "glowing_oak", true, DeepDarkBlocks.GLOWING_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_CACTUS_FENCE_GATE = regFenceGate(BlockType.WOOD, "glowing_cactus", true, DeepDarkBlocks.GLOWING_CACTUS_BLOCK)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static void init() { }

    private static RegisteredBlock regFenceGate(BlockType type, String name, boolean glows, ItemLike planks) {
        return regFenceGate(type, name, glows, Items.STICK, planks);
    }

    private static RegisteredBlock regFenceGate(BlockType type, String name, boolean glows, ItemLike craftItem, ItemLike craftBlock) {
        ArrayList<TagKey<Block>> tags = new ArrayList<>();
        WoodType gateType;
        if(type == BlockType.WEAK_METAL || type == BlockType.METAL)
            gateType = METAL_GATE;
        else if(type == BlockType.STONE)
            gateType = BRICK_GATE;
        else gateType = WoodType.OAK;

        if(type == BlockType.WOOD) {
            tags.add(Tags.Blocks.FENCE_GATES_WOODEN);
            tags.add(BlockTags.FENCE_GATES);
        }
        else {
            tags.add(Tags.Blocks.FENCE_GATES);
            tags.addAll(type.tags());
        }

        return PMRegistries.regBlockItem(
            name + "_fence_gate",
            () -> new FenceGateBlock(type.getProperties().lightLevel(s -> glows ? 7 : 0), gateType)
        )
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .tags(tags)
            .blockStateGenerator((gate, generator) -> genGateBlockState(name, gate, generator))
            .itemModelAlreadyExists()
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item)
                    .pattern("SPS")
                    .pattern("SPS")
                    .define('S', craftItem)
                    .define('P', craftBlock)
            )
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Fence Gate",
                "Puerta de valla de " + Utils.localizedMaterialName(name, true)
            );
    }

    private static void genGateBlockState(String name, RegisteredBlock gateBlock, BlockStateGenerator generator) {
        var modelName = "block/fence_gate/" + name;
        var modelBuilder = generator.models();
        var texture = Fences.fenceTexture(name);
        var gate = modelBuilder.fenceGate(modelName, texture);
        var gateOpen = modelBuilder.fenceGateOpen(modelName + "_open", texture);
        var gateWall = modelBuilder.fenceGateWall(modelName + "_wall", texture);
        var gateWallOpen = modelBuilder.fenceGateWallOpen(modelName + "_wall_open", texture);

        generator.itemModels()
            .parentBlockItem(gateBlock.get(), "fence_gate/" + name);

        generator.fenceGateBlock((FenceGateBlock) gateBlock.get(), gate, gateOpen, gateWall, gateWallOpen);
    }

    private static WoodType fenceGateType(String name, BlockSetType blockSetType) {
        return new WoodType(name, blockSetType, blockSetType.soundType(), SoundType.HANGING_SIGN, SoundEvents.FENCE_GATE_CLOSE, SoundEvents.FENCE_GATE_OPEN);
    }
}
