package net.paradisemod.building.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.SimpleWaterloggedBlock;
import net.minecraft.world.level.block.TrapDoorBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.Half;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.PMBlockSetTypes;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ItemModelGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;
import net.paradisemod.building.Trapdoors;

public class RedstoneTrapdoor extends HorizontalDirectionalBlock implements SimpleWaterloggedBlock, ModeledBlock {
	protected static final VoxelShape EAST_OPEN_AABB = Block.box(0, 0, 0, 3, 16, 16);
	protected static final VoxelShape WEST_OPEN_AABB = Block.box(13, 0, 0, 16, 16, 16);
	protected static final VoxelShape SOUTH_OPEN_AABB = Block.box(0, 0, 0, 16, 16, 3);
	protected static final VoxelShape NORTH_OPEN_AABB = Block.box(0, 0, 13, 16, 16, 16);
	protected static final VoxelShape BOTTOM_AABB = Block.box(0, 0, 0, 16, 3, 16);
	protected static final VoxelShape TOP_AABB = Block.box(0, 13, 0, 16, 16, 16);

	public RedstoneTrapdoor() {
		super(BlockType.WEAK_METAL.getProperties().noOcclusion());
		registerDefaultState(
			stateDefinition.any()
				.setValue(FACING, Direction.NORTH)
				.setValue(TrapDoorBlock.OPEN, false)
				.setValue(TrapDoorBlock.HALF, Half.BOTTOM)
				.setValue(TrapDoorBlock.WATERLOGGED, false)
		);
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter world, BlockPos pos, CollisionContext context) {
		if (!state.getValue(TrapDoorBlock.OPEN))
			return state.getValue(TrapDoorBlock.HALF) == Half.TOP ? TOP_AABB : BOTTOM_AABB;
		else
			return switch (state.getValue(FACING)) {
				default -> NORTH_OPEN_AABB;
				case SOUTH -> SOUTH_OPEN_AABB;
				case WEST -> WEST_OPEN_AABB;
				case EAST -> EAST_OPEN_AABB;
			};
	}

	@Override
	public boolean isPathfindable(BlockState state, BlockGetter world, BlockPos pos, PathComputationType type) {
		return switch(type) {
			case LAND, AIR -> state.getValue(TrapDoorBlock.OPEN);
			case WATER -> state.getValue(TrapDoorBlock.WATERLOGGED);
		};
	}

	@Override
	public InteractionResult use(BlockState state, Level world, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hit) {
		state = state.cycle(TrapDoorBlock.OPEN);
        world.setBlock(pos, state, 2);
        if (state.getValue(TrapDoorBlock.WATERLOGGED))
            world.scheduleTick(pos, Fluids.WATER, Fluids.WATER.getTickDelay(world));

        world.playSound(player, pos, state.getValue(DoorBlock.OPEN) ? PMBlockSetTypes.WEAK_METAL.trapdoorOpen() : PMBlockSetTypes.WEAK_METAL.trapdoorClose(), SoundSource.BLOCKS, 1.0F, world.getRandom().nextFloat() * 0.1F + 0.9F);
        world.gameEvent(player, state.getValue(DoorBlock.OPEN) ? GameEvent.BLOCK_OPEN : GameEvent.BLOCK_CLOSE, pos);
        return InteractionResult.sidedSuccess(world.isClientSide);
	}

	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context) {
		var blockstate = this.defaultBlockState();
		var fluidstate = context.getLevel().getFluidState(context.getClickedPos());
		var direction = context.getClickedFace();
		if (!context.replacingClickedOnBlock() && direction.getAxis().isHorizontal())
			blockstate = blockstate.setValue(FACING, direction)
				.setValue(TrapDoorBlock.HALF, context.getClickLocation().y - (double)context.getClickedPos().getY() > 0.5D ? Half.TOP : Half.BOTTOM);
		else
			blockstate = blockstate.setValue(FACING, context.getHorizontalDirection().getOpposite())
				.setValue(TrapDoorBlock.HALF, direction == Direction.UP ? Half.BOTTOM : Half.TOP);

		return blockstate.setValue(TrapDoorBlock.WATERLOGGED, fluidstate.getType() == Fluids.WATER);
	}

	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
		builder.add(FACING, TrapDoorBlock.OPEN, TrapDoorBlock.HALF, TrapDoorBlock.WATERLOGGED);
	}

	@Override
	public FluidState getFluidState(BlockState state) {
		return state.getValue(TrapDoorBlock.WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(state);
	}

	@Override
	public BlockState updateShape(BlockState stateIn, Direction facing, BlockState facingState, LevelAccessor world, BlockPos currentPos, BlockPos facingPos) {
		if (stateIn.getValue(TrapDoorBlock.WATERLOGGED))
			world.scheduleTick(currentPos, Fluids.WATER, Fluids.WATER.getTickDelay(world));

		return super.updateShape(stateIn, facing, facingState, world, currentPos, facingPos);
	}

	@Override
	public boolean isLadder(BlockState state, LevelReader world, BlockPos pos, LivingEntity entity) {
		if (state.getValue(TrapDoorBlock.OPEN)) {
			var downPos = pos.below();
			var down = world.getBlockState(downPos);
			return down.getBlock().makesOpenTrapdoorAboveClimbable(down, world, downPos, state);
		}

		return false;
	}

	@Override
	public int getSignal(BlockState blockState, BlockGetter blockAccess, BlockPos pos, Direction side) { return 15; }

	@Override
	public void genBlockState(BlockStateGenerator generator) {
		var modelBuilder = generator.models();
        var bottom = Trapdoors.genTrapdoorModel("redstone", "minecraft:block/redstone_block", "bottom", "solid", true, generator, modelBuilder::trapdoorOrientableBottom);
        var top = Trapdoors.genTrapdoorModel("redstone", "minecraft:block/redstone_block", "top", "solid", true, generator, modelBuilder::trapdoorOrientableTop);
        var open = Trapdoors.genTrapdoorModel("redstone", "minecraft:block/redstone_block", "open", "solid", true, generator, modelBuilder::trapdoorOrientableOpen);

		generator.getVariantBuilder(this)
			.forAllStatesExcept(
				state -> {
					int xRot = 0;
					int yRot = ((int) state.getValue(FACING).toYRot()) + 180;
					boolean isOpen = state.getValue(TrapDoorBlock.OPEN);

					if (isOpen && state.getValue(TrapDoorBlock.HALF) == Half.TOP) {
						xRot += 180;
						yRot += 180;
					}

					yRot %= 360;

					return generator.buildVariantModel(isOpen ? open : state.getValue(TrapDoorBlock.HALF) == Half.TOP ? top : bottom, xRot, yRot);
				},
				TrapDoorBlock.WATERLOGGED
			);
	}

	@Override
	public void genItemModel(ItemModelGenerator generator) {
		generator.parentBlockItem(this, "trapdoor/redstone_bottom");
	}
}