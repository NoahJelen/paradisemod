package net.paradisemod.building.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.sounds.SoundSource;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.FenceGateBlock;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.WoodType;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.client.model.generators.ModelFile;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;
import net.paradisemod.building.Fences;

public class RedstoneFenceGate extends HorizontalDirectionalBlock implements ModeledBlock {
	protected static final VoxelShape AABB_HITBOX_ZAXIS = Block.box(0, 0, 6, 16, 16, 10);
	protected static final VoxelShape AABB_HITBOX_XAXIS = Block.box(6, 0, 0, 10, 16, 16);
	protected static final VoxelShape AABB_HITBOX_ZAXIS_INWALL = Block.box(0, 0, 6, 16, 13, 10);
	protected static final VoxelShape AABB_HITBOX_XAXIS_INWALL = Block.box(6, 0, 0, 10, 13, 16);
	protected static final VoxelShape AABB_COLLISION_BOX_ZAXIS = Block.box(0, 0, 6, 16, 24, 10);
	protected static final VoxelShape AABB_COLLISION_BOX_XAXIS = Block.box(6, 0, 0, 10, 24, 16);
	protected static final VoxelShape AABB_RENDER_BOX_ZAXIS = Shapes.or(Block.box(0, 5, 7, 2, 16, 9), Block.box(14, 5, 7, 16, 16, 9));
	protected static final VoxelShape AABB_RENDER_BOX_XAXIS = Shapes.or(Block.box(7, 5, 0, 9, 16, 2), Block.box(7, 5, 14, 9, 16, 16));
	protected static final VoxelShape AABB_RENDER_BOX_ZAXIS_INWALL = Shapes.or(Block.box(0, 2, 7, 2, 13, 9), Block.box(14, 2, 7, 16, 13, 9));
	protected static final VoxelShape AABB_RENDER_BOX_XAXIS_INWALL = Shapes.or(Block.box(7, 2, 0, 9, 13, 2), Block.box(7, 2, 14, 9, 13, 16));

	public RedstoneFenceGate() {
		super(BlockType.METAL.getProperties().noOcclusion());
		registerDefaultState(
			stateDefinition.any()
				.setValue(FenceGateBlock.OPEN, false)
				.setValue(FenceGateBlock.IN_WALL, false)
		);
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter world, BlockPos pos, CollisionContext context) {
		if (state.getValue(FenceGateBlock.IN_WALL))
			return state.getValue(FACING).getAxis() == Direction.Axis.X ? AABB_HITBOX_XAXIS_INWALL : AABB_HITBOX_ZAXIS_INWALL;
		else
			return state.getValue(FACING).getAxis() == Direction.Axis.X ? AABB_HITBOX_XAXIS : AABB_HITBOX_ZAXIS;
	}

	@Override
	public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor world, BlockPos currentPos, BlockPos facingPos) {
		var axis = facing.getAxis();
		if (state.getValue(FACING).getClockWise().getAxis() != axis)
			return super.updateShape(state, facing, facingState, world, currentPos, facingPos);
		else {
			var inWall = isWall(facingState) || isWall(world.getBlockState(currentPos.relative(facing.getOpposite())));
			return state.setValue(FenceGateBlock.IN_WALL, inWall);
		}
	}

	@Override
	public VoxelShape getCollisionShape(BlockState state, BlockGetter world, BlockPos pos, CollisionContext context) {
		if (state.getValue(FenceGateBlock.OPEN)) return Shapes.empty();
		else
			return state.getValue(FACING).getAxis() == Direction.Axis.Z ? AABB_COLLISION_BOX_ZAXIS : AABB_COLLISION_BOX_XAXIS;
	}

	@Override
	public VoxelShape getOcclusionShape(BlockState state, BlockGetter world, BlockPos pos) {
		if (state.getValue(FenceGateBlock.IN_WALL))
			return state.getValue(FACING).getAxis() == Direction.Axis.X ? AABB_RENDER_BOX_XAXIS_INWALL : AABB_RENDER_BOX_ZAXIS_INWALL;
		else
			return state.getValue(FACING).getAxis() == Direction.Axis.X ? AABB_RENDER_BOX_XAXIS : AABB_RENDER_BOX_ZAXIS;
	}

	@Override
	public boolean isPathfindable(BlockState state, BlockGetter world, BlockPos pos, PathComputationType pathType) {
		return switch (pathType) {
			case LAND, AIR -> state.getValue(FenceGateBlock.OPEN);
			default -> false;
		};
	}

	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context) {
		var world = context.getLevel();
		var pos = context.getClickedPos();
		var direction = context.getHorizontalDirection();
		var axis = direction.getAxis();
		var flag1 = axis == Direction.Axis.Z && (isWall(world.getBlockState(pos.west())) || isWall(world.getBlockState(pos.east()))) || axis == Direction.Axis.X && (isWall(world.getBlockState(pos.north())) || isWall(world.getBlockState(pos.south())));
		return defaultBlockState()
			.setValue(FACING, direction)
			.setValue(FenceGateBlock.OPEN, false)
			.setValue(FenceGateBlock.IN_WALL, flag1);
	}

	private boolean isWall(BlockState state) { return state.is(BlockTags.WALLS); }

	@Override
	public InteractionResult use(BlockState state, Level world, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hit) {
		if (state.getValue(FenceGateBlock.OPEN))
			state = state.setValue(FenceGateBlock.OPEN, false);
		else {
			var direction = player.getDirection();
			if (state.getValue(FACING) == direction.getOpposite())
				state = state.setValue(FACING, direction);

			state = state.setValue(FenceGateBlock.OPEN, true);
		}

		world.setBlock(pos, state, 10);
		world.playSound(player, pos, state.getValue(FenceGateBlock.OPEN) ? WoodType.OAK.fenceGateOpen() : WoodType.OAK.fenceGateClose(), SoundSource.BLOCKS, 1.0F, world.getRandom().nextFloat() * 0.1F + 0.9F);
		world.gameEvent(player, state.getValue(FenceGateBlock.OPEN) ? GameEvent.BLOCK_OPEN : GameEvent.BLOCK_CLOSE, pos);
		return InteractionResult.sidedSuccess(world.isClientSide);
	}

	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
		builder.add(FACING, FenceGateBlock.OPEN, FenceGateBlock.IN_WALL);
	}

	@Override
	public int getSignal(BlockState blockState, BlockGetter blockAccess, BlockPos pos, Direction side) { return 15; }

	@Override
	public void genBlockState(BlockStateGenerator generator) {
		var texture = Fences.fenceTexture("redstone");
		var modelBuilder = generator.models();
        var gate = modelBuilder.fenceGate("block/fence_gate/redstone", texture);
        var gateOpen = modelBuilder.fenceGateOpen("block/fence_gate/redstone_open", texture);
        var gateWall = modelBuilder.fenceGateWall("block/fence_gate/redstone_wall", texture);
        var gateWallOpen = modelBuilder.fenceGateWallOpen("block/fence_gate/redstone_wall_open", texture);
		generator.getVariantBuilder(this)
			.forAllStates(
				state -> {
					ModelFile model = gate;
					if (state.getValue(FenceGateBlock.IN_WALL))
						model = gateWall;

					if (state.getValue(FenceGateBlock.OPEN))
						model = model == gateWall ? gateWallOpen : gateOpen;

					return generator.buildVariantModel(model, 0, (int) state.getValue(FenceGateBlock.FACING).toYRot());
				}
			);

		generator.itemModels()
            .parentBlockItem(this, "fence_gate/redstone");
	}
}