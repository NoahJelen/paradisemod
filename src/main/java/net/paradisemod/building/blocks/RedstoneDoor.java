package net.paradisemod.building.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.DoorHingeSide;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.level.material.PushReaction;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.client.model.generators.ModelFile;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.PMBlockSetTypes;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ItemModelGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;
import net.paradisemod.building.Doors;

public class RedstoneDoor extends Block implements ModeledBlock {
	protected static final VoxelShape SOUTH_AABB = Block.box(0, 0, 0, 16, 16, 3);
	protected static final VoxelShape NORTH_AABB = Block.box(0, 0, 13, 16, 16, 16);
	protected static final VoxelShape WEST_AABB = Block.box(13, 0, 0, 16, 16, 16);
	protected static final VoxelShape EAST_AABB = Block.box(0, 0, 0, 3, 16, 16);

	public RedstoneDoor() {
		super(BlockType.WEAK_METAL.getProperties().noOcclusion());
		this.registerDefaultState(
			stateDefinition.any()
				.setValue(DoorBlock.FACING, Direction.NORTH)
				.setValue(DoorBlock.OPEN, false)
				.setValue(DoorBlock.HINGE, DoorHingeSide.LEFT)
				.setValue(DoorBlock.HALF, DoubleBlockHalf.LOWER)
		);
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter world, BlockPos pos, CollisionContext ctx) {
		var direction = state.getValue(DoorBlock.FACING);
		var flag = !state.getValue(DoorBlock.OPEN);
		var flag1 = state.getValue(DoorBlock.HINGE) == DoorHingeSide.RIGHT;
		return switch (direction) {
			default -> flag ? EAST_AABB : (flag1 ? NORTH_AABB : SOUTH_AABB);
			case SOUTH -> flag ? SOUTH_AABB : (flag1 ? EAST_AABB : WEST_AABB);
			case WEST -> flag ? WEST_AABB : (flag1 ? SOUTH_AABB : NORTH_AABB);
			case NORTH -> flag ? NORTH_AABB : (flag1 ? WEST_AABB : EAST_AABB);
		};
	}

	@Override
	public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor world, BlockPos currentPos, BlockPos facingPos) {
		var doorHalf = state.getValue(DoorBlock.HALF);
		if (facing.getAxis() == Direction.Axis.Y && (doorHalf == DoubleBlockHalf.LOWER) == (facing == Direction.UP))
			return (facingState.is(this) && facingState.getValue(DoorBlock.HALF) != doorHalf) ?
				state.setValue(DoorBlock.FACING, facingState.getValue(DoorBlock.FACING))
					.setValue(DoorBlock.OPEN, facingState.getValue(DoorBlock.OPEN))
					.setValue(DoorBlock.HINGE, facingState.getValue(DoorBlock.HINGE)) :
				Blocks.AIR.defaultBlockState();
		else
			return (doorHalf == DoubleBlockHalf.LOWER && facing == Direction.DOWN && !state.canSurvive(world, currentPos)) ? Blocks.AIR.defaultBlockState() : super.updateShape(state, facing, facingState, world, currentPos, facingPos);
	}

	@Override
	public void playerWillDestroy(Level world, BlockPos pos, BlockState state, Player player) {
		if (!world.isClientSide && player.isCreative())
			world.setBlockAndUpdate(pos, Blocks.AIR.defaultBlockState());
		super.playerWillDestroy(world, pos, state, player);
	}

	@Override
	public boolean isPathfindable(BlockState state, BlockGetter world, BlockPos pos, PathComputationType pathType) {
		return switch (pathType) {
			case LAND, AIR -> state.getValue(DoorBlock.OPEN);
			default -> false;
		};
	}

	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context) {
		var blockpos = context.getClickedPos();
		var world = context.getLevel();
		if (blockpos.getY() < world.getMaxBuildHeight() - 1 && world.getBlockState(blockpos.above()).canBeReplaced(context))
			return this.defaultBlockState()
				.setValue(DoorBlock.FACING, context.getHorizontalDirection())
				.setValue(DoorBlock.HINGE, getHingeSide(context))
				.setValue(DoorBlock.HALF, DoubleBlockHalf.LOWER);
		
		else return null;
	}

	@Override
	public void setPlacedBy(Level world, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack) {
		world.setBlock(pos.above(), state.setValue(DoorBlock.HALF, DoubleBlockHalf.UPPER), 3);
	}

	private DoorHingeSide getHingeSide(BlockPlaceContext context) {
		var world = context.getLevel();
		var blockpos = context.getClickedPos();
		var direction = context.getHorizontalDirection();
		var pos1 = blockpos.above();
		var direction1 = direction.getCounterClockWise();
		var pos2 = blockpos.relative(direction1);
		var state = world.getBlockState(pos2);
		var pos3 = pos1.relative(direction1);
		var state1 = world.getBlockState(pos3);
		var direction2 = direction.getClockWise();
		var pos4 = blockpos.relative(direction2);
		var state2 = world.getBlockState(pos4);
		var pos5 = pos1.relative(direction2);
		var state3 = world.getBlockState(pos5);
		var i = (state.isCollisionShapeFullBlock(world, pos2) ? -1 : 0) + (state1.isCollisionShapeFullBlock(world, pos3) ? - 1 : 0) + (state2.isCollisionShapeFullBlock(world, pos4) ? 1 : 0) + (state3.isCollisionShapeFullBlock(world, pos5) ? 1 : 0);
		var flag = state.is(this) && state.getValue(DoorBlock.HALF) == DoubleBlockHalf.LOWER;
		var flag1 = state2.is(this) && state2.getValue(DoorBlock.HALF) == DoubleBlockHalf.LOWER;

		if ((!flag || flag1) && i <= 0) {
			if ((!flag1 || flag) && i == 0) {
				var j = direction.getStepX();
				var k = direction.getStepZ();
				var vec3d = context.getClickLocation();
				var d0 = vec3d.x - blockpos.getX();
				var d1 = vec3d.z - blockpos.getZ();
				return (j >= 0 || !(d1 < 0.5D)) && (j <= 0 || !(d1 > 0.5D)) && (k >= 0 || !(d0 > 0.5D)) && (k <= 0 || !(d0 < 0.5D)) ? DoorHingeSide.LEFT : DoorHingeSide.RIGHT;
			}
			else return DoorHingeSide.LEFT;

		}
		else return DoorHingeSide.RIGHT;
	}

	@Override
	public InteractionResult use(BlockState state, Level world, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hit) {
		state = state.cycle(DoorBlock.OPEN);
        world.setBlock(pos, state, 10);
		world.playSound(player, pos, state.getValue(DoorBlock.OPEN) ? PMBlockSetTypes.WEAK_METAL.doorOpen() : PMBlockSetTypes.WEAK_METAL.doorClose(), SoundSource.BLOCKS, 1.0F, world.getRandom().nextFloat() * 0.1F + 0.9F);
        world.gameEvent(player, state.getValue(DoorBlock.OPEN) ? GameEvent.BLOCK_OPEN : GameEvent.BLOCK_CLOSE, pos);
        return InteractionResult.sidedSuccess(world.isClientSide);
	}

	@Override
	public boolean canSurvive(BlockState state, LevelReader world, BlockPos pos) {
		var blockpos = pos.below();
		var blockBelow = world.getBlockState(blockpos);
		return state.getValue(DoorBlock.HALF) == DoubleBlockHalf.LOWER ? blockBelow.isFaceSturdy(world, blockpos, Direction.UP) : blockBelow.is(this);
	}

	@Override
	public PushReaction getPistonPushReaction(BlockState state) {
		return PushReaction.DESTROY;
	}

	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
		builder.add(DoorBlock.HALF, DoorBlock.FACING, DoorBlock.OPEN, DoorBlock.HINGE);
	}

	@Override
	public int getSignal(BlockState blockState, BlockGetter world, BlockPos pos, Direction side) { return 15; }

	@Override
	public void genBlockState(BlockStateGenerator generator) {
		var modelBuilder = generator.models();

        var bottom = Doors.genDoorModel(
            "redstone",
            "minecraft:block/redstone_block",
            "minecraft:block/redstone_block",
            "bottom",
            "solid",
            true,
            generator,
            modelBuilder::doorBottomLeft
        );

        var bottomHinge = Doors.genDoorModel(
            "redstone",
            "minecraft:block/redstone_block",
            "minecraft:block/redstone_block",
            "bottom_hinge",
            "solid",
            true,
            generator,
            modelBuilder::doorBottomRight
        );

        var top = Doors.genDoorModel(
            "redstone",
            "minecraft:block/redstone_block",
            "minecraft:block/redstone_block",
            "top",
            "solid",
            true,
            generator,
            modelBuilder::doorTopLeft
        );

        var topHinge = Doors.genDoorModel(
            "redstone",
            "minecraft:block/redstone_block",
            "minecraft:block/redstone_block",
            "top_hinge",
            "solid",
            true,
            generator,
            modelBuilder::doorTopRight
        );

		generator.getVariantBuilder(this)
			.forAllStates(
				state -> {
					int yRot = ((int) state.getValue(DoorBlock.FACING).toYRot()) + 90;
					boolean right = state.getValue(DoorBlock.HINGE) == DoorHingeSide.RIGHT;
					boolean open = state.getValue(DoorBlock.OPEN);
					boolean lower = state.getValue(DoorBlock.HALF) == DoubleBlockHalf.LOWER;
					if (open)
						yRot += 90;

					if (right && open)
						yRot += 180;

					yRot %= 360;

					ModelFile model = null;

					if (lower && right && open)
						model = bottomHinge;
					else if (lower && !right && open)
						model = bottom;

					if (lower && right && !open)
						model = bottomHinge;
					else if (lower && !right && !open)
						model = bottom;

					if (!lower && right && open)
						model = topHinge;
					else if (!lower && !right && open)
						model = top;

					if (!lower && right && !open)
						model = topHinge;
					else if (!lower && !right && !open)
						model = top;

					return generator.buildVariantModel(model, 0, yRot);
				}
			);
	}

	@Override
	public void genItemModel(ItemModelGenerator generator) {
		generator.basicItem(asItem(), "door/redstone", "cutout");
	}
}