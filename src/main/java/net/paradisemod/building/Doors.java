package net.paradisemod.building;

import java.util.ArrayList;

import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraftforge.client.model.generators.BlockModelBuilder;
import net.minecraftforge.client.model.generators.ModelFile;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.PMBlockSetTypes;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.building.blocks.RedstoneDoor;
import net.paradisemod.misc.Misc;
import net.paradisemod.world.DeepDarkBlocks;

public class Doors {
    // stone doors
    public static final RegisteredBlock ANDESITE_DOOR = regDoor("andesite", BlockType.STONE, 0, true, Blocks.ANDESITE);
    public static final RegisteredBlock BEDROCK_DOOR = regDoor("bedrock", BlockType.INDESTRUCTIBLE, 0, true, Blocks.BEDROCK);
    public static final RegisteredBlock COBBLESTONE_DOOR = regDoor("cobblestone", BlockType.STONE, 0, true, Blocks.COBBLESTONE);
    public static final RegisteredBlock DIORITE_DOOR = regDoor("diorite", BlockType.STONE, 0, true, Blocks.DIORITE);
    public static final RegisteredBlock END_STONE_DOOR = regDoor("end_stone", BlockType.STONE, 0, true, Blocks.END_STONE);
    public static final RegisteredBlock GRANITE_DOOR = regDoor("granite", BlockType.STONE, 0, true, Blocks.GRANITE);
    public static final RegisteredBlock MOSSY_COBBLESTONE_DOOR = regDoor("mossy_cobblestone", BlockType.STONE, 0, true, Blocks.MOSSY_COBBLESTONE);
    public static final RegisteredBlock OBSIDIAN_DOOR = regDoor("obsidian", BlockType.STRONG_STONE, 0, true, Blocks.OBSIDIAN);
    public static final RegisteredBlock STONE_DOOR = regDoor("stone", BlockType.STONE, 0, true, Blocks.STONE);

    // metal doors
    public static final RegisteredBlock DIAMOND_DOOR = regDoor("diamond", BlockType.METAL, 0, true, Items.DIAMOND);
    public static final RegisteredBlock GOLD_DOOR = regDoor("gold", BlockType.METAL, 0, true, Items.GOLD_INGOT);
    public static final RegisteredBlock EMERALD_DOOR = regDoor("emerald", BlockType.WEAK_METAL, 0, true, Items.EMERALD);
    public static final RegisteredBlock RUBY_DOOR = regDoor("ruby", BlockType.WEAK_METAL, 0, true, Misc.RUBY);
    public static final RegisteredBlock RUSTED_IRON_DOOR = regDoor("rusted_iron", BlockType.METAL, 0, true, Misc.RUSTED_IRON_INGOT);
    public static final RegisteredBlock SILVER_DOOR = regDoor("silver", BlockType.METAL, 0, true, Misc.SILVER_INGOT);

    public static final RegisteredBlock REDSTONE_DOOR = PMRegistries.regBlockItem(
        "redstone_door",
        RedstoneDoor::new
    )
        .tab(CreativeModeTabs.BUILDING_BLOCKS)
        .tags(
            BlockTags.MINEABLE_WITH_PICKAXE,
            BlockTags.NEEDS_IRON_TOOL
        )
        .lootTable((block, generator) -> generator.doorDrop(block))
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item)
                .pattern("##")
                .pattern("##")
                .pattern("##")
                .define('#', Items.REDSTONE)
        )
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item, 9)
                .pattern("##")
                .pattern("##")
                .pattern("##")
                .define('#', Blocks.REDSTONE_BLOCK)
        )
        .localizedName("Redstone Door", "Puerta de piedra roja");

    // wooden (and glass) doors
    public static final RegisteredBlock CACTUS_DOOR = regDoor("cactus", BlockType.WOOD, 0, true, Building.CACTUS_BLOCK);
    public static final RegisteredBlock PALO_VERDE_DOOR = regDoor("palo_verde", BlockType.WOOD, 0, false, Building.PALO_VERDE_PLANKS);
    public static final RegisteredBlock MESQUITE_DOOR = regDoor("mesquite", BlockType.WOOD, 0, false, Building.MESQUITE_PLANKS);
    public static final RegisteredBlock GLASS_DOOR = regDoor("glass", BlockType.GLASS, 0, false, Blocks.GLASS);

    public static final RegisteredBlock BLACKENED_OAK_DOOR = regDoor("blackened_oak", BlockType.WOOD, 0, false, DeepDarkBlocks.BLACKENED_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_SPRUCE_DOOR = regDoor("blackened_spruce", BlockType.WOOD, 0, false, DeepDarkBlocks.BLACKENED_SPRUCE_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_OAK_DOOR = regDoor("glowing_oak", BlockType.WOOD, 7, true, DeepDarkBlocks.GLOWING_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_CACTUS_DOOR = regDoor("glowing_cactus", BlockType.WOOD, 7, true, DeepDarkBlocks.GLOWING_CACTUS_BLOCK)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    // glowing obsidian doors
    public static final RegisteredBlock BLACK_GLOWING_OBSIDIAN_DOOR = regGlowingObsidianDoor(GlowingObsidianColor.BLACK);
    public static final RegisteredBlock BLUE_GLOWING_OBSIDIAN_DOOR = regGlowingObsidianDoor(GlowingObsidianColor.BLUE);
    public static final RegisteredBlock GREEN_GLOWING_OBSIDIAN_DOOR = regGlowingObsidianDoor(GlowingObsidianColor.GREEN);
    public static final RegisteredBlock INDIGO_GLOWING_OBSIDIAN_DOOR = regGlowingObsidianDoor(GlowingObsidianColor.INDIGO);
    public static final RegisteredBlock ORANGE_GLOWING_OBSIDIAN_DOOR = regGlowingObsidianDoor(GlowingObsidianColor.ORANGE);
    public static final RegisteredBlock RED_GLOWING_OBSIDIAN_DOOR = regGlowingObsidianDoor(GlowingObsidianColor.RED);
    public static final RegisteredBlock VIOLET_GLOWING_OBSIDIAN_DOOR = regGlowingObsidianDoor(GlowingObsidianColor.VIOLET);
    public static final RegisteredBlock WHITE_GLOWING_OBSIDIAN_DOOR = regGlowingObsidianDoor(GlowingObsidianColor.WHITE);
    public static final RegisteredBlock YELLOW_GLOWING_OBSIDIAN_DOOR = regGlowingObsidianDoor(GlowingObsidianColor.YELLOW);
    public static final RegisteredBlock GOLD_GLOWING_OBSIDIAN_DOOR = regGlowingObsidianDoor(GlowingObsidianColor.GOLD);
    public static final RegisteredBlock SILVER_GLOWING_OBSIDIAN_DOOR = regGlowingObsidianDoor(GlowingObsidianColor.SILVER);
    public static final RegisteredBlock IRON_GLOWING_OBSIDIAN_DOOR = regGlowingObsidianDoor(GlowingObsidianColor.IRON);
    public static final RegisteredBlock COPPER_GLOWING_OBSIDIAN_DOOR = regGlowingObsidianDoor(GlowingObsidianColor.COPPER);

    public static void init() { }

    private static RegisteredBlock regGlowingObsidianDoor(GlowingObsidianColor color) {
        var glowingObsidian = Building.GLOWING_OBSIDIAN.get(color);
        return regDoor(color.getName() + "_glowing_obsidian", BlockType.STRONG_STONE, 7, true, glowingObsidian)
            .localizedName(
                color.localizedName(false) + " Glowing Obsidian Door",
                "Puerta de obsidiana brillante " + color.localizedName(true)
            );
    }

    private static RegisteredBlock regDoor(String name, BlockType type, int lightLevel, boolean fancy, ItemLike craftItem) {
        var blockSetType = switch(type) {
            case WEAK_METAL -> PMBlockSetTypes.WEAK_METAL;
            case METAL -> PMBlockSetTypes.METAL;
            case GLASS -> PMBlockSetTypes.GLASS;
            case WOOD -> PMBlockSetTypes.VARIANT_WOOD;
            default -> PMBlockSetTypes.VARIANT_STONE;
        };

        ArrayList<TagKey<Block>> tags = new ArrayList<>();

        if(type == BlockType.WOOD) tags.add(BlockTags.WOODEN_DOORS);
        else {
            tags.add(BlockTags.DOORS);
            tags.addAll(type.tags());
        }

        var doorBlock = PMRegistries.regBlockItem(
            name + "_door",
            () -> new DoorBlock(
                type.getProperties()
                    .lightLevel(s -> lightLevel)
                    .noOcclusion(),

                blockSetType
            )
        )
            .recipe(
                (item, generator) -> {
                    var recipe = generator.getShapedBuilder(RecipeCategory.REDSTONE, item, 3)
                        .pattern("##")
                        .pattern("##")
                        .pattern("##")
                        .define('#', craftItem);

                    if(type.isMetal())
                        recipe = recipe.group("metal_doors");

                    return recipe;
                }
            )
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .tags(tags)
            .itemModel((door, generator) -> generator.basicItem(door.asItem(), "door/" + name, type == BlockType.GLASS ? "translucent" : "cutout"))
            .blockStateGenerator((door, generator) -> genDoorBlockState(name, type, fancy, door, generator))
            .lootTable((block, generator) -> generator.doorDrop(block))
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Door",
                "Puerta de " + Utils.localizedMaterialName(name, true)
            );

        if(type.isStone())
            doorBlock = doorBlock.recipe((item, generator) -> generator.stonecutterRecipe(craftItem, item));

        return doorBlock;
    }

    private static void genDoorBlockState(String name, BlockType type, boolean fancy, RegisteredBlock door, BlockStateGenerator generator) {
        var renderType = type == BlockType.GLASS ? "cutout" : "solid";
        var topTexture = "paradisemod:block/door/" + name + "_top";
        var bottomTexture = "paradisemod:block/door/" + name + "_bottom";

        if(name.endsWith("_glowing_obsidian")) {
            var color = name.substring(0, name.length() - 17);
            topTexture = "paradisemod:block/glowing_obsidian/" + color;
            bottomTexture = topTexture;
        }
        else if(type.isStone()) {
            topTexture = "minecraft:block/" + name;
            bottomTexture = topTexture;
        }
        else if(name == "emerald") {
            topTexture = "minecraft:block/emerald_block";
            bottomTexture = topTexture;
        }
        else if(name == "ruby") {
            topTexture = "paradisemod:block/ruby_block";
            bottomTexture = topTexture;
        }

        var modelBuilder = generator.models();

        var bottom = genDoorModel(
            name,
            topTexture,
            bottomTexture,
            "bottom",
            renderType,
            fancy,
            generator,
            modelBuilder::doorBottomLeft
        );

        var bottomHinge = genDoorModel(
            name,
            topTexture,
            bottomTexture,
            "bottom_hinge",
            renderType,
            fancy,
            generator,
            modelBuilder::doorBottomRight
        );

        if(name == "glass")
            bottomHinge = genDoorModel(
                name,
                topTexture,
                bottomTexture,
                "bottom_hinge",
                renderType,
                fancy,
                generator,
                modelBuilder::doorBottomLeft
            );

        var top = genDoorModel(
            name,
            topTexture,
            bottomTexture,
            "top",
            renderType,
            fancy,
            generator,
            modelBuilder::doorTopLeft
        );

        var topHinge = genDoorModel(
            name,
            topTexture,
            bottomTexture,
            "top_hinge",
            renderType,
            fancy,
            generator,
            modelBuilder::doorTopRight
        );

        if(name == "glass")
            topHinge = genDoorModel(
                name,
                topTexture + "_left",
                bottomTexture,
                "top_hinge",
                renderType,
                fancy,
                generator,
                modelBuilder::doorTopLeft
            );

        generator.doorBlock((DoorBlock) door.get(), bottom, bottomHinge, bottomHinge, bottom, top, topHinge, topHinge, top);
    }

    public static ModelFile genDoorModel(
        String name,
        String topTexture,
        String bottomTexture,
        String suffix,
        String renderType,
        boolean fancy,
        BlockStateGenerator generator,
        DoorModelGenerator modelGenerator
    ) {
        var modelBuilder = generator.models();
        var modelName = "door/" + name + "_" + suffix;

        if(fancy) {
            try {
                return generator.existingModel(modelName);
            }
            catch(Exception e) {
                var model = modelBuilder.withExistingParent("block/" + modelName, generator.modLoc("block/template/custom_door_" + suffix))
                    .texture("top", new ResourceLocation(topTexture))
                    .texture("bottom", new ResourceLocation(bottomTexture))
                    .renderType(new ResourceLocation(renderType));

                if(renderType != "solid")
                    model = model.renderType(renderType);

                return model;
            }
        }
        else {
            var model = modelGenerator.generate("block/" + modelName, new ResourceLocation(bottomTexture), new ResourceLocation(topTexture));

            if(renderType != "solid")
                model = model.renderType(renderType);

            return model;
        }
    }

    @FunctionalInterface
    public interface DoorModelGenerator {
        BlockModelBuilder generate(String name, ResourceLocation bottomTexture, ResourceLocation topTexture);
    }
}