package net.paradisemod.building;

import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.WallBlock;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.world.DeepDarkBlocks;

public class Walls {
    // wall blocks (thank God they're much easier to create now!)
    public static final RegisteredBlock BEDROCK_WALL = regWall("bedrock", BlockType.INDESTRUCTIBLE, Blocks.BEDROCK);
    public static final RegisteredBlock POLISHED_END_STONE_WALL = regWall("polished_end_stone", BlockType.ENHANCED_STONE, Building.POLISHED_END_STONE, Blocks.END_STONE);
    public static final RegisteredBlock END_STONE_WALL = regWall("end_stone", BlockType.ENHANCED_STONE, Blocks.END_STONE);
    public static final RegisteredBlock DRIPSTONE_WALL = regWall("dripstone", SoundType.DRIPSTONE_BLOCK, Blocks.DRIPSTONE_BLOCK);
    public static final RegisteredBlock CALCITE_WALL = regWall("calcite", SoundType.CALCITE, Blocks.CALCITE);
    public static final RegisteredBlock TUFF_WALL = regWall("tuff", SoundType.TUFF, Blocks.TUFF);
    public static final RegisteredBlock OBSIDIAN_WALL = regWall("obsidian", BlockType.STRONG_STONE, Blocks.OBSIDIAN);

    public static final RegisteredBlock DARKSTONE_WALL = regWall("darkstone", BlockType.ENHANCED_STONE, DeepDarkBlocks.DARKSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock POLISHED_DARKSTONE_WALL = regWall("polished_darkstone", BlockType.ENHANCED_STONE, DeepDarkBlocks.POLISHED_DARKSTONE, DeepDarkBlocks.DARKSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock POLISHED_DRIPSTONE_WALL = regWall("polished_dripstone", SoundType.DRIPSTONE_BLOCK, Building.POLISHED_DRIPSTONE, Blocks.DRIPSTONE_BLOCK);
    public static final RegisteredBlock POLISHED_CALCITE_WALL = regWall("polished_calcite", SoundType.CALCITE, Building.POLISHED_CALCITE, Blocks.CALCITE);
    public static final RegisteredBlock POLISHED_TUFF_WALL = regWall("polished_tuff", SoundType.TUFF, Building.POLISHED_TUFF, Blocks.TUFF);

    public static final RegisteredBlock POLISHED_DARKSTONE_BRICKS_WALL = regWall("polished_darkstone_bricks", BlockType.ENHANCED_STONE, DeepDarkBlocks.POLISHED_DARKSTONE_BRICKS, DeepDarkBlocks.POLISHED_DARKSTONE, DeepDarkBlocks.DARKSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock POLISHED_DRIPSTONE_BRICKS_WALL = regWall("polished_dripstone_bricks", SoundType.DRIPSTONE_BLOCK, Building.POLISHED_DRIPSTONE_BRICKS, Building.POLISHED_DRIPSTONE, Blocks.DRIPSTONE_BLOCK);
    public static final RegisteredBlock POLISHED_CALCITE_BRICKS_WALL = regWall("polished_calcite_bricks", SoundType.CALCITE, Building.POLISHED_CALCITE_BRICKS, Building.POLISHED_CALCITE, Blocks.CALCITE);
    public static final RegisteredBlock POLISHED_TUFF_BRICKS_WALL = regWall("polished_tuff_bricks", SoundType.TUFF, Building.POLISHED_TUFF_BRICKS, Building.POLISHED_TUFF, Blocks.TUFF);
    public static final RegisteredBlock POLISHED_ASPHALT_WALL = regWall("polished_asphalt", BlockType.STONE, Building.POLISHED_ASPHALT);
    public static final RegisteredBlock POLISHED_ASPHALT_BRICKS_WALL = regWall("polished_asphalt_bricks", BlockType.STONE, Building.POLISHED_ASPHALT_BRICKS, Building.POLISHED_ASPHALT);

    public static final RegisteredBlock BLACKENED_SANDSTONE_WALL = regWall("blackened_sandstone", BlockType.STONE, DeepDarkBlocks.BLACKENED_SANDSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACK_GLOWING_OBSIDIAN_WALL = regGlowingObsidianWall(GlowingObsidianColor.BLACK);
    public static final RegisteredBlock BLUE_GLOWING_OBSIDIAN_WALL = regGlowingObsidianWall(GlowingObsidianColor.BLUE);
    public static final RegisteredBlock GREEN_GLOWING_OBSIDIAN_WALL = regGlowingObsidianWall(GlowingObsidianColor.GREEN);
    public static final RegisteredBlock INDIGO_GLOWING_OBSIDIAN_WALL = regGlowingObsidianWall(GlowingObsidianColor.INDIGO);
    public static final RegisteredBlock ORANGE_GLOWING_OBSIDIAN_WALL = regGlowingObsidianWall(GlowingObsidianColor.ORANGE);
    public static final RegisteredBlock RED_GLOWING_OBSIDIAN_WALL = regGlowingObsidianWall(GlowingObsidianColor.RED);
    public static final RegisteredBlock VIOLET_GLOWING_OBSIDIAN_WALL = regGlowingObsidianWall(GlowingObsidianColor.VIOLET);
    public static final RegisteredBlock WHITE_GLOWING_OBSIDIAN_WALL = regGlowingObsidianWall(GlowingObsidianColor.WHITE);
    public static final RegisteredBlock YELLOW_GLOWING_OBSIDIAN_WALL = regGlowingObsidianWall(GlowingObsidianColor.YELLOW);
    public static final RegisteredBlock GOLD_GLOWING_OBSIDIAN_WALL = regGlowingObsidianWall(GlowingObsidianColor.GOLD);
    public static final RegisteredBlock SILVER_GLOWING_OBSIDIAN_WALL = regGlowingObsidianWall(GlowingObsidianColor.SILVER);
    public static final RegisteredBlock IRON_GLOWING_OBSIDIAN_WALL = regGlowingObsidianWall(GlowingObsidianColor.IRON);
    public static final RegisteredBlock COPPER_GLOWING_OBSIDIAN_WALL = regGlowingObsidianWall(GlowingObsidianColor.COPPER);

    public static void init() { }

    private static RegisteredBlock regGlowingObsidianWall(GlowingObsidianColor color) {
        var glowingObsidian = Building.GLOWING_OBSIDIAN.get(color);
        var texture = "paradisemod:block/glowing_obsidian/" + color.getName();
        var name = color.getName() + "_glowing_obsidian";

        var wall = regWall(
            name,
            texture,
            BlockType.STRONG_STONE
                .getProperties()
                .noOcclusion()
                .lightLevel(s -> 7),
            glowingObsidian
        )
            .tag(PMTags.Blocks.GLOWING_OBSIDIAN_WALLS)
            .localizedName(
                color.localizedName(false) + " Glowing Obsidian Wall",
                "Pared de obsidiana brillante " + color.localizedName(true)
            );

        return wall;
    }

    private static RegisteredBlock regWall(String name, SoundType sound, ItemLike craftItem, ItemLike... stonecuttingBlocks) {
        var texture = texture(name);

        return regWall(name, texture, BlockType.STONE.getProperties().noOcclusion().sound(sound), craftItem, stonecuttingBlocks)
            .tags(BlockType.STONE.tags())
            .tag(BlockTags.WALLS)
            .stonecutterRecipes(stonecuttingBlocks)
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.BUILDING_BLOCKS, item, 6)
                    .pattern("BBB")
                    .pattern("BBB")
                    .define('B', craftItem)
            );
    }

    private static RegisteredBlock regWall(String name, BlockType type, ItemLike craftItem, ItemLike... stonecuttingBlocks) {
        var texture = texture(name);

        return regWall(name, texture, type.getProperties().noOcclusion(), craftItem, stonecuttingBlocks)
            .tags(type.tags())
            .tag(BlockTags.WALLS)
            .stonecutterRecipes(stonecuttingBlocks)
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.BUILDING_BLOCKS, item, 6)
                    .pattern("BBB")
                    .pattern("BBB")
                    .define('B', craftItem)
            );
    }

    private static RegisteredBlock regWall(String name, String texture, Block.Properties properties, ItemLike craftItem, ItemLike... stonecuttingBlocks) {
        return PMRegistries.regBlockItem(
            name + "_wall",
            () -> new WallBlock(properties)
        )
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .itemModel((block, generator) -> generator.wallInventory(name + "_wall", new ResourceLocation(texture)))
            .blockStateGenerator((block, generator) -> genWallBlockState(name, texture, block, generator))
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Wall",
                "Pared de " + Utils.localizedMaterialName(name, true)
            );
    }

    private static void genWallBlockState(String name, String texture, RegisteredBlock wallBlock, BlockStateGenerator generator) {
        var modelBuilder = generator.models();
        var textureLoc = new ResourceLocation(texture);
        generator.wallBlock(
            (WallBlock) wallBlock.get(),
            modelBuilder.wallPost("block/wall/" + name + "_post", textureLoc),
            modelBuilder.wallSide("block/wall/" + name + "_side", textureLoc),
            modelBuilder.wallSideTall("block/wall/" + name + "_side_tall", textureLoc)
        );
    }

    private static String texture(String name) {
        return switch(name) {
            case "bedrock", "end_stone", "calcite", "tuff", "obsidian" -> "minecraft:block/" + name;
            case "dripstone" -> "minecraft:block/dripstone_block";
            default -> "paradisemod:block/" + name;
        };
    }
}