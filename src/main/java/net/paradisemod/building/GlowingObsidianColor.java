package net.paradisemod.building;

public enum GlowingObsidianColor {
    BLACK("Black", "negra"),
    BLUE("Blue", "azul"),
    GREEN("Green", "verde"),
    INDIGO("Indigo", "índigo"),
    ORANGE("Orange", "naranja"),
    RED("Red", "roja"),
    VIOLET("Violet", "violeta"),
    WHITE("White", "blanca"),
    YELLOW("Yellow", "amarilla"),
    GOLD("Golden", "dorada"),
    SILVER("Silver", "plateada"),
    IRON("Iron", "de hierro"),
    COPPER("Copper", "de cobre");

    private String englishName;
    private String spanishName;

    public String localizedName(boolean spanish) {
        return spanish ? spanishName : englishName;
    }

    GlowingObsidianColor(String englishName, String spanishName) {
        this.englishName = englishName;
        this.spanishName = spanishName;
    }

    public String getName() { return name().toLowerCase(); }
}