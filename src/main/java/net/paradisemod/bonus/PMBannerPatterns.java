package net.paradisemod.bonus;

import java.util.HashMap;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.BannerPatternItem;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BannerPattern;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.assets.PMTranslations;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredItem;

public class PMBannerPatterns {
    private static final DeferredRegister<BannerPattern> PATTERNS = PMRegistries.createRegistry(Registries.BANNER_PATTERN);
    public static final HashMap<TagKey<BannerPattern>, RegistryObject<BannerPattern>> REGISTERED_BANNERS = new HashMap<>();
    public static final RegistryObject<BannerPattern> ZOMBIE_CHARGE = createPattern("zombie_charge", "zc");
    public static final RegistryObject<BannerPattern> DAVID_STAR = createPattern("david_star", "ds");
    public static final RegistryObject<BannerPattern> ICTUS = createPattern("ictus", "ict");
    public static final RegistryObject<BannerPattern> TRIQUETRA = createPattern("triquetra", "tri");

    public static final RegisteredItem ZOMBIE_CHARGE_PATTERN = createPatternItem("zombie_charge")
        .recipe(
            (item, generator) -> generator.shapelessRecipe(
                RecipeCategory.MISC,
                item,
                Blocks.ZOMBIE_HEAD,
                Items.PAPER
            )
        );

    public static final RegisteredItem ICTUS_PATTERN = createPatternItem("ictus")
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item)
                .pattern(" g ")
                .pattern("gpg")
                .pattern(" g ")
                .define('g', Items.GOLD_INGOT)
                .define('p', Items.PAPER)
        );

    // TODO: when the red dragon is defeated, its head can be used to craft this banner pattern
    public static final RegisteredItem DAVID_STAR_PATTERN = createPatternItem("david_star");
    public static final RegisteredItem TRIQUETRA_PATTERN = createPatternItem("triquetra");

    public static void init(IEventBus eventBus) { PATTERNS.register(eventBus); }

    public static void translations(PMTranslations translator) {
        translator.addBanner(ZOMBIE_CHARGE, "Zombie Charge", "Cargo de Zombi");
        translator.addBanner(DAVID_STAR, "Star of David", "Estrella de David");
        translator.addBanner(ICTUS, "Ictus", "Ictus");
        translator.addBanner(TRIQUETRA, "Triquetra", "Triquetra");
    }

    private static RegistryObject<BannerPattern> createPattern(String name, String hash) {
        var bannerKey = TagKey.create(Registries.BANNER_PATTERN, new ResourceLocation(ParadiseMod.ID, name));

        var regPattern = Utils.handlePossibleException(
            () -> PATTERNS.register(name, () -> new BannerPattern(hash))
        );

        REGISTERED_BANNERS.put(bannerKey, regPattern);
        return regPattern;
    }

    private static RegisteredItem createPatternItem(String name) {
        var bannerKey = TagKey.create(Registries.BANNER_PATTERN, new ResourceLocation(ParadiseMod.ID, name));
        return PMRegistries.regItem(name + "_pattern", () -> new BannerPatternItem(bannerKey, new Item.Properties()))
            .model((item, generator) -> generator.basicItem(item.get(), generator.mcLoc("item/skull_banner_pattern")))
            .localizedName("Banner Pattern", "Diseño de estandarte")
            .tab(CreativeModeTabs.INGREDIENTS);
    }
}
