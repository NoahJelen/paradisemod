package net.paradisemod.bonus;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.animal.Cat;
import net.minecraft.world.entity.animal.Fox;
import net.minecraft.world.entity.animal.Ocelot;
import net.minecraft.world.entity.animal.Rabbit;
import net.minecraft.world.entity.animal.Wolf;
import net.minecraft.world.entity.npc.Villager;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.BushBlock;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.entries.TagEntry;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;
import net.minecraftforge.client.model.generators.ModelFile.UncheckedModelFile;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.event.entity.living.MobSpawnEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModList;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.base.registry.RegisteredCreativeTab;
import net.paradisemod.base.registry.RegisteredItem;
import net.paradisemod.bonus.blocks.HatBlock;
import net.paradisemod.bonus.xmas.ChristmasSapling;
import net.paradisemod.building.Building;

public class Bonus {
    public static ArmorMaterial SANTA_MAT = new ArmorMaterial() {
        @Override
        public int getDurabilityForType(ArmorItem.Type armorType) {
            return 2;
        };

        @Override
        public int getDefenseForType(ArmorItem.Type armorType) {
            return 0;
        };

        @Override
        public int getEnchantmentValue() { return 0; }

        @Override
        public SoundEvent getEquipSound() { return SoundEvents.ARMOR_EQUIP_LEATHER; }

        @Override
        public Ingredient getRepairIngredient() { return null; }

        @Override
        public String getName() { return ParadiseMod.ID + ":santa"; }

        @Override
        public float getToughness() { return 0; }

        @Override
        public float getKnockbackResistance() { return 0; }
    };

    public static final RegisteredCreativeTab CHRISTMAS_TAB = new RegisteredCreativeTab(
        "christmas",
        () -> Bonus.CHRISTMAS_TREE_SAPLING,
        "Christmas Decorations",
        "Decoraciones navideñas"
    );

    public static final RegisteredBlock CHRISTMAS_TREE_SAPLING = PMRegistries.regBlockItem(
        "christmas_tree_sapling",
        ChristmasSapling::new
    )
        .tag(BlockTags.SAPLINGS)
        .tab(CHRISTMAS_TAB)
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item.asItem())
                .pattern(" R ")
                .pattern("Y#G")
                .pattern(" B ")
                .define('#', Blocks.SPRUCE_SAPLING)
                .define('R', Building.RED_GLOWING_OBSIDIAN)
                .define('G', Building.GREEN_GLOWING_OBSIDIAN)
                .define('B', Building.BLUE_GLOWING_OBSIDIAN)
                .define('Y', Building.YELLOW_GLOWING_OBSIDIAN)
        )
        .localizedName("Christmas Tree Sapling", "Retoño de árbol navideño");

    public static final RegisteredBlock CHRISTMAS_LEAVES = PMRegistries.regBlockItem(
        "christmas_tree_leaves",
        () -> new LeavesBlock(Block.Properties.copy(Blocks.OAK_LEAVES).lightLevel(s -> 15))
    )
        .tab(CHRISTMAS_TAB)
        .tag(BlockTags.LEAVES)
        .renderType("cutout")
        .lootTable((block, generator) -> generator.leavesDrop(block, CHRISTMAS_TREE_SAPLING.get()))
        .localizedName("Christmas Tree Leaves", "Hojas de árbol navideño");

    public static final RegisteredBlock PRESENT = PMRegistries.regBlockItem(
        "present",
        () -> new Block(Block.Properties.copy(Blocks.OAK_LEAVES))
    )
        .tab(CHRISTMAS_TAB)
        .blockStateGenerator(
            (block, generator) -> {
                var model = generator.models()
                    .getBuilder("present")
                    .parent(new UncheckedModelFile("block/cube"))
                    .texture("up", "paradisemod:block/present_top")
                    .texture("down", "paradisemod:block/present_bottom")
                    .texture("east", "paradisemod:block/present_side2")
                    .texture("west", "paradisemod:block/present_side2")
                    .texture("north", "paradisemod:block/present_side")
                    .texture("south", "paradisemod:block/present_side")
                    .texture("particle", "paradisemod:block/present_side");

                generator.simpleBlock(block, model);
            }
        )
        .lootTable(
            (block, generator) -> generator.add(
                block,
                blockIn -> LootTable.lootTable()
                    .withPool(
                        LootPool.lootPool()
                            .setRolls(ConstantValue.exactly(1))
                            .setBonusRolls(UniformGenerator.between(0, 3))
                            .add(TagEntry.expandTag(PMTags.Items.PRESENTS))
                    )
            )
        )
        .localizedName("Present", "Regalo");

    public static final RegisteredBlock GOLD_CHRISTMAS_TREE_TOP = createChristmasTop("gold");
    public static final RegisteredBlock SILVER_CHRISTMAS_TREE_TOP = createChristmasTop("silver");
    public static final RegisteredBlock COPPER_CHRISTMAS_TREE_TOP = createChristmasTop("copper");

    public static final RegisteredBlock SANTA_HAT = PMRegistries.regBlockItem("santa_hat", HatBlock::new)
        .tab(CHRISTMAS_TAB)
        .blockStateGenerator(
            (block, generator) -> {
                generator.getVariantBuilder(block.get())
                    .forAllStates(
                        state -> {
                            Direction facing = state.getValue(HorizontalDirectionalBlock.FACING);
                            var model = generator.existingModel("santa_hat");
                            return generator.buildVariantModel(model, facing, true);
                        }
                    );
            }
        )
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item)
                .pattern(" w ")
                .pattern(" r ")
                .pattern("rrr")
                .define('r', Blocks.RED_WOOL)
                .define('w', Blocks.WHITE_WOOL)
        )
        .localizedName("Santa Hat", "Gorra de Papá Noel");

    public static final RegisteredItem SANTA_JACKET = PMRegistries.regItem(
        "santa_jacket",
        () -> new ArmorItem(SANTA_MAT, ArmorItem.Type.CHESTPLATE, new Item.Properties())
    )
        .tab(CHRISTMAS_TAB)
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item)
                .pattern("r r")
                .pattern("rwr")
                .pattern("rwr")
                .define('r', Blocks.RED_WOOL)
                .define('w', Blocks.WHITE_WOOL)
        )
        .localizedName("Santa Jacket", "Chaqueta de Papá Noel");

    public static final RegisteredItem SANTA_PANTS = PMRegistries.regItem(
        "santa_pants",
        () -> new ArmorItem(SANTA_MAT, ArmorItem.Type.LEGGINGS, new Item.Properties())
    )
        .tab(CHRISTMAS_TAB)
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item)
                .pattern("rrr")
                .pattern("r r")
                .pattern("r r")
                .define('r', Blocks.RED_WOOL)
        )
        .localizedName("Santa Pants", "Pantalones de Papá Noel");

    public static final RegisteredItem SANTA_BOOTS = PMRegistries.regItem(
        "santa_boots",
        () -> new ArmorItem(SANTA_MAT, ArmorItem.Type.BOOTS, new Item.Properties())
    )
        .tab(CHRISTMAS_TAB)
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.MISC, item)
                .pattern("l l")
                .pattern("r r")
                .define('l', Items.LEATHER)
                .define('r', Blocks.RED_WOOL)
        )
        .localizedName("Santa Boots", "Botas de Papá Noel");

    public static final RegisteredBlock CLASSIC_STONECUTTER = PMRegistries.regBlockItem(
        "classic_stonecutter",
        () -> new Block(Block.Properties.copy(Blocks.STONE))
    )
        .tab(CreativeModeTabs.BUILDING_BLOCKS)
        .blockStateGenerator(
            (block, generator) -> {
                var model = generator.models()
                    .getBuilder("classic_stonecutter")
                    .parent(new UncheckedModelFile("block/cube"))
                    .texture("particle", "paradisemod:block/classic_stonecutter_front")
                    .texture("north", "paradisemod:block/classic_stonecutter_front")
                    .texture("south", "paradisemod:block/classic_stonecutter_side")
                    .texture("east", "paradisemod:block/classic_stonecutter_side")
                    .texture("west", "paradisemod:block/classic_stonecutter_front")
                    .texture("up", "paradisemod:block/classic_stonecutter_top")
                    .texture("down", "paradisemod:block/classic_stonecutter_bottom");

                generator.simpleBlock(block, model);
            }
        )
        .localizedName("Classic Stonecutter", "Cortapiedras clásico");

    public static final RegisteredItem BLUE_FACE_DIAPER = PMRegistries.regItem("blue_face_diaper", () -> new Item(new Item.Properties()))
        .modelAlreadyExists()
        .localizedName("Face Diaper", "Pañal para cara");

    public static final RegisteredItem BLACK_FACE_DIAPER = PMRegistries.regItem("black_face_diaper", () -> new Item(new Item.Properties()))
        .modelAlreadyExists()
        .localizedName("Face Diaper", "Pañal para cara");

    public static void init(IEventBus eventbus) {
        PMBannerPatterns.init(eventbus);
        MinecraftForge.EVENT_BUS.register(BonusEvents.class);
        ParadiseMod.LOG.info("Loaded Bonus module");
    }

    private static RegisteredBlock createChristmasTop(String metalName) {
        var name = metalName + "_christmas_tree_top";

        String texture = switch(metalName) {
            case "gold" -> "minecraft:block/gold_block";
            case "silver" -> "paradisemod:block/silver_block";
            case "copper" -> "minecraft:block/copper_block";
            default -> Utils.modErrorTyped(metalName + " is not implemented for the christmas tree top!");
        };

        String englishName = switch(metalName) {
            case "gold" -> "Golden Christmas Tree Top";
            case "silver" -> "Silver Christmas Tree Top";
            case "copper" -> "Copper Christmas Tree Top";
            default -> Utils.modErrorTyped(metalName + " is not implemented for the christmas tree top!");
        };

        String spanishName = switch(metalName) {
            case "gold" -> "Cima dorada de árbol navideño";
            case "silver" -> "Cima plateada de árbol navideño";
            case "copper" -> "Cima de árbol navideño de cobre";
            default -> Utils.modErrorTyped(metalName + " is not implemented for the christmas tree top!");
        };

        return PMRegistries.regBlockItem(
            name,
            () -> new BushBlock(Block.Properties.copy(Blocks.OAK_LEAVES).sound(SoundType.METAL).noCollission()) {
                @Override
                public boolean mayPlaceOn(BlockState state, BlockGetter world, BlockPos pos) { return (state.is(CHRISTMAS_LEAVES.get()) || state.is(Blocks.SPRUCE_LOG)); }
            }
        )
            .tab(CHRISTMAS_TAB)
            .blockStateGenerator(
                (block, generator) -> {
                    var model = generator.models().withExistingParent(name, "paradisemod:block/template/christmas_tree_top")
                        .texture("texture", texture);

                    generator.simpleBlock(block, model);
                }
            )
            .localizedName(englishName, spanishName);
    }

    public static class BonusEvents {
        @SubscribeEvent
        public static void maskedVillager(EntityEvent event) {
            if(event.getEntity() instanceof Villager villager && !ModList.get().isLoaded("customnpcs"))
                if(villager.getDisplayName().getString().equalsIgnoreCase("doomer")) {
                    var headItem = villager.getItemBySlot(EquipmentSlot.HEAD);

                    if(!headItem.is(BLUE_FACE_DIAPER.get()) && !headItem.is(BLACK_FACE_DIAPER.get())) {
                        var villagerWorld = villager.level(); 
                        villager.setItemSlot(EquipmentSlot.HEAD, new ItemStack(() -> (villagerWorld.random.nextBoolean() ? BLUE_FACE_DIAPER : BLACK_FACE_DIAPER).get()));
                        villager.goalSelector.addGoal(2, new AvoidEntityGoal<>(villager, Villager.class, 2, 1, 2));
                        villager.goalSelector.addGoal(2, new AvoidEntityGoal<>(villager, Player.class, 2, 1, 2));
                    }
                }
        }

        // Playboy, OnlyFans, and PornHub are a disgrace to society!
        @SubscribeEvent
        public static void killPlayboy(MobSpawnEvent.FinalizeSpawn event) {
            if(event.getEntity() instanceof Wolf wolf)
                wolf.targetSelector.addGoal(9, new NearestAttackableTargetGoal<>(wolf, Rabbit.class, false, rabbit -> rabbit.getDisplayName().getString().equalsIgnoreCase("playboy")));
            else if(event.getEntity() instanceof Fox fox)
                fox.targetSelector.addGoal(4, new NearestAttackableTargetGoal<>(fox, Rabbit.class, false, rabbit -> rabbit.getDisplayName().getString().equalsIgnoreCase("playboy")));
            else if(event.getEntity() instanceof Ocelot ocelot)
                ocelot.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(ocelot, Rabbit.class, false, rabbit -> rabbit.getDisplayName().getString().equalsIgnoreCase("playboy")));
            else if(event.getEntity() instanceof Cat cat)
                cat.targetSelector.addGoal(4, new NearestAttackableTargetGoal<>(cat, Rabbit.class, false, rabbit -> rabbit.getDisplayName().getString().equalsIgnoreCase("playboy")));
        }
    }
}