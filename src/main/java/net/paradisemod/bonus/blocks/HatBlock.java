package net.paradisemod.bonus.blocks;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.Equipable;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.CarvedPumpkinBlock;

public class HatBlock extends CarvedPumpkinBlock implements Equipable {
    public HatBlock() {
        super(Block.Properties.copy(Blocks.WHITE_WOOL).noOcclusion());
    }

    @Override
    public EquipmentSlot getEquipmentSlot() {
        return EquipmentSlot.HEAD;
    }
}