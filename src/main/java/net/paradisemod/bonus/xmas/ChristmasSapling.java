package net.paradisemod.bonus.xmas;

import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SaplingBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.grower.AbstractTreeGrower;
import net.minecraft.world.level.block.state.BlockState;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;
import net.paradisemod.bonus.Bonus;
import net.paradisemod.world.PMWorld;

public class ChristmasSapling extends SaplingBlock implements ModeledBlock {
    private static final AbstractTreeGrower tree = new ChristmasTree();

    public ChristmasSapling() {
        super(tree, Block.Properties.copy(Blocks.OAK_SAPLING).noCollission().randomTicks().instabreak().sound(SoundType.GRASS).lightLevel(s -> 15));
    }

    @Override
    public void advanceTree(ServerLevel world, BlockPos pos, BlockState state, RandomSource rand) {
        if (state.getValue(STAGE) == 0) world.setBlock(pos, state.cycle(STAGE), 4);
        else {
            var grown = tree.growTree(world, world.getChunkSource().getGenerator(), pos, state, rand);

            if(grown) {
                var presentRolls = 3 + rand.nextInt(3);
                for(int i = 0; i <= presentRolls; i++) {
                    var newPos = pos.offset(rand.nextInt(-3, 3), 0, rand.nextInt(-3, 3));
                    var y = PMWorld.getGroundLevel(world, -54, newPos.getY() + 12, newPos);

                    if (
                        y.isEmpty() ||
                        (newPos.getX() == pos.getX() && newPos.getZ() == pos.getZ())
                    ) continue;

                    world.setBlockAndUpdate(newPos.atY(y.getAsInt() + 1), Bonus.PRESENT.get().defaultBlockState());
                }

                var topY = PMWorld.getGroundLevel(world, 0, pos.getY() + 12, pos, Bonus.CHRISTMAS_LEAVES.get());
                if(topY.isPresent()) {
                    var tops = List.of(Bonus.GOLD_CHRISTMAS_TREE_TOP.get(), Bonus.SILVER_CHRISTMAS_TREE_TOP.get(), Bonus.COPPER_CHRISTMAS_TREE_TOP.get());
                    world.setBlockAndUpdate(pos.atY(topY.getAsInt() + 1), tops.get(rand.nextInt(tops.size())).defaultBlockState());
                }
            }
        }
    }

    @Override
    public void genBlockState(BlockStateGenerator generator) {
        generator.genSimplePlant(this);
    }
}