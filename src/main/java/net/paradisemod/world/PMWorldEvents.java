package net.paradisemod.world;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.event.entity.player.BonemealEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.tags.PMTags;

public class PMWorldEvents {
    @SubscribeEvent
    public static void growRoses(BonemealEvent event) {
        var world = event.getLevel();
        var pos = event.getPos();
        var state = world.getBlockState(pos);

        if(!state.is(Blocks.GRASS_BLOCK)) return;
        var x = pos.getX() - 7;
        var y = pos.getY() - 7;
        var z = pos.getZ() - 7;
        if (y < 0) y = 0;

        var roses = Utils.getBlockTag(PMTags.Blocks.ROSES);
        for (int bx = x; bx < x + 14; bx++)
            for (int bz = z; bz < z + 14; bz++)
                for (int by = y; by < y + 14; by++)
                    if (world.getBlockState(new BlockPos(bx, by, bz)).is(Blocks.GRASS_BLOCK) && world.getBlockState(new BlockPos(bx, by + 1, bz)).isAir() && world.random.nextInt(16) == 0)
                        world.setBlockAndUpdate(new BlockPos(bx,by + 1, bz), roses.getRandomElement(world.random).get().defaultBlockState());
    }
}