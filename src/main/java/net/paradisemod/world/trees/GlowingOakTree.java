package net.paradisemod.world.trees;

import javax.annotation.Nullable;

import net.minecraft.resources.ResourceKey;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.block.grower.AbstractTreeGrower;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.paradisemod.worldgen.features.foliage.PMFoliage;

public class GlowingOakTree extends AbstractTreeGrower {
    @Nullable
    @Override
    protected ResourceKey<ConfiguredFeature<?, ?>> getConfiguredFeature(RandomSource rand, boolean largeHive) {
        return rand.nextBoolean() ? PMFoliage.GLOWING_OAK_TREE : PMFoliage.FANCY_GLOWING_OAK_TREE;
    }
}