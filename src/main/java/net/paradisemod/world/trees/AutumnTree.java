package net.paradisemod.world.trees;

import javax.annotation.Nullable;

import net.minecraft.resources.ResourceKey;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.grower.AbstractTreeGrower;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.worldgen.features.foliage.PMFoliage;

public class AutumnTree extends AbstractTreeGrower {
    public final Color color;

    // create an autumn tree of a specific color
    public AutumnTree(Color color) {
        this.color = color;
    }

    @Nullable
    @Override
    protected ResourceKey<ConfiguredFeature<?, ?>> getConfiguredFeature(RandomSource rand, boolean largeHive) {
        return largeHive ? PMFoliage.AUTUMN_SAPLING_TREES_BEES.get(color) : PMFoliage.AUTUMN_SAPLING_TREES.get(color);
    }

    public enum Color {
        BLUE(0x42339D),
        ORANGE(0xE8A749),
        RED(0xCD4B44),
        YELLOW(0xE6C85D);

        private final int leafColor;

        public int leafColor() {
            return leafColor;
        }

        private Color(int leafColor) {
            this.leafColor = leafColor;
        }

        public Block getLeaves() {
            return switch(this) {
                case BLUE -> Decoration.BLUE_AUTUMN_LEAVES.get();
                case ORANGE -> Decoration.ORANGE_AUTUMN_LEAVES.get();
                case RED -> Decoration.RED_AUTUMN_LEAVES.get();
                case YELLOW -> Decoration.YELLOW_AUTUMN_LEAVES.get();
            };
        }

        public String localizedSaplingColor(boolean spanish) {
            if(spanish)
                return switch(this) {
                    case BLUE -> "azul";
                    case ORANGE -> "anaranjado";
                    case RED -> "rojo";
                    case YELLOW -> "amarillo";
                };

            return switch(this) {
                case BLUE -> "Blue";
                case ORANGE -> "Orange";
                case RED -> "Red";
                case YELLOW -> "Yellow";
            };
        }

        public String localizedLeavesColor(boolean spanish) {
            if(spanish)
                return switch(this) {
                    case BLUE -> "azules";
                    case ORANGE -> "anaranjadas";
                    case RED -> "rojas";
                    case YELLOW -> "amarillas";
                };

            return localizedSaplingColor(false);
        }
    }

    public enum WoodType {
        OAK(Blocks.OAK_LOG),
        BIRCH(Blocks.BIRCH_LOG),
        SPRUCE(Blocks.SPRUCE_LOG);

        private final Block wood;

        WoodType(Block wood) { this.wood = wood; }

        public Block getWood() { return wood; }
    }
}