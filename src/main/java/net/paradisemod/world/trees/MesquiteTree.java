package net.paradisemod.world.trees;

import net.minecraft.core.BlockPos;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.grower.AbstractTreeGrower;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.paradisemod.worldgen.features.foliage.PMFoliage;

public class MesquiteTree extends AbstractTreeGrower {
    @Override
    protected ResourceKey<ConfiguredFeature<?, ?>> getConfiguredFeature(RandomSource rand, boolean largeHive) { return PMFoliage.MESQUITE_TREE; }

    @Override
    public boolean growTree(ServerLevel world, ChunkGenerator chunkGenerator, BlockPos pos, BlockState state, RandomSource rand) {
        var confFeatureRegistry = world.registryAccess().lookup(Registries.CONFIGURED_FEATURE).get();
        world.setBlock(pos, Blocks.AIR.defaultBlockState(), 4);
        if (confFeatureRegistry.getOrThrow(PMFoliage.MESQUITE_TREE).value().place(world, chunkGenerator, rand, pos)) return true;
        else {
            world.setBlock(pos, state, 4);
            return false;
        }
    }
}