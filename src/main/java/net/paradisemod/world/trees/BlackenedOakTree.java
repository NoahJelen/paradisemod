package net.paradisemod.world.trees;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.grower.AbstractMegaTreeGrower;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.paradisemod.worldgen.features.foliage.PMFoliage;

public class BlackenedOakTree extends AbstractMegaTreeGrower {
    @Override
    protected ResourceKey<ConfiguredFeature<?, ?>> getConfiguredMegaFeature(RandomSource rand) { return PMFoliage.BLACKENED_OAK_TREE; }

    @Nullable
    @Override
    protected ResourceKey<ConfiguredFeature<?, ?>> getConfiguredFeature(RandomSource rand, boolean largeHive) { return PMFoliage.BLACKENED_OAK_TREE; }

    @Override
    public boolean growTree(ServerLevel world, ChunkGenerator generator, BlockPos pos, BlockState state, RandomSource rand) {
        var confFeatureRegistry = world.registryAccess().lookup(Registries.CONFIGURED_FEATURE).get();
        world.setBlock(pos, Blocks.AIR.defaultBlockState(), 32);
        if(confFeatureRegistry.getOrThrow(PMFoliage.BLACKENED_OAK_TREE).value().place(world, generator, rand, pos)) return true;
        else world.setBlock(pos, state, 32);
        return false;
    }
}