package net.paradisemod.world.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.EndRodBlock;
import net.minecraft.world.level.block.SimpleWaterloggedBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class SmallCaveFormation extends EndRodBlock implements SimpleWaterloggedBlock {
    public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;
    private static final VoxelShape SHAPE_VERTICAL = Block.box(5, 0, 5, 11, 16, 11);
    private static final VoxelShape SHAPE_NS = Block.box(5, 5, 0, 11, 11, 16);
    private static final VoxelShape SHAPE_EW = Block.box(0, 5, 5, 16, 11, 11);

    public SmallCaveFormation(SoundType sound) { super(Block.Properties.copy(Blocks.REDSTONE_WIRE).sound(sound).noCollission()); }

    @Override
    public void animateTick(BlockState state, Level world, BlockPos pos, RandomSource rand) { }

    @Override
    public boolean canBeReplaced(BlockState state, BlockPlaceContext useContext) { return true; }

    @Override
    public boolean canSurvive(BlockState state, LevelReader world, BlockPos pos) {
        var direction = state.getValue(FACING);
        return isSupported(world, direction, pos);
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) {
        return switch (state.getValue(FACING).getAxis()) {
            default -> SHAPE_EW;
            case Y -> SHAPE_VERTICAL;
            case Z -> SHAPE_NS;
        };
    }

    @Override
    public void neighborChanged(BlockState state, Level world, BlockPos pos, Block block, BlockPos fromPos, boolean isMoving) {
        if (!isSupported(world, state.getValue(FACING), pos)) world.destroyBlock(pos, false);
    }

    protected static boolean isSupported(LevelReader world, Direction direction, BlockPos pos) {
        var support = getSupportPos(direction, pos);
        var state = world.getBlockState(support);
        return state.canOcclude() || state.is(Blocks.ICE) || state.is(Blocks.BLUE_ICE) || state.is(Blocks.PACKED_ICE) || state.is(Blocks.HONEY_BLOCK);
    }

    protected static boolean isSupported(Level world, Direction direction, BlockPos pos) {
        var support = getSupportPos(direction, pos);
        var state = world.getBlockState(support);
        return state.canOcclude() || state.is(Blocks.ICE) || state.is(Blocks.BLUE_ICE) || state.is(Blocks.PACKED_ICE);
    }

    private static BlockPos getSupportPos(Direction direction, BlockPos pos) {
        return switch (direction) {
            default -> pos.below();
            case DOWN -> pos.above();
            case EAST -> pos.west();
            case WEST -> pos.east();
            case NORTH -> pos.south();
            case SOUTH -> pos.north();
        };
    }

    @Override
    public boolean isPathfindable(BlockState state, BlockGetter world, BlockPos pos, PathComputationType type) { return true; }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        var fluidstate = context.getLevel().getFluidState(context.getClickedPos());
        var flag = fluidstate.getType() == Fluids.WATER;
        return super.getStateForPlacement(context).setValue(WATERLOGGED, flag);
    }

    @Override
    public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor world, BlockPos currentPos, BlockPos facingPos) {
        if (state.getValue(WATERLOGGED))
            world.scheduleTick(currentPos, Fluids.WATER, Fluids.WATER.getTickDelay(world));

        return super.updateShape(state, facing, facingState, world, currentPos, facingPos);
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) { builder.add(WATERLOGGED, FACING); }

    @Override
    public FluidState getFluidState(BlockState state) { return state.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(state); }
}