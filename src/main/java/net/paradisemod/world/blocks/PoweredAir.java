package net.paradisemod.world.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.AirBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.paradisemod.world.PMWorld;

public class PoweredAir extends AirBlock {
    public PoweredAir() { super(Block.Properties.copy(Blocks.AIR).lightLevel(s -> 7)); }

    @Override
    public boolean canSurvive(BlockState state, LevelReader world, BlockPos pos) {
        for(var dir : Direction.values())
            if(world.getBlockState(pos.relative(dir)).is(PMWorld.SHATTERED_REDSTONE_BLOCK.get()))
                return true;
        
        return false;
    }

    @Override
    public void neighborChanged(BlockState state, Level world, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) {
        for(var dir : Direction.values())
            if(world.getBlockState(pos.relative(dir)).is(PMWorld.SHATTERED_REDSTONE_BLOCK.get()))
                return;

        world.setBlockAndUpdate(pos, Blocks.AIR.defaultBlockState());
    }

    @Override
    public int getSignal(BlockState blockState, BlockGetter world, BlockPos pos, Direction side) { return 15; }
}