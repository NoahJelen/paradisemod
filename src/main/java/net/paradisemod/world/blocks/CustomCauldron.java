package net.paradisemod.world.blocks;

import java.util.Map;

import net.minecraft.core.BlockPos;
import net.minecraft.core.cauldron.CauldronInteraction;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.AbstractCauldronBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.client.extensions.common.IClientFluidTypeExtensions;
import net.minecraftforge.fluids.ForgeFlowingFluid;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.world.fluid.CustomFluidType;
import net.paradisemod.world.fluid.PMLiquidBlock;

public class CustomCauldron extends AbstractCauldronBlock implements ModeledBlock {
    private final RegisteredBlock fluidBlock;
    private final boolean translucent;

    public CustomCauldron(RegisteredBlock fluidBlock, Map<Item, CauldronInteraction> interactions, boolean translucent) {
        super(Block.Properties.copy(Blocks.CAULDRON).lightLevel(s -> getFluidLight(fluidBlock)), interactions);
        this.fluidBlock = fluidBlock;
        this.translucent = translucent;
    }

    @Override
    public boolean isFull(BlockState pState) {
        return true;
    }

    @Override
    public void genBlockState(BlockStateGenerator generator) {
        var fluidName = fluidBlock.shortName();
        var type = (CustomFluidType) getFluid().getFluidType();
        var content = type.cauldronTexture();

        var model = generator.models().withExistingParent(fluidName + "_cauldron", "block/template_cauldron_full")
            .texture("bottom", "minecraft:block/cauldron_bottom")
            .texture("inside", "minecraft:block/cauldron_inner")
            .texture("particle", "minecraft:block/cauldron_side")
            .texture("side", "minecraft:block/cauldron_side")
            .texture("top", "minecraft:block/cauldron_top")
            .texture("content", content);

        if(translucent) model = model.renderType("translucent");
        generator.simpleBlock(this, model);
    }

    @Override
    public int getAnalogOutputSignal(BlockState state, Level world, BlockPos pos) {
        return 3;
    }

    @Override
    public void entityInside(BlockState state, Level world, BlockPos pos, Entity entity) {
        if(isEntityInsideContent(state, pos, entity))
            fluidBlock.get().defaultBlockState().entityInside(world, pos, entity);
    }

    @Override
    public void animateTick(BlockState state, Level world, BlockPos pos, RandomSource rand) {
        fluidBlock.get()
            .animateTick(fluidBlock.get().defaultBlockState(), world, pos, rand);
    }

    @Override
    protected double getContentHeight(BlockState state) {
        return 0.9375D;
    }

    private ForgeFlowingFluid getFluid() {
        if(fluidBlock.get() instanceof PMLiquidBlock liquidBlock)
            return liquidBlock.getFluid();
        else return Utils.modErrorTyped("fluidBlock must of type PMLiquidBlock!");
    }

    private static int getFluidLight(RegisteredBlock fluidBlock) {
        if(fluidBlock.get() instanceof PMLiquidBlock liquidBlock)
            return liquidBlock.lightLevel();
        else return Utils.modErrorTyped("fluidBlock must of type PMLiquidBlock!");
    }

    public int fluidTintColor() {
        return IClientFluidTypeExtensions.of(getFluid()).getTintColor();
    }
}