package net.paradisemod.world.blocks;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.paradisemod.world.PMWorld;

public class ShatteredRedstoneBlock extends Block {
    public ShatteredRedstoneBlock() { super(Block.Properties.copy(Blocks.REDSTONE_BLOCK).lightLevel(state -> 7)); }

    @Override
    public void neighborChanged(@Nullable BlockState state, Level world, BlockPos pos, @Nullable Block block, @Nullable BlockPos fromPos, boolean isMoving) {
        for(var dir : Direction.values()) {
            var adjPos = pos.relative(dir);
            var adjState = world.getBlockState(adjPos);
            if (world.getBlockState(adjPos).isAir() && !adjState.is(PMWorld.POWERED_AIR.get()))
                world.setBlock(adjPos, PMWorld.POWERED_AIR.get().defaultBlockState(), 32);
        }
    }

    @Override
    public void tick(BlockState state, ServerLevel world, BlockPos pos, RandomSource rand) {
        this.neighborChanged(state, world, pos, this, BlockPos.ZERO, false);
    }

    @Override
    public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor world, BlockPos pos, BlockPos facingPos) {
        for(var dir : Direction.values()) {
            var adjPos = pos.relative(dir);
            var adjState = world.getBlockState(adjPos);
            if (world.getBlockState(adjPos).isAir() && !adjState.is(PMWorld.POWERED_AIR.get()))
                world.setBlock(adjPos, PMWorld.POWERED_AIR.get().defaultBlockState(), 32);
        }

        return defaultBlockState();
    }

    @Override
    public int getSignal(BlockState blockState, BlockGetter world, BlockPos pos, Direction side) { return 15; }
    
    @Override
    public boolean canConnectRedstone(BlockState state, BlockGetter level, BlockPos pos, Direction direction) {
        return true;
    }
}