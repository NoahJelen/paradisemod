package net.paradisemod.world.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.GlassBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.paradisemod.base.BlockType;
import net.paradisemod.world.dimension.PMDimensions;
import net.paradisemod.world.dimension.PMTeleporter;

public class PMPortalBlock extends GlassBlock {
    private final PMDimensions.Type dimension;

    public PMPortalBlock(PMDimensions.Type dimension) {
        super(BlockType.GLASS.getProperties().noCollission());
        this.dimension = dimension;
    }

    @Override
    public void animateTick(BlockState state, Level world, BlockPos pos, RandomSource rand) {
        if (rand.nextInt(100) == 0) world.playLocalSound(pos.getX() + 0.5D, pos.getY() + 0.5D, pos.getZ() + 0.5D, SoundEvents.PORTAL_AMBIENT, SoundSource.BLOCKS, 0.5F, rand.nextFloat() * 0.4F + 0.8F, false);

        for(var i = 0; i < 4; ++i) {
            var d0 = pos.getX() + rand.nextDouble();
            var d1 = pos.getY() + rand.nextDouble();
            var d2 = pos.getZ() + rand.nextDouble();
            var d3 = (rand.nextFloat() - 0.5D) / 2;
            var d4 = (rand.nextFloat() - 0.5D) / 2;
            var d5 = (rand.nextFloat() - 0.5D) / 2;
            var j = rand.nextInt(2) * 2 - 1;
            if (!world.getBlockState(pos.west()).is(this) && !world.getBlockState(pos.east()).is(this)) {
                d0 = pos.getX() + 0.5D + 0.25D * j;
                d3 = rand.nextFloat() * 2 * j;
            }
            else {
                d2 = pos.getZ() + 0.5D + 0.25D * j;
                d5 = rand.nextFloat() * 2 * j;
            }
            world.addParticle(dimension.getPortalParticles(), d0, d1, d2, d3, d4, d5);
        }
    }

    @Override
    public void entityInside(BlockState state, Level world, BlockPos pos, Entity entity) {
        var entityWorld = entity.level();
        if(entity.isOnPortalCooldown()) entity.setPortalCooldown();
        else if (
            world instanceof ServerLevel serverWorld &&
            !entity.isPassenger() &&
            !entity.isVehicle() &&
            entity.canChangeDimensions() &&
            !entityWorld.isClientSide
        ) {
            var server = serverWorld.getServer();
            var where2go = entityWorld.dimension() == Level.OVERWORLD ? dimension.getKey() : Level.OVERWORLD;
            var dest = server.getLevel(where2go);
            entityWorld.getProfiler().push(dimension.name().toLowerCase());
            if(!pos.equals(entity.portalEntrancePos)) entity.portalEntrancePos = pos;
            entity.setPortalCooldown();
            var axis = entity.getDirection().getAxis();
            entity.changeDimension(dest, new PMTeleporter(dest, dimension, this, axis));
            entityWorld.getProfiler().pop();
        }
    }
}