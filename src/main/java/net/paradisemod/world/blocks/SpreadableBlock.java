package net.paradisemod.world.blocks;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.TagKey;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.BonemealableBlock;
import net.minecraft.world.level.block.SpreadingSnowyDirtBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.paradisemod.base.Utils;

public class SpreadableBlock extends SpreadingSnowyDirtBlock implements BonemealableBlock {
    private final TagKey<Block> foliageTag;
    private final Block spreadToBlock;

    public SpreadableBlock(Block spreadToBlock, @Nullable TagKey<Block> foliageTag, Block.Properties properties) {
        super(properties.randomTicks());
        this.foliageTag = foliageTag;
        this.spreadToBlock = spreadToBlock;
    }

    @Override
    public boolean isValidBonemealTarget(LevelReader world, BlockPos pos, BlockState state, boolean isClient) {
        return foliageTag != null;
    }

    @Override
    public boolean isBonemealSuccess(Level world, RandomSource rand, BlockPos pos, BlockState state) {
        return foliageTag != null;
    }

    @Override
    public void performBonemeal(ServerLevel world, RandomSource rand, BlockPos pos, BlockState state) {
        if(foliageTag == null) return;
        var x = pos.getX() - 7;
        var y = pos.getY() - 7;
        var z = pos.getZ() - 7;

        if (y < 0) y = 0;

        var end_foliage = Utils.getBlockTag(foliageTag);
        world.setBlockAndUpdate(pos.above(), end_foliage.getRandomElement(rand).get().defaultBlockState());

        for (var bx = x; bx < x + 14; bx++)
            for (var bz = z; bz < z + 14; bz++)
                for (var by = y; by < y + 14; by++)
                    if (world.getBlockState(new BlockPos(bx, by, bz)).is(this) && world.getBlockState(new BlockPos(bx, by + 1, bz)).isAir() && rand.nextInt(8) == 0)
                        world.setBlockAndUpdate(new BlockPos(bx, by + 1, bz), end_foliage.getRandomElement(rand).get().defaultBlockState());
    }

    @Override
    public void randomTick(BlockState state, ServerLevel world, BlockPos pos, RandomSource rand) {
        BlockPos[] adjBlocks = {
            pos.west(), pos.east(), pos.north(), pos.south(),
            pos.above().west(), pos.above().east(), pos.above().north(), pos.above().south(),
            pos.below().west(), pos.below().east(), pos.below().north(), pos.below().south()
        };

        var blockToSpreadto = adjBlocks[rand.nextInt(12)];
        var adjBlock = world.getBlockState(blockToSpreadto).getBlock();
        var blockAbove = world.getBlockState(blockToSpreadto.above());
        var hasFluid = !world.getBlockState(pos.above()).getFluidState().isEmpty();
        if ((world.getBlockState(pos.above()).isSolidRender(world, pos) || hasFluid))
            world.setBlockAndUpdate(pos, spreadToBlock.defaultBlockState());

        if (world.getMaxLocalRawBrightness(pos.above()) >= 9 && adjBlock == spreadToBlock && !blockAbove.canOcclude() && !hasFluid)
            world.setBlockAndUpdate(blockToSpreadto, defaultBlockState());
    }
}