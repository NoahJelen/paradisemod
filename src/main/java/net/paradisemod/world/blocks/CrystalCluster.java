package net.paradisemod.world.blocks;

import java.util.ArrayList;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.EndRodBlock;
import net.minecraft.world.level.block.SimpleWaterloggedBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.level.material.PushReaction;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class CrystalCluster extends EndRodBlock implements SimpleWaterloggedBlock {
    public static final IntegerProperty TYPE = IntegerProperty.create("type", 0, 3);
    public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;

    private static final VoxelShape[] CRYSTAL_UP = {
        Block.box(5, 0, 5, 11, 17.5, 11),
        Shapes.block(),
        Block.box(5, 0, 5, 11, 14.5, 11),
        Block.box(2, 0, 2, 14, 18.5, 14)
    };

    private static final VoxelShape[] CRYSTAL_DOWN = {
        Block.box(5, -1.5, 5, 11, 16, 11),
        Shapes.block(),
        Block.box(5, 1.5, 5, 11, 16, 11),
        Block.box(2, -2.5, 2, 14, 16, 14)
    };

    private static final VoxelShape[] CRYSTAL_NORTH = {
        Block.box(5, 5, -1.5, 11, 11, 16),
        Shapes.block(),
        Block.box(5, 5, 1.5, 11, 11, 16),
        Block.box(2, 2, -2.5, 14, 14, 16)
    };

    private static final VoxelShape[] CRYSTAL_SOUTH = {
        Block.box(5, 5, 0, 11, 11, 17.5),
        Shapes.block(),
        Block.box(5, 5, 0, 11, 11, 14),
        Block.box(2, 2, 0, 14, 14, 18.5)
    };

    private static final VoxelShape[] CRYSTAL_EAST = {
        Block.box(0, 5, 5, 17.5, 11, 11),
        Shapes.block(),
        Block.box(0, 5, 5, 14, 11, 11),
        Block.box(0, 2, 2, 18.5, 14, 14)
    };

    private static final VoxelShape[] CRYSTAL_WEST = {
        Block.box(-1.5, 5, 5, 16, 11, 11),
        Shapes.block(),
        Block.box(1.5, 5, 5, 16, 11, 11),
        Block.box(-2.5, 2, 2, 16, 14, 14)
    };

    private final boolean randTeleport;
    private final boolean hasRedstonePower;
    
    public CrystalCluster(boolean randTeleport, boolean hasRedstonePower) {
        super(Block.Properties.copy(Blocks.AMETHYST_CLUSTER).lightLevel(s -> 10).noCollission());
        this.hasRedstonePower = hasRedstonePower;
        this.randTeleport = randTeleport;
        registerDefaultState(stateDefinition.any().setValue(FACING, Direction.UP).setValue(TYPE, 0).setValue(WATERLOGGED, false));
    }

    @Override
    public boolean canSurvive(BlockState state, LevelReader world, BlockPos pos) { return SmallCaveFormation.isSupported(world, state.getValue(FACING), pos); }

    @Override
    public void neighborChanged(BlockState state, Level world, BlockPos pos, Block block, BlockPos fromPos, boolean isMoving) {
        if (!SmallCaveFormation.isSupported(world, state.getValue(FACING), pos))
            world.destroyBlock(pos, true);
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter world, BlockPos pos, CollisionContext context) {
        var type = state.getValue(TYPE);
        var direction = state.getValue(FACING);
        return switch (direction) {
            default -> CRYSTAL_UP[type];
            case DOWN -> CRYSTAL_DOWN[type];
            case NORTH -> CRYSTAL_NORTH[type];
            case SOUTH -> CRYSTAL_SOUTH[type];
            case EAST -> CRYSTAL_EAST[type];
            case WEST -> CRYSTAL_WEST[type];
        };
    }

    @Override
    public void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) { builder.add(FACING, TYPE, WATERLOGGED); }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext ctx) {
        var world = ctx.getLevel();
        var state = defaultBlockState();
        var rand = world.random;
        var facing = ctx.getClickedFace();
        var fluidstate = world.getFluidState(ctx.getClickedPos());
        return state.setValue(FACING, facing).setValue(TYPE, rand.nextInt(4)).setValue(WATERLOGGED, fluidstate.getType() == Fluids.WATER);
    }

    @Override
    public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor world, BlockPos currentPos, BlockPos facingPos) {
        if (state.getValue(WATERLOGGED)) world.scheduleTick(currentPos, Fluids.WATER, Fluids.WATER.getTickDelay(world));
        return super.updateShape(state, facing, facingState, world, currentPos, facingPos);
    }

    @Override
    public PushReaction getPistonPushReaction(BlockState state) { return PushReaction.DESTROY; }

    @Override
    public FluidState getFluidState(BlockState state) { return state.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(state); }

    @Override
    public void animateTick(BlockState state, Level world, BlockPos pos, RandomSource rand) { }

    @Override
    public void entityInside(BlockState state, Level world, BlockPos pos, Entity entity) {
        if(randTeleport) {
            if(!entity.isOnPortalCooldown()) {
                ArrayList<BlockPos> otherClusters = new ArrayList<>();
                for (var x = -7; x <= 7; x++)
                    for (var z = -7; z <= 7; z++)
                        for (var y = -7; y <= 7; y++) {
                            if (x == 0 && y == 0 && z == 0) continue;
                            var newPos = pos.offset(x, y, z);
                            if (world.getBlockState(newPos).is(this)) otherClusters.add(newPos);
                        }

                if (otherClusters.isEmpty()) return;
                var newEntityPos = otherClusters.get(world.random.nextInt(otherClusters.size()));
                entity.teleportToWithTicket(newEntityPos.getX() + .5d, newEntityPos.getY(), newEntityPos.getZ() + .5d);
                entity.setPortalCooldown();
            }
        }
    }

    @Override
    public int getSignal(BlockState blockState, BlockGetter world, BlockPos pos, Direction side) {
        return hasRedstonePower ? 15 : 0;
    }

    @Override
    public boolean canConnectRedstone(BlockState state, BlockGetter level, BlockPos pos, Direction direction) {
        return hasRedstonePower;
    }
}