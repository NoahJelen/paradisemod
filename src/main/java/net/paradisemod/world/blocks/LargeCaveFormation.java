package net.paradisemod.world.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SimpleWaterloggedBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class LargeCaveFormation extends Block implements SimpleWaterloggedBlock {
    public static final EnumProperty<Size> SIZE = EnumProperty.create("size", Size.class);
    public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;
    public static final BooleanProperty INVERTED = BooleanProperty.create("inverted");
    private static final VoxelShape SMALL = Block.box(7, 0, 7, 9, 16, 9);
    private static final VoxelShape MEDIUM = Block.box(6, 0, 6, 10, 16, 10);
    private static final VoxelShape LARGE = Block.box(5, 0, 5, 11, 16, 11);

    public LargeCaveFormation(Block matchingBlock) {
        super(Block.Properties.copy(matchingBlock).noOcclusion());
        registerDefaultState(
            stateDefinition.any()
                .setValue(SIZE, Size.SMALL)
                .setValue(WATERLOGGED, false)
                .setValue(INVERTED, false)
        );
    }

    @Override
    public boolean canSurvive(BlockState state, LevelReader world, BlockPos pos) {
        if (state.getValue(INVERTED)) {
            var stateAbove = world.getBlockState(pos.above());
            if (stateAbove.is(this)) return !(stateAbove.getValue(SIZE) == Size.SMALL);
            return stateAbove.is(Blocks.ICE) || stateAbove.is(Blocks.BLUE_ICE) || stateAbove.is(Blocks.PACKED_ICE) || stateAbove.isSolid() || stateAbove.is(this);
        }
        else {
            var stateBelow = world.getBlockState(pos.below());
            if (stateBelow.is(this)) return !(stateBelow.getValue(SIZE) == Size.SMALL);
            return stateBelow.is(Blocks.ICE) || stateBelow.is(Blocks.BLUE_ICE) || stateBelow.is(Blocks.PACKED_ICE) || stateBelow.isSolid() || stateBelow.is(this);
        }
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter world, BlockPos pos, CollisionContext context) {
        return switch (state.getValue(SIZE)) {
            default -> SMALL;
            case MEDIUM -> MEDIUM;
            case LARGE -> LARGE;
        };
    }

    @Override
    public void neighborChanged(BlockState state, Level world, BlockPos pos, Block block, BlockPos fromPos, boolean isMoving) {
        if (state.getValue(INVERTED)){
            var stateAbove = world.getBlockState(pos.above());
            if (stateAbove.is(Blocks.ICE) || stateAbove.is(Blocks.BLUE_ICE) || stateAbove.is(Blocks.PACKED_ICE) || stateAbove.isSolid() || stateAbove.is(this)) return;
            world.destroyBlock(pos, false);
        }
        else {
            var stateBelow = world.getBlockState(pos.below());
            if (stateBelow.is(Blocks.ICE) || stateBelow.is(Blocks.BLUE_ICE) || stateBelow.is(Blocks.PACKED_ICE) || stateBelow.isSolid() || stateBelow.is(this)) return;
            world.destroyBlock(pos, false);
        }
    }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext ctx) {
        var world = ctx.getLevel();
        var pos = ctx.getClickedPos();
        var state = defaultBlockState();
        var stateBelow = world.getBlockState(pos.below());
        var stateAbove = world.getBlockState(pos.above());
        if (stateBelow.is(this)) {
            state = switch(stateBelow.getValue(SIZE)) {
                case SMALL, MEDIUM -> state.setValue(SIZE, Size.SMALL);
                default -> state.setValue(SIZE, Size.MEDIUM);
            };
        }
        else if(stateAbove.is(this)) {
            state = state.setValue(INVERTED, true);
            state = switch (stateAbove.getValue(SIZE)) {
                case SMALL, MEDIUM -> state.setValue(SIZE, Size.SMALL);
                default -> state.setValue(SIZE, Size.MEDIUM);
            };
        }
        else if(stateAbove.isSolid()) state = state.setValue(SIZE, Size.LARGE).setValue(INVERTED, true);
        else state = state.setValue(SIZE, Size.LARGE);
        var fluidstate = world.getFluidState(pos);
        return state.setValue(WATERLOGGED, fluidstate.getType() == Fluids.WATER);
    }

    @Override
    public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor world, BlockPos currentPos, BlockPos facingPos) {
        if (state.getValue(WATERLOGGED)) world.scheduleTick(currentPos, Fluids.WATER, Fluids.WATER.getTickDelay(world));
        return super.updateShape(state, facing, facingState, world, currentPos, facingPos);
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) { builder.add(SIZE, INVERTED, WATERLOGGED); }

    @Override
    public FluidState getFluidState(BlockState state) { return state.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(state); }

    // convenience method for placing large cave formations
    public void place(WorldGenLevel world, BlockPos pos, boolean inverted) {
        if (inverted) {
            var fluid1 = world.getFluidState(pos).getType();
            var fluid2 = world.getFluidState(pos.below()).getType();
            var fluid3 = world.getFluidState(pos.below(2)).getType();
            if (world.getBlockState(pos).isAir() || world.getBlockState(pos).is(Blocks.WATER))
                world.setBlock(pos, defaultBlockState().setValue(SIZE, Size.LARGE).setValue(INVERTED, true).setValue(WATERLOGGED, fluid1 == Fluids.WATER), 32);

            if (world.getBlockState(pos.below()).isAir() || world.getBlockState(pos.below()).is(Blocks.WATER))
                world.setBlock(pos.below(), defaultBlockState().setValue(SIZE, Size.MEDIUM).setValue(INVERTED, true).setValue(WATERLOGGED, fluid2 == Fluids.WATER), 32);

            if (world.getBlockState(pos.below(2)).isAir() || world.getBlockState(pos.below(2)).is(Blocks.WATER))
                world.setBlock(pos.below(2), defaultBlockState().setValue(SIZE, Size.SMALL).setValue(INVERTED, true).setValue(WATERLOGGED, fluid3 == Fluids.WATER), 32);
        }
        else {
            var fluid1 = world.getFluidState(pos).getType();
            var fluid2 = world.getFluidState(pos.above()).getType();
            var fluid3 = world.getFluidState(pos.above(2)).getType();
            if (world.getBlockState(pos).isAir() || world.getBlockState(pos).is(Blocks.WATER)) world.setBlock(pos, defaultBlockState().setValue(SIZE, Size.LARGE).setValue(WATERLOGGED, fluid1 == Fluids.WATER), 32);
            if (world.getBlockState(pos.above()).isAir() || world.getBlockState(pos.above()).is(Blocks.WATER)) world.setBlock(pos.above(), defaultBlockState().setValue(SIZE, Size.MEDIUM).setValue(WATERLOGGED, fluid2 == Fluids.WATER), 32);
            if (world.getBlockState(pos.above(2)).isAir() || world.getBlockState(pos.above(2)).is(Blocks.WATER)) world.setBlock(pos.above(2), defaultBlockState().setValue(SIZE, Size.SMALL).setValue(WATERLOGGED, fluid3 == Fluids.WATER), 32);
        }
    }

    public enum Size implements StringRepresentable {
        SMALL,
        MEDIUM,
        LARGE;

        @Override
        public String getSerializedName() { return name().toLowerCase(); }
    }
}