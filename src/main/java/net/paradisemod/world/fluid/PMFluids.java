package net.paradisemod.world.fluid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import net.minecraft.client.renderer.ItemBlockRenderTypes;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.core.cauldron.CauldronInteraction;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity.RemovalReason;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.BucketItem;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.material.Fluid;
import net.minecraftforge.client.model.generators.loaders.DynamicFluidContainerModelBuilder;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fluids.ForgeFlowingFluid;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.base.Utils;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.base.registry.RegisteredItem;
import net.paradisemod.building.Building;
import net.paradisemod.misc.Misc;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.Ores;
import net.paradisemod.world.PMWorld;
import net.paradisemod.world.blocks.CustomCauldron;

//FIXME2025: the waterlike fluids should have the same sound effects as water
public class PMFluids {
    public static final HashMap<RegistryObject<ForgeFlowingFluid>, Supplier<Item>> COLORED_BUCKETS = new HashMap<>();
    public static final ArrayList<RegisteredBlock> COLORED_CAULDRONS = new ArrayList<>();
    private static final DeferredRegister<Fluid> FLUIDS = PMRegistries.createRegistry(ForgeRegistries.FLUIDS);

    // ender acid
    public static final RegistryObject<ForgeFlowingFluid> ENDER_ACID = getFluid("ender_acid", () -> PMFluids.ENDER_ACID_PROPERTIES, true, false);
    public static final RegistryObject<ForgeFlowingFluid> FLOWING_ENDER_ACID = getFluid("ender_acid", () -> PMFluids.ENDER_ACID_PROPERTIES, false, false);
    public static final RegisteredItem ENDER_ACID_BUCKET = regBucket(ENDER_ACID);

    public static final RegisteredBlock ENDER_ACID_BLOCK = createFluidBlock(
        new PMLiquidBlock.Builder(ENDER_ACID, Blocks.WATER)
            .lightLevel(15)
            .animator(
                (state, world, pos, rand) -> {
                    if (rand.nextInt(10) == 0)
                        world.addParticle(ParticleTypes.DRAGON_BREATH, pos.getX() + rand.nextDouble(), pos.getY() + 1.1D, pos.getZ() + rand.nextDouble(), 0, 0, 0);
                }
            )
            .collideEvent(
                (state, world, pos, entity) -> {
                    if (entity instanceof ItemEntity) entity.remove(RemovalReason.KILLED);
                    if (entity instanceof LivingEntity creature)
                        creature.hurt(world.damageSources().dragonBreath(), 2);
                }
            )
            .translucentRender()
    );

    public static final RegisteredBlock ENDER_ACID_CAULDRON = createCauldron(ENDER_ACID_BLOCK, PMCauldronInteractions.ENDER_ACID, true);

    public static final ForgeFlowingFluid.Properties ENDER_ACID_PROPERTIES = new FluidPropertiesBuilder(ENDER_ACID, FLOWING_ENDER_ACID, ENDER_ACID_BLOCK, ENDER_ACID_BUCKET)
        .lightLevel(15)
        .fogColor(0xFF300090)
        .build();

    // glowing water
    public static final RegistryObject<ForgeFlowingFluid> GLOWING_WATER = getFluid("glowing_water", () -> PMFluids.GLOWING_WATER_PROPERTIES, true, false);
    public static final RegistryObject<ForgeFlowingFluid> FLOWING_GLOWING_WATER = getFluid("glowing_water", () -> PMFluids.GLOWING_WATER_PROPERTIES, false, false);
    public static final RegisteredItem GLOWING_WATER_BUCKET = regBucket(GLOWING_WATER);

    public static final RegisteredBlock GLOWING_WATER_BLOCK = createFluidBlock(
        new PMLiquidBlock.Builder(GLOWING_WATER, Blocks.WATER)
            .lightLevel(15)
            .animator(
                (state, world, pos, rand) -> {
                    if (rand.nextInt(10) == 0)
                        world.addParticle(ParticleTypes.END_ROD, pos.getX() + rand.nextDouble(), pos.getY() + 1.1D, pos.getZ() + rand.nextDouble(), 0, 0, 0);
                }
            )
            .collideEvent(
                (state, world, pos, entity) -> {
                    if (entity instanceof LivingEntity creature)
                    creature.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 100));
                }
            )
            .solidifyBlock(Misc.GLOWING_ICE)
            .solidifySourceBlock(Misc.GLOWING_ICE)
            .translucentRender()
    );

    public static final RegisteredBlock GLOWING_WATER_CAULDRON = createCauldron(GLOWING_WATER_BLOCK, PMCauldronInteractions.GLOWING_WATER, true);

    public static final ForgeFlowingFluid.Properties GLOWING_WATER_PROPERTIES = new FluidPropertiesBuilder(GLOWING_WATER, FLOWING_GLOWING_WATER, GLOWING_WATER_BLOCK, GLOWING_WATER_BUCKET)
        .fogColor(0xFF078E8E)
        .lightLevel(15)
        .build();

    // psychedelic fluid
    public static final RegistryObject<ForgeFlowingFluid> PSYCHEDELIC_FLUID = getFluid("psychedelic_fluid", () -> PMFluids.PSYCHEDELIC_FLUID_PROPERTIES, true, false);
    public static final RegistryObject<ForgeFlowingFluid> FLOWING_PSYCHEDELIC_FLUID = getFluid("psychedelic_fluid", () -> PMFluids.PSYCHEDELIC_FLUID_PROPERTIES, false, false);
    public static final RegisteredItem PSYCHEDELIC_FLUID_BUCKET = regBucket(PSYCHEDELIC_FLUID);

    public static final RegisteredBlock PSYCHEDELIC_FLUID_BLOCK = createFluidBlock(
        new PMLiquidBlock.Builder(PSYCHEDELIC_FLUID, Blocks.WATER)
            .lightLevel(15)
    );

    public static final RegisteredBlock PSYCHEDELIC_FLUID_CAULDRON = createCauldron(PSYCHEDELIC_FLUID_BLOCK, PMCauldronInteractions.PSYCHEDELIC, false);

    public static final ForgeFlowingFluid.Properties PSYCHEDELIC_FLUID_PROPERTIES = new FluidPropertiesBuilder(PSYCHEDELIC_FLUID, FLOWING_PSYCHEDELIC_FLUID, PSYCHEDELIC_FLUID_BLOCK, PSYCHEDELIC_FLUID_BUCKET)
        .lightLevel(15)
        .build();

    // honey
    public static final RegistryObject<ForgeFlowingFluid> HONEY = getFluid("honey", () -> PMFluids.HONEY_PROPERTIES, true, false);
    public static final RegistryObject<ForgeFlowingFluid> FLOWING_HONEY = getFluid("honey", () -> PMFluids.HONEY_PROPERTIES, false, false);
    public static final RegisteredItem HONEY_BUCKET = regBucket(HONEY);

    public static final RegisteredBlock HONEY_BLOCK = createFluidBlock(
        new PMLiquidBlock.Builder(HONEY, Blocks.WATER)
            .solidifyBlock(Misc.HONEY_CRYSTAL_BLOCK)
            .solidifySourceBlock(() -> Blocks.HONEY_BLOCK)
            .ambientSound(SoundEvents.HONEY_DRINK)
            .translucentRender()
    );

    public static final RegisteredBlock HONEY_CAULDRON = createCauldron(HONEY_BLOCK, PMCauldronInteractions.HONEY, true);

    public static final ForgeFlowingFluid.Properties HONEY_PROPERTIES = new FluidPropertiesBuilder(HONEY, FLOWING_HONEY, HONEY_BLOCK, HONEY_BUCKET)
        .density(3000)
        .temperature(500)
        .viscosity(3000)
        .fillSound(SoundEvents.BUCKET_FILL_LAVA)
        .emptySound(SoundEvents.BUCKET_EMPTY_LAVA)
        .tickRate(15)
        .fogColor(0xFFC29E11)
        .build();

    // liquid redstone
    public static final RegistryObject<ForgeFlowingFluid> LIQUID_REDSTONE = getFluid("liquid_redstone", () -> PMFluids.LIQUID_REDSTONE_PROPERTIES, true, true);
    public static final RegistryObject<ForgeFlowingFluid> FLOWING_LIQUID_REDSTONE = getFluid("liquid_redstone", () -> PMFluids.LIQUID_REDSTONE_PROPERTIES, false, true);
    public static final RegisteredItem LIQUID_REDSTONE_BUCKET = regBucket(LIQUID_REDSTONE);

    public static final RegisteredBlock LIQUID_REDSTONE_BLOCK = createFluidBlock(
        new PMLiquidBlock.Builder(LIQUID_REDSTONE, Blocks.LAVA)
            .solidifyBlock(() -> Blocks.REDSTONE_BLOCK)
            .solidifySourceBlock(PMWorld.SHATTERED_REDSTONE_BLOCK)
            .hasRedstoneSignal(true)
            .isHot(true)
            .lightLevel(15)
    );

    public static final RegisteredBlock LIQUID_REDSTONE_CAULDRON = createCauldron(LIQUID_REDSTONE_BLOCK, PMCauldronInteractions.LIQUID_REDSTONE, false);

    public static final ForgeFlowingFluid.Properties LIQUID_REDSTONE_PROPERTIES = new FluidPropertiesBuilder(LIQUID_REDSTONE, FLOWING_LIQUID_REDSTONE, LIQUID_REDSTONE_BLOCK, LIQUID_REDSTONE_BUCKET)
        .canBecomeSource(false)
        .density(3000)
        .lightLevel(15)
        .temperature(1300)
        .viscosity(6000)
        .fillSound(SoundEvents.BUCKET_FILL_LAVA)
        .emptySound(SoundEvents.BUCKET_EMPTY_LAVA)
        .tickRate(30)
        .rendersLikeLava()
        .tintColor(0xFFFF0000)
        .isHot()
        .build();

    // molten salt
    public static final RegistryObject<ForgeFlowingFluid> MOLTEN_SALT = getFluid("molten_salt", () -> PMFluids.MOLTEN_SALT_PROPERTIES, true, true);
    public static final RegistryObject<ForgeFlowingFluid> FLOWING_MOLTEN_SALT = getFluid("molten_salt", () -> PMFluids.MOLTEN_SALT_PROPERTIES, false, true);
    public static final RegisteredItem MOLTEN_SALT_BUCKET = regBucket(MOLTEN_SALT);

    public static final RegisteredBlock MOLTEN_SALT_BLOCK = createFluidBlock(
        new PMLiquidBlock.Builder(MOLTEN_SALT, Blocks.LAVA)
            .solidifyBlock(Ores.COMPACT_SALT_BLOCK)
            .solidifySourceBlock(Ores.COMPACT_SALT_BLOCK)
            .isHot(true)
            .lightLevel(15)
    );

    public static final RegisteredBlock MOLTEN_SALT_CAULDRON = createCauldron(MOLTEN_SALT_BLOCK, PMCauldronInteractions.MOLTEN_SALT, false);

    public static final ForgeFlowingFluid.Properties MOLTEN_SALT_PROPERTIES = new FluidPropertiesBuilder(MOLTEN_SALT, FLOWING_MOLTEN_SALT, MOLTEN_SALT_BLOCK, MOLTEN_SALT_BUCKET)
        .canBecomeSource(false)
        .density(3000)
        .lightLevel(15)
        .temperature(1300)
        .viscosity(1000)
        .fillSound(SoundEvents.BUCKET_FILL_LAVA)
        .emptySound(SoundEvents.BUCKET_EMPTY_LAVA)
        .tickRate(10)
        .rendersLikeLava()
        .tintColor(0xFFD07373)
        .isHot()
        .build();

    // psychedelic lava
    public static final RegistryObject<ForgeFlowingFluid> PSYCHEDELIC_LAVA = getFluid("psychedelic_lava", () -> PMFluids.PSYCHEDELIC_LAVA_PROPERTIES, true, true);
    public static final RegistryObject<ForgeFlowingFluid> FLOWING_PSYCHEDELIC_LAVA = getFluid("psychedelic_lava", () -> PMFluids.PSYCHEDELIC_LAVA_PROPERTIES, false, true);
    public static final RegisteredItem PSYCHEDELIC_LAVA_BUCKET = regBucket(PSYCHEDELIC_LAVA);

    public static final RegisteredBlock PSYCHEDELIC_LAVA_BLOCK = createFluidBlock(
        new PMLiquidBlock.Builder(PSYCHEDELIC_LAVA, Blocks.LAVA)
            .isHot(true)
            .lightLevel(15)
    );

    public static final RegisteredBlock PSYCHEDELIC_LAVA_CAULDRON = createCauldron(PSYCHEDELIC_LAVA_BLOCK, PMCauldronInteractions.PSYCHEDELIC_LAVA, false);

    public static final ForgeFlowingFluid.Properties PSYCHEDELIC_LAVA_PROPERTIES = new FluidPropertiesBuilder(PSYCHEDELIC_LAVA, FLOWING_PSYCHEDELIC_LAVA, PSYCHEDELIC_LAVA_BLOCK, PSYCHEDELIC_LAVA_BUCKET)
        .canBecomeSource(false)
        .density(3000)
        .lightLevel(15)
        .temperature(1300)
        .viscosity(6000)
        .fillSound(SoundEvents.BUCKET_FILL_LAVA)
        .emptySound(SoundEvents.BUCKET_EMPTY_LAVA)
        .tickRate(30)
        .rendersLikeLava()
        .isHot()
        .build();

    // tar
    public static final RegistryObject<ForgeFlowingFluid> TAR = getFluid("tar", () -> PMFluids.TAR_PROPERTIES, true, false);
    public static final RegistryObject<ForgeFlowingFluid> FLOWING_TAR = getFluid("tar", () -> PMFluids.TAR_PROPERTIES, false, false);
    public static final RegisteredItem TAR_BUCKET = regBucket(TAR);

    public static final RegisteredBlock TAR_BLOCK = createFluidBlock(
        new PMLiquidBlock.Builder(TAR, Blocks.WATER)
            .solidifyBlock(Building.ASPHALT)
            .solidifySourceBlock(Building.ASPHALT)
    )
        .tag(BlockTags.INFINIBURN_OVERWORLD);

    public static final RegisteredBlock TAR_CAULDRON = createCauldron(TAR_BLOCK, PMCauldronInteractions.TAR, false);

    public static final ForgeFlowingFluid.Properties TAR_PROPERTIES = new FluidPropertiesBuilder(TAR, FLOWING_TAR, TAR_BLOCK, TAR_BUCKET)
        .canBecomeSource(true)
        .viscosity(5000)
        .fillSound(SoundEvents.BUCKET_FILL_LAVA)
        .emptySound(SoundEvents.BUCKET_EMPTY_LAVA)
        .tickRate(50)
        .rendersLikeLava()
        .tintColor(0xFF101010)
        .build();

    // dark lava
    public static final RegistryObject<ForgeFlowingFluid> DARK_LAVA = getFluid("dark_lava", () -> PMFluids.DARK_LAVA_PROPERTIES, true, true);
    public static final RegistryObject<ForgeFlowingFluid> FLOWING_DARK_LAVA = getFluid("dark_lava", () -> PMFluids.DARK_LAVA_PROPERTIES, false, true);
    public static final RegisteredItem DARK_LAVA_BUCKET = regBucket(DARK_LAVA);

    public static final RegisteredBlock DARK_LAVA_BLOCK = createFluidBlock(
        new PMLiquidBlock.Builder(DARK_LAVA, Blocks.LAVA)
            .solidifyBlock(DeepDarkBlocks.DARKSTONE)
            .solidifySourceBlock(Building.BLACK_GLOWING_OBSIDIAN)
            .lightLevel(7)
            .isHot(true)
    );

    public static final RegisteredBlock DARK_LAVA_CAULDRON = createCauldron(DARK_LAVA_BLOCK, PMCauldronInteractions.DARK_LAVA, false);

    public static final ForgeFlowingFluid.Properties DARK_LAVA_PROPERTIES = new FluidPropertiesBuilder(DARK_LAVA, FLOWING_DARK_LAVA, DARK_LAVA_BLOCK, DARK_LAVA_BUCKET)
        .canBecomeSource(false)
        .density(3000)
        .lightLevel(7)
        .temperature(1300)
        .viscosity(6000)
        .fillSound(SoundEvents.BUCKET_FILL_LAVA)
        .emptySound(SoundEvents.BUCKET_EMPTY_LAVA)
        .tickRate(10)
        .rendersLikeLava()
        .tintColor(0xFF2E2E2E)
        .isHot()
        .build();

    // molten gold
    public static final RegistryObject<ForgeFlowingFluid> MOLTEN_GOLD = getFluid("molten_gold", () -> PMFluids.MOLTEN_GOLD_PROPERTIES, true, true);
    public static final RegistryObject<ForgeFlowingFluid> FLOWING_MOLTEN_GOLD = getFluid("molten_gold", () -> PMFluids.MOLTEN_GOLD_PROPERTIES, false, true);
    public static final RegisteredItem MOLTEN_GOLD_BUCKET = regBucket(MOLTEN_GOLD);

    public static final RegisteredBlock MOLTEN_GOLD_BLOCK = createFluidBlock(
        new PMLiquidBlock.Builder(MOLTEN_GOLD, Blocks.LAVA)
            .solidifyBlock(() -> Blocks.GOLD_BLOCK)
            .solidifySourceBlock(Building.GOLD_GLOWING_OBSIDIAN)
            .lightLevel(15)
            .isHot(true)
    );

    public static final RegisteredBlock MOLTEN_GOLD_CAULDRON = createCauldron(MOLTEN_GOLD_BLOCK, PMCauldronInteractions.MOLTEN_GOLD, false);

    public static final ForgeFlowingFluid.Properties MOLTEN_GOLD_PROPERTIES = new FluidPropertiesBuilder(MOLTEN_GOLD, FLOWING_MOLTEN_GOLD, MOLTEN_GOLD_BLOCK, MOLTEN_GOLD_BUCKET)
        .canBecomeSource(false)
        .density(3000)
        .lightLevel(15)
        .temperature(1300)
        .viscosity(6000)
        .fillSound(SoundEvents.BUCKET_FILL_LAVA)
        .emptySound(SoundEvents.BUCKET_EMPTY_LAVA)
        .tickRate(10)
        .rendersLikeLava()
        .tintColor(0xFFE4B200)
        .isHot()
        .build();

    // molten silver
    public static final RegistryObject<ForgeFlowingFluid> MOLTEN_SILVER = getFluid("molten_silver", () -> PMFluids.MOLTEN_SILVER_PROPERTIES, true, true);
    public static final RegistryObject<ForgeFlowingFluid> FLOWING_MOLTEN_SILVER = getFluid("molten_silver", () -> PMFluids.MOLTEN_SILVER_PROPERTIES, false, true);
    public static final RegisteredItem MOLTEN_SILVER_BUCKET = regBucket(MOLTEN_SILVER);

    public static final RegisteredBlock MOLTEN_SILVER_BLOCK = createFluidBlock(
        new PMLiquidBlock.Builder(MOLTEN_SILVER, Blocks.LAVA)
            .solidifyBlock(Ores.SILVER_BLOCK)
            .solidifySourceBlock(Building.SILVER_GLOWING_OBSIDIAN)
            .lightLevel(15)
            .isHot(true)
    );

    public static final RegisteredBlock MOLTEN_SILVER_CAULDRON = createCauldron(MOLTEN_SILVER_BLOCK, PMCauldronInteractions.MOLTEN_SILVER, false);

    public static final ForgeFlowingFluid.Properties MOLTEN_SILVER_PROPERTIES = new FluidPropertiesBuilder(MOLTEN_SILVER, FLOWING_MOLTEN_SILVER, MOLTEN_SILVER_BLOCK, MOLTEN_SILVER_BUCKET)
        .canBecomeSource(false)
        .density(3000)
        .lightLevel(15)
        .temperature(1300)
        .viscosity(6000)
        .fillSound(SoundEvents.BUCKET_FILL_LAVA)
        .emptySound(SoundEvents.BUCKET_EMPTY_LAVA)
        .tickRate(10)
        .rendersLikeLava()
        .tintColor(0xFF84869D)
        .isHot()
        .build();

    // molten iron
    public static final RegistryObject<ForgeFlowingFluid> MOLTEN_IRON = getFluid("molten_iron", () -> PMFluids.MOLTEN_IRON_PROPERTIES, true, true);
    public static final RegistryObject<ForgeFlowingFluid> FLOWING_MOLTEN_IRON = getFluid("molten_iron", () -> PMFluids.MOLTEN_IRON_PROPERTIES, false, true);
    public static final RegisteredItem MOLTEN_IRON_BUCKET = regBucket(MOLTEN_IRON);

    public static final RegisteredBlock MOLTEN_IRON_BLOCK = createFluidBlock(
        new PMLiquidBlock.Builder(MOLTEN_IRON, Blocks.LAVA)
            .solidifyBlock(() -> Blocks.IRON_BLOCK)
            .solidifySourceBlock(Building.IRON_GLOWING_OBSIDIAN)
            .lightLevel(15)
            .isHot(true)
    );

    public static final RegisteredBlock MOLTEN_IRON_CAULDRON = createCauldron(MOLTEN_IRON_BLOCK, PMCauldronInteractions.MOLTEN_IRON, false);

    public static final ForgeFlowingFluid.Properties MOLTEN_IRON_PROPERTIES = new FluidPropertiesBuilder(MOLTEN_IRON, FLOWING_MOLTEN_IRON, MOLTEN_IRON_BLOCK, MOLTEN_IRON_BUCKET)
        .canBecomeSource(false)
        .density(3000)
        .lightLevel(15)
        .temperature(1300)
        .viscosity(6000)
        .fillSound(SoundEvents.BUCKET_FILL_LAVA)
        .emptySound(SoundEvents.BUCKET_EMPTY_LAVA)
        .tickRate(10)
        .rendersLikeLava()
        .tintColor(0xFF727272)
        .isHot()
        .build();

    // molten copper
    public static final RegistryObject<ForgeFlowingFluid> MOLTEN_COPPER = getFluid("molten_copper", () -> PMFluids.MOLTEN_COPPER_PROPERTIES, true, true);
    public static final RegistryObject<ForgeFlowingFluid> FLOWING_MOLTEN_COPPER = getFluid("molten_copper", () -> PMFluids.MOLTEN_COPPER_PROPERTIES, false, true);
    public static final RegisteredItem MOLTEN_COPPER_BUCKET = regBucket(MOLTEN_COPPER);

    public static final RegisteredBlock MOLTEN_COPPER_BLOCK = createFluidBlock(
        new PMLiquidBlock.Builder(MOLTEN_COPPER, Blocks.LAVA)
            .solidifyBlock(() -> Blocks.COPPER_BLOCK)
            .solidifySourceBlock(Building.COPPER_GLOWING_OBSIDIAN)
            .lightLevel(15)
            .isHot(true)
    );

    public static final RegisteredBlock MOLTEN_COPPER_CAULDRON = createCauldron(MOLTEN_COPPER_BLOCK, PMCauldronInteractions.MOLTEN_COPPER, false);

    public static final ForgeFlowingFluid.Properties MOLTEN_COPPER_PROPERTIES = new FluidPropertiesBuilder(MOLTEN_COPPER, FLOWING_MOLTEN_COPPER, MOLTEN_COPPER_BLOCK, MOLTEN_COPPER_BUCKET)
        .canBecomeSource(false)
        .density(3000)
        .lightLevel(15)
        .temperature(1300)
        .viscosity(6000)
        .fillSound(SoundEvents.BUCKET_FILL_LAVA)
        .emptySound(SoundEvents.BUCKET_EMPTY_LAVA)
        .tickRate(10)
        .rendersLikeLava()
        .tintColor(0xFF7A450D)
        .isHot()
        .build();

    public static void init(IEventBus eventbus) {
        FLUIDS.register(eventbus);
        FluidPropertiesBuilder.FLUID_TYPES.register(eventbus);
        eventbus.addListener(PMCauldronInteractions::init);
    }

    public static void initClient() {
        ItemBlockRenderTypes.setRenderLayer(ENDER_ACID.get(), RenderType.translucent());
        ItemBlockRenderTypes.setRenderLayer(FLOWING_ENDER_ACID.get(), RenderType.translucent());
        ItemBlockRenderTypes.setRenderLayer(GLOWING_WATER.get(), RenderType.translucent());
        ItemBlockRenderTypes.setRenderLayer(FLOWING_GLOWING_WATER.get(), RenderType.translucent());
        ItemBlockRenderTypes.setRenderLayer(HONEY.get(), RenderType.translucent());
        ItemBlockRenderTypes.setRenderLayer(FLOWING_HONEY.get(), RenderType.translucent());
    }

    private static RegisteredBlock createFluidBlock(PMLiquidBlock.Builder builder) {
        var stillFluid = builder.getFluid();
        var name = stillFluid.getKey().location().getPath();

        return PMRegistries.regBlock(
        name,
        () -> builder.build()
    )
        .localizedName(
            fluidName(stillFluid, false),
            fluidName(stillFluid, true)
        )
        .noDrops();
    }

    private static RegisteredItem regBucket(RegistryObject<ForgeFlowingFluid> fluid) {
        return PMRegistries.regItem(
            fluid.getKey().location().getPath() + "_bucket",
            () -> new BucketItem(fluid, new Item.Properties().stacksTo(1))
        )
            .tab(CreativeModeTabs.TOOLS_AND_UTILITIES)
            .model(
                (item, generator) -> generator.withExistingParent("item/" + fluid.getKey().location().getPath() + "_bucket", new ResourceLocation("forge", "item/bucket_drip"))
                    .customLoader(DynamicFluidContainerModelBuilder::begin)
                    .fluid(fluid.get())
            )
            .localizedName(
                fluidName(fluid, false) + " Bucket",
                "Cubeta de " + fluidName(fluid, true).toLowerCase()
            );
    }

    private static RegistryObject<ForgeFlowingFluid> getFluid(String name, Supplier<ForgeFlowingFluid.Properties> properties, boolean source, boolean hot) {
        return Utils.handlePossibleException(
            () -> FLUIDS.register(
                (source ? "" : "flowing_") + name,
                () -> source ? new PMFluid.Source(properties.get(), hot) : new PMFluid.Flowing(properties.get(), hot)
            )
        );
    }

    private static RegisteredBlock createCauldron(RegisteredBlock fluidBlock, Map<Item, CauldronInteraction> interactions, boolean translucent) {
        var cauldron =  PMRegistries.regBlock(
            fluidBlock.shortName() + "_cauldron",
            () -> new CustomCauldron(fluidBlock, interactions, translucent)
        )
            .dropsItem(Blocks.CAULDRON)
            .tag(BlockTags.CAULDRONS);

        COLORED_CAULDRONS.add(cauldron);

        return cauldron;
    }
    private static String fluidName(RegistryObject<ForgeFlowingFluid> fluid, boolean spanish) {
        var name = fluid.getKey().location().getPath();

        return switch(name) {
            case "ender_acid" -> spanish ? "Ácido del Fin" : "Ender Acid";
            case "glowing_water" -> spanish ? "Agua brillante" : "Glowing Water";
            case "honey" -> spanish ? "Miel" : "Honey";
            case "liquid_redstone" -> spanish ? "Líquido de piedra roja" : "Liquid Redstone";
            case "molten_salt" -> spanish ? "Sal fundida" : "Molten Salt";
            case "psychedelic_lava" -> spanish ? "Lava psicodélica" : "Psychedelic Lava";
            case "psychedelic_fluid" -> spanish ? "Fluido psicodélico" : "Psychedelic Fluid";
            case "tar" -> spanish ? "Alquitrán" : "Tar";
            case "dark_lava" -> spanish ? "Lava oscura" : "Dark Lava";
            case "molten_gold" -> spanish ? "Oro fundido" : "Molten Gold";
            case "molten_silver" -> spanish ? "Plata fundida" : "Molten Silver";
            case "molten_iron" -> spanish ? "Hierro fundido" : "Molten Iron";
            case "molten_copper" -> spanish ? "Cobre fundido" : "Molten Copper";
            default -> "";
        };
    }
}