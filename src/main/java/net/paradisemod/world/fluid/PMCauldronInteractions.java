package net.paradisemod.world.fluid;

import java.util.Map;

import net.minecraft.core.cauldron.CauldronInteraction;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.base.registry.RegisteredItem;

public class PMCauldronInteractions {
    public static Map<Item, CauldronInteraction> ENDER_ACID = CauldronInteraction.newInteractionMap();
    public static Map<Item, CauldronInteraction> GLOWING_WATER = CauldronInteraction.newInteractionMap();
    public static Map<Item, CauldronInteraction> HONEY = CauldronInteraction.newInteractionMap();
    public static Map<Item, CauldronInteraction> PSYCHEDELIC = CauldronInteraction.newInteractionMap();
    public static Map<Item, CauldronInteraction> LIQUID_REDSTONE = CauldronInteraction.newInteractionMap();
    public static Map<Item, CauldronInteraction> MOLTEN_SALT = CauldronInteraction.newInteractionMap();
    public static Map<Item, CauldronInteraction> PSYCHEDELIC_LAVA = CauldronInteraction.newInteractionMap();
    public static Map<Item, CauldronInteraction> TAR = CauldronInteraction.newInteractionMap();
    public static Map<Item, CauldronInteraction> DARK_LAVA = CauldronInteraction.newInteractionMap();
    public static Map<Item, CauldronInteraction> MOLTEN_GOLD = CauldronInteraction.newInteractionMap();
    public static Map<Item, CauldronInteraction> MOLTEN_SILVER = CauldronInteraction.newInteractionMap();
    public static Map<Item, CauldronInteraction> MOLTEN_IRON = CauldronInteraction.newInteractionMap();
    public static Map<Item, CauldronInteraction> MOLTEN_COPPER = CauldronInteraction.newInteractionMap();

    public static void init(FMLCommonSetupEvent event) {
        defaultInteractions();
        ENDER_ACID.put(Items.BUCKET, emptyWaterLike(PMFluids.ENDER_ACID_CAULDRON, PMFluids.ENDER_ACID_BUCKET));
        GLOWING_WATER.put(Items.BUCKET, emptyWaterLike(PMFluids.GLOWING_WATER_CAULDRON, PMFluids.GLOWING_WATER_BUCKET));
        HONEY.put(Items.BUCKET, emptyWaterLike(PMFluids.HONEY_CAULDRON, PMFluids.HONEY_BUCKET));
        PSYCHEDELIC.put(Items.BUCKET, emptyWaterLike(PMFluids.PSYCHEDELIC_FLUID_CAULDRON, PMFluids.PSYCHEDELIC_FLUID_BUCKET));
        LIQUID_REDSTONE.put(Items.BUCKET, emptyLavaLike(PMFluids.LIQUID_REDSTONE_CAULDRON, PMFluids.LIQUID_REDSTONE_BUCKET));
        MOLTEN_SALT.put(Items.BUCKET, emptyLavaLike(PMFluids.MOLTEN_SALT_CAULDRON, PMFluids.MOLTEN_SALT_BUCKET));
        PSYCHEDELIC_LAVA.put(Items.BUCKET, emptyLavaLike(PMFluids.PSYCHEDELIC_LAVA_CAULDRON, PMFluids.PSYCHEDELIC_LAVA_BUCKET));
        TAR.put(Items.BUCKET, emptyWaterLike(PMFluids.TAR_CAULDRON, PMFluids.TAR_BUCKET));
        DARK_LAVA.put(Items.BUCKET, emptyLavaLike(PMFluids.DARK_LAVA_CAULDRON, PMFluids.DARK_LAVA_BUCKET));
        MOLTEN_GOLD.put(Items.BUCKET, emptyLavaLike(PMFluids.MOLTEN_GOLD_CAULDRON, PMFluids.MOLTEN_GOLD_BUCKET));
        MOLTEN_SILVER.put(Items.BUCKET, emptyLavaLike(PMFluids.MOLTEN_SILVER_CAULDRON, PMFluids.MOLTEN_SILVER_BUCKET));
        MOLTEN_IRON.put(Items.BUCKET, emptyLavaLike(PMFluids.MOLTEN_IRON_CAULDRON, PMFluids.MOLTEN_IRON_BUCKET));
        MOLTEN_COPPER.put(Items.BUCKET, emptyLavaLike(PMFluids.MOLTEN_COPPER_CAULDRON, PMFluids.MOLTEN_COPPER_BUCKET));
    }

    private static CauldronInteraction fillWaterLike(RegisteredBlock cauldron) {
        return (state, world, pos, player, hand, stack) -> CauldronInteraction.emptyBucket(world, pos, player, hand, stack, cauldron.get().defaultBlockState(), SoundEvents.BUCKET_EMPTY);
    }

    private static CauldronInteraction fillLavaLike(RegisteredBlock cauldron) {
        return (state, world, pos, player, hand, stack) -> CauldronInteraction.emptyBucket(world, pos, player, hand, stack, cauldron.get().defaultBlockState(), SoundEvents.BUCKET_EMPTY_LAVA);
    }

    private static CauldronInteraction emptyLavaLike(RegisteredBlock cauldron, RegisteredItem bucket) {
        return (state, world, pos, player, hand, stack) -> CauldronInteraction.fillBucket(state, world, pos, player, hand, stack, new ItemStack(bucket), s -> true, SoundEvents.BUCKET_FILL_LAVA);
    }

    private static CauldronInteraction emptyWaterLike(RegisteredBlock cauldron, RegisteredItem bucket) {
        return (state, world, pos, player, hand, stack) -> CauldronInteraction.fillBucket(state, world, pos, player, hand, stack, new ItemStack(bucket), s -> true, SoundEvents.BUCKET_FILL);
    }

    static void defaultInteractions() {
        CauldronInteraction.EMPTY.put(PMFluids.ENDER_ACID_BUCKET.asItem(), fillWaterLike(PMFluids.ENDER_ACID_CAULDRON));
        CauldronInteraction.EMPTY.put(PMFluids.GLOWING_WATER_BUCKET.asItem(), fillWaterLike(PMFluids.GLOWING_WATER_CAULDRON));
        CauldronInteraction.EMPTY.put(PMFluids.HONEY_BUCKET.asItem(), fillWaterLike(PMFluids.HONEY_CAULDRON));
        CauldronInteraction.EMPTY.put(PMFluids.PSYCHEDELIC_FLUID_BUCKET.asItem(), fillWaterLike(PMFluids.PSYCHEDELIC_FLUID_CAULDRON));
        CauldronInteraction.EMPTY.put(PMFluids.LIQUID_REDSTONE_BUCKET.asItem(), fillLavaLike(PMFluids.LIQUID_REDSTONE_CAULDRON));
        CauldronInteraction.EMPTY.put(PMFluids.MOLTEN_SALT_BUCKET.asItem(), fillLavaLike(PMFluids.MOLTEN_SALT_CAULDRON));
        CauldronInteraction.EMPTY.put(PMFluids.PSYCHEDELIC_LAVA_BUCKET.asItem(), fillLavaLike(PMFluids.PSYCHEDELIC_LAVA_CAULDRON));
        CauldronInteraction.EMPTY.put(PMFluids.TAR_BUCKET.asItem(), fillWaterLike(PMFluids.TAR_CAULDRON));
        CauldronInteraction.EMPTY.put(PMFluids.DARK_LAVA_BUCKET.asItem(), fillLavaLike(PMFluids.DARK_LAVA_CAULDRON));
        CauldronInteraction.EMPTY.put(PMFluids.MOLTEN_GOLD_BUCKET.asItem(), fillLavaLike(PMFluids.MOLTEN_GOLD_CAULDRON));
        CauldronInteraction.EMPTY.put(PMFluids.MOLTEN_SILVER_BUCKET.asItem(), fillLavaLike(PMFluids.MOLTEN_SILVER_CAULDRON));
        CauldronInteraction.EMPTY.put(PMFluids.MOLTEN_IRON_BUCKET.asItem(), fillLavaLike(PMFluids.MOLTEN_IRON_CAULDRON));
        CauldronInteraction.EMPTY.put(PMFluids.MOLTEN_COPPER_BUCKET.asItem(), fillLavaLike(PMFluids.MOLTEN_COPPER_CAULDRON));
    }
}
