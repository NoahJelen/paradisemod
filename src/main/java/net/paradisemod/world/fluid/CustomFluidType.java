package net.paradisemod.world.fluid;

import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.fluids.FluidType;

public abstract class CustomFluidType extends FluidType {
    public CustomFluidType(Properties properties) { super(properties); }

    public abstract ResourceLocation cauldronTexture();
}