package net.paradisemod.world.fluid;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.GameRules;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.material.FluidState;
import net.minecraftforge.event.ForgeEventFactory;
import net.minecraftforge.fluids.ForgeFlowingFluid;

public class PMFluid {
    public static class Source extends ForgeFlowingFluid.Source {
        private boolean isHot;

        public Source(ForgeFlowingFluid.Properties properties, boolean isHot) {
            super(properties);
            this.isHot = isHot;
        }

        @Override
        protected void randomTick(Level world, BlockPos pos, FluidState fluidState, RandomSource rand) {
            causeFire(world, pos, rand);
        }

        @Override
        protected boolean isRandomlyTicking() {
            return isHot;
        }
    }

    public static class Flowing extends ForgeFlowingFluid.Flowing {
        private boolean isHot;

        public Flowing(ForgeFlowingFluid.Properties properties, boolean isHot) {
            super(properties);
            this.isHot = isHot;
        }

        @Override
        protected void randomTick(Level world, BlockPos pos, FluidState fluidState, RandomSource rand) {
            causeFire(world, pos, rand);
        }

        @Override
        protected boolean isRandomlyTicking() {
            return isHot;
        }
    }

    private static void causeFire(Level world, BlockPos pos, RandomSource rand) {
        if(world.getGameRules().getBoolean(GameRules.RULE_DOFIRETICK)) {
            int i = rand.nextInt(3);

            if(i > 0) {
                var blockpos = pos;

                for(int j = 0; j < i; ++j) {
                    blockpos = blockpos.offset(rand.nextInt(3) - 1, 1, rand.nextInt(3) - 1);
                    if (!world.isLoaded(blockpos)) return;
                }

                var blockstate = world.getBlockState(blockpos);

                if(blockstate.isAir()) {
                    if(hasFlammableNeighbours(world, blockpos)) {
                        world.setBlockAndUpdate(blockpos, ForgeEventFactory.fireFluidPlaceBlockEvent(world, blockpos, pos, Blocks.FIRE.defaultBlockState()));
                        return;
                    }
                }
                else if(blockstate.blocksMotion()) return;
            }
        }
        else {
            for(int k = 0; k < 3; k++) {
                var blockpos1 = pos.offset(rand.nextInt(3) - 1, 0, rand.nextInt(3) - 1);
                if(!world.isLoaded(blockpos1)) return;

                if(world.isEmptyBlock(blockpos1.above()) && isFlammable(world, blockpos1, Direction.UP))
                    world.setBlockAndUpdate(blockpos1.above(), ForgeEventFactory.fireFluidPlaceBlockEvent(world, blockpos1.above(), pos, Blocks.FIRE.defaultBlockState()));
            }
        }
    }

    private static boolean hasFlammableNeighbours(LevelReader pLevel, BlockPos pPos) {
        for(var direction : Direction.values())
            if(isFlammable(pLevel, pPos.relative(direction), direction.getOpposite()))
                return true;

        return false;
    }

    private static boolean isFlammable(LevelReader world, BlockPos pos, Direction face) {
        return pos.getY() >= world.getMinBuildHeight() && pos.getY() < world.getMaxBuildHeight() && !world.hasChunkAt(pos) ? false : world.getBlockState(pos).isFlammable(world, pos, face);
    }
}