package net.paradisemod.world.fluid;

import java.util.List;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.Entity.RemovalReason;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.FlowingFluid;
import net.minecraftforge.common.Tags;
import net.minecraftforge.fluids.ForgeFlowingFluid;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.misc.Misc;

public class PMLiquidBlock extends LiquidBlock implements ModeledBlock {
    @Nullable
    private List<Block> solidSourceBlocks;

    @Nullable
    private List<Block> solidBlocks;

    @Nullable
    private final SoundEvent ambientSound;

    @Nullable
    private final Animator animator;

    @Nullable
    private final EntityCollideEvent collideEvent;

    private final boolean isHot;
    private final boolean hasRedstoneSignal;
    private final RegistryObject<ForgeFlowingFluid> fluid;
    private final boolean translucentRender;
    private int lightLevel;

    private PMLiquidBlock(
        RegistryObject<ForgeFlowingFluid> fluid,
        Block.Properties properties,
        @Nullable List<Supplier<Block>> solidBlocks,
        @Nullable List<Supplier<Block>> solidSourceBlocks,
        @Nullable SoundEvent ambientSound,
        boolean isHot,
        @Nullable Animator animator,
        @Nullable EntityCollideEvent collideEvent,
        int lightLevel,
        boolean hasRedstoneSignal,
        boolean translucentRender
    ) {
        super(fluid, properties);

        if(solidBlocks != null)
            this.solidBlocks = solidBlocks.stream().map(Supplier::get).toList();

        if(solidSourceBlocks != null)
            this.solidSourceBlocks = solidSourceBlocks.stream().map(Supplier::get).toList();

        this.ambientSound = ambientSound;
        this.isHot = isHot;
        this.animator = animator;
        this.collideEvent = collideEvent;
        this.hasRedstoneSignal = hasRedstoneSignal;
        this.fluid = fluid;
        this.translucentRender = translucentRender;
        this.lightLevel = lightLevel;
    }

    @Override
    public void animateTick(BlockState state, Level world, BlockPos pos, RandomSource rand) {
        SoundEvent sound;
        if(ambientSound != null) sound = ambientSound;
        else if(isHot) sound = SoundEvents.LAVA_AMBIENT;
        else sound = SoundEvents.WATER_AMBIENT;
        var fluid = state.getFluidState();

        if(animator != null) animator.run(state, world, pos, rand);
        else {
            if(isHot) {
                var blockpos = pos.above();
                if(world.getBlockState(blockpos).isAir() && !world.getBlockState(blockpos).isSolidRender(world, blockpos)) {
                    if(rand.nextInt(100) == 0) {
                        var x = pos.getX() + rand.nextDouble();
                        var y = pos.getY() + 1;
                        var z = pos.getZ() + rand.nextDouble();
                        world.addParticle(ParticleTypes.LAVA, x, y, z, 0, 0, 0);
                    }
                }
            }
            else if(rand.nextInt(10) == 0)
                world.addParticle(ParticleTypes.UNDERWATER, pos.getX() + rand.nextDouble(), pos.getY() + rand.nextDouble(), pos.getZ() + rand.nextDouble(), 0, 0, 0);
        }

        if(isHot) {
            var blockpos = pos.above();
            if(world.getBlockState(blockpos).isAir() && !world.getBlockState(blockpos).isSolidRender(world, blockpos)) {
                if(rand.nextInt(100) == 0) {
                    var x = pos.getX() + rand.nextDouble();
                    var y = pos.getY() + 1;
                    var z = pos.getZ() + rand.nextDouble();
                    world.playLocalSound(x, y, z, SoundEvents.LAVA_POP, SoundSource.BLOCKS, 0.2F + rand.nextFloat() * 0.2F, 0.9F + rand.nextFloat() * 0.15F, false);
                }

                if(rand.nextInt(200) == 0)
                    world.playLocalSound(pos.getX(), pos.getY(), pos.getZ(), sound, SoundSource.BLOCKS, 0.2F + rand.nextFloat() * 0.2F, 0.9F + rand.nextFloat() * 0.15F, false);
            }
        }
        else if(!fluid.isSource() && !fluid.getValue(FlowingFluid.FALLING) && rand.nextInt(64) == 0)
            world.playLocalSound(pos.getX() + 0.5D, pos.getY() + 0.5D, pos.getZ() + 0.5D, sound, SoundSource.BLOCKS, rand.nextFloat() * 0.25F + 0.75F, rand.nextFloat() + 0.5F, false);

        if(this.fluid.getKey() == PMFluids.PSYCHEDELIC_LAVA.getKey() || this.fluid.getKey() == PMFluids.PSYCHEDELIC_FLUID.getKey())
            world.sendBlockUpdated(pos, state, state, 8);
    }

    @Override
    public void entityInside(BlockState state, Level world, BlockPos pos, Entity entity) {
        if(collideEvent != null)
            collideEvent.process(state, world, pos, entity);

        if(isHot) {
            var rand = world.getRandom();
            if(entity instanceof ItemEntity item) {
                if(item.fireImmune()) return;
                item.remove(RemovalReason.KILLED);
                world.playLocalSound(pos, SoundEvents.FIRE_EXTINGUISH, SoundSource.BLOCKS, rand.nextFloat() * 0.25F + 0.75F, rand.nextFloat() + 0.5F, false);
            }
            else entity.setRemainingFireTicks(300);
        }
    }

    @Override
    public void onPlace(BlockState state, Level world, BlockPos pos, BlockState oldState, boolean isMoving) {
        solidify(world, pos, state);
    }

    @Override
    public void neighborChanged(BlockState state, Level world, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) {
        solidify(world, pos, state);
    }

    @Override
    public int getSignal(BlockState blockState, BlockGetter blockAccess, BlockPos pos, Direction side) { return hasRedstoneSignal ? 15 : 0; }

    @Override
    public void genBlockState(BlockStateGenerator generator) {
        var name = fluid.getKey().location().getPath();
        var model = generator.models()
            .getBuilder("block/fluid/" + name);

        if(translucentRender)
            model = model.renderType("translucent");

        generator.simpleBlock(this, model);
    }

    @Override
    public ForgeFlowingFluid getFluid() {
        return fluid.get();
    }

    public int lightLevel() {
        return lightLevel;
    }

    // turn the fluid into solid blocks if it comes in contact with water
    private void solidify(Level world, BlockPos pos, BlockState curState) {
        var currFluid = world.getFluidState(pos);

        if(solidBlocks == null || solidSourceBlocks == null) {
            if(fluid.getKey() == PMFluids.PSYCHEDELIC_LAVA.getKey()) {
                var sourceTagBlocks = Utils.getBlockTag(PMTags.Blocks.GLOWING_OBSIDIAN);
                var solidTagBlocks = Utils.getBlockTag(Tags.Blocks.STONE);
                solidSourceBlocks = sourceTagBlocks.stream().toList();
                solidBlocks = solidTagBlocks.stream().toList();
            }
            else if(fluid.getKey() == PMFluids.PSYCHEDELIC_FLUID.getKey()) {
                solidSourceBlocks = List.of(Blocks.ICE, Blocks.PACKED_ICE, Blocks.BLUE_ICE, Misc.GLOWING_ICE.get());
                solidBlocks = solidSourceBlocks;
            }
        }

        if(solidBlocks != null || solidSourceBlocks != null) {
            var flowBlock = solidBlocks.get(world.random.nextInt(solidBlocks.size()));
            var sourceBlock = solidSourceBlocks.get(world.random.nextInt(solidSourceBlocks.size()));

            for(var direction : Direction.values()) {
                var waterPos = pos.relative(direction);
                var water = world.getFluidState(waterPos);

                if(water.is(FluidTags.WATER) && isForeignFluid(world, waterPos)) {
                    var placePos = pos.below(direction == Direction.DOWN ? 1 : 0);
                    world.setBlockAndUpdate(placePos, ((currFluid.isSource() && direction != Direction.DOWN) ? sourceBlock : flowBlock).defaultBlockState());
                    if(isHot) fizz(world, placePos);
                    return;
                }
            }
        }

        world.scheduleTick(pos, currFluid.getType(), currFluid.getType().getTickDelay(world));
    }

    private boolean isForeignFluid(Level world, BlockPos pos) {
        return !world.getBlockState(pos).is(this);
    }

    private void fizz(LevelAccessor world, BlockPos pos) {
        world.levelEvent(1501, pos, 0);
    }

    @FunctionalInterface
    public interface Animator {
        void run(BlockState state, Level world, BlockPos pos, RandomSource rand);
    }

    @FunctionalInterface
    public interface EntityCollideEvent {
        void process(BlockState state, Level world, BlockPos pos, Entity entity);
    }

    public static class Builder {
        private final RegistryObject<ForgeFlowingFluid> fluid;
        private final Block fluidToMimic;
        private List<Supplier<Block>> solidSourceBlocks = null;
        private List<Supplier<Block>> solidBlocks = null;
        private SoundEvent ambientSound = null;
        private boolean isHot = false;
        private Animator animator = null;
        private EntityCollideEvent collideEvent = null;
        private int lightLevel = 0;
        private boolean hasRedstoneSignal = false;
        private boolean translucentRender = false;

        public Builder(RegistryObject<ForgeFlowingFluid> fluid, Block fluidToMimic) {
            this.fluid = fluid;
            this.fluidToMimic = fluidToMimic;
        }

        public Builder solidifyBlock(Supplier<Block> block) {
            solidBlocks = List.of(block);
            return this;
        }

        public Builder solidifySourceBlock(Supplier<Block> block) {
            solidSourceBlocks = List.of(block);
            return this;
        }

        public Builder ambientSound(SoundEvent sound) {
            ambientSound = sound;
            return this;
        }

        public Builder isHot(boolean isHot) {
            this.isHot = isHot;
            return this;
        }

        public Builder animator(Animator animator) {
            this.animator = animator;
            return this;
        }

        public Builder collideEvent(EntityCollideEvent event) {
            this.collideEvent = event;
            return this;
        }

        public Builder lightLevel(int lightLevel) {
            this.lightLevel = lightLevel;
            return this;
        }

        public Builder hasRedstoneSignal(boolean hasRedstoneSignal) {
            this.hasRedstoneSignal = hasRedstoneSignal;
            return this;
        }

        public Builder translucentRender() {
            translucentRender = true;
            return this;
        }

        public PMLiquidBlock build() {
            var properties = Block.Properties.copy(fluidToMimic).lightLevel($ -> lightLevel).noCollission().strength(100);
            if(isHot) properties = properties.randomTicks();

            return new PMLiquidBlock(
                fluid,
                properties,
                solidBlocks,
                solidSourceBlocks,
                ambientSound,
                isHot,
                animator,
                collideEvent,
                lightLevel,
                hasRedstoneSignal,
                translucentRender
            );
        }

        protected RegistryObject<ForgeFlowingFluid> getFluid() {
            return fluid;
        }
    }
}