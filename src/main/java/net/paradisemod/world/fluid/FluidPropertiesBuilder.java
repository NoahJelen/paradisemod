package net.paradisemod.world.fluid;

import java.util.function.Consumer;
import java.util.function.Supplier;

import org.joml.Vector3f;

import net.minecraft.client.Camera;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.MoverType;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.client.extensions.common.IClientFluidTypeExtensions;
import net.minecraftforge.common.SoundActions;
import net.minecraftforge.fluids.FluidType;
import net.minecraftforge.fluids.ForgeFlowingFluid;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.Events;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredItem;

@SuppressWarnings("unchecked")
public class FluidPropertiesBuilder {
    public static final DeferredRegister<FluidType> FLUID_TYPES = PMRegistries.createRegistry(ForgeRegistries.Keys.FLUID_TYPES);

    private boolean canBecomeSource = true;
    private int density = 1000;
    private int lightLevel = 0;
    private int temp = 300;
    private int viscosity = 1000;
    private SoundEvent bucketFillSound = SoundEvents.BUCKET_FILL;
    private SoundEvent bucketEmptySound = SoundEvents.BUCKET_EMPTY;
    private int tickRate = 5;
    private final RegistryObject<ForgeFlowingFluid> sourceFluid;
    private final RegistryObject<ForgeFlowingFluid> flowingFluid;
    private final Supplier<LiquidBlock> fluidBlock;
    private final RegisteredItem bucket;
    private boolean rendersLikeLava = false;
    private boolean isHot = false;
    private int tintColor = 0xFFFFFFFF;
    private int fogColor = 0xFFFFFFFF;

    public FluidPropertiesBuilder(RegistryObject<ForgeFlowingFluid> sourceFluid, RegistryObject<ForgeFlowingFluid> flowingFluid, Supplier<? extends Block> fluidBlock, RegisteredItem bucket) {
        this.sourceFluid = sourceFluid;
        this.flowingFluid = flowingFluid;
        this.fluidBlock = (Supplier<LiquidBlock>) fluidBlock;
        this.bucket = bucket;
        PMFluids.COLORED_BUCKETS.put(sourceFluid, bucket);
    }

    public FluidPropertiesBuilder isHot() {
        this.isHot = true;
        return this;
    }

    public FluidPropertiesBuilder canBecomeSource(boolean canBecomeSource) {
        this.canBecomeSource = canBecomeSource;
        return this;
    }

    public FluidPropertiesBuilder density(int density) {
        this.density = density;
        return this;
    }

    public FluidPropertiesBuilder lightLevel(int lightLevel) {
        this.lightLevel = lightLevel;
        return this;
    }

    public FluidPropertiesBuilder temperature(int temp) {
        this.temp = temp;
        return this;
    }

    public FluidPropertiesBuilder viscosity(int viscosity) {
        this.viscosity = viscosity;
        return this;
    }

    public FluidPropertiesBuilder tickRate(int tickRate) {
        this.tickRate = tickRate;
        return this;
    }

    public FluidPropertiesBuilder fillSound(SoundEvent sound) {
        bucketFillSound = sound;
        return this;
    }

    public FluidPropertiesBuilder emptySound(SoundEvent sound) {
        bucketEmptySound = sound;
        return this;
    }

    public FluidPropertiesBuilder rendersLikeLava() {
        this.rendersLikeLava = true;
        return this;
    }

    public FluidPropertiesBuilder tintColor(int tintColor) {
        this.tintColor = tintColor;
        fogColor = tintColor;
        return this;
    }

    public FluidPropertiesBuilder fogColor(int fogColor) {
        this.fogColor = fogColor;
        return this;
    }

    public ForgeFlowingFluid.Properties build() {
        final var name = sourceFluid.getKey().location().getPath();

        return new ForgeFlowingFluid.Properties(
            FLUID_TYPES.register(
                sourceFluid.getKey().location().getPath(),
                () -> new CustomFluidType(
                    FluidType.Properties.create()
                        .canConvertToSource(canBecomeSource)
                        .density(density)
                        .lightLevel(lightLevel)
                        .temperature(temp)
                        .viscosity(viscosity)
                        .sound(SoundActions.BUCKET_EMPTY, bucketEmptySound)
                        .sound(SoundActions.BUCKET_FILL, bucketFillSound)
                        .canSwim(!isHot)
                        .canDrown(!isHot)
                        .supportsBoating(!isHot)
                ) {
                    @Override
                    public boolean canRideVehicleUnder(Entity vehicle, Entity rider) {
                        return !vehicle.dismountsUnderwater() && !isHot;
                    }

                    @Override
                    public boolean move(FluidState state, LivingEntity creature, Vec3 movementVector, double gravity) {
                        if(isHot || state.is(PMFluids.HONEY.get()) || state.is(PMFluids.FLOWING_HONEY.get()) || state.is(PMFluids.TAR.get()) || state.is(PMFluids.FLOWING_TAR.get())) {
                            var moveFactor = 0.5D;
                            var fallMod = 0.8D;
                            var collisionMod = 0.6D;

                            if(state.is(PMFluids.HONEY.get()) || state.is(PMFluids.FLOWING_HONEY.get())) {
                                moveFactor /= 2;
                                fallMod /= 2;
                                collisionMod /= 2;
                            }
                            else if(state.is(PMFluids.TAR.get()) || state.is(PMFluids.FLOWING_TAR.get())) {
                                moveFactor /= 4;
                                fallMod /= 4;
                                collisionMod /= 4;
                            }

                            boolean isFalling = creature.getDeltaMovement().y <= 0;
                            var creatureY = creature.getY();
                            creature.moveRelative(0.02F, movementVector);
                            creature.move(MoverType.SELF, creature.getDeltaMovement());

                            if (creature.getFluidTypeHeight(this) <= creature.getFluidJumpThreshold()) {
                                creature.setDeltaMovement(creature.getDeltaMovement().multiply(moveFactor, fallMod, moveFactor));
                                var vec33 = creature.getFluidFallingAdjustedMovement(gravity, isFalling, creature.getDeltaMovement());
                                creature.setDeltaMovement(vec33);
                            }
                            else creature.setDeltaMovement(creature.getDeltaMovement().scale(moveFactor));

                            if (!creature.isNoGravity())
                                creature.setDeltaMovement(creature.getDeltaMovement().add(0, -gravity / 4, 0));

                            var vec34 = creature.getDeltaMovement();

                            if (creature.horizontalCollision && creature.isFree(vec34.x, vec34.y + collisionMod - creature.getY() + creatureY, vec34.z))
                                creature.setDeltaMovement(vec34.x, collisionMod / 2, vec34.z);

                            return true;
                        }

                        return false;
                    }

                    @Override
                    public double motionScale(Entity entity) {
                        if(sourceFluid.getKey() == PMFluids.HONEY.getKey())
                            return 0.0035D;
                        else if(sourceFluid.getKey() == PMFluids.TAR.getKey())
                            return 0.000875D;
                        else if(isHot)
                            return entity.level().dimensionType().ultraWarm() ? 0.007D : 0.0023333333333333335D;
                        else return 0.014D;
                    }

                    @Override
                    public void initializeClient(Consumer<IClientFluidTypeExtensions> consumer) {
                        consumer.accept(new FluidRenderer(name, rendersLikeLava, tintColor, fogColor));
                    }

                    @Override
                    public ResourceLocation cauldronTexture() {
                        if(rendersLikeLava)
                            return new ResourceLocation(ParadiseMod.ID, "block/fluid/lava_like");
                        else return new ResourceLocation(ParadiseMod.ID, "block/fluid/" + name);
                    }
                }
            ),
            sourceFluid,
            flowingFluid
        )
            .block(fluidBlock)
            .bucket(bucket)
            .slopeFindDistance(8)
            .levelDecreasePerBlock(1)
            .tickRate(tickRate);
    }

    private record FluidRenderer(String name, boolean rendersLikeLava, int tintColor, int fogColor) implements IClientFluidTypeExtensions {
        @Override
        public ResourceLocation getStillTexture() {
            if(rendersLikeLava)
                return new ResourceLocation(ParadiseMod.ID, "block/fluid/lava_like");
            else return new ResourceLocation(ParadiseMod.ID, "block/fluid/" + name);
        }

        @Override
        public ResourceLocation getFlowingTexture() {
            if(rendersLikeLava)
                return new ResourceLocation(ParadiseMod.ID, "block/fluid/flowing_lava_like");
            else return new ResourceLocation(ParadiseMod.ID, "block/fluid/flowing_" + name);
        }

        @Override
        public int getTintColor() {
            if(isPsychedelic())
                return Events.RAINBOW_COLOR.color();
            else return tintColor;
        }

        @Override
        public Vector3f modifyFogColor(Camera camera, float partialTick, ClientLevel level, int renderDistance, float darkenWorldAmount, Vector3f fluidFogColor) {
            int color = fogColor;

            if(isPsychedelic())
                color = Events.RAINBOW_COLOR.color();

            return new Vector3f((color >> 16 & 0xFF) / 255F, (color >> 8 & 0xFF) / 255F, (color & 0xFF) / 255F);
        }

        private boolean isPsychedelic() {
            return name.startsWith("psychedelic");
        }
    }
}