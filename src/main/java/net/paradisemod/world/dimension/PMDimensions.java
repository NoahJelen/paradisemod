package net.paradisemod.world.dimension;

import java.util.OptionalLong;
import java.util.function.Supplier;

import com.google.common.collect.ImmutableSet;

import net.minecraft.core.HolderGetter;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.core.particles.SimpleParticleType;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.entity.ai.village.poi.PoiType;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.dimension.BuiltinDimensionTypes;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.dimension.DimensionType.MonsterSettings;
import net.minecraft.world.level.dimension.LevelStem;
import net.minecraft.world.level.levelgen.NoiseBasedChunkGenerator;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.misc.Misc;
import net.paradisemod.world.Ores;
import net.paradisemod.world.PMWorld;
import net.paradisemod.world.biome.PMBiomeSource;

public class PMDimensions {
    private static final DeferredRegister<PoiType> POI_TYPES = PMRegistries.createRegistry(ForgeRegistries.POI_TYPES);

    // point of interest types for the portals
    public static final RegistryObject<PoiType> OWC_POI = POI_TYPES.register("owc_poi", () -> new PoiType(ImmutableSet.of(PMWorld.OVERWORLD_CORE_PORTAL.get().defaultBlockState()), 0, 1));
    public static final RegistryObject<PoiType> DEEP_DARK_POI = POI_TYPES.register("deep_dark_poi", () -> new PoiType(ImmutableSet.of(PMWorld.DEEP_DARK_PORTAL.get().defaultBlockState()), 0, 1));
    public static final RegistryObject<PoiType> ELYSIUM_POI = POI_TYPES.register("elysium_poi", () -> new PoiType(ImmutableSet.of(PMWorld.ELYSIUM_PORTAL.get().defaultBlockState()), 0, 1));

    public static void init(IEventBus eventbus) {
        POI_TYPES.register(eventbus);
        eventbus.addListener(DimensionEffects::regEffects);

        // register the biome source
        var biomeSourceRegistry = PMRegistries.createRegistry(Registries.BIOME_SOURCE);
        biomeSourceRegistry.register("dimension_biomes", () -> PMBiomeSource.CODEC);
        biomeSourceRegistry.register(eventbus);
    }

    public static void buildDimensionTypes(BootstapContext<DimensionType> context) {
        ParadiseMod.LOG.debug("Generating dimension types...");
        for(var dimType: Type.values())
            context.register(
                PMRegistries.createModResourceKey(Registries.DIMENSION_TYPE, dimType.getName()),
                dimType.buildDimType()
            );
    }

    public static void buildDimensions(BootstapContext<LevelStem> context) {
        ParadiseMod.LOG.debug("Generating dimensions...");
        var dimTypeGetter = context.lookup(Registries.DIMENSION_TYPE);
        var noiseSettingsGetter = context.lookup(Registries.NOISE_SETTINGS);
        for(var dimType: Type.values())
            context.register(
                PMRegistries.createModResourceKey(Registries.LEVEL_STEM, dimType.getName()),
                dimType.buildDimension(dimTypeGetter, noiseSettingsGetter)
            );
    }

    public static boolean isDimOverworldLike(WorldGenLevel world) {
        var dim = world.getLevel().dimension();
        return dim == Level.OVERWORLD ||
            PMDimensions.Type.OVERWORLD_CORE.is(dim) ||
            PMDimensions.Type.ELYSIUM.is(dim);
    }

    public enum Type {
        OVERWORLD_CORE(ParticleTypes.CRIT, Ores.BLAZE_BLOCK, OWC_POI),
        DEEP_DARK(ParticleTypes.SCULK_SOUL, Ores.ENDERITE_BLOCK, DEEP_DARK_POI),
        ELYSIUM(ParticleTypes.TOTEM_OF_UNDYING, () -> Blocks.GLOWSTONE, ELYSIUM_POI);

        private final ResourceLocation key;
        private final SimpleParticleType portalParticles;
        private final Supplier<Block> frame;
        private final RegistryObject<PoiType> poiType;

        Type(SimpleParticleType portalParticles, Supplier<Block> frame, RegistryObject<PoiType> poiType) {
            key = new ResourceLocation("paradisemod:" + name().toLowerCase());
            this.portalParticles = portalParticles;
            this.frame = frame;
            this.poiType = poiType;
        }

        public boolean is(ResourceKey<Level> dim) {
            return dim == getKey();
        }

        public ResourceKey<Level> getKey() { return ResourceKey.create(Registries.DIMENSION, key); }
        public ResourceKey<LevelStem> getLevelStemKey() { return ResourceKey.create(Registries.LEVEL_STEM, key); }
        public SimpleParticleType getPortalParticles() { return portalParticles; }
        public Block getFrameBlock() { return frame.get(); }
        public ResourceKey<PoiType> getPointOfInterestType() { return poiType.getKey(); }
        public String getName() { return name().toLowerCase(); }

        public ItemLike portalCraftItem() {
            return switch(this) {
                case OVERWORLD_CORE -> Items.BLAZE_POWDER;
                case ELYSIUM -> Items.GLOWSTONE_DUST;
                case DEEP_DARK -> Misc.ENDERITE_INGOT;
            };
        }

        private DimensionType buildDimType() {
            var effectsLoc = this == ELYSIUM ? BuiltinDimensionTypes.OVERWORLD_EFFECTS : new ResourceLocation(ParadiseMod.ID, getName());
            var coordScale = this == OVERWORLD_CORE ? 8 : 4;

            return new DimensionType(
                this == ELYSIUM ? OptionalLong.empty() : OptionalLong.of(18000),
                this == ELYSIUM,
                this == OVERWORLD_CORE,
                false,
                true,
                coordScale,
                true,
                false,
                -64,
                384,
                384,
                BlockTags.INFINIBURN_OVERWORLD,
                effectsLoc,
                0,
                new MonsterSettings(
                    this == OVERWORLD_CORE,
                    this != DEEP_DARK,
                    UniformInt.of(0, 7),
                    0
                )
            );
        }

        private LevelStem buildDimension(HolderGetter<DimensionType> dimTypeGetter, HolderGetter<NoiseGeneratorSettings> noiseSettingsGetter) {
            var dimType = dimTypeGetter.getOrThrow(PMRegistries.createModResourceKey(Registries.DIMENSION_TYPE, getName()));
            var noiseSettings = noiseSettingsGetter.getOrThrow(PMRegistries.createModResourceKey(Registries.NOISE_SETTINGS, getName()));

            return new LevelStem(
                dimType,
                new NoiseBasedChunkGenerator(new PMBiomeSource(getName()), noiseSettings)
            );
        }

        public String localizedName(boolean spanish) {
            return switch(this) {
                case OVERWORLD_CORE -> spanish ? "centro del sobremundo" : "Overworld Core";
                case DEEP_DARK -> spanish ? "oscuro profundo" : "Deep Dark";
                case ELYSIUM -> spanish ? "elíseo" : "Elysium";
            };
        }
    }
}