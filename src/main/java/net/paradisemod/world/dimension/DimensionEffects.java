package net.paradisemod.world.dimension;

import net.minecraft.client.renderer.DimensionSpecialEffects;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.client.event.RegisterDimensionSpecialEffectsEvent;

public class DimensionEffects extends DimensionSpecialEffects {
    DimensionEffects(float cloudLevel, boolean hasGround, DimensionSpecialEffects.SkyType skyType) {
        super(cloudLevel, hasGround, skyType, false, false);
    }

    // returns the fog color
    @Override
    public Vec3 getBrightnessDependentFogColor(Vec3 fogColor, float brightness) {
        return fogColor;
    }

    // is it foggy at the specified position?
    @Override
    public boolean isFoggyAt(int x, int z) { return true; }

    public static void regEffects(RegisterDimensionSpecialEffectsEvent event) {
        event.register(new ResourceLocation("paradisemod:overworld_core"), new DimensionEffects(128, true, SkyType.NONE));
        event.register(new ResourceLocation("paradisemod:deep_dark"), new DimensionEffects(Float.NaN, false, SkyType.NONE));
    }
}
