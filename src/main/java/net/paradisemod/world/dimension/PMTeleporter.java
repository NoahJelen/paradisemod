package net.paradisemod.world.dimension;

import java.util.Comparator;
import java.util.Optional;
import java.util.function.Function;

import org.jetbrains.annotations.Nullable;

import net.minecraft.BlockUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.TicketType;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.ai.village.poi.PoiManager;
import net.minecraft.world.entity.ai.village.poi.PoiRecord;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.portal.PortalInfo;
import net.minecraft.world.level.portal.PortalShape;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.util.ITeleporter;
import net.paradisemod.world.blocks.PMPortalBlock;

public class PMTeleporter implements ITeleporter {
    private final PMDimensions.Type dimension;
    private final ServerLevel world;
    private final PMPortalBlock portal;
    private final Direction.Axis portalAxis;

    public PMTeleporter(ServerLevel world, PMDimensions.Type dimension, PMPortalBlock portal, Direction.Axis portalAxis) {
        this.dimension = dimension;
        this.world = world;
        this.portal = portal;
        this.portalAxis = portalAxis;
    }

    public Optional<BlockUtil.FoundRectangle> getExistingPortal(BlockPos pos) {
        var poiManager = world.getPoiManager();
        poiManager.ensureLoadedAndValid(world, pos, 128);
        var optional = poiManager.getInSquare(
            poiType ->
                poiType.is(dimension.getPointOfInterestType()), pos, 128, PoiManager.Occupancy.ANY
        )
            .sorted(
                Comparator.<PoiRecord>comparingDouble(poi -> poi.getPos().distSqr(pos))
                    .thenComparingInt(poi -> poi.getPos().getY())
            )
            .filter(poi -> world.getBlockState(poi.getPos()).is(portal))
            .findFirst();

        return optional.map(
            poi -> {
                var blockpos = poi.getPos();
                world.getChunkSource().addRegionTicket(TicketType.PORTAL, new ChunkPos(blockpos), 3, blockpos);
                var blockstate = world.getBlockState(blockpos);
                return BlockUtil.getLargestRectangleAround(blockpos, portalAxis, 21, Direction.Axis.Y, 21,
                    posIn -> world.getBlockState(posIn) == blockstate
                );
            }
        );
    }

    public Optional<BlockUtil.FoundRectangle> makePortal(BlockPos pos) {
        var direction = Direction.get(Direction.AxisDirection.POSITIVE, portalAxis);
        var d0 = -1D;
        BlockPos blockpos = null;
        var d1 = -1D;
        BlockPos blockpos1 = null;
        var border = world.getWorldBorder();
        int dimensionLogicalHeight = world.getHeight() - 1;
        var mutablePos = pos.mutable();

        for (var mutablePos1 : BlockPos.spiralAround(pos, 16, Direction.EAST, Direction.SOUTH)) {
            int j = Math.min(dimensionLogicalHeight, world.getHeight(Heightmap.Types.MOTION_BLOCKING, mutablePos1.getX(), mutablePos1.getZ()));
            if (border.isWithinBounds(mutablePos1) && border.isWithinBounds(mutablePos1.move(direction, 1))) {
                mutablePos1.move(direction.getOpposite(), 1);

                for (int l = j; l >= 0; l--) {
                    mutablePos1.setY(l);
                    if (world.isEmptyBlock(mutablePos1)) {
                        int i1;
                        for (i1 = l; l > 0 && world.isEmptyBlock(mutablePos1.move(Direction.DOWN)); l--) { }

                        if (l + 4 <= dimensionLogicalHeight) {
                            int j1 = i1 - l;
                            if (j1 <= 0 || j1 >= 3) {
                                mutablePos1.setY(l);
                                if (checkRegionForPlacement(mutablePos1, mutablePos, direction, 0)) {
                                    var d2 = pos.distSqr(mutablePos1);
                                    if (checkRegionForPlacement(mutablePos1, mutablePos, direction, -1) && checkRegionForPlacement(mutablePos1, mutablePos, direction, 1) && (d0 == -1.0D || d0 > d2)) {
                                        d0 = d2;
                                        blockpos = mutablePos1.immutable();
                                    }

                                    if (d0 == -1D && (d1 == -1D || d1 > d2)) {
                                        d1 = d2;
                                        blockpos1 = mutablePos1.immutable();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (d0 == -1D && d1 != -1D) {
            blockpos = blockpos1;
            d0 = d1;
        }

        if (d0 == -1D) {
            blockpos = pos.atY(Mth.clamp(pos.getY(), 70, world.getHeight() - 10));
            var direction1 = direction.getClockWise();
            if (!border.isWithinBounds(blockpos))
                return Optional.empty();

            for (int l1 = -1; l1 < 2; ++l1)
                for (int k2 = 0; k2 < 2; ++k2)
                    for (int i3 = -1; i3 < 3; ++i3) {
                        var blockstate1 = i3 < 0 ? dimension.getFrameBlock().defaultBlockState() : Blocks.AIR.defaultBlockState();
                        mutablePos.setWithOffset(blockpos, k2 * direction.getStepX() + l1 * direction1.getStepX(), i3, k2 * direction.getStepZ() + l1 * direction1.getStepZ());
                        world.setBlockAndUpdate(mutablePos, blockstate1);
                    }

        }

        for (int k1 = -1; k1 < 3; ++k1)
            for (int i2 = -1; i2 < 4; ++i2)
                if (k1 == -1 || k1 == 2 || i2 == -1 || i2 == 3) {
                    mutablePos.setWithOffset(blockpos, k1 * direction.getStepX(), i2, k1 * direction.getStepZ());
                    world.setBlock(mutablePos, dimension.getFrameBlock().defaultBlockState(), 3);
                }

        for (int j2 = 0; j2 < 2; ++j2)
            for (int l2 = 0; l2 < 3; ++l2) {
                mutablePos.setWithOffset(blockpos, j2 * direction.getStepX(), l2, j2 * direction.getStepZ());
                world.setBlock(mutablePos, portal.defaultBlockState(), 18);
            }

        return Optional.of(new BlockUtil.FoundRectangle(blockpos.immutable(), 2, 3));
    }

    private boolean checkRegionForPlacement(BlockPos originalPos, BlockPos.MutableBlockPos offsetPos, Direction directionIn, int offsetScale) {
        var direction = directionIn.getClockWise();

        for (int i = -1; i < 3; i++)
            for (int j = -1; j < 4; j++) {
                offsetPos.setWithOffset(originalPos, directionIn.getStepX() * i + direction.getStepX() * offsetScale, j, directionIn.getStepZ() * i + direction.getStepZ() * offsetScale);
                if (j < 0 && !world.getBlockState(offsetPos).isSolid())
                    return false;

                if (j >= 0 && !world.isEmptyBlock(offsetPos))
                    return false;
            }

        return true;
    }

    @Nullable
    @Override
    public PortalInfo getPortalInfo(Entity entity, ServerLevel destWorld, Function<ServerLevel, PortalInfo> defaultPortalInfo) {
        var inDim = destWorld.dimension() == dimension.getKey();
        var entityWorld = entity.level();

        if (entityWorld.dimension() != dimension.getKey() && !inDim) return null;
        else {
            var border = destWorld.getWorldBorder();
            var minX = Math.max(-2.9999872E7D, border.getMinX() + 16);
            var minZ = Math.max(-2.9999872E7D, border.getMinZ() + 16);
            var maxX = Math.min(2.9999872E7D, border.getMaxX() - 16);
            var maxZ = Math.min(2.9999872E7D, border.getMaxZ() - 16);
            var coordinateDifference = DimensionType.getTeleportationScale(entityWorld.dimensionType(), destWorld.dimensionType());
            var blockpos = new BlockPos((int) Mth.clamp(entity.getX() * coordinateDifference, minX, maxX), (int) entity.getY(), (int) Mth.clamp(entity.getZ() * coordinateDifference, minZ, maxZ));
            return getOrMakePortal(blockpos)
                .map(
                    result -> {
                        var blockstate = entityWorld.getBlockState(entity.portalEntrancePos);
                        Vec3 portalPos;
                        if (blockstate.is(portal)) {
                            var rectangle = BlockUtil.getLargestRectangleAround(entity.portalEntrancePos, portalAxis, 21, Direction.Axis.Y, 21, (pos) -> entityWorld.getBlockState(pos) == blockstate);
                            portalPos = entity.getRelativePortalPosition(portalAxis, rectangle);
                        }
                        else portalPos = new Vec3(0.5D, 0, 0);

                        return PortalShape.createPortalInfo(destWorld, result, portalAxis, portalPos, entity, entity.getDeltaMovement(), entity.getYRot(), entity.getXRot());
                    }
                )
                .orElse(null);
        }
    }

    protected Optional<BlockUtil.FoundRectangle> getOrMakePortal(BlockPos pos) {
        Optional<BlockUtil.FoundRectangle> existingPortal = getExistingPortal(pos);
        if (existingPortal.isPresent()) return existingPortal;
        else return makePortal(pos);
    }
}