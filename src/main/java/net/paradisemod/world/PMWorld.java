package net.paradisemod.world;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;
import java.util.function.Supplier;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SoundType;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.client.model.generators.ModelFile.UncheckedModelFile;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.Tags;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.Events;
import net.paradisemod.base.Utils;
import net.paradisemod.base.blocks.CustomPlant;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.redstone.Buttons;
import net.paradisemod.world.biome.PMBiomes;
import net.paradisemod.world.blocks.LargeCaveFormation;
import net.paradisemod.world.blocks.PMPortalBlock;
import net.paradisemod.world.blocks.PoweredAir;
import net.paradisemod.world.blocks.ShatteredRedstoneBlock;
import net.paradisemod.world.blocks.SmallCaveFormation;
import net.paradisemod.world.blocks.SpreadableBlock;
import net.paradisemod.world.dimension.PMDimensions;
import net.paradisemod.world.fluid.PMFluids;
import net.paradisemod.worldgen.PMBiomeModifier;
import net.paradisemod.worldgen.carver.PMCarvers;
import net.paradisemod.worldgen.features.PMFeatures;
import net.paradisemod.worldgen.structures.PMStructures;
import net.paradisemod.worldgen.surfacerules.PMBandlands;
import net.paradisemod.worldgen.surfacerules.PMSurfaceRules;
import terrablender.api.SurfaceRuleManager;

public class PMWorld {
    // small cave formations
    public static final RegisteredBlock ANDESITE_FORMATION = regSmallFormation("andesite");
    public static final RegisteredBlock BASALT_FORMATION = regSmallFormation("basalt", SoundType.BASALT);
    public static final RegisteredBlock BLUE_ICICLE = regSmallFormation("blue_icicle", SoundType.GLASS);
    public static final RegisteredBlock DARKSTONE_FORMATION = regSmallFormation("darkstone");
    public static final RegisteredBlock DEEPSLATE_FORMATION = regSmallFormation("deepslate", SoundType.DEEPSLATE);
    public static final RegisteredBlock TUFF_FORMATION = regSmallFormation("tuff", SoundType.TUFF);
    public static final RegisteredBlock DIORITE_FORMATION = regSmallFormation("diorite");
    public static final RegisteredBlock GRANITE_FORMATION = regSmallFormation("granite");
    public static final RegisteredBlock ICICLE = regSmallFormation("icicle", SoundType.GLASS);
    public static final RegisteredBlock MOSSY_COBBLESTONE_FORMATION = regSmallFormation("mossy_cobblestone");
    public static final RegisteredBlock NETHERRACK_FORMATION = regSmallFormation("netherrack", SoundType.NETHERRACK);
    public static final RegisteredBlock RED_SANDSTONE_FORMATION = regSmallFormation("red_sandstone");
    public static final RegisteredBlock SANDSTONE_FORMATION = regSmallFormation("sandstone");
    public static final RegisteredBlock BLACKENED_SANDSTONE_FORMATION = regSmallFormation("blackened_sandstone");
    public static final RegisteredBlock STONE_FORMATION = regSmallFormation("stone");
    public static final RegisteredBlock END_STONE_FORMATION = regSmallFormation("end_stone");

    // large cave formations
    public static final RegisteredBlock LARGE_ANDESITE_FORMATION = regLargeFormation("andesite", Blocks.ANDESITE);
    public static final RegisteredBlock LARGE_BASALT_FORMATION = regLargeFormation("basalt", Blocks.BASALT);
    public static final RegisteredBlock LARGE_BLUE_ICICLE = regLargeFormation("blue_icicle", Blocks.BLUE_ICE);
    public static final RegisteredBlock LARGE_DARKSTONE_FORMATION = regLargeFormation("darkstone", DeepDarkBlocks.DARKSTONE);
    public static final RegisteredBlock LARGE_DEEPSLATE_FORMATION = regLargeFormation("deepslate", Blocks.DEEPSLATE);
    public static final RegisteredBlock LARGE_TUFF_FORMATION = regLargeFormation("tuff", Blocks.TUFF);
    public static final RegisteredBlock LARGE_DIORITE_FORMATION = regLargeFormation("diorite", Blocks.DIORITE);
    public static final RegisteredBlock LARGE_GRANITE_FORMATION = regLargeFormation("granite", Blocks.GRANITE);
    public static final RegisteredBlock LARGE_ICICLE = regLargeFormation("icicle", Blocks.ICE);
    public static final RegisteredBlock LARGE_MOSSY_COBBLESTONE_FORMATION = regLargeFormation("mossy_cobblestone", Blocks.MOSSY_COBBLESTONE);
    public static final RegisteredBlock LARGE_NETHERRACK_FORMATION = regLargeFormation("netherrack", Blocks.NETHERRACK);
    public static final RegisteredBlock LARGE_RED_SANDSTONE_FORMATION = regLargeFormation("red_sandstone", Blocks.RED_SANDSTONE);
    public static final RegisteredBlock LARGE_SANDSTONE_FORMATION = regLargeFormation("sandstone", Blocks.SANDSTONE);
    public static final RegisteredBlock LARGE_BLACKENED_SANDSTONE_FORMATION = regLargeFormation("blackened_sandstone", DeepDarkBlocks.BLACKENED_SANDSTONE);
    public static final RegisteredBlock LARGE_STONE_FORMATION = regLargeFormation("stone", Blocks.STONE);
    public static final RegisteredBlock LARGE_END_STONE_FORMATION = regLargeFormation("end_stone", Blocks.END_STONE);

    // shattered redstone block
    public static final RegisteredBlock SHATTERED_REDSTONE_BLOCK = PMRegistries.regBlockItem(
        "shattered_redstone_block",
        ShatteredRedstoneBlock::new
    )
        .tab(CreativeModeTabs.BUILDING_BLOCKS)
        .tags(
            BlockTags.MINEABLE_WITH_PICKAXE,
            BlockTags.NEEDS_IRON_TOOL
        )
        .localizedName("Shattered Redstone Block", "Bloque destruido de piedra roja");

    public static final RegisteredBlock POWERED_AIR = PMRegistries.regBlock("powered_air", PoweredAir::new)
        .blockStateGenerator((block, generator) -> generator.simpleBlock(block, generator.existingMCModel("air")))
        .noDrops();

    // end grassland foliage
    public static final RegisteredBlock OVERGROWN_END_STONE = PMRegistries.regBlockItem(
        "overgrown_end_stone",
        () -> new SpreadableBlock(
            Blocks.END_STONE,
            PMTags.Blocks.END_FOLIAGE,
            Block.Properties.copy(Blocks.END_STONE)
        )
    )
        .tab(CreativeModeTabs.NATURAL_BLOCKS)
        .tags(
            BlockTags.MINEABLE_WITH_PICKAXE,
            Tags.Blocks.END_STONES,
            BlockTags.DRAGON_IMMUNE,
            PMTags.Blocks.GROUND_BLOCKS
        )
        .blockStateGenerator((block, generator) -> generator.grassBlockLike(block, "minecraft:block/end_stone", false))
        .lootTable((block, generator) -> generator.dropUnlessSilkTouch(block, Blocks.END_STONE))
        .localizedName("Overgrown End Stone", "Piedra sobrecrecida del Fin");

    public static final RegisteredBlock END_GRASS = PMRegistries.regBlockItem(
        "end_grass",
        () -> new CustomPlant(false, true, CustomPlant.Type.END)
    )
        .tab(CreativeModeTabs.NATURAL_BLOCKS)
        .tags(
            BlockTags.REPLACEABLE_BY_TREES,
            PMTags.Blocks.END_FOLIAGE
        )
        .blockStateGenerator((block, generator) -> generator.tintedGrass(block))
        .localizedName("Ender Grass", "Pasto del Fin");

    //TODO: this can be a true 2 block tall plant
    public static final RegisteredBlock TALL_END_GRASS = PMRegistries.regBlockItem(
        "tall_end_grass",
        () -> new CustomPlant(true, true, CustomPlant.Type.END)
    )
        .tab(CreativeModeTabs.NATURAL_BLOCKS)
        .tags(
            BlockTags.REPLACEABLE_BY_TREES,
            PMTags.Blocks.END_FOLIAGE
        )
        .blockStateGenerator((block, generator) -> generator.simpleBlock(block, generator.existingModel("tall_end_grass")))
        .itemModel(
            (block, generator) -> generator.getBuilder("item/tall_end_grass")
                .parent(new UncheckedModelFile("minecraft:item/generated"))
                .texture("layer0", generator.mcLoc("block/tall_grass_top"))
        )
        .localizedName("Tall Ender Grass", "Pasto alto del Fin");

    // portals
    public static final RegisteredBlock OVERWORLD_CORE_PORTAL = regPortalBlock(PMDimensions.Type.OVERWORLD_CORE);
    public static final RegisteredBlock DEEP_DARK_PORTAL = regPortalBlock(PMDimensions.Type.DEEP_DARK);
    public static final RegisteredBlock ELYSIUM_PORTAL = regPortalBlock(PMDimensions.Type.ELYSIUM);

    public static void init(IEventBus eventbus) {
        // submodules
        PMDimensions.init(eventbus);
        PMFluids.init(eventbus);
        PMStructures.init(eventbus);
        PMFeatures.init(eventbus);
        PMCarvers.init(eventbus);
        PMBiomeModifier.init(eventbus);
        PMBiomes.init(eventbus);
        DeepDarkBlocks.init(eventbus);
        CrystalClusters.init();
        Ores.init();

        eventbus.addListener((FMLCommonSetupEvent event) -> {
            Registry.register(BuiltInRegistries.MATERIAL_RULE, new ResourceLocation("paradisemod:bandlands"), PMBandlands.CODEC.codec());
            event.enqueueWork(
                () -> SurfaceRuleManager.addSurfaceRules(SurfaceRuleManager.RuleCategory.OVERWORLD, ParadiseMod.ID, PMSurfaceRules.buildOverworldRules())
            );
        });

        MinecraftForge.EVENT_BUS.register(PMWorldEvents.class);
        Events.registerBasicColoredBlocks(0x21746A, END_GRASS, TALL_END_GRASS);
        ParadiseMod.LOG.info("Loaded World module");
    }

    public static void initClient(FMLClientSetupEvent event) {
        PMFluids.initClient();
    }

    public static OptionalInt getGroundLevel(LevelAccessor world, int minY, int maxY, BlockPos pos, boolean isUnderwater, Block... groundBlocks) {
        int y;

        for (y = maxY; y > minY; y--) {
            var newPos = pos.atY(y);
            var block = world.getBlockState(newPos);
            var blockAbove = world.getBlockState(newPos.above());
            if (
                (isUnderwater && List.of(groundBlocks).contains(block.getBlock()) && blockAbove.is(Blocks.WATER)) ||
                (
                    List.of(groundBlocks).contains(block.getBlock()) &&
                    (
                        !blockAbove.isSolid() ||
                        blockAbove.is(BlockTags.LOGS)
                    )
                )
            )
                return OptionalInt.of(y);
        }

        return OptionalInt.empty();
    }

    public static OptionalInt getGroundLevel(LevelAccessor world, int minY, int maxY, BlockPos pos, Block... groundBlocks) { return getGroundLevel(world, minY, maxY, pos, false, groundBlocks); }

    public static OptionalInt getGroundLevel(LevelAccessor world, int minY, int maxY, BlockPos pos) {
        int y;

        for (y = maxY; y > minY; y--) {
            var newPos = pos.atY(y);
            var block = world.getBlockState(newPos);
            var blockAbove = world.getBlockState(newPos.above());
            if(
                block.is(PMTags.Blocks.GROUND_BLOCKS) &&
                (
                    !blockAbove.isSolid() ||
                    blockAbove.is(BlockTags.LOGS) ||
                    blockAbove.is(Decoration.PRICKLY_PEAR.get())
                )
            )
                return OptionalInt.of(y);
        }

        return OptionalInt.empty();
    }

    public static int getLowestY(LevelAccessor world, BlockPos pos1, BlockPos pos2) {
        var deltaX = Math.abs(pos2.getX() - pos1.getX());
        var deltaZ = Math.abs(pos2.getZ() - pos1.getZ());
        ArrayList<Integer> yVals = new ArrayList<>();
        for(int x = 0; x < deltaX; x++)
            for(int z = 0; z < deltaZ; z++)
                yVals.add(getGroundLevel(world, world.getMinBuildHeight(), world.getMaxBuildHeight(), pos1.offset(x, 0, z)).orElse(-64));

        return Utils.getMinValue(yVals);
    }

    public static int getHighestY(LevelAccessor world, BlockPos pos1, BlockPos pos2) {
        var deltaX = Math.abs(pos2.getX() - pos1.getX());
        var deltaZ = Math.abs(pos2.getZ() - pos1.getZ());
        ArrayList<Integer> yVals = new ArrayList<>();
        for(var x = 0; x < deltaX; x++)
            for(var z = 0; z < deltaZ; z++)
                yVals.add(getGroundLevel(world, world.getMinBuildHeight(), world.getMaxBuildHeight(), pos1.offset(x, 0, z)).orElse(-64));

        return Utils.getMaxValue(yVals);
    }

    /** checks if a block should not be replaced by a worldgen feature */
    public static boolean doNotReplace(LevelAccessor world, BlockPos pos) {
        return world.getBlockState(pos).is(BlockTags.FEATURES_CANNOT_REPLACE);
    }

    /** updates blocks in a given area (like fences connecting to each other) */
    public static void updateBlockStates(LevelAccessor world, BlockPos pos, int sizeX, int sizeY, int sizeZ) {
        for(var x = 0; x <= sizeX; x++)
            for(var z = 0; z <= sizeZ; z++)
                for(var y = 0; y <= sizeY; y++) {
                    var newPos = pos.offset(x, y, z);
                    var curState = world.getBlockState(newPos);
                    curState.updateNeighbourShapes(world, newPos, 32);
                }
    }

    public static boolean isFiller(LevelAccessor world, BlockPos pos, boolean airOnly) {
        if(world.getBlockState(pos).is(Blocks.LAVA) || world.getBlockState(pos).is(Blocks.LADDER)) return false;
        else if (airOnly && world.getBlockState(pos).isAir()) {
            for(var facing : Direction.values())
                if(!(world.getBlockState(pos.relative(facing)).isAir()))
                    return false;

            return true;
        }
        else if(!world.getBlockState(pos).canOcclude()) {
            for(var facing : Direction.values())
                if(world.getBlockState(pos.relative(facing)).canOcclude())
                    return false;

            return true;
        }

        return false;
    }

    public static boolean isBlockExposed(LevelAccessor world, BlockPos pos) {
        for(var direction : Direction.values()) {
            var curState = world.getBlockState(pos.relative(direction));
            if(curState.isAir() || world.isWaterAt(pos.relative(direction))) return true;
        }

        return false;
    }

    public static boolean isBlockInWall(LevelAccessor world, BlockPos pos) {
        var vertFacings = 0;
        var horizFacings = 0;

        for(var direction : Direction.values()) {
            var curState = world.getBlockState(pos.relative(direction));
            var isSolid = curState.isSolid();
            if(direction.getAxis() == Direction.Axis.Y && isSolid)
                vertFacings++;
            else if(isSolid)
                horizFacings++;
        }

        return horizFacings >= 1 && horizFacings <= 3 && vertFacings >= 1;
    }

    private static RegisteredBlock regPortalBlock(PMDimensions.Type dimType) {
        return PMRegistries.regBlockItem(dimType.getName() + "_portal", () -> new PMPortalBlock(dimType))
            .tab(CreativeModeTabs.FUNCTIONAL_BLOCKS)
            .renderType("translucent")
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.BUILDING_BLOCKS, item, 6)
                    .pattern("iii")
                    .pattern("ipi")
                    .pattern("iii")
                    .define('i', dimType.portalCraftItem())
                    .define('p', Items.ENDER_PEARL)
            )
            .localizedName(
                dimType.localizedName(false) + " Portal",
                "Portal de " + dimType.localizedName(true)
            );
    }

    private static RegisteredBlock regLargeFormation(String name, Supplier<Block> matchingBlock) {
        var formationName = "large_" + (name.contains("icicle") ? name : name + "_formation");

        var englishName = switch(name) {
            case "icicle" -> "Large Icicle";
            case "blue_icicle" -> "Large Blue Icicle";
            default -> "Large " + Utils.localizedMaterialName(name, false) + " Cave Formation";
        };

        var spanishName = switch(name) {
            case "icicle" -> "Carámbano grande";
            case "blue_icicle" -> "Carámbano azul grande";
            default -> "Formación grande de cueva de " + Utils.localizedMaterialName(name, true);
        };

        return PMRegistries.regBlockItem(
            formationName,
            () -> new LargeCaveFormation(matchingBlock.get())
        )
            .tab(name == "darkstone" ? DeepDarkBlocks.DEEP_DARK_TAB.key() : CreativeModeTabs.NATURAL_BLOCKS)
            .itemModel((block, generator) ->
                generator.withExistingParent(formationName, new ResourceLocation("paradisemod:block/large_formation/" + name + "_large"))
            )
            .blockStateGenerator(
                (block, generator) -> {
                    var modelBuilder = generator.models();

                    var small = modelBuilder
                        .withExistingParent("block/large_formation/" + name + "_small", new ResourceLocation("paradisemod:block/template/large_formation_small"))
                        .texture("texture", Buttons.texturePath(name));

                    var medium = modelBuilder
                        .withExistingParent("block/large_formation/" + name + "_medium", new ResourceLocation("paradisemod:block/template/large_formation_medium"))
                        .texture("texture", Buttons.texturePath(name));

                    var large = modelBuilder
                        .withExistingParent("block/large_formation/" + name + "_large", new ResourceLocation("paradisemod:block/template/large_formation_large"))
                        .texture("texture", Buttons.texturePath(name));
                    
                    if(name == "icicle") {
                        small = small.renderType("translucent");
                        medium = medium.renderType("translucent");
                        large = large.renderType("translucent");
                    }

                    buildLargeFormationBlockstate(block.get(), generator, small, medium, large);
                }
            )
            .noDrops()
            .localizedName(englishName, spanishName);
    }

    private static void buildLargeFormationBlockstate(Block formation, BlockStateGenerator generator, ModelFile small, ModelFile medium, ModelFile large) {
        generator.getVariantBuilder(formation)
            .forAllStatesExcept(
                state -> {
                    return switch(state.getValue(LargeCaveFormation.SIZE)) {
                        case SMALL -> generator.buildVariantModel(small);
                        case MEDIUM -> generator.buildVariantModel(medium);
                        case LARGE -> generator.buildVariantModel(large);
                    };
                },
                LargeCaveFormation.WATERLOGGED
            );
    }

    private static RegisteredBlock regLargeFormation(String name, Block matchingBlock) {
        return regLargeFormation(name, () -> matchingBlock);
    }

    private static RegisteredBlock regSmallFormation(String name, SoundType sound) {
        var formationName = name.contains("icicle") ? name : name + "_formation";

        var englishName = switch(name) {
            case "icicle" -> "Icicle";
            case "blue_icicle" -> "Blue Icicle";
            default -> Utils.localizedMaterialName(name, false) + " Cave Formation";
        };

        var spanishName = switch(name) {
            case "icicle" -> "Carámbano";
            case "blue_icicle" -> "Carámbano azul";
            default -> "Formación de cueva de " + Utils.localizedMaterialName(name, true);
        };

        return PMRegistries.regBlockItem(
            formationName,
            () -> new SmallCaveFormation(sound)
        )
            .tab(name =="darkstone" ? DeepDarkBlocks.DEEP_DARK_TAB.key() : CreativeModeTabs.NATURAL_BLOCKS)
            .itemModel((block, generator) ->
                generator.withExistingParent(formationName, new ResourceLocation("paradisemod:block/formation/" + name))
            )
            .blockStateGenerator(
                (block, generator) -> {
                    var model = generator.models()
                        .withExistingParent("block/formation/" + name, new ResourceLocation("paradisemod:block/template/formation"))
                        .texture("texture", Buttons.texturePath(name));

                    if(name == "icicle") model = model.renderType("translucent");
                    final var newModel = model;

                    generator.getVariantBuilder(block.get())
                        .forAllStatesExcept(
                            state -> {
                                Direction facing = state.getValue(SmallCaveFormation.FACING);
                                return generator.buildVariantModelFacingUp(newModel, facing);
                            },
                            SmallCaveFormation.WATERLOGGED
                        );
                }
            )
            .noDrops()
            .localizedName(englishName, spanishName);
    }

    private static RegisteredBlock regSmallFormation(String name) {
        return regSmallFormation(name, SoundType.STONE);
    }

    @FunctionalInterface
    public interface WorldgenFactory<T> {
        T generate(BootstapContext<T> context);
    }
}