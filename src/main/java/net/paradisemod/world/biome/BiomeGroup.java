package net.paradisemod.world.biome;

import java.util.ArrayList;
import java.util.List;
import java.util.function.LongFunction;
import java.util.stream.Stream;

import javax.annotation.Nullable;

import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.util.random.SimpleWeightedRandomList;
import net.minecraft.util.random.WeightedEntry;
import net.minecraft.util.random.WeightedRandom;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.paradisemod.base.Utils;
import net.paradisemod.base.mixin.WeightedListAccessor;
import net.paradisemod.world.noise.FastNoiseLite;
import terrablender.worldgen.noise.Area;
import terrablender.worldgen.noise.AreaContext;
import terrablender.worldgen.noise.AreaTransformer0;
import terrablender.worldgen.noise.LayeredNoiseUtil;
import terrablender.worldgen.noise.ZoomLayer;

@SuppressWarnings("unchecked")
public class BiomeGroup {
    protected final String name;
    protected final int numLayers;
    private @Nullable Registry<Biome> biomeRegistry;
    private final BiomeInit upperLayer;
    private final BiomeInit lowerLayer;
    private final ArrayList<ResourceKey<Biome>> allBiomes = new ArrayList<>();

    protected BiomeGroup(String name) {
        this.name = name;

        numLayers = switch(name) {
            case "overworld_core" -> 4;
            case "deep_dark" -> 3;
            case "elysium" -> 2;
            default -> Utils.modErrorTyped("Biome group name \"" + name + "\" is invalid!");
        };

        upperLayer = new BiomeInit(false);
        lowerLayer = new BiomeInit(true);
        biomeRegistry = null;
    }

    protected void initBiomeRegistry(Registry<Biome> biomeRegistry) {
        this.biomeRegistry = biomeRegistry;
    }

    protected List<Area> genBiomes(long seed) {
        var biomeLayers = new ArrayList<Area>();
        biomeLayers.add(lowerLayer.genLayers(seed));
        for(int i = 1; i < numLayers; i++)
            biomeLayers.add(upperLayer.genLayers(seed + (420777L * i)));

        return biomeLayers;
    }

    protected Stream<Holder<Biome>> getAllBiomes() {
        return allBiomes.stream()
            .map(biome -> (Holder<Biome>) biomeRegistry.getHolderOrThrow(biome));
    }

    protected List<FastNoiseLite> getLayerNoises(long seed) {
        var noises = new ArrayList<FastNoiseLite>();
        for(int i = 0; i < numLayers - 1; i++) {
            var noise = new FastNoiseLite((int) seed + (420777 * i));
            noise.SetNoiseType(FastNoiseLite.NoiseType.OpenSimplex2);
            noise.SetFrequency(0.005F);
            noises.add(noise);
        }

        return noises;
    }

    protected Holder<Biome> getBiome(Area layer, int x, int z) {
        return biomeRegistry.getHolder(layer.get(x, z))
            .orElseThrow();
    }

    private static SimpleWeightedRandomList<ResourceKey<Biome>> deepDarkBiomes() {
        return new SimpleWeightedRandomList.Builder<ResourceKey<Biome>>()
            .add(Biomes.DEEP_DARK, 20)
            .add(PMBiomes.DEEP_DARK_FLATS, 10)
            .add(PMBiomes.FROZEN_DEEP_DARK_FLATS, 10)
            .add(PMBiomes.GLOWING_GLACIER, 10)
            .add(PMBiomes.POLAR_WINTER, 10)
            .add(PMBiomes.SILVA_INSOMNIUM, 10)
            .add(PMBiomes.TAIGA_INSOMNIUM, 10)
            .add(PMBiomes.DARK_VOLCANIC_FIELD, 10)
            .add(PMBiomes.DARK_DESERT, 10)
            .add(PMBiomes.GLOWING_FOREST, 5)
            .add(PMBiomes.CRYSTAL_FOREST, 5)
            .build();
    }

    private static SimpleWeightedRandomList<ResourceKey<Biome>> OWCBiomes(boolean lower) {
        var builder = new SimpleWeightedRandomList.Builder<ResourceKey<Biome>>();
        for(var biome : PMBiomes.ROSE_LANDS_BY_COLOR.values())
            builder = builder.add(biome, 5);

        if(lower)
            builder = builder.add(PMBiomes.WARM_UNDERGROUND_OCEAN, 10)
                .add(PMBiomes.LUKEWARM_UNDERGROUND_OCEAN, 10)
                .add(PMBiomes.UNDERGROUND_OCEAN, 10)
                .add(PMBiomes.COLD_UNDERGROUND_OCEAN, 10)
                .add(PMBiomes.FROZEN_UNDERGROUND_OCEAN, 10)
                .add(PMBiomes.UNDERGROUND_SWAMP, 10)
                .add(PMBiomes.UNDERGROUND_MANGROVE_SWAMP, 10);

        return builder
            .add(Biomes.DRIPSTONE_CAVES, 10)
            .add(Biomes.LUSH_CAVES, 10)
            .add(PMBiomes.UNDERGROUND_GRASSLAND, 10)
            .add(PMBiomes.UNDERGROUND_DARK_FOREST, 10)
            .add(PMBiomes.UNDERGROUND_FOREST, 10)
            .add(PMBiomes.UNDERGROUND_FLOWER_FOREST, 10)
            .add(PMBiomes.UNDERGROUND_BIRCH_FOREST, 10)
            .add(PMBiomes.UNDERGROUND_AUTUMN_FOREST, 10)
            .add(PMBiomes.UNDERGROUND_TAIGA, 10)
            .add(PMBiomes.COLD_UNDERGROUND_ROCKY_DESERT, 10)
            .add(PMBiomes.SNOWY_UNDERGROUND_ROCKY_DESERT, 10)
            .add(PMBiomes.SNOWY_UNDERGROUND_TAIGA, 10)
            .add(PMBiomes.UNDERGROUND_TUNDRA, 10)
            .add(PMBiomes.ICE_SPIKES_CAVE, 10)
            .add(PMBiomes.UNDERGROUND_GLACIER, 10)
            .add(PMBiomes.UNDERGROUND_JUNGLE, 10)
            .add(PMBiomes.UNDERGROUND_TEMPERATE_RAINFOREST, 10)
            .add(PMBiomes.UNDERGROUND_DESERT, 10)
            .add(PMBiomes.SALT_CAVE, 10)
            .add(PMBiomes.VOLCANIC_CAVE, 10)
            .add(PMBiomes.TERRACOTTA_CAVE, 10)
            .add(PMBiomes.UNDERGROUND_ROCKY_DESERT, 10)
            .add(PMBiomes.UNDERGROUND_PALO_VERDE_FOREST, 10)
            .add(PMBiomes.UNDERGROUND_MESQUITE_FOREST, 10)
            .add(PMBiomes.UNDERGROUND_SAVANNA, 10)
            .add(PMBiomes.UNDERGROUND_CHERRY_FOREST, 10)
            .add(PMBiomes.MUSHROOM_CAVE, 10)
            .add(PMBiomes.HONEY_CAVE, 10)
            .build();
    }

    private static SimpleWeightedRandomList<ResourceKey<Biome>> elysiumBiomes(boolean lower) {
        var builder = new SimpleWeightedRandomList.Builder<ResourceKey<Biome>>();
        for(var biome : PMBiomes.ROSE_FIELDS_BY_COLOR.values())
            builder = builder.add(biome, 5);

        if(lower)
            builder = builder.add(Biomes.DRIPSTONE_CAVES, 10)
                .add(Biomes.LUSH_CAVES, 10)
                .add(PMBiomes.SALT_DEPOSIT, 10);

        return builder
            .add(Biomes.PLAINS, 10)
            .add(Biomes.SWAMP, 10)
            .add(Biomes.DARK_FOREST, 10)
            .add(Biomes.FOREST, 10)
            .add(Biomes.BIRCH_FOREST, 10)
            .add(Biomes.TAIGA, 10)
            .add(Biomes.SNOWY_TAIGA, 10)
            .add(Biomes.SNOWY_PLAINS, 10)
            .add(Biomes.DESERT, 10)
            .add(PMBiomes.HIGH_ROCKY_DESERT, 10)
            .add(PMBiomes.SNOWY_ROCKY_DESERT, 10)
            .add(PMBiomes.GLACIER, 10)
            .add(PMBiomes.SUBGLACIAL_VOLCANIC_FIELD, 10)
            .add(PMBiomes.ROCKY_DESERT, 10)
            .add(Biomes.MANGROVE_SWAMP, 10)
            .add(Biomes.CHERRY_GROVE, 10)
            .add(Biomes.OLD_GROWTH_BIRCH_FOREST, 7)
            .add(PMBiomes.AUTUMN_FOREST, 5)
            .add(PMBiomes.TEMPERATE_RAINFOREST, 5)
            .add(PMBiomes.SALT_FLAT, 5)
            .add(PMBiomes.VOLCANIC_FIELD, 5)
            .add(PMBiomes.PALO_VERDE_FOREST, 5)
            .add(PMBiomes.MESQUITE_FOREST, 5)
            .add(PMBiomes.THE_ORIGIN, 5)
            .add(PMBiomes.WEEPING_FOREST, 5)
            .add(PMBiomes.FROZEN_WEEPING_FOREST, 5)
            .add(PMBiomes.CHRISTMAS_FOREST, 5)
            .add(PMBiomes.CONCRETE_BADLANDS, 5)
            .add(Biomes.JUNGLE, 5)
            .add(Biomes.FLOWER_FOREST, 5)
            .add(Biomes.BADLANDS, 5)
            .add(Biomes.ICE_SPIKES, 5)
            .add(Biomes.BAMBOO_JUNGLE, 5)
            .add(Biomes.MUSHROOM_FIELDS, 5)
            .build();
    }

    private class BiomeInit implements AreaTransformer0 {
        private final List<WeightedEntry.Wrapper<ResourceKey<Biome>>> entries;
        private final int totalWeight;

        BiomeInit(boolean lower) {
            SimpleWeightedRandomList<ResourceKey<Biome>> biomes = switch(name) {
                case "overworld_core" -> OWCBiomes(lower);
                case "deep_dark" -> deepDarkBiomes();
                case "elysium" -> elysiumBiomes(lower);
                default -> Utils.modErrorTyped("This should not be reached!");
            };

            this.totalWeight = ((WeightedListAccessor<WeightedEntry.Wrapper<ResourceKey<Biome>>>) biomes).totalWeight();
            this.entries = getEntries(biomes, allBiomes);
        }

        @Override
        public int apply(AreaContext areaContext, int x, int z) {
            var weightedItem = WeightedRandom.getWeightedItem(entries, areaContext.nextRandom(totalWeight));
            var resourceKeyWrapper = weightedItem.get();
            var key = resourceKeyWrapper.getData();
            return biomeRegistry.getId(biomeRegistry.get(key));
        }

        private Area genLayers(long seed) {
            LongFunction<AreaContext> contextFactory = seedModifier -> new AreaContext(25, seed, seedModifier);
            var factory = run(contextFactory.apply(1L));
            factory = ZoomLayer.FUZZY.run(contextFactory.apply(2000L), factory);
            factory = LayeredNoiseUtil.zoom(2001L, ZoomLayer.FUZZY, factory, 3, contextFactory);
            factory = LayeredNoiseUtil.zoom(1001L, ZoomLayer.NORMAL, factory, 4, contextFactory);
            return factory.make();
        }

        private static List<WeightedEntry.Wrapper<ResourceKey<Biome>>> getEntries(SimpleWeightedRandomList<ResourceKey<Biome>> biomeList, ArrayList<ResourceKey<Biome>> allBiomeList) {
            var items = ((WeightedListAccessor<WeightedEntry.Wrapper<ResourceKey<Biome>>>) biomeList).items();
            for(var biomeItem : items) {
                var biome = biomeItem.getData();
                allBiomeList.add(biome);
            }

            return items;
        }
    }
}