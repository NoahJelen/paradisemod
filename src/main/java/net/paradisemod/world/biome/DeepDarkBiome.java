package net.paradisemod.world.biome;

import net.minecraft.core.HolderGetter;
import net.minecraft.data.worldgen.BiomeDefaultFeatures;
import net.minecraft.sounds.Musics;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.level.biome.AmbientMoodSettings;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeGenerationSettings;
import net.minecraft.world.level.biome.BiomeSpecialEffects;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.carver.ConfiguredWorldCarver;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.paradisemod.worldgen.carver.PMCarvers;
import net.paradisemod.worldgen.features.PMFeatures;
import net.paradisemod.worldgen.features.foliage.PMFoliage;
import net.paradisemod.worldgen.structures.PMStructures;

public class DeepDarkBiome {
    public static Biome insomniumForest(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter, boolean taiga) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        genSettings.addFeature(GenerationStep.Decoration.SURFACE_STRUCTURES, PMFeatures.INSOMNIUM_FOSSIL);
        defaultFeatures(spawnInfo, genSettings);
        genSettings.addFeature(GenerationStep.Decoration.SURFACE_STRUCTURES, PMStructures.HOME);

        if(taiga) {
            genSettings.addFeature(GenerationStep.Decoration.LOCAL_MODIFICATIONS, PMFeatures.INSOMNIUM_ROCK);
            genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFoliage.BLACKENED_SPRUCE_COVER);
        }
        else
            genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFoliage.LARGE_BLACKENED_FOLIAGE);

        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFoliage.BLACKENED_FOLIAGE);

        if(!taiga) {
            genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.PSYCHEDELIC_LAKE);
            genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.PSYCHEDELIC_LAKE_UNDERGROUND);
            genSettings.addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.PSYCHEDELIC_SPRING);
        }

        return createBiome(
            genSettings,
            spawnInfo,
            true,
            0.25F,
            0,
            0x574934,
            0x574934,
            0x100C02,
            0x100C02
        );
    }

    public static Biome glowingForest(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter, boolean crystals) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(spawnInfo, genSettings);
        genSettings.addFeature(GenerationStep.Decoration.SURFACE_STRUCTURES, PMStructures.HOME);
        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFoliage.GLOWING_FOLIAGE);
        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFoliage.LARGE_GLOWING_FOLIAGE);
        if(crystals)
            genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFeatures.CRYSTAL_FOREST_CRYSTALS);

        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.PSYCHEDELIC_LAKE);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.PSYCHEDELIC_LAKE_UNDERGROUND);
        genSettings.addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.PSYCHEDELIC_SPRING);

        return createBiome(
            genSettings,
            spawnInfo,
            true,
            0.25F,
            1,
            0x2B494B,
            0x2B494B,
            0x051B21,
            0x051B21
        );
    }

    public static Biome deepDarkFlats(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter, boolean frozen) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(spawnInfo, genSettings);
        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFeatures.DDF_CRYSTALS);
        var temp = frozen ? -1 : 0.25F;
        var downfall = frozen ? 1 : 0;
        if(frozen)
            genSettings.addFeature(GenerationStep.Decoration.LOCAL_MODIFICATIONS, PMFeatures.ICE_CHUNK);

        return createBiome(
            genSettings,
            spawnInfo,
            true,
            temp,
            downfall,
            0x2B494B,
            0x2B494B,
            0,
            0
        );
    }

    public static Biome deepDarkGlacier(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(spawnInfo, genSettings);
        genSettings.addCarver(GenerationStep.Carving.AIR, carverGetter.getOrThrow(PMCarvers.CREVASSE));
        genSettings.addCarver(GenerationStep.Carving.AIR, carverGetter.getOrThrow(PMCarvers.ICE_CAVES));
        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMStructures.RESEARCH_BASE);

        return createBiome(
            genSettings,
            spawnInfo,
            true,
            -1,
            1,
            0x2B494B,
            0x2B494B,
            0x0B0B15,
            0x0B0B15
        );
    }

    public static Biome darkVolcanicField(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(spawnInfo, genSettings);

        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.VOLCANIC_DARK_LAVA_LAKE);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.VOLCANIC_LAVA_LAKE_UNDERGROUND);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.MOLTEN_GOLD_LAKE);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.MOLTEN_GOLD_LAKE_UNDERGROUND);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.MOLTEN_SILVER_LAKE);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.MOLTEN_SILVER_LAKE_UNDERGROUND);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.MOLTEN_IRON_LAKE);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.MOLTEN_IRON_LAKE_UNDERGROUND);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.MOLTEN_COPPER_LAKE);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.MOLTEN_COPPER_LAKE_UNDERGROUND);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.VOLCANIC_BLACK_GLOWING_OBSIDIAN);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.VOLCANIC_GOLD_GLOWING_OBSIDIAN);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.VOLCANIC_SILVER_GLOWING_OBSIDIAN);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.VOLCANIC_IRON_GLOWING_OBSIDIAN);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.VOLCANIC_COPPER_GLOWING_OBSIDIAN);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.DARK_MAGMA);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.VOLCANIC_TUFF);
        genSettings.addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.VOLCANIC_DARK_LAVA_SPRING);
        genSettings.addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.GOLD_SPRING);
        genSettings.addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.SILVER_SPRING);
        genSettings.addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.IRON_SPRING);
        genSettings.addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.COPPER_SPRING);
        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFeatures.DARK_FISSURE);

        return createBiome(
            genSettings,
            spawnInfo,
            false,
            2,
            0,
            4159204,
            329011,
            0,
            0
        );
    }

    public static Biome darkDesert(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(spawnInfo, genSettings);
        BiomeDefaultFeatures.desertSpawns(spawnInfo);

        return createBiome(
            genSettings,
            spawnInfo,
            false,
            2,
            0,
            4159204,
            329011,
            0,
            0
        );
    }

    private static void defaultFeatures(MobSpawnSettings.Builder spawnInfo, BiomeGenerationSettings.Builder genSettings) {
        spawnInfo.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.WITHER_SKELETON, 100, 2, 5));
        BiomeDefaultFeatures.commonSpawns(spawnInfo);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.DEEP_DARK_LAVA_LAKE);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.DEEP_DARK_LAVA_LAKE_UNDERGROUND);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.DARK_LAVA_LAKE);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.DARK_LAVA_LAKE_UNDERGROUND);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.GLOWING_WATER_LAKE);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.GLOWING_WATER_LAKE_UNDERGROUND);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.WATER_LAKE);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.WATER_LAKE_UNDERGROUND);
        genSettings.addFeature(GenerationStep.Decoration.SURFACE_STRUCTURES, PMStructures.SMALL_DARK_DUNGEON);
        genSettings.addFeature(GenerationStep.Decoration.SURFACE_STRUCTURES, PMStructures.DEEP_DARK_BLACK_CROSS);
        genSettings.addFeature(GenerationStep.Decoration.SURFACE_STRUCTURES, PMStructures.DEEP_DARK_GOLD_CROSS);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.REGEN_STONE_BLOB);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.DEEP_DARK_DEEPSLATE_BLOB);
        genSettings.addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.GLOWING_WATER_SPRING);
        genSettings.addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.DEEP_DARK_WATER_SPRING);
        genSettings.addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.DEEP_DARK_LAVA_SPRING);
        genSettings.addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.DARK_LAVA_SPRING);
        genSettings.addCarver(GenerationStep.Carving.AIR, PMCarvers.DEEP_DARK_CAVES);
        genSettings.addCarver(GenerationStep.Carving.AIR, PMCarvers.DEEP_DARK_CANYONS);
    }

    private static Biome createBiome(
        BiomeGenerationSettings.Builder genSettings,
        MobSpawnSettings.Builder spawnInfo,
        boolean hasRain,
        float temp,
        float downfall,
        int waterColor,
        int waterFogColor,
        int fogColor,
        int skyColor
    ) {
        return new Biome.BiomeBuilder()
            .hasPrecipitation(hasRain)
            .temperature(temp)
            .downfall(downfall)
            .specialEffects(
                new BiomeSpecialEffects.Builder()
                    .backgroundMusic(Musics.createGameMusic(SoundEvents.MUSIC_BIOME_DEEP_DARK))
                    .waterColor(waterColor)
                    .waterFogColor(waterFogColor)
                    .fogColor(fogColor)
                    .skyColor(skyColor)
                    .ambientMoodSound(AmbientMoodSettings.LEGACY_CAVE_SETTINGS)
                    .build()
            )
            .mobSpawnSettings(spawnInfo.build())
            .generationSettings(genSettings.build())
            .build();
    }
}