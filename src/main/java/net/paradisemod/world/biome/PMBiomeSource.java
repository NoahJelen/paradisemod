package net.paradisemod.world.biome;

import java.util.ArrayList;
import java.util.stream.Stream;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.Holder;
import net.minecraft.core.QuartPos;
import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeSource;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.biome.Climate;
import net.minecraftforge.server.ServerLifecycleHooks;
import net.paradisemod.base.Utils;
import net.paradisemod.world.noise.FastNoiseLite;
import terrablender.worldgen.noise.Area;

public class PMBiomeSource extends BiomeSource {
    public static final Codec<PMBiomeSource> CODEC = RecordCodecBuilder.create(
        builder -> builder.group(
            Codec.STRING.fieldOf("group")
                .stable()
                .forGetter(source -> source.group.name)
        )
            .apply(builder, builder.stable(PMBiomeSource::new))
    );

    private final BiomeGroup group;
    private long seed = 0;
    private final ArrayList<Area> genBiomes = new ArrayList<>();
    private final ArrayList<FastNoiseLite> layerNoises = new ArrayList<>();

    public PMBiomeSource(String groupName) {
        group = new BiomeGroup(groupName);
    }

    @Override
    protected Codec<PMBiomeSource> codec() { return CODEC; }

    @Override
    public Holder<Biome> getNoiseBiome(int x, int y, int z, Climate.Sampler sampler) {
        initRegistry();
        var heightFromZero = 0;

        switch(group.name) {
            case "overworld_core" -> {
                var index = 0;

                if(y > layerNoiseAtBlockLevel(0, 77, x, z) && y < layerNoiseAtBlockLevel(1, 158, x, z))
                    index = 1;
                else if(y > layerNoiseAtBlockLevel(1, 158, x, z) && y < layerNoiseAtBlockLevel(2, 239, x, z))
                    index = 2;
                else if(y > layerNoiseAtBlockLevel(2, 239, x, z))
                    index = 3;

                return group.getBiome(genBiomes.get(index), x, z);
            }

            case "elysium" -> {
                var undergroundLevel = layerNoiseAtBlockLevel(0, 50, x, z);
                var underground = genBiomes.get(0);
                var ground = genBiomes.get(1);

                if(y < undergroundLevel) {
                    var biome = group.getBiome(underground, x, z);

                    if(biome.is(Biomes.LUSH_CAVES) || biome.is(Biomes.DRIPSTONE_CAVES) || biome.is(PMBiomes.SALT_DEPOSIT))
                        return biome;
                }

                return group.getBiome(ground, x, z);
            }

            case "deep_dark" -> heightFromZero = QuartPos.fromBlock(144);
            default -> Utils.modErrorTyped("Biome group name \"" + group.name + "\" is invalid!");
        }

        var layerSize = heightFromZero / group.numLayers;
        int i = 1;

        for(var genBiome : genBiomes) {
            var floor = -16F;
            var ceiling = 80F;
            if(i > 1)
                floor = layerNoiseAtLevel(i - 2, (layerSize * (i - 1)), x, z);

            if(i < group.numLayers)
                ceiling = layerNoiseAtLevel(i - 1, (layerSize * i), x, z);

            if(y > floor && y < ceiling || i == group.numLayers)
                return group.getBiome(genBiome, x, z);

            i++;
        }

        return group.getBiome(genBiomes.get(group.numLayers - 1), x, z);
    }

    private int layerNoiseAtLevel(int index, int level, int x, int z) {
        return level + (int) (layerNoises.get(index).GetNoise(x, z) * 5);
    }

    private int layerNoiseAtBlockLevel(int index, int level, int x, int z) {
        return layerNoiseAtLevel(index, QuartPos.fromBlock(level), x, z);
    }

    @Override
    protected Stream<Holder<Biome>> collectPossibleBiomes() {
        initRegistry();
        return group.getAllBiomes();
    }

    // if this is the first time getting a biome,
    // let's initialize the biome registry reference and world
    // seed reference
    private void initRegistry() {
        if(genBiomes.isEmpty() && layerNoises.isEmpty() && seed == 0) {
            var server = ServerLifecycleHooks.getCurrentServer();

            group.initBiomeRegistry(
                server.registryAccess()
                    .registryOrThrow(Registries.BIOME)
            );

            var worldData = server.getWorldData();
            var options = worldData.worldGenOptions();
            seed = options.seed();
            genBiomes.addAll(group.genBiomes(seed));
            layerNoises.addAll(group.getLayerNoises(seed));
        }
    }
}