package net.paradisemod.world.biome;

import javax.annotation.Nullable;

import net.minecraft.core.HolderGetter;
import net.minecraft.data.worldgen.BiomeDefaultFeatures;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.level.biome.AmbientMoodSettings;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeGenerationSettings;
import net.minecraft.world.level.biome.BiomeSpecialEffects;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.GenerationStep.Decoration;
import net.minecraft.world.level.levelgen.carver.ConfiguredWorldCarver;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.paradisemod.worldgen.carver.PMCarvers;
import net.paradisemod.worldgen.features.PMFeatures;
import net.paradisemod.worldgen.features.foliage.PMFoliage;

public class UndergroundBiome {
    protected static final int DEFAULT_SKY = 0x14847e;

    public static Biome biome(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter, boolean hasRain, float temp, int waterColor, int grassColor, boolean shroomCave) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(spawnInfo, genSettings);
        if(temp <= 0) genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.GLOWING_ICE_ORE);
        else genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.GLOWSTONE_ORE);
        if(shroomCave)
            genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFoliage.OWC_SHROOMS);

        if(temp >= 2)
            BiomeDefaultFeatures.desertSpawns(spawnInfo);

        return createBiome(
            genSettings,
            spawnInfo,
            hasRain,
            temp,
            0.4F,
            grassColor,
            waterColor,
            waterColor,
            DEFAULT_SKY
        );
    }
    
    public static Biome undergroundSwamp(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(spawnInfo, genSettings);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.GLOWSTONE_ORE);
        spawnInfo.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.SLIME, 1, 1, 1));
        spawnInfo.addSpawn(MobCategory.CREATURE, new MobSpawnSettings.SpawnerData(EntityType.FROG, 10, 2, 5));

        return createBiome(
            genSettings,
            spawnInfo,
            true,
            0.25F,
            0.4F,
            0x6A7039,
            0x617B64,
            0x617B64,
            DEFAULT_SKY
        );
    }

    public static Biome undergroundMangroveSwamp(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(spawnInfo, genSettings);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.GLOWSTONE_ORE);
        spawnInfo.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.SLIME, 1, 1, 1));
        spawnInfo.addSpawn(MobCategory.CREATURE, new MobSpawnSettings.SpawnerData(EntityType.FROG, 10, 2, 5));
        spawnInfo.addSpawn(MobCategory.WATER_AMBIENT, new MobSpawnSettings.SpawnerData(EntityType.TROPICAL_FISH, 25, 8, 8));

        return createBiome(
            genSettings,
            spawnInfo,
            true,
            0.25F,
            0.4F,
            0x6A7039,
            0x3A7A6A,
            0x3A7A6A,
            DEFAULT_SKY
        );
    }

    public static Biome saltCave(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(spawnInfo, genSettings);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.MOLTEN_SALT_LAKE);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.SALT_LAMP_ORE);
        genSettings.addFeature(Decoration.FLUID_SPRINGS, PMFeatures.SALT_SPRING);
        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFeatures.EXTRA_SALT_CRYSTALS);
        BiomeDefaultFeatures.desertSpawns(spawnInfo);

        return createBiome(
            genSettings,
            spawnInfo,
            false,
            2,
            0,
            null,
            0xC29191,
            0xC29191,
            0xE940F2
        );
    }

    public static Biome glacier(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(spawnInfo, genSettings);
        genSettings.addCarver(GenerationStep.Carving.AIR, carverGetter.getOrThrow(PMCarvers.CREVASSE));
        genSettings.addCarver(GenerationStep.Carving.AIR, carverGetter.getOrThrow(PMCarvers.ICE_CAVES));

        return createBiome(
            genSettings,
            spawnInfo,
            true,
            -0.25F,
            1,
            null,
            4159204,
            329011,
            DEFAULT_SKY
        );
    }

    public static Biome volcanicCave(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(spawnInfo, genSettings);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.VOLCANIC_LAVA_LAKE_UNDERGROUND);
        genSettings.addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.VOLCANIC_SPRING);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.VOLCANIC_OBSIDIAN);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.VOLCANIC_MAGMA);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.VOLCANIC_BASALT);

        return createBiome(
            genSettings,
            spawnInfo,
            false,
            2,
            0,
            0xffcc4d,
            4159204,
            329011,
            DEFAULT_SKY
        );
    }

    public static Biome honeyCave(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(spawnInfo, genSettings);
        genSettings.addFeature(GenerationStep.Decoration.LAKES , PMFeatures.HONEY_LAKE);
        genSettings.addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.HONEY_SPRING);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.HONEY_CRYSTAL_ORE);
        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFeatures.HONEY_CRYSTALS);

        return createBiome(
            genSettings,
            spawnInfo,
            true,
            2,
            1,
            null,
            0xe1a110,
            0xe1a110,
            0xEBD81A
        );
    }

    protected static void defaultFeatures(MobSpawnSettings.Builder spawnInfo, BiomeGenerationSettings.Builder genSettings) {
        OverworldBiome.defaultFeatures(genSettings);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.PSYCHEDELIC_LAVA_LAKE);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.LIQUID_REDSTONE_LAKE);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.TAR_PIT);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.WATER_LAKE_UNDERGROUND);
        genSettings.addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.PSYCHEDELIC_LAVA_SPRING);
        genSettings.addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.LIQUID_REDSTONE_SPRING);
        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFoliage.OWC_TREES);
        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFoliage.OWC_FOLIAGE);
        BiomeDefaultFeatures.commonSpawns(spawnInfo);
    }

    protected static Biome createBiome(
        BiomeGenerationSettings.Builder genSettings,
        MobSpawnSettings.Builder spawnInfo,
        boolean hasRain,
        float temp,
        float downfall,
        @Nullable Integer grassColor,
        int waterColor,
        int waterFogColor,
        int skyColor
    ) {
        var effectsBuilder = new BiomeSpecialEffects.Builder()
            .waterColor(waterColor)
            .waterFogColor(waterFogColor)
            .fogColor(skyColor)
            .skyColor(skyColor)
            .ambientMoodSound(AmbientMoodSettings.LEGACY_CAVE_SETTINGS);

        if(grassColor != null)
            effectsBuilder
                .grassColorOverride(grassColor)
                .foliageColorOverride(grassColor);

        return new Biome.BiomeBuilder()
            .hasPrecipitation(hasRain)
            .temperature(temp)
            .downfall(downfall)
            .specialEffects(effectsBuilder.build())
            .mobSpawnSettings(spawnInfo.build())
            .generationSettings(genSettings.build())
            .build();
    }
}