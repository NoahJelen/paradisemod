package net.paradisemod.world.biome;

import java.util.function.Consumer;

import com.mojang.datafixers.util.Pair;

import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Climate.ParameterPoint;
import terrablender.api.Region;
import terrablender.api.RegionType;

public class BiomeRegion extends Region {
    private final ResourceKey<Biome> biome;
    private final ResourceKey<Biome> vanillaBiomeToReplace;
    public BiomeRegion(ResourceKey<Biome> biome, ResourceKey<Biome> vanillaBiomeToReplace) {
        super(biome.location(), RegionType.OVERWORLD, 100);
        this.biome = biome;
        this.vanillaBiomeToReplace = vanillaBiomeToReplace;
    }

    @Override
    public void addBiomes(Registry<Biome> registry, Consumer<Pair<ParameterPoint, ResourceKey<Biome>>> mapper) {
        addModifiedVanillaOverworldBiomes(mapper, builder -> builder.replaceBiome(vanillaBiomeToReplace, biome));
    }
}