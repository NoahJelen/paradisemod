package net.paradisemod.world.biome;

import net.minecraft.core.HolderGetter;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeGenerationSettings;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.carver.ConfiguredWorldCarver;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.paradisemod.worldgen.features.PMFeatures;
import net.paradisemod.worldgen.features.foliage.PMFoliage;

public class ElysiumBiome {
    public static Biome christmasForest(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        OverworldBiome.defaultFeatures(genSettings);
        OverworldBiome.defaultVegetationAndSpawns(spawnInfo, genSettings);
        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFoliage.CHRISTMAS_CRYSTALS);
        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFoliage.CHRISTMAS_LANTERNS);
        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFeatures.FULL_CHRISTMAS_TREE);

        return OverworldBiome.createBiome(
            spawnInfo,
            genSettings,
            true,
            -0.25F,
            0.4F,
            0x153079,
            0x153079,
            12638463,
            0x06812d,
            -0.25F
        );
    }

    public static Biome concreteBadlands(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        OverworldBiome.defaultFeatures(genSettings);

        return OverworldBiome.createBiome(
            spawnInfo,
            genSettings,
            true,
            0.8F,
            0.4F,
            0x0d16ee,
            0x0d16ee,
            12638463,
            0x16ee0d,
            0.8F
        );
    }
}