package net.paradisemod.world.biome;

import net.minecraft.core.HolderGetter;
import net.minecraft.data.worldgen.BiomeDefaultFeatures;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeGenerationSettings;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.carver.ConfiguredWorldCarver;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.paradisemod.worldgen.features.PMFeatures;

public class UndergroundOcean {
    public static Biome biome(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter, OceanType type) {
        BiomeGenerationSettings.Builder genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        var spawnInfo = new MobSpawnSettings.Builder();
        BiomeDefaultFeatures.oceanSpawns(spawnInfo, 1, 4, 10);
        spawnInfo.addSpawn(MobCategory.WATER_CREATURE, new MobSpawnSettings.SpawnerData(EntityType.GLOW_SQUID, 1, 1, 4));
        if(type == OceanType.WARM) BiomeDefaultFeatures.warmOceanSpawns(spawnInfo, 10, 4);
        if(type == OceanType.FROZEN) {
            BiomeDefaultFeatures.addIcebergs(genSettings);
            BiomeDefaultFeatures.addBlueIce(genSettings);
        }

        UndergroundBiome.defaultFeatures(spawnInfo, genSettings);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.SEA_LANTERN_ORE);

        return UndergroundBiome.createBiome(
            genSettings,
            spawnInfo,
            true,
            type.temp,
            0.5F,
            null,
            type.waterColor,
            type.waterColor,
            type.waterColor
        );
    }

    protected enum OceanType {
        NORMAL(0.5f,0x3F76E4),
        LUKEWARM(0.7f,0x45ADF2),
        WARM(1,0x43D5EE),
        COLD(0,0x3D57D6),
        FROZEN(-0.25f,0x3938C9);

        private final float temp;
        private final int waterColor;

        OceanType(float temp, int waterColor) {
            this.temp = temp;
            this.waterColor = waterColor;
        }

        public float getTemp() { return temp; }
    }
}