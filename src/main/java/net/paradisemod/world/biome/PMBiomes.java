package net.paradisemod.world.biome;

import java.util.EnumMap;
import java.util.HashMap;

import com.google.common.collect.ArrayListMultimap;

import net.minecraft.core.Holder;
import net.minecraft.core.HolderGetter;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.BiomeTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.levelgen.carver.ConfiguredWorldCarver;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraftforge.common.BiomeManager;
import net.minecraftforge.common.BiomeManager.BiomeType;
import net.minecraftforge.common.Tags;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.paradisemod.base.data.assets.PMTranslations;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.world.PMWorld.WorldgenFactory;
import net.paradisemod.world.biome.OverworldBiome.RockyDesertType;
import net.paradisemod.world.biome.UndergroundOcean.OceanType;
import terrablender.api.Regions;

@SuppressWarnings("unchecked")
public class PMBiomes {
    public static HashMap<ResourceKey<Biome>, BiomeFactory> BIOMES = new HashMap<>();
    public static ArrayListMultimap<TagKey<Biome>, ResourceKey<Biome>> TAGGED_BIOMES = ArrayListMultimap.create();
    private static final HashMap<ResourceKey<Biome>, String> ENGLISH_NAMES = new HashMap<>();
    private static final HashMap<ResourceKey<Biome>, String> SPANISH_NAMES = new HashMap<>();

    // overworld biomes
    public static EnumMap<DyeColor, ResourceKey<Biome>> ROSE_FIELDS_BY_COLOR = new EnumMap<>(DyeColor.class);

    public static final ResourceKey<Biome> AUTUMN_FOREST = regBiome(
        "autumn_forest",
        "Autumn Forest",
        "Bosque de otoño",
        OverworldBiome::autumnForest,
        BiomeTags.IS_FOREST,
        BiomeTags.HAS_RUINED_PORTAL_STANDARD,
        BiomeTags.HAS_STRONGHOLD,
        BiomeTags.IS_OVERWORLD
    );

    public static final ResourceKey<Biome> ROCKY_DESERT = regBiome(
        "rocky_desert",
        "Rocky Desert",
        "Desierto rocoso",
        (featureGetter, carverGetter) -> OverworldBiome.rockyDesert(featureGetter, carverGetter, RockyDesertType.NORMAL),
        PMTags.Biomes.ROCKY_DESERTS,
        BiomeTags.IS_OVERWORLD
    );

    public static final ResourceKey<Biome> HIGH_ROCKY_DESERT = regBiome(
        "high_rocky_desert",
        "High Rocky Desert",
        "Desierto rocoso alto",
        (featureGetter, carverGetter) -> OverworldBiome.rockyDesert(featureGetter, carverGetter, RockyDesertType.HIGH),
        PMTags.Biomes.COLD_ROCKY_DESERTS,
        BiomeTags.IS_OVERWORLD
    );

    public static final ResourceKey<Biome> SNOWY_ROCKY_DESERT = regBiome(
        "snowy_rocky_desert",
        "Snowy Rocky Desert",
        "Desierto rocoso nevado",
        (featureGetter, carverGetter) -> OverworldBiome.rockyDesert(featureGetter, carverGetter, RockyDesertType.SNOWY),
        Tags.Biomes.IS_SNOWY,
        PMTags.Biomes.COLD_ROCKY_DESERTS,
        BiomeTags.IS_OVERWORLD
    );

    public static final ResourceKey<Biome> MESQUITE_FOREST = regBiome(
        "mesquite_forest",
        "Mesquite Forest",
        "Bosque de mezquite",
        (featureGetter, carverGetter) -> OverworldBiome.rockyDesert(featureGetter, carverGetter, RockyDesertType.NORMAL),
        PMTags.Biomes.ROCKY_DESERTS,
        BiomeTags.IS_OVERWORLD
    );

    public static final ResourceKey<Biome> PALO_VERDE_FOREST = regBiome(
        "palo_verde_forest",
        "Palo Verde Forest",
        "Bosque de palo verde",
        (featureGetter, carverGetter) -> OverworldBiome.rockyDesert(featureGetter, carverGetter, RockyDesertType.NORMAL),
        PMTags.Biomes.ROCKY_DESERTS,
        BiomeTags.IS_OVERWORLD
    );

    public static final ResourceKey<Biome> SALT_FLAT = regBiome(
        "salt_flat",
        "Salt Flat",
        "Salina",
        OverworldBiome::saltFlat,
        PMTags.Biomes.SALT,
        BiomeTags.IS_OVERWORLD
    );

    public static final ResourceKey<Biome> SALT_DEPOSIT = regBiome(
        "salt_deposit",
        "Salt Deposit",
        "Depósito de sal",
        OverworldBiome::saltFlat,
        PMTags.Biomes.SALT,
        BiomeTags.IS_OVERWORLD
    );

    public static final ResourceKey<Biome> TEMPERATE_RAINFOREST = regBiome(
        "temperate_rainforest",
        "Temperate Rainforest",
        "Selva tropical templada",
        OverworldBiome::temperateRainforest,
        Tags.Biomes.IS_WET_OVERWORLD,
        BiomeTags.IS_JUNGLE,
        BiomeTags.IS_OVERWORLD
    );

    public static final ResourceKey<Biome> GLACIER = regBiome(
        "glacier",
        "Glacier",
        "Glaciar",
        OverworldBiome::glacier,
        Tags.Biomes.IS_COLD,
        Tags.Biomes.IS_SNOWY,
        BiomeTags.HAS_RUINED_PORTAL_STANDARD,
        BiomeTags.HAS_STRONGHOLD,
        BiomeTags.IS_OVERWORLD
    );

    public static final ResourceKey<Biome> VOLCANIC_FIELD = regBiome(
        "volcanic_field",
        "Volcanic Field",
        "Tierra volcánica",
        (featureGetter, carverGetter) -> OverworldBiome.volcanicField(featureGetter, carverGetter, false),
        PMTags.Biomes.VOLCANIC,
        Tags.Biomes.IS_HOT,
        Tags.Biomes.IS_DRY,
        BiomeTags.HAS_RUINED_PORTAL_STANDARD,
        BiomeTags.HAS_STRONGHOLD,
        BiomeTags.IS_OVERWORLD
    );

    public static final ResourceKey<Biome> SUBGLACIAL_VOLCANIC_FIELD = regBiome(
        "subglacial_volcanic_field",
        "Subglacial Volcanic Field",
        "Tierra volcánica debajo de glaciar",
        (featureGetter, carverGetter) -> OverworldBiome.volcanicField(featureGetter, carverGetter, true),
        PMTags.Biomes.VOLCANIC,
        Tags.Biomes.IS_COLD,
        Tags.Biomes.IS_SNOWY,
        BiomeTags.HAS_RUINED_PORTAL_STANDARD,
        BiomeTags.HAS_STRONGHOLD,
        BiomeTags.IS_OVERWORLD
    );

    // overworld core biomes
    public static EnumMap<DyeColor, ResourceKey<Biome>> ROSE_LANDS_BY_COLOR = new EnumMap<>(DyeColor.class);

    public static final ResourceKey<Biome> WARM_UNDERGROUND_OCEAN = regBiome(
        "warm_underground_ocean",
        "Warm Underground Ocean",
        "Océano subterráneo cálido",
        (featureGetter, carverGetter) -> UndergroundOcean.biome(featureGetter, carverGetter, OceanType.WARM),
        Tags.Biomes.IS_HOT,
        Tags.Biomes.IS_WATER,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> LUKEWARM_UNDERGROUND_OCEAN = regBiome(
        "lukewarm_underground_ocean",
        "Lukewarm Underground Ocean",
        "Océano subterráneo tibio",
        (featureGetter, carverGetter) -> UndergroundOcean.biome(featureGetter, carverGetter, OceanType.LUKEWARM),
        Tags.Biomes.IS_WATER,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> UNDERGROUND_OCEAN = regBiome(
        "underground_ocean",
        "Underground Ocean",
        "Océano subterráneo",
        (featureGetter, carverGetter) -> UndergroundOcean.biome(featureGetter, carverGetter, OceanType.NORMAL),
        Tags.Biomes.IS_WATER,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> COLD_UNDERGROUND_OCEAN = regBiome(
        "cold_underground_ocean",
        "Cold Underground Ocean",
        "Océano subterráneo frío",
        (featureGetter, carverGetter) -> UndergroundOcean.biome(featureGetter, carverGetter, OceanType.COLD),
        Tags.Biomes.IS_COLD,
        Tags.Biomes.IS_WATER,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> FROZEN_UNDERGROUND_OCEAN = regBiome(
        "frozen_underground_ocean",
        "Frozen Underground Ocean",
        "Océano subterráneo congelado",
        (featureGetter, carverGetter) -> UndergroundOcean.biome(featureGetter, carverGetter, OceanType.FROZEN),
        Tags.Biomes.IS_COLD,
        Tags.Biomes.IS_SNOWY,
        Tags.Biomes.IS_WATER,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> UNDERGROUND_AUTUMN_FOREST = regBiome(
        "underground_autumn_forest",
        "Underground Autumn Forest",
        "Bosque subterráneo de otoño",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, 0.8F, 329011, 0x79751a, false),
        PMTags.Biomes.OVERWORLD_CORE,
        BiomeTags.IS_FOREST
    );

    public static final ResourceKey<Biome> UNDERGROUND_ROCKY_DESERT = regBiome(
        "underground_rocky_desert",
        "Underground Rocky Desert",
        "Desierto rocoso subterráneo",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, false, 2F, 329011, 0xffcc4d, false),
        PMTags.Biomes.ROCKY_DESERTS,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> COLD_UNDERGROUND_ROCKY_DESERT = regBiome(
        "cold_underground_rocky_desert",
        "Cold Underground Rocky Desert",
        "Desierto rocoso frío subterráneo",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, 0.25F, 329011, 0x97ff4d, false),
        PMTags.Biomes.COLD_ROCKY_DESERTS,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> SNOWY_UNDERGROUND_ROCKY_DESERT = regBiome(
        "snowy_underground_rocky_desert",
        "Snowy Underground Rocky Desert",
        "Desierto rocoso nevado subterráneo",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, -0.25F, 329011, 0x97ff4d, false),
        PMTags.Biomes.COLD_ROCKY_DESERTS,
        Tags.Biomes.IS_SNOWY,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> UNDERGROUND_MESQUITE_FOREST = regBiome(
        "underground_mesquite_forest",
        "Underground Mesquite Forest",
        "Bosque subterráneo de mezquite",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, false, 2F, 329011, 0xffcc4d, false),
        PMTags.Biomes.ROCKY_DESERTS,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> UNDERGROUND_PALO_VERDE_FOREST = regBiome(
        "underground_palo_verde_forest",
        "Underground Palo Verde Forest",
        "Bosque subterráneo de palo verde",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, false, 2F, 329011, 0xffcc4d, false),
        PMTags.Biomes.ROCKY_DESERTS,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> SALT_CAVE = regBiome(
        "salt_cave",
        "Salt Cave",
        "Cueva Salada",
        UndergroundBiome::saltCave,
        PMTags.Biomes.OVERWORLD_CORE,
        PMTags.Biomes.SALT
    );

    public static final ResourceKey<Biome> UNDERGROUND_TEMPERATE_RAINFOREST = regBiome(
        "underground_temperate_rainforest",
        "Underground Temperate Rainforest",
        "Selva tropical templada subterránea",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, 0.8F, 329011, 0x3bdb6d, false),
        Tags.Biomes.IS_WET,
        BiomeTags.IS_JUNGLE,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> VOLCANIC_CAVE = regBiome(
        "volcanic_cave",
        "Volcanic Cave",
        "Cueva volcánica",
        UndergroundBiome::volcanicCave,
        PMTags.Biomes.OVERWORLD_CORE,
        PMTags.Biomes.VOLCANIC
    );

    public static final ResourceKey<Biome> UNDERGROUND_GLACIER = regBiome(
        "underground_glacier",
        "Underground Glacier",
        "Glaciar subterráneo",
        UndergroundBiome::glacier,
        Tags.Biomes.IS_COLD,
        Tags.Biomes.IS_SNOWY,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> HONEY_CAVE = regBiome(
        "honey_cave",
        "Honey Cave",
        "Cueva de miel",
        UndergroundBiome::honeyCave,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> UNDERGROUND_GRASSLAND = regBiome(
        "underground_grassland",
        "Underground Grassland",
        "Pradera subterránea",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, 0.8F, 0x3F76E4, 0x91BD59, false),
        PMTags.Biomes.OVERWORLD_CORE,
        Tags.Biomes.IS_PLAINS
    );

    public static final ResourceKey<Biome> UNDERGROUND_DESERT = regBiome(
        "underground_desert",
        "Underground Desert",
        "Desierto subterráneo",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, false, 2F, 0x3F76E4, 0xBFB755, false),
        Tags.Biomes.IS_DRY,
        Tags.Biomes.IS_HOT,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> UNDERGROUND_FOREST = regBiome(
        "underground_forest",
        "Underground Forest",
        "Bosque subterráneo",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, 0.7F, 0x3F76E4, 0x79C05A, false),
        PMTags.Biomes.OVERWORLD_CORE,
        BiomeTags.IS_FOREST
    );

    public static final ResourceKey<Biome> UNDERGROUND_CHERRY_FOREST = regBiome(
        "underground_cherry_forest",
        "Underground Cherry Forest",
        "Bosque subterráneo de cerezo",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, 0.7F, 0x3F76E4, 0x79C05A, false),
        PMTags.Biomes.OVERWORLD_CORE,
        BiomeTags.IS_FOREST
    );

    public static final ResourceKey<Biome> UNDERGROUND_TAIGA = regBiome(
        "underground_taiga",
        "Underground Taiga", 
        "Taiga subterránea",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, 0.25F, 0x3F76E4, 0x86B783, false),
        Tags.Biomes.IS_COLD,
        Tags.Biomes.IS_CONIFEROUS,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> UNDERGROUND_SWAMP = regBiome(
        "underground_swamp",
        "Underground Swamp",
        "Pantano subterráneo",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, 0.25F, 0x617B64, 0x6A7039, false),
        Tags.Biomes.IS_WET,
        Tags.Biomes.IS_SWAMP,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> UNDERGROUND_MANGROVE_SWAMP = regBiome(
        "underground_mangrove_swamp",
        "Underground Mangrove Swamp",
        "Pantano subterráneo de mangle",
        UndergroundBiome::undergroundMangroveSwamp,
        Tags.Biomes.IS_WET,
        Tags.Biomes.IS_SWAMP,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> UNDERGROUND_TUNDRA = regBiome(
        "underground_tundra",
        "Underground Tundra",
        "Tundra subterránea",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, 0, 0x3F76E4, 0x80B497, false),
        Tags.Biomes.IS_COLD,
        Tags.Biomes.IS_SNOWY,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> MUSHROOM_CAVE = regBiome(
        "mushroom_cave",
        "Mushroom Cave",
        "Cueva de hongo",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, 0.9F, 0x3F76E4 ,0x55C93F, true),
        Tags.Biomes.IS_MUSHROOM,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> UNDERGROUND_JUNGLE = regBiome(
        "underground_jungle",
        "Underground Jungle",
        "Jungla subterránea",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, 0.95F, 0x3F76E4, 0x59C93C, false),
        Tags.Biomes.IS_WET,
        BiomeTags.IS_JUNGLE,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> UNDERGROUND_BIRCH_FOREST = regBiome(
        "underground_birch_forest",
        "Underground Birch Forest",
        "Bosque subterráneo de abedul",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, 0.7F, 0x3F76E4,0x88BB67, false),
        PMTags.Biomes.OVERWORLD_CORE,
        BiomeTags.IS_FOREST
    );

    public static final ResourceKey<Biome> UNDERGROUND_DARK_FOREST = regBiome(
        "underground_dark_forest",
        "Underground Dark Forest",
        "Bosque subterráneo oscuro",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, 0.7F, 0x3F76E4,0x507A32, false),
        PMTags.Biomes.OVERWORLD_CORE,
        BiomeTags.IS_FOREST
    );

    public static final ResourceKey<Biome> SNOWY_UNDERGROUND_TAIGA = regBiome(
        "snowy_underground_taiga",
        "Snowy Underground Taiga",
        "Taiga subterránea nevada",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, 0.25F, 0x3D57D6, 0x80B497, false),
        Tags.Biomes.IS_COLD,
        Tags.Biomes.IS_SNOWY,
        Tags.Biomes.IS_CONIFEROUS,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> UNDERGROUND_SAVANNA = regBiome(
        "underground_savanna",
        "Underground Savanna",
        "Sabana subterránea",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, false, 1.2F, 0x3F76E4, 0xBFB755, false),
        BiomeTags.IS_SAVANNA,
        Tags.Biomes.IS_DRY,
        Tags.Biomes.IS_HOT,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> UNDERGROUND_FLOWER_FOREST = regBiome(
        "underground_flower_forest",
        "Underground Flower Forest",
        "Bosque subterráneo con flores",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, 0.7F, 0x3F76E4, 0x79C05A, false),
        PMTags.Biomes.OVERWORLD_CORE,
        BiomeTags.IS_FOREST
    );

    public static final ResourceKey<Biome> ICE_SPIKES_CAVE = regBiome(
        "ice_spikes_cave",
        "Ice Spikes Cave",
        "Cueva de pinchos de hielo",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, -0.25F, 0x3F76E4, 0x80B497, false),
        Tags.Biomes.IS_COLD,
        Tags.Biomes.IS_SNOWY,
        PMTags.Biomes.OVERWORLD_CORE
    );

    public static final ResourceKey<Biome> TERRACOTTA_CAVE = regBiome(
        "terracotta_cave",
        "Terracotta Cave",
        "Cueva de terracota",
        (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, false, 2F, 0x3F76E4, 0x90814D, false),
        BiomeTags.IS_BADLANDS,
        PMTags.Biomes.OVERWORLD_CORE
    );

    // deep dark biomes
    public static final ResourceKey<Biome> DEEP_DARK_FLATS = regBiome(
        "deep_dark_flats",
        "Deep Dark Flats",
        "Tierra de oscuro profundo",
        (featureGetter, carverGetter) -> DeepDarkBiome.deepDarkFlats(featureGetter, carverGetter, false),
        PMTags.Biomes.DEEP_DARK
    );

    public static final ResourceKey<Biome> FROZEN_DEEP_DARK_FLATS = regBiome(
        "frozen_deep_dark_flats",
        "Frozen Deep Dark Flats",
        "Tierra congelada de oscuro profundo",
        (featureGetter, carverGetter) -> DeepDarkBiome.deepDarkFlats(featureGetter, carverGetter, true),
        PMTags.Biomes.DEEP_DARK,
        Tags.Biomes.IS_SNOWY
    );

    public static final ResourceKey<Biome> SILVA_INSOMNIUM = regBiome(
        "silva_insomnium",
        "Silva Insomnium",
        "Selva Insomnium",
        (featureGetter, carverGetter) -> DeepDarkBiome.insomniumForest(featureGetter, carverGetter, false),
        PMTags.Biomes.DEEP_DARK
    );

    public static final ResourceKey<Biome> TAIGA_INSOMNIUM = regBiome(
        "taiga_insomnium",
        "Taiga Insomnium",
        "Taiga Insomnium",
        (featureGetter, carverGetter) -> DeepDarkBiome.insomniumForest(featureGetter, carverGetter, true),
        PMTags.Biomes.DEEP_DARK
    );

    public static final ResourceKey<Biome> GLOWING_FOREST = regBiome(
        "glowing_forest",
        "Glowing Forest",
        "Bosque brillante",
        (featureGetter, carverGetter) -> DeepDarkBiome.glowingForest(featureGetter, carverGetter, false),
        PMTags.Biomes.DEEP_DARK
    );

    public static final ResourceKey<Biome> CRYSTAL_FOREST = regBiome(
        "crystal_forest",
        "Crystal Forest",
        "Bosque de cristales",
        (featureGetter, carverGetter) -> DeepDarkBiome.glowingForest(featureGetter, carverGetter, true),
        PMTags.Biomes.DEEP_DARK
    );

    public static final ResourceKey<Biome> GLOWING_GLACIER = regBiome(
        "glowing_glacier",
        "Glowing Glacier",
        "Glaciar brillante",
        DeepDarkBiome::deepDarkGlacier,
        PMTags.Biomes.DEEP_DARK,
        Tags.Biomes.IS_SNOWY
    );

    public static final ResourceKey<Biome> POLAR_WINTER = regBiome(
        "polar_winter",
        "Polar Winter",
        "Invierno polar",
        DeepDarkBiome::deepDarkGlacier,
        PMTags.Biomes.DEEP_DARK,
        Tags.Biomes.IS_SNOWY
    );

    public static final ResourceKey<Biome> DARK_VOLCANIC_FIELD = regBiome(
        "dark_volanic_field",
        "Dark Volcanic Field",
        "Tierra volcánica oscura",
        DeepDarkBiome::darkVolcanicField,
        PMTags.Biomes.DEEP_DARK,
        Tags.Biomes.IS_HOT,
        Tags.Biomes.IS_DRY
    );

    public static final ResourceKey<Biome> DARK_DESERT = regBiome(
        "dark_desert",
        "Dark Desert",
        "Desierto oscuro",
        DeepDarkBiome::darkDesert,
        PMTags.Biomes.DEEP_DARK,
        Tags.Biomes.IS_HOT,
        Tags.Biomes.IS_DRY,
        PMTags.Biomes.DARK_DESERTS
    );

    // surreal biomes
    public static final ResourceKey<Biome> CHRISTMAS_FOREST = regBiome(
        "christmas_forest",
        "Christmas Forest",
        "Bosque navideño",
        ElysiumBiome::christmasForest,
        PMTags.Biomes.ELYSIUM,
        Tags.Biomes.IS_SNOWY
    );

    public static final ResourceKey<Biome> THE_ORIGIN = regBiome(
        "the_origin",
        "The Origin",
        "El origen",
        OverworldBiome::theOrigin,
        BiomeTags.HAS_RUINED_PORTAL_STANDARD,
        BiomeTags.HAS_STRONGHOLD
    );

    public static final ResourceKey<Biome> WEEPING_FOREST = regBiome(
        "weeping_forest",
        "Weeping Forest",
        "Bosque llanto",
        (featureGetter, carverGetter) -> OverworldBiome.weepingForest(featureGetter, carverGetter, false),
        BiomeTags.HAS_RUINED_PORTAL_STANDARD,
        BiomeTags.HAS_STRONGHOLD
    );

    public static final ResourceKey<Biome> FROZEN_WEEPING_FOREST = regBiome(
        "frozen_weeping_forest",
        "Frozen Weeping Forest",
        "Bosque llanto congelado",
        (featureGetter, carverGetter) -> OverworldBiome.weepingForest(featureGetter, carverGetter, true),
        Tags.Biomes.IS_COLD,
        Tags.Biomes.IS_SNOWY,
        BiomeTags.HAS_RUINED_PORTAL_STANDARD,
        BiomeTags.HAS_STRONGHOLD
    );

    public static final ResourceKey<Biome> CONCRETE_BADLANDS = regBiome(
        "concrete_badlands",
        "Concrete Badlands",
        "Badlands de concreto",
        ElysiumBiome::concreteBadlands,
        PMTags.Biomes.ELYSIUM
    );

    static {
        for(var color : DyeColor.values()) {
            var spanishColor = PMTranslations.spanishColor(color, true);
            if(spanishColor.endsWith("a") || spanishColor.endsWith("e") || spanishColor.endsWith("é"))
                spanishColor = spanishColor + "s";
            else if(color == DyeColor.LIGHT_BLUE)
                spanishColor = "azules claras";
            else if(color == DyeColor.LIGHT_GRAY)
                spanishColor = "gris claras";
            else if(color == DyeColor.LIME)
                spanishColor = "verdes limas";
            else if(color == DyeColor.GRAY) { }
            else spanishColor = spanishColor + "es";

            var roseField = regBiome(
                color.getName() + "_rose_field",
                PMTranslations.englishColor(color) + " Rose Field",
                "Campo de rosas " + spanishColor,
                (featureGetter, carverGetter) -> OverworldBiome.roseField(featureGetter, carverGetter, color),
                PMTags.Biomes.ROSE_FIELDS
            );

            var roseLand = regBiome(
                color.getName() + "_roseland",
                PMTranslations.englishColor(color) + " Rose Land",
                "Tierra de rosas " + spanishColor,
                (featureGetter, carverGetter) -> UndergroundBiome.biome(featureGetter, carverGetter, true, 0.8F, 0x3F76E4, 0x91BD59, false),
                PMTags.Biomes.OVERWORLD_CORE,
                Tags.Biomes.IS_PLAINS
            );

            ROSE_FIELDS_BY_COLOR.put(color, roseField);
            ROSE_LANDS_BY_COLOR.put(color, roseLand);
        }
    }

    public static void init(IEventBus eventbus) {
        eventbus.addListener(PMBiomes::setupOverworldBiomes);
    }

    public static boolean isSandyDesert(Holder<Biome> biome) {
        return biome.is(Tags.Biomes.IS_DRY) &&
            !biome.is(PMTags.Biomes.ROCKY_DESERTS) &&
            !biome.is(PMTags.Biomes.COLD_ROCKY_DESERTS) &&
            !biome.is(PMTags.Biomes.SALT) &&
            !biome.is(PMTags.Biomes.VOLCANIC) &&
            !biome.is(BiomeTags.IS_SAVANNA) &&
            !biome.is(BiomeTags.IS_BADLANDS) &&
            (biome.is(BiomeTags.IS_OVERWORLD) || biome.is(PMTags.Biomes.OVERWORLD_CORE));
    }

    public static boolean isRockyDesert(Holder<Biome> biome) {
        return biome.is(PMTags.Biomes.ROCKY_DESERTS) ||
            biome.is(PMTags.Biomes.COLD_ROCKY_DESERTS);
    }

    public static void translations(PMTranslations translator) {
        for(var biome : BIOMES.keySet()) {
            var englishName = ENGLISH_NAMES.get(biome);
            var spanishName = SPANISH_NAMES.get(biome);
            translator.addBiome(biome, englishName, spanishName);
        }
    }

    private static void setupOverworldBiomes(FMLCommonSetupEvent event) {
        for(var field : ROSE_FIELDS_BY_COLOR.values())
            setupOverworldBiome(field, BiomeManager.BiomeType.WARM, 5);

        setupOverworldBiome(AUTUMN_FOREST, BiomeType.WARM, 3);
        setupOverworldBiome(ROCKY_DESERT, BiomeType.DESERT, 10);
        setupOverworldBiome(HIGH_ROCKY_DESERT, BiomeType.COOL, 10);
        setupOverworldBiome(SNOWY_ROCKY_DESERT, BiomeType.ICY, 10);
        setupOverworldBiome(MESQUITE_FOREST, BiomeType.DESERT, 10);
        setupOverworldBiome(PALO_VERDE_FOREST, BiomeType.DESERT, 10);
        setupOverworldBiome(SALT_FLAT, BiomeType.DESERT, 5);
        setupOverworldBiome(TEMPERATE_RAINFOREST, BiomeType.WARM, 15);
        setupOverworldBiome(GLACIER, BiomeType.ICY, 5);
        setupOverworldBiome(VOLCANIC_FIELD, BiomeType.DESERT, 2);
        setupOverworldBiome(SUBGLACIAL_VOLCANIC_FIELD, BiomeType.ICY, 2);

        // surreal biomes
        setupOverworldBiome(WEEPING_FOREST, BiomeType.COOL, 3);
        setupOverworldBiome(FROZEN_WEEPING_FOREST, BiomeType.ICY, 3);
        setupOverworldBiome(THE_ORIGIN, BiomeType.WARM, 3);
    }

    private static ResourceKey<Biome> regBiome(String name, String englishName, String spanishName, BiomeFactory biome, TagKey<Biome>... tags) {
        var key = PMRegistries.createModResourceKey(Registries.BIOME, name);
        ENGLISH_NAMES.put(key, englishName);
        SPANISH_NAMES.put(key, spanishName);
        BIOMES.put(key, biome);
        for(var tag : tags) TAGGED_BIOMES.put(tag, key);
        return key;
    }

    private static void setupOverworldBiome(ResourceKey<Biome> biome, BiomeManager.BiomeType type, int weight) {
        var similarVanillaBiome = switch(type) {
            case DESERT -> Biomes.DESERT;
            case WARM -> Biomes.FOREST;
            case COOL -> Biomes.TAIGA;
            case ICY -> Biomes.SNOWY_PLAINS;
            default -> Biomes.PLAINS;
        };

        Regions.register(new BiomeRegion(biome, similarVanillaBiome));
        BiomeManager.addBiome(type, new BiomeManager.BiomeEntry(biome, weight));
    }

    @FunctionalInterface
    public interface BiomeFactory extends WorldgenFactory<Biome> {
        default Biome generate(BootstapContext<Biome> context) {
            var featureGetter = context.lookup(Registries.PLACED_FEATURE);
            var carverGetter = context.lookup(Registries.CONFIGURED_CARVER);
            return generate(featureGetter, carverGetter);
        }

        Biome generate(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter);
    }
}