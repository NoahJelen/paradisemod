package net.paradisemod.world.biome;

import net.minecraft.core.HolderGetter;
import net.minecraft.data.worldgen.BiomeDefaultFeatures;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.level.biome.AmbientMoodSettings;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeGenerationSettings;
import net.minecraft.world.level.biome.BiomeSpecialEffects;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.minecraft.world.level.biome.MobSpawnSettings.Builder;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.GenerationStep.Decoration;
import net.minecraft.world.level.levelgen.carver.ConfiguredWorldCarver;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.paradisemod.worldgen.carver.PMCarvers;
import net.paradisemod.worldgen.features.PMFeatures;
import net.paradisemod.worldgen.features.foliage.PMFoliage;

public class OverworldBiome {
    public static Biome autumnForest(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(genSettings);
        defaultVegetationAndSpawns(spawnInfo, genSettings);
        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFoliage.AUTUMN_PUMPKINS);
        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFoliage.AUTUMN_PLACER);

        return createBiome(
            spawnInfo,
            genSettings,
            true,
            0.8F,
            0.4F,
            4159204,
            329011,
            12638463,
            0x79751a,
            0.8F
        );
    }

    public static Biome glacier(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(genSettings);
        genSettings.addCarver(GenerationStep.Carving.AIR, PMCarvers.CREVASSE);
        genSettings.addCarver(GenerationStep.Carving.AIR, PMCarvers.ICE_CAVES);

        return createBiome(
            spawnInfo,
            genSettings,
            true,
            -0.25F,
            0.4F,
            4159204,
            329011,
            12638463,
            null,
            -0.25F
        );
    }

    public static Biome rockyDesert(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter, RockyDesertType type) {
        var grassColor = (type != RockyDesertType.NORMAL) ? 0x97ff4d : 0xffcc4d;
        var downfall = (type != RockyDesertType.NORMAL) ? 1f : 0f;
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(genSettings);
        BiomeDefaultFeatures.addDesertVegetation(genSettings);
        BiomeDefaultFeatures.addDesertExtraVegetation(genSettings);
        genSettings.addFeature(Decoration.LOCAL_MODIFICATIONS, PMFeatures.DESERT_ROCK);

        if(type == RockyDesertType.NORMAL)
            genSettings.addFeature(Decoration.VEGETAL_DECORATION, PMFoliage.DESERT_TREES);
        else
            genSettings.addFeature(Decoration.VEGETAL_DECORATION, PMFoliage.COLD_DESERT_TREES);

        genSettings.addFeature(Decoration.VEGETAL_DECORATION, PMFoliage.RD_CACTUS);
        genSettings.addFeature(Decoration.VEGETAL_DECORATION, PMFoliage.RD_FOLIAGE);
        BiomeDefaultFeatures.desertSpawns(spawnInfo);

        return createBiome(
            spawnInfo,
            genSettings,
            type.hasRain,
            type.temp,
            downfall,
            4159204,
            329011,
            12638463,
            grassColor,
            2
        );
    }

    public static Biome roseField(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter, DyeColor color) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(genSettings);
        defaultVegetationAndSpawns(spawnInfo, genSettings);
        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFeatures.ROSE_FIELD_PATCHES.get(color));

        return createBiome(
            spawnInfo,
            genSettings,
            true,
            0.8F,
            0.4F,
            4159204,
            329011,
            12638463,
            null,
            0.8F
        );
    }

    // reference to Wintersun's The Forest that Weeps
    public static Biome weepingForest(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter, boolean isFrozen) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(genSettings);
        defaultVegetationAndSpawns(spawnInfo, genSettings);
        genSettings.addFeature(Decoration.VEGETAL_DECORATION, PMFoliage.WEEPING_FOREST_TREES);

        return createBiome(
            spawnInfo,
            genSettings,
            true,
            isFrozen ? -0.25F : 0.8F,
            0.4F,
            0x636363,
            0x636363,
            0xbab9b9,
            0x848484,
            0.8F
        );
    }

    public static Biome theOrigin(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(genSettings);
        defaultVegetationAndSpawns(spawnInfo, genSettings);
        genSettings.addFeature(Decoration.VEGETAL_DECORATION, PMFoliage.ORIGIN_TREES);

        return createBiome(
            spawnInfo,
            genSettings,
            true,
            0.8F,
            0.4F,
            0x0d16ee,
            0x0d16ee,
            12638463,
            0x16ee0d,
            0.8F
        );
    }

    public static Biome temperateRainforest(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter) {
        var spawnInfo = new MobSpawnSettings.Builder();
        BiomeDefaultFeatures.baseJungleSpawns(spawnInfo);
        BiomeGenerationSettings.Builder genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(genSettings);
        BiomeDefaultFeatures.addJungleGrass(genSettings);
        BiomeDefaultFeatures.addPlainVegetation(genSettings);
        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFoliage.TEMP_RAINFOREST_TREES);
        spawnInfo.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.OCELOT, 2, 1, 1));

        return createBiome(
            spawnInfo,
            genSettings,
            true,
            0.8F,
            1,
            4159204,
            329011,
            12638463,
            0x3bdb6d,
            0.8F
        );
    }

    public static Biome saltFlat(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter) {
        var spawnInfo = new MobSpawnSettings.Builder();
        BiomeDefaultFeatures.desertSpawns(spawnInfo);
        BiomeGenerationSettings.Builder genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(genSettings);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.MOLTEN_SALT_LAKE);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.MOLTEN_SALT_LAKE_UNDERGROUND);
        genSettings.addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.SALT_SPRING);
        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFeatures.EXTRA_SALT_CRYSTALS);

        return createBiome(
            spawnInfo,
            genSettings,
            false,
            2,
            0,
            0xc29191,
            0xc29191,
            12638463,
            0xffcc4d,
            2
        );
    }

    public static Biome volcanicField(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter, boolean frozen) {
        var spawnInfo = new MobSpawnSettings.Builder();
        var genSettings = new BiomeGenerationSettings.Builder(featureGetter, carverGetter);
        defaultFeatures(genSettings);
        BiomeDefaultFeatures.addDefaultGrass(genSettings);
        BiomeDefaultFeatures.addDesertVegetation(genSettings);
        BiomeDefaultFeatures.addDesertExtraVegetation(genSettings);
        if(frozen) {
            genSettings.addCarver(GenerationStep.Carving.AIR, carverGetter.getOrThrow(PMCarvers.CREVASSE));
            genSettings.addCarver(GenerationStep.Carving.AIR, carverGetter.getOrThrow(PMCarvers.ICE_CAVES));
        }

        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.VOLCANIC_LAVA_LAKE);
        genSettings.addFeature(GenerationStep.Decoration.LAKES, PMFeatures.VOLCANIC_LAVA_LAKE_UNDERGROUND);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.VOLCANIC_OBSIDIAN);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.VOLCANIC_OBSIDIAN_DEEPSLATE);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.VOLCANIC_MAGMA);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.VOLCANIC_MAGMA_DEEPSLATE);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.VOLCANIC_BASALT);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.VOLCANIC_TUFF);
        genSettings.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.VOLCANIC_TUFF_DEEPSLATE);
        genSettings.addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.VOLCANIC_SPRING);
        if(!frozen)
            genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFoliage.RD_CACTUS);

        genSettings.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFeatures.FISSURE);

        return createBiome(
            spawnInfo,
            genSettings,
            frozen,
            frozen ? -1 : 2,
            frozen ? 1 : 0,
            4159204,
            329011,
            12638463,
            frozen ? 0x97ff4d : 0xffcc4d,
            -0.25F
        );
    }

    protected static void defaultFeatures(BiomeGenerationSettings.Builder genSettings) {
        BiomeDefaultFeatures.addDefaultCarversAndLakes(genSettings);
        BiomeDefaultFeatures.addDefaultCrystalFormations(genSettings);
        BiomeDefaultFeatures.addDefaultMonsterRoom(genSettings);
        BiomeDefaultFeatures.addDefaultUndergroundVariety(genSettings);
        BiomeDefaultFeatures.addDefaultSprings(genSettings);
        BiomeDefaultFeatures.addSurfaceFreezing(genSettings);
        BiomeDefaultFeatures.addDefaultOres(genSettings);
        BiomeDefaultFeatures.addDefaultSoftDisks(genSettings);
    }

    protected static void defaultVegetationAndSpawns(MobSpawnSettings.Builder spawnInfo, BiomeGenerationSettings.Builder genSettings) {
        BiomeDefaultFeatures.addForestGrass(genSettings);
        BiomeDefaultFeatures.addDefaultMushrooms(genSettings);
        BiomeDefaultFeatures.addDefaultExtraVegetation(genSettings);
        BiomeDefaultFeatures.plainsSpawns(spawnInfo);
    }

    protected static Biome createBiome(
        Builder spawnInfo,
        BiomeGenerationSettings.Builder genSettings,
        boolean hasRain,
        float temp,
        float downfall,
        int waterColor,
        int waterFogcolor,
        int fogColor,
        Integer grassColor,
        float skyTemp
    ) {
        var effectsBuilder = new BiomeSpecialEffects.Builder()
            .waterColor(waterColor)
            .waterFogColor(waterFogcolor)
            .fogColor(fogColor)
            .skyColor(getSkyColorFromTemp(skyTemp))
            .ambientMoodSound(AmbientMoodSettings.LEGACY_CAVE_SETTINGS);

        if(grassColor != null)
            effectsBuilder
                .grassColorOverride(grassColor)
                .foliageColorOverride(grassColor);

        return new Biome.BiomeBuilder()
            .hasPrecipitation(hasRain)
            .temperature(temp)
            .downfall(downfall)
            .specialEffects(effectsBuilder.build())
            .mobSpawnSettings(spawnInfo.build())
            .generationSettings(genSettings.build())
            .build();
    }

    private static int getSkyColorFromTemp(float temp) {
        var f = temp / 3;
        f = Mth.clamp(f, -1, 1);
        return Mth.hsvToRgb(0.62222224F - f * 0.05F, 0.5F + f * 0.1F, 1);
    }

    protected enum RockyDesertType {
        NORMAL(2, false),
        HIGH(0.25F, true),
        SNOWY(-0.25F, true);

        private final float temp;
        private final boolean hasRain;

        RockyDesertType(float temp, boolean hasRain) {
            this.temp = temp;
            this.hasRain = hasRain;
        }
    }
}