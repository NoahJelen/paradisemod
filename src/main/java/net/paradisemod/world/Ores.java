package net.paradisemod.world;

import java.util.ArrayList;

import javax.annotation.Nullable;

import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.Tiers;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FallingBlock;
import net.minecraft.world.level.block.state.BlockBehaviour.Properties;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.misc.Misc;

public class Ores {
    // ores
    public static final RegisteredBlock END_RUBY_ORE = createOre("end_ruby", Blocks.DIAMOND_ORE, Tiers.IRON);
    public static final RegisteredBlock ENDERITE_ORE = createOre("enderite", Blocks.DIAMOND_ORE, Tiers.DIAMOND);
    public static final RegisteredBlock NETHER_SILVER_ORE = createOre("nether_silver", Blocks.NETHER_GOLD_ORE, Tiers.WOOD);
    public static final RegisteredBlock RUBY_ORE = createOre("ruby", Blocks.DIAMOND_ORE, Tiers.IRON);

    public static final RegisteredBlock SALT_ORE = createOre("salt", Blocks.COAL_ORE, Tiers.WOOD);
    public static final RegisteredBlock SILVER_ORE = createOre("silver", Blocks.GOLD_ORE, Tiers.STONE);
    public static final RegisteredBlock DEEPSLATE_RUBY_ORE = createOre("deepslate_ruby", Blocks.DEEPSLATE_DIAMOND_ORE, Tiers.IRON);
    public static final RegisteredBlock DEEPSLATE_SALT_ORE = createOre("deepslate_salt", Blocks.DEEPSLATE_COAL_ORE, Tiers.WOOD);
    public static final RegisteredBlock DEEPSLATE_SILVER_ORE = createOre("deepslate_silver", Blocks.DEEPSLATE_GOLD_ORE, Tiers.STONE);

    // deep dark ores
    public static final RegisteredBlock DARKSTONE_COAL_ORE = createOre("darkstone_coal", Blocks.COAL_ORE, Tiers.WOOD)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock DARKSTONE_GOLD_ORE = createOre("darkstone_gold", Blocks.GOLD_ORE, Tiers.IRON)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock DARKSTONE_IRON_ORE = createOre("darkstone_iron", Blocks.IRON_ORE, Tiers.STONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock DARKSTONE_SILVER_ORE = createOre("darkstone_silver", Blocks.GOLD_ORE, Tiers.IRON)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    // resource blocks
    public static final RegisteredBlock BLAZE_BLOCK = createResourceBlock("blaze", BlockType.WEAK_METAL, true);
    public static final RegisteredBlock ENDERITE_BLOCK = createResourceBlock("enderite", BlockType.NETHERITE_LIKE, false);
    public static final RegisteredBlock COMPACT_SALT_BLOCK = createResourceBlock("compact_salt", BlockType.STONE, false);
    public static final RegisteredBlock RUBY_BLOCK = createResourceBlock("ruby", BlockType.METAL, false);
    public static final RegisteredBlock RUSTED_IRON_BLOCK = createResourceBlock("rusted_iron", BlockType.WEAK_METAL, false);
    public static final RegisteredBlock SILVER_BLOCK = createResourceBlock("silver", BlockType.METAL, false);

    public static final RegisteredBlock SALT_BLOCK = PMRegistries.regBlockItem(
        "salt_block",
        () -> new FallingBlock(Properties.copy(Blocks.SAND))
    )
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.DECORATIONS, item)
                .pattern("ss")
                .pattern("ss")
                .define('s', Misc.SALT_ITEM)
        )
        .tab(CreativeModeTabs.NATURAL_BLOCKS)
        .tags(
            BlockTags.MINEABLE_WITH_SHOVEL,
            PMTags.Blocks.GROUND_BLOCKS
        )
        .localizedName("Salt Block", "Bloque de sal");

    public static void init() { }

    private static RegisteredBlock createOre(String name, Block mimic, Tier toolTier) {
        ArrayList<TagKey<Block>> tags = new ArrayList<>();
        
        if(name.contains("gold"))
            tags.add(BlockTags.GOLD_ORES);

        return PMRegistries.regBlockItem(
            name + "_ore",
            () -> new Block(Properties.copy(mimic))
        )
            .lootTable(
                (block, generator) -> {
                    var drop = getDrop(name);

                    if(drop != null)
                        generator.randomItemDrop(block, drop, name.endsWith("ruby") || name == "darkstone_coal" ? 1 : 4);
                    else
                        generator.dropSelf(block);
                }
            )
            .blockStateGenerator((block, generator) -> generator.simpleBlock(block.get(), "ore/" + name))
            .itemModel((block, generator) -> generator.parentBlockItem(block, "ore/" + name))
            .tab(CreativeModeTabs.NATURAL_BLOCKS)
            .tags(tags)
            .tags(
                BlockTags.MINEABLE_WITH_PICKAXE,
                toolTier.getTag()
            )
            .localizedName(
                localizedName(name, false) + " Ore",
                "Mineral de " + localizedName(name, true)
            );
    }

    private static RegisteredBlock createResourceBlock(String name, BlockType type, boolean glows) {
        var tags = new ArrayList<>(type.tags());

        return PMRegistries.regBlockItem(
            name + "_block",
            () -> new Block(type.getProperties().lightLevel(s -> glows ? 15 : 0))
        )
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.DECORATIONS, item)
                    .pattern("iii")
                    .pattern("iii")
                    .pattern("iii")
                    .define('i', resourceBlockItem(name))
            )
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .tags(tags)
            .tag(BlockTags.BEACON_BASE_BLOCKS)
            .localizedName(
                localizedName(name, false) + " Block",
                "Bloque de " + localizedName(name, true)
            );
    }

    private static @Nullable ItemLike getDrop(String oreName) {
        return switch(oreName) {
            case "ruby", "end_ruby", "deepslate_ruby" -> Misc.RUBY;
            case "salt", "deepslate_salt" -> Misc.SALT_ITEM;
            case "darkstone_coal" -> Items.COAL;
            case "nether_silver" -> Misc.SILVER_NUGGET;
            default -> null;
        };
    }

    private static ItemLike resourceBlockItem(String name) {
        return switch(name) {
            case "blaze" -> Items.BLAZE_POWDER;
            case "enderite" -> Misc.ENDERITE_INGOT;
            case "compact_salt" -> Misc.SALT_ITEM;
            case "ruby" -> Misc.RUBY;
            case "rusted_iron" -> Misc.RUSTED_IRON_INGOT;
            case "silver" -> Misc.SILVER_INGOT;
            default -> Utils.modErrorTyped("The resource block type " + name + " has no item!");
        };
        
    }

    private static String localizedName(String name, boolean spanish) {
        if(spanish)
            return switch(name) {
                case "blaze" -> "polvo de fuego";
                case "ruby", "end_ruby", "deepslate_ruby" -> "rubí";
                case "enderite" -> "metal del Fin";
                case "silver", "nether_silver", "deepslate_silver", "darkstone_silver" -> "plata";
                case "salt", "deepslate_salt" -> "sal";
                case "compact_salt" -> "sal compacta";
                case "darkstone_coal" -> "carbón";
                case "darkstone_iron" -> "hierro";
                case "darkstone_gold" -> "oro";
                case "rusted_iron" -> "hierro oxidado";
                default -> "";
            };

        return switch(name) {
            case "blaze" -> "Blaze";
            case "ruby", "end_ruby", "deepslate_ruby" -> "Ruby";
            case "enderite" -> "Enderite";
            case "silver", "nether_silver", "deepslate_silver", "darkstone_silver" -> "Silver";
            case "salt", "deepslate_salt" -> "Salt";
            case "compact_salt" -> "Compact Salt";
            case "darkstone_coal" -> "Coal";
            case "darkstone_iron" -> "Iron";
            case "darkstone_gold" -> "Gold";
            case "rusted_iron" -> "Rusted Iron";
            default -> "";
        };
    }
}