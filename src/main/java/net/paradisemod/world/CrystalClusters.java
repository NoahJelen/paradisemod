package net.paradisemod.world;

import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraftforge.client.model.generators.ModelFile;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.misc.Misc;
import net.paradisemod.world.blocks.CrystalCluster;

public class CrystalClusters {
    // crystal clusters
    public static final RegisteredBlock DIAMOND_CRYSTAL_CLUSTER = regCrystalCluster("diamond", Items.DIAMOND);
    public static final RegisteredBlock EMERALD_CRYSTAL_CLUSTER = regCrystalCluster("emerald", Items.EMERALD);
    public static final RegisteredBlock HONEY_CRYSTAL_CLUSTER = regCrystalCluster("honey", Misc.HONEY_CRYSTAL);
    public static final RegisteredBlock LAPIS_CRYSTAL_CLUSTER = regCrystalCluster("lapis", Items.LAPIS_LAZULI);
    public static final RegisteredBlock QUARTZ_CRYSTAL_CLUSTER = regCrystalCluster("quartz", Items.QUARTZ);
    public static final RegisteredBlock REDSTONE_CRYSTAL_CLUSTER = regCrystalCluster("redstone", Items.REDSTONE);
    public static final RegisteredBlock RUBY_CRYSTAL_CLUSTER = regCrystalCluster("ruby", Misc.RUBY);
    public static final RegisteredBlock SALT_CRYSTAL_CLUSTER = regCrystalCluster("salt", Misc.SALT_ITEM);
    public static final RegisteredBlock ENDER_PEARL_CLUSTER = regCrystalCluster("ender_pearl", Items.ENDER_PEARL);

    public static void init() { }

    private static RegisteredBlock regCrystalCluster(String name, ItemLike item) {
        var regName = name.equals("ender_pearl") ? "ender_pearl_cluster" : name + "_crystal_cluster";

        var crystal = PMRegistries.regBlockItem(
            regName,
            () -> new CrystalCluster(name == "ender_pearl", name == "redstone")
        )
            .tab(CreativeModeTabs.NATURAL_BLOCKS)
            .itemModel((block, generator) ->
                generator.withExistingParent(regName, new ResourceLocation("paradisemod:block/crystal_cluster/" + name + "_0"))
            )
            .blockStateGenerator(
                (block, generator) -> {
                    var modelBuilder = generator.models();

                    ModelFile[] models = {
                        modelBuilder.withExistingParent("block/crystal_cluster/" + name + "_0", new ResourceLocation("paradisemod:block/template/crystal_0"))
                            .texture("texture", "paradisemod:block/crystal_cluster/" + name),

                        modelBuilder.withExistingParent("block/crystal_cluster/" + name + "_1", new ResourceLocation("paradisemod:block/template/crystal_1"))
                            .texture("texture", "paradisemod:block/crystal_cluster/" + name),

                        modelBuilder.withExistingParent("block/crystal_cluster/" + name + "_2", new ResourceLocation("paradisemod:block/template/crystal_2"))
                            .texture("texture", "paradisemod:block/crystal_cluster/" + name),

                        modelBuilder.withExistingParent("block/crystal_cluster/" + name + "_3", new ResourceLocation("paradisemod:block/template/crystal_3"))
                            .texture("texture", "paradisemod:block/crystal_cluster/" + name)
                    };

                    generator.getVariantBuilder(block.get())
                        .forAllStatesExcept(
                            state -> {
                                Direction facing = state.getValue(CrystalCluster.FACING);
                                int crystalType = state.getValue(CrystalCluster.TYPE);
                                return generator.buildVariantModelFacingUp(models[crystalType], facing);
                            },
                            CrystalCluster.WATERLOGGED
                        );
                }
            )
            .lootTable((block, generator) -> generator.randomItemDrop(block, item, 4))
            .localizedName(
                name.equals("ender_pearl") ? "Ender Pearl Cluster" : Utils.localizedMaterialName(name, false) + " Crystal Cluster",
                "Racimo de " + Utils.localizedMaterialName(name, true)
            );

        if(name != "honey" && name != "ender_pearl")
            crystal = crystal.tag(PMTags.Blocks.CRYSTAL_CLUSTERS);

        return crystal;
    }
}