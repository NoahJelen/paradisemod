package net.paradisemod.world;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.CactusBlock;
import net.minecraft.world.level.block.FallingBlock;
import net.minecraft.world.level.block.HugeMushroomBlock;
import net.minecraft.world.level.block.MagmaBlock;
import net.minecraft.world.level.block.MushroomBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.eventbus.api.IEventBus;
import net.paradisemod.base.BlockTemplates;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.Events;
import net.paradisemod.base.blocks.CustomPlant;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.base.registry.RegisteredCreativeTab;
import net.paradisemod.building.Slabs;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.world.blocks.SpreadableBlock;
import net.paradisemod.world.trees.BlackenedOakTree;
import net.paradisemod.world.trees.BlackenedSpruceTree;
import net.paradisemod.world.trees.GlowingOakTree;
import net.paradisemod.worldgen.features.foliage.PMFoliage;

@SuppressWarnings("unchecked")
public class DeepDarkBlocks {
    public static final RegisteredCreativeTab DEEP_DARK_TAB = new RegisteredCreativeTab(
        "deep_dark",
        () -> DeepDarkBlocks.DARKSTONE,
        "Deep Dark Blocks",
        "Bloques del oscuro profundo"
    );

    public static final RegisteredBlock DARKSTONE = BlockTemplates.improvedRock("darkstone")
        .tab(DEEP_DARK_TAB)
        .tags(
            BlockTags.DIRT,
            PMTags.Blocks.GROUND_BLOCKS
        )
        .itemTags(
            ItemTags.STONE_CRAFTING_MATERIALS,
            ItemTags.STONE_TOOL_MATERIALS
        )
        .localizedName("Darkstone", "Piedra oscura");

    public static final RegisteredBlock POLISHED_DARKSTONE = BlockTemplates.improvedRock("polished_darkstone", DARKSTONE)
        .tab(DEEP_DARK_TAB)
        .localizedName("Polished Darkstone", "Piedra oscura pulida");

    public static final RegisteredBlock POLISHED_DARKSTONE_BRICKS = BlockTemplates.cutImprovedRock("polished_darkstone_bricks", POLISHED_DARKSTONE, DARKSTONE)
        .tab(DEEP_DARK_TAB)
        .localizedName("Polished Darkstone Bricks", "Ladrillos de piedra oscura pulida");

    public static final RegisteredBlock CHISELED_POLISHED_DARKSTONE = BlockTemplates.cutImprovedRock("chiseled_polished_darkstone", POLISHED_DARKSTONE, DARKSTONE)
        .tab(DEEP_DARK_TAB)
        .localizedName("Chiseled Polished Darkstone", "Piedra oscura pulida grabada");

    public static final RegisteredBlock BLACKENED_SAND = PMRegistries.regBlockItem(
        "blackened_sand",
        () -> new FallingBlock(Block.Properties.copy(Blocks.SAND))
    )
        .tab(CreativeModeTabs.NATURAL_BLOCKS)
        .tags(
            BlockTags.MINEABLE_WITH_SHOVEL,
            BlockTags.SAND
        )
        .localizedName("Blackened Sand", "Arena ennegrecida");

    public static final RegisteredBlock BLACKENED_SANDSTONE = BlockTemplates.rock("blackened_sandstone")
        .tab(DEEP_DARK_TAB)
        .tag(PMTags.Blocks.GROUND_BLOCKS)
        .localizedName("Blackened Sandstone", "Arenisca ennegrecida")
        .blockStateGenerator(
            (block, generator) -> {
                var model = generator.models()
                    .withExistingParent("blackened_sandstone", "block/cube_bottom_top")
                    .texture("side", "paradisemod:block/blackened_sandstone")
                    .texture("top", "paradisemod:block/blackened_sandstone_top")
                    .texture("bottom", "paradisemod:block/blackened_sandstone_bottom");

                generator.simpleBlock(block, model);
            }
        )
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.BUILDING_BLOCKS, item)
                .pattern("ss")
                .pattern("ss")
                .define('s', BLACKENED_SAND)
        );

    public static final RegisteredBlock CUT_BLACKENED_SANDSTONE = BlockTemplates.rock("cut_blackened_sandstone", BLACKENED_SANDSTONE)
        .tab(DEEP_DARK_TAB)
        .tag(PMTags.Blocks.GROUND_BLOCKS)
        .localizedName("Cut Blackened Sandstone", "Arenisca ennegrecida cortada")
        .blockStateGenerator(
            (block, generator) -> {
                var model = generator.models()
                    .withExistingParent("cut_blackened_sandstone", "block/cube_column")
                    .texture("side", "paradisemod:block/cut_blackened_sandstone")
                    .texture("end", "paradisemod:block/blackened_sandstone_top");

                generator.simpleBlock(block, model);
            }
        );

    public static final RegisteredBlock CHISELED_BLACKENED_SANDSTONE = BlockTemplates.rock("chiseled_blackened_sandstone")
        .tab(DEEP_DARK_TAB)
        .tag(PMTags.Blocks.GROUND_BLOCKS)
        .localizedName("Chiseled Blackened Sandstone", "Arenisca ennegrecida grabada")
        .blockStateGenerator(
            (block, generator) -> {
                var model = generator.models()
                    .withExistingParent("chiseled_blackened_sandstone", "block/cube_column")
                    .texture("side", "paradisemod:block/chiseled_blackened_sandstone")
                    .texture("end", "paradisemod:block/blackened_sandstone_top");

                generator.simpleBlock(block, model);
            }
        )
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.DECORATIONS, item)
                .pattern("s")
                .pattern("s")
                .define('s', Slabs.BLACKENED_SANDSTONE_SLAB)
        )
        .stonecutterRecipes(BLACKENED_SANDSTONE, CUT_BLACKENED_SANDSTONE);

    public static final RegisteredBlock SMOOTH_BLACKENED_SANDSTONE = BlockTemplates.rock("smooth_blackened_sandstone")
        .tab(DEEP_DARK_TAB)
        .tag(PMTags.Blocks.GROUND_BLOCKS)
        .localizedName("Smooth Blackened Sandstone", "Arenisca ennegrecida lisa")
        .blockStateGenerator(
            (block, generator) -> {
                var model = generator.models()
                    .withExistingParent("block/smooth_blackened_sandstone", "block/cube_all")
                    .texture("all", "paradisemod:block/blackened_sandstone_top");

                generator.simpleBlock(block, model);
            }
        )
        .stonecutterRecipes(BLACKENED_SANDSTONE, CUT_BLACKENED_SANDSTONE);

    public static final RegisteredBlock REGENERATION_STONE = PMRegistries.regBlockItem("regeneration_stone",
        () -> new Block(BlockType.STONE.getProperties().lightLevel(s -> 7)) {
            @Override
            public void stepOn(Level world, BlockPos pos, BlockState state, Entity entity) {
                if (!(entity instanceof LivingEntity creature)) return;
                creature.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 100));
            }
        }
    )
        .tab(DEEP_DARK_TAB)
        .tag(BlockTags.MINEABLE_WITH_PICKAXE)
        .localizedName("Regeneration Stone", "Piedra de regeneración");

    public static final RegisteredBlock DARK_MAGMA_BLOCK = PMRegistries.regBlockItem("dark_magma_block",
        () -> new MagmaBlock(Block.Properties.copy(Blocks.MAGMA_BLOCK))
    )
        .tab(DEEP_DARK_TAB)
        .tag(BlockTags.MINEABLE_WITH_PICKAXE)
        .localizedName("Dark Magma Block", "Bloque oscuro de magma");

    public static final RegisteredBlock GLOWING_NYLIUM = PMRegistries.regBlockItem(
        "glowing_nylium",
        () -> new SpreadableBlock(DARKSTONE.get(), PMTags.Blocks.GLOWING_FOLIAGE, Block.Properties.copy(Blocks.END_STONE).lightLevel(s -> 7)) {
            @Override
            public void animateTick(BlockState state, Level world, BlockPos pos, RandomSource rand) {
                world.sendBlockUpdated(pos, state, state, 8);
            };
        }
    )
        .tab(DEEP_DARK_TAB)
        .tags(
            BlockTags.MINEABLE_WITH_PICKAXE,
            BlockTags.DIRT,
            BlockTags.MUSHROOM_GROW_BLOCK,
            PMTags.Blocks.GROUND_BLOCKS
        )
        .blockStateGenerator(
            (block, generator) -> {
                var modelBuilder = generator.models();
                var model = generator.existingModel("glowing_nylium");
                var snowy = modelBuilder
                    .cubeBottomTop("snowy_glowing_nylium", new ResourceLocation("paradisemod:block/snowy_overgrown_darkstone_side"), new ResourceLocation("paradisemod:block/darkstone"), generator.mcLoc("block/grass_block_top"))
                    .renderType("cutout");

                generator.getVariantBuilder(block.get())
                    .forAllStates(
                        state -> {
                            boolean isBlockSnowy = state.getValue(SpreadableBlock.SNOWY);

                            if(isBlockSnowy)
                                return generator.buildVariantModel(snowy);
                            else return generator.buildVariantModel(model);
                        }
                    );
            }
        )
        .lootTable((block, generator) -> generator.dropUnlessSilkTouch(block, DARKSTONE))
        .localizedName("Glowing Nylium", "Nilio brillante");

    public static final RegisteredBlock OVERGROWN_DARKSTONE = PMRegistries.regBlockItem(
        "overgrown_darkstone",
        () -> new SpreadableBlock(DARKSTONE.get(), PMTags.Blocks.BLACKENED_FOLIAGE, Block.Properties.copy(Blocks.END_STONE))
    )
        .tab(DEEP_DARK_TAB)
        .tags(
            BlockTags.MINEABLE_WITH_PICKAXE,
            BlockTags.DIRT,
            BlockTags.MUSHROOM_GROW_BLOCK,
            PMTags.Blocks.GROUND_BLOCKS
        )
        .blockStateGenerator((block, generator) -> generator.grassBlockLike(block, "paradisemod:block/darkstone", true))
        .lootTable((block, generator) -> generator.dropUnlessSilkTouch(block, DARKSTONE))
        .localizedName("Overgrown Darkstone", "Piedra ocura sobrecrecida");

    // deep dark foliage
    public static final RegisteredBlock GLOWING_SHRUB = PMRegistries.regBlockItem(
        "glowing_shrub",
        () -> new CustomPlant(false, CustomPlant.Type.DEEP_DARK, 7) {
            @Override
            public void animateTick(BlockState state, Level world, BlockPos pos, net.minecraft.util.RandomSource rand) {
                world.sendBlockUpdated(pos, state, state, 8);
            };
        }
    )
        .tab(DEEP_DARK_TAB)
        .tags(
            BlockTags.REPLACEABLE_BY_TREES,
            PMTags.Blocks.GLOWING_FOLIAGE
        )
        .blockStateGenerator((block, generator) -> generator.tintedGrass(block))
        .itemModelAlreadyExists()
        .localizedName("Glowing Shrub", "Arbusto brillante");

    public static final RegisteredBlock BLACKENED_SHRUB = PMRegistries.regBlockItem(
        "blackened_shrub",
        () -> new CustomPlant(false, true, CustomPlant.Type.DEEP_DARK)
    )
        .tab(DEEP_DARK_TAB)
        .tags(
            BlockTags.REPLACEABLE_BY_TREES,
            PMTags.Blocks.BLACKENED_FOLIAGE
        )
        .blockStateGenerator((block, generator) -> generator.tintedGrass(block))
        .itemModelAlreadyExists()
        .localizedName("Blackened Shrub", "Arbusto ennegrecido");

    public static final RegisteredBlock GLOWSHROOM_BLOCK = getGlowshroomBlock(false);
    public static final RegisteredBlock GLOWSHROOM_STEM = getGlowshroomBlock(true);

    public static final RegisteredBlock GLOWSHROOM = PMRegistries.regBlockItem("glowshroom",
        () -> new MushroomBlock(
            Block.Properties.copy(Blocks.RED_MUSHROOM)
                .lightLevel(s -> 7)
                .noOcclusion(),

            PMFoliage.HUGE_GLOWSHROOM
        )
    )
        .tab(DEEP_DARK_TAB)
        .blockStateGenerator((block, generator) -> generator.simpleBlock(block, generator.existingModel("glowshroom")))
        .itemModel((block, generator) -> generator.flatBlockItem("glowshroom"))
        .tag(PMTags.Blocks.GLOWING_FOLIAGE)
        .localizedName("Glowshroom", "Hongo brillante");

    // blackened oak wood type
    public static final RegisteredBlock STRIPPED_BLACKENED_OAK_LOG = BlockTemplates.strippedLog("blackened_oak", false, false)
        .tab(DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_OAK_LOG = BlockTemplates.log("blackened_oak", false, STRIPPED_BLACKENED_OAK_LOG, false)
        .tab(DEEP_DARK_TAB);

    public static final RegisteredBlock STRIPPED_BLACKENED_OAK_WOOD = BlockTemplates.strippedLog("blackened_oak", true, false)
        .tab(DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_OAK_WOOD = BlockTemplates.log("blackened_oak", true, STRIPPED_BLACKENED_OAK_WOOD, false)
        .tab(DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_OAK_SAPLING = Decoration.regSapling("blackened_oak", new BlackenedOakTree(), true, false);

    public static final RegisteredBlock BLACKENED_OAK_LEAVES = BlockTemplates.leaves("blackened_oak", "oak", BLACKENED_OAK_SAPLING, false)
        .tab(DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_OAK_PLANKS = BlockTemplates.planks("blackened_oak", false)
        .tab(DEEP_DARK_TAB);

    // blackened spruce wood type
    public static final RegisteredBlock STRIPPED_BLACKENED_SPRUCE_LOG = BlockTemplates.strippedLog("blackened_spruce", false, false)
        .tab(DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_SPRUCE_LOG = BlockTemplates.log("blackened_spruce", false, STRIPPED_BLACKENED_SPRUCE_LOG, false)
        .tab(DEEP_DARK_TAB);

    public static final RegisteredBlock STRIPPED_BLACKENED_SPRUCE_WOOD = BlockTemplates.strippedLog("blackened_spruce", true, false)
        .tab(DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_SPRUCE_WOOD = BlockTemplates.log("blackened_spruce", true, STRIPPED_BLACKENED_SPRUCE_WOOD, false)
        .tab(DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_SPRUCE_SAPLING = Decoration.regSapling("blackened_spruce", new BlackenedSpruceTree(), true, false);

    public static final RegisteredBlock BLACKENED_SPRUCE_LEAVES = BlockTemplates.leaves("blackened_spruce", "spruce", BLACKENED_SPRUCE_SAPLING, false)
        .tab(DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_SPRUCE_PLANKS = BlockTemplates.planks("blackened_spruce", false)
        .tab(DEEP_DARK_TAB);

    // glowing oak wood type
    public static final RegisteredBlock STRIPPED_GLOWING_OAK_LOG = BlockTemplates.strippedLog("glowing_oak", false, true)
        .tab(DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_OAK_LOG = BlockTemplates.log("glowing_oak", false, STRIPPED_GLOWING_OAK_LOG, true)
        .tab(DEEP_DARK_TAB);
    
    public static final RegisteredBlock STRIPPED_GLOWING_OAK_WOOD = BlockTemplates.strippedLog("glowing_oak", true, true)
        .tab(DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_OAK_WOOD = BlockTemplates.log("glowing_oak", true, STRIPPED_GLOWING_OAK_WOOD, true)
        .tab(DEEP_DARK_TAB);
    
    public static final RegisteredBlock GLOWING_OAK_SAPLING = Decoration.regSapling("glowing_oak", new GlowingOakTree(), true, true);
    
    public static final RegisteredBlock GLOWING_OAK_LEAVES = BlockTemplates.leaves("glowing_oak", "oak", GLOWING_OAK_SAPLING, true)
        .tab(DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_OAK_PLANKS = BlockTemplates.planks("glowing_oak", true)
        .tab(DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_CACTUS = PMRegistries.regBlockItem("glowing_cactus",
        () -> new CactusBlock(
            Block.Properties.copy(Blocks.CACTUS)
                .lightLevel(s -> 7)
        ) {
            public boolean canSustainPlant(BlockState state, BlockGetter world, BlockPos pos, Direction facing, IPlantable plantable) {
                var plant = plantable.getPlant(world, pos.relative(facing));
                if (plant.getBlock() == this)
                    return state.is(this) || state.is(BlockTags.SAND);
                return false;
            };
        }
    )
        .tab(DEEP_DARK_TAB)
        .blockStateGenerator((block, generator) -> generator.simpleBlock(block, generator.existingModel("glowing_cactus")))
        .localizedName("Glowing Cactus", "Cactus brillante");

    public static final RegisteredBlock GLOWING_CACTUS_BLOCK = PMRegistries.regBlockItem(
        "glowing_cactus_block",
        () -> new Block(BlockType.WOOD.getProperties().lightLevel(s -> 7))
    )
        .tab(DEEP_DARK_TAB)
        .tag(BlockTags.PLANKS)
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.BUILDING_BLOCKS, item)
                .pattern("cc")
                .pattern("cc")
                .define('c', GLOWING_CACTUS)
        )
        .localizedName("Glowing Cactus Block", "Bloque de cactus brillante");

    public static void init(IEventBus eventbus) {
        Events.registerRainbowBlocks(GLOWING_NYLIUM, GLOWING_OAK_LEAVES, GLOWING_SHRUB);
        Events.registerBasicColoredBlocks(0x1A1A1A, BLACKENED_OAK_LEAVES, BLACKENED_SHRUB);
        Events.registerBasicColoredBlocks(0x363636, BLACKENED_SPRUCE_LEAVES);
    }

    private static RegisteredBlock getGlowshroomBlock(boolean isStem) {
        var name = "glowshroom_" + (isStem ? "stem" : "block");

        return PMRegistries.regBlockItem(
            name,
            () -> new HugeMushroomBlock(
                Block.Properties.copy(Blocks.RED_MUSHROOM_BLOCK)
                    .lightLevel(s -> 7)
                    .noOcclusion()
            ) {
                @Override
                public boolean skipRendering(BlockState state, BlockState adjacentBlockState, Direction side) {
                    return adjacentBlockState.is(this);
                }
            }
        )
            .tab(DEEP_DARK_TAB)
            .itemModel(
                (block, generator) -> generator.withExistingParent(name, "minecraft:block/cube_all")
                    .texture("all", "paradisemod:block/" + name)
            )
            .blockStateGenerator(
                (block, generator) -> {
                    var modelBuilder = generator.models();
                    var outside = modelBuilder.withExistingParent("glowshroom_" + (isStem ? "stem" : "block"), "block/template_single_face")
                        .texture("texture", "block/glowshroom_" + (isStem ? "stem" : "block"))
                        .renderType("translucent");

                    var inside = modelBuilder.withExistingParent("glowshroom_block_inside", "block/template_single_face")
                        .texture("texture", "block/glowshroom_block_inside")
                        .renderType("translucent");

                    generator.getMultipartBuilder(block.get())
                        .part()
                            .modelFile(outside)
                            .addModel()
                            .condition(HugeMushroomBlock.NORTH, true)
                            .end()
                        .part()
                            .modelFile(outside)
                            .rotationY(90)
                            .uvLock(true)
                            .addModel()
                            .condition(HugeMushroomBlock.EAST, true)
                            .end()
                        .part()
                            .modelFile(outside)
                            .rotationY(180)
                            .uvLock(true)
                            .addModel()
                            .condition(HugeMushroomBlock.SOUTH, true)
                            .end()
                        .part()
                            .modelFile(outside)
                            .rotationY(270)
                            .uvLock(true)
                            .addModel()
                            .condition(HugeMushroomBlock.WEST, true)
                            .end()
                        .part()
                            .modelFile(outside)
                            .rotationX(270)
                            .uvLock(true)
                            .addModel()
                            .condition(HugeMushroomBlock.UP, true)
                            .end()
                        .part()
                            .modelFile(outside)
                            .rotationX(90)
                            .uvLock(true)
                            .addModel()
                            .condition(HugeMushroomBlock.DOWN, true)
                            .end()
                        .part()
                            .modelFile(inside)
                            .addModel()
                            .condition(HugeMushroomBlock.NORTH, false)
                            .end()
                        .part()
                            .modelFile(inside)
                            .rotationY(90)
                            .uvLock(true)
                            .addModel()
                            .condition(HugeMushroomBlock.EAST, false)
                            .end()
                        .part()
                            .modelFile(inside)
                            .rotationY(180)
                            .uvLock(true)
                            .addModel()
                            .condition(HugeMushroomBlock.SOUTH, false)
                            .end()
                        .part()
                            .modelFile(inside)
                            .rotationY(270)
                            .uvLock(true)
                            .addModel()
                            .condition(HugeMushroomBlock.WEST, false)
                            .end()
                        .part()
                            .modelFile(inside)
                            .rotationX(270)
                            .uvLock(true)
                            .addModel()
                            .condition(HugeMushroomBlock.UP, false)
                            .end()
                        .part()
                            .modelFile(inside)
                            .rotationX(90)
                            .uvLock(true)
                            .addModel()
                            .condition(HugeMushroomBlock.DOWN, false)
                            .end();
                    }
            )
            .lootTable((block, generator) -> generator.dropUnlessSilkTouch(block, GLOWSHROOM))
            .localizedName(
                "Glowshroom " + (isStem ? "Stem" : "Block"),
                (isStem ? "Tallo" : "Bloque") + " de hongo brillante"
            );
    }
}