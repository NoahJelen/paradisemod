package net.paradisemod.redstone;

import java.util.EnumMap;
import java.util.List;

import net.minecraft.ChatFormatting;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.network.chat.Component;
import net.minecraft.util.RandomSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.RedstoneLampBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.data.assets.PMTranslations;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;

public class Lamps {
    // colored redstone lamps
    public static EnumMap<DyeColor, RegisteredBlock> REDSTONE_LAMPS = new EnumMap<>(DyeColor.class);

    public static final RegisteredBlock BLACK_REDSTONE_LAMP = regColoredRedstoneLamp(DyeColor.BLACK);
    public static final RegisteredBlock BLUE_REDSTONE_LAMP = regColoredRedstoneLamp(DyeColor.BLUE);
    public static final RegisteredBlock BROWN_REDSTONE_LAMP = regColoredRedstoneLamp(DyeColor.BROWN);
    public static final RegisteredBlock CYAN_REDSTONE_LAMP = regColoredRedstoneLamp(DyeColor.CYAN);
    public static final RegisteredBlock GRAY_REDSTONE_LAMP = regColoredRedstoneLamp(DyeColor.GRAY);
    public static final RegisteredBlock GREEN_REDSTONE_LAMP = regColoredRedstoneLamp(DyeColor.GREEN);
    public static final RegisteredBlock LIGHT_BLUE_REDSTONE_LAMP = regColoredRedstoneLamp(DyeColor.LIGHT_BLUE);
    public static final RegisteredBlock LIGHT_GRAY_REDSTONE_LAMP = regColoredRedstoneLamp(DyeColor.LIGHT_GRAY);
    public static final RegisteredBlock LIME_REDSTONE_LAMP = regColoredRedstoneLamp(DyeColor.LIME);
    public static final RegisteredBlock MAGENTA_REDSTONE_LAMP = regColoredRedstoneLamp(DyeColor.MAGENTA);
    public static final RegisteredBlock ORANGE_REDSTONE_LAMP = regColoredRedstoneLamp(DyeColor.ORANGE);
    public static final RegisteredBlock PINK_REDSTONE_LAMP = regColoredRedstoneLamp(DyeColor.PINK);
    public static final RegisteredBlock PURPLE_REDSTONE_LAMP = regColoredRedstoneLamp(DyeColor.PURPLE);
    public static final RegisteredBlock RED_REDSTONE_LAMP = regColoredRedstoneLamp(DyeColor.RED);
    public static final RegisteredBlock WHITE_REDSTONE_LAMP = regColoredRedstoneLamp(DyeColor.WHITE);
    public static final RegisteredBlock YELLOW_REDSTONE_LAMP = regColoredRedstoneLamp(DyeColor.YELLOW);
    public static final RegisteredBlock GOLD_REDSTONE_LAMP = goldRedstoneLamp();
    public static final RegisteredBlock IRIDESCENT_REDSTONE_LAMP = iridescentRedstoneLamp();

    public static void init() { }

    private static RegisteredBlock regColoredRedstoneLamp(DyeColor color) {
        RegisteredBlock lamp =  PMRegistries.regBlockItem(
            color.name().toLowerCase() + "_redstone_lamp",
            () -> new RedstoneLampBlock(BlockBehaviour.Properties.copy(Blocks.REDSTONE_LAMP))
        )
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item, 8)
                    .pattern("lll")
                    .pattern("ldl")
                    .pattern("lll")
                    .define('d', ParadiseMod.DYES_BY_COLOR.get(color))
                    .define('l', Blocks.REDSTONE_LAMP)
            )
            .tabs(CreativeModeTabs.REDSTONE_BLOCKS, CreativeModeTabs.COLORED_BLOCKS)
            .tag(PMTags.Blocks.BUOY_LIGHTS)
            .itemModel((block, generator) -> generator.parentBlockItem(block.get(), "colored_redstone_lamp"))
            .blockStateGenerator(
                (block, generator) -> {
                    var model = generator.existingModel("colored_redstone_lamp");
                    var modelOn = generator.existingModel("colored_redstone_lamp_on");

                    generator.getVariantBuilder(block.get())
                        .forAllStates(
                            state -> {
                                boolean isOn = state.getValue(RedstoneLampBlock.LIT);

                                if(isOn)
                                    return generator.buildVariantModel(modelOn);
                                else 
                                    return generator.buildVariantModel(model);
                            }
                        );
                }
            )
            .localizedName(
                PMTranslations.englishColor(color) + " Redstone Lamp",
                "Lámpara " + PMTranslations.spanishColor(color, true)
            );

        REDSTONE_LAMPS.put(color, lamp);
        return lamp;
    }

    private static RegisteredBlock goldRedstoneLamp() {
        return PMRegistries.regBlockItem(
            "gold_redstone_lamp",
            () -> new RedstoneLampBlock(Block.Properties.copy(Blocks.REDSTONE_LAMP)) {
                @Override
                public void stepOn(Level world, BlockPos pos, BlockState state, Entity entity) {
                    if(entity instanceof LivingEntity creature)
                        creature.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 100));
                }

                // I finally figured out to add information to blocks and items, ChaosDog!
                @Override
                public void appendHoverText(ItemStack pStack, BlockGetter world, List<Component> tooltip, TooltipFlag flag) {
                    tooltip.add(Component.translatable("paradisemod.no_more_school").withStyle(ChatFormatting.LIGHT_PURPLE));
                }

                @Override
                public void animateTick(BlockState state, Level world, BlockPos pos, RandomSource rand) {
                    super.animateTick(state, world, pos, rand);
                    if (rand.nextInt(10) == 0)
                        world.addParticle(ParticleTypes.AMBIENT_ENTITY_EFFECT, pos.getX() + rand.nextDouble(), pos.getY() + 1.1D, pos.getZ() + rand.nextDouble(), 0., 0., 0.);
                }
            }
        )
            .tabs(CreativeModeTabs.REDSTONE_BLOCKS, CreativeModeTabs.COLORED_BLOCKS)
            .tag(PMTags.Blocks.BUOY_LIGHTS)
            .itemModel((block, generator) -> generator.parentBlockItem(block.get(), "colored_redstone_lamp"))
            .blockStateGenerator(
                (block, generator) -> {
                    var model = generator.existingModel("colored_redstone_lamp");
                    var modelOn = generator.existingModel("colored_redstone_lamp_on");

                    generator.getVariantBuilder(block.get())
                        .forAllStates(
                            state -> {
                                boolean isOn = state.getValue(RedstoneLampBlock.LIT);

                                if(isOn)
                                    return generator.buildVariantModel(modelOn);
                                else 
                                    return generator.buildVariantModel(model);
                            }
                        );
                }
            )
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item)
                    .pattern("GGG")
                    .pattern("GlG")
                    .pattern("GGG")
                    .define('G', Blocks.GOLD_BLOCK)
                    .define('l', Blocks.REDSTONE_LAMP)
            )
            .localizedName("Golden Redstone Lamp", "Lámpara dorada");
    }

    private static RegisteredBlock iridescentRedstoneLamp() {
        return PMRegistries.regBlockItem(
            "iridescent_redstone_lamp",
            () -> new RedstoneLampBlock(Block.Properties.copy(Blocks.REDSTONE_LAMP)) {
                @Override
                public void animateTick(BlockState state, Level world, BlockPos pos, net.minecraft.util.RandomSource rand) {
                    world.sendBlockUpdated(pos, state, state, 8);
                };
            }
        )
            .tabs(CreativeModeTabs.REDSTONE_BLOCKS, CreativeModeTabs.COLORED_BLOCKS)
            .tag(PMTags.Blocks.BUOY_LIGHTS)
            .itemModel((block, generator) -> generator.parentBlockItem(block.get(), "colored_redstone_lamp"))
            .blockStateGenerator(
                (block, generator) -> {
                    var model = generator.existingModel("colored_redstone_lamp");
                    var modelOn = generator.existingModel("colored_redstone_lamp_on");

                    generator.getVariantBuilder(block.get())
                        .forAllStates(
                            state -> {
                                boolean isOn = state.getValue(RedstoneLampBlock.LIT);

                                if(isOn)
                                    return generator.buildVariantModel(modelOn);
                                else 
                                    return generator.buildVariantModel(model);
                            }
                        );
                }
            )
            .recipe(
                (item, generator) -> generator.shapelessRecipe(
                    RecipeCategory.REDSTONE,
                    item,
                    Items.RED_DYE,
                    Items.ORANGE_DYE,
                    Items.YELLOW_DYE,
                    Items.GREEN_DYE,
                    Items.CYAN_DYE,
                    Items.BLUE_DYE,
                    Items.PURPLE_DYE
                )
            )
            .localizedName("Iridescent Redstone Lamp", "Lámpara iridescente");
    }
}