package net.paradisemod.redstone.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.PressurePlateBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockSetType;
import net.minecraft.world.level.gameevent.GameEvent;

public class CustomPressurePlate extends PressurePlateBlock {
    private final int ticks;
    private final Sensitivity sensitivity;
    private final BlockSetType blockType;

    public CustomPressurePlate(Sensitivity sensitivity, BlockSetType blockType, Properties properties, int plateTicks) {
        super(PressurePlateBlock.Sensitivity.EVERYTHING, properties, blockType);
        this.sensitivity = sensitivity;
        ticks = plateTicks;
        this.blockType = blockType;
    }

    @Override
    public void tick(BlockState state, ServerLevel world, BlockPos pos, RandomSource rand) {
        var i = this.getSignalForState(state);
        if (i > 0) checkPressed((Entity)null, world, pos, state, i);
    }

    @Override
    public void entityInside(BlockState state, Level world, BlockPos pos, Entity entity) {
        if (!world.isClientSide) {
            var i = getSignalForState(state);
            if (i == 0) checkPressed(entity, world, pos, state, i);
        }
    }

    private void checkPressed(Entity entity, Level world, BlockPos pos, BlockState state, int signalStrength) {
        int i = getSignalStrength(world, pos);
        var flag = signalStrength > 0;
        var flag1 = i > 0;

        if (signalStrength != i) {
            var blockstate = this.setSignalForState(state, i);
            world.setBlock(pos, blockstate, 2);
            updateNeighbours(world, pos);
            world.setBlocksDirty(pos, state, blockstate);
        }

        if (!flag1 && flag) {
            world.playSound(null, pos, blockType.pressurePlateClickOff(), SoundSource.BLOCKS);
            world.gameEvent(entity, GameEvent.BLOCK_DEACTIVATE, pos);
        }
        else if (flag1 && !flag) {
            world.playSound(null, pos, blockType.pressurePlateClickOn(), SoundSource.BLOCKS);
            world.gameEvent(entity, GameEvent.BLOCK_ACTIVATE, pos);
        }
        if (flag1) world.scheduleTick(pos, this, ticks);
    }

    @Override
    public int getPressedTime() { return ticks; }

    @Override
    public int getSignalStrength(Level world, BlockPos pos) {
        var axisalignedbb = TOUCH_AABB.move(pos);
        var entities = switch (sensitivity) {
            case EVERYTHING -> world.getEntities(null, axisalignedbb);
            case MOBS -> world.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            case PLAYERS -> world.getEntitiesOfClass(Player.class, axisalignedbb);
        };

        for(var entity : entities)
            if (!entity.isIgnoringBlockTriggers()) return 15;

        return 0;
    }

    public enum Sensitivity {
        EVERYTHING,
        MOBS,
        PLAYERS
    }
}
