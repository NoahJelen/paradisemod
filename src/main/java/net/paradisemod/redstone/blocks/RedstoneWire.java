package net.paradisemod.redstone.blocks;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.DustParticleOptions;
import net.minecraft.util.RandomSource;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.DaylightDetectorBlock;
import net.minecraft.world.level.block.DiodeBlock;
import net.minecraft.world.level.block.ObserverBlock;
import net.minecraft.world.level.block.SimpleWaterloggedBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;

public class RedstoneWire extends Block implements SimpleWaterloggedBlock, ModeledBlock {
    private static final VoxelShape CENTER = Block.box(6, 6, 6, 10, 10, 10); // the inputs on this are very concerning...
    private static final VoxelShape CONNECT_NORTH = Block.box(6, 6, 0, 10, 10, 6);
    private static final VoxelShape CONNECT_NORTH_NORMAL = Block.box(6, 0, 0, 10, 6, 4);
    private static final VoxelShape CONNECT_SOUTH = Block.box(6, 6, 10, 10, 10, 16);
    private static final VoxelShape CONNECT_SOUTH_NORMAL = Block.box(6, 0, 12, 10, 6, 16);
    private static final VoxelShape CONNECT_EAST = Block.box(10, 6, 6, 16, 10, 10);
    private static final VoxelShape CONNECT_EAST_NORMAL = Block.box(12, 0, 6, 16, 6, 10);
    private static final VoxelShape CONNECT_WEST = Block.box(0, 6, 6, 6, 10, 10);
    private static final VoxelShape CONNECT_WEST_NORMAL = Block.box(0, 0, 6, 4, 6, 10);
    private static final VoxelShape CONNECT_UP = Block.box(6, 10, 6, 10, 16, 10);
    private static final VoxelShape CONNECT_DOWN = Block.box(6, 0, 6, 10, 6, 10);
    private static final HashMap<BlockState, VoxelShape> SHAPES_CACHE = new HashMap<>();

    public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;
    public static final IntegerProperty POWER = BlockStateProperties.POWER;
    public static final BooleanProperty UP = BooleanProperty.create("up");
    public static final BooleanProperty DOWN = BooleanProperty.create("down");
    public static final EnumProperty<Connection> NORTH = EnumProperty.create("north", Connection.class);
    public static final EnumProperty<Connection> SOUTH = EnumProperty.create("south", Connection.class);
    public static final EnumProperty<Connection> EAST = EnumProperty.create("east", Connection.class);
    public static final EnumProperty<Connection> WEST = EnumProperty.create("west", Connection.class);
    public static final EnumProperty<BaseDirection> BASEDIR = EnumProperty.create("basedir", BaseDirection.class);

    public RedstoneWire() {
        super(Properties.copy(Blocks.OAK_PRESSURE_PLATE).sound(SoundType.METAL));
        registerDefaultState(
            stateDefinition.any()
                .setValue(POWER, 0)
                .setValue(UP, false)
                .setValue(DOWN, false)
                .setValue(NORTH, Connection.NONE)
                .setValue(SOUTH, Connection.NONE)
                .setValue(EAST, Connection.NONE)
                .setValue(WEST, Connection.NONE)
                .setValue(BASEDIR, BaseDirection.NONE)
                .setValue(WATERLOGGED, false)
        );
        if(SHAPES_CACHE.isEmpty()) {
            for (var state : getStateDefinition().getPossibleStates())
                if (state.getValue(POWER) == 0 && state.getValue(BASEDIR) == BaseDirection.NONE && !state.getValue(WATERLOGGED)) SHAPES_CACHE.put(state, calcShape(state));
        }
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) { builder.add(WATERLOGGED, POWER, UP, DOWN, NORTH, SOUTH, EAST, WEST, BASEDIR); }

    @Override
    public void neighborChanged(BlockState state, Level world, BlockPos pos, @Nullable Block block, @Nullable BlockPos fromPos, boolean isMoving) {
        ArrayList<Integer> adjStrengths = new ArrayList<>();
        adjStrengths.add(0);
        var newState = state;
        ArrayList<Direction> baseDirs = new ArrayList<>();
        for(var facing : Direction.values()) {
            var facingPos = pos.relative(facing);
            var testState = world.getBlockState(facingPos);
            var testBlock = testState.getBlock();
            if(testBlock instanceof ObserverBlock) 
                if(facing != testState.getValue(ObserverBlock.FACING)) continue;

            var signal = world.getSignal(facingPos, facing);
            if(signal <= 15) adjStrengths.add(signal);

            var connectable = testState.canRedstoneConnectTo(world, facingPos, facing) || testState.getBlock() instanceof RedstoneWire;
            newState = switch (facing) {
                case UP -> newState.setValue(UP, connectable);
                case DOWN -> newState.setValue(DOWN, connectable);
                case EAST -> newState.setValue(EAST, Connection.get(testState, connectable));
                case WEST -> newState.setValue(WEST, Connection.get(testState, connectable));
                case NORTH -> newState.setValue(NORTH, Connection.get(testState, connectable));
                case SOUTH -> newState.setValue(SOUTH, Connection.get(testState, connectable));
            };
            if(!connectable) {
                var relPos = pos.relative(facing);
                var relState = world.getBlockState(relPos);
                if(relState.isFaceSturdy(world, relPos, facing.getOpposite())) baseDirs.add(facing);
            }
        }

        var dir = !baseDirs.isEmpty() ? BaseDirection.fromDir(baseDirs.get(0)) : BaseDirection.NONE;
        newState = newState.setValue(BASEDIR, dir);
        var greatestStrength = Utils.getMaxValue(adjStrengths);
        world.setBlock(pos, newState.setValue(POWER, greatestStrength), 3);
    }

    @Override
    public void onPlace(BlockState state, Level world, BlockPos pos, BlockState oldState, boolean isMoving) { neighborChanged(state, world, pos, null, null, false); }

    @Override
    public int getSignal(BlockState state, BlockGetter reader, BlockPos pos, Direction otherFacing) {
        var wireFacing = otherFacing.getOpposite();
        var otherState = reader.getBlockState(pos.relative(wireFacing));
        var otherBlock = otherState.getBlock();
        var connected = false;

        switch(wireFacing) {
            case UP -> {
                if (otherBlock instanceof RedstoneWire) connected = state.getValue(UP);
                else connected = state.getValue(DOWN);
            }

            case DOWN -> {
                if (otherBlock instanceof RedstoneWire) connected = state.getValue(DOWN);
                else connected = state.getValue(UP);
            }

            case NORTH -> connected = state.getValue(NORTH).connected();
            case SOUTH -> connected = state.getValue(SOUTH).connected();
            case EAST -> connected = state.getValue(EAST).connected();
            case WEST -> connected = state.getValue(WEST).connected();
        }

        var power = state.getValue(POWER) - 1;
        if(power < 0) power = 0;
        return connected ? power : 0;
    }

    @Override
    public boolean canConnectRedstone(BlockState state, BlockGetter world, BlockPos pos, Direction side) { return true; }

    @Override
    public boolean isSignalSource(BlockState state) { return true; }

    @Override
    public VoxelShape getCollisionShape(BlockState state, BlockGetter world, BlockPos pos, CollisionContext ctx) { return getShape(state, world, pos, ctx); }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter reader, BlockPos pos, CollisionContext ctx) {
        return SHAPES_CACHE.get(state.setValue(POWER, 0).setValue(BASEDIR, BaseDirection.NONE).setValue(WATERLOGGED, false));
    }

    private static VoxelShape calcShape(BlockState state) {
        var shape = CENTER;
        if(state.getValue(UP)) shape = Shapes.or(shape, CONNECT_UP);
        if(state.getValue(DOWN)) shape = Shapes.or(shape, CONNECT_DOWN);
        if(state.getValue(NORTH).connected()) shape = Shapes.or(shape, CONNECT_NORTH);
        if(state.getValue(NORTH) == Connection.NORMAL) shape = Shapes.or(shape, CONNECT_NORTH_NORMAL);
        if(state.getValue(SOUTH).connected()) shape = Shapes.or(shape, CONNECT_SOUTH);
        if(state.getValue(SOUTH) == Connection.NORMAL) shape = Shapes.or(shape, CONNECT_SOUTH_NORMAL);
        if(state.getValue(EAST).connected()) shape = Shapes.or(shape, CONNECT_EAST);
        if(state.getValue(EAST) == Connection.NORMAL) shape = Shapes.or(shape, CONNECT_EAST_NORMAL);
        if(state.getValue(WEST).connected()) shape = Shapes.or(shape, CONNECT_WEST);
        if(state.getValue(WEST) == Connection.NORMAL) shape = Shapes.or(shape, CONNECT_WEST_NORMAL);
        return shape;
    }

    @Override
    @Nullable
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        var fluidstate = context.getLevel().getFluidState(context.getClickedPos());
        var flag = fluidstate.getType() == Fluids.WATER;
        return super.getStateForPlacement(context).setValue(WATERLOGGED, flag);
    }

    @Override
    public BlockState updateShape(BlockState stateIn, Direction facing, BlockState facingState, LevelAccessor world, BlockPos currentPos, BlockPos facingPos) {
        if(stateIn.getValue(WATERLOGGED)) world.scheduleTick(currentPos, Fluids.WATER, Fluids.WATER.getTickDelay(world));
        return super.updateShape(stateIn, facing, facingState, world, currentPos, facingPos);
    }

    @Override
    public FluidState getFluidState(BlockState state) { return state.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(state); }

    @Override
    public void animateTick(BlockState state, Level world, BlockPos pos, RandomSource rand) {
        if (state.getValue(POWER) > 0) {
            var d0 = pos.getX() + 0.5D + (rand.nextDouble() - 0.5) * 0.2;
            var d1 = pos.getY() + 0.4D + (rand.nextDouble() - 0.5) * 0.2;
            var d2 = pos.getZ() + 0.5D + (rand.nextDouble() - 0.5) * 0.2;
            var f = -5F / 16F;
            var d3 = f * Direction.UP.getStepX();
            var d4 = f * Direction.UP.getStepZ();
            world.addParticle(DustParticleOptions.REDSTONE, d0 + d3, d1, d2 + d4, 0.0D, 0.0D, 0.0D);
        }
    }

    @Override
    public void genBlockState(BlockStateGenerator generator) {
        var base = generator.existingModel("redstone_wire/base");
        var center = generator.existingModel("redstone_wire/center");
        var link = generator.existingModel("redstone_wire/link");
        var linkNormal = generator.existingModel("redstone_wire/link_normal");

        generator.getMultipartBuilder(this)
            .part()
                .modelFile(center)
                .addModel()
                .end()
            .part()
                .modelFile(base)
                .rotationX(90)
                .addModel()
                .condition(BASEDIR, BaseDirection.DOWN)
                .condition(DOWN, false)
                .end()
            .part()
                .modelFile(base)
                .rotationX(270)
                .addModel()
                .condition(BASEDIR, BaseDirection.UP)
                .condition(UP, false)
                .end()
            .part()
                .modelFile(base)
                .addModel()
                .condition(BASEDIR, BaseDirection.NORTH)
                .condition(NORTH, Connection.NONE)
                .end()
            .part()
                .modelFile(base)
                .rotationY(180)
                .addModel()
                .condition(BASEDIR, BaseDirection.SOUTH)
                .condition(SOUTH, Connection.NONE)
                .end()
            .part()
                .modelFile(base)
                .rotationY(90)
                .addModel()
                .condition(BASEDIR, BaseDirection.EAST)
                .condition(EAST, Connection.NONE)
                .end()
            .part()
                .modelFile(base)
                .rotationY(270)
                .addModel()
                .condition(BASEDIR, BaseDirection.WEST)
                .condition(WEST, Connection.NONE)
                .end()
            .part()
                .modelFile(link)
                .rotationX(270)
                .addModel()
                .condition(UP, true)
                .end()
            .part()
                .modelFile(link)
                .rotationX(90)
                .addModel()
                .condition(DOWN, true)
                .end()
            .part()
                .modelFile(link)
                .rotationY(90)
                .addModel()
                .condition(EAST, Connection.WIRE)
                .end()
            .part()
                .modelFile(linkNormal)
                .rotationY(90)
                .addModel()
                .condition(EAST, Connection.NORMAL)
                .end()
            .part()
                .modelFile(link)
                .rotationY(270)
                .addModel()
                .condition(WEST, Connection.WIRE)
                .end()
            .part()
                .modelFile(linkNormal)
                .rotationY(270)
                .addModel()
                .condition(WEST, Connection.NORMAL)
                .end()
            .part()
                .modelFile(link)
                .addModel()
                .condition(NORTH, Connection.WIRE)
                .end()
            .part()
                .modelFile(linkNormal)
                .addModel()
                .condition(NORTH, Connection.NORMAL)
                .end()
            .part()
                .modelFile(link)
                .rotationY(180)
                .addModel()
                .condition(SOUTH, Connection.WIRE)
                .end()
            .part()
                .modelFile(linkNormal)
                .rotationY(180)
                .addModel()
                .condition(SOUTH, Connection.NORMAL)
                .end();
    }

    public enum Connection implements StringRepresentable {
        WIRE,
        NORMAL,
        NONE;

        @Override
        public String getSerializedName() { return name().toLowerCase(); }

        public static Connection get(BlockState testState, boolean connectable) {
            var testBlock = testState.getBlock();

            if(connectable) {
                if(testBlock instanceof DiodeBlock || testBlock instanceof Antenna || testBlock instanceof Transmitter || testBlock instanceof DaylightDetectorBlock) return NORMAL;
                else if (testState.canOcclude() || testBlock instanceof RedstoneWire || testBlock instanceof WireRepeater) return WIRE;
                else return NORMAL;
            }
            else return NONE;
        }

        public boolean connected() { return this == NORMAL || this == WIRE; }
    }

    public enum BaseDirection implements StringRepresentable {
        UP,
        DOWN,
        EAST,
        WEST,
        NORTH,
        SOUTH,
        NONE;

        @Override
        public String getSerializedName() { return name().toLowerCase(); }

        public static BaseDirection fromDir(Direction direction) {
            return switch (direction) {
                case DOWN -> DOWN;
                case UP -> UP;
                case NORTH -> NORTH;
                case SOUTH -> SOUTH;
                case WEST -> WEST;
                case EAST -> EAST;
            };
        }
    }
}