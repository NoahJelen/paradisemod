package net.paradisemod.redstone.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.DustParticleOptions;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraft.world.ticks.TickPriority;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ItemModelGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;

public class Antenna extends Block implements ModeledBlock {
    public static final BooleanProperty POWERED = BlockStateProperties.POWERED;
    private static final VoxelShape SHAPE1 = Block.box(2, 0, 2, 14, 2, 14);
    private static final VoxelShape SHAPE2 = Block.box(4, 2, 4, 12, 5, 12);
    private static final VoxelShape SHAPE3 = Block.box(7, 5, 7, 9, 15, 9);
    private final DyeColor color;

    public Antenna(DyeColor color) {
        super(Properties.copy(Blocks.REPEATER));
        registerDefaultState(stateDefinition.any().setValue(POWERED, false));
        this.color = color;
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) { builder.add(POWERED); }

    @Override
    public void tick(BlockState state, ServerLevel world, BlockPos pos, RandomSource rand) {
        // look for a powered sending transmitter
        var foundPowered = false;
        for(var i = -16; i <= 16; i++) {
            for(var j = -16; j <= 16; j++) {
                for(var k = -16; k <= 16; k++) {
                    var newPos = pos.offset(i, j, k);
                    var testState = world.getBlockState(newPos);
                    var block = testState.getBlock();
                    if(block instanceof Transmitter transmitter) {
                        if(transmitter.color == color && !testState.getValue(Transmitter.RECEIVER) && testState.getValue(Transmitter.POWERED)) {
                            foundPowered = true;
                            break;
                        }
                    }
                }
                if(foundPowered) break;
            }
            if(foundPowered) break;
        }

        world.setBlock(pos, state.setValue(POWERED, foundPowered), 3);
        for(var direction : Direction.values()) world.updateNeighborsAt(pos.relative(direction), this);
        world.scheduleTick(pos, this, 2, TickPriority.VERY_HIGH);
    }

    @Override
    public int getSignal(BlockState state, BlockGetter read, BlockPos pos, Direction side) { return state.getValue(POWERED) ? 15 : 0; }

    @Override
    public int getDirectSignal(BlockState state, BlockGetter reader, BlockPos pos, Direction side) { return getSignal(state, reader, pos, side); }

    @Override
    public boolean canConnectRedstone(BlockState state, BlockGetter world, BlockPos pos, Direction side) { return true; }

    @Override
    public void animateTick(BlockState state, Level world, BlockPos pos, RandomSource rand) {
        if (state.getValue(POWERED)) {
            var d0 = pos.getX() + 0.5D + (rand.nextDouble() - 0.5D) * 0.2D;
            var d1 = pos.getY() + 0.4D + (rand.nextDouble() - 0.5D) * 0.2D;
            var d2 = pos.getZ() + 0.5D + (rand.nextDouble() - 0.5D) * 0.2D;
            var f = -5F / 16F;
            var d3 = f * Direction.UP.getStepX();
            var d4 = f * Direction.UP.getStepZ();
            world.addParticle(DustParticleOptions.REDSTONE, d0 + d3, d1, d2 + d4, 0.0D, 0.0D, 0.0D);
        }
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter reader, BlockPos pos, CollisionContext ctx) {
        return Shapes.or(SHAPE1, SHAPE2, SHAPE3);
    }

    @Override
    public void neighborChanged(BlockState state, Level world, BlockPos pos, Block block, BlockPos fromPos, boolean isMoving) {
        if (canSurvive(state, world, pos)) return;
        dropResources(state, world, pos);
        world.setBlockAndUpdate(pos, Blocks.AIR.defaultBlockState());
    }

    @Override
    public boolean canSurvive(BlockState state, LevelReader world, BlockPos pos) {
        var blockBelow = world.getBlockState(pos.below());
        return !blockBelow.isAir();
    }

    @Override
    public void onPlace(BlockState state, Level world, BlockPos pos, BlockState oldState, boolean isMoving) { world.scheduleTick(pos, this, 2, TickPriority.VERY_HIGH); }

    @Override
    public boolean isSignalSource(BlockState pState) { return true; }

    @Override
    public void genItemModel(ItemModelGenerator generator) {
        generator.parentBlockItem(this, "antenna/" + color.getName());
    }

    @Override
    public void genBlockState(BlockStateGenerator generator) {
        var modelBuilder = generator.models();
        var antenna = modelBuilder.withExistingParent("block/antenna/" + color.getName(), generator.modLoc("block/antenna/parent"))
            .texture("texture", "block/antenna/" + color.getName());

        var antennaOn = modelBuilder.withExistingParent("block/antenna/" + color.getName() + "_on", generator.modLoc("block/antenna/parent_on"))
            .texture("texture", "block/antenna/" + color.getName());

        generator.getVariantBuilder(this)
            .forAllStates(
                state -> {
                    if(state.getValue(POWERED))
                        return generator.buildVariantModel(antennaOn);
                    else
                        return generator.buildVariantModel(antenna);
                }
            );
    }
}