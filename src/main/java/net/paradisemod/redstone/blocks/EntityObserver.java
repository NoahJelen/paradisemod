package net.paradisemod.redstone.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.ticks.TickPriority;
import net.minecraftforge.client.model.generators.ModelFile;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ItemModelGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;

public class EntityObserver extends Block implements ModeledBlock {
    public static final IntegerProperty DISTANCE = IntegerProperty.create("distance", 1, 4);
    public static final BooleanProperty POWERED = BlockStateProperties.POWERED;
    public static final DirectionProperty FACING = BlockStateProperties.FACING;

    public EntityObserver() {
        super(Properties.copy(Blocks.OBSERVER));
        registerDefaultState(
            stateDefinition.any()
                .setValue(DISTANCE, 1)
                .setValue(POWERED, false)
                .setValue(FACING, Direction.EAST)
        );
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) { builder.add(DISTANCE, POWERED, FACING); }

    @Override
    public int getSignal(BlockState state, BlockGetter reader, BlockPos pos, Direction side) { return state.getValue(POWERED) && state.getValue(FACING) == side ? 15 : 0; }

    @Override
    public int getDirectSignal(BlockState state, BlockGetter reader, BlockPos pos, Direction side) { return state.getSignal(reader, pos, side); }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext ctx) { return defaultBlockState().setValue(FACING, ctx.getNearestLookingDirection().getOpposite().getOpposite()); }

    @Override
    public void onPlace(BlockState state, Level world, BlockPos pos, BlockState oldState, boolean isMoving) { world.scheduleTick(pos, this, 2, TickPriority.VERY_HIGH); }

    @Override
    public InteractionResult use(BlockState state, Level world, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hit) {
        if (!player.getAbilities().mayBuild) return InteractionResult.PASS;
        else {
            world.setBlock(pos, state.cycle(DISTANCE), 3);
            return InteractionResult.sidedSuccess(world.isClientSide);
        }
    }

    @Override
    public void tick(BlockState state, ServerLevel world, BlockPos pos, RandomSource rand) {
        for(var i = 1; i <= state.getValue(DISTANCE); i++) {
            var readPos = pos.relative(state.getValue(FACING), i);
            if(world.getBlockState(readPos).canOcclude()) break;
            var box = new AABB(readPos, readPos.offset(1, 1, 1));
            var entities = world.getEntities(null, box);
            var powered = state.getValue(POWERED);
            if(!entities.isEmpty()) world.setBlock(pos, state.setValue(POWERED, !powered), 3);
            else if(powered) world.setBlock(pos, state.setValue(POWERED, false), 3);
        }

        for(var direction : Direction.values()) world.updateNeighborsAt(pos.relative(direction), this);
        world.scheduleTick(pos, this, 2, TickPriority.VERY_HIGH);
    }

    @Override
    public boolean isSignalSource(BlockState pState) { return true; }

    @Override
    public boolean canConnectRedstone(BlockState state, BlockGetter world, BlockPos pos, Direction side) { return side == state.getValue(FACING); }

    @Override
    public void genItemModel(ItemModelGenerator generator) {
        generator.parentBlockItem(this, "entity_observer/off_1");
    }

    @Override
    public void genBlockState(BlockStateGenerator generator) {
        var modelBuilder = generator.models();

        ModelFile[] offModels = {
            modelBuilder.withExistingParent("block/entity_observer/off_1", generator.modLoc("block/entity_observer/parent"))
                .texture("top", "block/entity_observer/top_1"),

            modelBuilder.withExistingParent("block/entity_observer/off_2", generator.modLoc("block/entity_observer/parent"))
                .texture("top", "block/entity_observer/top_2"),

            modelBuilder.withExistingParent("block/entity_observer/off_3", generator.modLoc("block/entity_observer/parent"))
                .texture("top", "block/entity_observer/top_3"),

            modelBuilder.withExistingParent("block/entity_observer/off_4", generator.modLoc("block/entity_observer/parent"))
                .texture("top", "block/entity_observer/top_4")
        };

        ModelFile[] onModels = {
            modelBuilder.withExistingParent("block/entity_observer/on_1", generator.modLoc("block/entity_observer/parent_on"))
                .texture("top", "block/entity_observer/top_1"),

            modelBuilder.withExistingParent("block/entity_observer/on_2", generator.modLoc("block/entity_observer/parent_on"))
                .texture("top", "block/entity_observer/top_2"),

            modelBuilder.withExistingParent("block/entity_observer/on_3", generator.modLoc("block/entity_observer/parent_on"))
                .texture("top", "block/entity_observer/top_3"),

            modelBuilder.withExistingParent("block/entity_observer/on_4", generator.modLoc("block/entity_observer/parent_on"))
                .texture("top", "block/entity_observer/top_4")
        };

        generator.getVariantBuilder(this)
            .forAllStates(
                state -> {
                    int distance = state.getValue(DISTANCE);
                    boolean powered = state.getValue(POWERED);
                    Direction facing = state.getValue(FACING);

                    var model = offModels[distance - 1];
                    if(powered) model = onModels[distance - 1];
                    return generator.buildVariantModel(model, facing);
                }
            );
    }
}