package net.paradisemod.redstone.blocks;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.DustParticleOptions;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.DiodeBlock;
import net.minecraft.world.level.block.ObserverBlock;
import net.minecraft.world.level.block.RedStoneWireBlock;
import net.minecraft.world.level.block.SimpleWaterloggedBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraft.world.ticks.TickPriority;
import net.minecraftforge.client.model.generators.ModelFile;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ItemModelGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;

public class WireRepeater extends Block implements SimpleWaterloggedBlock, ModeledBlock {
    private static final VoxelShape SHAPE_NS = Block.box(3, 3, 2, 13, 13, 14);
    private static final VoxelShape SHAPE_EW = Block.box(2, 3, 3, 14, 13, 13);
    private static final VoxelShape SHAPE_UD = Block.box(3, 2, 3, 13, 14, 13);
    public static final BooleanProperty POWERED = BlockStateProperties.POWERED;
    public static final DirectionProperty FACING = BlockStateProperties.FACING;
    public static final IntegerProperty DELAY = BlockStateProperties.DELAY;
    public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;

    public WireRepeater() {
        super(Properties.copy(Blocks.OAK_PRESSURE_PLATE).sound(SoundType.METAL));
        registerDefaultState(
            stateDefinition.any()
                .setValue(FACING, Direction.NORTH)
                .setValue(DELAY, 1)
                .setValue(POWERED, false)
                .setValue(WATERLOGGED, false)
        );
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) { builder.add(POWERED, FACING, DELAY, WATERLOGGED); }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext ctx) {
        var fluid = ctx.getLevel().getFluidState(ctx.getClickedPos());
        var waterlogged = fluid.getType() == Fluids.WATER;
        var facing = ctx.getNearestLookingDirection();
        return defaultBlockState().setValue(FACING, facing).setValue(WATERLOGGED, waterlogged);
    }

    @Override
    public boolean canConnectRedstone(BlockState state, BlockGetter world, BlockPos pos, Direction side) {
        var facing = state.getValue(FACING);
        return side == facing || side == facing.getOpposite();
    }

    @Override
    public boolean isSignalSource(BlockState state) { return true; }

    @Override
    public BlockState updateShape(BlockState stateIn, Direction facing, BlockState facingState, LevelAccessor world, BlockPos currentPos, BlockPos facingPos) {
        if(stateIn.getValue(WATERLOGGED)) world.scheduleTick(currentPos, Fluids.WATER, Fluids.WATER.getTickDelay(world));
        return super.updateShape(stateIn, facing, facingState, world, currentPos, facingPos);
    }

    @Override
    public FluidState getFluidState(BlockState state) { return state.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(state); }

    @Override
    public void animateTick(BlockState state, Level world, BlockPos pos, RandomSource rand) {
        if (state.getValue(POWERED)) {
            var direction = state.getValue(FACING);
            var d0 = pos.getX() + 0.5 + (rand.nextDouble() - 0.5) * 0.2;
            var d1 = pos.getY() + 0.4 + (rand.nextDouble() - 0.5) * 0.2;
            var d2 = pos.getZ() + 0.5 + (rand.nextDouble() - 0.5) * 0.2;
            var f = -5F / 16F;
            var d3 = f * direction.getStepX();
            var d4 = f * direction.getStepZ();
            world.addParticle(DustParticleOptions.REDSTONE, d0 + d3, d1, d2 + d4, 0.0D, 0.0D, 0.0D);
        }
    }

    @Override
    public int getSignal(BlockState state, BlockGetter reader, BlockPos pos, Direction otherFacing) { return state.getValue(POWERED) && otherFacing == state.getValue(FACING).getOpposite() ? 15 : 0; }

    @Override
    public int getDirectSignal(BlockState state, BlockGetter reader, BlockPos pos, Direction side) { return getSignal(state, reader, pos, side); }

    @Override
    public void onPlace(BlockState state, Level world, BlockPos pos, BlockState oldState, boolean isMoving) { neighborChanged(state, world, pos, null, null, false); }

    @Override
    public void neighborChanged(BlockState state, Level world, BlockPos pos, @Nullable Block block, @Nullable BlockPos fromPos, boolean isMoving) {
        var facing = state.getValue(FACING);
        var behind = pos.relative(facing.getOpposite());
        var testState = world.getBlockState(behind);
        var relFacing = getRelativeDirecion(testState, facing);
        var signal = world.getSignal(behind, relFacing);
        if(testState.getBlock() instanceof RedstoneWire) signal = testState.getValue(RedstoneWire.POWER);
        if(signal == 0) world.scheduleTick(pos, this, state.getValue(DELAY) * 2, TickPriority.VERY_HIGH);
        else world.setBlock(pos, state.setValue(POWERED, true), 3);
    }

    @Override
    public void tick(BlockState state, ServerLevel world, BlockPos pos, RandomSource rand) {
        var facing = state.getValue(FACING);
        var behind = pos.relative(facing.getOpposite());
        var testState = world.getBlockState(behind);
        var signal = 0;
        if(testState.getBlock() instanceof RedstoneWire) signal = testState.getValue(RedstoneWire.POWER);
        world.setBlock(pos, state.setValue(POWERED, signal > 0), 3);
    }

    private static Direction getRelativeDirecion(BlockState state, Direction dir) {
        var testBlock = state.getBlock();
        return (testBlock instanceof RedstoneWire || testBlock instanceof RedStoneWireBlock || testBlock instanceof DiodeBlock || testBlock instanceof Transmitter || testBlock instanceof ObserverBlock || testBlock instanceof EntityObserver || testBlock instanceof WireRepeater) ? dir.getOpposite() : dir;
    }

    @Override
    public InteractionResult use(BlockState state, Level world, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hit) {
        if (!player.getAbilities().mayBuild) return InteractionResult.PASS;
        else {
            int delay = state.getValue(DELAY);
            if(delay == 4) delay = 1;
            else delay++;
            world.setBlock(pos, state.setValue(DELAY, delay), 3);
            return InteractionResult.sidedSuccess(world.isClientSide);
        }
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter reader, BlockPos pos, CollisionContext context) {
        return switch (state.getValue(FACING)) {
            case NORTH, SOUTH -> SHAPE_NS;
            case EAST, WEST -> SHAPE_EW;
            default -> SHAPE_UD;
        };
    }

    @Override
    public VoxelShape getCollisionShape(BlockState state, BlockGetter world, BlockPos pos, CollisionContext ctx) { return getShape(state, world, pos, ctx); }

    @Override
    public void genItemModel(ItemModelGenerator generator) {
        generator.parentBlockItem(this, "wire_repeater/1");
    }

    @Override
    public void genBlockState(BlockStateGenerator generator) {
        ModelFile[] offModels = {
            generator.existingModel("wire_repeater/1"),
            generator.existingModel("wire_repeater/2"),
            generator.existingModel("wire_repeater/3"),
            generator.existingModel("wire_repeater/4")
        };

        ModelFile[] onModels = {
            generator.existingModel("wire_repeater/1_on"),
            generator.existingModel("wire_repeater/2_on"),
            generator.existingModel("wire_repeater/3_on"),
            generator.existingModel("wire_repeater/4_on")
        };

        generator.getVariantBuilder(this)
            .forAllStatesExcept(
                state -> {
                    Direction facing = state.getValue(FACING);
                    boolean powered = state.getValue(POWERED);
                    int delay = state.getValue(DELAY);

                    var model = offModels[delay - 1];

                    if(powered) model = onModels[delay - 1];

                    return generator.buildVariantModel(model, facing);
                },
                WATERLOGGED
            );
    }
}