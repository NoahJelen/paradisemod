package net.paradisemod.redstone.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.DustParticleOptions;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraft.world.ticks.TickPriority;
import net.paradisemod.base.PMConfig;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ItemModelGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;
import net.paradisemod.base.data.tags.PMTags;

public class Transmitter extends Block implements ModeledBlock {
    public static final BooleanProperty POWERED = BlockStateProperties.POWERED;
    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;
    public static final BooleanProperty RECEIVER = BooleanProperty.create("receiver");
    private static final VoxelShape SHAPE = Block.box(0.0D, 0.0D, 0.0D, 16.0D, 2.0D, 16.0D);
    protected final DyeColor color;
    private final int range = PMConfig.SETTINGS.redstone.transmitterRange.get();

    public Transmitter(DyeColor color) {
        super(Properties.copy(Blocks.REPEATER));
        registerDefaultState(stateDefinition.any().setValue(POWERED, false).setValue(RECEIVER, false).setValue(FACING, Direction.EAST));
        this.color = color;
    }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext ctx) { return defaultBlockState().setValue(FACING, ctx.getHorizontalDirection().getOpposite()); }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) { builder.add(POWERED, RECEIVER, FACING); }

    @Override
    public void tick(BlockState state, ServerLevel world, BlockPos pos, RandomSource rand) {
        if(state.getValue(RECEIVER)) {
            // look for a powered sending transmitter
            var foundPowered = false;
            for(int i = -range; i <= range; i++) {
                for(int j = -range; j <= range; j++) {
                    for(int k = -range; k <= range; k++) {
                        var newPos = pos.offset(i, j, k);
                        var testState = world.getBlockState(newPos);
                        var block = testState.getBlock();
                        if(block instanceof Transmitter transmitter) {
                            if(transmitter.color == color && !testState.getValue(RECEIVER) && testState.getValue(POWERED)) {
                                foundPowered = true;
                                break;
                            }
                        }
                    }

                    if(foundPowered) break;
                }

                if(foundPowered) break;
            }
            world.setBlock(pos, state.setValue(POWERED, foundPowered), 3);
        }
        else {
            var adjPos = pos.relative(state.getValue(FACING));
            var signal = world.getSignal(adjPos, state.getValue(FACING));
            world.setBlock(pos, state.setValue(POWERED, signal > 0), 3);
        }
        for(var direction : Direction.values()) world.updateNeighborsAt(pos.relative(direction), this);
        world.scheduleTick(pos, this, 2, TickPriority.VERY_HIGH);
    }

    @Override
    public InteractionResult use(BlockState state, Level world, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hit) {
        var receiver = state.getValue(RECEIVER);

        if (!player.getAbilities().mayBuild) return InteractionResult.PASS;
        else {
            float pitch = state.getValue(RECEIVER) ? 0.55F : 0.5F;
            world.playSound(player, pos, SoundEvents.COMPARATOR_CLICK, SoundSource.BLOCKS, 0.3F, pitch);
            world.setBlock(pos, state.setValue(RECEIVER, !receiver), 3);
            return InteractionResult.sidedSuccess(world.isClientSide);
        }
    }

    @Override
    public void onPlace(BlockState state, Level world, BlockPos pos, BlockState oldState, boolean isMoving) { world.scheduleTick(pos, this, 2, TickPriority.VERY_HIGH); }

    @Override
    public int getSignal(BlockState state, BlockGetter read, BlockPos pos, Direction side) { return state.getValue(RECEIVER) && state.getValue(POWERED) && side == state.getValue(FACING).getOpposite() ? 15 : 0; }

    @Override
    public int getDirectSignal(BlockState state, BlockGetter reader, BlockPos pos, Direction side) { return getSignal(state, reader, pos, side); }

    @Override
    public boolean isSignalSource(BlockState pState) { return true; }

    @Override
    public boolean canConnectRedstone(BlockState state, BlockGetter world, BlockPos pos, Direction side) { return side == state.getValue(FACING).getOpposite(); }

    @Override
    public void animateTick(BlockState state, Level world, BlockPos pos, RandomSource rand) {
        if (state.getValue(POWERED)) {
            var direction = state.getValue(FACING);
            var d0 = pos.getX() + 0.5D + (rand.nextDouble() - 0.5D) * 0.2D;
            var d1 = pos.getY() + 0.4D + (rand.nextDouble() - 0.5D) * 0.2D;
            var d2 = pos.getZ() + 0.5D + (rand.nextDouble() - 0.5D) * 0.2D;
            var f = -5F / 16F;
            var d3 = f * direction.getStepX();
            var d4 = f * direction.getStepZ();
            world.addParticle(DustParticleOptions.REDSTONE, d0 + d3, d1, d2 + d4, 0.0D, 0.0D, 0.0D);
        }
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter reader, BlockPos pos, CollisionContext ctx) { return SHAPE; }

    @Override
    public void neighborChanged(BlockState state, Level world, BlockPos pos, Block block, BlockPos fromPos, boolean isMoving) {
        if (canSurvive(state, world, pos)) return;
        dropResources(state, world, pos);
        world.setBlockAndUpdate(pos, Blocks.AIR.defaultBlockState());
    }

    @Override
    public boolean canSurvive(BlockState state, LevelReader world, BlockPos pos) {
        var blockBelow = world.getBlockState(pos.below());
        return blockBelow.is(PMTags.Blocks.GROUND_BLOCKS);
    }

    @Override
    public void genItemModel(ItemModelGenerator generator) {
        generator.basicItem(asItem(), "transmitter/" + color.getName());
    }

    @Override
    public void genBlockState(BlockStateGenerator generator) {
        var modelBuilder = generator.models();
        var model = modelBuilder.withExistingParent("block/transmitter/" + color.getName(), generator.modLoc("block/transmitter/parent"))
            .texture("texture", "block/transmitter/" + color.getName());
        
        var modelOn = modelBuilder.withExistingParent("block/transmitter/" + color.getName() + "_on", generator.modLoc("block/transmitter/parent_on"))
            .texture("texture", "block/transmitter/" + color.getName());
        
        var receiverModel = modelBuilder.withExistingParent("block/transmitter/" + color.getName() + "_receiver", generator.modLoc("block/transmitter/parent_receiver"))
            .texture("texture", "block/transmitter/" + color.getName());
        
        var receiverModelOn = modelBuilder.withExistingParent("block/transmitter/" + color.getName() + "_receiver_on", generator.modLoc("block/transmitter/parent_receiver_on"))
            .texture("texture", "block/transmitter/" + color.getName());

        generator.getVariantBuilder(this)
            .forAllStates(
                state -> {
                    Direction facing = state.getValue(FACING);
                    boolean powered = state.getValue(POWERED);
                    boolean receiver = state.getValue(RECEIVER);

                    if(powered && receiver)
                        return generator.buildVariantModel(receiverModelOn, facing, true);
                    else if(receiver)
                        return generator.buildVariantModel(receiverModel, facing, true);
                    else if(powered)
                        return generator.buildVariantModel(modelOn, facing, true);
                    else
                        return generator.buildVariantModel(model, facing, true);
                }
            );
    }
}