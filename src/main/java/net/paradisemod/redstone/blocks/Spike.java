package net.paradisemod.redstone.blocks;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.RandomSource;
import net.minecraft.world.damagesource.DamageSources;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EndRodBlock;
import net.minecraft.world.level.block.SimpleWaterloggedBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ItemModelGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;

public class Spike extends EndRodBlock implements SimpleWaterloggedBlock, ModeledBlock {
    public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;

    public Spike() { super(BlockType.WEAK_METAL.getProperties().noOcclusion().noCollission()); }

    @Override
    public void stepOn(Level world, BlockPos pPos, BlockState state, Entity entity) { attackCreature(world, entity); }

    @Override
    public void entityInside(BlockState state, Level world, BlockPos pos, Entity entity) { attackCreature(world, entity); }

    @Override
    public void animateTick(BlockState state, Level world, BlockPos pos, RandomSource rand) { }

    private void attackCreature(Level world, Entity entity) {
        var regAccess = world.registryAccess();
        var damageSources = new DamageSources(regAccess);

        if(entity instanceof LivingEntity creature)
            creature.hurt(damageSources.cactus(), 4);
    }

    @Override
    @Nullable
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        var fluidstate = context.getLevel().getFluidState(context.getClickedPos());
        var flag = fluidstate.getType() == Fluids.WATER;
        return super.getStateForPlacement(context).setValue(WATERLOGGED, flag);
    }

    @Override
    public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor world, BlockPos currentPos, BlockPos facingPos) {
        if (state.getValue(WATERLOGGED)) world.scheduleTick(currentPos, Fluids.WATER, Fluids.WATER.getTickDelay(world));
        return super.updateShape(state, facing, facingState, world, currentPos, facingPos);
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) { builder.add(WATERLOGGED, FACING); }

    @Override
    public FluidState getFluidState(BlockState state) { return state.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(state); }

    @Override
    public void genItemModel(ItemModelGenerator generator) {
        generator.parentBlockItem(this);
    }

    @Override
    public void genBlockState(BlockStateGenerator generator) {
        generator.getVariantBuilder(this)
            .forAllStatesExcept(
                state -> {
                    Direction facing = state.getValue(FACING);
                    return generator.buildVariantModelFacingUp(
                        generator.existingModel("spike"),
                        facing
                    );
                },
                WATERLOGGED
            );
    }
}
