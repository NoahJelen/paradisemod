package net.paradisemod.redstone.blocks.superpiston;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.piston.MovingPistonBlock;
import net.minecraft.world.level.block.piston.PistonBaseBlock;
import net.minecraft.world.level.block.piston.PistonHeadBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.PistonType;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.level.material.PushReaction;
import net.minecraftforge.client.model.generators.ModelFile.UncheckedModelFile;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ItemModelGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;
import net.paradisemod.redstone.Redstone;
import net.paradisemod.redstone.tile.SuperPistonEntity;

public class SuperPiston extends PistonBaseBlock implements ModeledBlock {
    private final boolean sticky;

    public SuperPiston(boolean sticky) {
        super(sticky,Properties.copy(Blocks.PISTON));
        this.sticky = sticky;
    }

    @Override
    public PushReaction getPistonPushReaction(BlockState state) {
        return (state.getValue(EXTENDED)) ? PushReaction.BLOCK : PushReaction.NORMAL;
    }

    @Override
    public void neighborChanged(BlockState state, Level world, BlockPos pos, Block block, BlockPos fromPos, boolean isMoving) {
        if (!world.isClientSide) extendIfPowered(world, pos, state);
    }

    @Override
    public void onPlace(BlockState state, Level world, BlockPos pos, BlockState oldState, boolean isMoving) {
        if (!oldState.is(state.getBlock())) {
            if (!world.isClientSide && world.getBlockEntity(pos) == null) extendIfPowered(world, pos, state);
        }
    }

    @Override
    public void setPlacedBy(Level world, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack) {
        if (!world.isClientSide) extendIfPowered(world, pos, state);
    }

    @Override
    public boolean triggerEvent(BlockState state, Level world, BlockPos pos, int id, int param) {
        var direction = state.getValue(FACING);
        if (!world.isClientSide) {
            var flag = isNeighborPowered(world, pos, direction);
            if (flag && (id == 1 || id == 2)) {
                world.setBlock(pos, state.setValue(EXTENDED, true), 2);
                return false;
            }

            if (!flag && id == 0) return false;
        }

        if (id == 0) {
            if (!moveBlocks(world, pos, direction, true)) return false;
            world.setBlock(pos, state.setValue(EXTENDED, true), 67);
            world.playSound(null, pos, SoundEvents.PISTON_EXTEND, SoundSource.BLOCKS, 0.5F, world.random.nextFloat() * 0.25F + 0.6F);
            world.gameEvent(GameEvent.BLOCK_ACTIVATE, pos, GameEvent.Context.of(state));
        }
        else if (id == 1 || id == 2) {
            var blockentity1 = world.getBlockEntity(pos.relative(direction));
            if (blockentity1 instanceof SuperPistonEntity piston)
                piston.finalTick();

            var blockstate = Redstone.MOVING_SUPER_PISTON.get().defaultBlockState().setValue(MovingPistonBlock.FACING, direction).setValue(MovingPistonBlock.TYPE, sticky ? PistonType.STICKY : PistonType.DEFAULT);
            world.setBlock(pos, blockstate, 20);
            world.setBlockEntity(new SuperPistonEntity(pos, blockstate, defaultBlockState().setValue(FACING, Direction.from3DDataValue(param & 7)), direction, false, true));
            world.blockUpdated(pos, blockstate.getBlock());
            blockstate.updateNeighbourShapes(world, pos, 2);

            if (sticky) {
                var blockpos = pos.offset(direction.getStepX() * 2, direction.getStepY() * 2, direction.getStepZ() * 2);
                var blockstate1 = world.getBlockState(blockpos);
                var flag1 = false;
                if (blockstate1.is(Redstone.MOVING_SUPER_PISTON.get())) {
                    var blockentity = world.getBlockEntity(blockpos);
                    if (blockentity instanceof SuperPistonEntity piston) {
                        if (piston.getDirection() == direction && piston.isExtending()) {
                            piston.finalTick();
                            flag1 = true;
                        }
                    }
                }

                if (!flag1) {
                    if (id != 1 || blockstate1.isAir() || !isPushable(blockstate1, world, blockpos, direction.getOpposite(), false, direction) || blockstate1.getPistonPushReaction() != PushReaction.NORMAL && !blockstate1.is(Redstone.SUPER_PISTON.get()) && !blockstate1.is(Redstone.STICKY_SUPER_PISTON.get()))
                        world.removeBlock(pos.relative(direction), false);

                    else moveBlocks(world, pos,  direction, false);

                }
            }
            else world.removeBlock(pos.relative(direction), false);

            world.playSound(null, pos, SoundEvents.PISTON_CONTRACT, SoundSource.BLOCKS, 0.5F, world.random.nextFloat() * 0.15F + 0.6F);
            world.gameEvent(GameEvent.BLOCK_DEACTIVATE, pos, GameEvent.Context.of(blockstate));
        }

        return true;
    }

    private boolean isNeighborPowered(Level world, BlockPos pos, Direction facing) {
        for(var direction : Direction.values())
            if (direction != facing && world.hasSignal(pos.relative(direction), direction))
                return true;

        if (world.hasSignal(pos, Direction.DOWN)) return true;
        else {
            var blockpos = pos.above();

            for(Direction direction1 : Direction.values()) {
                if (direction1 != Direction.DOWN && world.hasSignal(blockpos.relative(direction1), direction1))
                    return true;
            }

            return false;
        }
    }

    private boolean moveBlocks(Level world, BlockPos pos, Direction dir, boolean extending) {
        var blockpos = pos.relative(dir);
        if (!extending && world.getBlockState(blockpos).is(Redstone.SUPER_PISTON_HEAD.get()))
            world.setBlock(blockpos, Blocks.AIR.defaultBlockState(), 20);

        var structure = new SuperPistonStructure(world, pos, dir, extending);
        if (!structure.resolve()) return false;
        else {
            HashMap<BlockPos, BlockState> map = new HashMap<>();
            var list = structure.getToPush();
            ArrayList<BlockState> list1 = new ArrayList<>();

            for (var blockpos1 : list) {
                var blockstate = world.getBlockState(blockpos1);
                list1.add(blockstate);
                map.put(blockpos1, blockstate);
            }

            var list2 = structure.getToDestroy();
            BlockState[] ablockstate = new BlockState[list.size() + list2.size()];
            var direction = extending ? dir : dir.getOpposite();
            int j = 0;

            for(int k = list2.size() - 1; k >= 0; k--) {
                var blockpos2 = list2.get(k);
                var blockstate1 = world.getBlockState(blockpos2);
                var tile = blockstate1.hasBlockEntity() ? world.getBlockEntity(blockpos2) : null;
                dropResources(blockstate1, world, blockpos2, tile);
                world.setBlock(blockpos2, Blocks.AIR.defaultBlockState(), 18);
                if (!blockstate1.is(BlockTags.FIRE))
                    world.addDestroyBlockEffect(blockpos2, blockstate1);

                ablockstate[j++] = blockstate1;
            }

            for(int l = list.size() - 1; l >= 0; l--) {
                var blockpos3 = list.get(l);
                var blockstate5 = world.getBlockState(blockpos3);
                blockpos3 = blockpos3.relative(direction);
                map.remove(blockpos3);
                var blockstate8 = Redstone.MOVING_SUPER_PISTON.get().defaultBlockState().setValue(FACING, dir);
                world.setBlock(blockpos3, blockstate8, 68);
                world.setBlockEntity(new SuperPistonEntity(blockpos3, blockstate8, list1.get(l), dir, extending, false));
                ablockstate[j++] = blockstate5;
            }

            if (extending) {
                var pistontype = sticky ? PistonType.STICKY : PistonType.DEFAULT;
                var movedState = Redstone.SUPER_PISTON_HEAD.get().defaultBlockState().setValue(PistonHeadBlock.FACING, dir).setValue(PistonHeadBlock.TYPE, pistontype);
                var blockstate6 = Redstone.MOVING_SUPER_PISTON.get().defaultBlockState().setValue(MovingPistonBlock.FACING, dir).setValue(MovingPistonBlock.TYPE, sticky ? PistonType.STICKY : PistonType.DEFAULT);
                map.remove(blockpos);
                world.setBlock(blockpos, blockstate6, 68);
                world.setBlockEntity(new SuperPistonEntity(blockpos, blockstate6, movedState, dir, true, true));
            }

            var blockstate3 = Blocks.AIR.defaultBlockState();

            for(var blockpos4 : map.keySet())
                world.setBlock(blockpos4, blockstate3, 82);

            for(var entry : map.entrySet()) {
                var blockpos5 = entry.getKey();
                var blockstate2 = entry.getValue();
                blockstate2.updateIndirectNeighbourShapes(world, blockpos5, 2);
                blockstate3.updateNeighbourShapes(world, blockpos5, 2);
                blockstate3.updateIndirectNeighbourShapes(world, blockpos5, 2);
            }

            j = 0;

            for(int i1 = list2.size() - 1; i1 >= 0; i1--) {
                var blockstate7 = ablockstate[j++];
                var blockpos6 = list2.get(i1);
                blockstate7.updateIndirectNeighbourShapes(world, blockpos6, 2);
                world.updateNeighborsAt(blockpos6, blockstate7.getBlock());
            }

            for(int j1 = list.size() - 1; j1 >= 0; j1--)
                world.updateNeighborsAt(list.get(j1), ablockstate[j++].getBlock());

            if (extending) world.updateNeighborsAt(blockpos, Redstone.SUPER_PISTON_HEAD.get());
            return true;
        }
    }

    private void extendIfPowered(Level world, BlockPos pos, BlockState state) {
        var facing = state.getValue(FACING);
        var powered = hasPower(world, pos, facing);
        if (powered && !state.getValue(EXTENDED)) {
            var structure = new SuperPistonStructure(world, pos, facing, true);
            if(structure.resolve()) world.blockEvent(pos, this, 0, facing.get3DDataValue());
        }
        else if (!powered && state.getValue(EXTENDED)) {
            var facingPos = pos.relative(facing, 2);
            var facingState = world.getBlockState(facingPos);
            var i = 1;
            if(facingState.is(Redstone.MOVING_SUPER_PISTON.get()) && facingState.getValue(FACING) == facing) {
                var tile = world.getBlockEntity(facingPos);
                if(tile instanceof SuperPistonEntity piston) {
                    if(piston.isExtending() && (piston.getProgress(0F) < 0.5F || world.getGameTime() == piston.getLastTicked() || ((ServerLevel) world).isHandlingTick()))
                        i = 2;
                }
            }

            world.blockEvent(pos, this, i, facing.get3DDataValue());
        }
    }

    private boolean hasPower(Level world, BlockPos pos, Direction facing) {
        for(var direction : Direction.values())
            if(direction != facing && world.hasSignal(pos.relative(direction), direction)) return true;

        if (world.hasSignal(pos, Direction.DOWN)) return true;
        else {
            var blockpos = pos.above();
            for(var direction : Direction.values())
                if(direction != Direction.DOWN && world.hasSignal(blockpos.relative(direction), direction)) return true;

            return false;
        }
    }

    protected static boolean canPush(BlockState state, Level world, BlockPos pos, Direction facing, boolean shouldDestroy, Direction dir) {
        var pistons = List.of(
            Blocks.PISTON,
            Blocks.STICKY_PISTON,
            Redstone.SUPER_PISTON.get(),
            Redstone.STICKY_SUPER_PISTON.get()
        );

        if (pos.getY() >= world.getMinBuildHeight() && pos.getY() <= world.getMaxBuildHeight() - 1 && world.getWorldBorder().isWithinBounds(pos)) {
            if (state.isAir()) return true;
            else {
                if (facing == Direction.DOWN && pos.getY() == world.getMinBuildHeight())
                    return false;

                else if (facing == Direction.UP && pos.getY() == world.getMaxBuildHeight() - 1) return false;

                else {
                    if (!pistons.contains(state.getBlock())) {
                        if (state.getDestroySpeed(world, pos) == -1.0F) return false;

                        switch (state.getPistonPushReaction()) {
                            case DESTROY -> {
                                return shouldDestroy;
                            }
                            case BLOCK, PUSH_ONLY -> {
                                return facing == dir;
                            }
                        }
                    }
                    else if (state.getValue(EXTENDED))
                        return false;

                    return !state.hasBlockEntity();
                }
            }
        }
        else return false;
    }

    @Override
    public void genItemModel(ItemModelGenerator generator) {
        generator.getBuilder((sticky ? "sticky_" : "") + "super_piston")
            .parent(new UncheckedModelFile("minecraft:block/cube_bottom_top"))
            .texture("bottom", "paradisemod:block/super_piston/bottom")
            .texture("side", "paradisemod:block/super_piston/side")
            .texture("top", "paradisemod:block/super_piston/top" + (sticky ? "_sticky" : ""));
    }

    @Override
    public void genBlockState(BlockStateGenerator generator) {
        var topTexture = "paradisemod:block/super_piston/top" + (sticky ? "_sticky" : "");
        var bottomTexture = "paradisemod:block/super_piston/bottom";
        var sideTexture = "paradisemod:block/super_piston/side";
        var insideTexture = "paradisemod:block/super_piston/inner";

        var modelBuilder = generator.models();
        var pistonModel = modelBuilder.withExistingParent("block/super_piston/" + (sticky ? "sticky" : "normal"), "block/template_piston")
            .texture("platform", topTexture)
            .texture("bottom", bottomTexture)
            .texture("side", sideTexture);

        var extendedModel = modelBuilder.withExistingParent("block/super_piston/extended", "block/piston_extended")
            .texture("bottom", bottomTexture)
            .texture("side", sideTexture)
            .texture("inside", insideTexture);

        generator.getVariantBuilder(this)
            .forAllStates(
                state -> {
                    boolean extended = state.getValue(EXTENDED);
                    Direction facing = state.getValue(FACING);

                    if(extended)
                        return generator.buildVariantModel(extendedModel, facing);
                    else
                        return generator.buildVariantModel(pistonModel, facing);
                }
            );
    }
}