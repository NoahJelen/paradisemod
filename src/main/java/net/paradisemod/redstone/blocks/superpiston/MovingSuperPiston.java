package net.paradisemod.redstone.blocks.superpiston;

import java.util.Collections;
import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.piston.MovingPistonBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.storage.loot.LootParams;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;
import net.paradisemod.redstone.Redstone;
import net.paradisemod.redstone.tile.SuperPistonEntity;

public class MovingSuperPiston extends MovingPistonBlock implements ModeledBlock {
    public MovingSuperPiston() {
        super(Properties.copy(Blocks.MOVING_PISTON));
    }

    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level world, BlockState state, BlockEntityType<T> blockEntityType) {
        return createTickerHelper(blockEntityType, Redstone.SUPER_PISTON_TILE.get(), SuperPistonEntity::tick);
    }

    @Override
    public void onRemove(BlockState state, Level world, BlockPos pos, BlockState newState, boolean isMoving) {
        if (!state.is(newState.getBlock())) {
            var blockentity = world.getBlockEntity(pos);
            if (blockentity instanceof SuperPistonEntity piston)
                piston.finalTick();
        }
    }

    @Override
    public List<ItemStack> getDrops(BlockState pState, LootParams.Builder builder) {
        var piston = getBlockEntity(builder.getLevel(), BlockPos.containing(builder.getParameter(LootContextParams.ORIGIN)));
        return piston == null ? Collections.emptyList() : piston.getMovedState().getDrops(builder);
    }

    private SuperPistonEntity getBlockEntity(BlockGetter reader, BlockPos pos) {
        var tile = reader.getBlockEntity(pos);
        return tile instanceof SuperPistonEntity piston ? piston : null;
    }

    @Override
    public void genBlockState(BlockStateGenerator generator) {
        var model = generator.models().withExistingParent("moving_super_piston", "block/moving_piston");
        generator.simpleBlock(this, model);
    }
}