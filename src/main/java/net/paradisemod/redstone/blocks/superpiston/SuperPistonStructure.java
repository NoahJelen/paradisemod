package net.paradisemod.redstone.blocks.superpiston;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.PushReaction;
import net.paradisemod.base.PMConfig;

import java.util.ArrayList;
import java.util.List;

public class SuperPistonStructure {
    private final Level world;
    private final BlockPos pistonPos;
    private final boolean extending;
    private final BlockPos startPos;
    private final Direction pushDirection;
    private final ArrayList<BlockPos> toPush = new ArrayList<>();
    private final ArrayList<BlockPos> toDestroy = new ArrayList<>();
    private final Direction pistonDirection;
    private final int maxBlocks = PMConfig.SETTINGS.redstone.superPistonPushLimit.get();

    public SuperPistonStructure(Level world, BlockPos pistonPos, Direction pistonDirection, boolean extending) {
        this.world = world;
        this.pistonPos = pistonPos;
        this.pistonDirection = pistonDirection;
        this.extending = extending;
        if (extending) {
            this.pushDirection = pistonDirection;
            this.startPos = pistonPos.relative(pistonDirection);
        }
        else {
            this.pushDirection = pistonDirection.getOpposite();
            this.startPos = pistonPos.relative(pistonDirection, 2);
        }
    }

    public boolean resolve() {
        toPush.clear();
        toDestroy.clear();
        var blockstate = world.getBlockState(startPos);
        if (!SuperPiston.canPush(blockstate, world, startPos, pushDirection, false, pistonDirection)) {
            if (extending && blockstate.getPistonPushReaction() == PushReaction.DESTROY) {
                toDestroy.add(startPos);
                return true;
            }
            else return false;
        }
        else if (!addBlockLine(startPos, pushDirection)) return false;
        else {
            for(int i = 0; i < this.toPush.size(); ++i) {
                var pushPos = this.toPush.get(i);
                if (world.getBlockState(pushPos).isStickyBlock() && !addBranchingBlocks(pushPos))
                    return false;
            }

            return true;
        }
    }

    private boolean addBlockLine(BlockPos originPos, Direction direction) {
        var blockstate = world.getBlockState(originPos);
        if(world.isEmptyBlock(originPos)) return true;
        else if(!SuperPiston.canPush(blockstate, world, originPos, pushDirection, false, direction))
            return true;
        else if (originPos.equals(pistonPos)) return true;
        else if (toPush.contains(originPos)) return true;
        else {
            int i = 1;
            if (i + toPush.size() > maxBlocks) return false;
            else {
                BlockState oldState;
                while(blockstate.isStickyBlock()) {
                    var blockpos = originPos.relative(pushDirection.getOpposite(), i);
                    oldState = blockstate;
                    blockstate = world.getBlockState(blockpos);
                    if (blockstate.isAir() || !oldState.canStickTo(blockstate) || !SuperPiston.canPush(blockstate, world, blockpos, pushDirection, false, pushDirection.getOpposite()) || blockpos.equals(pistonPos))
                        break;

                    i++;
                    if (i + toPush.size() > maxBlocks) return false;
                }

                int l = 0;
                for(int i1 = i - 1; i1 >= 0; i1--) {
                    toPush.add(originPos.relative(pushDirection.getOpposite(), i1));
                    l++;
                }

                int j1 = 1;
                while(true) {
                    var blockpos1 = originPos.relative(pushDirection, j1);
                    int j = toPush.indexOf(blockpos1);
                    if (j > -1) {
                        reorderListAtCollision(l, j);
                        for(int k = 0; k <= j + l; ++k) {
                            var blockpos2 = toPush.get(k);
                            if (world.getBlockState(blockpos2).isStickyBlock() && !addBranchingBlocks(blockpos2))
                                return false;
                        }

                        return true;
                    }

                    blockstate = world.getBlockState(blockpos1);
                    if (blockstate.isAir()) return true;
                    if (!SuperPiston.canPush(blockstate, world, blockpos1, pushDirection, true, pushDirection) || blockpos1.equals(pistonPos))
                        return false;

                    if (blockstate.getPistonPushReaction() == PushReaction.DESTROY) {
                        toDestroy.add(blockpos1);
                        return true;
                    }

                    if (toPush.size() >= maxBlocks) return false;
                    toPush.add(blockpos1);
                    l++;
                    j1++;
                }
            }
        }
    }

    private void reorderListAtCollision(int offsets, int index) {
        var list = new ArrayList<>(toPush.subList(0, index));
        var list1 = new ArrayList<>(toPush.subList(toPush.size() - offsets, toPush.size()));
        var list2 = new ArrayList<>(toPush.subList(index, toPush.size() - offsets));
        toPush.clear();
        toPush.addAll(list);
        toPush.addAll(list1);
        toPush.addAll(list2);
    }

    private boolean addBranchingBlocks(BlockPos fromPos) {
        var blockstate = world.getBlockState(fromPos);

        for(var direction : Direction.values()) {
            if (direction.getAxis() != pushDirection.getAxis()) {
                var blockpos = fromPos.relative(direction);
                var blockstate1 = world.getBlockState(blockpos);
                if (blockstate1.canStickTo(blockstate) && !addBlockLine(blockpos, direction))
                    return false;
            }
        }

        return true;
    }

    public List<BlockPos> getToPush() { return toPush; }

    public List<BlockPos> getToDestroy() { return toDestroy; }
}