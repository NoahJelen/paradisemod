package net.paradisemod.redstone.blocks.superpiston;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.piston.PistonBaseBlock;
import net.minecraft.world.level.block.piston.PistonHeadBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.PistonType;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;
import net.paradisemod.redstone.Redstone;

public class SuperPistonHead extends PistonHeadBlock implements ModeledBlock {
    public SuperPistonHead() { super(Properties.copy(Blocks.PISTON_HEAD)); }

    @Override
    public boolean canSurvive(BlockState state, LevelReader world, BlockPos pos) {
        var blockstate = world.getBlockState(pos.relative(state.getValue(FACING).getOpposite()));
        return fitsBase(state, blockstate) || blockstate.is(Redstone.MOVING_SUPER_PISTON.get()) && blockstate.getValue(FACING) == state.getValue(FACING);
    }

    @Override
    public void playerWillDestroy(Level world, BlockPos pos, BlockState state, Player player) {
        if (!world.isClientSide && player.getAbilities().instabuild) {
            var blockpos = pos.relative(state.getValue(FACING).getOpposite());
            if (fitsBase(state, world.getBlockState(blockpos)))
                world.destroyBlock(blockpos, false);
        }

        super.playerWillDestroy(world, pos, state, player);
    }

    @Override
    public void onRemove(BlockState state, Level world, BlockPos pos, BlockState newState, boolean isMoving) {
        if(!state.is(newState.getBlock())) {
            super.onRemove(state, world, pos, newState, isMoving);
            var basePos = pos.relative(state.getValue(FACING).getOpposite());
            if(fitsBase(state, world.getBlockState(basePos)))
                world.destroyBlock(basePos, true);
        }
    }

    @Override
    public ItemStack getCloneItemStack(BlockGetter world, BlockPos pos, BlockState state) {
        return (state.getValue(TYPE) == PistonType.DEFAULT) ? new ItemStack(Redstone.SUPER_PISTON.get().asItem()) : new ItemStack(Redstone.STICKY_SUPER_PISTON.get().asItem());
    }

    private boolean fitsBase(BlockState headState, BlockState baseState) {
        var block = headState.getValue(TYPE) == PistonType.DEFAULT ? Redstone.SUPER_PISTON : Redstone.STICKY_SUPER_PISTON;
        return baseState.is(block.get()) && baseState.getValue(PistonBaseBlock.EXTENDED) && baseState.getValue(FACING) == headState.getValue(FACING);
    }

    @Override
    public void genBlockState(BlockStateGenerator generator) {

        var topTexture = "paradisemod:block/super_piston/top";
        var sideTexture = "paradisemod:block/super_piston/side";
        var modelBuilder = generator.models();

        var headModel = modelBuilder.withExistingParent("block/super_piston/head", "block/template_piston_head")
            .texture("platform", topTexture)
            .texture("side", sideTexture)
            .texture("unsticky", topTexture);

        var headShortModel = modelBuilder.withExistingParent("block/super_piston/head_short", "block/template_piston_head_short")
            .texture("platform", topTexture)
            .texture("side", sideTexture)
            .texture("unsticky", topTexture);

        var stickyHeadModel = modelBuilder.withExistingParent("block/super_piston/head_sticky", "block/template_piston_head")
            .texture("platform", topTexture)
            .texture("side", sideTexture)
            .texture("unsticky", topTexture);

        var stickyHeadShortModel = modelBuilder.withExistingParent("block/super_piston/head_sticky_short", "block/template_piston_head_short")
            .texture("platform", topTexture)
            .texture("side", sideTexture)
            .texture("unsticky", topTexture);

        generator.getVariantBuilder(this)
            .forAllStates(
                state -> {
                    Direction facing = state.getValue(FACING);
                    boolean isShort = state.getValue(SHORT);
                    PistonType type = state.getValue(TYPE);

                    if(isShort && type == PistonType.STICKY)
                        return generator.buildVariantModel(stickyHeadShortModel, facing);
                    else if(isShort)
                        return generator.buildVariantModel(headShortModel, facing);
                    else if(type == PistonType.STICKY)
                        return generator.buildVariantModel(stickyHeadModel, facing);
                    else return generator.buildVariantModel(headModel, facing);
                }
            );
    }
}