package net.paradisemod.redstone.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.RepeaterBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.ticks.TickPriority;
import net.minecraftforge.client.model.generators.ModelFile;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.ItemModelGenerator;
import net.paradisemod.base.data.assets.ModeledBlock;

public class PulseExtenderRepeater extends RepeaterBlock implements ModeledBlock {
    public static final IntegerProperty PULSE_LEVEL = IntegerProperty.create("pulse_level", 0, 15);

    public PulseExtenderRepeater() {
        super(Properties.copy(Blocks.REPEATER));
        registerDefaultState(
            stateDefinition.any()
                .setValue(FACING, Direction.NORTH)
                .setValue(DELAY, 1)
                .setValue(LOCKED, false)
                .setValue(POWERED, false)
                .setValue(PULSE_LEVEL, 0)
        );
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(PULSE_LEVEL);
        super.createBlockStateDefinition(builder);
    }

    @Override
    public void tick(BlockState state, ServerLevel world, BlockPos pos, RandomSource rand) {
        if (!isLocked(world, pos, state)) {
            int level = state.getValue(PULSE_LEVEL);
            if(shouldTurnOn(world, pos, state)) world.setBlock(pos, state.setValue(PULSE_LEVEL, 15).setValue(POWERED, true), 3);
            else if(level > 0) world.setBlock(pos, state.setValue(PULSE_LEVEL, level - 1), 3);
            else if(level == 0) world.setBlock(pos, state.setValue(POWERED, false), 3);
        }
        for(var direction : Direction.values()) world.updateNeighborsAt(pos.relative(direction), this);
        world.scheduleTick(pos, this, getDelay(state), TickPriority.VERY_HIGH);
    }

    @Override
    public void onPlace(BlockState state, Level world, BlockPos pos, BlockState oldState, boolean isMoving) { world.scheduleTick(pos, this, getDelay(state), TickPriority.VERY_HIGH); }

    @Override
    protected int getOutputSignal(BlockGetter reader, BlockPos pos, BlockState state) { return state.getValue(PULSE_LEVEL); }

    @Override
    public void genItemModel(ItemModelGenerator generator) {
        generator.basicItem(asItem(), "pulse_extender_repeater");
    }

    @Override
    public void genBlockState(BlockStateGenerator generator) {
        ModelFile[] offModels = {
            generator.existingModel("pulse_extender_repeater/1tick"),
            generator.existingModel("pulse_extender_repeater/2tick"),
            generator.existingModel("pulse_extender_repeater/3tick"),
            generator.existingModel("pulse_extender_repeater/4tick")
        };

        ModelFile[] lockedModels = {
            generator.existingModel("pulse_extender_repeater/1tick_locked"),
            generator.existingModel("pulse_extender_repeater/2tick_locked"),
            generator.existingModel("pulse_extender_repeater/3tick_locked"),
            generator.existingModel("pulse_extender_repeater/4tick_locked")
        };

        ModelFile[] onModels = {
            generator.existingModel("pulse_extender_repeater/1tick_on"),
            generator.existingModel("pulse_extender_repeater/2tick_on"),
            generator.existingModel("pulse_extender_repeater/3tick_on"),
            generator.existingModel("pulse_extender_repeater/4tick_on")
        };

        ModelFile[] lockedOnModels = {
            generator.existingModel("pulse_extender_repeater/1tick_on_locked"),
            generator.existingModel("pulse_extender_repeater/2tick_on_locked"),
            generator.existingModel("pulse_extender_repeater/3tick_on_locked"),
            generator.existingModel("pulse_extender_repeater/4tick_on_locked")
        };

        generator.getVariantBuilder(this)
            .forAllStatesExcept(
                state -> {
                    int delay = state.getValue(DELAY);
                    Direction facing = state.getValue(FACING);
                    boolean locked = state.getValue(LOCKED);
                    boolean powered = state.getValue(POWERED);

                    var model = offModels[delay - 1];

                    if(locked && powered)
                        model = lockedOnModels[delay - 1];
                    else if(locked)
                        model = lockedModels[delay - 1];
                    else if(powered)
                        model = onModels[delay - 1];

                    return generator.buildVariantModel(model, facing, true);
                },
                PULSE_LEVEL
            );
    }
}