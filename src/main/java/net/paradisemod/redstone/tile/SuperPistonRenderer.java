package net.paradisemod.redstone.tile;

import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.block.BlockRenderDispatcher;
import net.minecraft.client.renderer.block.ModelBlockRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.piston.PistonBaseBlock;
import net.minecraft.world.level.block.piston.PistonHeadBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.PistonType;
import net.minecraftforge.client.ForgeHooksClient;
import net.paradisemod.redstone.Redstone;

public class SuperPistonRenderer implements BlockEntityRenderer<SuperPistonEntity> {
    private BlockRenderDispatcher blockRenderer;

    public SuperPistonRenderer(BlockEntityRendererProvider.Context context) { this.blockRenderer = context.getBlockRenderDispatcher(); }

    @Override
    public void render(SuperPistonEntity piston, float partialTicks, PoseStack matrixStack, MultiBufferSource buffer, int combinedLight, int combinedOverlay) {
        var world = piston.getLevel();
        if (world != null) {
            var pistonPos = piston.getBlockPos().relative(piston.getMovementDirection().getOpposite());
            var movedState = piston.getMovedState();
            if(!movedState.isAir()) {
                ModelBlockRenderer.enableCaching();
                matrixStack.pushPose();
                matrixStack.translate(piston.getXOff(partialTicks), piston.getYOff(partialTicks), piston.getZOff(partialTicks));
                if(movedState.is(Redstone.SUPER_PISTON_HEAD.get()) && piston.getProgress(partialTicks) <= 4.0F) {
                    movedState = movedState.setValue(PistonHeadBlock.SHORT, piston.getProgress(partialTicks) <= 0.5F);
                    renderBlock(pistonPos, movedState, matrixStack, buffer, world, false, combinedOverlay);
                }
                else if(piston.isSourcePiston() && !piston.isExtending()) {
                    var type = movedState.is(Redstone.STICKY_SUPER_PISTON.get()) ? PistonType.STICKY : PistonType.DEFAULT;
                    var pistonHead = Redstone.SUPER_PISTON_HEAD.get().defaultBlockState()
                        .setValue(PistonHeadBlock.TYPE, type)
                        .setValue(PistonHeadBlock.FACING, movedState.getValue(PistonBaseBlock.FACING))
                        .setValue(PistonHeadBlock.SHORT, piston.getProgress(partialTicks) >= 0.5F);

                    renderBlock(pistonPos, pistonHead, matrixStack, buffer, world, false, combinedOverlay);
                    var destPos = pistonPos.relative(piston.getMovementDirection());
                    matrixStack.popPose();
                    matrixStack.pushPose();
                    movedState = movedState.setValue(PistonBaseBlock.EXTENDED, true);
                    renderBlock(destPos, movedState, matrixStack, buffer, world, true, combinedOverlay);
                }
                else renderBlock(pistonPos, movedState, matrixStack, buffer, world, false, combinedOverlay);
                matrixStack.popPose();
                ModelBlockRenderer.clearCache();
            }
        }
    }

    private void renderBlock(BlockPos pos, BlockState state, PoseStack matrixStack, MultiBufferSource buffer, Level world, boolean checkSides, int combinedOverlay) {
        if(blockRenderer == null) blockRenderer = Minecraft.getInstance().getBlockRenderer();
        ForgeHooksClient.renderPistonMovedBlocks(pos, state, matrixStack, buffer, world, checkSides, combinedOverlay, blockRenderer);
    }
}