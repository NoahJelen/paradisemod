package net.paradisemod.redstone.tile;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.piston.PistonMovingBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.paradisemod.redstone.Redstone;

public class SuperPistonEntity extends PistonMovingBlockEntity {
    public SuperPistonEntity(BlockPos pos, BlockState state) { super(pos, state); }
    public SuperPistonEntity(BlockPos pos, BlockState state, BlockState movedState, Direction direction, boolean extending, boolean isSourcePiston) { super(pos, state, movedState, direction, extending, isSourcePiston); }

    @Override
    public BlockEntityType<SuperPistonEntity> getType() { return Redstone.SUPER_PISTON_TILE.get(); }

    @Override
    public void finalTick() {
        if (level != null && (progressO < 1.0F || level.isClientSide)) {
            progress = 1F;
            progressO = progress;
            level.removeBlockEntity(worldPosition);
            setRemoved();
            if (level.getBlockState(worldPosition).is(Redstone.MOVING_SUPER_PISTON.get())) {
                BlockState blockstate;
                if (isSourcePiston) blockstate = Blocks.AIR.defaultBlockState();
                else blockstate = Block.updateFromNeighbourShapes(movedState, level, worldPosition);
                level.setBlock(worldPosition, blockstate, 3);
                level.neighborChanged(worldPosition, blockstate.getBlock(), worldPosition);
            }
        }
    }

    public static void tick(Level world, BlockPos pos, BlockState state, SuperPistonEntity piston) {
        piston.lastTicked = world.getGameTime();
        piston.progressO = piston.progress;
        if (piston.progressO >= 1.0F) {
            if (world.isClientSide && piston.deathTicks < 5) piston.deathTicks++;
            else {
                world.removeBlockEntity(pos);
                piston.setRemoved();
                if (world.getBlockState(pos).is(Redstone.MOVING_SUPER_PISTON.get())) {
                    var blockstate = Block.updateFromNeighbourShapes(piston.movedState, world, pos);
                    if (blockstate.isAir()) {
                        world.setBlock(pos, piston.movedState, 84);
                        Block.updateOrDestroy(piston.movedState, blockstate, world, pos, 3);
                    }
                    else {
                        if (blockstate.hasProperty(BlockStateProperties.WATERLOGGED) && blockstate.getValue(BlockStateProperties.WATERLOGGED))
                            blockstate = blockstate.setValue(BlockStateProperties.WATERLOGGED, false);

                        world.setBlock(pos, blockstate, 67);
                        world.neighborChanged(pos, blockstate.getBlock(), pos);
                    }
                }
            }
        }
        else {
            var newProgress = piston.progress + 0.5F;
            moveCollidedEntities(world, pos, newProgress, piston);
            moveStuckEntities(world, pos, newProgress, piston);
            piston.progress = newProgress;
            if (piston.progress >= 1.0F)
                piston.progress = 1.0F;
        }
    }
}