package net.paradisemod.redstone;

import java.util.ArrayList;

import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.properties.BlockSetType;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.PMBlockSetTypes;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.building.Building;
import net.paradisemod.building.GlowingObsidianColor;
import net.paradisemod.misc.Misc;
import net.paradisemod.redstone.blocks.CustomPressurePlate;
import net.paradisemod.redstone.blocks.CustomPressurePlate.Sensitivity;
import net.paradisemod.world.DeepDarkBlocks;

public class  Plates {
    // pressure plates
    public static final RegisteredBlock ANDESITE_PRESSURE_PLATE = regStonePlate("andesite", Blocks.ANDESITE);
    public static final RegisteredBlock DIORITE_PRESSURE_PLATE = regStonePlate("diorite", Blocks.DIORITE);
    public static final RegisteredBlock GRANITE_PRESSURE_PLATE = regStonePlate("granite", Blocks.GRANITE);
    public static final RegisteredBlock DRIPSTONE_PRESSURE_PLATE = regStonePlate("dripstone", PMBlockSetTypes.DRIPSTONE, Blocks.DRIPSTONE_BLOCK);
    public static final RegisteredBlock CALCITE_PRESSURE_PLATE = regStonePlate("calcite", PMBlockSetTypes.CALCITE, Blocks.CALCITE);
    public static final RegisteredBlock TUFF_PRESSURE_PLATE = regStonePlate("tuff", PMBlockSetTypes.TUFF, Blocks.TUFF);
    public static final RegisteredBlock COBBLESTONE_PRESSURE_PLATE = regStonePlate("cobblestone", Blocks.COBBLESTONE);
    public static final RegisteredBlock DEEPSLATE_PRESSURE_PLATE = regStonePlate("deepslate", PMBlockSetTypes.DEEPSLATE, Blocks.DEEPSLATE);
    public static final RegisteredBlock COBBLED_DEEPSLATE_PRESSURE_PLATE = regStonePlate("cobbled_deepslate", PMBlockSetTypes.DEEPSLATE, Blocks.COBBLED_DEEPSLATE);
    public static final RegisteredBlock POLISHED_DEEPSLATE_PRESSURE_PLATE = regStonePlate("polished_deepslate", PMBlockSetTypes.DEEPSLATE, Blocks.POLISHED_DEEPSLATE);
    public static final RegisteredBlock MOSSY_COBBLESTONE_PRESSURE_PLATE = regStonePlate("mossy_cobblestone", Blocks.MOSSY_COBBLESTONE);
    public static final RegisteredBlock END_STONE_PRESSURE_PLATE = regStonePlate("end_stone", Blocks.END_STONE);

    public static final RegisteredBlock DARKSTONE_PRESSURE_PLATE = regStonePlate("darkstone", DeepDarkBlocks.DARKSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKSTONE_PRESSURE_PLATE = regStonePlate("blackstone", Blocks.BLACKSTONE);
    public static final RegisteredBlock POLISHED_ANDESITE_PRESSURE_PLATE = regStonePlate("polished_andesite", Blocks.POLISHED_ANDESITE);
    public static final RegisteredBlock POLISHED_DIORITE_PRESSURE_PLATE = regStonePlate("polished_diorite", Blocks.POLISHED_DIORITE);
    public static final RegisteredBlock POLISHED_GRANITE_PRESSURE_PLATE = regStonePlate("polished_granite", Blocks.POLISHED_GRANITE);
    public static final RegisteredBlock POLISHED_DRIPSTONE_PRESSURE_PLATE = regStonePlate("polished_dripstone", PMBlockSetTypes.DRIPSTONE, Building.POLISHED_DRIPSTONE);
    public static final RegisteredBlock POLISHED_CALCITE_PRESSURE_PLATE = regStonePlate("polished_calcite", PMBlockSetTypes.CALCITE, Building.POLISHED_CALCITE);
    public static final RegisteredBlock POLISHED_TUFF_PRESSURE_PLATE = regStonePlate("polished_tuff", PMBlockSetTypes.TUFF, Building.POLISHED_TUFF);
    public static final RegisteredBlock SMOOTH_STONE_PRESSURE_PLATE = regStonePlate("smooth_stone", Blocks.SMOOTH_STONE);
    public static final RegisteredBlock POLISHED_END_STONE_PRESSURE_PLATE = regStonePlate("polished_end_stone", Building.POLISHED_END_STONE);
    public static final RegisteredBlock POLISHED_DARKSTONE_PRESSURE_PLATE = regStonePlate("polished_darkstone", DeepDarkBlocks.POLISHED_DARKSTONE);
    public static final RegisteredBlock SANDSTONE_PRESSURE_PLATE = regStonePlate("sandstone", Blocks.SANDSTONE);
    public static final RegisteredBlock RED_SANDSTONE_PRESSURE_PLATE = regStonePlate("red_sandstone", Blocks.RED_SANDSTONE);

    public static final RegisteredBlock BLACKENED_SANDSTONE_PRESSURE_PLATE = regStonePlate("blackened_sandstone", DeepDarkBlocks.BLACKENED_SANDSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

        public static final RegisteredBlock BEDROCK_PRESSURE_PLATE = regPlate("bedrock", BlockType.INDESTRUCTIBLE, CustomPressurePlate.Sensitivity.PLAYERS, 20, Blocks.BEDROCK);
    public static final RegisteredBlock DIRT_PRESSURE_PLATE = regPlate("dirt", BlockType.SOIL, Sensitivity.EVERYTHING, 20, Blocks.DIRT);

    public static final RegisteredBlock GRASS_PRESSURE_PLATE = regPlate(
        "grass",
        BlockType.SOIL,
        PMBlockSetTypes.GRASS,
        Sensitivity.EVERYTHING,
        20,
        Blocks.GRASS_BLOCK
    )
        .tags(
            BlockTags.PRESSURE_PLATES,
            BlockTags.MINEABLE_WITH_SHOVEL
        );

    public static final RegisteredBlock DIAMOND_PRESSURE_PLATE = regMetalPlate("diamond", 14, Items.DIAMOND);
    public static final RegisteredBlock EMERALD_PRESSURE_PLATE = regMetalPlate("emerald", 10, Items.EMERALD);
    public static final RegisteredBlock RUBY_PRESSURE_PLATE = regMetalPlate("ruby", 2, Misc.RUBY);
    public static final RegisteredBlock RUSTED_IRON_PRESSURE_PLATE = regMetalPlate("rusted_iron", 2, Misc.RUSTED_IRON_INGOT);
    public static final RegisteredBlock SILVER_PRESSURE_PLATE = regMetalPlate("silver", 20, Misc.SILVER_INGOT);

    public static final RegisteredBlock GLASS_PRESSURE_PLATE = regPlate(
        "glass",
        Block.Properties.copy(Blocks.OAK_PRESSURE_PLATE),
        PMBlockSetTypes.GLASS,
        Sensitivity.EVERYTHING,
        5,
        Blocks.GLASS
    )
        .tag(BlockTags.PRESSURE_PLATES);

    public static final RegisteredBlock CACTUS_PRESSURE_PLATE = regWoodPlate("cactus", Building.CACTUS_BLOCK);
    public static final RegisteredBlock PALO_VERDE_PRESSURE_PLATE = regWoodPlate("palo_verde", Building.PALO_VERDE_PLANKS);
    public static final RegisteredBlock MESQUITE_PRESSURE_PLATE = regWoodPlate("mesquite", Building.MESQUITE_PLANKS);
    public static final RegisteredBlock POLISHED_ASPHALT_PRESSURE_PLATE = regStonePlate("polished_asphalt", Building.POLISHED_ASPHALT);

    public static final RegisteredBlock BLACKENED_OAK_PRESSURE_PLATE = regWoodPlate("blackened_oak", DeepDarkBlocks.BLACKENED_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_SPRUCE_PRESSURE_PLATE = regWoodPlate("blackened_spruce", DeepDarkBlocks.BLACKENED_SPRUCE_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_OAK_PRESSURE_PLATE = regPlate(
        "glowing_oak",
        Block.Properties.copy(Blocks.OAK_PRESSURE_PLATE).lightLevel(state -> 7),
        PMBlockSetTypes.VARIANT_WOOD,
        Sensitivity.EVERYTHING,
        30,
        DeepDarkBlocks.GLOWING_OAK_PLANKS
    )
        .tab(DeepDarkBlocks.DEEP_DARK_TAB)
        .tag(BlockTags.WOODEN_PRESSURE_PLATES);

    public static final RegisteredBlock GLOWING_CACTUS_PRESSURE_PLATE = regPlate(
        "glowing_cactus",
        Block.Properties.copy(Blocks.OAK_PRESSURE_PLATE).lightLevel(state -> 7),
        PMBlockSetTypes.VARIANT_WOOD,
        Sensitivity.EVERYTHING,
        30,
        DeepDarkBlocks.GLOWING_CACTUS_BLOCK
    )
        .tab(DeepDarkBlocks.DEEP_DARK_TAB)
        .tag(BlockTags.WOODEN_PRESSURE_PLATES);

    public static final RegisteredBlock BLACK_GLOWING_OBSIDIAN_PRESSURE_PLATE = regGlowingObsidianPlate(GlowingObsidianColor.BLACK);
    public static final RegisteredBlock BLUE_GLOWING_OBSIDIAN_PRESSURE_PLATE = regGlowingObsidianPlate(GlowingObsidianColor.BLUE);
    public static final RegisteredBlock GREEN_GLOWING_OBSIDIAN_PRESSURE_PLATE = regGlowingObsidianPlate(GlowingObsidianColor.GREEN);
    public static final RegisteredBlock INDIGO_GLOWING_OBSIDIAN_PRESSURE_PLATE = regGlowingObsidianPlate(GlowingObsidianColor.INDIGO);
    public static final RegisteredBlock ORANGE_GLOWING_OBSIDIAN_PRESSURE_PLATE = regGlowingObsidianPlate(GlowingObsidianColor.ORANGE);
    public static final RegisteredBlock RED_GLOWING_OBSIDIAN_PRESSURE_PLATE = regGlowingObsidianPlate(GlowingObsidianColor.RED);
    public static final RegisteredBlock VIOLET_GLOWING_OBSIDIAN_PRESSURE_PLATE = regGlowingObsidianPlate(GlowingObsidianColor.VIOLET);
    public static final RegisteredBlock WHITE_GLOWING_OBSIDIAN_PRESSURE_PLATE = regGlowingObsidianPlate(GlowingObsidianColor.WHITE);
    public static final RegisteredBlock YELLOW_GLOWING_OBSIDIAN_PRESSURE_PLATE = regGlowingObsidianPlate(GlowingObsidianColor.YELLOW);
    public static final RegisteredBlock GOLD_GLOWING_OBSIDIAN_PRESSURE_PLATE = regGlowingObsidianPlate(GlowingObsidianColor.GOLD);
    public static final RegisteredBlock SILVER_GLOWING_OBSIDIAN_PRESSURE_PLATE = regGlowingObsidianPlate(GlowingObsidianColor.SILVER);
    public static final RegisteredBlock IRON_GLOWING_OBSIDIAN_PRESSURE_PLATE = regGlowingObsidianPlate(GlowingObsidianColor.IRON);
    public static final RegisteredBlock COPPER_GLOWING_OBSIDIAN_PRESSURE_PLATE = regGlowingObsidianPlate(GlowingObsidianColor.COPPER);

    public static void init() { }

    private static RegisteredBlock regWoodPlate(String name, ItemLike planks) {
        return regPlate(name, BlockType.WOOD, Sensitivity.EVERYTHING, 30, planks);
    }

    private static RegisteredBlock regMetalPlate(String name, int ticks, ItemLike craftItem) {
        return regPlate(name, BlockType.METAL, Sensitivity.MOBS, ticks, craftItem);
    }

    private static RegisteredBlock regStonePlate(String name, ItemLike stone) {
        return regStonePlate(name, PMBlockSetTypes.VARIANT_STONE, stone);
    }

    private static RegisteredBlock regStonePlate(String name, BlockSetType stoneType, ItemLike stone) {
        return regPlate(name, BlockType.STONE.getProperties(), stoneType, Sensitivity.MOBS, 20, stone)
            .tags(
                BlockTags.STONE_PRESSURE_PLATES,
                BlockTags.MINEABLE_WITH_PICKAXE
            );
    }
    
    private static RegisteredBlock regGlowingObsidianPlate(GlowingObsidianColor color) {
        return regPlate(
            color.getName() + "_glowing_obsidian",
            "paradisemod:block/glowing_obsidian/" + color.getName(),
            BlockType.STRONG_STONE
                .getProperties()
                .lightLevel(state -> 7),
            PMBlockSetTypes.VARIANT_STONE,
            CustomPressurePlate.Sensitivity.PLAYERS,
            20,
            Building.GLOWING_OBSIDIAN.get(color)
        )
            .localizedName(
                color.localizedName(false) + " Glowing Obsidian Pressure Plate",
                "Placa de presión de obsidiana brillante " + color.localizedName(true)
            );
    }

    private static RegisteredBlock regPlate(String name, BlockType type, Sensitivity sensitivity, int ticks, ItemLike craftItem) {
        ArrayList<TagKey<Block>> tags = new ArrayList<>();
        var blockSetType = PMBlockSetTypes.VARIANT_STONE;

        switch(type) {
            case WOOD -> {
                blockSetType = PMBlockSetTypes.VARIANT_WOOD;
                tags.add(BlockTags.WOODEN_PRESSURE_PLATES);
            }

            case METAL -> {
                blockSetType = PMBlockSetTypes.METAL;
            }

            case WEAK_METAL -> {
                blockSetType = PMBlockSetTypes.WEAK_METAL;
            }

            case SOIL -> {
                blockSetType = PMBlockSetTypes.SOIL;
            }

            default -> { }
        }

        if(type != BlockType.WOOD) {
            tags.add(BlockTags.PRESSURE_PLATES);
            tags.addAll(type.tags());
        }

        return regPlate(name, type.getProperties().noCollission(), blockSetType, sensitivity, ticks, craftItem)
            .tags(tags);
    }

    private static RegisteredBlock regPlate(String name, BlockType blockType, BlockSetType blockSetType, Sensitivity sensitivity, int ticks, ItemLike craftItem) {
        return regPlate(name, blockType.getProperties(), blockSetType, sensitivity, ticks, craftItem);
    }

    private static RegisteredBlock regPlate(String name, Block.Properties props, BlockSetType blockType, Sensitivity sensitivity, int ticks, ItemLike craftItem) {
        return regPlate(name, Buttons.texturePath(name), props.noCollission(), blockType, sensitivity, ticks, craftItem);
    }

    private static RegisteredBlock regPlate(String name, String texturePath, Block.Properties props, BlockSetType blockType, Sensitivity sensitivity, int ticks, ItemLike craftItem) {
        return PMRegistries.regBlockItem(
            name + "_pressure_plate",
            () -> new CustomPressurePlate(sensitivity, blockType, props.noCollission(), ticks)
        )
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .blockStateGenerator((block, generator) -> genPlateBlockState(name, texturePath, block, generator))
            .itemModel((block, generator) -> generator.withExistingParent(name + "_pressure_plate", "paradisemod:block/plate/" + name))
            .recipe(
                (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item)
                    .pattern("II")
                    .define('I', craftItem)
            )
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Pressure Plate",
                "Placa de presión de " + Utils.localizedMaterialName(name, true)
            );
    }

    private static void genPlateBlockState(String name, String texturePath, RegisteredBlock plateBlock, BlockStateGenerator generator) {
        if(name == "grass")
            generator.pressurePlateBlock(
                (CustomPressurePlate) plateBlock.get(),
                generator.existingModel("plate/grass"),
                generator.existingModel("plate/grass_down")
            );
        else {
            var modelBuilder = generator.models();
            var plate = modelBuilder.pressurePlate("block/plate/" + name, new ResourceLocation(texturePath));
            var plateDown = modelBuilder.pressurePlateDown("block/plate/" + name + "_down", new ResourceLocation(texturePath));

            if(name == "glass") {
                plate = plate.renderType("cutout");
                plateDown = plateDown.renderType("cutout");
            }
            else if(name.contains("glass")) {
                plate = plate.renderType("translucent");
                plateDown = plateDown.renderType("translucent");
            }

            generator.pressurePlateBlock((CustomPressurePlate) plateBlock.get(), plate, plateDown);
        }
    }
}