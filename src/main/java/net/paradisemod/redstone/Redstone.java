package net.paradisemod.redstone;

import java.util.ArrayList;

import net.minecraft.client.renderer.blockentity.BlockEntityRenderers;
import net.minecraft.core.Direction;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.LeverBlock;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.properties.AttachFace;
import net.minecraftforge.common.Tags;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.Events;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.data.assets.PMTranslations;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.redstone.blocks.Antenna;
import net.paradisemod.redstone.blocks.EntityObserver;
import net.paradisemod.redstone.blocks.PulseExtenderRepeater;
import net.paradisemod.redstone.blocks.RedstoneWire;
import net.paradisemod.redstone.blocks.Spike;
import net.paradisemod.redstone.blocks.Transmitter;
import net.paradisemod.redstone.blocks.WireRepeater;
import net.paradisemod.redstone.blocks.superpiston.MovingSuperPiston;
import net.paradisemod.redstone.blocks.superpiston.SuperPiston;
import net.paradisemod.redstone.blocks.superpiston.SuperPistonHead;
import net.paradisemod.redstone.tile.SuperPistonEntity;
import net.paradisemod.redstone.tile.SuperPistonRenderer;

public class Redstone {
    public static final RegisteredBlock SPIKE = PMRegistries.regBlockItem("spike", Spike::new)
        .tab(CreativeModeTabs.REDSTONE_BLOCKS)
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item)
                .pattern("I")
                .pattern("I")
                .pattern("N")
                .define('I', Tags.Items.INGOTS_IRON)
                .define('N', Tags.Items.INGOTS_NETHERITE)
        )
        .localizedName("Spike", "Pincho");

    public static final RegisteredBlock ENTITY_OBSERVER = PMRegistries.regBlockItem(
        "entity_observer",
        EntityObserver::new
    )
        .tab(CreativeModeTabs.REDSTONE_BLOCKS)
        .tag(BlockTags.MINEABLE_WITH_PICKAXE)
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item)
                .pattern("CCC")
                .pattern("RDQ")
                .pattern("CCC")
                .define('C', Blocks.COBBLESTONE)
                .define('D', Blocks.DRAGON_HEAD)
                .define('R', Items.REDSTONE)
                .define('Q', Items.QUARTZ)
        )
        .localizedName("Entity Observer", "Observador de entidades");

    public static final RegisteredBlock MOSSY_COBBLESTONE_LEVER = PMRegistries.regBlockItem(
        "mossy_cobblestone_lever",
        () -> new LeverBlock(Block.Properties.copy(Blocks.LEVER))
    )
        .tab(CreativeModeTabs.REDSTONE_BLOCKS)
        .itemModel((block, generator) -> generator.flatBlockItem(block))
        .blockStateGenerator(
            (block, generator) -> {
                var lever = generator.existingModel("mossy_cobblestone_lever");
                var leverOn = generator.existingModel("mossy_cobblestone_lever_on");

                generator.getVariantBuilder(block.get())
                    .forAllStates(
                        state -> {
                            AttachFace face = state.getValue(LeverBlock.FACE);
                            Direction facing = state.getValue(LeverBlock.FACING);
                            boolean powered = state.getValue(LeverBlock.POWERED);
                            var xRot = 0;
                            var yRot = 0;

                            switch(face) {
                                case FLOOR -> yRot = BlockStateGenerator.calcYRot(facing);

                                case CEILING -> {
                                    xRot = 180;
                                    switch(facing) {
                                        case EAST -> yRot = 270;
                                        case NORTH -> yRot = 180;
                                        case WEST -> yRot = 90;
                                    }
                                }

                                case WALL -> {
                                    xRot = 90;
                                    switch(facing) {
                                        case EAST -> yRot = 90;
                                        case SOUTH -> yRot = 180;
                                        case WEST -> yRot = 270;
                                    }
                                }
                            }

                            return generator.buildVariantModel(powered ? leverOn : lever, xRot, yRot);
                        }
                    );
            }
        )
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item)
                .pattern("s")
                .pattern("M")
                .define('s', Items.STICK)
                .define('M', Blocks.MOSSY_COBBLESTONE)
        )
        .localizedName("Mossy Cobblestone Lever", "Palanca de piedra labrada musgosa");

    public static final RegisteredBlock PULSE_EXTENDER_REPEATER = PMRegistries.regBlockItem("pulse_extender_repeater", PulseExtenderRepeater::new)
        .tab(CreativeModeTabs.REDSTONE_BLOCKS)
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item)
                .pattern(" X ")
                .pattern("#Q#")
                .pattern("III")
                .define('#', Blocks.REDSTONE_TORCH)
                .define('X', Items.REDSTONE)
                .define('I', Tags.Items.STONE)
                .define('Q', Items.QUARTZ)
        )
        .localizedName("Pulse Extender Repeater", "Repetidor de extensor de pulso");

    public static final RegisteredBlock REDSTONE_WIRE = PMRegistries.regBlockItem("redstone_wire", RedstoneWire::new)
        .tab(CreativeModeTabs.REDSTONE_BLOCKS)
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item, 16)
                .pattern("GRG")
                .pattern("GRG")
                .pattern("GRG")
                .define('G', Tags.Items.INGOTS_GOLD)
                .define('R', Items.REDSTONE)
        )
        .localizedName("Redstone Wire", "Alambre de piedra roja");

    public static final RegisteredBlock WIRE_REPEATER = PMRegistries.regBlockItem("wire_repeater", WireRepeater::new)
        .tab(CreativeModeTabs.REDSTONE_BLOCKS)
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item)
                .pattern("TWT")
                .pattern("SSS")
                .define('W', REDSTONE_WIRE)
                .define('T', Blocks.REDSTONE_TORCH)
                .define('S', Tags.Items.STONE)
        )
        .localizedName("Redstone Wire Repeater", "Repetidor de alambre de piedra roja");

    public static final RegisteredBlock SUPER_PISTON = PMRegistries.regBlockItem("super_piston", () -> new SuperPiston(false))
        .tab(CreativeModeTabs.REDSTONE_BLOCKS)
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item)
                .pattern("III")
                .pattern("ODO")
                .pattern("ORO")
                .define('R', Items.REDSTONE)
                .define('I', Tags.Items.INGOTS_IRON)
                .define('D', Items.DIAMOND)
                .define('O', Blocks.OBSIDIAN)
        )
        .localizedName("Super Piston", "Superpistón");

    public static final RegisteredBlock STICKY_SUPER_PISTON = PMRegistries.regBlockItem("sticky_super_piston", () -> new SuperPiston(true))
        .tab(CreativeModeTabs.REDSTONE_BLOCKS)
        .recipe(
            (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item)
                .pattern("S")
                .pattern("P")
                .define('S', Items.SLIME_BALL)
                .define('P', SUPER_PISTON)
        )
        .localizedName("Sticky Super Piston", "Superpistón pegajoso");

    public static final RegisteredBlock SUPER_PISTON_HEAD = PMRegistries.regBlock("super_piston_head", SuperPistonHead::new);
    public static final RegisteredBlock MOVING_SUPER_PISTON = PMRegistries.regBlock("moving_super_piston", MovingSuperPiston::new);
    public static final RegistryObject<BlockEntityType<SuperPistonEntity>> SUPER_PISTON_TILE = PMRegistries.createTile("super_piston_tile", SuperPistonEntity::new, MOVING_SUPER_PISTON);

    public static final ArrayList<RegisteredBlock> TRANSMITTERS = new ArrayList<>();
    public static final ArrayList<RegisteredBlock> ANTENNAS = new ArrayList<>();

    public static void init(IEventBus eventbus) {
        for(var color : DyeColor.values()) {
            var transmitter = PMRegistries.regBlockItem(
                    color.getName() + "_transmitter",
                    () -> new Transmitter(color)
                )
                    .tab(CreativeModeTabs.REDSTONE_BLOCKS)
                    .itemTag(PMTags.Items.TRANSMITTERS);

            if(color == DyeColor.RED)
                transmitter = transmitter.recipe(
                    (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item)
                        .pattern(" X ")
                        .pattern("#G#")
                        .pattern("III")
                        .define('#', Blocks.REDSTONE_TORCH)
                        .define('X', Items.REDSTONE)
                        .define('I', Tags.Items.STONE)
                        .define('G', Tags.Items.INGOTS_GOLD)
                );

            TRANSMITTERS.add(
                transmitter.recipe(
                    (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item, 8)
                        .pattern("TTT")
                        .pattern("TDT")
                        .pattern("TTT")
                        .define('T', PMTags.Items.TRANSMITTERS)
                        .define('D', ParadiseMod.DYES_BY_COLOR.get(color))
                )
                .localizedName(
                    PMTranslations.englishColor(color) + " Transmitter",
                    "Transmisor " + PMTranslations.spanishColor(color, false)
                )
            );
        }

        for(var color : DyeColor.values()) {
            var antenna = PMRegistries.regBlockItem(
                    color.getName() + "_antenna",
                    () -> new Antenna(color)
            )
                .tab(CreativeModeTabs.REDSTONE_BLOCKS)
                .itemTag(PMTags.Items.ANTENNAS);

                if(color == DyeColor.RED)
                    antenna = antenna.recipe(
                        (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item)
                            .pattern(" r ")
                            .pattern(" RN")
                            .pattern("SSS")
                            .define('r', Blocks.REDSTONE_TORCH)
                            .define('R', Items.REDSTONE)
                            .define('S', Tags.Items.STONE)
                            .define('N', Tags.Items.INGOTS_NETHERITE)
                    );

            ANTENNAS.add(
                antenna.recipe(
                    (item, generator) -> generator.getShapedBuilder(RecipeCategory.REDSTONE, item, 8)
                        .pattern("AAA")
                        .pattern("ADA")
                        .pattern("AAA")
                        .define('A', PMTags.Items.ANTENNAS)
                        .define('D', ParadiseMod.DYES_BY_COLOR.get(color))
                )
                .localizedName(
                    PMTranslations.englishColor(color) + " Antenna",
                    "Antena " + PMTranslations.spanishColor(color, true)
                )
            );
        }

        // submodules
        Lamps.init();
        Buttons.init();
        Plates.init();

        for(var entry : Lamps.REDSTONE_LAMPS.entrySet()) {
            var dyeColor = entry.getKey();
            var lamp = entry.getValue();

            Events.registerBasicColoredBlocks(dyeColor.getFireworkColor(), lamp);
        }

        Events.registerGrassColoredBlocks(Buttons.GRASS_BUTTON, Plates.GRASS_PRESSURE_PLATE);
        Events.registerBasicColoredBlocks(0xFFE0AA1E, Lamps.GOLD_REDSTONE_LAMP);
        Events.registerRainbowBlocks(Lamps.IRIDESCENT_REDSTONE_LAMP);
        ParadiseMod.LOG.info("Loaded Redstone module");
    }

    public static void initClient() {
        BlockEntityRenderers.register(SUPER_PISTON_TILE.get(), SuperPistonRenderer::new);
    }
}