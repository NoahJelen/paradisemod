package net.paradisemod.redstone;

import java.util.ArrayList;

import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.ButtonBlock;
import net.minecraft.world.level.block.state.properties.BlockSetType;
import net.paradisemod.base.BlockType;
import net.paradisemod.base.PMBlockSetTypes;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.assets.BlockStateGenerator;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.building.Building;
import net.paradisemod.building.GlowingObsidianColor;
import net.paradisemod.misc.Misc;
import net.paradisemod.world.DeepDarkBlocks;

public class Buttons {
    public static final RegisteredBlock ANDESITE_BUTTON = regStoneButton("andesite", Blocks.ANDESITE);
    public static final RegisteredBlock DIORITE_BUTTON = regStoneButton("diorite", Blocks.DIORITE);
    public static final RegisteredBlock GRANITE_BUTTON = regStoneButton("granite", Blocks.GRANITE);
    public static final RegisteredBlock COBBLESTONE_BUTTON = regStoneButton("cobblestone", Blocks.COBBLESTONE);
    public static final RegisteredBlock DEEPSLATE_BUTTON = regStoneButton("deepslate", PMBlockSetTypes.DEEPSLATE, Blocks.DEEPSLATE);
    public static final RegisteredBlock POLISHED_DEEPSLATE_BUTTON = regStoneButton("polished_deepslate", PMBlockSetTypes.DEEPSLATE, Blocks.POLISHED_DEEPSLATE);
    public static final RegisteredBlock COBBLED_DEEPSLATE_BUTTON = regStoneButton("cobbled_deepslate", PMBlockSetTypes.DEEPSLATE, Blocks.COBBLED_DEEPSLATE);
    public static final RegisteredBlock MOSSY_COBBLESTONE_BUTTON = regStoneButton("mossy_cobblestone", Blocks.MOSSY_COBBLESTONE);
    public static final RegisteredBlock END_STONE_BUTTON = regStoneButton("end_stone", Blocks.END_STONE);
    public static final RegisteredBlock DRIPSTONE_BUTTON = regStoneButton("dripstone", PMBlockSetTypes.DRIPSTONE, Blocks.DRIPSTONE_BLOCK);
    public static final RegisteredBlock CALCITE_BUTTON = regStoneButton("calcite", PMBlockSetTypes.CALCITE, Blocks.CALCITE);
    public static final RegisteredBlock TUFF_BUTTON = regStoneButton("tuff", PMBlockSetTypes.TUFF, Blocks.TUFF);

    public static final RegisteredBlock DARKSTONE_BUTTON = regStoneButton("darkstone", DeepDarkBlocks.DARKSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKSTONE_BUTTON = regStoneButton("blackstone", Blocks.BLACKSTONE);
    public static final RegisteredBlock POLISHED_ANDESITE_BUTTON = regStoneButton("polished_andesite", Blocks.POLISHED_ANDESITE);
    public static final RegisteredBlock POLISHED_DIORITE_BUTTON = regStoneButton("polished_diorite", Blocks.POLISHED_DIORITE);
    public static final RegisteredBlock POLISHED_GRANITE_BUTTON = regStoneButton("polished_granite", Blocks.POLISHED_GRANITE);
    public static final RegisteredBlock POLISHED_DRIPSTONE_BUTTON = regStoneButton("polished_dripstone", PMBlockSetTypes.DRIPSTONE, Building.POLISHED_DRIPSTONE);
    public static final RegisteredBlock POLISHED_CALCITE_BUTTON = regStoneButton("polished_calcite", PMBlockSetTypes.CALCITE, Building.POLISHED_CALCITE);
    public static final RegisteredBlock POLISHED_TUFF_BUTTON = regStoneButton("polished_tuff", PMBlockSetTypes.TUFF, Building.POLISHED_TUFF);
    public static final RegisteredBlock SMOOTH_STONE_BUTTON = regStoneButton("smooth_stone", Blocks.SMOOTH_STONE);
    public static final RegisteredBlock POLISHED_END_STONE_BUTTON = regStoneButton("polished_end_stone", Building.POLISHED_END_STONE);
    public static final RegisteredBlock POLISHED_DARKSTONE_BUTTON = regStoneButton("polished_darkstone", DeepDarkBlocks.POLISHED_DARKSTONE);
    public static final RegisteredBlock SANDSTONE_BUTTON = regStoneButton("sandstone", Blocks.SANDSTONE);
    public static final RegisteredBlock RED_SANDSTONE_BUTTON = regStoneButton("red_sandstone", Blocks.RED_SANDSTONE);

    public static final RegisteredBlock BLACKENED_SANDSTONE_BUTTON = regStoneButton("blackened_sandstone", DeepDarkBlocks.BLACKENED_SANDSTONE)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BEDROCK_BUTTON = regButton("bedrock", 20, BlockType.INDESTRUCTIBLE, Blocks.BEDROCK);
    public static final RegisteredBlock OBSIDIAN_BUTTON = regButton("obsidian", 20, BlockType.STRONG_STONE, Blocks.OBSIDIAN);

    public static final RegisteredBlock DIRT_BUTTON = regButton("dirt", 20, PMBlockSetTypes.SOIL, BlockType.SOIL, Blocks.DIRT)
        .tags(
            BlockTags.BUTTONS,
            BlockTags.MINEABLE_WITH_SHOVEL
        );

    public static final RegisteredBlock GRASS_BUTTON = regButton(
        "grass",
        20,
        PMBlockSetTypes.GRASS,
        BlockType.SOIL,
        Blocks.GRASS_BLOCK
    )
        .tags(
            BlockTags.BUTTONS,
            BlockTags.MINEABLE_WITH_SHOVEL
        );

    public static final RegisteredBlock DIAMOND_BUTTON = regMetalButton("diamond", 14, Items.DIAMOND);
    public static final RegisteredBlock EMERALD_BUTTON = regMetalButton("emerald", 10, Items.EMERALD);
    public static final RegisteredBlock RUBY_BUTTON = regMetalButton("ruby", 2, Misc.RUBY);
    public static final RegisteredBlock RUSTED_IRON_BUTTON = regMetalButton("rusted_iron", 2, Misc.RUSTED_IRON_INGOT);
    public static final RegisteredBlock SILVER_BUTTON = regMetalButton("silver", 20, Misc.SILVER_INGOT);

    public static final RegisteredBlock GLASS_BUTTON = regButton(
        "glass",
        5,
        PMBlockSetTypes.GLASS,
        BlockType.GLASS,
        Blocks.GLASS
    )
        .tag(BlockTags.BUTTONS);

    public static final RegisteredBlock CACTUS_BUTTON = regWoodButton("cactus", Building.CACTUS_BLOCK);
    public static final RegisteredBlock PALO_VERDE_BUTTON = regWoodButton("palo_verde", Building.PALO_VERDE_PLANKS);
    public static final RegisteredBlock MESQUITE_BUTTON = regWoodButton("mesquite", Building.MESQUITE_PLANKS);
    public static final RegisteredBlock POLISHED_ASPHALT_BUTTON = regStoneButton("polished_asphalt", Building.POLISHED_ASPHALT);

    public static final RegisteredBlock BLACKENED_OAK_BUTTON = regWoodButton("blackened_oak", DeepDarkBlocks.BLACKENED_OAK_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock BLACKENED_SPRUCE_BUTTON = regWoodButton("blackened_spruce", DeepDarkBlocks.BLACKENED_SPRUCE_PLANKS)
        .tab(DeepDarkBlocks.DEEP_DARK_TAB);

    public static final RegisteredBlock GLOWING_OAK_BUTTON = regButton(
        "glowing_oak",
        30,
        PMBlockSetTypes.VARIANT_WOOD,
        Block.Properties.copy(Blocks.OAK_BUTTON)
            .lightLevel(s -> 7),
        DeepDarkBlocks.GLOWING_OAK_PLANKS
    )
        .tab(DeepDarkBlocks.DEEP_DARK_TAB)
        .tag(BlockTags.WOODEN_BUTTONS);

    public static final RegisteredBlock GLOWING_CACTUS_BUTTON = regButton(
        "glowing_cactus",
        30,
        PMBlockSetTypes.VARIANT_WOOD,
        Block.Properties.copy(Blocks.OAK_BUTTON)
            .lightLevel(s -> 7),
        DeepDarkBlocks.GLOWING_CACTUS_BLOCK
    )
        .tab(DeepDarkBlocks.DEEP_DARK_TAB)
        .tag(BlockTags.WOODEN_BUTTONS);

    public static final RegisteredBlock BLACK_GLOWING_OBSIDIAN_BUTTON = regGlowingObsidianButton(GlowingObsidianColor.BLACK);
    public static final RegisteredBlock BLUE_GLOWING_OBSIDIAN_BUTTON = regGlowingObsidianButton(GlowingObsidianColor.BLUE);
    public static final RegisteredBlock GREEN_GLOWING_OBSIDIAN_BUTTON = regGlowingObsidianButton(GlowingObsidianColor.GREEN);
    public static final RegisteredBlock INDIGO_GLOWING_OBSIDIAN_BUTTON = regGlowingObsidianButton(GlowingObsidianColor.INDIGO);
    public static final RegisteredBlock ORANGE_GLOWING_OBSIDIAN_BUTTON = regGlowingObsidianButton(GlowingObsidianColor.ORANGE);
    public static final RegisteredBlock RED_GLOWING_OBSIDIAN_BUTTON = regGlowingObsidianButton(GlowingObsidianColor.RED);
    public static final RegisteredBlock VIOLET_GLOWING_OBSIDIAN_BUTTON = regGlowingObsidianButton(GlowingObsidianColor.VIOLET);
    public static final RegisteredBlock WHITE_GLOWING_OBSIDIAN_BUTTON = regGlowingObsidianButton(GlowingObsidianColor.WHITE);
    public static final RegisteredBlock YELLOW_GLOWING_OBSIDIAN_BUTTON = regGlowingObsidianButton(GlowingObsidianColor.YELLOW);
    public static final RegisteredBlock GOLD_GLOWING_OBSIDIAN_BUTTON = regGlowingObsidianButton(GlowingObsidianColor.GOLD);
    public static final RegisteredBlock SILVER_GLOWING_OBSIDIAN_BUTTON = regGlowingObsidianButton(GlowingObsidianColor.SILVER);
    public static final RegisteredBlock IRON_GLOWING_OBSIDIAN_BUTTON = regGlowingObsidianButton(GlowingObsidianColor.IRON);
    public static final RegisteredBlock COPPER_GLOWING_OBSIDIAN_BUTTON = regGlowingObsidianButton(GlowingObsidianColor.COPPER);

    public static void init() { }

    private static RegisteredBlock regWoodButton(String name, ItemLike planks) {
        return regButton(name, 30, BlockType.WOOD, planks);
    }

    private static RegisteredBlock regMetalButton(String name, int ticks, ItemLike craftItem) {
        return regButton(name, ticks, BlockType.METAL, craftItem);
    }

    private static RegisteredBlock regStoneButton(String name, ItemLike stone) {
        return regStoneButton(name, PMBlockSetTypes.VARIANT_STONE, stone);
    }

    private static RegisteredBlock regStoneButton(String name, BlockSetType stoneType, ItemLike stone) {
        return regButton(
            name, 20,
            stoneType,
            BlockType.STONE,
            stone
        )
            .tag(BlockTags.STONE_BUTTONS);
    }

    private static RegisteredBlock regGlowingObsidianButton(GlowingObsidianColor color) {
        return regButton(
            color.getName() + "_glowing_obsidian",
            "paradisemod:block/glowing_obsidian/" + color.getName(),
            20,
            PMBlockSetTypes.VARIANT_STONE,
            BlockType.STRONG_STONE.getProperties().lightLevel(s -> 7),
            Building.GLOWING_OBSIDIAN.get(color)
        )
            .tag(BlockTags.BUTTONS)
            .tags(BlockType.STRONG_STONE.tags())
            .localizedName(
                color.localizedName(false) + " Glowing Obsidian Button",
                "Botón de obsidiana brillante " + color.localizedName(true)
            );
    }

    private static RegisteredBlock regButton(String name, int ticks, BlockType type, ItemLike craftItem) {
        ArrayList<TagKey<Block>> tags = new ArrayList<>();

        var blockSetType = PMBlockSetTypes.VARIANT_STONE;

        switch(type) {
            case WOOD -> {
                blockSetType = PMBlockSetTypes.VARIANT_WOOD;
                tags.add(BlockTags.WOODEN_BUTTONS);
            }

            case METAL -> {
                blockSetType = PMBlockSetTypes.METAL;
            }

            case WEAK_METAL -> {
                blockSetType = PMBlockSetTypes.WEAK_METAL;
            }

            case STONE, ENHANCED_STONE, STRONG_STONE -> {
                tags.add(BlockTags.STONE_BUTTONS);
            }

            default -> { }
        }

        if(type != BlockType.WOOD && type != BlockType.STONE && type != BlockType.ENHANCED_STONE && type != BlockType.STRONG_STONE) {
            tags.add(BlockTags.BUTTONS);
            tags.addAll(type.tags());
        }

        return regButton(name, ticks, blockSetType, type.getProperties(), craftItem)
            .tags(tags);
    }

    private static RegisteredBlock regButton(String name, int ticks, BlockSetType blockType, Block.Properties properties, ItemLike craftItem) {
        return regButton(name, texturePath(name), ticks, blockType, properties, craftItem);
    }

    private static RegisteredBlock regButton(String name, int ticks, BlockSetType blockSetType, BlockType blockType, ItemLike craftItem) {
        return regButton(name, texturePath(name), ticks, blockSetType, blockType.getProperties(), craftItem);
    }

    private static RegisteredBlock regButton(String name, String texturePath, int ticks, BlockSetType blockType, Block.Properties properties, ItemLike craftItem) {
        return PMRegistries.regBlockItem(
            name + "_button",
            () -> new ButtonBlock(properties.noCollission(), blockType, ticks, blockType == PMBlockSetTypes.VARIANT_WOOD)
        )
            .recipe((item, generator) -> generator.shapelessRecipe(RecipeCategory.REDSTONE, item, craftItem))
            .tab(CreativeModeTabs.BUILDING_BLOCKS)
            .itemModel(
                (block, generator) -> {
                    if(name != "grass")
                        generator.buttonInventory(name + "_button", new ResourceLocation(texturePath));
                }
            )
            .blockStateGenerator((block, generator) -> genButtonBlockState(name, texturePath, block, generator))
            .localizedName(
                Utils.localizedMaterialName(name, false) + " Button",
                "Botón de " + Utils.localizedMaterialName(name, true)
            );
    }

    private static void genButtonBlockState(String name, String texturePath, RegisteredBlock buttonBlock, BlockStateGenerator generator) {
        if(name == "grass")
            generator.buttonBlock(
                (ButtonBlock) buttonBlock.get(),
                generator.existingModel("button/grass"),
                generator.existingModel("button/grass_pressed")
            );
        else {
            var modelBuilder = generator.models();
            var button = modelBuilder.button("block/button/" + name, new ResourceLocation(texturePath));
            var buttonPressed = modelBuilder.buttonPressed("block/button/" + name + "_pressed", new ResourceLocation(texturePath));

            if(name == "glass") {
                button = button.renderType("cutout");
                buttonPressed = buttonPressed.renderType("cutout");
            }
            else if(name.contains("glass")) {
                button = button.renderType("translucent");
                buttonPressed = buttonPressed.renderType("translucent");
            }

            generator.buttonBlock((ButtonBlock) buttonBlock.get(), button, buttonPressed);
        }
    }

    public static String texturePath(String name) {
        if(name == "blackened_sandstone")
            return "paradisemod:block/blackened_sandstone_top";
        else if(name.endsWith("sandstone"))
            return "minecraft:block/" + name + "_top";

        return switch(name) {
            case "grass" -> "minecraft:block/grass_block_top";
            case "dripstone", "diamond", "emerald" ->
                "minecraft:block/" + name + "_block";

            case "darkstone", "polished_dripstone",
            "polished_calcite", "polished_tuff",
            "polished_end_stone", "polished_darkstone",
            "polished_asphalt" ->
                "paradisemod:block/" + name;

            case "ruby", "rusted_iron", "silver", "cactus", "glowing_cactus" -> "paradisemod:block/" + name + "_block";

            case "palo_verde",
            "mesquite", "blackened_oak",
            "blackened_spruce", "glowing_oak" ->
                "paradisemod:block/" + name + "_planks";

            case "basalt" -> "minecraft:block/basalt_side";
            case "icicle" -> "minecraft:block/ice";
            case "blue_icicle" -> "minecraft:block/blue_ice";

            default -> "minecraft:block/" + name;
        };
    }
}