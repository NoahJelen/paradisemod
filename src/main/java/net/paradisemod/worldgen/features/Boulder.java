package net.paradisemod.worldgen.features;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;

public class Boulder extends BasicFeature {
    private final ArrayList<Block> stones = new ArrayList<>();

    public Boulder(Block... stones) {
        this.stones.addAll(List.of(stones));
    }

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        for(; pos.getY() > world.getMinBuildHeight() + 3; pos = pos.below()) {
            if (!world.isEmptyBlock(pos.below())) {
                BlockState blockstate = world.getBlockState(pos.below());
                if(isDirt(blockstate) || isStone(blockstate))
                    break;
            }
        }

        if (pos.getY() <= world.getMinBuildHeight() + 3)
            return false;
        else {
            for(int l = 0; l < 3; l++) {
                var i = rand.nextInt(2);
                var j = rand.nextInt(2);
                var k = rand.nextInt(2);
                float f = (float) (i + j + k) * 0.333F + 0.5F;

                for(var pos1 : BlockPos.betweenClosed(pos.offset(-i, -j, -k), pos.offset(i, j, k)))
                    if (pos1.distSqr(pos) <= Math.pow(f, 2))
                        world.setBlock(pos1, stones.get(rand.nextInt(stones.size())).defaultBlockState(), 4);

                pos = pos.offset(-1 + rand.nextInt(2), -rand.nextInt(2), -1 + rand.nextInt(2));
            }

            return true;
        }
    }
}