package net.paradisemod.worldgen.features.cave;

import java.util.HashMap;

import com.machinezoo.noexception.optional.OptionalBoolean;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Holder;
import net.minecraft.tags.BiomeTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraftforge.common.Tags;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.PMWorld;
import net.paradisemod.world.biome.PMBiomes;
import net.paradisemod.world.blocks.LargeCaveFormation;
import net.paradisemod.world.blocks.SmallCaveFormation;
import net.paradisemod.worldgen.features.BasicFeature;

public class CaveFormation extends BasicFeature {
    private static final HashMap<Block, Block> FORMATIONS = new HashMap<>();
    private static final HashMap<Block, Block> LARGE_FORMATIONS = new HashMap<>();

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        initStoneFormations();
        var worldgenLimit = PMCaveFeatures.dimWorldgenLimit(world);
        var maybeInverted = isInverted(world, pos);

        if(
            maybeInverted.isPresent() &&
            (!PMCaveFeatures.seesSky(world, pos) || pos.getY() <= worldgenLimit) &&
            (world.getBlockState(pos).isAir() || world.isWaterAt(pos))
        ) {
            var biome = world.getBiome(pos);
            var isLarge = rand.nextBoolean();
            var isInverted = maybeInverted.getAsBoolean();

            if(
                PMCaveFeatures.seesSky(world, pos) &&
                (
                    biome.is(PMBiomes.GLOWING_GLACIER) ||
                    biome.is(PMBiomes.POLAR_WINTER) ||
                    biome.is(PMBiomes.GLACIER) ||
                    biome.is(PMBiomes.SUBGLACIAL_VOLCANIC_FIELD)
                )
            )
                return false;

            if(isLarge) {
                var formation = (LargeCaveFormation) PMWorld.LARGE_STONE_FORMATION.get();

                if(biome.is(PMTags.Biomes.DEEP_DARK))
                    formation = (LargeCaveFormation) PMWorld.LARGE_DARKSTONE_FORMATION.get();

                if(PMCaveFeatures.shouldBeMossy(rand, biome))
                    formation = (LargeCaveFormation) PMWorld.LARGE_MOSSY_COBBLESTONE_FORMATION.get();
                else if(shouldBeIcicle(rand, biome))
                    formation = (LargeCaveFormation) (rand.nextBoolean() ? PMWorld.LARGE_ICICLE : PMWorld.LARGE_BLUE_ICICLE).get();
                else if(biome.is(Biomes.DESERT) && rand.nextInt(5) == 0)
                    formation = (LargeCaveFormation) PMWorld.LARGE_SANDSTONE_FORMATION.get();
                else if(biome.is(PMBiomes.DARK_DESERT) && rand.nextBoolean())
                    formation = (LargeCaveFormation) PMWorld.LARGE_BLACKENED_SANDSTONE_FORMATION.get();
                else if(biome.is(BiomeTags.IS_BADLANDS) && rand.nextInt(5) == 0)
                    formation = (LargeCaveFormation) PMWorld.LARGE_RED_SANDSTONE_FORMATION.get();
                else {
                    var facingState = world.getBlockState(pos.above(isInverted ? 1 : -1));
                    var newFormation = LARGE_FORMATIONS.get(facingState.getBlock());

                    if(newFormation != null)
                        formation = (LargeCaveFormation) newFormation;
                }

                formation.place(world, pos, isInverted);
            }
            else {
                var isWater = world.isWaterAt(pos);
                var formation = PMWorld.STONE_FORMATION.get();

                if(biome.is(PMTags.Biomes.DEEP_DARK))
                    formation = PMWorld.DARKSTONE_FORMATION.get();

                if(PMCaveFeatures.shouldBeMossy(rand, biome))
                    formation = PMWorld.MOSSY_COBBLESTONE_FORMATION.get();
                if(shouldBeIcicle(rand, biome))
                    formation = (rand.nextBoolean() ? PMWorld.ICICLE : PMWorld.BLUE_ICICLE).get();
                else if(biome.is(Biomes.DESERT) && rand.nextInt(5) == 0)
                    formation = PMWorld.SANDSTONE_FORMATION.get();
                else if(biome.is(PMBiomes.DARK_DESERT) && rand.nextBoolean())
                    formation = PMWorld.BLACKENED_SANDSTONE_FORMATION.get();
                else if(biome.is(BiomeTags.IS_BADLANDS) && rand.nextInt(5) == 0)
                    formation = PMWorld.RED_SANDSTONE_FORMATION.get();
                else {
                    var facingState = world.getBlockState(pos.above(isInverted ? 1 : -1));
                    var newFormation = FORMATIONS.get(facingState.getBlock());

                    if(newFormation != null)
                        formation = newFormation;
                }

                world.setBlock(
                    pos,
                    formation.defaultBlockState()
                        .setValue(SmallCaveFormation.FACING, isInverted ? Direction.DOWN : Direction.UP)
                        .setValue(SmallCaveFormation.WATERLOGGED, isWater),
                    32
                );
            }

            return true;
        }
        else return false;
    }

    private boolean shouldBeIcicle(RandomSource rand, Holder<Biome> biome) {
        return (biome.is(Tags.Biomes.IS_COLD) || biome.is(Tags.Biomes.IS_SNOWY)) && rand.nextInt(5) == 0;
    }

    private OptionalBoolean isInverted(WorldGenLevel world, BlockPos pos) {
        if(PMCaveFeatures.isValidBlock(world.getBlockState(pos.above())))
            return OptionalBoolean.of(true);
        else if(PMCaveFeatures.isValidBlock(world.getBlockState(pos.below())))
            return OptionalBoolean.of(false);
        else return OptionalBoolean.empty();
    }

    private static void initStoneFormations() {
        if(FORMATIONS.isEmpty() && LARGE_FORMATIONS.isEmpty()) {
            FORMATIONS.put(Blocks.ANDESITE, PMWorld.ANDESITE_FORMATION.get());
            FORMATIONS.put(Blocks.BASALT, PMWorld.BASALT_FORMATION.get());
            FORMATIONS.put(DeepDarkBlocks.DARKSTONE.get(), PMWorld.DARKSTONE_FORMATION.get());
            FORMATIONS.put(Blocks.DEEPSLATE, PMWorld.DEEPSLATE_FORMATION.get());
            FORMATIONS.put(Blocks.TUFF, PMWorld.TUFF_FORMATION.get());
            FORMATIONS.put(Blocks.DIORITE, PMWorld.DIORITE_FORMATION.get());
            FORMATIONS.put(Blocks.GRANITE, PMWorld.GRANITE_FORMATION.get());
            FORMATIONS.put(Blocks.NETHERRACK, PMWorld.NETHERRACK_FORMATION.get());
            FORMATIONS.put(Blocks.STONE, PMWorld.STONE_FORMATION.get());
            FORMATIONS.put(Blocks.END_STONE, PMWorld.END_STONE_FORMATION.get());
            LARGE_FORMATIONS.put(Blocks.ANDESITE, PMWorld.LARGE_ANDESITE_FORMATION.get());
            LARGE_FORMATIONS.put(Blocks.BASALT, PMWorld.LARGE_BASALT_FORMATION.get());
            LARGE_FORMATIONS.put(DeepDarkBlocks.DARKSTONE.get(), PMWorld.LARGE_DARKSTONE_FORMATION.get());
            LARGE_FORMATIONS.put(Blocks.DEEPSLATE, PMWorld.LARGE_DEEPSLATE_FORMATION.get());
            LARGE_FORMATIONS.put(Blocks.TUFF, PMWorld.LARGE_TUFF_FORMATION.get());
            LARGE_FORMATIONS.put(Blocks.DIORITE, PMWorld.LARGE_DIORITE_FORMATION.get());
            LARGE_FORMATIONS.put(Blocks.GRANITE, PMWorld.LARGE_GRANITE_FORMATION.get());
            LARGE_FORMATIONS.put(Blocks.NETHERRACK, PMWorld.LARGE_NETHERRACK_FORMATION.get());
            LARGE_FORMATIONS.put(Blocks.STONE, PMWorld.LARGE_STONE_FORMATION.get());
            LARGE_FORMATIONS.put(Blocks.END_STONE, PMWorld.LARGE_END_STONE_FORMATION.get());
        }
    }
}