package net.paradisemod.worldgen.features.cave;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.VineBlock;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.paradisemod.base.Utils;
import net.paradisemod.worldgen.features.BasicFeature;

public class CaveVine extends BasicFeature {

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        var worldgenLimit = PMCaveFeatures.dimWorldgenLimit(world);

        if(
            PMCaveFeatures.isValidBlock(world.getBlockState(pos.above())) &&
            world.getBlockState(pos).isAir() &&
            pos.getY() <= worldgenLimit
        ) {
            for(var dir : Direction.Plane.HORIZONTAL) {
                var newPos = pos.relative(dir);

                if (world.getBlockState(newPos).isSolid()) {
                    // place the first vine
                    var vineState = Blocks.VINE.defaultBlockState();
                    vineState = switch (dir) {
                        case NORTH -> vineState.setValue(VineBlock.SOUTH, true);
                        case SOUTH -> vineState.setValue(VineBlock.NORTH, true);
                        case WEST -> vineState.setValue(VineBlock.EAST, true);
                        case EAST -> vineState.setValue(VineBlock.WEST, true);
                        default -> Utils.modErrorTyped("Vines can't face down!");
                    };

                    world.setBlock(newPos, vineState.setValue(VineBlock.UP, true), 32);

                    // how long is the vine?
                    var length = rand.nextInt(4);

                    if(length > 0) {
                        // extend the vine down
                        for (int i = 1; i <= length; i++) {
                            var extPos = newPos.below(i);
                            var existingBlock = world.getBlockState(extPos);
                            if (existingBlock.isAir())
                                world.setBlock(extPos, vineState, 32);

                            else break;
                        }
                    }

                    return true;
                }
            }
        }

        return false;
    }
}