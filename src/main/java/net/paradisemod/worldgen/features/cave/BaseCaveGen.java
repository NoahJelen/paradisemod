package net.paradisemod.worldgen.features.cave;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Holder;
import net.minecraft.tags.BiomeTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.material.Fluids;
import net.minecraftforge.common.Tags;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.world.Ores;
import net.paradisemod.world.PMWorld;
import net.paradisemod.world.biome.PMBiomes;
import net.paradisemod.world.blocks.LargeCaveFormation;
import net.paradisemod.world.blocks.SmallCaveFormation;
import net.paradisemod.world.dimension.PMDimensions;
import net.paradisemod.world.fluid.PMFluids;
import net.paradisemod.worldgen.features.chunk.ChunkProcessor;

public class BaseCaveGen extends ChunkProcessor {
    @Override
    protected void processBlock(WorldGenLevel world, RandomSource rand, BlockPos pos, BlockState curState, Holder<Biome> biome, ChunkGenerator generator) {
        var stateAbove = world.getBlockState(pos.above());
        Block surface = null;
        Block underSurface = null;
        var dim = world.getLevel().dimension();

        if(PMBiomes.isSandyDesert(biome) && PMDimensions.isDimOverworldLike(world) && pos.getY() <= 80)
            surface = Blocks.SANDSTONE;
        else if(PMBiomes.isRockyDesert(biome)) {
            surface = Blocks.COARSE_DIRT;
            underSurface = Blocks.COARSE_DIRT;
        }
        else if(biome.is(PMBiomes.UNDERGROUND_MANGROVE_SWAMP) || biome.is(Biomes.MANGROVE_SWAMP)) {
            surface = Blocks.MUD;
            underSurface = Blocks.MUD;
        }
        else if(biome.is(BiomeTags.IS_JUNGLE) || biome.is(Tags.Biomes.IS_SWAMP)) {
            surface = Blocks.GRASS_BLOCK;
            underSurface = Blocks.DIRT;
        }
        else if(biome.is(Tags.Biomes.IS_MUSHROOM)) {
            surface = Blocks.MYCELIUM;
            underSurface = Blocks.DIRT;
        }
        else if(biome.is(PMTags.Biomes.SALT)) {
            surface = Ores.SALT_BLOCK.get();
            underSurface = Ores.COMPACT_SALT_BLOCK.get();
        }
        else if(biome.is(BiomeTags.IS_BADLANDS)) {
            if(pos.getY() <= 80)
                surface = Blocks.RED_SANDSTONE;

            if(PMCaveFeatures.isValidBlock(curState) && PMWorld.isBlockInWall(world, pos)) {
                genBadlandStripes(world, pos);
                return;
            }
        }

        // replace the cave surface block if it is valid and has a nonsolid block above it
        if(
            PMCaveFeatures.isValidBlock(curState) &&
            stateAbove.getFluidState().is(Fluids.EMPTY) &&
            !stateAbove.isSolid() &&
            !(stateAbove.getBlock() instanceof LargeCaveFormation) &&
            !(stateAbove.getBlock() instanceof SmallCaveFormation)
        ) {
            if(rand.nextInt(1000) == 0)
                world.setBlock(pos.above(), Blocks.COBWEB.defaultBlockState(), 32);

            // place the surface blocks
            if(surface != null) {
                world.setBlock(pos, surface.defaultBlockState(), 32);

                if(underSurface != null && world.getBlockState(pos.below()).isSolid())
                    world.setBlock(pos.below(), underSurface.defaultBlockState(), 32);
            }
        }
        else if(curState.getFluidState().is(Fluids.LAVA) || curState.is(Blocks.OBSIDIAN)) {
            // turn normal lava into psychedelic lava in the overworld core
            if(PMDimensions.Type.OVERWORLD_CORE.is(dim) && pos.getY() <= -54) {
                var blockToPlace = PMFluids.PSYCHEDELIC_LAVA_BLOCK.get();
                if(pos.getY() == -54) {
                    var glowingObsidian = Utils.getBlockTag(PMTags.Blocks.GLOWING_OBSIDIAN);
                    blockToPlace = glowingObsidian.getRandomElement(rand).get();
                }

                world.setBlock(pos, blockToPlace.defaultBlockState(), 16);
            }

            // otherwise, turn any block exposed to lava into magma
            else for(var dir : Direction.values()) {
                var relPos = pos.relative(dir);

                if(world.getBlockState(relPos).is(Tags.Blocks.STONE) && PMDimensions.isDimOverworldLike(world))
                    world.setBlock(relPos, Blocks.MAGMA_BLOCK.defaultBlockState(), 32);
            }
        }
    }

    @Override
    protected int maxHeight(WorldGenLevel world) {
        return PMCaveFeatures.dimWorldgenLimit(world);
    }

    // generate striped terracotta on the walls in badlands biomes
    private void genBadlandStripes(WorldGenLevel world, BlockPos pos) {
        var block = Blocks.TERRACOTTA;
        var y = pos.getY();

        if(y % 5 == 0)
            block = Blocks.ORANGE_TERRACOTTA;
        else if(y % 6 == 0)
            block = Blocks.WHITE_TERRACOTTA;
        else if(y % 7 == 0)
            block = Blocks.BROWN_TERRACOTTA;
        else if(y % 8 == 0)
            block = Blocks.YELLOW_TERRACOTTA;
        else if(y % 9 == 0)
            block = Blocks.RED_TERRACOTTA;

        world.setBlock(pos, block.defaultBlockState(), 32);
    }
}