package net.paradisemod.worldgen.features.cave;

import java.util.function.Supplier;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.BiomeTags;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RandomSource;
import net.minecraft.util.valueproviders.ClampedNormalInt;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.placement.BiomeFilter;
import net.minecraft.world.level.levelgen.placement.InSquarePlacement;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.placement.RandomOffsetPlacement;
import net.minecraftforge.common.Tags;
import net.paradisemod.world.dimension.PMDimensions;
import net.paradisemod.worldgen.features.BasicFeature;
import net.paradisemod.worldgen.features.PMFeatures;
import net.paradisemod.worldgen.features.placement.InfiniteCountPlacement;

public class PMCaveFeatures {
    public static final ResourceKey<PlacedFeature> CAVE_FORMATIONS = createScatteredCaveFeature("formations", CaveFormation::new);
    public static final ResourceKey<PlacedFeature> CAVE_BLOCKS = createScatteredCaveFeature("blocks", CaveBlocks::new);
    public static final ResourceKey<PlacedFeature> CAVE_FOLIAGE = createScatteredCaveFeature("foliage", CaveFoliage::new);
    public static final ResourceKey<PlacedFeature> CAVE_VINE = createScatteredCaveFeature("vine", CaveVine::new);

    public static void init() { }

    public static int dimWorldgenLimit(WorldGenLevel world) {
        var dim = world.getLevel().dimension();
        var worldgenLimit = 62;

        if(dim == Level.NETHER)
            worldgenLimit = 127;

        if(PMDimensions.Type.OVERWORLD_CORE.is(dim) || PMDimensions.Type.DEEP_DARK.is(dim))
            worldgenLimit = 319;

        return worldgenLimit;
    }

    protected static boolean shouldBeMossy(RandomSource rand, Holder<Biome> biome) {
        return (biome.is(Tags.Biomes.IS_SWAMP) || biome.is(BiomeTags.IS_JUNGLE)) && rand.nextInt(5) == 0;
    }

    protected static boolean isValidBlock(BlockState state) {
        return state.is(Tags.Blocks.STONE) ||
            state.is(Blocks.NETHERRACK) ||
            state.is(Blocks.BASALT) ||
            state.is(Tags.Blocks.COBBLESTONE) ||
            state.is(Tags.Blocks.SANDSTONE) ||
            state.is(BlockTags.TERRACOTTA);
    }

    protected static boolean seesSky(WorldGenLevel world, BlockPos pos) {
        for(int y = pos.getY() + 1; y < 320; y++) {
            var newPos = pos.atY(y);
            if(!world.getBlockState(newPos).isAir())
                return false;
        }

        return true;
    }

    private static ResourceKey<PlacedFeature> createScatteredCaveFeature(String name, Supplier<? extends BasicFeature> feature) {
        return PMFeatures.getBasicFeature(
            "cave/" + name,
            feature,
            InfiniteCountPlacement.of(2048),
            InSquarePlacement.spread(),
            PMFeatures.FULL_HEIGHT_RANGE,
            InfiniteCountPlacement.of(UniformInt.of(1, 5)),
            RandomOffsetPlacement.of(
                ClampedNormalInt.of(0, 3, -10, 10),
                ClampedNormalInt.of(0, 0.6F, -2, 2)
            ),
            BiomeFilter.biome()
        );
    }
}