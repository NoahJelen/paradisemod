package net.paradisemod.worldgen.features.cave;

import java.util.ArrayList;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.data.worldgen.placement.TreePlacements;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.BiomeTags;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraftforge.common.Tags;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.biome.PMBiomes;
import net.paradisemod.world.dimension.PMDimensions;
import net.paradisemod.worldgen.features.BasicFeature;
import net.paradisemod.worldgen.features.foliage.PMFoliage;

public class CaveFoliage extends BasicFeature {
    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        var curState = world.getBlockState(pos);
        var stateBelow = world.getBlockState(pos.below());
        var biome = world.getBiome(pos);
        var dim = world.getLevel().dimension();
        var worldgenLimit = PMCaveFeatures.dimWorldgenLimit(world);

        if(PMDimensions.Type.ELYSIUM.is(dim))
            worldgenLimit = 319;

        if(
            pos.getY() <= worldgenLimit &&
            curState.isAir() &&
            (PMCaveFeatures.isValidBlock(stateBelow) || stateBelow.is(BlockTags.DIRT) || stateBelow.is(BlockTags.SAND))
        ) {
            ArrayList<ResourceKey<PlacedFeature>> trees = new ArrayList<>();
            ArrayList<Block> plants = new ArrayList<>();
            Block soil = null;

            if(PMBiomes.isRockyDesert(biome)) {
                soil = Blocks.COARSE_DIRT;

                plants.add(Blocks.DEAD_BUSH);
                plants.add(Blocks.GRASS);
                plants.add(Blocks.TALL_GRASS);

                if(!PMDimensions.Type.OVERWORLD_CORE.is(dim)) {
                    trees.add(PMFoliage.MESQUITE_PLACER);
                    if(biome.is(PMTags.Biomes.COLD_ROCKY_DESERTS))
                        trees.add(TreePlacements.SPRUCE_CHECKED);
                    else trees.add(PMFoliage.PALO_VERDE_PLACER);
                }

                if(rand.nextInt(5) == 0) {
                    world.setBlock(pos.below(), Blocks.SAND.defaultBlockState(), 32);
                    return placeCactus(world, pos, rand, false);
                }
            }
            else if(biome.is(Tags.Biomes.IS_MUSHROOM)) {
                soil = Blocks.MYCELIUM;

                plants.add(Blocks.RED_MUSHROOM);
                plants.add(Blocks.BROWN_MUSHROOM);
                plants.add(DeepDarkBlocks.GLOWSHROOM.get());

                if(!PMDimensions.Type.OVERWORLD_CORE.is(dim)) {
                    trees.add(PMFoliage.BROWN_MUSHROOM);
                    trees.add(PMFoliage.RED_MUSHROOM);
                    trees.add(PMFoliage.HUGE_GLOWSHROOM_PLACER);
                }
            }
            else if(biome.is(PMBiomes.DARK_DESERT)) {
                plants.add(Blocks.DEAD_BUSH);
                soil = DeepDarkBlocks.BLACKENED_SAND.get();

                if(rand.nextInt(5) == 0) {
                    world.setBlock(pos.below(), soil.defaultBlockState(), 16);
                    return placeCactus(world, pos, rand, true);
                }
            }
            else if(biome.is(BiomeTags.IS_BADLANDS) || PMBiomes.isSandyDesert(biome)) {
                soil = Blocks.RED_SAND;

                if(PMBiomes.isSandyDesert(biome))
                    soil = Blocks.SAND;

                plants.add(Blocks.DEAD_BUSH);

                if(rand.nextInt(5) == 0) {
                    world.setBlock(pos.below(), soil.defaultBlockState(), 16);
                    return placeCactus(world, pos, rand, false);
                }
            }
            else if(biome.is(BiomeTags.IS_JUNGLE) || biome.is(Tags.Biomes.IS_SWAMP)) {
                soil = Blocks.GRASS_BLOCK;

                if(biome.is(BiomeTags.IS_JUNGLE)) {
                    if(biome.is(PMBiomes.TEMPERATE_RAINFOREST) || biome.is(PMBiomes.UNDERGROUND_TEMPERATE_RAINFOREST)) {
                        trees.add(PMFoliage.TR_OAK);
                        trees.add(PMFoliage.TR_BIRCH);
                        trees.add(TreePlacements.JUNGLE_TREE_CHECKED);
                    }
                    else {
                        trees.add(TreePlacements.JUNGLE_TREE_CHECKED);
                        trees.add(TreePlacements.JUNGLE_BUSH);
                        trees.add(PMFoliage.TR_OAK);
                    }

                    plants.add(Blocks.MELON);
                    var flowers = Utils.getBlockTag(BlockTags.FLOWERS);
                    plants.add(flowers.getRandomElement(rand).get());
                }
                else if(biome.is(PMBiomes.UNDERGROUND_MANGROVE_SWAMP) || biome.is(Biomes.MANGROVE_SWAMP)) {
                    soil = Blocks.MUD;
                    plants.add(Blocks.MOSS_CARPET);
                    plants.add(Blocks.DEAD_BUSH);
                    trees.add(TreePlacements.MANGROVE_CHECKED);
                }
                else trees.add(PMFoliage.TR_OAK);

                plants.add(Blocks.GRASS);
                plants.add(Blocks.TALL_GRASS);
                plants.add(Blocks.FERN);
            }
            else if(PMDimensions.Type.ELYSIUM.is(dim) && stateBelow.is(BlockTags.DIRT)) {
                plants.add(Blocks.GRASS);
                plants.add(Blocks.TALL_GRASS);
            }

            // place snow in random spots if this is a cold biome
            if(
                (biome.is(Tags.Biomes.IS_COLD) || biome.is(Tags.Biomes.IS_SNOWY)) &&
                (PMDimensions.isDimOverworldLike(world) || PMDimensions.Type.DEEP_DARK.is(dim)) &&
                rand.nextBoolean()
            ) {
                world.setBlock(pos, Blocks.SNOW.defaultBlockState(), 16);
                return true;
            }

            // otherwise, place the specified plant
            // place a tree
            else if(!trees.isEmpty() && rand.nextInt(5) == 0 && soil != null) {
                world.setBlock(pos.below(), soil.defaultBlockState(), 16);
                return getPlacedFeature(trees.get(rand.nextInt(trees.size())))
                    .place(world, generator, rand, pos);
            }

            // place a plant
            else if(!plants.isEmpty()) {
                var plant = plants.get(rand.nextInt(plants.size()));

                if(soil != null)
                    world.setBlock(pos.below(), soil.defaultBlockState(), 16);

                if(stateBelow.is(BlockTags.DIRT) || stateBelow.is(BlockTags.SAND) || soil != null)
                    world.setBlock(pos, plant.defaultBlockState(), 16);

                if(plant.defaultBlockState().hasProperty(BlockStateProperties.DOUBLE_BLOCK_HALF))
                    world.setBlock(pos.above(), plant.defaultBlockState().setValue(BlockStateProperties.DOUBLE_BLOCK_HALF, DoubleBlockHalf.UPPER), 32);

                return true;
            }
        }
        else if(biome.is(Tags.Biomes.IS_SWAMP) && pos.getY() <= worldgenLimit && curState.isAir() && world.isWaterAt(pos.below()))
            world.setBlock(pos, Blocks.LILY_PAD.defaultBlockState(), 16);

        return false;
    }

    private static boolean placeCactus(WorldGenLevel world, BlockPos pos, RandomSource rand, boolean glowing) {
        int height = rand.nextInt(3);

        for (int i = 0; i <= height; i++) {
            var newPos = pos.above(i);

            for(var dir : Direction.Plane.HORIZONTAL) {
                var adjPos = newPos.relative(dir);
                var adjState = world.getBlockState(adjPos);
                if(!adjState.isAir() && !adjState.is(Blocks.SNOW)) return false;
            }

            world.setBlock(newPos, (glowing ? DeepDarkBlocks.GLOWING_CACTUS.get() : Blocks.CACTUS).defaultBlockState(), 16);
        }

        return true;
    }
}