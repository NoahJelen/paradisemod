package net.paradisemod.worldgen.features.cave;

import java.util.ArrayList;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.tags.BiomeTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.BeehiveBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BeehiveBlockEntity;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraftforge.common.Tags;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.misc.Misc;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.Ores;
import net.paradisemod.world.PMWorld;
import net.paradisemod.world.biome.PMBiomes;
import net.paradisemod.world.dimension.PMDimensions;
import net.paradisemod.worldgen.features.BasicFeature;

public class CaveBlocks extends BasicFeature {
    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        var curState = world.getBlockState(pos);
        var biome = world.getBiome(pos);
        var dim = world.getLevel().dimension();
        var worldgenLimit = PMCaveFeatures.dimWorldgenLimit(world);

        if(
            (pos.getY() <= worldgenLimit || !PMCaveFeatures.seesSky(world, pos)) &&
            PMWorld.isBlockExposed(world, pos) &&
            PMCaveFeatures.isValidBlock(curState)
        ) {
            Block blockToPlace = null;

            if(curState.is(Blocks.DEEPSLATE))
                blockToPlace = Blocks.COBBLED_DEEPSLATE;
            else if(curState.is(Blocks.STONE))
                blockToPlace = PMCaveFeatures.shouldBeMossy(rand, world.getBiome(pos)) ? Blocks.MOSSY_COBBLESTONE : Blocks.COBBLESTONE;

            if(PMBiomes.isSandyDesert(biome) && PMDimensions.isDimOverworldLike(world) && rand.nextInt(5) == 0)
                blockToPlace = Blocks.SANDSTONE;

            if(biome.is(PMBiomes.DARK_DESERT))
                blockToPlace = DeepDarkBlocks.BLACKENED_SANDSTONE.get();

            if(biome.is(BiomeTags.IS_JUNGLE) || biome.is(Tags.Biomes.IS_SWAMP)) {
                if(biome.is(Tags.Biomes.IS_SWAMP) && rand.nextInt(5) == 0)
                    blockToPlace = Blocks.SLIME_BLOCK;

                if(rand.nextBoolean())
                    blockToPlace = Blocks.MOSSY_COBBLESTONE;

                if(rand.nextInt(5) == 0)
                    blockToPlace = Blocks.GLOWSTONE;
            }

            if(biome.is(BiomeTags.IS_BADLANDS) && rand.nextBoolean())
                blockToPlace = Blocks.RED_SANDSTONE;

            if(biome.is(PMTags.Biomes.VOLCANIC) && rand.nextInt(5) == 0)
                blockToPlace = Blocks.MAGMA_BLOCK;

            if(biome.is(BiomeTags.IS_OCEAN)) {
                if(rand.nextBoolean())
                    blockToPlace = Blocks.PRISMARINE;

                if(rand.nextInt(5) == 0)
                    blockToPlace = rand.nextBoolean() ? Misc.PRISMARINE_CRYSTAL_BLOCK.get() : Blocks.SEA_LANTERN;
            }

            if(biome.is(PMTags.Biomes.SALT)) {
                if(rand.nextBoolean())
                    blockToPlace = Ores.COMPACT_SALT_BLOCK.get();

                if(rand.nextBoolean()) {
                    if(curState.is(Blocks.STONE))
                        blockToPlace = Ores.SALT_ORE.get();
                    else if(curState.is(Blocks.DEEPSLATE))
                        blockToPlace = Ores.DEEPSLATE_SALT_ORE.get();
                }

                if(rand.nextInt(5) == 0)
                    blockToPlace = Misc.SALT_LAMP.get();
            }

            if(biome.is(PMBiomes.HONEY_CAVE)) {
                if(rand.nextBoolean())
                        blockToPlace = Blocks.HONEYCOMB_BLOCK;

                if(rand.nextInt(5) == 0)
                        blockToPlace = Misc.HONEY_CRYSTAL_BLOCK.get();

                // place a bee hive
                if(
                    rand.nextInt(25) == 0 &&
                    supportsBeehive(world, pos) &&
                    PMWorld.isBlockInWall(world, pos)
                ) {
                    world.setBlock(
                        pos,
                        Blocks.BEE_NEST.defaultBlockState()
                            .setValue(BeehiveBlock.FACING, exposedBeehiveDirection(world, pos))
                            .setValue(BeehiveBlock.HONEY_LEVEL, rand.nextInt(6)),
                        32
                    );

                    var hive = (BeehiveBlockEntity) world.getBlockEntity(pos);
                    for(int i = 0; i < rand.nextInt(4); i++)
                        hive.addOccupant(EntityType.BEE.create(world.getLevel()), rand.nextBoolean());

                    return true;
                }
            }

            if(
                (biome.is(Tags.Biomes.IS_COLD) || biome.is(Tags.Biomes.IS_SNOWY)) &&
                (PMDimensions.isDimOverworldLike(world) || PMDimensions.Type.DEEP_DARK.is(dim))
            ) {
                if(rand.nextInt(5) == 0)
                    blockToPlace = Blocks.BLUE_ICE;
                else if(rand.nextBoolean())
                    blockToPlace = Blocks.PACKED_ICE;
                if(PMDimensions.Type.OVERWORLD_CORE.is(dim) && rand.nextInt(5) == 0)
                    blockToPlace = Misc.GLOWING_ICE.get();
            }

            if(
                PMDimensions.Type.OVERWORLD_CORE.is(dim) &&
                rand.nextInt(5) == 0
            )
                blockToPlace = Blocks.GLOWSTONE;

            if(blockToPlace != null) {
                world.setBlock(pos, blockToPlace.defaultBlockState(), 32);
                return true;
            }
        }

        return false;
    }

    private static boolean supportsBeehive(LevelAccessor world, BlockPos pos) {
        for(var direction : Direction.Plane.HORIZONTAL) {
            var curState = world.getBlockState(pos.relative(direction));
            if(curState.isAir()) return true;
        }

        return false;
    }

    private static Direction exposedBeehiveDirection(LevelAccessor world, BlockPos pos) {
        var rand = world.getRandom();
        ArrayList<Direction> exposedDirs = new ArrayList<>();
        for(var direction : Direction.Plane.HORIZONTAL) {
            var curState = world.getBlockState(pos.relative(direction));
            if(curState.isAir()) exposedDirs.add(direction);
        }

        return exposedDirs.get(rand.nextInt(exposedDirs.size()));
    }
}
