package net.paradisemod.worldgen.features;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.function.Supplier;

import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.HolderGetter;
import net.minecraft.core.HolderSet;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.data.worldgen.placement.VegetationPlacements;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.random.SimpleWeightedRandomList;
import net.minecraft.util.valueproviders.ClampedNormalInt;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.GeodeBlockSettings;
import net.minecraft.world.level.levelgen.GeodeCrackSettings;
import net.minecraft.world.level.levelgen.GeodeLayerSettings;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.blockpredicates.BlockPredicate;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.LakeFeature;
import net.minecraft.world.level.levelgen.feature.WeightedPlacedFeature;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.GeodeConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.OreConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.RandomFeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.SimpleBlockConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.SpringConfiguration;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.feature.stateproviders.WeightedStateProvider;
import net.minecraft.world.level.levelgen.placement.*;
import net.minecraft.world.level.levelgen.structure.templatesystem.BlockMatchTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.RuleTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.TagMatchTest;
import net.minecraft.world.level.material.Fluids;
import net.minecraftforge.common.Tags;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.building.Building;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.misc.Misc;
import net.paradisemod.world.CrystalClusters;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.Ores;
import net.paradisemod.world.PMWorld.WorldgenFactory;
import net.paradisemod.world.biome.PMBiomes;
import net.paradisemod.world.blocks.CrystalCluster;
import net.paradisemod.world.fluid.PMFluids;
import net.paradisemod.worldgen.OreType;
import net.paradisemod.worldgen.features.cave.BaseCaveGen;
import net.paradisemod.worldgen.features.cave.PMCaveFeatures;
import net.paradisemod.worldgen.features.chunk.ChunkProcessingFeature;
import net.paradisemod.worldgen.features.chunk.ChunkProcessor;
import net.paradisemod.worldgen.features.foliage.FoliagePatch;
import net.paradisemod.worldgen.features.foliage.PMFoliage;
import net.paradisemod.worldgen.features.placement.ConfiguredOrePlacement;
import net.paradisemod.worldgen.features.placement.InfiniteCountPlacement;

public class PMFeatures {
    // feature registry
    private static final DeferredRegister<Feature<?>> FEATURES = PMRegistries.createRegistry(ForgeRegistries.FEATURES);
    public static final HashMap<ResourceKey<ConfiguredFeature<?, ?>>, ConfiguredFeatureFactory> CFG_FEATURES = new HashMap<>();
    public static final HashMap<ResourceKey<PlacedFeature>, PlacedFeatureFactory> PLACED_FEATURES = new HashMap<>();

    // placement modifier registry
    private static final DeferredRegister<PlacementModifierType<?>> PLACEMENT_MODIFIERS = PMRegistries.createRegistry(Registries.PLACEMENT_MODIFIER_TYPE);

    // custom placement modifiers
    public static final RegistryObject<PlacementModifierType<InfiniteCountPlacement>> INFINITE_COUNT = createPlacementModifierType("infinite_count", InfiniteCountPlacement.CODEC);
    public static final RegistryObject<PlacementModifierType<ConfiguredOrePlacement>> CONFIGURED_ORE = createPlacementModifierType("configured_ore", ConfiguredOrePlacement.CODEC);
    public static final PlacementModifier FULL_HEIGHT_RANGE = HeightRangePlacement.uniform(VerticalAnchor.absolute(-64), VerticalAnchor.absolute(320));

    // a feature that generates nothing
    public static final ResourceKey<PlacedFeature> EMPTY = getBasicFeature("empty", EmptyFeature::new);

    public static final ResourceKey<PlacedFeature> RAW_CHUNK_PROCESSOR = getBasicFeature("raw_chunk_processor", () -> new ChunkProcessingFeature(false));
    public static final ResourceKey<PlacedFeature> VEGETAL_CHUNK_PROCESSOR = getBasicFeature("vegetal_chunk_processor", () -> new ChunkProcessingFeature(true));

    // crystal features
    public static final ResourceKey<PlacedFeature> SALT_CRYSTALS = getCrystalFeature(CrystalGen.Type.SALT, false);
    public static final ResourceKey<PlacedFeature> EXTRA_SALT_CRYSTALS = getCrystalFeature(CrystalGen.Type.SALT, true);
    public static final ResourceKey<PlacedFeature> HONEY_CRYSTALS = getCrystalFeature(CrystalGen.Type.HONEY, true);
    public static final ResourceKey<PlacedFeature> QUARTZ_CRYSTALS = getCrystalFeature(CrystalGen.Type.QUARTZ, false);
    public static final ResourceKey<PlacedFeature> MIXED_CRYSTALS = getCrystalFeature(CrystalGen.Type.MIXED, false);
    public static final ResourceKey<PlacedFeature> RANDOM_CRYSTALS = getCrystalFeature(CrystalGen.Type.RANDOM, false);
    public static final ResourceKey<PlacedFeature> MIXED_END_CRYSTALS = getCrystalFeature(CrystalGen.Type.END_MIXED, false);
    public static final ResourceKey<PlacedFeature> RANDOM_END_CRYSTALS = getCrystalFeature(CrystalGen.Type.END_RANDOM, false);

    public static final ResourceKey<PlacedFeature> FALLEN_TREE = getBasicFeature("fallen_tree", FallenTree::new, PlacementUtils.HEIGHTMAP_WORLD_SURFACE);

    // geodes
    public static final ResourceKey<PlacedFeature> DIAMOND_GEODE = createGeode(
        "diamond",
        192,
        CrystalClusters.DIAMOND_CRYSTAL_CLUSTER,
        () -> Blocks.DIAMOND_BLOCK,
        () -> Blocks.CALCITE,
        () -> Blocks.SMOOTH_BASALT
    );

    public static final ResourceKey<PlacedFeature> EMERALD_GEODE = createGeode(
        "emerald",
        96,
        CrystalClusters.EMERALD_CRYSTAL_CLUSTER,
        () -> Blocks.EMERALD_BLOCK,
        () -> Blocks.CALCITE,
        () -> Blocks.SMOOTH_BASALT
    );

    public static final ResourceKey<PlacedFeature> LAPIS_GEODE = createGeode(
        "lapis_lazuli",
        48,
        CrystalClusters.LAPIS_CRYSTAL_CLUSTER,
        () -> Blocks.LAPIS_BLOCK,
        () -> Blocks.CALCITE,
        () -> Blocks.SMOOTH_BASALT
    );

    public static final ResourceKey<PlacedFeature> QUARTZ_GEODE = createGeode(
        "quartz",
        48,
        CrystalClusters.QUARTZ_CRYSTAL_CLUSTER,
        () -> Blocks.QUARTZ_BLOCK,
        () -> Blocks.CALCITE,
        () -> Blocks.SMOOTH_BASALT
    );

    public static final ResourceKey<PlacedFeature> QUARTZ_GEODE_NETHER = createGeode(
        "quartz_nether",
        48,
        CrystalClusters.QUARTZ_CRYSTAL_CLUSTER,
        () -> Blocks.QUARTZ_BLOCK,
        () -> Blocks.SMOOTH_BASALT,
        () -> Blocks.BLACKSTONE
    );

    public static final ResourceKey<PlacedFeature> REDSTONE_GEODE = createGeode(
        "redstone",
        96,
        CrystalClusters.REDSTONE_CRYSTAL_CLUSTER,
        () -> Blocks.REDSTONE_BLOCK,
        () -> Blocks.CALCITE,
        () -> Blocks.SMOOTH_BASALT
    );

    public static final ResourceKey<PlacedFeature> RUBY_GEODE = createGeode(
        "ruby",
        192,
        CrystalClusters.RUBY_CRYSTAL_CLUSTER,
        Ores.RUBY_BLOCK,
        () -> Blocks.CALCITE,
        () -> Blocks.SMOOTH_BASALT
    );

    public static final ResourceKey<PlacedFeature> SALT_GEODE = createGeode(
        "salt",
        48,
        CrystalClusters.SALT_CRYSTAL_CLUSTER,
        Ores.COMPACT_SALT_BLOCK,
        () -> Blocks.CALCITE,
        () -> Blocks.SMOOTH_BASALT
    );

    public static final ResourceKey<PlacedFeature> ENDERITE_GEODE = createGeode(
        "enderite",
        96,
        CrystalClusters.ENDER_PEARL_CLUSTER,
        Ores.ENDERITE_BLOCK,
        Building.POLISHED_END_STONE,
        Building.POLISHED_END_STONE
    );

    // lakes
    public static final ResourceKey<PlacedFeature> WATER_LAKE = getLakeFeature("water", () -> Blocks.WATER, 4, false);
    public static final ResourceKey<PlacedFeature> WATER_LAKE_UNDERGROUND = getLakeFeature("water", () -> Blocks.WATER, 4, true);
    public static final ResourceKey<PlacedFeature> LIQUID_REDSTONE_LAKE = getLakeFeature("liquid_redstone", PMFluids.LIQUID_REDSTONE_BLOCK, 40, true);
    public static final ResourceKey<PlacedFeature> PSYCHEDELIC_LAVA_LAKE = getLakeFeature("psychedelic_lava", PMFluids.PSYCHEDELIC_LAVA_BLOCK, 40, true);
    public static final ResourceKey<PlacedFeature> MOLTEN_SALT_LAKE = getLakeFeature("molten_salt", PMFluids.MOLTEN_SALT_BLOCK, 4, false);
    public static final ResourceKey<PlacedFeature> MOLTEN_SALT_LAKE_UNDERGROUND = getLakeFeature("molten_salt", PMFluids.MOLTEN_SALT_BLOCK, 4, true);
    public static final ResourceKey<PlacedFeature> HONEY_LAKE = getLakeFeature("honey_lake", PMFluids.HONEY_BLOCK, () -> Blocks.HONEY_BLOCK, 4, true);
    public static final ResourceKey<PlacedFeature> ENDER_ACID_LAKE = getLakeFeature("ender_acid", PMFluids.ENDER_ACID_BLOCK, () -> Blocks.AIR, 8, false);
    public static final ResourceKey<PlacedFeature> ENDER_ACID_LAKE_UNDERGROUND = getLakeFeature("ender_acid", PMFluids.ENDER_ACID_BLOCK, 8, true);
    public static final ResourceKey<PlacedFeature> GLOWING_WATER_LAKE = getLakeFeature("glowing_water", PMFluids.GLOWING_WATER_BLOCK, 4, false);
    public static final ResourceKey<PlacedFeature> GLOWING_WATER_LAKE_UNDERGROUND = getLakeFeature("glowing_water", PMFluids.GLOWING_WATER_BLOCK, 4, true);
    public static final ResourceKey<PlacedFeature> PSYCHEDELIC_LAKE = getLakeFeature("psychedelic", PMFluids.PSYCHEDELIC_FLUID_BLOCK, 4, false);
    public static final ResourceKey<PlacedFeature> PSYCHEDELIC_LAKE_UNDERGROUND = getLakeFeature("psychedelic", PMFluids.PSYCHEDELIC_FLUID_BLOCK, 4, true);
    public static final ResourceKey<PlacedFeature> DEEP_DARK_LAVA_LAKE = getLakeFeature("deep_dark_lava", () -> Blocks.LAVA, DeepDarkBlocks.DARKSTONE, 10, false);
    public static final ResourceKey<PlacedFeature> DEEP_DARK_LAVA_LAKE_UNDERGROUND = getLakeFeature("deep_dark_lava", () -> Blocks.LAVA, DeepDarkBlocks.DARKSTONE, 10, true);
    public static final ResourceKey<PlacedFeature> VOLCANIC_LAVA_LAKE = getLakeFeature("volcanic_lava", () -> Blocks.LAVA, 2, false);
    public static final ResourceKey<PlacedFeature> VOLCANIC_LAVA_LAKE_UNDERGROUND = getLakeFeature("volcanic_lava", () -> Blocks.LAVA, 2, true);
    public static final ResourceKey<PlacedFeature> DARK_LAVA_LAKE = getLakeFeature("dark_lava", PMFluids.DARK_LAVA_BLOCK, DeepDarkBlocks.DARK_MAGMA_BLOCK, 4,  false);
    public static final ResourceKey<PlacedFeature> DARK_LAVA_LAKE_UNDERGROUND = getLakeFeature("dark_lava", PMFluids.DARK_LAVA_BLOCK, DeepDarkBlocks.DARK_MAGMA_BLOCK, 4,  true);
    public static final ResourceKey<PlacedFeature> VOLCANIC_DARK_LAVA_LAKE = getLakeFeature("volcanic_dark_lava", PMFluids.DARK_LAVA_BLOCK, DeepDarkBlocks.DARK_MAGMA_BLOCK, 2,  false);
    public static final ResourceKey<PlacedFeature> VOLCANIC_DARK_LAVA_LAKE_UNDERGROUND = getLakeFeature("volcanic_dark_lava", PMFluids.DARK_LAVA_BLOCK, DeepDarkBlocks.DARK_MAGMA_BLOCK, 2,  true);
    public static final ResourceKey<PlacedFeature> TAR_PIT = getLakeFeature("tar_pit", PMFluids.TAR_BLOCK, 80, false);
    public static final ResourceKey<PlacedFeature> MOLTEN_GOLD_LAKE = getLakeFeature("molten_gold", PMFluids.MOLTEN_GOLD_BLOCK, 4, false);
    public static final ResourceKey<PlacedFeature> MOLTEN_GOLD_LAKE_UNDERGROUND = getLakeFeature("molten_gold", PMFluids.MOLTEN_GOLD_BLOCK, 4, true);
    public static final ResourceKey<PlacedFeature> MOLTEN_SILVER_LAKE = getLakeFeature("molten_silver", PMFluids.MOLTEN_SILVER_BLOCK, 4, false);
    public static final ResourceKey<PlacedFeature> MOLTEN_SILVER_LAKE_UNDERGROUND = getLakeFeature("molten_silver", PMFluids.MOLTEN_SILVER_BLOCK, 4, true);
    public static final ResourceKey<PlacedFeature> MOLTEN_IRON_LAKE = getLakeFeature("molten_iron", PMFluids.MOLTEN_IRON_BLOCK, 4, false);
    public static final ResourceKey<PlacedFeature> MOLTEN_IRON_LAKE_UNDERGROUND = getLakeFeature("molten_iron", PMFluids.MOLTEN_IRON_BLOCK, 4, true);
    public static final ResourceKey<PlacedFeature> MOLTEN_COPPER_LAKE = getLakeFeature("molten_copper", PMFluids.MOLTEN_COPPER_BLOCK, 4, false);
    public static final ResourceKey<PlacedFeature> MOLTEN_COPPER_LAKE_UNDERGROUND = getLakeFeature("molten_copper", PMFluids.MOLTEN_COPPER_BLOCK, 4, true);

    // fluid spring configs
    private static final Supplier<SpringConfiguration> LIQUID_REDSTONE_SPRING_CONFIG = () -> new SpringConfiguration(PMFluids.LIQUID_REDSTONE.get().defaultFluidState(), true, 4, 1, blockList(Blocks.STONE, Blocks.GRANITE, Blocks.DIORITE, Blocks.ANDESITE, Blocks.TUFF, Blocks.DEEPSLATE));
    private static final Supplier<SpringConfiguration> PSYCHEDELIC_LAVA_SPRING_CONFIG = () -> new SpringConfiguration(PMFluids.PSYCHEDELIC_LAVA.get().defaultFluidState(), true, 4, 1, blockList(Blocks.STONE, Blocks.GRANITE, Blocks.DIORITE, Blocks.ANDESITE, Blocks.TUFF, Blocks.DEEPSLATE));
    private static final Supplier<SpringConfiguration> HONEY_SPRING_CONFIG = () -> new SpringConfiguration(PMFluids.HONEY.get().defaultFluidState(), true, 4, 1, blockList(Blocks.STONE, Blocks.GRANITE, Blocks.DIORITE, Blocks.ANDESITE, Blocks.HONEYCOMB_BLOCK, Blocks.HONEY_BLOCK, Blocks.TUFF, Blocks.DEEPSLATE));
    private static final Supplier<SpringConfiguration> GLOWING_WATER_SPRING_CONFIG = () -> new SpringConfiguration(PMFluids.GLOWING_WATER.get().defaultFluidState(), true, 4, 1, blockList(DeepDarkBlocks.DARKSTONE.get(), Blocks.DEEPSLATE));
    private static final Supplier<SpringConfiguration> DEEP_DARK_LAVA_SPRING_CONFIG = () -> new SpringConfiguration(Fluids.LAVA.defaultFluidState(), true, 4, 1, blockList(DeepDarkBlocks.DARKSTONE.get(), Blocks.DEEPSLATE));
    private static final Supplier<SpringConfiguration> DEEP_DARK_WATER_SPRING_CONFIG = () -> new SpringConfiguration(Fluids.WATER.defaultFluidState(), true, 4, 1, blockList(DeepDarkBlocks.DARKSTONE.get(), Blocks.DEEPSLATE));
    private static final Supplier<SpringConfiguration> DARK_LAVA_SPRING_CONFIG = () -> new SpringConfiguration(PMFluids.DARK_LAVA.get().defaultFluidState(), true, 4, 1, blockList(DeepDarkBlocks.DARKSTONE.get(), Blocks.DEEPSLATE));
    private static final Supplier<SpringConfiguration> TAR_SPRING_CONFIG = () -> new SpringConfiguration(PMFluids.TAR.get().defaultFluidState(), true, 4, 1, blockList(Blocks.STONE, Blocks.GRANITE, Blocks.DIORITE, Blocks.ANDESITE, Blocks.TUFF, Blocks.DEEPSLATE));
    private static final Supplier<SpringConfiguration> ENDER_ACID_SPRING_CONFIG = () -> new SpringConfiguration(PMFluids.ENDER_ACID.get().defaultFluidState(), true, 4, 1, blockList(Blocks.END_STONE));
    private static final Supplier<SpringConfiguration> MOLTEN_SALT_SPRING_CONFIG = () -> new SpringConfiguration(PMFluids.MOLTEN_SALT.get().defaultFluidState(), true, 4, 1, blockList(Blocks.STONE, Blocks.GRANITE, Blocks.DIORITE, Blocks.ANDESITE, Blocks.TUFF, Blocks.DEEPSLATE));
    private static final Supplier<SpringConfiguration> VOLCANIC_SPRING_CONFIG = () -> new SpringConfiguration(Fluids.LAVA.defaultFluidState(), true, 4, 1, blockList(Blocks.STONE, Blocks.GRANITE, Blocks.DIORITE, Blocks.ANDESITE, Blocks.TUFF, Blocks.DEEPSLATE));
    private static final Supplier<SpringConfiguration> MOLTEN_GOLD_SPRING_CONFIG = () -> new SpringConfiguration(PMFluids.MOLTEN_GOLD.get().defaultFluidState(), true, 4, 1, blockList(DeepDarkBlocks.DARKSTONE.get(), Blocks.DEEPSLATE));
    private static final Supplier<SpringConfiguration> MOLTEN_SILVER_SPRING_CONFIG = () -> new SpringConfiguration(PMFluids.MOLTEN_SILVER.get().defaultFluidState(), true, 4, 1, blockList(DeepDarkBlocks.DARKSTONE.get(), Blocks.DEEPSLATE));
    private static final Supplier<SpringConfiguration> MOLTEN_IRON_SPRING_CONFIG = () -> new SpringConfiguration(PMFluids.MOLTEN_IRON.get().defaultFluidState(), true, 4, 1, blockList(DeepDarkBlocks.DARKSTONE.get(), Blocks.DEEPSLATE));
    private static final Supplier<SpringConfiguration> MOLTEN_COPPER_SPRING_CONFIG = () -> new SpringConfiguration(PMFluids.MOLTEN_COPPER.get().defaultFluidState(), true, 4, 1, blockList(DeepDarkBlocks.DARKSTONE.get(), Blocks.DEEPSLATE));
    private static final Supplier<SpringConfiguration> PSYCHEDELIC_SPRING_CONFIG = () -> new SpringConfiguration(PMFluids.PSYCHEDELIC_FLUID.get().defaultFluidState(), true, 4, 1, blockList(DeepDarkBlocks.DARKSTONE.get(), Blocks.DEEPSLATE));

    // fluid springs
    public static final ResourceKey<PlacedFeature> LIQUID_REDSTONE_SPRING = getSpringFeature("liquid_redstone", LIQUID_REDSTONE_SPRING_CONFIG, false);
    public static final ResourceKey<PlacedFeature> PSYCHEDELIC_LAVA_SPRING = getSpringFeature("psychedelic_lava", PSYCHEDELIC_LAVA_SPRING_CONFIG, false);
    public static final ResourceKey<PlacedFeature> TAR_SPRING = getSpringFeature("tar", TAR_SPRING_CONFIG, false);
    public static final ResourceKey<PlacedFeature> HONEY_SPRING = getSpringFeature("honey", HONEY_SPRING_CONFIG, true);
    public static final ResourceKey<PlacedFeature> ENDER_ACID_SPRING = getSpringFeature("ender_acid", ENDER_ACID_SPRING_CONFIG, false);
    public static final ResourceKey<PlacedFeature> SALT_SPRING = getSpringFeature("salt", MOLTEN_SALT_SPRING_CONFIG, true);
    public static final ResourceKey<PlacedFeature> VOLCANIC_SPRING = getSpringFeature("volcanic", VOLCANIC_SPRING_CONFIG, true);
    public static final ResourceKey<PlacedFeature> GLOWING_WATER_SPRING = getSpringFeature("glowing_water", GLOWING_WATER_SPRING_CONFIG, false);
    public static final ResourceKey<PlacedFeature> DEEP_DARK_WATER_SPRING = getSpringFeature("deep_dark_water", DEEP_DARK_WATER_SPRING_CONFIG, false);
    public static final ResourceKey<PlacedFeature> DEEP_DARK_LAVA_SPRING = getSpringFeature("deep_dark_lava", DEEP_DARK_LAVA_SPRING_CONFIG, false);
    public static final ResourceKey<PlacedFeature> DARK_LAVA_SPRING = getSpringFeature("dark_lava", DARK_LAVA_SPRING_CONFIG, false);
    public static final ResourceKey<PlacedFeature> VOLCANIC_DARK_LAVA_SPRING = getSpringFeature("volcanic_dark_lava", DARK_LAVA_SPRING_CONFIG, true);
    public static final ResourceKey<PlacedFeature> GOLD_SPRING = getSpringFeature("gold", MOLTEN_GOLD_SPRING_CONFIG, true);
    public static final ResourceKey<PlacedFeature> SILVER_SPRING = getSpringFeature("silver", MOLTEN_SILVER_SPRING_CONFIG, true);
    public static final ResourceKey<PlacedFeature> IRON_SPRING = getSpringFeature("iron", MOLTEN_IRON_SPRING_CONFIG, true);
    public static final ResourceKey<PlacedFeature> COPPER_SPRING = getSpringFeature("copper", MOLTEN_COPPER_SPRING_CONFIG, true);
    public static final ResourceKey<PlacedFeature> PSYCHEDELIC_SPRING = getSpringFeature("salt", PSYCHEDELIC_SPRING_CONFIG, true);

    // overworld core features
    public static final ResourceKey<PlacedFeature> GLOWSTONE_ORE = getBlobFeature("ore/glowstone", () -> Blocks.GLOWSTONE, () -> new TagMatchTest(Tags.Blocks.STONE));
    public static final ResourceKey<PlacedFeature> SALT_LAMP_ORE = getBlobFeature("ore/salt_lamp", Misc.SALT_LAMP, () -> new TagMatchTest(Tags.Blocks.STONE));
    public static final ResourceKey<PlacedFeature> GLOWING_ICE_ORE = getBlobFeature("ore/glowing_ice_ore", Misc.GLOWING_ICE, () -> new TagMatchTest(Tags.Blocks.STONE));
    public static final ResourceKey<PlacedFeature> HONEY_CRYSTAL_ORE = getBlobFeature("ore/honey_crystal_", Misc.HONEY_CRYSTAL_BLOCK,() -> new TagMatchTest(Tags.Blocks.STONE));
    public static final ResourceKey<PlacedFeature> SEA_LANTERN_ORE = getBlobFeature("ore/sea_lantern", () -> Blocks.SEA_LANTERN, () -> new TagMatchTest(Tags.Blocks.STONE));

    // deep dark features
    public static final ResourceKey<PlacedFeature> INSOMNIUM_FOSSIL = getBasicFeature("insomnium_fossil", InsomniumFossil::new, CountOnEveryLayerPlacement.of(1), RarityFilter.onAverageOnceEvery(2), BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> INSOMNIUM_ROCK = getBasicFeature("insomnium_rock", () -> new Boulder(Blocks.DEEPSLATE, Blocks.COBBLED_DEEPSLATE), CountOnEveryLayerPlacement.of(2), InSquarePlacement.spread(), BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> ICE_CHUNK = getBasicFeature("ice_chunk", () -> new Boulder(Blocks.PACKED_ICE, Blocks.BLUE_ICE), CountOnEveryLayerPlacement.of(2), InSquarePlacement.spread(), BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> DDF_CRYSTALS = getBasicFeature("ddf_crystals", () -> new FoliagePatch(PMTags.Blocks.CRYSTAL_CLUSTERS), CountOnEveryLayerPlacement.of(5), BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> CRYSTAL_FOREST_CRYSTALS = getBasicFeature("crystal_forest_crystals", () -> new FoliagePatch(PMTags.Blocks.CRYSTAL_CLUSTERS), CountOnEveryLayerPlacement.of(5), BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> REGEN_STONE_BLOB = getBlobFeature("regen_stone_blob", DeepDarkBlocks.REGENERATION_STONE, () -> new BlockMatchTest(DeepDarkBlocks.DARKSTONE.get()));
    public static final ResourceKey<PlacedFeature> DEEP_DARK_DEEPSLATE_BLOB = getBlobFeature("deepslate_blob", () -> Blocks.DEEPSLATE, () -> new BlockMatchTest(DeepDarkBlocks.DARKSTONE.get()));

    // misc features
    public static final ResourceKey<PlacedFeature> FULL_CHRISTMAS_TREE = getBasicFeature(
        "full_christmas_tree",
        FullChristmasTree::new,
        InfiniteCountPlacement.of(512),
        InSquarePlacement.spread(),
        FULL_HEIGHT_RANGE,
        InfiniteCountPlacement.of(UniformInt.of(1, 5)),
        RandomOffsetPlacement.of(
            ClampedNormalInt.of(0, 3, -10, 10),
            ClampedNormalInt.of(0, 0.6F, -2, 2)
        ),
        BiomeFilter.biome()
    );

    public static final ResourceKey<PlacedFeature> ICE_SPIRE = getBasicFeature("ice_spire", IceSpire::new, InSquarePlacement.spread(), CountPlacement.of(40), PlacementUtils.HEIGHTMAP, BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> ICE_SPIRE_OWC = getBasicFeature("ice_spire_owc", IceSpire::new, InSquarePlacement.spread(), CountOnEveryLayerPlacement.of(20), BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> FISSURE = getBasicFeature("fissure", () -> new LavaFissure(false), InSquarePlacement.spread(), CountOnEveryLayerPlacement.of(10), BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> DARK_FISSURE = getBasicFeature("dark_fissure", () -> new LavaFissure(true), InSquarePlacement.spread(), CountOnEveryLayerPlacement.of(10), BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> VOLCANIC_OBSIDIAN = getBlobFeature("volcanic_obsidian", () -> Blocks.OBSIDIAN, () -> new TagMatchTest(Tags.Blocks.STONE));
    public static final ResourceKey<PlacedFeature> VOLCANIC_OBSIDIAN_DEEPSLATE = getBlobFeature("volcanic_obsidian_ds", () -> Blocks.OBSIDIAN, () -> new BlockMatchTest(Blocks.DEEPSLATE));
    public static final ResourceKey<PlacedFeature> VOLCANIC_TUFF = getBlobFeature("volcanic_tuff", () -> Blocks.TUFF, () -> new TagMatchTest(Tags.Blocks.STONE));
    public static final ResourceKey<PlacedFeature> VOLCANIC_TUFF_DEEPSLATE = getBlobFeature("volcanic_tuff_ds", () -> Blocks.TUFF, () -> new BlockMatchTest(Blocks.DEEPSLATE));
    public static final ResourceKey<PlacedFeature> VOLCANIC_MAGMA = getBlobFeature("volcanic_magma", () -> Blocks.MAGMA_BLOCK, () -> new TagMatchTest(Tags.Blocks.STONE));
    public static final ResourceKey<PlacedFeature> VOLCANIC_MAGMA_DEEPSLATE = getBlobFeature("volcanic_magma_ds", () -> Blocks.MAGMA_BLOCK, () -> new BlockMatchTest(Blocks.DEEPSLATE));
    public static final ResourceKey<PlacedFeature> DARK_MAGMA = getBlobFeature("dark_magma", DeepDarkBlocks.DARK_MAGMA_BLOCK, () -> new BlockMatchTest(DeepDarkBlocks.DARKSTONE.get()));
    public static final ResourceKey<PlacedFeature> VOLCANIC_BASALT = getBlobFeature("volcanic_basalt", () -> Blocks.BASALT, () -> new TagMatchTest(Tags.Blocks.STONE));
    public static final ResourceKey<PlacedFeature> VOLCANIC_BASALT_DEEPSLATE = getBlobFeature("volcanic_basalt_ds", () -> Blocks.BASALT, () -> new BlockMatchTest(Blocks.DEEPSLATE));
    public static final ResourceKey<PlacedFeature> DESERT_ROCK = getBasicFeature("desert_rock", () -> new Boulder(Blocks.STONE, Blocks.COBBLESTONE), CountPlacement.of(2), InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> VOLCANIC_BLACK_GLOWING_OBSIDIAN = getBlobFeature("volcanic_black_glowing_obsidian", () -> Building.BLACK_GLOWING_OBSIDIAN.get(), () -> new BlockMatchTest(DeepDarkBlocks.DARKSTONE.get()));
    public static final ResourceKey<PlacedFeature> VOLCANIC_GOLD_GLOWING_OBSIDIAN = getBlobFeature("volcanic_gold_glowing_obsidian", () -> Building.GOLD_GLOWING_OBSIDIAN.get(), () -> new BlockMatchTest(DeepDarkBlocks.DARKSTONE.get()));
    public static final ResourceKey<PlacedFeature> VOLCANIC_SILVER_GLOWING_OBSIDIAN = getBlobFeature("volcanic_silver_glowing_obsidian", () -> Building.SILVER_GLOWING_OBSIDIAN.get(), () -> new BlockMatchTest(DeepDarkBlocks.DARKSTONE.get()));
    public static final ResourceKey<PlacedFeature> VOLCANIC_IRON_GLOWING_OBSIDIAN = getBlobFeature("volcanic_iron_glowing_obsidian", () -> Building.IRON_GLOWING_OBSIDIAN.get(), () -> new BlockMatchTest(DeepDarkBlocks.DARKSTONE.get()));
    public static final ResourceKey<PlacedFeature> VOLCANIC_COPPER_GLOWING_OBSIDIAN = getBlobFeature("volcanic_copper_glowing_obsidian", () -> Building.COPPER_GLOWING_OBSIDIAN.get(), () -> new BlockMatchTest(DeepDarkBlocks.DARKSTONE.get()));
    public static final List<ResourceKey<PlacedFeature>> ORES = createOreFeatures();
    public static final EnumMap<DyeColor, ResourceKey<PlacedFeature>> ROSE_FIELD_PATCHES = createColorFlowerPatches();

    public static void init(IEventBus eventbus) {
        PMFoliage.init();
        PMCaveFeatures.init();
        FEATURES.register(eventbus);
        PLACEMENT_MODIFIERS.register(eventbus);
        ChunkProcessor.addRawProcessor(new BaseCaveGen());
        ChunkProcessor.addRawProcessor(new OWCSurfaceFixerUpper());
        ChunkProcessor.addRawProcessor(new IceSheet(() -> Blocks.SNOW_BLOCK, Misc.GLOWING_ICE, Misc.GLOWING_ICE, PMBiomes.GLOWING_GLACIER));
        ChunkProcessor.addRawProcessor(new IceSheet(() -> Blocks.SNOW_BLOCK, () -> Blocks.PACKED_ICE, () -> Blocks.BLUE_ICE, PMBiomes.POLAR_WINTER, PMBiomes.GLACIER, PMBiomes.SUBGLACIAL_VOLCANIC_FIELD, PMBiomes.UNDERGROUND_GLACIER));
        ChunkProcessor.addVegetalProcessor(new SnowBlanket(PMBiomes.GLOWING_GLACIER, PMBiomes.POLAR_WINTER, PMBiomes.GLACIER, PMBiomes.SUBGLACIAL_VOLCANIC_FIELD, PMBiomes.UNDERGROUND_GLACIER));
    }

    private static <T extends PlacementModifier> RegistryObject<PlacementModifierType<T>> createPlacementModifierType(String name, Codec<T> codec) {
        return PLACEMENT_MODIFIERS.register(name, () -> () -> codec);
    }

    // create and register a geode feature
    private static ResourceKey<PlacedFeature> createGeode(String name, int chance, Supplier<Block> crystal, Supplier<Block> inner, Supplier<Block> middle, Supplier<Block> outer) {
        var geodeName = "geode/" + name;
        var feature = regConfFeature(
            geodeName, Feature.GEODE,
            (cfgFeatureGetter, featureGetter) -> {
                var crystals = List.of(
                    crystal.get().defaultBlockState().setValue(CrystalCluster.TYPE, 0),
                    crystal.get().defaultBlockState().setValue(CrystalCluster.TYPE, 1),
                    crystal.get().defaultBlockState().setValue(CrystalCluster.TYPE, 2),
                    crystal.get().defaultBlockState().setValue(CrystalCluster.TYPE, 3)
                );

                return new GeodeConfiguration(
                    new GeodeBlockSettings(
                        BlockStateProvider.simple(Blocks.AIR),
                        BlockStateProvider.simple(inner.get()),
                        BlockStateProvider.simple(inner.get()),
                        BlockStateProvider.simple(middle.get()),
                        BlockStateProvider.simple(outer.get()),
                        crystals,
                        BlockTags.FEATURES_CANNOT_REPLACE,
                        BlockTags.GEODE_INVALID_BLOCKS
                    ),
                    new GeodeLayerSettings(1.7D, 2.2D, 3.2D, 4.2D),
                    new GeodeCrackSettings(0.95D, 2.0D, 2),
                    0.35D,
                    0.083D,
                    true,
                    UniformInt.of(4, 6),
                    UniformInt.of(3, 4),
                    UniformInt.of(1, 2),
                    -16,
                    16,
                    0.05D,
                    1
                );
            }
        );

        return regPlacedFeature(
            geodeName,
            feature,
            RarityFilter.onAverageOnceEvery(chance),
            InSquarePlacement.spread(),
            HeightRangePlacement.uniform(
                VerticalAnchor.aboveBottom(6),
                VerticalAnchor.absolute(30)
            ),
            BiomeFilter.biome()
        );
    }

    // create and register a fluid spring feature
    private static ResourceKey<PlacedFeature> getSpringFeature(String name, Supplier<SpringConfiguration> config, boolean biomeSpecific) {
        return regPlacedFeature(
            "spring/" + name,
            Feature.SPRING,
            (cfgFeatureGetter, featureGetter) -> config.get(),
            CountPlacement.of(biomeSpecific ? 150 : 25),
            InSquarePlacement.spread(),
            FULL_HEIGHT_RANGE,
            BiomeFilter.biome()
        );
    }

    // create and register a blob feature (similar to vanilla stone deposits)
    private static ResourceKey<PlacedFeature> getBlobFeature(String name, Supplier<Block> block, Supplier<RuleTest> targetBlocks) {
        return regPlacedFeature(name, Feature.ORE,
            (cfgFeatureGetter, featureGetter) -> new OreConfiguration(targetBlocks.get(), block.get().defaultBlockState(), 33),
            FULL_HEIGHT_RANGE,
            CountPlacement.of(256),
            InSquarePlacement.spread(),
            BiomeFilter.biome()
        );
    }

    private static ResourceKey<PlacedFeature> getLakeFeature(String name, Supplier<? extends Block> fluidBlock, int chance, boolean underground) {
        return getLakeFeature(name, fluidBlock, () -> Blocks.AIR, chance, underground);
    }

    // create and register a lake feature
    private static ResourceKey<PlacedFeature> getLakeFeature(String name, Supplier<? extends Block> fluidBlock, Supplier<? extends Block> barrier, int chance, boolean underground) {
        PlacementModifier[] placements;
        if(underground)
            placements = new PlacementModifier[] {
                RarityFilter.onAverageOnceEvery(chance),
                InSquarePlacement.spread(),
                FULL_HEIGHT_RANGE,
                EnvironmentScanPlacement.scanningFor(Direction.DOWN, BlockPredicate.allOf(BlockPredicate.not(BlockPredicate.ONLY_IN_AIR_PREDICATE), BlockPredicate.insideWorld(new BlockPos(0, -5, 0))), 32), SurfaceRelativeThresholdFilter.of(Heightmap.Types.OCEAN_FLOOR_WG, Integer.MIN_VALUE, -5),
                BiomeFilter.biome()
            };
        else
            placements = new PlacementModifier[] {
                RarityFilter.onAverageOnceEvery(chance),
                InSquarePlacement.spread(),
                PlacementUtils.HEIGHTMAP_WORLD_SURFACE,
                BiomeFilter.biome()
            };

        return regPlacedFeature("lake/" + name + (underground ? "_underground" : ""), Feature.LAKE,
            (cfgFeatureGetter, featureGetter) -> new LakeFeature.Configuration(
                BlockStateProvider.simple(fluidBlock.get().defaultBlockState()),
                BlockStateProvider.simple(barrier.get().defaultBlockState())
            ),
            placements
        );
    }

    // create and register a crystal generator
    private static ResourceKey<PlacedFeature> getCrystalFeature(CrystalGen.Type type, boolean genExtra) {
        var height = (type == CrystalGen.Type.END_MIXED || type == CrystalGen.Type.END_RANDOM || type == CrystalGen.Type.QUARTZ) ? 129 : 310;
        var featureName = "crystals/" + (genExtra ? "extra_" : "") + type.name().toLowerCase();
        
        return regPlacedFeature(
            featureName,
            (Supplier<CrystalGen>) () -> new CrystalGen(type),
            (cfgFeatureGetter, featureGetter) -> FeatureConfiguration.NONE,

            genExtra ? new PlacementModifier[] {
                InfiniteCountPlacement.of(128),
                InSquarePlacement.spread(),
                FULL_HEIGHT_RANGE,
                InfiniteCountPlacement.of(UniformInt.of(1, 5)),
                RandomOffsetPlacement.of(
                    ClampedNormalInt.of(0, 3, -10, 10),
                    ClampedNormalInt.of(0, 0.6F, -2, 2)
                ),
                BiomeFilter.biome()
            } :
            new PlacementModifier[] {
                HeightRangePlacement.uniform(
                    VerticalAnchor.bottom(),
                    VerticalAnchor.absolute(height)
                )
            }
        );
    }

    // create and register a feature that extends BasicFeature
    public static ResourceKey<PlacedFeature> getBasicFeature(String name, Supplier<? extends BasicFeature> feature, PlacementModifier... placements) {
        return regPlacedFeature(name, feature, (cfgFeatureGetter, featureGetter) -> FeatureConfiguration.NONE, placements);
    }

    public static ResourceKey<PlacedFeature> getBigFoliagePlacer(String name, boolean sparse, boolean multilayer, List<ResourceKey<PlacedFeature>> commonPlants, List<ResourceKey<PlacedFeature>> rarePlants, List<ResourceKey<ConfiguredFeature<?, ?>>> inlinePlants) {
        var chance = sparse ? 5 : 10;
        ArrayList<PlacementModifier> placements = multilayer ? new ArrayList<>(List.of(CountOnEveryLayerPlacement.of(chance))) : new ArrayList<>(VegetationPlacements.treePlacement(PlacementUtils.countExtra(chance, 0.1F, 1)));
        placements.add(BiomeFilter.biome());

        return regPlacedFeature(name, Feature.RANDOM_SELECTOR,
            (cfgFeatureGetter, featureGetter) -> {
                ArrayList<WeightedPlacedFeature> plants = new ArrayList<>(
                    commonPlants.stream()
                        .map(
                            plant ->
                                new WeightedPlacedFeature(featureGetter.getOrThrow(plant), 1 / (float) commonPlants.size())
                        )
                        .toList()
                );

                plants.addAll(
                    rarePlants.stream()
                        .map(
                            plant ->
                                new WeightedPlacedFeature(featureGetter.getOrThrow(plant), 1 / (float) ((commonPlants.size() + inlinePlants.size()) * 2))
                        )
                        .toList()
                );

                for(var inlinePlant : inlinePlants)
                    plants.add(
                        new WeightedPlacedFeature(PlacementUtils.inlinePlaced(cfgFeatureGetter.getOrThrow(inlinePlant)), 1 / (float) ((commonPlants.size() + inlinePlants.size()) * 2))
                    );
                
                return new RandomFeatureConfiguration(plants, featureGetter.getOrThrow(commonPlants.get(0)));
            },
            placements.toArray(PlacementModifier[]::new)
        );
    }

    @SafeVarargs
    public static ResourceKey<PlacedFeature> getBigFoliagePlacer(String name, boolean sparse, boolean multilayer, List<ResourceKey<PlacedFeature>> plants, ResourceKey<ConfiguredFeature<?, ?>>... inlinePlants) {
        var chance = sparse ? 5 : 10;
        ArrayList<PlacementModifier> placements = multilayer ? new ArrayList<>(List.of(CountOnEveryLayerPlacement.of(chance))) : new ArrayList<>(VegetationPlacements.treePlacement(PlacementUtils.countExtra(chance, 0.1F, 1)));
        placements.add(BiomeFilter.biome());

        return regPlacedFeature(name, Feature.RANDOM_SELECTOR,
            (cfgFeatureGetter, featureGetter) -> {
                ArrayList<WeightedPlacedFeature> plantList = new ArrayList<>(
                    plants.stream()
                        .map(plant ->
                            new WeightedPlacedFeature(featureGetter.getOrThrow(plant), 1 / (float) (plants.size() + inlinePlants.length))
                        )
                        .toList()
                );

                for(var inlinePlant : inlinePlants)
                    plantList.add(
                        new WeightedPlacedFeature(PlacementUtils.inlinePlaced(cfgFeatureGetter.getOrThrow(inlinePlant)), 1 / (float) (plants.size() + inlinePlants.length))
                    );

                ResourceKey<PlacedFeature> plant;

                if(!plants.isEmpty())
                    plant = plants.get(0);
                else plant = EMPTY;

                return new RandomFeatureConfiguration(plantList, featureGetter.getOrThrow(plant));
            },
            placements.toArray(PlacementModifier[]::new)
        );
    }

    public static HolderSet<Block> blockList(Block... blocks) {
        return HolderSet.direct(Block::builtInRegistryHolder, blocks);
    }

    private static EnumMap<DyeColor, ResourceKey<PlacedFeature>> createColorFlowerPatches() {
        var flowerPatches = new EnumMap<DyeColor, ResourceKey<PlacedFeature>>(DyeColor.class);

        for(var color : DyeColor.values()) {
            flowerPatches.put(color,
                regPlacedFeature("rose_patch/" + color.getName(), Feature.RANDOM_PATCH,
                    (cfgFeatureGetter, featureGetter) -> FeatureUtils.simpleRandomPatchConfiguration(192,
                        PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK,
                            new SimpleBlockConfiguration(new WeightedStateProvider(getFlowers(color)))
                        )
                    ),
                    CountPlacement.of(5),
                    InSquarePlacement.spread(),
                    PlacementUtils.HEIGHTMAP_WORLD_SURFACE,
                    BiomeFilter.biome()
                )
            );
        }

        return flowerPatches;
    }

    private static SimpleWeightedRandomList<BlockState> getFlowers(DyeColor color) {
        var flowers = new SimpleWeightedRandomList.Builder<BlockState>();
        switch (color) {
            case BLACK -> flowers
                .add(Blocks.WITHER_ROSE.defaultBlockState(), 10);

            case RED -> flowers
                .add(Blocks.POPPY.defaultBlockState(), 10)
                .add(Blocks.RED_TULIP.defaultBlockState(), 10);

            case BLUE -> flowers
                .add(Blocks.CORNFLOWER.defaultBlockState(), 10);

            case PINK -> flowers
                .add(Blocks.PINK_TULIP.defaultBlockState(), 10);

            case WHITE -> flowers
                .add(Blocks.LILY_OF_THE_VALLEY.defaultBlockState(), 10);

            case ORANGE -> flowers
                .add(Blocks.ORANGE_TULIP.defaultBlockState(), 10);

            case YELLOW -> flowers
                .add(Blocks.DANDELION.defaultBlockState(), 10);

            case MAGENTA -> flowers
                .add(Blocks.ALLIUM.defaultBlockState(), 10);

            case LIGHT_BLUE -> flowers
                .add(Blocks.BLUE_ORCHID.defaultBlockState(), 10);

            case LIGHT_GRAY -> flowers
                .add(Blocks.AZURE_BLUET.defaultBlockState(), 10)
                .add(Blocks.OXEYE_DAISY.defaultBlockState(), 10)
                .add(Blocks.WHITE_TULIP.defaultBlockState(), 10);
        }

        var rose = Decoration.ROSES.get(color).get().defaultBlockState();
        flowers.add(rose, 20);
        return flowers.build();
    }

    // create all the ore features
    private static List<ResourceKey<PlacedFeature>> createOreFeatures() {
        ArrayList<ResourceKey<PlacedFeature>> featureList = new ArrayList<>();
        for (var ore : OreType.values()) {
            featureList.add(
                regPlacedFeature("ore/" + ore.name().toLowerCase(), Feature.ORE,
                    (cfgFeatureGetter, featureGetter) -> new OreConfiguration(
                        ore.getGenRule(),
                        ore.getBlock().defaultBlockState(), ore.getMaxVeinSize()
                    ),
                    InSquarePlacement.spread(),
                    CountPlacement.of(10),
                    HeightRangePlacement.triangle(
                        VerticalAnchor.absolute(ore.getMinHeight()),
                        VerticalAnchor.absolute(ore.getMaxHeight())
                    ),
                    RarityFilter.onAverageOnceEvery(ore.getRarity()),
                    new ConfiguredOrePlacement(ore.configKey())
                )
            );
        }
        return featureList;
    }

    /** Create configured feature from a vanilla base feature */
    public static <FC extends FeatureConfiguration, F extends Feature<FC >> ResourceKey<ConfiguredFeature<?, ?>> regConfFeature(String name, F feature, FeatureConfigSupplier<FC> config) {
        var key = PMRegistries.createModResourceKey(Registries.CONFIGURED_FEATURE, name);
        CFG_FEATURES.put(
            key,
            (cfgFeatureGetter, featureGetter) -> new ConfiguredFeature<>(feature, config.get(cfgFeatureGetter, featureGetter))
        );
        return key;
    }

    /** Create placed feature from vanilla base feature */
    public static <FC extends FeatureConfiguration, F extends Feature<FC>> ResourceKey<PlacedFeature> regPlacedFeature(String name, F feature, FeatureConfigSupplier<FC> config, PlacementModifier... placements) {
        var confFeature = regConfFeature(name, feature, config);
        var key = PMRegistries.createModResourceKey(Registries.PLACED_FEATURE, name);
        PLACED_FEATURES.put(
            key,
            (cfgFeatureGetter, featureGetter) -> new PlacedFeature(cfgFeatureGetter.getOrThrow(confFeature), List.of(placements))
        );
        return key;
    }

    /** Create placed feature from custom base feature */
    public static <FC extends FeatureConfiguration> ResourceKey<PlacedFeature> regPlacedFeature(String name, Supplier<? extends Feature<FC>> feature, FeatureConfigSupplier<FC> config, PlacementModifier... placements) {
        var regFeature = Utils.handlePossibleException(() -> FEATURES.register(name.replace("/", "_"), feature));
        var cfgFeatureKey = PMRegistries.createModResourceKey(Registries.CONFIGURED_FEATURE, name);
        CFG_FEATURES.put(
            cfgFeatureKey,
            (cfgFeatureGetter, featureGetter) -> new ConfiguredFeature<>(regFeature.get(), config.get(cfgFeatureGetter, featureGetter))
        );

        var key = PMRegistries.createModResourceKey(Registries.PLACED_FEATURE, name);
        PLACED_FEATURES.put(
            key,
            (cfgFeatureGetter, featureGetter) -> new PlacedFeature(cfgFeatureGetter.getOrThrow(cfgFeatureKey), List.of(placements))
        );

        return key;
    }

    /** Create placed feature from configured feature */
    public static ResourceKey<PlacedFeature> regPlacedFeature(String name, ResourceKey<ConfiguredFeature<?, ?>> confFeature, PlacementModifier... placements) {
        var key = PMRegistries.createModResourceKey(Registries.PLACED_FEATURE, name);
        PLACED_FEATURES.put(key, (cfgFeatureGetter, featureGetter) -> new PlacedFeature(cfgFeatureGetter.getOrThrow(confFeature), List.of(placements)));
        return key;
    }

    @FunctionalInterface
    public interface ConfiguredFeatureFactory extends WorldgenFactory<ConfiguredFeature<?, ?>> {
        default ConfiguredFeature<?, ?> generate(BootstapContext<ConfiguredFeature<?, ?>> context) {
            var cfgFeatureGetter = context.lookup(Registries.CONFIGURED_FEATURE);
            var featureGetter = context.lookup(Registries.PLACED_FEATURE);
            return create(cfgFeatureGetter, featureGetter);
        }

        ConfiguredFeature<?, ?> create(HolderGetter<ConfiguredFeature<?, ?>> cfgFeatureGetter, HolderGetter<PlacedFeature> featureGetter);
    }

    @FunctionalInterface
    public interface PlacedFeatureFactory extends WorldgenFactory<PlacedFeature> {
        default PlacedFeature generate(BootstapContext<PlacedFeature> context) {
            var cfgFeatureGetter = context.lookup(Registries.CONFIGURED_FEATURE);
            var featureGetter = context.lookup(Registries.PLACED_FEATURE);
            return create(cfgFeatureGetter, featureGetter);
        }

        PlacedFeature create(HolderGetter<ConfiguredFeature<?, ?>> cfgFeatureGetter, HolderGetter<PlacedFeature> featureGetter);
    }

    @FunctionalInterface
    public interface FeatureConfigSupplier<FC extends FeatureConfiguration> {
        FC get(HolderGetter<ConfiguredFeature<?, ?>> cfgFeatureGetter, HolderGetter<PlacedFeature> featureGetter);
    }
}