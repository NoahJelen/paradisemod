package net.paradisemod.worldgen.features;

import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
import net.paradisemod.world.Ores;
import net.paradisemod.worldgen.structures.SmallStructure;

public class InsomniumFossil extends SmallStructure {
    private static final List<ResourceLocation> FOSSILS = List.of(
        new ResourceLocation("fossil/spine_1"),
        new ResourceLocation("fossil/spine_2"),
        new ResourceLocation("fossil/spine_3"),
        new ResourceLocation("fossil/spine_4"),
        new ResourceLocation("fossil/skull_1"),
        new ResourceLocation("fossil/skull_2"),
        new ResourceLocation("fossil/skull_3"),
        new ResourceLocation("fossil/skull_4")
    );

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        var template = world.getLevel().getStructureManager().get(FOSSILS.get(rand.nextInt(FOSSILS.size()))).get();
        var sizeX = template.getSize().getX();
        int sizeZ = template.getSize().getZ();

        var structurePos = pos;
        if(!world.getBlockState(pos.below()).canOcclude())
            structurePos = pos.below();

        if(isFlat(world, structurePos, sizeX, sizeZ, true))
            return genStructureFromTemplate(template, world, rand, structurePos, generator, true);
        else return false;
    }

    @Override
    protected void postProcessStructure(StructureTemplate template, WorldGenLevel world, RandomSource rand, BlockPos pos, StructurePlaceSettings settings, ChunkGenerator generator) {
        Block[] ores = {Ores.DARKSTONE_COAL_ORE.get(), Blocks.DEEPSLATE_DIAMOND_ORE};

        postProcessByBlock(
            world, template, pos,
            (curState, curPos) -> {
                if(curState.is(Blocks.BONE_BLOCK) && rand.nextInt(10) == 0)
                        world.setBlock(curPos, ores[rand.nextInt(ores.length)].defaultBlockState(), 32);
            }
        );
    }
}