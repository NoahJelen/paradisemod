package net.paradisemod.worldgen.features;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.tags.BiomeTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.VineBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraftforge.common.Tags;
import net.paradisemod.world.biome.PMBiomes;

public class FallenTree extends BasicFeature {

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        var dim = world.getLevel().dimension();
        var dimName = dim.location().toString();
        var curPos = pos.below();

        // length of the log
        var length = 4 + rand.nextInt(3);

        // biome of the stump
        var biome = world.getBiome(curPos);

        // should it generate along the x or z axis?
        var isZ = rand.nextBoolean();

        // should the stump be on the other side?
        var otherSide = rand.nextBoolean();

        // distance of the log from the stump
        var distance = 2 + rand.nextInt(2);

        // is the biome a valid biome?
        BlockState log;
        if(biome.is(BiomeTags.IS_FOREST) && dimName.equals("minecraft:overworld")) {
            log = rand.nextBoolean() ? Blocks.OAK_LOG.defaultBlockState() : Blocks.BIRCH_LOG.defaultBlockState();
            if(biome.is(Biomes.BIRCH_FOREST) || biome.is(Biomes.OLD_GROWTH_BIRCH_FOREST) || biome.is(PMBiomes.UNDERGROUND_BIRCH_FOREST)) log = Blocks.BIRCH_LOG.defaultBlockState();
            if(biome.is(Tags.Biomes.IS_CONIFEROUS)) log = Blocks.SPRUCE_LOG.defaultBlockState();
        }
        else if(biome.is(BiomeTags.IS_JUNGLE)) {
            log = Blocks.JUNGLE_LOG.defaultBlockState();
            if(biome.is(PMBiomes.TEMPERATE_RAINFOREST) || biome.is(PMBiomes.UNDERGROUND_TEMPERATE_RAINFOREST)) {
                Block[] logsToPick = {Blocks.OAK_LOG, Blocks.BIRCH_LOG, Blocks.JUNGLE_LOG, Blocks.SPRUCE_LOG};
                log = logsToPick[rand.nextInt(4)].defaultBlockState();
            }
        }
        else return false;

        // is the position valid?
        BlockPos newPos;
        for (var a = 0; a < length + distance; a++){
            if (isZ) newPos = (otherSide) ? curPos.above().north(a) : curPos.above().south(a);
            else newPos = (otherSide) ? curPos.above().west(a) : curPos.above().east(a);
            if (world.getBlockState(newPos).canOcclude() || !world.getBlockState(newPos.below()).canOcclude() || world.getBlockState(newPos.below()).is(Blocks.BRICKS)) return false;
        }

        // place the stump
        world.setBlock(curPos, Blocks.DIRT.defaultBlockState(), 1);
        world.setBlock(curPos.above(), log, 1);

        // place vines around the stump
        if(!world.getBlockState(curPos.west().above()).canOcclude()) world.setBlock(curPos.west().above(), Blocks.VINE.defaultBlockState().setValue(VineBlock.EAST, true), 1);
        if(!world.getBlockState(curPos.east().above()).canOcclude()) world.setBlock(curPos.east().above(), Blocks.VINE.defaultBlockState().setValue(VineBlock.WEST, true), 1);
        if(!world.getBlockState(curPos.north().above()).canOcclude()) world.setBlock(curPos.north().above(), Blocks.VINE.defaultBlockState().setValue(VineBlock.SOUTH, true), 1);
        if(!world.getBlockState(curPos.south().above()).canOcclude()) world.setBlock(curPos.south().above(), Blocks.VINE.defaultBlockState().setValue(VineBlock.NORTH, true), 1);

        // place the log
        BlockPos logPos;
        for(var a = distance; a < length + distance; a++) {
            var axis = isZ ? Direction.Axis.Z : Direction.Axis.X;
            if(isZ) logPos = (otherSide) ? curPos.above().north(a) : curPos.above().south(a);
            else logPos = (otherSide) ? curPos.above().west(a) : curPos.above().east(a);
            world.setBlock(logPos, log.setValue(RotatedPillarBlock.AXIS, axis), 1);
            if(world.getBlockState(logPos.below()).is(Blocks.GRASS_BLOCK)) world.setBlock(logPos.below(), Blocks.DIRT.defaultBlockState(), 1);
        }

        return true;
    }
}