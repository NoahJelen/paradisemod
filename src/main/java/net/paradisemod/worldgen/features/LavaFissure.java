package net.paradisemod.worldgen.features;

import java.util.ArrayList;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraftforge.common.Tags;
import net.paradisemod.world.fluid.PMFluids;

public class LavaFissure extends BasicFeature {
    private boolean isDark;

    public LavaFissure(boolean isDark) {
        this.isDark = isDark;
    }

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        var dimType = world.getLevel().dimensionType();
        var height = dimType.height();
        var chunkX = (pos.getX() >> 4) * 16;
        var chunkZ = (pos.getZ() >> 4) * 16;
        var chunkCorner = new BlockPos(chunkX, 63, chunkZ);
        if(dimType.hasCeiling()) {
            var layers = new ArrayList<Integer>();
            for(int y = 0; y < height; y++)
                if(y % 10 == 0) layers.add(y);

            chunkCorner = new BlockPos(chunkX, layers.get(rand.nextInt(layers.size())), chunkZ);
        }

        var toPlacePos = chunkCorner.offset(rand.nextInt(8), 0, rand.nextInt(8));
        for(int x = 0; x < 8; x++) {
            for(int z = 0; z < 8; z++) {
                for(int y = 0; y < 194; y++) {
                    var newPos = toPlacePos.offset(x, y, z);
                    if(newPos.getY() > height - 10) break;
                    var block = world.getBlockState(newPos);
                    var blockAbove = world.getBlockState(newPos.above());
                    if(block.is(Tags.Blocks.STONE) && (blockAbove.isAir() || blockAbove.is(Blocks.BLUE_ICE)) && rand.nextInt(5) == 0) {
                        for(var dir : Direction.Plane.HORIZONTAL) {
                            var adjPos = newPos.relative(dir);
                            if(world.getBlockState(adjPos).canOcclude()) continue;
                            return false;
                        }

                        world.setBlock(newPos, (isDark ? PMFluids.DARK_LAVA_BLOCK.get() : Blocks.LAVA).defaultBlockState(), 32);
                    }
                }
            }
        }

        return true;
    }
}