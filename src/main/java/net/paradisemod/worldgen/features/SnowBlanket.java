package net.paradisemod.worldgen.features;

import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SnowyDirtBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraftforge.common.Tags;
import net.paradisemod.misc.Misc;
import net.paradisemod.world.fluid.PMFluids;
import net.paradisemod.worldgen.features.chunk.ChunkProcessor;

public class SnowBlanket extends ChunkProcessor {
    private final List<ResourceKey<Biome>> ignoredBiomes;

    @SafeVarargs
    public SnowBlanket(ResourceKey<Biome>... ignoredBiomes) {
        this.ignoredBiomes = List.of(ignoredBiomes);
    }

    @Override
    protected void processBlock(WorldGenLevel world, RandomSource rand, BlockPos pos, BlockState curState, Holder<Biome> biome, ChunkGenerator generator) {
        var stateAbove = world.getBlockState(pos.above());

        if(stateAbove.isAir() && biome.is(Tags.Biomes.IS_SNOWY) && !ignoredBiomes.contains(biome.unwrapKey().get())) {
            // cover trees in snow in snowy overworld core biomes
            if(curState.is(BlockTags.LEAVES))
                world.setBlock(pos.above(), Blocks.SNOW.defaultBlockState(), 32);

            // turn exposed water in snowy overworld core biomes to ice
            else if(world.isWaterAt(pos))
                world.setBlock(pos, Blocks.ICE.defaultBlockState(), 32);

            // if this is a snowy deep dark biome, turn the glowing water into glowing ice
            else if(world.getFluidState(pos).is(PMFluids.GLOWING_WATER.get()))
                world.setBlock(pos, Misc.GLOWING_ICE.get().defaultBlockState(), 32);

            // place a blanket of snow on every dirt-like block
            else if(curState.is(BlockTags.DIRT)) {
                if(curState.hasProperty(SnowyDirtBlock.SNOWY))
                    world.setBlock(pos, curState.setValue(SnowyDirtBlock.SNOWY, true), 32);

                world.setBlock(pos.above(), Blocks.SNOW.defaultBlockState(), 32);
            }
        }
    }
}