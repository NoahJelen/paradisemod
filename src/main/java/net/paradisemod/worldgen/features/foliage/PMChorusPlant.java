package net.paradisemod.worldgen.features.foliage;

import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.ChorusFlowerBlock;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraftforge.common.Tags;
import net.paradisemod.worldgen.features.BasicFeature;

public class PMChorusPlant extends BasicFeature {
    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        if (world.isEmptyBlock(pos) && world.getBlockState(pos.below()).is(Tags.Blocks.END_STONES)) {
            ChorusFlowerBlock.generatePlant(world, pos, rand, 8);
            return true;
        }
        return false;
    }
}