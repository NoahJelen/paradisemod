package net.paradisemod.worldgen.features.foliage;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.OptionalInt;

import com.google.common.collect.ArrayListMultimap;

import net.minecraft.data.worldgen.features.TreeFeatures;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.data.worldgen.placement.TreePlacements;
import net.minecraft.data.worldgen.placement.VegetationPlacements;
import net.minecraft.resources.ResourceKey;
import net.minecraft.util.valueproviders.ConstantInt;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.HugeMushroomBlock;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.WeightedPlacedFeature;
import net.minecraft.world.level.levelgen.feature.configurations.HugeMushroomFeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.RandomFeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration.TreeConfigurationBuilder;
import net.minecraft.world.level.levelgen.feature.featuresize.ThreeLayersFeatureSize;
import net.minecraft.world.level.levelgen.feature.featuresize.TwoLayersFeatureSize;
import net.minecraft.world.level.levelgen.feature.foliageplacers.AcaciaFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.foliageplacers.BlobFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.foliageplacers.DarkOakFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.foliageplacers.FancyFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.foliageplacers.SpruceFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.feature.treedecorators.BeehiveDecorator;
import net.minecraft.world.level.levelgen.feature.treedecorators.LeaveVineDecorator;
import net.minecraft.world.level.levelgen.feature.treedecorators.TreeDecorator;
import net.minecraft.world.level.levelgen.feature.treedecorators.TrunkVineDecorator;
import net.minecraft.world.level.levelgen.feature.trunkplacers.DarkOakTrunkPlacer;
import net.minecraft.world.level.levelgen.feature.trunkplacers.FancyTrunkPlacer;
import net.minecraft.world.level.levelgen.feature.trunkplacers.ForkingTrunkPlacer;
import net.minecraft.world.level.levelgen.feature.trunkplacers.StraightTrunkPlacer;
import net.minecraft.world.level.levelgen.placement.BiomeFilter;
import net.minecraft.world.level.levelgen.placement.CountOnEveryLayerPlacement;
import net.minecraft.world.level.levelgen.placement.CountPlacement;
import net.minecraft.world.level.levelgen.placement.InSquarePlacement;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.placement.PlacementModifier;
import net.minecraft.world.level.levelgen.placement.RarityFilter;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.bonus.Bonus;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.trees.AutumnTree;
import net.paradisemod.worldgen.features.PMFeatures;

public class PMFoliage {

    // trees
    public static final ArrayListMultimap<AutumnTree.Color, ResourceKey<PlacedFeature>> AUTUMN_TREES = createAutumnTrees();
    public static final EnumMap<AutumnTree.Color, ResourceKey<ConfiguredFeature<?, ?>>> AUTUMN_SAPLING_TREES = createAutumnSaplingTrees(false);
    public static final EnumMap<AutumnTree.Color, ResourceKey<ConfiguredFeature<?, ?>>> AUTUMN_SAPLING_TREES_BEES = createAutumnSaplingTrees(true);

    public static final ResourceKey<ConfiguredFeature<?, ?>> AUTUMN_TREE = PMFeatures.regConfFeature(
        "tree/autumn",
        Feature.RANDOM_SELECTOR,
        (cfgFeatureGetter, featureGetter) -> {
            var trees = allAutumnTrees();
            var tree = trees
                .stream()
                .map(treeKey -> new WeightedPlacedFeature(featureGetter.getOrThrow(treeKey), 1F / (float) trees.size()))
                .toList();

            return new RandomFeatureConfiguration(tree, featureGetter.getOrThrow(AUTUMN_TREES.get(AutumnTree.Color.BLUE).get(0)));
        }
    );

    public static final ResourceKey<PlacedFeature> AUTUMN_PLACER = PMFeatures.regPlacedFeature(
        "tree/autumn",
        AUTUMN_TREE,
        VegetationPlacements.treePlacement(PlacementUtils.countExtra(10, 0.1F, 1))
            .toArray(PlacementModifier[]::new)
    );

    public static final ResourceKey<ConfiguredFeature<?, ?>> BLACKENED_OAK_TREE = PMFeatures.regConfFeature("tree/blackened_oak", Feature.TREE,
        (cfgFeatureGetter, featureGetter) -> new TreeConfigurationBuilder(
            BlockStateProvider.simple(DeepDarkBlocks.BLACKENED_OAK_LOG.get()),
            new DarkOakTrunkPlacer(6, 2, 1),
            BlockStateProvider.simple(DeepDarkBlocks.BLACKENED_OAK_LEAVES.get()),
            new DarkOakFoliagePlacer(
                ConstantInt.of(0),
                ConstantInt.of(0)
            ),
            new ThreeLayersFeatureSize(1, 1, 0, 1, 2, OptionalInt.empty())
        )
            .ignoreVines()
            .build()
    );

    public static final ResourceKey<PlacedFeature> BLACKENED_OAK_PLACER = PMFeatures.regPlacedFeature("tree/blackened_oak", BLACKENED_OAK_TREE, PlacementUtils.filteredByBlockSurvival(Blocks.OAK_SAPLING));

    public static final ResourceKey<ConfiguredFeature<?, ?>> BLACKENED_SPRUCE_TREE = PMFeatures.regConfFeature("tree/blackened_spruce", Feature.TREE,
        (cfgFeatureGetter, featureGetter) -> new TreeConfigurationBuilder(
            BlockStateProvider.simple(DeepDarkBlocks.BLACKENED_SPRUCE_LOG.get()),
            new StraightTrunkPlacer(5, 2, 1),
            BlockStateProvider.simple(DeepDarkBlocks.BLACKENED_SPRUCE_LEAVES.get()),
            new SpruceFoliagePlacer(
                UniformInt.of(2, 3),
                UniformInt.of(0, 2),
                UniformInt.of(1, 2)
            ),
            new TwoLayersFeatureSize(2, 0, 2)
        )
            .ignoreVines()
            .build()
    );

    public static final ResourceKey<PlacedFeature> BLACKENED_SPRUCE_PLACER = PMFeatures.regPlacedFeature("tree/blackened_spruce", BLACKENED_SPRUCE_TREE, PlacementUtils.filteredByBlockSurvival(Blocks.OAK_SAPLING));

    public static final ResourceKey<ConfiguredFeature<?, ?>> GLOWING_OAK_TREE = PMFeatures.regConfFeature("tree/glowing_oak", Feature.TREE,
        (cfgFeatureGetter, featureGetter) -> new TreeConfigurationBuilder(
            BlockStateProvider.simple(DeepDarkBlocks.GLOWING_OAK_LOG.get()),
            new StraightTrunkPlacer(4, 2, 0),
            BlockStateProvider.simple(DeepDarkBlocks.GLOWING_OAK_LEAVES.get()),
            new BlobFoliagePlacer(
                ConstantInt.of(2),
                ConstantInt.of(0),
                3
            ),
            new TwoLayersFeatureSize(1, 0, 1)
        )
            .build()
    );

    public static final ResourceKey<ConfiguredFeature<?, ?>> FANCY_GLOWING_OAK_TREE = PMFeatures.regConfFeature("tree/fancy_glowing_oak", Feature.TREE,
        (cfgFeatureGetter, featureGetter) -> new TreeConfigurationBuilder(
            BlockStateProvider.simple(DeepDarkBlocks.GLOWING_OAK_LOG.get()),
            new FancyTrunkPlacer(3, 11, 0),
            BlockStateProvider.simple(DeepDarkBlocks.GLOWING_OAK_LEAVES.get()),
            new FancyFoliagePlacer(
                ConstantInt.of(2),
                ConstantInt.of(4),
                4
            ),
            new TwoLayersFeatureSize(0, 0, 0, OptionalInt.of(4))
        )
            .build()
    );

    public static final ResourceKey<PlacedFeature> GLOWING_OAK_PLACER = PMFeatures.regPlacedFeature("tree/glowing_oak", GLOWING_OAK_TREE, PlacementUtils.filteredByBlockSurvival(Blocks.OAK_SAPLING));
    public static final ResourceKey<PlacedFeature> FANCY_GLOWING_OAK_PLACER = PMFeatures.regPlacedFeature("tree/fancy_glowing_oak", FANCY_GLOWING_OAK_TREE, PlacementUtils.filteredByBlockSurvival(Blocks.OAK_SAPLING));

    public static final ResourceKey<ConfiguredFeature<?, ?>> MESQUITE_TREE = PMFeatures.regConfFeature("tree/mequite", Feature.TREE,
        (cfgFeatureGetter, featureGetter) -> new TreeConfigurationBuilder(
            BlockStateProvider.simple(Decoration.MESQUITE_LOG.get()),
            new ForkingTrunkPlacer(3, 1, 1),
            BlockStateProvider.simple(Decoration.MESQUITE_LEAVES.get()),
            new AcaciaFoliagePlacer(ConstantInt.of(1), ConstantInt.of(0)),
            new TwoLayersFeatureSize(1, 0, 2)
        )
            .ignoreVines()
            .build()
    );

    public static final ResourceKey<PlacedFeature> MESQUITE_PLACER = PMFeatures.regPlacedFeature("tree/mesquite", MESQUITE_TREE, PlacementUtils.filteredByBlockSurvival(Blocks.OAK_SAPLING));

    public static final ResourceKey<ConfiguredFeature<?, ?>> PALO_VERDE_TREE = PMFeatures.regConfFeature("tree/palo_verde", Feature.TREE, (cfgFeatureGetter, featureGetter) -> paloVerdeConfig(false));
    public static final ResourceKey<ConfiguredFeature<?, ?>> PALO_VERDE_TREE_BEES = PMFeatures.regConfFeature("tree/palo_verde_bees", Feature.TREE, (cfgFeatureGetter, featureGetter) -> paloVerdeConfig(true));
    public static final ResourceKey<PlacedFeature> PALO_VERDE_PLACER = PMFeatures.regPlacedFeature("tree/palo_verde_tree", PALO_VERDE_TREE, PlacementUtils.filteredByBlockSurvival(Blocks.OAK_SAPLING));

    public static final ResourceKey<ConfiguredFeature<?, ?>> XMAS_TREE = PMFeatures.regConfFeature(
        "tree/christmas", Feature.TREE,
        (cfgFeatureGetter, featureGetter) -> new TreeConfigurationBuilder(
            BlockStateProvider.simple(Blocks.SPRUCE_LOG),
            new StraightTrunkPlacer(5, 2, 1),
            BlockStateProvider.simple(Bonus.CHRISTMAS_LEAVES.get()),
            new SpruceFoliagePlacer(
                UniformInt.of(2, 3),
                UniformInt.of(0, 2),
                UniformInt.of(1, 2)
            ),
            new TwoLayersFeatureSize(2, 0, 2)
        )
            .build()
    );

    public static final ResourceKey<PlacedFeature> XMAS_PLACER = PMFeatures.regPlacedFeature("tree/christmas", XMAS_TREE, PlacementUtils.filteredByBlockSurvival(Blocks.OAK_SAPLING));

    // tree features
    public static final ResourceKey<PlacedFeature> RED_MUSHROOM = PMFeatures.regPlacedFeature("red_mushroom", TreeFeatures.HUGE_RED_MUSHROOM, PlacementUtils.filteredByBlockSurvival(Blocks.OAK_SAPLING));
    public static final ResourceKey<PlacedFeature> BROWN_MUSHROOM = PMFeatures.regPlacedFeature("brown_mushroom", TreeFeatures.HUGE_BROWN_MUSHROOM, PlacementUtils.filteredByBlockSurvival(Blocks.OAK_SAPLING));
    public static final ResourceKey<ConfiguredFeature<?, ?>> HUGE_GLOWSHROOM = PMFeatures.regConfFeature("huge_glowshroom", Feature.HUGE_RED_MUSHROOM, (cfgFeatureGetter, featureGetter) -> new HugeMushroomFeatureConfiguration(BlockStateProvider.simple(DeepDarkBlocks.GLOWSHROOM_BLOCK.get().defaultBlockState().setValue(HugeMushroomBlock.DOWN, false)), BlockStateProvider.simple(DeepDarkBlocks.GLOWSHROOM_STEM.get().defaultBlockState().setValue(HugeMushroomBlock.UP, false).setValue(HugeMushroomBlock.DOWN, false)), 2));
    public static final ResourceKey<PlacedFeature> HUGE_GLOWSHROOM_PLACER = PMFeatures.regPlacedFeature("huge_glowshroom", HUGE_GLOWSHROOM, PlacementUtils.filteredByBlockSurvival(Blocks.OAK_SAPLING));
    public static final ResourceKey<PlacedFeature> TR_BIRCH = PMFeatures.regPlacedFeature("tree/tr_birch", Feature.TREE, (cfgFeatureGetter, featureGetter) -> tempRainforestTree(Blocks.BIRCH_LOG, Blocks.BIRCH_LEAVES, false), PlacementUtils.filteredByBlockSurvival(Blocks.OAK_SAPLING));
    public static final ResourceKey<PlacedFeature> TR_BIRCH_BEES = PMFeatures.regPlacedFeature("tree/tr_birch_bees", Feature.TREE, (cfgFeatureGetter, featureGetter) -> tempRainforestTree(Blocks.BIRCH_LOG, Blocks.BIRCH_LEAVES, true), PlacementUtils.filteredByBlockSurvival(Blocks.OAK_SAPLING));
    public static final ResourceKey<PlacedFeature> TR_OAK = PMFeatures.regPlacedFeature("tree/tr_oak", Feature.TREE, (cfgFeatureGetter, featureGetter) -> tempRainforestTree(Blocks.OAK_LOG, Blocks.OAK_LEAVES, false), PlacementUtils.filteredByBlockSurvival(Blocks.OAK_SAPLING));
    public static final ResourceKey<PlacedFeature> TR_OAK_BEES = PMFeatures.regPlacedFeature("tree/tr_oak_bees", Feature.TREE, (cfgFeatureGetter, featureGetter) -> tempRainforestTree(Blocks.OAK_LOG, Blocks.OAK_LEAVES, true), PlacementUtils.filteredByBlockSurvival(Blocks.OAK_SAPLING));

    public static final ResourceKey<PlacedFeature> TEMP_RAINFOREST_TREES = PMFeatures.getBigFoliagePlacer("tree/temp_rainforest_trees", false, false,
        List.of(
            TR_BIRCH,
            TR_BIRCH_BEES,
            TR_OAK,
            TR_OAK_BEES,
            TreePlacements.JUNGLE_TREE_CHECKED,
            TreePlacements.SPRUCE_CHECKED,
            TreePlacements.MEGA_JUNGLE_TREE_CHECKED,
            TreePlacements.MEGA_SPRUCE_CHECKED,
            TreePlacements.MEGA_PINE_CHECKED
        )
    );

    public static final ResourceKey<PlacedFeature> GALLERY_FOREST = PMFeatures.getBigFoliagePlacer("gallery_forest", false, false,
        List.of(
            TreePlacements.OAK_BEES_0002,
            TreePlacements.BIRCH_BEES_002,
            TreePlacements.JUNGLE_TREE_CHECKED
        )
    );

    public static final ResourceKey<PlacedFeature> WEEPING_FOREST_TREES = PMFeatures.getBigFoliagePlacer("weeping_forest_trees", false, false,
        List.of(
            TreePlacements.SPRUCE_CHECKED,
            TreePlacements.MEGA_SPRUCE_CHECKED,
            TreePlacements.MEGA_PINE_CHECKED,
            TreePlacements.OAK_BEES_002,
            TreePlacements.OAK_CHECKED,
            TreePlacements.FANCY_OAK_BEES,
            TreePlacements.FANCY_OAK_CHECKED
        )
    );

    public static final ResourceKey<PlacedFeature> EXTRA_PALO_VERDE = PMFeatures.getBigFoliagePlacer("extra_palo_verde", false, false, List.of(PALO_VERDE_PLACER));
    public static final ResourceKey<PlacedFeature> EXTRA_MESQUITE = PMFeatures.getBigFoliagePlacer("extra_mesquite", false, false, List.of(MESQUITE_PLACER));

    public static final ResourceKey<PlacedFeature> DESERT_TREES = PMFeatures.getBigFoliagePlacer("desert_trees", true, false,
        List.of(
            MESQUITE_PLACER,
            PALO_VERDE_PLACER
        )
    );

    public static final ResourceKey<PlacedFeature> COLD_DESERT_TREES = PMFeatures.getBigFoliagePlacer("cold_desert_trees", true, false,
        List.of(
            MESQUITE_PLACER,
            TreePlacements.SPRUCE_CHECKED,
            TreePlacements.MEGA_SPRUCE_CHECKED,
            TreePlacements.MEGA_PINE_CHECKED
        )
    );

    public static final ResourceKey<PlacedFeature> ORIGIN_TREES = PMFeatures.getBigFoliagePlacer("origin_trees", false, false, List.of(TreePlacements.OAK_CHECKED));

    // deep dark foliage
    public static final ResourceKey<PlacedFeature> GLOWING_FOLIAGE = PMFeatures.getBasicFeature("glowing_foliage", () -> new FoliagePatch(PMTags.Blocks.GLOWING_FOLIAGE), CountOnEveryLayerPlacement.of(24), BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> BLACKENED_FOLIAGE = PMFeatures.getBasicFeature("blackened_foliage", () -> new FoliagePatch(PMTags.Blocks.BLACKENED_FOLIAGE), CountOnEveryLayerPlacement.of(24), BiomeFilter.biome());

    public static final ResourceKey<PlacedFeature> LARGE_GLOWING_FOLIAGE = PMFeatures.getBigFoliagePlacer("large_glowing_foliage", false, true,
        List.of(
            GLOWING_OAK_PLACER,
            FANCY_GLOWING_OAK_PLACER
        ),

        List.of(
            BLACKENED_OAK_PLACER
        ),

        List.of(
            HUGE_GLOWSHROOM,
            TreeFeatures.HUGE_RED_MUSHROOM,
            TreeFeatures.HUGE_BROWN_MUSHROOM
        )
    );

    public static final ResourceKey<PlacedFeature> BLACKENED_SPRUCE_COVER = PMFeatures.getBigFoliagePlacer("blackened_spruce_cover", false, true, List.of(BLACKENED_SPRUCE_PLACER));

    public static final ResourceKey<PlacedFeature> LARGE_BLACKENED_FOLIAGE = PMFeatures.getBigFoliagePlacer("large_blackened_foliage", false, true,
        List.of(
            BLACKENED_OAK_PLACER,
            BLACKENED_OAK_PLACER
        ),

        List.of(
            GLOWING_OAK_PLACER,
            FANCY_GLOWING_OAK_PLACER
        ),

        List.of(
            HUGE_GLOWSHROOM,
            TreeFeatures.HUGE_RED_MUSHROOM,
            TreeFeatures.HUGE_BROWN_MUSHROOM
        )
    );

    // overworld core foliage
    public static final ResourceKey<PlacedFeature> OWC_TREES = PMFeatures.getBasicFeature("owc_trees", OWCTrees::new, CountOnEveryLayerPlacement.of(128));
    public static final ResourceKey<PlacedFeature> OWC_FOLIAGE = PMFeatures.getBasicFeature("owc_foliage", OWCFoliage::new, CountOnEveryLayerPlacement.of(128));

    public static final ResourceKey<PlacedFeature> OWC_SHROOMS = PMFeatures.getBigFoliagePlacer("owc_shrooms", true, true,
        List.of(),
        HUGE_GLOWSHROOM,
        TreeFeatures.HUGE_BROWN_MUSHROOM,
        TreeFeatures.HUGE_RED_MUSHROOM
    );

    // misc foliage features
    public static final ResourceKey<PlacedFeature> CHRISTMAS_LANTERNS = PMFeatures.getBasicFeature("christmas_lanterns", () -> new FoliagePatch(PMTags.Blocks.LANTERNS), InSquarePlacement.spread(), CountPlacement.of(10), BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> CHRISTMAS_CRYSTALS = PMFeatures.getBasicFeature("christmas_crystals", () -> new FoliagePatch(PMTags.Blocks.CRYSTAL_CLUSTERS), InSquarePlacement.spread(), CountPlacement.of(10), BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> SOUL_PUMPKIN_PATCH = PMFeatures.getBasicFeature("soul_pumpkin_patch", () -> new FoliagePatch(Decoration.SOUL_PUMPKIN.get()), InSquarePlacement.spread(), RarityFilter.onAverageOnceEvery(10), BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> ROSES = PMFeatures.getBasicFeature("roses", () -> new FoliagePatch(PMTags.Blocks.ROSES), InSquarePlacement.spread(), RarityFilter.onAverageOnceEvery(10));
    public static final ResourceKey<PlacedFeature> PRICKLY_PEAR = PMFeatures.getBasicFeature("prickly_pear", PricklyPearGen::new, InSquarePlacement.spread(), RarityFilter.onAverageOnceEvery(15), BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> RD_CACTUS = PMFeatures.getBasicFeature("rd_cactus", Cactus::new, InSquarePlacement.spread(), CountPlacement.of(32), BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> END_FOLIAGE = PMFeatures.getBasicFeature("end_foliage", () -> new FoliagePatch(PMTags.Blocks.END_FOLIAGE), InSquarePlacement.spread(), CountPlacement.of(10), BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> RD_FOLIAGE = PMFeatures.getBasicFeature("rd_foliage", () -> new FoliagePatch(Blocks.GRASS, Blocks.TALL_GRASS), InSquarePlacement.spread(), CountPlacement.of(5), BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> PM_CHORUS_PLANT = PMFeatures.getBasicFeature("pm_chorus_plant", PMChorusPlant::new, CountPlacement.of(UniformInt.of(0, 4)), InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome());
    public static final ResourceKey<PlacedFeature> AUTUMN_PUMPKINS = PMFeatures.getBasicFeature("autumn_pumpkins", () -> new FoliagePatch(Blocks.PUMPKIN), InSquarePlacement.spread(), CountPlacement.of(5), BiomeFilter.biome());

    public static void init() { }

    private static TreeConfiguration tempRainforestTree(Block wood, Block leaves, boolean bees) {
        var builder = new TreeConfiguration.TreeConfigurationBuilder(
            BlockStateProvider.simple(wood),
            new StraightTrunkPlacer(4, 2, 0),
            BlockStateProvider.simple(leaves),
            new BlobFoliagePlacer(
                ConstantInt.of(2),
                ConstantInt.of(0),
                3
            ),
            new TwoLayersFeatureSize(1, 0, 1)
        );

        ArrayList<TreeDecorator> decorators = new ArrayList<>(List.of(new LeaveVineDecorator(0.25F), TrunkVineDecorator.INSTANCE));
        if(bees) decorators.add(new BeehiveDecorator(0.5F));
        builder.decorators(decorators);
        return builder.build();
    }

    private static TreeConfiguration paloVerdeConfig(boolean bees) {
        TreeConfiguration.TreeConfigurationBuilder builder = new TreeConfiguration.TreeConfigurationBuilder(
            BlockStateProvider.simple(Decoration.PALO_VERDE_LOG.get()),
            new FancyTrunkPlacer(3, 11, 0),
            BlockStateProvider.simple(Decoration.PALO_VERDE_LEAVES.get()),
            new FancyFoliagePlacer(
                ConstantInt.of(2),
                ConstantInt.of(4),
                4
            ),
            new TwoLayersFeatureSize(0, 0, 0, OptionalInt.of(4))
        )
            .ignoreVines();

        if(bees)
            builder = builder.decorators(List.of(new BeehiveDecorator(0.5F)));

        return builder.build();
    }

    public static List<ResourceKey<PlacedFeature>> allAutumnTrees() {
        ArrayList<ResourceKey<PlacedFeature>> trees = new ArrayList<>();

        for(var color : AutumnTree.Color.values())
            trees.addAll(AUTUMN_TREES.get(color));

        return trees;
    }

    private static ArrayListMultimap<AutumnTree.Color, ResourceKey<PlacedFeature>> createAutumnTrees() {
        ArrayListMultimap<AutumnTree.Color, ResourceKey<PlacedFeature>> trees = ArrayListMultimap.create();

        for(var color : AutumnTree.Color.values())
            for(var woodType : AutumnTree.WoodType.values()) {
                trees.put(color, createAutumnTree(woodType, color, false, false));
                trees.put(color, createAutumnTree(woodType, color, false, true));
                trees.put(color, createAutumnTree(woodType, color, true, false));
                trees.put(color, createAutumnTree(woodType, color, true, true));
            }

        return trees;
    }

    private static EnumMap<AutumnTree.Color, ResourceKey<ConfiguredFeature<?, ?>>> createAutumnSaplingTrees(boolean bees) {
        EnumMap<AutumnTree.Color, ResourceKey<ConfiguredFeature<?, ?>>> trees = new EnumMap<>(AutumnTree.Color.class);

        for(var color : AutumnTree.Color.values())
            trees.put(color, createAutumnSaplingTree(color, bees));

        return trees;
    }

    private static ResourceKey<PlacedFeature> createAutumnTree(AutumnTree.WoodType type, AutumnTree.Color color, boolean fancy, boolean bees) {
        var name = color.name().toLowerCase() + "_autumn_";
        if(fancy)
            name = "fancy_" + name;

        name = name + type.name().toLowerCase();

        if(bees)
            name = name + "_bees";

        return PMFeatures.regPlacedFeature("tree/" + name, Feature.TREE, (cfgFeatureGetter, featureGetter) -> autumnTreeConfig(type, color, fancy, bees), PlacementUtils.filteredByBlockSurvival(Blocks.OAK_SAPLING));
    }

    private static ResourceKey<ConfiguredFeature<?, ?>> createAutumnSaplingTree(AutumnTree.Color color, boolean bees) {
        var name = "tree/" + color.name().toLowerCase() + "_autumn";
        var trees = AUTUMN_TREES.get(color);
        if(bees) name = name + "_bees";
        return PMFeatures.regConfFeature(name, Feature.RANDOM_SELECTOR,
            (cfgFeatureGetter, featureGetter) -> new RandomFeatureConfiguration(
                trees.stream()
                    .map(tree -> new WeightedPlacedFeature(featureGetter.getOrThrow(tree), 0.2F))
                    .toList(),

                featureGetter.getOrThrow(trees.get(0))
            )
        );
    }

    private static TreeConfiguration autumnTreeConfig(AutumnTree.WoodType type, AutumnTree.Color color, boolean bees, boolean fancy) {
        TreeConfiguration.TreeConfigurationBuilder builder;

        if(fancy) {
            builder = new TreeConfiguration.TreeConfigurationBuilder(
                BlockStateProvider.simple(type.getWood()),
                new FancyTrunkPlacer(3, 11, 0),
                BlockStateProvider.simple(color.getLeaves()),
                new FancyFoliagePlacer(
                    ConstantInt.of(2),
                    ConstantInt.of(4),
                    4
                ),
                new TwoLayersFeatureSize(0, 0, 0, OptionalInt.of(4))
            );
        }
        else {
            builder = new TreeConfiguration.TreeConfigurationBuilder(
                BlockStateProvider.simple(type.getWood()),
                new StraightTrunkPlacer(4, 2, 0),
                BlockStateProvider.simple(color.getLeaves()),
                new BlobFoliagePlacer(
                    ConstantInt.of(2),
                    ConstantInt.of(0),
                    3
                ),
                new TwoLayersFeatureSize(1, 0, 1)
            );
        }

        if(bees)
            builder.decorators(List.of(new BeehiveDecorator(0.5F)));

        return builder.ignoreVines().build();
    }
}