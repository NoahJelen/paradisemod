package net.paradisemod.worldgen.features.foliage;

import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.worldgen.features.BasicFeature;

public class Cactus extends BasicFeature {

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        var pricklyPearGen = new PricklyPearGen();
        var y = world.getHeight(Heightmap.Types.WORLD_SURFACE, pos.getX(), pos.getZ());
        var blockpos = new BlockPos(pos.getX(), y, pos.getZ());
        if(rand.nextBoolean()) return pricklyPearGen.place(FeatureConfiguration.NONE, world, generator, rand, pos);
        var height = 1 + rand.nextInt(2);
        if (world.getBlockState(blockpos.below()).is(PMTags.Blocks.GROUND_BLOCKS)) {
            world.setBlock(blockpos.below(), Blocks.SAND.defaultBlockState(), 4);
            for(int i = 0; i <= height; i++) world.setBlock(blockpos.above(i), Blocks.CACTUS.defaultBlockState(), 4);
        }

        return true;
    }
}