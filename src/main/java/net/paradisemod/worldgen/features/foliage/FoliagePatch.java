package net.paradisemod.worldgen.features.foliage;

import java.util.ArrayList;
import java.util.Arrays;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.PumpkinBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.PMWorld;
import net.paradisemod.world.blocks.CrystalCluster;
import net.paradisemod.worldgen.features.BasicFeature;

public class FoliagePatch extends BasicFeature {
    private final @Nullable TagKey<Block> tagName;
    private final ArrayList<BlockState> blocks = new ArrayList<>();

    public FoliagePatch(@Nullable TagKey<Block> tagName) { this.tagName = tagName; }

    public FoliagePatch(Block... blocks) {
        tagName = null;
        this.blocks.addAll(
            Arrays.stream(blocks)
                .map(Block::defaultBlockState)
                .toList()
        );
    }

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        if(blocks.isEmpty()) {
            blocks.addAll(
                Utils.getBlockTag(tagName)
                    .stream()
                    .map(Block::defaultBlockState)
                    .toList()
            );
        }

        var dim = world.getLevel().dimension();
        var dimType = world.getLevel().dimensionType();
        var minY = world.getLevel().dimensionType().minY();
        var height = dimType.height();
        var dimName = dim.location().toString();
        var chunkX = (pos.getX() >> 4) * 16;
        var chunkZ = (pos.getZ() >> 4) * 16;
        var chunkCorner = new BlockPos(chunkX, 63, chunkZ);
        var origBiome = world.getBiome(pos);
        if(dimType.hasCeiling() || dimName.contains("paradisemod")) {
            var layers = new ArrayList<Integer>();
            for(int y = 0; y < height; y++)
                if(y % 10 == 0) {
                    var layerPos = new BlockPos(pos.getX(), y, pos.getZ());
                    var layerBiome = world.getBiome(layerPos);
                    if(layerBiome.is(origBiome.unwrapKey().get()))
                        layers.add(y);
                }
            if(!layers.isEmpty())
                chunkCorner = new BlockPos(chunkX, minY + layers.get(rand.nextInt(layers.size())), chunkZ);
        }

        if(dimName.equals("minecraft:the_end")) chunkCorner = new BlockPos(chunkX, 0, chunkZ);
        var blockToPlace = blocks.get(rand.nextInt(blocks.size()));
        var toPlacePos = chunkCorner.offset(rand.nextInt(8), 0, rand.nextInt(8));
        var chance = 5 * blocks.size();
        if(blocks.get(0).getBlock() instanceof PumpkinBlock) chance = 10;
        for(int x = 0; x < 8; x++) {
            for(int z = 0; z < 8; z++) {
                for(int y = 0; y < 194; y++) {
                    var newPos = toPlacePos.offset(x, y, z);
                    var biome = world.getBiome(newPos);
                    if(newPos.getY() > height - 10) break;
                    var blockstate = world.getBlockState(newPos);
                    var blockstateAbove = world.getBlockState(newPos.above());
                    var validBottomBlock = !blockstate.is(DeepDarkBlocks.DARKSTONE.get()) || tagName == PMTags.Blocks.CRYSTAL_CLUSTERS;
                    var validBlock = blockstateAbove.isAir() || blockstateAbove.is(Blocks.SNOW);
                    if((blockstate.is(BlockTags.DIRT) || blockstate.is(PMWorld.OVERGROWN_END_STONE.get())) && validBottomBlock && validBlock && rand.nextInt(chance) == 0) {
                        if(tagName == PMTags.Blocks.CRYSTAL_CLUSTERS) {
                            blockToPlace = blocks.get(rand.nextInt(blocks.size()));
                            var crystal = (CrystalCluster) blockToPlace.getBlock();
                            blockToPlace = crystal.defaultBlockState().setValue(CrystalCluster.TYPE, rand.nextInt(4));
                        }
                        else if(tagName == PMTags.Blocks.BLACKENED_FOLIAGE || tagName == PMTags.Blocks.GLOWING_FOLIAGE || tagName == PMTags.Blocks.END_FOLIAGE || tagName == PMTags.Blocks.LANTERNS)
                            blockToPlace = blocks.get(rand.nextInt(blocks.size()));

                        if(biome.is(origBiome.unwrapKey().get())) {
                            world.setBlock(newPos.above(), blockToPlace, 32);
                            if(blockToPlace.hasProperty(BlockStateProperties.DOUBLE_BLOCK_HALF))
                                world.setBlock(newPos.above(2), blockToPlace.setValue(BlockStateProperties.DOUBLE_BLOCK_HALF, DoubleBlockHalf.UPPER), 32);
                        }
                    }
                }
            }
        }

        return true;
    }
}