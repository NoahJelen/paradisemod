package net.paradisemod.worldgen.features.foliage;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.Heightmap;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.decoration.blocks.PricklyPear;
import net.paradisemod.worldgen.features.BasicFeature;

public class PricklyPearGen extends BasicFeature {
    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        var heightY = world.getHeight(Heightmap.Types.WORLD_SURFACE, pos.getX(), pos.getZ());
        var blockpos = new BlockPos(pos.getX(), heightY, pos.getZ());
        Direction.Axis[] axes = {Direction.Axis.X, Direction.Axis.Z};

        if(!world.getBlockState(blockpos.below()).is(PMTags.Blocks.GROUND_BLOCKS))
            return true;

        world.setBlock(blockpos, Decoration.PRICKLY_PEAR.get().defaultBlockState().setValue(PricklyPear.AXIS, axes[rand.nextInt(2)]).setValue(PricklyPear.HAS_FRUIT, rand.nextBoolean()), 4);

        for (var i = 0; i < 5; i++)
            for (var x = -1; x <= 1; x++)
                for (var z = -1; z <= 1; z++)
                    for (var y = 0; y <= 1; y++) {
                        var state  = world.getBlockState(blockpos.offset(x,y,z));
                        if (state.getBlock() instanceof PricklyPear)
                            genPlant(state, world, blockpos.offset(x, y, z), rand);
                    }

        return true;
    }

    private static void genPlant(BlockState state, WorldGenLevel world, BlockPos pos, RandomSource rand) {
        BlockPos[] spreadTo = {pos.above().east(), pos.above().west(), pos.above()};

        if (state.getValue(PricklyPear.AXIS) == Direction.Axis.Z) {
            spreadTo[0] = pos.above().north();
            spreadTo[1] = pos.above().south();
        }

        var newpos = spreadTo[rand.nextInt(3)];
        if (world.getBlockState(newpos).getBlock() != Blocks.AIR) return;
        if (world.getBlockState(newpos).getBlock() instanceof PricklyPear) return;
        var newState = updateState(world, newpos, state);
        world.setBlock(newpos, newState.setValue(PricklyPear.HAS_FRUIT, rand.nextBoolean()), 1);
    }

    private static BlockState updateState(WorldGenLevel world, BlockPos pos, BlockState state) {
        if ((world.getBlockState(pos.east()).is(Decoration.PRICKLY_PEAR.get()) && world.getBlockState(pos.west()).is(Decoration.PRICKLY_PEAR.get())) || (world.getBlockState(pos.north()).is(Decoration.PRICKLY_PEAR.get()) && world.getBlockState(pos.south()).is(Decoration.PRICKLY_PEAR.get()))) return state.setValue(PricklyPear.TILT_DIR, PricklyPear.TiltDir.NONE);
        else if (world.getBlockState(pos.below().east()).is(Decoration.PRICKLY_PEAR.get())) return state.setValue(PricklyPear.TILT_DIR, PricklyPear.TiltDir.WEST);
        else if (world.getBlockState(pos.below().west()).is(Decoration.PRICKLY_PEAR.get())) return state.setValue(PricklyPear.TILT_DIR, PricklyPear.TiltDir.EAST);
        else if (world.getBlockState(pos.below().north()).is(Decoration.PRICKLY_PEAR.get())) return state.setValue(PricklyPear.TILT_DIR, PricklyPear.TiltDir.SOUTH);
        else if (world.getBlockState(pos.below().south()).is(Decoration.PRICKLY_PEAR.get())) return state.setValue(PricklyPear.TILT_DIR, PricklyPear.TiltDir.NORTH);
        return state;
    }
}