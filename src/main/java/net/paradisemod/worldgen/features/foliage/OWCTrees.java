package net.paradisemod.worldgen.features.foliage;

import net.minecraft.core.BlockPos;
import net.minecraft.data.worldgen.placement.TreePlacements;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.BiomeTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.world.biome.PMBiomes;
import net.paradisemod.worldgen.features.BasicFeature;

public class OWCTrees extends BasicFeature {

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        var featurePos = pos;
        if(world.getBlockState(pos.below()).is(Blocks.SNOW)) featurePos = pos.below();

        var testState = world.getBlockState(featurePos);
        if(!(testState.isAir() || testState.is(Blocks.SNOW))) return false;

        var biome = world.getBiome(featurePos);
        ResourceKey<PlacedFeature> tree = null;
        var treeSpacing = 10;

        if(biome.is(PMBiomes.UNDERGROUND_JUNGLE)) {
            tree = TreePlacements.JUNGLE_BUSH;
            if(rand.nextBoolean()) tree = TreePlacements.JUNGLE_TREE_CHECKED;
            else if(rand.nextBoolean()) tree = TreePlacements.MEGA_JUNGLE_TREE_CHECKED;
        }
        else if(biome.is(PMBiomes.UNDERGROUND_BIRCH_FOREST)) {
            treeSpacing = 20;
            tree = TreePlacements.BIRCH_CHECKED;
        }
        else if(biome.is(PMBiomes.UNDERGROUND_DARK_FOREST)) {
            treeSpacing = 15;
            if(rand.nextInt(5) == 0) {
                if(rand.nextBoolean())
                    tree = TreePlacements.BIRCH_CHECKED;
                else tree = TreePlacements.OAK_CHECKED;
            }
            else tree = TreePlacements.DARK_OAK_CHECKED;
        }
        else if(biome.is(PMBiomes.SNOWY_UNDERGROUND_TAIGA) || biome.is(PMBiomes.UNDERGROUND_TAIGA)) {
            if(biome.is(PMBiomes.SNOWY_UNDERGROUND_TAIGA)) treeSpacing = 15;
            tree = TreePlacements.SPRUCE_CHECKED;
            if(rand.nextBoolean())
                tree = TreePlacements.MEGA_SPRUCE_CHECKED;
            else if(rand.nextBoolean())
                tree = TreePlacements.MEGA_PINE_CHECKED;
        }
        else if(biome.is(PMBiomes.UNDERGROUND_SAVANNA)) {
            treeSpacing = 40;
            tree = TreePlacements.ACACIA_CHECKED;
        }
        else if(biome.is(PMBiomes.UNDERGROUND_FOREST) || biome.is(PMBiomes.UNDERGROUND_FLOWER_FOREST)) {
            treeSpacing = 20;
            if(rand.nextBoolean()) tree = TreePlacements.BIRCH_CHECKED;
            else tree = TreePlacements.OAK_CHECKED;
        }
        else if(biome.is(PMBiomes.UNDERGROUND_ROCKY_DESERT)) {
            treeSpacing = 25;
            if(rand.nextBoolean()) tree = PMFoliage.MESQUITE_PLACER;
            else tree = PMFoliage.PALO_VERDE_PLACER;
        }
        else if(biome.is(PMTags.Biomes.COLD_ROCKY_DESERTS)) {
            treeSpacing = 25;
            if(rand.nextBoolean()) tree = TreePlacements.SPRUCE_CHECKED;
            else if(rand.nextBoolean())
                tree = TreePlacements.MEGA_SPRUCE_CHECKED;
            else if(rand.nextBoolean())
                tree = TreePlacements.MEGA_PINE_CHECKED;
            else tree = PMFoliage.MESQUITE_PLACER;
        }
        else if(biome.is(PMBiomes.UNDERGROUND_PALO_VERDE_FOREST)) {
            treeSpacing = 25;
            tree = PMFoliage.PALO_VERDE_PLACER;
        }
        else if(biome.is(PMBiomes.UNDERGROUND_MESQUITE_FOREST)) {
            treeSpacing = 25;
            tree = PMFoliage.MESQUITE_PLACER;
        }
        else if(biome.is(PMBiomes.UNDERGROUND_CHERRY_FOREST)) {
            treeSpacing = 25;
            tree = TreePlacements.CHERRY_CHECKED;
        }
        else if(biome.is(PMBiomes.UNDERGROUND_AUTUMN_FOREST)) {
            var trees = PMFoliage.allAutumnTrees();
            tree = trees.get(rand.nextInt(trees.size()));
        }
        else if(biome.is(PMBiomes.UNDERGROUND_SWAMP))
            tree = PMFoliage.TR_OAK;
        else if(biome.is(PMBiomes.UNDERGROUND_MANGROVE_SWAMP))
            tree = rand.nextBoolean() ? TreePlacements.MANGROVE_CHECKED : TreePlacements.TALL_MANGROVE_CHECKED;
        else if(biome.is(PMBiomes.UNDERGROUND_TEMPERATE_RAINFOREST))
            switch(rand.nextInt(4)) {
                case 0 -> tree = PMFoliage.TR_BIRCH;
                case 1 -> tree = PMFoliage.TR_OAK;
                case 2 -> tree = rand.nextBoolean() ? TreePlacements.SPRUCE_CHECKED : TreePlacements.MEGA_SPRUCE_CHECKED;
                case 3 -> tree = rand.nextBoolean() ? TreePlacements.JUNGLE_TREE_CHECKED : TreePlacements.MEGA_JUNGLE_TREE_CHECKED;
            }

        if(biome.is(BiomeTags.IS_FOREST) && !biome.is(PMBiomes.UNDERGROUND_AUTUMN_FOREST) && rand.nextInt(5) == 0) {
            tree = PMFoliage.RED_MUSHROOM;
            if(rand.nextBoolean())
                tree = PMFoliage.BROWN_MUSHROOM;
            else if(rand.nextBoolean())
                tree = PMFoliage.HUGE_GLOWSHROOM_PLACER;
        }

        if(tree == null) return false;
        else if(rand.nextInt(treeSpacing) == 0) {
            world.setBlock(featurePos, Blocks.AIR.defaultBlockState(), 32);
            world.setBlock(featurePos.above(), Blocks.AIR.defaultBlockState(), 32);
            world.setBlock(featurePos.above(2), Blocks.AIR.defaultBlockState(), 32);
            var treeToPlace = getPlacedFeature(tree);
            treeToPlace.place(world, generator, rand, featurePos);
            return true;
        }

        return false;
    }
}