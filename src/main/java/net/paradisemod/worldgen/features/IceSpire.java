package net.paradisemod.worldgen.features;

import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceKey;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.paradisemod.world.biome.PMBiomes;

public class IceSpire extends BasicFeature {
    private static final List<ResourceKey<Biome>> ALLOWED_BIOMES = List.of(
        Biomes.ICE_SPIKES,
        PMBiomes.ICE_SPIKES_CAVE
    );

    private static int genFunc(RandomSource rand, int x, int y) {
        return (int) ((100 + rand.nextInt(200)) * Math.cos((Math.pow(x, 2F) + Math.pow(y, 2)) / 20) / (Math.pow(x, 2) + Math.pow(y, 2) + 1));
    }

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        var featurePos = pos;
        if(world.getBlockState(pos.below()).is(Blocks.SNOW)) featurePos = pos.below();

        var biome = world.getBiome(featurePos);
        if(
            isFlat(world, featurePos.offset(-4, 0, -4), 8, 8, true) &&
            ALLOWED_BIOMES.contains(biome.unwrapKey().orElseThrow())
        )
            for(var x = -4; x < 4; x ++)
                for(var z = -4; z < 4; z++)
                    for(var y = featurePos.getY(); y <= featurePos.getY() + genFunc(rand, x, z); y ++) {
                        if(y > 310) continue;
                        var newPos = new BlockPos(featurePos.getX() + x, y, featurePos.getZ() + z);
                        if(!world.getBlockState(newPos).canOcclude())
                            world.setBlock(newPos, Blocks.PACKED_ICE.defaultBlockState(), 1);
                        else break;
                    }

        return true;
    }
}