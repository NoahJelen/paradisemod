package net.paradisemod.worldgen.features;

import java.util.EnumMap;
import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.BiomeTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraftforge.common.Tags;
import net.paradisemod.base.QuarkBlocks;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.building.Building;
import net.paradisemod.building.Doors;
import net.paradisemod.building.FenceGates;
import net.paradisemod.building.Fences;
import net.paradisemod.building.Slabs;
import net.paradisemod.building.Stairs;
import net.paradisemod.building.Trapdoors;
import net.paradisemod.decoration.ColoredLanterns;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.decoration.Tables;
import net.paradisemod.misc.Chests;
import net.paradisemod.redstone.Lamps;
import net.paradisemod.redstone.Plates;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.biome.PMBiomes;

public abstract class BasicFeature extends Feature<NoneFeatureConfiguration> {
    private Registry<ConfiguredFeature<?, ?>> cfgFeatureRegistry;
    private Registry<PlacedFeature> placedFeatureRegistry;

    public BasicFeature() { super(NoneFeatureConfiguration.CODEC); }

    @Override
    public boolean place(FeaturePlaceContext<NoneFeatureConfiguration> context) {
        var world = context.level();
        var regAccess = world.registryAccess();
        cfgFeatureRegistry = regAccess.registry(Registries.CONFIGURED_FEATURE).get();
        placedFeatureRegistry = regAccess.registry(Registries.PLACED_FEATURE).get();

        return Utils.handlePossibleException(
            () -> placeFeature(world, context.random(), context.origin(), context.chunkGenerator())
        );
    }

    protected final ConfiguredFeature<?, ?> getConfiguredFeature(ResourceKey<ConfiguredFeature<?, ?>> key) {
        return cfgFeatureRegistry.getOrThrow(key);
    }

    protected final Holder<ConfiguredFeature<?, ?>> getConfiguredFeatureHolder(ResourceKey<ConfiguredFeature<?, ?>> key) {
        return cfgFeatureRegistry.getHolderOrThrow(key);
    }

    protected final PlacedFeature getPlacedFeature(ResourceKey<PlacedFeature> key) {
        return placedFeatureRegistry.getOrThrow(key);
    }

    protected final Holder<PlacedFeature> getPlacedFeatureHolder(ResourceKey<PlacedFeature> key) {
        return placedFeatureRegistry.getHolderOrThrow(key);
    }

    protected abstract boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator);

    protected static boolean isUndergroundAreaValid(WorldGenLevel world, BlockPos pos, int sizeX, int sizeY, int sizeZ) {
        List<Block> surfaceBlocks = List.of(
            Blocks.GRASS_BLOCK,
            Blocks.MYCELIUM,
            Blocks.PODZOL,
            Blocks.COARSE_DIRT,
            DeepDarkBlocks.GLOWING_NYLIUM.get(),
            DeepDarkBlocks.OVERGROWN_DARKSTONE.get()
        );

        var numValidBlocks = 0;
        for(var x = 0; x < sizeX; x++)
            for(var y = 0; y < sizeY; y++)
                for(var z = 0; z < sizeZ; z++) {
                    var newPos = pos.offset(x, y, z);
                    var blockstate = world.getBlockState(newPos);
                    if(surfaceBlocks.contains(blockstate.getBlock())) return false;
                    if(blockstate.is(DeepDarkBlocks.REGENERATION_STONE.get()) || blockstate.is(DeepDarkBlocks.DARKSTONE.get()) || blockstate.is(Blocks.DEEPSLATE))
                        numValidBlocks++;
                }

        return (float) numValidBlocks / (sizeX * sizeY * sizeZ) >= 0.5F;
    }

    /** Checks if an area is flat */
    protected static boolean isFlat(WorldGenLevel world, BlockPos structurePos, int sizeX, int sizeZ, boolean checkAir) {
        for(var x = 0; x < sizeX; x++) {
            for (var z = 0; z < sizeZ; z++) {
                var newPos = structurePos.offset(x, -1, z);

                if(checkAir) {
                    var newPosAbove = structurePos.offset(x, 0, z);
                    var stateAbove = world.getBlockState(newPosAbove);
                    if (
                        (stateAbove.canOcclude() && !stateAbove.is(Blocks.SNOW)) ||
                        !world.getBlockState(newPos).canOcclude()
                    )
                        return false;
                }
                else if (!world.getBlockState(newPos).canOcclude()) return false;
            }
        }

        return true;
    }

    public static List<Block> getWood(RandomSource rand, Holder<Biome> biome) {
        var fence = Blocks.OAK_FENCE;
        var fenceGate = Blocks.OAK_FENCE_GATE;
        var craftingTable = Blocks.CRAFTING_TABLE;
        var log = Blocks.OAK_LOG;
        var plate = Blocks.OAK_PRESSURE_PLATE;
        var planks = Blocks.OAK_PLANKS;
        var stairs = Blocks.OAK_STAIRS;
        var door = Blocks.OAK_DOOR;
        var chest = QuarkBlocks.OAK_CHEST;
        var trapdoor = Blocks.OAK_TRAPDOOR;
        var bookShelf = Blocks.BOOKSHELF;
        var strippedLog = Blocks.STRIPPED_OAK_LOG;
        var slab = Blocks.OAK_SLAB;

        if(biome.is(Biomes.DARK_FOREST) || biome.is(PMBiomes.UNDERGROUND_DARK_FOREST)) {
            fence = Blocks.DARK_OAK_FENCE;
            fenceGate = Blocks.DARK_OAK_FENCE_GATE;
            craftingTable = Tables.DARK_OAK_CRAFTING_TABLE.get();
            log = Blocks.DARK_OAK_LOG;
            plate = Blocks.DARK_OAK_PRESSURE_PLATE;
            planks = Blocks.DARK_OAK_PLANKS;
            stairs = Blocks.DARK_OAK_STAIRS;
            door = Blocks.DARK_OAK_DOOR;
            chest = QuarkBlocks.DARK_OAK_CHEST;
            trapdoor = Blocks.DARK_OAK_TRAPDOOR;
            bookShelf = QuarkBlocks.DARK_OAK_BOOKSHELF;
            strippedLog = Blocks.STRIPPED_DARK_OAK_LOG;
            slab = Blocks.DARK_OAK_SLAB;
        }
        else if(biome.is(Biomes.CHERRY_GROVE)) {
            fence = Blocks.CHERRY_FENCE;
            fenceGate = Blocks.CHERRY_FENCE_GATE;
            craftingTable = Tables.CHERRY_CRAFTING_TABLE.get();
            log = Blocks.CHERRY_LOG;
            plate = Blocks.CHERRY_PRESSURE_PLATE;
            planks = Blocks.CHERRY_PLANKS;
            stairs = Blocks.CHERRY_STAIRS;
            door = Blocks.CHERRY_DOOR;
            chest = QuarkBlocks.CHERRY_CHEST;
            trapdoor = Blocks.CHERRY_TRAPDOOR;
            bookShelf = QuarkBlocks.CHERRY_BOOKSHELF;
            strippedLog = Blocks.STRIPPED_CHERRY_LOG;
            slab = Blocks.CHERRY_SLAB;
        }
        else if(biome.is(Biomes.MANGROVE_SWAMP)) {
            fence = Blocks.MANGROVE_FENCE;
            fenceGate = Blocks.MANGROVE_FENCE_GATE;
            craftingTable = Tables.MANGROVE_CRAFTING_TABLE.get();
            log = Blocks.MANGROVE_LOG;
            plate = Blocks.MANGROVE_PRESSURE_PLATE;
            planks = Blocks.MANGROVE_PLANKS;
            stairs = Blocks.MANGROVE_STAIRS;
            door = Blocks.MANGROVE_DOOR;
            chest = QuarkBlocks.MANGROVE_CHEST;
            trapdoor = Blocks.MANGROVE_TRAPDOOR;
            bookShelf = QuarkBlocks.MANGROVE_BOOKSHELF;
            strippedLog = Blocks.STRIPPED_MANGROVE_LOG;
            slab = Blocks.MANGROVE_SLAB;
        }
        else if(biome.is(PMTags.Biomes.ROCKY_DESERTS) || biome.is(PMTags.Biomes.COLD_ROCKY_DESERTS)) {
            fence = Fences.PALO_VERDE_FENCE.get();
            fenceGate = FenceGates.PALO_VERDE_FENCE_GATE.get();
            craftingTable = Tables.PALO_VERDE_CRAFTING_TABLE.get();
            log = Decoration.PALO_VERDE_LOG.get();
            plate = Plates.PALO_VERDE_PRESSURE_PLATE.get();
            planks = Building.PALO_VERDE_PLANKS.get();
            stairs = Stairs.PALO_VERDE_STAIRS.get();
            door = Doors.PALO_VERDE_DOOR.get();
            chest = Chests.PALO_VERDE_CHEST;
            trapdoor = Trapdoors.PALO_VERDE_TRAPDOOR.get();
            bookShelf = Decoration.PALO_VERDE_BOOKSHELF.get();
            strippedLog = Decoration.STRIPPED_PALO_VERDE_LOG.get();
            slab = Slabs.PALO_VERDE_SLAB.get();

            if(biome.is(PMBiomes.MESQUITE_FOREST) || rand.nextBoolean()) {
                fence = Fences.MESQUITE_FENCE.get();
                fenceGate = FenceGates.MESQUITE_FENCE_GATE.get();
                craftingTable = Tables.MESQUITE_CRAFTING_TABLE.get();
                log = Decoration.MESQUITE_LOG.get();
                plate = Plates.MESQUITE_PRESSURE_PLATE.get();
                planks = Building.MESQUITE_PLANKS.get();
                stairs = Stairs.MESQUITE_STAIRS.get();
                door = Doors.MESQUITE_DOOR.get();
                chest = Chests.MESQUITE_CHEST;
                trapdoor = Trapdoors.MESQUITE_TRAPDOOR.get();
                bookShelf = Decoration.MESQUITE_BOOKSHELF.get();
                strippedLog = Decoration.STRIPPED_MESQUITE_LOG.get();
                slab = Slabs.MESQUITE_SLAB.get();
            }
        }
        else if(biome.is(Biomes.BIRCH_FOREST) || biome.is(Biomes.OLD_GROWTH_BIRCH_FOREST) || biome.is(PMBiomes.UNDERGROUND_BIRCH_FOREST)) {
            fence = Blocks.BIRCH_FENCE;
            fenceGate = Blocks.BIRCH_FENCE_GATE;
            craftingTable = Tables.BIRCH_CRAFTING_TABLE.get();
            log = Blocks.BIRCH_LOG;
            plate = Blocks.BIRCH_PRESSURE_PLATE;
            planks = Blocks.BIRCH_PLANKS;
            stairs = Blocks.BIRCH_STAIRS;
            door = Blocks.BIRCH_DOOR;
            chest = QuarkBlocks.BIRCH_CHEST;
            trapdoor = Blocks.BIRCH_TRAPDOOR;
            bookShelf = QuarkBlocks.BIRCH_BOOKSHELF;
            strippedLog = Blocks.STRIPPED_BIRCH_LOG;
            slab = Blocks.BIRCH_SLAB;
        }
        else if(biome.is(PMBiomes.WEEPING_FOREST) || biome.is(PMBiomes.FROZEN_WEEPING_FOREST)) {
            fence = Blocks.SPRUCE_FENCE;
            fenceGate = Blocks.SPRUCE_FENCE_GATE;
            craftingTable = Tables.SPRUCE_CRAFTING_TABLE.get();
            log = Blocks.SPRUCE_LOG;
            plate = Blocks.SPRUCE_PRESSURE_PLATE;
            planks = Blocks.SPRUCE_PLANKS;
            stairs = Blocks.SPRUCE_STAIRS;
            door = Blocks.SPRUCE_DOOR;
            chest = QuarkBlocks.SPRUCE_CHEST;
            trapdoor = Blocks.SPRUCE_TRAPDOOR;
            bookShelf = QuarkBlocks.SPRUCE_BOOKSHELF;
            strippedLog = Blocks.STRIPPED_SPRUCE_LOG;
            slab = Blocks.SPRUCE_SLAB;

            if(rand.nextBoolean()) {
                fence = Blocks.OAK_FENCE;
                fenceGate = Blocks.OAK_FENCE_GATE;
                craftingTable = Blocks.CRAFTING_TABLE;
                log = Blocks.OAK_LOG;
                plate = Blocks.OAK_PRESSURE_PLATE;
                planks = Blocks.OAK_PLANKS;
                stairs = Blocks.OAK_STAIRS;
                door = Blocks.OAK_DOOR;
                chest = QuarkBlocks.OAK_CHEST;
                trapdoor = Blocks.OAK_TRAPDOOR;
                bookShelf = Blocks.BOOKSHELF;
                strippedLog = Blocks.STRIPPED_OAK_LOG;
                slab = Blocks.OAK_SLAB;
            }
        }
        else if(biome.is(Tags.Biomes.IS_CONIFEROUS) || biome.is(Tags.Biomes.IS_COLD) || biome.is(Tags.Biomes.IS_SNOWY)) {
            fence = Blocks.SPRUCE_FENCE;
            fenceGate = Blocks.SPRUCE_FENCE_GATE;
            craftingTable = Tables.SPRUCE_CRAFTING_TABLE.get();
            log = Blocks.SPRUCE_LOG;
            plate = Blocks.SPRUCE_PRESSURE_PLATE;
            planks = Blocks.SPRUCE_PLANKS;
            stairs = Blocks.SPRUCE_STAIRS;
            door = Blocks.SPRUCE_DOOR;
            chest = QuarkBlocks.SPRUCE_CHEST;
            trapdoor = Blocks.SPRUCE_TRAPDOOR;
            bookShelf = QuarkBlocks.SPRUCE_BOOKSHELF;
            strippedLog = Blocks.STRIPPED_SPRUCE_LOG;
            slab = Blocks.SPRUCE_SLAB;
        }
        else if(biome.is(PMBiomes.TEMPERATE_RAINFOREST) || biome.is(PMBiomes.UNDERGROUND_TEMPERATE_RAINFOREST)) {
            switch (rand.nextInt(4)) {
                case 1 -> {
                    fence = Blocks.BIRCH_FENCE;
                    fenceGate = Blocks.BIRCH_FENCE_GATE;
                    craftingTable = Tables.BIRCH_CRAFTING_TABLE.get();
                    log = Blocks.BIRCH_LOG;
                    plate = Blocks.BIRCH_PRESSURE_PLATE;
                    planks = Blocks.BIRCH_PLANKS;
                    stairs = Blocks.BIRCH_STAIRS;
                    door = Blocks.BIRCH_DOOR;
                    chest = QuarkBlocks.BIRCH_CHEST;
                    trapdoor = Blocks.BIRCH_TRAPDOOR;
                    bookShelf = QuarkBlocks.BIRCH_BOOKSHELF;
                    strippedLog = Blocks.STRIPPED_BIRCH_LOG;
                    slab = Blocks.BIRCH_SLAB;
                }

                case 2 -> {
                    fence = Blocks.JUNGLE_FENCE;
                    fenceGate = Blocks.JUNGLE_FENCE_GATE;
                    craftingTable = Tables.JUNGLE_CRAFTING_TABLE.get();
                    log = Blocks.JUNGLE_LOG;
                    plate = Blocks.JUNGLE_PRESSURE_PLATE;
                    planks = Blocks.JUNGLE_PLANKS;
                    stairs = Blocks.JUNGLE_STAIRS;
                    door = Blocks.JUNGLE_DOOR;
                    chest = QuarkBlocks.JUNGLE_CHEST;
                    trapdoor = Blocks.JUNGLE_TRAPDOOR;
                    bookShelf = QuarkBlocks.JUNGLE_BOOKSHELF;
                    strippedLog = Blocks.STRIPPED_JUNGLE_LOG;
                    slab = Blocks.JUNGLE_SLAB;
                }

                case 3 -> {
                    fence = Blocks.SPRUCE_FENCE;
                    fenceGate = Blocks.SPRUCE_FENCE_GATE;
                    craftingTable = Tables.SPRUCE_CRAFTING_TABLE.get();
                    log = Blocks.SPRUCE_LOG;
                    plate = Blocks.SPRUCE_PRESSURE_PLATE;
                    planks = Blocks.SPRUCE_PLANKS;
                    stairs = Blocks.SPRUCE_STAIRS;
                    door = Blocks.SPRUCE_DOOR;
                    chest = QuarkBlocks.SPRUCE_CHEST;
                    trapdoor = Blocks.SPRUCE_TRAPDOOR;
                    bookShelf = QuarkBlocks.SPRUCE_BOOKSHELF;
                    strippedLog = Blocks.STRIPPED_SPRUCE_LOG;
                    slab = Blocks.SPRUCE_SLAB;
                }
            }
        }
        else if(biome.is(BiomeTags.IS_JUNGLE)) {
            fence = Blocks.JUNGLE_FENCE;
            fenceGate = Blocks.JUNGLE_FENCE_GATE;
            craftingTable = Tables.JUNGLE_CRAFTING_TABLE.get();
            log = Blocks.JUNGLE_LOG;
            plate = Blocks.JUNGLE_PRESSURE_PLATE;
            planks = Blocks.JUNGLE_PLANKS;
            stairs = Blocks.JUNGLE_STAIRS;
            door = Blocks.JUNGLE_DOOR;
            chest = QuarkBlocks.JUNGLE_CHEST;
            trapdoor = Blocks.JUNGLE_TRAPDOOR;
            bookShelf = QuarkBlocks.JUNGLE_BOOKSHELF;
            strippedLog = Blocks.STRIPPED_JUNGLE_LOG;
            slab = Blocks.JUNGLE_SLAB;

            if(biome.is(Biomes.BAMBOO_JUNGLE) && rand.nextBoolean()) {
                fence = Blocks.BAMBOO_FENCE;
                fenceGate = Blocks.BAMBOO_FENCE_GATE;
                craftingTable = Tables.BAMBOO_CRAFTING_TABLE.get();
                log = Blocks.BAMBOO_PLANKS;
                plate = Blocks.BAMBOO_PRESSURE_PLATE;
                planks = Blocks.BAMBOO_PLANKS;
                stairs = Blocks.BAMBOO_STAIRS;
                door = Blocks.BAMBOO_DOOR;
                chest = Chests.BAMBOO_CHEST;
                trapdoor = Blocks.BAMBOO_TRAPDOOR;
                bookShelf = Decoration.BAMBOO_BOOKSHELF.get();
                strippedLog = Blocks.BAMBOO_PLANKS;
                slab = Blocks.BAMBOO_SLAB;
            }
        }
        else if(biome.is(BiomeTags.IS_SAVANNA)) {
            fence = Blocks.ACACIA_FENCE;
            fenceGate = Blocks.ACACIA_FENCE_GATE;
            craftingTable = Tables.ACACIA_CRAFTING_TABLE.get();
            log = Blocks.ACACIA_LOG;
            plate = Blocks.ACACIA_PRESSURE_PLATE;
            planks = Blocks.ACACIA_PLANKS;
            stairs = Blocks.ACACIA_STAIRS;
            door = Blocks.ACACIA_DOOR;
            chest = QuarkBlocks.ACACIA_CHEST;
            trapdoor = Blocks.ACACIA_TRAPDOOR;
            bookShelf = QuarkBlocks.ACACIA_BOOKSHELF;
            strippedLog = Blocks.STRIPPED_ACACIA_LOG;
            slab = Blocks.ACACIA_SLAB;
        }
        else if(biome.is(PMBiomes.AUTUMN_FOREST) || biome.is(PMBiomes.UNDERGROUND_AUTUMN_FOREST)) {
            switch (rand.nextInt(3)) {
                case 1 -> {
                    fence = Blocks.BIRCH_FENCE;
                    fenceGate = Blocks.BIRCH_FENCE_GATE;
                    craftingTable = Tables.BIRCH_CRAFTING_TABLE.get();
                    log = Blocks.BIRCH_LOG;
                    plate = Blocks.BIRCH_PRESSURE_PLATE;
                    planks = Blocks.BIRCH_PLANKS;
                    stairs = Blocks.BIRCH_STAIRS;
                    door = Blocks.BIRCH_DOOR;
                    chest = QuarkBlocks.BIRCH_CHEST;
                    trapdoor = Blocks.BIRCH_TRAPDOOR;
                    bookShelf = QuarkBlocks.BIRCH_BOOKSHELF;
                    strippedLog = Blocks.STRIPPED_BIRCH_LOG;
                    slab = Blocks.BIRCH_SLAB;
                }

                case 2 -> {
                    fence = Blocks.SPRUCE_FENCE;
                    fenceGate = Blocks.SPRUCE_FENCE_GATE;
                    craftingTable = Tables.SPRUCE_CRAFTING_TABLE.get();
                    log = Blocks.SPRUCE_LOG;
                    plate = Blocks.SPRUCE_PRESSURE_PLATE;
                    planks = Blocks.SPRUCE_PLANKS;
                    stairs = Blocks.SPRUCE_STAIRS;
                    door = Blocks.SPRUCE_DOOR;
                    chest = QuarkBlocks.SPRUCE_CHEST;
                    trapdoor = Blocks.SPRUCE_TRAPDOOR;
                    bookShelf = QuarkBlocks.SPRUCE_BOOKSHELF;
                    strippedLog = Blocks.STRIPPED_SPRUCE_LOG;
                    slab = Blocks.SPRUCE_SLAB;
                }
            }
        }
        else if(biome.is(PMTags.Biomes.DEEP_DARK)) {
            fence = Fences.BLACKENED_OAK_FENCE.get();
            fenceGate = FenceGates.BLACKENED_OAK_FENCE_GATE.get();
            craftingTable = Tables.BLACKENED_OAK_CRAFTING_TABLE.get();
            log = DeepDarkBlocks.BLACKENED_OAK_LOG.get();
            plate = Plates.BLACKENED_OAK_PRESSURE_PLATE.get();
            planks = DeepDarkBlocks.BLACKENED_OAK_PLANKS.get();
            stairs = Stairs.BLACKENED_OAK_STAIRS.get();
            door = Doors.BLACKENED_OAK_DOOR.get();
            chest = Chests.BLACKENED_OAK_CHEST;
            trapdoor = Trapdoors.BLACKENED_OAK_TRAPDOOR.get();
            bookShelf = Decoration.BLACKENED_OAK_BOOKSHELF.get();
            strippedLog = DeepDarkBlocks.STRIPPED_BLACKENED_OAK_LOG.get();
            slab = Slabs.BLACKENED_OAK_SLAB.get();

            if(biome.is(PMBiomes.TAIGA_INSOMNIUM)) {
                fence = Fences.BLACKENED_SPRUCE_FENCE.get();
                fenceGate = FenceGates.BLACKENED_SPRUCE_FENCE_GATE.get();
                craftingTable = Tables.BLACKENED_SPRUCE_CRAFTING_TABLE.get();
                log = DeepDarkBlocks.BLACKENED_SPRUCE_LOG.get();
                plate = Plates.BLACKENED_SPRUCE_PRESSURE_PLATE.get();
                planks = DeepDarkBlocks.BLACKENED_SPRUCE_PLANKS.get();
                stairs = Stairs.BLACKENED_SPRUCE_STAIRS.get();
                door = Doors.BLACKENED_SPRUCE_DOOR.get();
                chest = Chests.BLACKENED_SPRUCE_CHEST;
                trapdoor = Trapdoors.BLACKENED_SPRUCE_TRAPDOOR.get();
                bookShelf = Decoration.BLACKENED_SPRUCE_BOOKSHELF.get();
                strippedLog = DeepDarkBlocks.STRIPPED_BLACKENED_SPRUCE_LOG.get();
                slab = Slabs.BLACKENED_SPRUCE_SLAB.get();
            }
            else if(biome.is(PMBiomes.GLOWING_FOREST) || biome.is(PMBiomes.CRYSTAL_FOREST)) {
                fence = Fences.GLOWING_OAK_FENCE.get();
                fenceGate = FenceGates.GLOWING_OAK_FENCE_GATE.get();
                craftingTable = Tables.GLOWING_OAK_CRAFTING_TABLE.get();
                log = DeepDarkBlocks.GLOWING_OAK_LOG.get();
                plate = Plates.GLOWING_OAK_PRESSURE_PLATE.get();
                planks = DeepDarkBlocks.GLOWING_OAK_PLANKS.get();
                stairs = Stairs.GLOWING_OAK_STAIRS.get();
                door = Doors.GLOWING_OAK_DOOR.get();
                chest = Chests.GLOWING_OAK_CHEST;
                trapdoor = Trapdoors.GLOWING_OAK_TRAPDOOR.get();
                bookShelf = Decoration.GLOWING_OAK_BOOKSHELF.get();
                strippedLog = DeepDarkBlocks.STRIPPED_BLACKENED_OAK_LOG.get();
                slab = Slabs.GLOWING_OAK_SLAB.get();
            }
            else if(biome.is(PMBiomes.DARK_DESERT) || biome.is(PMBiomes.DARK_VOLCANIC_FIELD)) {
                fence = Fences.GLOWING_CACTUS_FENCE.get();
                fenceGate = FenceGates.GLOWING_CACTUS_FENCE_GATE.get();
                craftingTable = Tables.GLOWING_CACTUS_CRAFTING_TABLE.get();
                log = DeepDarkBlocks.GLOWING_CACTUS_BLOCK.get();
                plate = Plates.GLOWING_CACTUS_PRESSURE_PLATE.get();
                planks = DeepDarkBlocks.GLOWING_CACTUS_BLOCK.get();
                stairs = Stairs.GLOWING_CACTUS_STAIRS.get();
                door = Doors.GLOWING_CACTUS_DOOR.get();
                chest = Chests.GLOWING_CACTUS_CHEST;
                trapdoor = Trapdoors.GLOWING_CACTUS_TRAPDOOR.get();
                bookShelf = Decoration.GLOWING_CACTUS_BOOKSHELF.get();
                strippedLog = DeepDarkBlocks.GLOWING_CACTUS_BLOCK.get();
                slab = Slabs.GLOWING_CACTUS_SLAB.get();
            }
        }
        else if(biome.is(Tags.Biomes.IS_DRY) && !biome.is(BiomeTags.IS_BADLANDS)) {
            fence = Fences.CACTUS_FENCE.get();
            fenceGate = FenceGates.CACTUS_FENCE_GATE.get();
            craftingTable = Tables.CACTUS_CRAFTING_TABLE.get();
            log = Building.CACTUS_BLOCK.get();
            plate = Plates.CACTUS_PRESSURE_PLATE.get();
            planks = Building.CACTUS_BLOCK.get();
            stairs = Stairs.CACTUS_STAIRS.get();
            door = Doors.CACTUS_DOOR.get();
            chest = Chests.CACTUS_CHEST;
            trapdoor = Trapdoors.CACTUS_TRAPDOOR.get();
            bookShelf = Decoration.CACTUS_BOOKSHELF.get();
            strippedLog = Building.CACTUS_BLOCK.get();
            slab = Slabs.CACTUS_SLAB.get();
        }

        return List.of(
            fence, // 0
            fenceGate, // 1
            craftingTable, // 2
            log, // 3
            plate, // 4
            planks, // 5
            stairs, // 6
            door, // 7
            chest, // 8
            trapdoor, // 9
            bookShelf, // 10
            strippedLog, // 11
            slab // 12
        );
    }

    public static List<Block> getColoredBlocks(RandomSource rand, Holder<Biome> biome) {
        var roseFieldBlocks = getRoseColoredBlocks(PMBiomes.ROSE_FIELDS_BY_COLOR, biome);
        var roseLandBlocks = getRoseColoredBlocks(PMBiomes.ROSE_LANDS_BY_COLOR, biome);
        if(roseFieldBlocks.size() > 0) return roseFieldBlocks;
        else if(roseLandBlocks.size() > 0) return roseLandBlocks;

        var wool = Blocks.RED_WOOL;
        var lantern = Blocks.LANTERN;
        var lamp = Blocks.REDSTONE_LAMP;
        var bed = Blocks.RED_BED;

        if(biome.is(PMBiomes.SILVA_INSOMNIUM) || biome.is(PMBiomes.TAIGA_INSOMNIUM)) {
            wool = Blocks.BLACK_WOOL;
            lantern = ColoredLanterns.BLACK_LANTERN.get();
            lamp = Lamps.BLACK_REDSTONE_LAMP.get();
            bed = Blocks.BLACK_BED;
        }
        else if(biome.is(PMBiomes.GLOWING_FOREST) || biome.is(PMBiomes.CRYSTAL_FOREST)) {
            wool = Blocks.LIGHT_GRAY_WOOL;
            lantern = ColoredLanterns.LIGHT_GRAY_LANTERN.get();
            lamp = Lamps.LIGHT_GRAY_REDSTONE_LAMP.get();
            bed = Blocks.LIGHT_GRAY_BED;
        }
        else if(biome.is(Biomes.DARK_FOREST) || biome.is(PMBiomes.UNDERGROUND_DARK_FOREST)) {
            wool = Blocks.BLUE_WOOL;
            lantern = ColoredLanterns.BLUE_LANTERN.get();
            lamp = Lamps.BLUE_REDSTONE_LAMP.get();
            bed = Blocks.BLUE_BED;
        }
        else if(biome.is(BiomeTags.IS_BADLANDS) || biome.is(PMTags.Biomes.ROCKY_DESERTS) || biome.is(PMTags.Biomes.COLD_ROCKY_DESERTS)) {
            wool = Blocks.BROWN_WOOL;
            lantern = ColoredLanterns.BROWN_LANTERN.get();
            lamp = Lamps.BROWN_REDSTONE_LAMP.get();
            bed = Blocks.BROWN_BED;
        }
        else if(
            biome.is(Biomes.BIRCH_FOREST) ||
            biome.is(Biomes.OLD_GROWTH_BIRCH_FOREST) ||
            biome.is(PMBiomes.UNDERGROUND_BIRCH_FOREST)
        ) {
            wool = Blocks.CYAN_WOOL;
            lantern = ColoredLanterns.CYAN_LANTERN.get();
            lamp = Lamps.CYAN_REDSTONE_LAMP.get();
            bed = Blocks.CYAN_BED;
        }
        else if(
            biome.is(Tags.Biomes.IS_SWAMP) ||
            biome.is(PMBiomes.TEMPERATE_RAINFOREST) ||
            biome.is(PMBiomes.UNDERGROUND_TEMPERATE_RAINFOREST)
        ) {
            wool = Blocks.GREEN_WOOL;
            lantern = ColoredLanterns.GREEN_LANTERN.get();
            lamp = Lamps.GREEN_REDSTONE_LAMP.get();
            bed = Blocks.GREEN_BED;
        }
        else if(biome.is(Tags.Biomes.IS_CONIFEROUS) || biome.is(Tags.Biomes.IS_COLD) || biome.is(Tags.Biomes.IS_SNOWY)) {
            wool = Blocks.LIGHT_BLUE_WOOL;
            lantern = ColoredLanterns.LIGHT_BLUE_LANTERN.get();
            lamp = Lamps.LIGHT_BLUE_REDSTONE_LAMP.get();
            bed = Blocks.LIGHT_BLUE_BED;
        }
        else if(biome.is(BiomeTags.IS_JUNGLE)) {
            wool = Blocks.LIME_WOOL;
            lantern = ColoredLanterns.LIME_LANTERN.get();
            lamp = Lamps.LIME_REDSTONE_LAMP.get();
            bed = Blocks.LIME_BED;
        }
        else if(biome.is(BiomeTags.IS_SAVANNA)) {
            wool = Blocks.ORANGE_WOOL;
            lantern = ColoredLanterns.ORANGE_LANTERN.get();
            lamp = Lamps.ORANGE_REDSTONE_LAMP.get();
            bed = Blocks.ORANGE_BED;
        }
        else if(biome.is(Tags.Biomes.IS_DRY)) {
            wool = Blocks.YELLOW_WOOL;
            lantern = ColoredLanterns.YELLOW_LANTERN.get();
            lamp = Lamps.YELLOW_REDSTONE_LAMP.get();
            bed = Blocks.YELLOW_BED;
        }
        else if(biome.is(Biomes.CHERRY_GROVE) || biome.is(PMBiomes.UNDERGROUND_CHERRY_FOREST)) {
            wool = Blocks.PINK_WOOL;
            lantern = ColoredLanterns.PINK_LANTERN.get();
            lamp = Lamps.PINK_REDSTONE_LAMP.get();
            bed = Blocks.PINK_BED;
        }
        else if(biome.is(PMBiomes.WEEPING_FOREST) || biome.is(PMBiomes.FROZEN_WEEPING_FOREST)) {
            wool = Blocks.GRAY_WOOL;
            lantern = ColoredLanterns.GRAY_LANTERN.get();
            lamp = Lamps.GRAY_REDSTONE_LAMP.get();
            bed = Blocks.GRAY_BED;
        }
        else if(biome.is(PMBiomes.AUTUMN_FOREST) || biome.is(PMBiomes.UNDERGROUND_AUTUMN_FOREST)) {
            switch(rand.nextInt(4)) {
                case 0 -> {
                    wool = Blocks.YELLOW_WOOL;
                    lantern = ColoredLanterns.YELLOW_LANTERN.get();
                    lamp = Lamps.YELLOW_REDSTONE_LAMP.get();
                    bed = Blocks.YELLOW_BED;
                }

                case 1 -> {
                    wool = Blocks.ORANGE_WOOL;
                    lantern = ColoredLanterns.ORANGE_LANTERN.get();
                    lamp = Lamps.ORANGE_REDSTONE_LAMP.get();
                    bed = Blocks.ORANGE_BED;
                }

                case 2 -> {
                    lantern = ColoredLanterns.RED_LANTERN.get();
                    lamp = Lamps.RED_REDSTONE_LAMP.get();
                }

                case 3 -> {
                    wool = Blocks.BLUE_WOOL;
                    lantern = ColoredLanterns.BLUE_LANTERN.get();
                    lamp = Lamps.BLUE_REDSTONE_LAMP.get();
                    bed = Blocks.BLUE_BED;
                }
            }
        }

        return List.of(
            wool, // 0
            lantern, // 1
            lamp, // 2
            bed // 3
        );
    }

    private static List<Block> getRoseColoredBlocks(EnumMap<DyeColor, ResourceKey<Biome>> roseBiomes, Holder<Biome> biome) {
        var wool = Blocks.RED_WOOL;
        var lantern = Blocks.LANTERN;
        var lamp = Blocks.REDSTONE_LAMP;
        var bed = Blocks.RED_BED;
        

        for(var entry : roseBiomes.entrySet()) {
            var color = entry.getKey();
            var roseBiome = entry.getValue();

            if(biome.is(roseBiome)) {
                lamp = Lamps.REDSTONE_LAMPS.get(color).get();
                lantern = ColoredLanterns.LANTERNS.get(color).get();
                switch(color) {
                    case WHITE -> {
                        wool = Blocks.WHITE_WOOL;
                        bed = Blocks.WHITE_BED;
                    }

                    case ORANGE -> {
                        wool = Blocks.ORANGE_WOOL;
                        bed = Blocks.ORANGE_BED;
                    }

                    case MAGENTA -> {
                        wool = Blocks.MAGENTA_WOOL;
                        bed = Blocks.MAGENTA_BED;
                    }

                    case LIGHT_BLUE -> {
                        wool = Blocks.LIGHT_BLUE_WOOL;
                        bed = Blocks.LIGHT_BLUE_BED;
                    }

                    case YELLOW -> {
                        wool = Blocks.YELLOW_WOOL;
                        bed = Blocks.YELLOW_BED;
                    }

                    case LIME -> {
                        wool = Blocks.LIME_WOOL;
                        bed = Blocks.LIME_BED;
                    }

                    case PINK -> {
                        wool = Blocks.PINK_WOOL;
                        bed = Blocks.PINK_BED;
                    }

                    case GRAY -> {
                        wool = Blocks.GRAY_WOOL;
                        bed = Blocks.GRAY_BED;
                    }

                    case LIGHT_GRAY -> {
                        wool = Blocks.LIGHT_GRAY_WOOL;
                        bed = Blocks.LIGHT_GRAY_BED;
                    }

                    case CYAN -> {
                        wool = Blocks.CYAN_WOOL;
                        bed = Blocks.CYAN_BED;
                    }

                    case PURPLE -> {
                        wool = Blocks.PURPLE_WOOL;
                        bed = Blocks.PURPLE_BED;
                    }

                    case BLUE -> {
                        wool = Blocks.BLUE_WOOL;
                        bed = Blocks.BLUE_BED;
                    }

                    case BROWN -> {
                        wool = Blocks.BROWN_WOOL;
                        bed = Blocks.BROWN_BED;
                    }

                    case GREEN -> {
                        wool = Blocks.GREEN_WOOL;
                        bed = Blocks.GREEN_BED;
                    }

                    case BLACK -> {
                        wool = Blocks.BLACK_WOOL;
                        bed = Blocks.BLACK_BED;
                    }
                }

                return List.of(wool, lantern, lamp, bed);
            }
        }

        return List.of();
    }
}