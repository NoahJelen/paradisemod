package net.paradisemod.worldgen.features;

import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.paradisemod.bonus.Bonus;
import net.paradisemod.world.PMWorld;
import net.paradisemod.worldgen.features.foliage.PMFoliage;

public class FullChristmasTree extends BasicFeature {
    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        var curState = world.getBlockState(pos);
        var stateBelow = world.getBlockState(pos.below());

        if((curState.isAir() || curState.is(Blocks.SNOW)) && stateBelow.is(BlockTags.DIRT)) {
            var grown = getPlacedFeature(PMFoliage.XMAS_PLACER)
                .place(world, generator, rand, pos);

            if(grown) {
                var presentRolls = 3 + rand.nextInt(3);
                for(int i = 0; i <= presentRolls; i++) {
                    var newPos = pos.offset(rand.nextInt(-3, 3), 0, rand.nextInt(-3, 3));
                    var y = PMWorld.getGroundLevel(world, -54, newPos.getY() + 12, newPos);

                    if (
                        y.isEmpty() ||
                        (newPos.getX() == pos.getX() && newPos.getZ() == pos.getZ())
                    ) continue;

                    world.setBlock(newPos.atY(y.getAsInt() + 1), Bonus.PRESENT.get().defaultBlockState(), 32);
                }

                var topY = PMWorld.getGroundLevel(world, 0, pos.getY() + 12, pos, Bonus.CHRISTMAS_LEAVES.get());
                if(topY.isPresent()){
                    var tops = List.of(Bonus.GOLD_CHRISTMAS_TREE_TOP.get(), Bonus.SILVER_CHRISTMAS_TREE_TOP.get(), Bonus.COPPER_CHRISTMAS_TREE_TOP.get());
                    world.setBlock(pos.atY(topY.getAsInt() + 1), tops.get(rand.nextInt(tops.size())).defaultBlockState(), 32);
                }
            }
        }

        return true;
    }
}