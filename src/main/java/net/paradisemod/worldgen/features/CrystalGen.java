package net.paradisemod.worldgen.features;

import java.util.ArrayList;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.paradisemod.base.PMConfig;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.world.CrystalClusters;
import net.paradisemod.world.biome.PMBiomes;
import net.paradisemod.world.blocks.CrystalCluster;

public class CrystalGen extends BasicFeature {
    private final Type type;
    public CrystalGen(Type type) { this.type = type; }

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        if(!type.shouldGenerate(rand)) return false;
        var crystals = Utils.getBlockTag(PMTags.Blocks.CRYSTAL_CLUSTERS);
        var crystal = (CrystalCluster) CrystalClusters.DIAMOND_CRYSTAL_CLUSTER.get();

        var dimName = world.getLevel().dimension().location().toString();

        CrystalCluster[] endCrystals = {
            (CrystalCluster) CrystalClusters.EMERALD_CRYSTAL_CLUSTER.get(),
            (CrystalCluster) CrystalClusters.RUBY_CRYSTAL_CLUSTER.get(),
            (CrystalCluster) CrystalClusters.ENDER_PEARL_CLUSTER.get()
        };

        if (type == Type.RANDOM) crystal = (CrystalCluster) crystals.getRandomElement(rand).get();
        else if (type == Type.END_RANDOM) crystal = endCrystals[rand.nextInt(endCrystals.length)];

        var sizeXZ = 16 + rand.nextInt(8);
        var sizeY = 20 + rand.nextInt(10);

        var height = 62;
        if(dimName.equals("paradisemod:overworld_core") || type == Type.HONEY) height = 317;
        else if(dimName.equals("minecraft:the_nether") || dimName.equals("minecraft:the_end")) height = 126;
        for(var i = 0; i < sizeXZ; i++) {
            for(var j = 0; j < sizeXZ; j++) {
                for(var k = 0; k < sizeY; k++) {
                    if(rand.nextInt(5) == 0) {
                        crystal = switch (type) {
                            case SALT -> (CrystalCluster) CrystalClusters.SALT_CRYSTAL_CLUSTER.get();
                            case MIXED -> (CrystalCluster) crystals.getRandomElement(rand).get();
                            case QUARTZ -> (CrystalCluster) CrystalClusters.QUARTZ_CRYSTAL_CLUSTER.get();
                            case HONEY -> (CrystalCluster) CrystalClusters.HONEY_CRYSTAL_CLUSTER.get();
                            case END_MIXED -> endCrystals[rand.nextInt(endCrystals.length)];
                            default -> crystal;
                        };

                        var newPos = pos.offset(i, -k, j);
                        var biome = world.getBiome(newPos);
                        if(newPos.getY() > height || (type == Type.HONEY && !biome.is(PMBiomes.HONEY_CAVE)))
                            continue;

                        genCrystal(rand, world, newPos, crystal);
                    }
                }
            }
        }

        return true;
    }

    private static void genCrystal(RandomSource rand, WorldGenLevel world, BlockPos pos, CrystalCluster crystal) {
        if(world.getBlockState(pos).isAir() || world.getBlockState(pos).is(Blocks.WATER)) {
            if (rand.nextInt(3) != 0) return;
            ArrayList<Direction> validDirs = new ArrayList<>();

            for(var facing : Direction.values()) {
                var relState = world.getBlockState(pos.relative(facing));
                if (
                    (relState.isSolid() || relState.is(Blocks.HONEY_BLOCK)) &&
                    !relState.is(Blocks.SNOW)
                )
                    validDirs.add(facing);
            }

            if (validDirs.isEmpty()) return;
            var toPlaceDir = validDirs.get(rand.nextInt(validDirs.size()));
            var waterlogged = world.getBlockState(pos).is(Blocks.WATER);
            world.setBlock(pos, crystal.defaultBlockState().setValue(CrystalCluster.TYPE, rand.nextInt(4)).setValue(CrystalCluster.FACING, toPlaceDir.getOpposite()).setValue(CrystalCluster.WATERLOGGED, waterlogged), 1);
        }
    }

    public enum Type {
        SALT,
        QUARTZ,
        HONEY,
        RANDOM,
        MIXED,
        END_RANDOM,
        END_MIXED;

        public boolean shouldGenerate(RandomSource rand) {
            if(this == HONEY) return rand.nextInt(5) == 0;

            var settings = switch (this) {
                case END_MIXED, END_RANDOM -> PMConfig.SETTINGS.betterEnd.endCrystals;
                default -> PMConfig.SETTINGS.caveCrystals;
            };

            return settings.shouldGenerate(rand);
        }
    }
}