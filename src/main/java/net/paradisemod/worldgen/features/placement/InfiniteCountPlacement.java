package net.paradisemod.worldgen.features.placement;

import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.util.valueproviders.ConstantInt;
import net.minecraft.util.valueproviders.IntProvider;
import net.minecraft.world.level.levelgen.placement.PlacementModifierType;
import net.minecraft.world.level.levelgen.placement.RepeatingPlacement;
import net.paradisemod.worldgen.features.PMFeatures;

/** Acts exactly like CountPlacement, but doesn't error out when a count greater than 256 is specified */
public class InfiniteCountPlacement extends RepeatingPlacement {
    public static final Codec<InfiniteCountPlacement> CODEC = IntProvider.codec(0, Integer.MAX_VALUE)
        .fieldOf("count")
        .xmap(InfiniteCountPlacement::new, placement -> placement.count)
        .codec();

    private final IntProvider count;

    private InfiniteCountPlacement(IntProvider count) {
        this.count = count;
    }

    public static InfiniteCountPlacement of(IntProvider count) {
        return new InfiniteCountPlacement(count);
    }

    public static InfiniteCountPlacement of(int pCount) {
        return of(ConstantInt.of(pCount));
    }

    @Override
    protected int count(RandomSource rand, BlockPos pos) {
        return this.count.sample(rand);
    }

    @Override
    public PlacementModifierType<InfiniteCountPlacement> type() {
        return PMFeatures.INFINITE_COUNT.get();
    }
}