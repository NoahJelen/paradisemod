package net.paradisemod.worldgen.features.placement;

import java.util.HashMap;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.levelgen.placement.PlacementContext;
import net.minecraft.world.level.levelgen.placement.PlacementFilter;
import net.minecraft.world.level.levelgen.placement.PlacementModifierType;
import net.minecraftforge.common.ForgeConfigSpec;
import net.paradisemod.base.PMConfig;
import net.paradisemod.worldgen.features.PMFeatures;

public class ConfiguredOrePlacement extends PlacementFilter {
    private static final HashMap<String, ForgeConfigSpec.BooleanValue> CONFIG_KEYS = new HashMap<>();

    static {
        CONFIG_KEYS.put("overworld", PMConfig.SETTINGS.ores.overworld);
        CONFIG_KEYS.put("netherSilver", PMConfig.SETTINGS.ores.netherSilver);
        CONFIG_KEYS.put("end", PMConfig.SETTINGS.ores.end);
        CONFIG_KEYS.put("deepDark", PMConfig.SETTINGS.ores.deepDark);
    }

    public static final Codec<ConfiguredOrePlacement> CODEC = RecordCodecBuilder.create(
        builder -> builder.group(
            Codec.STRING.fieldOf("key")
                .stable()
                .forGetter(placement -> placement.configKey)
        )
            .apply(builder, ConfiguredOrePlacement::new)
    );

    private final String configKey;

    public ConfiguredOrePlacement(String configKey) {
        this.configKey = configKey;
    }

    @Override
    protected boolean shouldPlace(PlacementContext pContext, RandomSource pRandom, BlockPos pPos) {
        return CONFIG_KEYS.get(configKey).get();
    }

    @Override
    public PlacementModifierType<ConfiguredOrePlacement> type() {
        return PMFeatures.CONFIGURED_ORE.get();
    }
}