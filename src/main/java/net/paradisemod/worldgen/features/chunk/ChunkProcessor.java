package net.paradisemod.worldgen.features.chunk;

import java.util.ArrayList;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.paradisemod.world.dimension.PMDimensions;

public abstract class ChunkProcessor {
    protected static final ArrayList<ChunkProcessor> RAW_PROCESSORS = new ArrayList<>();
    protected static final ArrayList<ChunkProcessor> VEGETAL_PROCESSORS = new ArrayList<>();

    protected abstract void processBlock(WorldGenLevel world, RandomSource rand, BlockPos pos, BlockState curState, Holder<Biome> biome, ChunkGenerator generator);

    protected int maxHeight(WorldGenLevel world) { return world.getMaxBuildHeight(); }

    protected int minHeight(WorldGenLevel world) { return world.getMinBuildHeight(); }

    protected boolean isValidWorld(WorldGenLevel world) {
        var dim = world.getLevel().dimension();

        return PMDimensions.isDimOverworldLike(world) ||
            PMDimensions.Type.DEEP_DARK.is(dim);
    }

    public static void addRawProcessor(ChunkProcessor processor) {
        RAW_PROCESSORS.add(processor);
    }

    public static void addVegetalProcessor(ChunkProcessor processor) {
        VEGETAL_PROCESSORS.add(processor);
    }
}