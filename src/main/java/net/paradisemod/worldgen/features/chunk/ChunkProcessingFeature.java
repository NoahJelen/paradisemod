package net.paradisemod.worldgen.features.chunk;

import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.paradisemod.worldgen.features.BasicFeature;

public class ChunkProcessingFeature extends BasicFeature {
    private final boolean isVegetal;

    public ChunkProcessingFeature(boolean isVegetal) {
        this.isVegetal = isVegetal;
    }

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        var chunkX = (pos.getX() >> 4) * 16;
        var chunkZ = (pos.getZ() >> 4) * 16;
        var chunkCorner = new BlockPos(chunkX, 0, chunkZ);

        for(var x = 0; x <= 15; x++)
            for(var z = 0; z <= 15; z++)
                for (int y = world.getMinBuildHeight(); y < world.getMaxBuildHeight(); y++) {
                    var newPos = chunkCorner.offset(x, y, z);
                    var biome = world.getBiome(newPos);
                    var curState = world.getBlockState(newPos);

                    for(var processor : isVegetal ? ChunkProcessor.VEGETAL_PROCESSORS : ChunkProcessor.RAW_PROCESSORS) 
                        if(y > processor.minHeight(world) && y < processor.maxHeight(world) && processor.isValidWorld(world)) 
                            processor.processBlock(world, rand, newPos, curState, biome, generator);
                }

        return true;
    }
}