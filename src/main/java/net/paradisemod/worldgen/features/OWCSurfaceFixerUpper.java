package net.paradisemod.worldgen.features;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.paradisemod.world.Ores;
import net.paradisemod.world.dimension.PMDimensions;
import net.paradisemod.worldgen.features.chunk.ChunkProcessor;

public class OWCSurfaceFixerUpper extends ChunkProcessor {
    @Override
    protected void processBlock(WorldGenLevel world, RandomSource rand, BlockPos pos, BlockState curState, Holder<Biome> biome, ChunkGenerator generator) {
        Block dirt = null;
        if(curState.is(Blocks.GRASS_BLOCK))
            dirt = Blocks.DIRT;
        else if(curState.is(Ores.SALT_BLOCK.get()))
            dirt = Ores.COMPACT_SALT_BLOCK.get();
        else if(curState.is(Blocks.COARSE_DIRT))
            dirt = Blocks.COARSE_DIRT;
        else if(curState.is(Blocks.MUD))
            dirt = Blocks.MUD;

        if(dirt != null) {
            world.setBlock(pos.below(), dirt.defaultBlockState(), 32);
            return;
        }
    }

    @Override
    protected boolean isValidWorld(WorldGenLevel world) {
        var dim = world.getLevel().dimension();

        return PMDimensions.Type.OVERWORLD_CORE.is(dim);
    }
}
