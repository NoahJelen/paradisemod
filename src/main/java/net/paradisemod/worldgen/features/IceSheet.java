package net.paradisemod.worldgen.features;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Holder;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.AirBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.ChestBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.paradisemod.base.registry.RegisteredBlock;
import net.paradisemod.world.PMWorld;
import net.paradisemod.world.blocks.LargeCaveFormation;
import net.paradisemod.world.blocks.SmallCaveFormation;
import net.paradisemod.world.dimension.PMDimensions;
import net.paradisemod.worldgen.features.chunk.ChunkProcessor;

public class IceSheet extends ChunkProcessor {
    private static final List<RegisteredBlock> ICICLES = List.of(PMWorld.ICICLE, PMWorld.BLUE_ICICLE);
    private static final List<Supplier<LargeCaveFormation>> LARGE_ICICLES = List.of(
        () -> (LargeCaveFormation) PMWorld.LARGE_ICICLE.get(),
        () -> (LargeCaveFormation) PMWorld.LARGE_BLUE_ICICLE.get()
    );

    @Nullable
    private final Supplier<Block> top;

    @Nullable
    private final Supplier<Block> middle;
    private final Supplier<Block> bottom;
    private final List<ResourceKey<Biome>> allowedBiomes;

    @SafeVarargs
    public IceSheet(Supplier<Block> top, Supplier<Block> middle, Supplier<Block> bottom, ResourceKey<Biome>... allowedBiomes) {
        this.top = top;
        this.middle = middle;
        this.bottom = bottom;
        this.allowedBiomes = Arrays.asList(allowedBiomes);
    }

    @Override
    protected void processBlock(WorldGenLevel world, RandomSource rand, BlockPos pos, BlockState curState, Holder<Biome> biome, ChunkGenerator generator) {
        var biomeKey = biome.unwrapKey().orElseThrow();

        if(allowedBiomes.contains(biomeKey)) {
            var blockBelow = world.getBlockState(pos.below());
            if(
                (
                    (
                        curState.is(Blocks.AIR) ||
                        curState.getBlock() instanceof LargeCaveFormation ||
                        curState.getBlock() instanceof SmallCaveFormation ||
                        curState.is(Blocks.SNOW)
                    ) &&
                    blockBelow.isSolid() &&
                    !blockBelow.is(Blocks.SNOW) &&
                    !blockBelow.is(Blocks.SNOW_BLOCK) &&
                    !blockBelow.is(BlockTags.FEATURES_CANNOT_REPLACE) &&
                    !(blockBelow.getBlock() instanceof ChestBlock)
                ) ||
                curState.is(Blocks.WATER)
            ) {
                var bound1 = 5 + rand.nextInt(5);
                var bound2 = 15 + rand.nextInt(10);
                for (var i = 0; i < 30; i++) {
                    var placePos = pos.above(i);
                    var blockToPlace = bottom.get().defaultBlockState();
                    if (i > bound2 && top != null)
                        blockToPlace = top.get().defaultBlockState();
                    else if (i > bound1 && middle != null)
                        blockToPlace = middle.get().defaultBlockState();

                    var toReplace = world.getBlockState(placePos).getBlock();

                    if(toReplace == Blocks.CAVE_AIR)
                        continue;
                    else if(
                        toReplace instanceof AirBlock ||
                        toReplace == Blocks.WATER
                    ) {
                        world.setBlock(placePos, blockToPlace, 32);
                        if(world.getBlockState(placePos.below()).is(Blocks.CAVE_AIR) && !blockToPlace.is(Blocks.SNOW_BLOCK)) {
                            if(rand.nextInt(5) == 0)
                                world.setBlock(placePos.below(), ICICLES.get(rand.nextInt(ICICLES.size())).get().defaultBlockState().setValue(SmallCaveFormation.FACING, Direction.DOWN).setValue(SmallCaveFormation.WATERLOGGED, false), 32);
                            else if(rand.nextInt(10) == 0)
                                LARGE_ICICLES.get(rand.nextInt(LARGE_ICICLES.size())).get()
                                    .place(world, placePos.below(), true);
                        }
                        else if(world.getBlockState(placePos.above()).is(Blocks.CAVE_AIR) && !blockToPlace.is(Blocks.SNOW_BLOCK)) {
                            if(rand.nextInt(5) == 0)
                                world.setBlock(placePos.above(), ICICLES.get(rand.nextInt(ICICLES.size())).get().defaultBlockState().setValue(SmallCaveFormation.FACING, Direction.UP).setValue(SmallCaveFormation.WATERLOGGED, false), 32);
                            else if(rand.nextInt(10) == 0)
                                LARGE_ICICLES.get(rand.nextInt(LARGE_ICICLES.size())).get()
                                    .place(world, placePos.below(), false);
                        }
                    }
                    else if(
                        toReplace instanceof LargeCaveFormation ||
                        toReplace instanceof SmallCaveFormation ||
                        toReplace == Blocks.SNOW
                    ) { }
                    else return;
                }

                var blockBelowSnow = world.getBlockState(pos.above(29));
                if(blockBelowSnow.canOcclude())
                    world.setBlock(pos.above(30), Blocks.SNOW.defaultBlockState(), 32);
            }
        }
    }

    @Override
    protected int minHeight(WorldGenLevel world) {
        var dim = world.getLevel().dimension();

        if(PMDimensions.Type.DEEP_DARK.is(dim))
            return world.getMinBuildHeight();
        else return 62;
    }
}