package net.paradisemod.worldgen.structures;

import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.ChestBlock;
import net.minecraft.world.level.block.entity.RandomizableContainerBlockEntity;
import net.minecraft.world.level.block.entity.SpawnerBlockEntity;
import net.minecraft.world.level.block.state.properties.ChestType;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.storage.loot.BuiltInLootTables;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.PMConfig;
import net.paradisemod.misc.Chests;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.worldgen.features.BasicFeature;

public class SmallDarkDungeon extends BasicFeature {
    // possible mobs for mob spawner
    private static final List<EntityType<?>> MOBS = List.of(
        EntityType.ZOMBIE,
        EntityType.CREEPER,
        EntityType.SPIDER,
        EntityType.CAVE_SPIDER,
        EntityType.SKELETON,
        EntityType.WITHER_SKELETON
    );

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        if(
            PMConfig.SETTINGS.structures.smallDarkDungeon.shouldGenerate(rand) &&
            SmallStructure.checkArea(world, pos, new Vec3i(7, 7, 7)) &&
            isUndergroundAreaValid(world, pos, 7, 7, 7)
        ) {
            // generate the walls of the dungeon
            for (var x = 0; x < 7; x++)
                for (var y = 0; y < 7; y++)
                    for (var z = 0; z < 7; z++) {
                        var newPos = pos.offset(x, y, z);
                        var blockstate = world.getBlockState(newPos);
                        if(blockstate.is(Blocks.DEEPSLATE))
                            world.setBlock(newPos, Blocks.DEEPSLATE_BRICKS.defaultBlockState(), 32);
                        else if (!blockstate.isAir())
                            world.setBlock(newPos, DeepDarkBlocks.POLISHED_DARKSTONE_BRICKS.get().defaultBlockState(), 32);
                    }

            for (var x = 1; x < 6; x++)
                for (var y = 1; y < 6; y++)
                    for (var z = 1; z < 6; z++)
                        world.setBlock(pos.offset(x, y, z), Blocks.AIR.defaultBlockState(), 32);

            // bottom platform
            for (var x = 0; x < 7; x++)
                for (var z = 0; z < 7; z++) {
                    var newPos = pos.offset(x, 0, z);
                    var blockstate = world.getBlockState(newPos);
                    if(blockstate.is(Blocks.DEEPSLATE))
                        world.setBlock(newPos, Blocks.DEEPSLATE_BRICKS.defaultBlockState(), 32);
                    else
                        world.setBlock(newPos, DeepDarkBlocks.POLISHED_DARKSTONE_BRICKS.get().defaultBlockState(), 32);
                }

            // chest 1
            var chest1Pos = pos.offset(1, 1, 1);
            world.setBlock(chest1Pos, Chests.BLACKENED_OAK_CHEST.defaultBlockState().setValue(ChestBlock.FACING, Direction.SOUTH).setValue(ChestBlock.TYPE, ChestType.RIGHT), 32);
            RandomizableContainerBlockEntity.setLootTable(world, rand, chest1Pos, BuiltInLootTables.SIMPLE_DUNGEON);

            // chest 2
            var chest2Pos = pos.offset(2, 1, 1);
            world.setBlock(chest2Pos, Chests.BLACKENED_OAK_CHEST.defaultBlockState().setValue(ChestBlock.FACING, Direction.SOUTH).setValue(ChestBlock.TYPE, ChestType.LEFT), 32);
            RandomizableContainerBlockEntity.setLootTable(world, rand, chest2Pos, BuiltInLootTables.SIMPLE_DUNGEON);

            // chest 3
            var chest3Pos = pos.offset(5, 1, 5);
            world.setBlock(chest3Pos, Chests.BLACKENED_OAK_CHEST.defaultBlockState().setValue(ChestBlock.FACING, Direction.WEST).setValue(ChestBlock.TYPE, ChestType.LEFT), 32);
            RandomizableContainerBlockEntity.setLootTable(world, rand, chest3Pos, BuiltInLootTables.SIMPLE_DUNGEON);

            // chest 4
            var chest4Pos = pos.offset(5, 1, 4);
            world.setBlock(chest4Pos, Chests.BLACKENED_OAK_CHEST.defaultBlockState().setValue(ChestBlock.FACING, Direction.WEST).setValue(ChestBlock.TYPE, ChestType.RIGHT), 32);
            RandomizableContainerBlockEntity.setLootTable(world, rand, chest4Pos, BuiltInLootTables.SIMPLE_DUNGEON);

            // mob spawner
            var spawnerPos = pos.offset(3, 1, 3);
            world.setBlock(spawnerPos, Blocks.SPAWNER.defaultBlockState(), 32);
            var tile =  world.getBlockEntity(spawnerPos);
            if (tile instanceof SpawnerBlockEntity spawner)
                spawner.setEntityId(MOBS.get(rand.nextInt(MOBS.size())), rand);
            else ParadiseMod.LOG.error("Failed to fetch mob spawner entity at ({}, {}, {})", spawnerPos.getX(), spawnerPos.getY(), spawnerPos.getZ());

            return true;
        }

        return false;
    }
}