package net.paradisemod.worldgen.structures;

import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Holder;
import net.minecraft.core.Vec3i;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.BedBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.ChestBlock;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.TrapDoorBlock;
import net.minecraft.world.level.block.entity.ChestBlockEntity;
import net.minecraft.world.level.block.state.properties.ChestType;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
import net.minecraftforge.common.Tags;
import net.paradisemod.base.PMConfig;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.world.PMWorld;
import net.paradisemod.world.biome.PMBiomes;

public class Home extends SmallStructure {
    private Direction direction = Direction.EAST;
    private boolean isBalcony = false;

    @Nullable
    private Holder<Biome> biome = null;

    private List<Block> wood = List.of();

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        if(PMConfig.SETTINGS.structures.homes.shouldGenerate(rand)) {
            biome = world.getBiome(pos);
            wood = getWood(rand, biome);

            if(biome.is(Tags.Biomes.IS_WATER) || biome.is(PMTags.Biomes.SALT) || biome.is(PMTags.Biomes.VOLCANIC))
                return false;

            direction = Direction.Plane.HORIZONTAL.getRandomDirection(rand);
            var home = new ResourceLocation("paradisemod:starter_house/" + direction.name().toLowerCase());
            var balcony = new ResourceLocation("paradisemod:starter_house/balcony_" + direction.name().toLowerCase());

            // get home size based on the facing direction
            int[] homeSize = {6, 7};
            if(direction.getAxis() == Direction.Axis.Z) homeSize = new int[] {7, 6};

            // calculate position of the base
            var y = PMWorld.getHighestY(world, pos, pos.offset(homeSize[0], 0, homeSize[1]));
            if (y < 2) return false;
            var homePos = pos.atY(y);

            // calculate balcony position (if it is needed)
            var balconyPos = getBalconyPos(homePos);
            if(!checkArea(world, homePos, new Vec3i(10, 8, 10))) return false;

            isBalcony = needsBalcony(world, balconyPos);

            if(direction == Direction.WEST)
                genStructureFromTemplate(home, world, rand, homePos.east(3), generator, true);
            else if(direction == Direction.NORTH)
                genStructureFromTemplate(home, world, rand, homePos.south(3), generator, true);
            else genStructureFromTemplate(home, world, rand, homePos, generator, true);

            if(isBalcony) {
                if(direction == Direction.WEST)
                    genStructureFromTemplate(balcony, world, rand, balconyPos.east(3), generator, true);
                else if(direction == Direction.NORTH)
                    genStructureFromTemplate(balcony, world, rand, balconyPos.south(3), generator, true);
                else genStructureFromTemplate(balcony, world, rand, balconyPos, generator, true);
            }

            if(
                balconyPos.getX() < homePos.getX() &&
                balconyPos.getZ() < homePos.getZ()
            )
                PMWorld.updateBlockStates(world, balconyPos, 9, 8, 9);
            else PMWorld.updateBlockStates(world, homePos, 9, 8, 9);

            return true;
        }

        return false;
    }

    @Override
    protected void postProcessStructure(StructureTemplate template, WorldGenLevel world, RandomSource rand, BlockPos pos, StructurePlaceSettings settings, ChunkGenerator generator) {
        // replace the blocks in the structure based on the biome
        var coloredBlocks = getColoredBlocks(rand, biome);
        var fence = wood.get(0);
        var fenceGate = wood.get(1);
        var craftingTable = wood.get(2);
        var log = wood.get(3);
        var plate = wood.get(4);
        var planks = wood.get(5);
        var door = wood.get(7);
        var chest = wood.get(8);
        var trapdoor = wood.get(9);
        var bookShelf = wood.get(10);
        var lantern = coloredBlocks.get(1);
        var bed = coloredBlocks.get(3);
        var newPos = pos;
        if(direction == Direction.EAST && !isBalcony) newPos = pos.south();

        postProcessByBlock(
            world, 9, 8, 9, newPos,
            (curState, curPos) -> {
                if(curState.is(Blocks.CRAFTING_TABLE))
                    world.setBlock(curPos, craftingTable.defaultBlockState(), 32);

                if(curState.is(Blocks.OAK_PLANKS))
                    world.setBlock(curPos, planks.defaultBlockState(), 32);

                if(world.getBlockState(curPos).is(Blocks.LANTERN))
                    world.setBlock(curPos, lantern.defaultBlockState(), 32);

                if(world.getBlockState(curPos).is(Blocks.RED_BED)) {
                    var part = world.getBlockState(curPos).getValue(BedBlock.PART);
                    var bedDir = world.getBlockState(curPos).getValue(BedBlock.FACING);
                    world.setBlock(curPos, bed.defaultBlockState().setValue(BedBlock.PART, part).setValue(BedBlock.FACING, bedDir), 32);
                }

                if (world.getBlockState(curPos).is(Blocks.OAK_FENCE))
                    world.setBlock(curPos, fence.defaultBlockState(), 32);

                if (world.getBlockState(curPos).is(Blocks.CHEST)) {
                    var facing = curState.getValue(ChestBlock.FACING);
                    var leftBlock = world.getBlockState(curPos.relative(facing.getCounterClockWise()));
                    var rightBlock = world.getBlockState(curPos.relative(facing.getClockWise()));
                    var type = ChestType.SINGLE;
                    if(leftBlock.is(Blocks.CHEST)) type = ChestType.RIGHT;
                    else if(rightBlock.is(Blocks.CHEST)) type = ChestType.LEFT;
                    world.setBlock(curPos, Blocks.AIR.defaultBlockState(), 32);

                    world.setBlock(
                        curPos,
                        chest.defaultBlockState()
                            .setValue(ChestBlock.FACING, facing)
                            .setValue(ChestBlock.TYPE, type),
                        32
                    );

                    var tile = world.getBlockEntity(curPos);

                    if(tile instanceof ChestBlockEntity chestTile) {
                        if(biome.is(PMBiomes.GLOWING_FOREST) || biome.is(PMBiomes.CRYSTAL_FOREST))
                            chestTile.setLootTable(new ResourceLocation("chests/buried_treasure"), rand.nextLong());
                        else chestTile.setLootTable(new ResourceLocation("paradisemod:chests/starter_chest"), rand.nextLong());
                    }
                }

                if(world.getBlockState(curPos).is(Blocks.OAK_PRESSURE_PLATE))
                    world.setBlock(curPos, plate.defaultBlockState(), 32);

                if(world.getBlockState(curPos).is(Blocks.BOOKSHELF))
                    world.setBlock(curPos, bookShelf.defaultBlockState(), 32);

                if(world.getBlockState(curPos).is(Blocks.OAK_FENCE_GATE)) {
                    var gateDir = world.getBlockState(curPos).getValue(HorizontalDirectionalBlock.FACING);
                    world.setBlock(curPos, fenceGate.defaultBlockState().setValue(HorizontalDirectionalBlock.FACING, gateDir), 32);
                }

                if(curState.is(Blocks.OAK_DOOR)) {
                    var facing = curState.getValue(DoorBlock.FACING);
                    var open = curState.getValue(DoorBlock.OPEN);
                    var hinge = curState.getValue(DoorBlock.HINGE);
                    var half = curState.getValue(DoorBlock.HALF);

                    if(half == DoubleBlockHalf.LOWER) {
                        world.setBlock(curPos, door.defaultBlockState().setValue(DoorBlock.FACING, facing).setValue(DoorBlock.OPEN, open).setValue(DoorBlock.HINGE, hinge).setValue(DoorBlock.HALF, DoubleBlockHalf.LOWER), 32);
                        world.setBlock(curPos.above(), door.defaultBlockState().setValue(DoorBlock.FACING, facing).setValue(DoorBlock.OPEN, open).setValue(DoorBlock.HINGE, hinge).setValue(DoorBlock.HALF, DoubleBlockHalf.UPPER), 32);
                    }
                }

                if(curState.is(Blocks.OAK_TRAPDOOR)) {
                    var facing = curState.getValue(TrapDoorBlock.FACING);
                    var half = curState.getValue(TrapDoorBlock.HALF);
                    world.setBlock(curPos, trapdoor.defaultBlockState().setValue(TrapDoorBlock.FACING, facing).setValue(TrapDoorBlock.HALF, half), 32);
                }

                if(curState.is(Blocks.OAK_SLAB)) {
                    var stiltPos = new BlockPos.MutableBlockPos(curPos.getX(), curPos.getY() - 1, curPos.getZ());

                    while(!world.getBlockState(stiltPos).canOcclude()) {
                        world.setBlock(stiltPos, log.defaultBlockState(), 32);
                        stiltPos.move(Direction.DOWN);
                        if (stiltPos.getY() < -34) break;
                    }

                    world.setBlock(curPos, planks.defaultBlockState(), 32);
                }
            }
        );

        PMWorld.updateBlockStates(world, newPos, 9, 8, 9);
    }

    private boolean needsBalcony(WorldGenLevel world, BlockPos balconyPos) {
        for(var i = 0; i < 6; i++) {
            var newPos = balconyPos.east(i);
            if(direction.getAxis() == Direction.Axis.Z) newPos = balconyPos.south(i);
            if(!world.getBlockState(newPos).canOcclude()) return true;
        }

        return false;
    }

    private BlockPos getBalconyPos(BlockPos basePos) {
        return switch (direction) {
            default -> basePos.east(6).south();
            case WEST -> basePos.west(3);
            case NORTH -> basePos.east().north(3);
            case SOUTH -> basePos.south(6);
        };
    }
}