package net.paradisemod.worldgen.structures;

import java.util.function.Supplier;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.BlockPos.MutableBlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Holder;
import net.minecraft.core.Vec3i;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
import net.paradisemod.base.PMConfig;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.world.Ores;
import net.paradisemod.world.PMWorld;
import net.paradisemod.world.biome.PMBiomes;

public class ResearchBase extends SmallStructure {
    private Direction direction = Direction.EAST;
    private BaseType baseType = null;

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        baseType = BaseType.fromBiome(world.getBiome(pos));

        if(baseType != null && PMConfig.SETTINGS.structures.bases.shouldGenerate(rand)) {
            direction = Direction.Plane.HORIZONTAL.getRandomDirection(rand);
            var base = new ResourceLocation("paradisemod:" + baseType.name().toLowerCase() + "_base/base_" + direction.name().toLowerCase());
            var balcony = new ResourceLocation("paradisemod:" + baseType.name().toLowerCase() + "_base/balcony_" + direction.name().toLowerCase());

            // get base size based on the facing direction
            int[] baseSize = {6, 7};
            if(direction.getAxis() == Direction.Axis.Z) baseSize = new int[]{7, 6};

            // calculate position of the base
            var y = PMWorld.getHighestY(world, pos, pos.offset(baseSize[0], 0, baseSize[1]));
            if(y < 2) return false;
            var basePos = new BlockPos(pos.getX(), y, pos.getZ());

            // calculate balcony position (if it is needed)
            var balconyPos = getBalconyPos(basePos);
            if(!checkArea(world, basePos, new Vec3i(10, 8, 10))) return false;

            var useBase = false;
            var useBalcony = false;

            switch(direction) {
                case WEST, NORTH -> useBalcony = true;
                default -> useBase = true;
            }

            var hasBalcony = needsBalcony(world, balconyPos);

            if(direction == Direction.WEST)
                genStructureFromTemplate(base, world, rand, basePos.east(3), generator, hasBalcony && useBase);
            else if(direction == Direction.NORTH)
                genStructureFromTemplate(base, world, rand, basePos.south(3), generator, hasBalcony && useBase);
            else genStructureFromTemplate(base, world, rand, basePos, generator, hasBalcony && useBase);

            if(hasBalcony) {
                if(direction == Direction.WEST)
                    return genStructureFromTemplate(balcony, world, rand, balconyPos.east(3), generator, useBalcony);
                else if(direction == Direction.NORTH)
                    return genStructureFromTemplate(balcony, world, rand, balconyPos.south(3), generator, useBalcony);
                else return genStructureFromTemplate(balcony, world, rand, balconyPos, generator, useBalcony);
            }

            if (
                balconyPos.getX() < basePos.getX() &&
                balconyPos.getZ() < basePos.getZ()
            )
                PMWorld.updateBlockStates(world, balconyPos, 10, 8, 10);
            else PMWorld.updateBlockStates(world, basePos, 10, 8, 10);

            return true;
        }

        return false;
    }

    @Override
    protected void postProcessStructure(StructureTemplate template, WorldGenLevel world, RandomSource rand, BlockPos pos, StructurePlaceSettings settings, ChunkGenerator generator) {
        var stiltBlock = baseType.stiltBlock.get().defaultBlockState();
        var size = direction.getAxis() == Direction.Axis.Z ? new int[]{6, 9} : new int[]{9, 6};
        var newPos = pos.east(direction == Direction.SOUTH ? 1 : 0);

        postProcessByBlock(
            world, size[0], 0, size[1], newPos,
            (curState, curPos) -> {
                var x = curPos.getX() - newPos.getX();
                var z = curPos.getZ() - newPos.getZ();

                if((x == 0 && z == 0) || (x == 0 && z == size[1] - 1) || (x == size[0] - 1 && z == 0) || (x == size[0] - 1 && z == size[1] - 1)) {
                    var stiltPos = new MutableBlockPos(curPos.getX(), curPos.getY() - 1, curPos.getZ());
                    var blockToReplace = world.getBlockState(stiltPos);
                    while(!blockToReplace.canOcclude() || blockToReplace.is(Blocks.SNOW)) {
                        world.setBlock(stiltPos, stiltBlock, 32);
                        stiltPos.move(Direction.DOWN);
                        if(stiltPos.getY() < 30) break;
                        blockToReplace = world.getBlockState(stiltPos);
                    }
                }
            }
        );
    }

    private boolean needsBalcony(WorldGenLevel world, BlockPos balconyPos) {
        for(var i = 0; i < 6; i++) {
            var newPos = balconyPos.east(i);
            if(direction.getAxis() == Direction.Axis.Z) newPos = balconyPos.south(i);
            if(!world.getBlockState(newPos).canOcclude()) return true;
        }
        return false;
    }

    private BlockPos getBalconyPos(BlockPos basePos) {
        return switch (direction) {
            default -> basePos.east(6);
            case WEST -> basePos.west(3).south();
            case NORTH -> basePos.north(3);
            case SOUTH -> basePos.south(6).east();
        };
    }

    private enum BaseType {
        SALT(Ores.COMPACT_SALT_BLOCK),
        GLACIER(() -> Blocks.PACKED_ICE),
        VOLCANIC(() -> Blocks.OBSIDIAN);

        private final Supplier<Block> stiltBlock;
        BaseType(Supplier<Block> stiltBlock) { this.stiltBlock = stiltBlock; }

        @Nullable
        private static BaseType fromBiome(Holder<Biome> biome) {
            if(biome.is(PMBiomes.SALT_FLAT) || biome.is(PMBiomes.SALT_CAVE)) return SALT;

            else if(
                biome.is(PMBiomes.GLACIER) ||
                biome.is(PMBiomes.SUBGLACIAL_VOLCANIC_FIELD) ||
                biome.is(PMBiomes.UNDERGROUND_GLACIER) ||
                biome.is(PMBiomes.POLAR_WINTER)
            )
                return GLACIER;

            else if(biome.is(PMTags.Biomes.VOLCANIC)) return VOLCANIC;
            else return null;
        }
    }
}