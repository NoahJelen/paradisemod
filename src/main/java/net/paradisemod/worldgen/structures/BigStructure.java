package net.paradisemod.worldgen.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.QuartPos;
import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.structure.PoolElementStructurePiece;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraft.world.level.levelgen.structure.StructureType;
import net.minecraft.world.level.levelgen.structure.pools.EmptyPoolElement;
import net.minecraft.world.level.levelgen.structure.pools.JigsawPlacement;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.Tags;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.PMConfig;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.world.biome.PMBiomes;
import net.paradisemod.worldgen.structures.PMStructures.StructureGenType;

public class BigStructure extends Structure {
    public static final HashMap<String, ForgeConfigSpec.BooleanValue> CONFIG_KEYS = new HashMap<>();

    static {
        var config = PMConfig.SETTINGS;
        CONFIG_KEYS.put("mediumDarkDungeon", config.structures.mediumDarkDungeon);
        CONFIG_KEYS.put("darkTower", config.structures.darkTower);
        CONFIG_KEYS.put("brickPyramids", config.structures.brickPyramids);
        CONFIG_KEYS.put("terrariums", config.structures.terrariums);
        CONFIG_KEYS.put("minerBases", config.structures.minerBases);
        CONFIG_KEYS.put("largeDarkDungeon", config.structures.largeDarkDungeon);
        CONFIG_KEYS.put("badlandsPyramids", config.structures.badlandsPyramids);
        CONFIG_KEYS.put("darkDesertPyramids", config.structures.darkDesertPyramids);
        CONFIG_KEYS.put("skyWheels", config.structures.skyWheels);
        CONFIG_KEYS.put("enderOutpost", config.structures.enderOutposts);
        CONFIG_KEYS.put("wickerMan", config.structures.wickerMan);
        CONFIG_KEYS.put("traderTent", config.structures.traderTent);
        CONFIG_KEYS.put("monastery", config.structures.monastery);
        CONFIG_KEYS.put("conduit", config.structures.conduit);
    }

    public static final Codec<BigStructure> CODEC = RecordCodecBuilder.<BigStructure>mapCodec(
        instance ->
            instance.group(
                settingsCodec(instance),
                Codec.STRING
                    .fieldOf("start_pool")
                    .forGetter(structure -> structure.startPoolName),

                Codec.STRING
                    .fieldOf("config_key")
                    .forGetter(structure -> structure.configKey),

                Codec.INT
                    .fieldOf("size")
                    .orElse(1)
                    .forGetter(structure -> structure.size),

                Codec.STRING
                    .fieldOf("gen_type")
                    .forGetter(structure -> structure.genType.name().toLowerCase())
            )
                .apply(instance, BigStructure::new)
    )
        .codec();

    //TODO2025: option for generating the structure in the ground (1 block down)
    private final String startPoolName;
    private final String configKey;
    private final int size;
    private final StructureGenType genType;

    public BigStructure(
        StructureSettings config,
        String startPoolName,
        String configKey,
        int size,
        StructureGenType genType
    ) {
        super(config);
        this.startPoolName = startPoolName;
        this.size = size;
        this.configKey = configKey;
        this.genType = genType;
    }

    public BigStructure(
        StructureSettings config,
        String startPoolName,
        String configKey,
        int size,
        String genTypeStr
    ) {
        this(
            config,
            startPoolName,
            configKey,
            size,
            StructureGenType.valueOf(genTypeStr.toUpperCase())
        );
    }

    @Override
    public Optional<GenerationStub> findGenerationPoint(GenerationContext context) {
        var shouldGenerate = CONFIG_KEYS.get(configKey).get();

        if(shouldGenerate) {
            var chunkGenerator = context.chunkGenerator();
            var worldgenRand = context.random();
            var rand = worldgenRand.fork();
            var heightVariation = switch(genType) {
                case UNDERGROUND -> rand.nextInt(60);
                case SKY -> rand.nextInt(20);
                default -> 0;
            };

            var startY = genType.startHeight() + heightVariation;
            var chunkpos = context.chunkPos();
            var structPos = new BlockPos(chunkpos.getMinBlockX(), startY, chunkpos.getMinBlockZ());
            Optional<Heightmap.Types> heightMap = Optional.empty();

            if(genType.isGround()) {
                heightMap = Optional.of(Heightmap.Types.WORLD_SURFACE_WG);

                var biome = chunkGenerator.getBiomeSource()
                    .getNoiseBiome(
                        QuartPos.fromBlock(structPos.getX()),
                        QuartPos.fromBlock(structPos.getY()),
                        QuartPos.fromBlock(structPos.getZ()),
                        context.randomState().sampler()
                    );

                if(genType == StructureGenType.GROUND && biome.is(Tags.Biomes.IS_WATER))
                    heightMap = Optional.of(Heightmap.Types.OCEAN_FLOOR_WG);
                else if(
                    genType == StructureGenType.DRY_GROUND &&
                    (
                        biome.is(Tags.Biomes.IS_WATER) ||
                        biome.is(PMBiomes.GLACIER) ||
                        biome.is(PMBiomes.SUBGLACIAL_VOLCANIC_FIELD)
                    )
                )
                    return Optional.empty();
            }

            var regAccess = context.registryAccess();
            var structPoolRegistry = regAccess.registryOrThrow(Registries.TEMPLATE_POOL);
            var key = PMRegistries.createResourceKey(Registries.TEMPLATE_POOL, startPoolName);
            var startPool = structPoolRegistry.getHolder(key);

            if(startPool.isPresent())
                return addPieces(context, startPool.get(), structPos, heightMap);
            else
                ParadiseMod.LOG.error("Structure pool element \"" + startPoolName + "\" doesn't seem to exist!");
        }

        return Optional.empty();
    }

    private Optional<Structure.GenerationStub> addPieces(Structure.GenerationContext context, Holder<StructureTemplatePool> startPool, BlockPos structPos, Optional<Heightmap.Types> heightMap) {
        var regAccess = context.registryAccess();
        var chunkgenerator = context.chunkGenerator();
        var structuretemplatemanager = context.structureTemplateManager();
        var heightaccessor = context.heightAccessor();
        var worldgenrandom = context.random();
        var structPoolRegistry = regAccess.registryOrThrow(Registries.TEMPLATE_POOL);
        var rotation = Rotation.getRandom(worldgenrandom);
        var templatepool = startPool.value();
        var poolElement = templatepool.getRandomTemplate(worldgenrandom);

        if(poolElement == EmptyPoolElement.INSTANCE)
            return Optional.empty();
        else {
            var piece = new PoolElementStructurePiece(structuretemplatemanager, poolElement, structPos, 0, rotation, poolElement.getBoundingBox(structuretemplatemanager, structPos, rotation));
            var structBox = piece.getBoundingBox();
            var placeX = (structBox.maxX() + structBox.minX()) / 2;
            var placeZ = (structBox.maxZ() + structBox.minZ()) / 2;
            int placeY;

            if(heightMap.isPresent()) {
                placeY = structPos.getY() + chunkgenerator.getFirstFreeHeight(placeX, placeZ, heightMap.get(), heightaccessor, context.randomState());
                piece.move(0, placeY, 0);
            }
            else
                placeY = structPos.getY();

            return Optional.of(
                new Structure.GenerationStub(
                    new BlockPos(placeX, placeY, placeZ),
                    builder -> {
                        ArrayList<PoolElementStructurePiece> list = new ArrayList<>();
                        list.add(piece);
                        if (size > 0) {
                            var aabb = new AABB(placeX - 128, placeY - 128, placeZ - 128, placeX + 129, placeY + 129, placeZ + 129);
                            var shape = Shapes.join(Shapes.create(aabb), Shapes.create(AABB.of(structBox)), BooleanOp.ONLY_FIRST);
                            JigsawPlacement.addPieces(context.randomState(), size, true, chunkgenerator, structuretemplatemanager, heightaccessor, worldgenrandom, structPoolRegistry, piece, list, shape);
                            list.forEach(builder::addPiece);
                        }
                    }
                )
            );
        }
    }

    @Override
    public StructureType<?> type() {
        return PMStructures.BIG_STRUCTURE.get();
    }
}