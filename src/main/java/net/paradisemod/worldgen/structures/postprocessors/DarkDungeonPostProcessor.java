package net.paradisemod.worldgen.structures.postprocessors;

import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.data.worldgen.features.CaveFeatures;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.paradisemod.world.DeepDarkBlocks;

public class DarkDungeonPostProcessor extends StructurePostProcessor {
    public DarkDungeonPostProcessor(BlockPos minPos, BlockPos farCorner) {
        super(minPos, farCorner);
    }

    @Override
    protected void postProcessBlock(WorldGenLevel world, ChunkGenerator generator, RandomSource rand, BlockState state, BlockPos blockPos) {
        var deepslateBricks = List.of(Blocks.DEEPSLATE_BRICKS, Blocks.CRACKED_DEEPSLATE_BRICKS);

        if(state.is(Blocks.BLACK_STAINED_GLASS) || state.is(DeepDarkBlocks.POLISHED_DARKSTONE_BRICKS.get())) {
            var hasAdjStone = false;

            for(var direction : Direction.values()) {
                var adjBlock = world.getBlockState(blockPos.relative(direction));
                if(adjBlock.is(Blocks.DEEPSLATE) || adjBlock.is(Blocks.COBBLED_DEEPSLATE)) {
                    hasAdjStone = true;
                    world.setBlock(blockPos, deepslateBricks.get(rand.nextInt(2)).defaultBlockState(), 2);
                    break;
                }
                else if(adjBlock.is(DeepDarkBlocks.DARKSTONE.get())) {
                    hasAdjStone = true;
                    world.setBlock(blockPos, DeepDarkBlocks.POLISHED_DARKSTONE_BRICKS.get().defaultBlockState(), 2);
                    break;
                }
            }

            if(!hasAdjStone && state.is(Blocks.BLACK_STAINED_GLASS))
                world.setBlock(blockPos, Blocks.GLASS.defaultBlockState(), 2);
        }
        else if(state.is(Blocks.BLUE_STAINED_GLASS)) {
            world.setBlock(blockPos, Blocks.AIR.defaultBlockState(), 2);
            var sculkPatch = getFeature(CaveFeatures.SCULK_PATCH_ANCIENT_CITY).value();
            sculkPatch.place(world, generator, rand, blockPos);
        }
    }

}
