package net.paradisemod.worldgen.structures.postprocessors;

import java.util.EnumMap;
import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Holder;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.BiomeTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.CandleBlock;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.IronBarsBlock;
import net.minecraft.world.level.block.LanternBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.WallBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.Half;
import net.minecraft.world.level.block.state.properties.StairsShape;
import net.minecraft.world.level.block.state.properties.WallSide;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraftforge.common.Tags;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.world.biome.PMBiomes;
import net.paradisemod.worldgen.features.BasicFeature;

public class MonasteryPostProcessor extends StructurePostProcessor {
    public static final List<Block> NEUTRAL_PANES = List.of(
        Blocks.WHITE_STAINED_GLASS_PANE,
        Blocks.LIGHT_GRAY_STAINED_GLASS_PANE,
        Blocks.GRAY_STAINED_GLASS_PANE,
        Blocks.BLACK_STAINED_GLASS_PANE
    );

    public MonasteryPostProcessor(BlockPos minPos, BlockPos farCorner) {
        super(minPos, farCorner);
    }

    @Override
    protected void postProcessBlock(WorldGenLevel world, ChunkGenerator generator, RandomSource rand, BlockState state, BlockPos blockPos) {
        var biome = world.getBiome(blockPos);
        var wood = BasicFeature.getWood(rand, biome);
        var coloredBlocks = coloredGlassAndCandles(rand, biome);
        var isLush = biome.is(BiomeTags.IS_JUNGLE) || biome.is(Tags.Biomes.IS_SWAMP);

        if(state.is(Blocks.STONE_BRICKS)) {
            if(rand.nextInt(20) == 0)
                world.setBlock(blockPos, Blocks.CRACKED_STONE_BRICKS.defaultBlockState(), 32);
            else if(rand.nextInt(20) == 0 && isLush)
                world.setBlock(blockPos, Blocks.MOSSY_STONE_BRICKS.defaultBlockState(), 32);
        }
        else if(state.is(Blocks.DEEPSLATE_BRICKS) && rand.nextInt(20) == 0)
            world.setBlock(blockPos, Blocks.CRACKED_DEEPSLATE_BRICKS.defaultBlockState(), 32);
        else if(state.is(Blocks.STONE_BRICK_STAIRS) && rand.nextInt(20) == 0 && isLush) {
            Direction facing = state.getValue(StairBlock.FACING);
            Half half = state.getValue(StairBlock.HALF);
            StairsShape shape = state.getValue(StairBlock.SHAPE);

            world.setBlock(
                blockPos,
                Blocks.MOSSY_STONE_BRICK_STAIRS.defaultBlockState()
                    .setValue(StairBlock.FACING, facing)
                    .setValue(StairBlock.HALF, half)
                    .setValue(StairBlock.SHAPE, shape),
                32
            );
        }
        else if(state.is(Blocks.STONE_BRICK_WALL) && rand.nextInt(20) == 0 && isLush) {
            var north = state.getValue(WallBlock.NORTH_WALL);
            WallSide south = state.getValue(WallBlock.SOUTH_WALL);
            WallSide east = state.getValue(WallBlock.EAST_WALL);
            WallSide west = state.getValue(WallBlock.WEST_WALL);
            boolean up = state.getValue(WallBlock.UP);

            world.setBlock(
                blockPos,
                Blocks.MOSSY_STONE_BRICK_WALL.defaultBlockState()
                    .setValue(WallBlock.NORTH_WALL, north)
                    .setValue(WallBlock.SOUTH_WALL, south)
                    .setValue(WallBlock.EAST_WALL, east)
                    .setValue(WallBlock.WEST_WALL, west)
                    .setValue(WallBlock.UP, up),
                32
            );
        }
        else if(state.is(Blocks.GLASS_PANE)) {
            boolean north = state.getValue(IronBarsBlock.NORTH);
            boolean south = state.getValue(IronBarsBlock.SOUTH);
            boolean east = state.getValue(IronBarsBlock.EAST);
            boolean west = state.getValue(IronBarsBlock.WEST);
            var stainedGlass = Utils.getBlockTag(Tags.Blocks.STAINED_GLASS_PANES);

            world.setBlock(
                blockPos,
                stainedGlass.getRandomElement(rand).get()
                    .defaultBlockState()
                    .setValue(IronBarsBlock.NORTH, north)
                    .setValue(IronBarsBlock.SOUTH, south)
                    .setValue(IronBarsBlock.EAST, east)
                    .setValue(IronBarsBlock.WEST, west),
                32
            );
        }
        else if(state.is(Blocks.BLACK_STAINED_GLASS_PANE)) {
            boolean north = state.getValue(IronBarsBlock.NORTH);
            boolean south = state.getValue(IronBarsBlock.SOUTH);
            boolean east = state.getValue(IronBarsBlock.EAST);
            boolean west = state.getValue(IronBarsBlock.WEST);

            world.setBlock(
                blockPos,
                NEUTRAL_PANES.get(rand.nextInt(NEUTRAL_PANES.size()))
                    .defaultBlockState()
                    .setValue(IronBarsBlock.NORTH, north)
                    .setValue(IronBarsBlock.SOUTH, south)
                    .setValue(IronBarsBlock.EAST, east)
                    .setValue(IronBarsBlock.WEST, west),
                32
            );
        }
        else if(state.is(Blocks.YELLOW_STAINED_GLASS_PANE)) {
            boolean north = state.getValue(IronBarsBlock.NORTH);
            boolean south = state.getValue(IronBarsBlock.SOUTH);
            boolean east = state.getValue(IronBarsBlock.EAST);
            boolean west = state.getValue(IronBarsBlock.WEST);

            world.setBlock(
                blockPos,
                coloredBlocks.get(0)
                    .defaultBlockState()
                    .setValue(IronBarsBlock.NORTH, north)
                    .setValue(IronBarsBlock.SOUTH, south)
                    .setValue(IronBarsBlock.EAST, east)
                    .setValue(IronBarsBlock.WEST, west),
                32
            );
        }
        else if(state.is(Blocks.YELLOW_CANDLE))
            world.setBlock(
                blockPos,
                coloredBlocks.get(1)
                    .defaultBlockState()
                    .setValue(CandleBlock.CANDLES, 4)
                    .setValue(CandleBlock.LIT, true),
                    32
                );
        else if(state.is(Blocks.OAK_STAIRS)) {
            Direction facing = state.getValue(StairBlock.FACING);
            Half half = state.getValue(StairBlock.HALF);
            StairsShape shape = state.getValue(StairBlock.SHAPE);

            world.setBlock(
                blockPos,
                wood.get(6).defaultBlockState()
                    .setValue(StairBlock.FACING, facing)
                    .setValue(StairBlock.HALF, half)
                    .setValue(StairBlock.SHAPE, shape),
                32
            );
        }
        else if(state.is(Blocks.OAK_DOOR)) {
            var facing = state.getValue(DoorBlock.FACING);
            var open = state.getValue(DoorBlock.OPEN);
            var hinge = state.getValue(DoorBlock.HINGE);
            var half = state.getValue(DoorBlock.HALF);

            world.setBlock(
                blockPos,
                wood.get(7).defaultBlockState()
                    .setValue(DoorBlock.FACING, facing)
                    .setValue(DoorBlock.OPEN, open)
                    .setValue(DoorBlock.HINGE, hinge)
                    .setValue(DoorBlock.HALF, half),
                32
            );
        }
        else if(state.is(Blocks.LANTERN))
            world.setBlock(
                blockPos,
                BasicFeature.getColoredBlocks(rand, biome).get(1)
                    .defaultBlockState()
                    .setValue(LanternBlock.HANGING, true),
                32
            );
    }

    public static List<Block> coloredGlassAndCandles(RandomSource rand, Holder<Biome> biome) {
        var roseFieldBlocks = roseGlassAndCandles(PMBiomes.ROSE_FIELDS_BY_COLOR, biome);
        var roseLandBlocks = roseGlassAndCandles(PMBiomes.ROSE_LANDS_BY_COLOR, biome);
        if(!roseFieldBlocks.isEmpty()) return roseFieldBlocks;
        else if(!roseLandBlocks.isEmpty()) return roseLandBlocks;

        var glassPane = Blocks.RED_STAINED_GLASS_PANE;
        var candle = Blocks.RED_CANDLE;

        if(biome.is(Biomes.DEEP_DARK) || biome.is(PMBiomes.TAIGA_INSOMNIUM)) {
            glassPane = Blocks.BLACK_STAINED_GLASS_PANE;
            candle = Blocks.BLACK_CANDLE;
        }
        else if(biome.is(Biomes.DARK_FOREST) || biome.is(PMBiomes.UNDERGROUND_DARK_FOREST)) {
            glassPane = Blocks.BLUE_STAINED_GLASS_PANE;
            candle = Blocks.BLUE_CANDLE;
        }
        else if(biome.is(BiomeTags.IS_BADLANDS) || biome.is(PMTags.Biomes.ROCKY_DESERTS) || biome.is(PMTags.Biomes.COLD_ROCKY_DESERTS)) {
            glassPane = Blocks.BROWN_STAINED_GLASS_PANE;
            candle = Blocks.BROWN_CANDLE;
        }
        else if(
            biome.is(Biomes.BIRCH_FOREST) ||
            biome.is(Biomes.OLD_GROWTH_BIRCH_FOREST) ||
            biome.is(PMBiomes.UNDERGROUND_BIRCH_FOREST)
        ) {
            glassPane = Blocks.CYAN_STAINED_GLASS_PANE;
            candle = Blocks.CYAN_CANDLE;
        }
        else if(
            biome.is(Tags.Biomes.IS_SWAMP) ||
            biome.is(PMBiomes.TEMPERATE_RAINFOREST) ||
            biome.is(PMBiomes.UNDERGROUND_TEMPERATE_RAINFOREST)
        ) {
            glassPane = Blocks.GREEN_STAINED_GLASS_PANE;
            candle = Blocks.GREEN_CANDLE;
        }
        else if(biome.is(Tags.Biomes.IS_CONIFEROUS) || biome.is(Tags.Biomes.IS_COLD) || biome.is(Tags.Biomes.IS_SNOWY)) {
            glassPane = Blocks.LIGHT_BLUE_STAINED_GLASS_PANE;
            candle = Blocks.LIGHT_BLUE_CANDLE;
        }
        else if(biome.is(BiomeTags.IS_JUNGLE)) {
            glassPane = Blocks.LIME_STAINED_GLASS_PANE;
            candle = Blocks.LIME_CANDLE;
        }
        else if(biome.is(BiomeTags.IS_SAVANNA)) {
            glassPane = Blocks.ORANGE_STAINED_GLASS_PANE;
            candle = Blocks.ORANGE_CANDLE;
        }
        else if(biome.is(Tags.Biomes.IS_DRY)) {
            glassPane = Blocks.YELLOW_STAINED_GLASS_PANE;
            candle = Blocks.YELLOW_CANDLE;
        }
        else if(biome.is(Biomes.CHERRY_GROVE) || biome.is(PMBiomes.UNDERGROUND_CHERRY_FOREST)) {
            glassPane = Blocks.PINK_STAINED_GLASS_PANE;
            candle = Blocks.PINK_CANDLE;
        }
        else if(biome.is(PMBiomes.AUTUMN_FOREST) || biome.is(PMBiomes.UNDERGROUND_AUTUMN_FOREST)) {
            switch(rand.nextInt(4)) {
                case 0 -> {
                    glassPane = Blocks.YELLOW_STAINED_GLASS_PANE;
                    candle = Blocks.YELLOW_CANDLE;
                }

                case 1 -> {
                    glassPane = Blocks.ORANGE_STAINED_GLASS_PANE;
                    candle = Blocks.ORANGE_CANDLE;
                }

                case 2 -> {
                    glassPane = Blocks.RED_STAINED_GLASS_PANE;
                    candle = Blocks.RED_CANDLE;
                }

                case 3 -> {
                    glassPane = Blocks.BLUE_STAINED_GLASS_PANE;
                    candle = Blocks.BLUE_CANDLE;
                }
            }
        }

        return List.of(glassPane, candle);
    }

    private static List<Block> roseGlassAndCandles(EnumMap<DyeColor, ResourceKey<Biome>> roseBiomes, Holder<Biome> biome) {
        var glassPane = Blocks.RED_STAINED_GLASS_PANE;
        var candle = Blocks.RED_CANDLE;

        for(var entry : roseBiomes.entrySet()) {
            var color = entry.getKey();
            var roseBiome = entry.getValue();

            if(biome.is(roseBiome)) {
                switch(color) {
                    case WHITE -> {
                        glassPane = Blocks.WHITE_STAINED_GLASS_PANE;
                        candle = Blocks.WHITE_CANDLE;
                    }

                    case ORANGE -> {
                        glassPane = Blocks.ORANGE_STAINED_GLASS_PANE;
                        candle = Blocks.ORANGE_CANDLE;
                    }

                    case MAGENTA -> {
                        glassPane = Blocks.MAGENTA_STAINED_GLASS_PANE;
                        candle = Blocks.MAGENTA_CANDLE;
                    }

                    case LIGHT_BLUE -> {
                        glassPane = Blocks.LIGHT_BLUE_STAINED_GLASS_PANE;
                        candle = Blocks.LIGHT_BLUE_CANDLE;
                    }

                    case YELLOW -> {
                        glassPane = Blocks.YELLOW_STAINED_GLASS_PANE;
                        candle = Blocks.YELLOW_CANDLE;
                    }

                    case LIME -> {
                        glassPane = Blocks.LIME_STAINED_GLASS_PANE;
                        candle = Blocks.LIME_CANDLE;
                    }

                    case PINK -> {
                        glassPane = Blocks.PINK_STAINED_GLASS_PANE;
                        candle = Blocks.PINK_CANDLE;
                    }

                    case GRAY -> {
                        glassPane = Blocks.GRAY_STAINED_GLASS_PANE;
                        candle = Blocks.GRAY_CANDLE;
                    }

                    case LIGHT_GRAY -> {
                        glassPane = Blocks.LIGHT_GRAY_STAINED_GLASS_PANE;
                        candle = Blocks.LIGHT_GRAY_CANDLE;
                    }

                    case CYAN -> {
                        glassPane = Blocks.CYAN_STAINED_GLASS_PANE;
                        candle = Blocks.CYAN_CANDLE;
                    }

                    case PURPLE -> {
                        glassPane = Blocks.PURPLE_STAINED_GLASS_PANE;
                        candle = Blocks.PURPLE_CANDLE;
                    }

                    case BLUE -> {
                        glassPane = Blocks.BLUE_STAINED_GLASS_PANE;
                        candle = Blocks.BLUE_CANDLE;
                    }

                    case BROWN -> {
                        glassPane = Blocks.BROWN_STAINED_GLASS_PANE;
                        candle = Blocks.BROWN_CANDLE;
                    }

                    case GREEN -> {
                        glassPane = Blocks.GREEN_STAINED_GLASS_PANE;
                        candle = Blocks.GREEN_CANDLE;
                    }

                    case BLACK -> {
                        glassPane = Blocks.BLACK_STAINED_GLASS_PANE;
                        candle = Blocks.BLACK_CANDLE;
                    }
                }

                return List.of(glassPane, candle);
            }
        }

        return List.of();
    }
}