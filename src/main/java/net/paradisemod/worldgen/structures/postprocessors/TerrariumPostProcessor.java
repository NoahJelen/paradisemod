package net.paradisemod.worldgen.structures.postprocessors;

import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Holder;
import net.minecraft.data.worldgen.placement.TreePlacements;
import net.minecraft.tags.BiomeTags;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.npc.Villager;
import net.minecraft.world.entity.npc.VillagerType;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.BeetrootBlock;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraftforge.common.Tags;
import net.paradisemod.base.Utils;
import net.paradisemod.building.Trapdoors;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.biome.PMBiomes;
import net.paradisemod.worldgen.features.foliage.PMFoliage;

public class TerrariumPostProcessor extends BrickDungeonPostProcessor {
    private static final List<EntityType<? extends Mob>> ANIMALS = List.of(EntityType.COW, EntityType.SHEEP, EntityType.PIG, EntityType.CHICKEN);

    public TerrariumPostProcessor(BlockPos minPos, BlockPos farCorner) {
        super(minPos, farCorner);
    }

    @Override
    protected void postProcessBlock(WorldGenLevel world, ChunkGenerator generator, RandomSource rand, BlockState state, BlockPos blockPos) {
        var deepslateBricks = List.of(Blocks.DEEPSLATE_BRICKS, Blocks.CRACKED_DEEPSLATE_BRICKS);
        var biome = world.getBiome(minPos);
        var grasses = List.of(Blocks.GRASS, Blocks.FERN, Blocks.TALL_GRASS);
        var flowers = Utils.getBlockTag(BlockTags.FLOWERS);
        var deepslateLevel = rand.nextInt(5);

        if(state.is(Blocks.GRASS_BLOCK)) {
            var posAbove = blockPos.above();

            if(rand.nextInt(10) == 0 && rand.nextBoolean()) {
                var flower = flowers.getRandomElement(rand).get();
                if(flower != Blocks.WITHER_ROSE)
                    world.setBlock(posAbove, flower.defaultBlockState(), 32);

                if(flower.defaultBlockState().hasProperty(BlockStateProperties.DOUBLE_BLOCK_HALF))
                    world.setBlock(posAbove.above(), flower.defaultBlockState().setValue(BlockStateProperties.DOUBLE_BLOCK_HALF, DoubleBlockHalf.UPPER), 32);
            }
            else if(rand.nextBoolean()) {
                var grass = grasses.get(rand.nextInt(3));
                world.setBlock(posAbove, grass.defaultBlockState(), 2);
                if(grass.defaultBlockState().hasProperty(BlockStateProperties.DOUBLE_BLOCK_HALF))
                    world.setBlock(posAbove.above(), grass.defaultBlockState().setValue(BlockStateProperties.DOUBLE_BLOCK_HALF, DoubleBlockHalf.UPPER), 32);
            }
            else if(rand.nextInt(20) == 0)
                placeBiomeTree(world, generator, rand, posAbove);
        }
        else if(state.is(Blocks.FARMLAND)) {
            var crops = List.of(Blocks.CARROTS, Blocks.POTATOES, Blocks.WHEAT, Blocks.BEETROOTS);
            var crop = crops.get(rand.nextInt(4));
            if(crop instanceof BeetrootBlock) world.setBlock(blockPos.above(), crop.defaultBlockState().setValue(BeetrootBlock.AGE, rand.nextInt(3)), 32);
            else world.setBlock(blockPos.above(), crop.defaultBlockState().setValue(CropBlock.AGE, rand.nextInt(7)), 32);
        }
        else if(state.is(BlockTags.STONE_BRICKS)) {
            if(biome.is(Biomes.DEEP_DARK))
                world.setBlock(blockPos, DeepDarkBlocks.POLISHED_DARKSTONE_BRICKS.get().defaultBlockState(), 2);
            else if(blockPos.getY() <= deepslateLevel)
                world.setBlock(blockPos, deepslateBricks.get(rand.nextInt(2)).defaultBlockState(), 2);
            else if(biome.is(BiomeTags.IS_OCEAN)) {
                for(var direction : Direction.values()) {
                    var adjBlock = world.getBlockState(blockPos.relative(direction));
                    if(adjBlock.is(Blocks.WATER)) {
                        world.setBlock(blockPos, Blocks.PRISMARINE_BRICKS.defaultBlockState(), 2);
                        break;
                    }
                }
            }
        }
        else if(state.is(Trapdoors.EMERALD_TRAPDOOR.get())) {
            world.setBlock(blockPos, Blocks.AIR.defaultBlockState(), 16);
            for(var x = 0; x < 8; x++)
                placeMobAtPos(world, new Villager(EntityType.VILLAGER, world.getLevel(), getVillagerType(world.getBiome(minPos))), blockPos);
        }
        else if(state.is(Trapdoors.STONE_TRAPDOOR.get())) {
            world.setBlock(blockPos, Blocks.AIR.defaultBlockState(), 16);
            for(var x = 0; x < 4; x++)
                placeMobAtPos(world, ANIMALS.get(rand.nextInt(ANIMALS.size())), blockPos);
        }

        super.postProcessBlock(world, generator, rand, state, blockPos);
    }

    private static VillagerType getVillagerType(Holder<Biome> biome) {
        if(biome.is(Tags.Biomes.IS_SWAMP)) return VillagerType.SWAMP;
        else if(biome.is(Tags.Biomes.IS_HOT)) {
            if(biome.is(BiomeTags.IS_SAVANNA)) return VillagerType.SAVANNA;
            else if(biome.is(Tags.Biomes.IS_DRY)) return VillagerType.DESERT;
            return VillagerType.JUNGLE;
        }
        else if(biome.is(Tags.Biomes.IS_COLD)) {
            if(biome.is(Tags.Biomes.IS_CONIFEROUS)) return VillagerType.TAIGA;
            return VillagerType.SNOW;
        }

        return VillagerType.PLAINS;
    }

    private void placeBiomeTree(WorldGenLevel world, ChunkGenerator generator, RandomSource rand, BlockPos blockPos) {
        var biome = world.getBiome(minPos);
        var tree = rand.nextBoolean() ? TreePlacements.OAK_CHECKED : TreePlacements.BIRCH_CHECKED;

        if(biome.is(Tags.Biomes.IS_HOT)) {
            if(biome.is(BiomeTags.IS_SAVANNA)) tree = TreePlacements.ACACIA_CHECKED;
            else tree = TreePlacements.JUNGLE_TREE_CHECKED;
        }
        else if(biome.is(Biomes.DARK_FOREST) || biome.is(PMBiomes.UNDERGROUND_DARK_FOREST)) tree = TreePlacements.DARK_OAK_CHECKED;
        else if(biome.is(Biomes.BIRCH_FOREST) || biome.is(Biomes.OLD_GROWTH_BIRCH_FOREST) || biome.is(PMBiomes.UNDERGROUND_BIRCH_FOREST))
            tree = TreePlacements.BIRCH_CHECKED;

        else if(biome.is(PMBiomes.AUTUMN_FOREST)) {
            var trees = PMFoliage.allAutumnTrees();
            tree = trees.get(rand.nextInt(trees.size()));
        }

        else if(biome.is(Tags.Biomes.IS_COLD)) tree = TreePlacements.SPRUCE_CHECKED;

        for(var dir : Direction.Plane.HORIZONTAL) {
            var nearState = world.getBlockState(blockPos.relative(dir));
            var farState = world.getBlockState(blockPos.relative(dir, 2));

            if(nearState.is(BlockTags.LOGS) || farState.is(BlockTags.LOGS)) return;
        }

        if(getPlacedFeature(tree).value().place(world, generator, rand, blockPos))
            world.setBlock(blockPos.below(), Blocks.DIRT.defaultBlockState(), 2);
    }
}