package net.paradisemod.worldgen.structures.postprocessors;

import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.BedBlock;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FenceBlock;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.LanternBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.paradisemod.building.Trapdoors;
import net.paradisemod.worldgen.features.BasicFeature;

public class TraderTentPostProcessor extends StructurePostProcessor {
    public TraderTentPostProcessor(BlockPos minPos, BlockPos farCorner) {
        super(minPos, farCorner);
    }

    @Override
    protected void postProcessBlock(WorldGenLevel world, ChunkGenerator generator, RandomSource rand, BlockState state, BlockPos blockPos) {
        // replace the blocks in the structure based on the biome
        var biome = world.getBiome(blockPos);
        var coloredBlocks = BasicFeature.getColoredBlocks(rand, biome);
        var wood = BasicFeature.getWood(rand, biome);
        var fence = wood.get(0);
        var fenceGate = wood.get(1);
        var wool = coloredBlocks.get(0);
        var lantern = coloredBlocks.get(1);
        var craftingTable = wood.get(2);
        var bed = coloredBlocks.get(3);

        if(state.is(Trapdoors.GLASS_TRAPDOOR.get())) {
            var llama1 = EntityType.TRADER_LLAMA.create(world.getLevel());
            var llama2 = EntityType.TRADER_LLAMA.create(world.getLevel());
            llama1.setDespawnDelay(Integer.MAX_VALUE);
            llama2.setDespawnDelay(Integer.MAX_VALUE);
            placeMobAtPos(world, llama1, blockPos);
            placeMobAtPos(world, llama2, blockPos);
            world.setBlock(blockPos, Blocks.AIR.defaultBlockState(), 32);
        }
        else if(state.is(Blocks.OAK_TRAPDOOR)) {
            var trader = EntityType.WANDERING_TRADER.create(world.getLevel());
            trader.setDespawnDelay(-1);
            placeMobAtPos(world, trader, blockPos);
            world.setBlock(blockPos, Blocks.AIR.defaultBlockState(), 32);
        }
        else if(state.is(Blocks.RED_WOOL)) {
            var nWools = List.of(Blocks.WHITE_WOOL, Blocks.LIGHT_GRAY_WOOL, Blocks.GRAY_WOOL, Blocks.BLACK_WOOL);
            world.setBlock(blockPos, (rand.nextInt(4) == 0 ? nWools.get(rand.nextInt(4)) : wool).defaultBlockState(), 32);
        }
        else if(state.is(Blocks.LANTERN))
            world.setBlock(blockPos, lantern.defaultBlockState().setValue(LanternBlock.HANGING, true), 32);
        else if(world.getBlockState(blockPos).is(Blocks.RED_BED)) {
            var part = world.getBlockState(blockPos).getValue(BedBlock.PART);
            var bedDir = world.getBlockState(blockPos).getValue(BedBlock.FACING);
            world.setBlock(blockPos, bed.defaultBlockState().setValue(BedBlock.PART, part).setValue(BedBlock.FACING, bedDir), 32);
        }
        else if(world.getBlockState(blockPos).is(Blocks.OAK_FENCE)) {
            var north = state.getValue(FenceBlock.NORTH);
            var south = state.getValue(FenceBlock.SOUTH);
            var east = state.getValue(FenceBlock.EAST);
            var west = state.getValue(FenceBlock.WEST);

            world.setBlock(
                blockPos,
                fence.defaultBlockState()
                    .setValue(FenceBlock.NORTH, north)
                    .setValue(FenceBlock.SOUTH, south)
                    .setValue(FenceBlock.EAST, east)
                    .setValue(FenceBlock.WEST, west)
                    .setValue(FenceBlock.WATERLOGGED, false),
                32
            );
        }
        else if(world.getBlockState(blockPos).is(Blocks.OAK_FENCE_GATE)) {
            var gateDir = world.getBlockState(blockPos).getValue(HorizontalDirectionalBlock.FACING);
            world.setBlock(blockPos, fenceGate.defaultBlockState().setValue(HorizontalDirectionalBlock.FACING, gateDir), 1);
        }
        else if(world.getBlockState(blockPos).is(Blocks.CRAFTING_TABLE))
            world.setBlock(blockPos, craftingTable.defaultBlockState(), 8);
    }
}