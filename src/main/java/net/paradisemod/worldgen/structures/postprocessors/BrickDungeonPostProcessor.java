package net.paradisemod.worldgen.structures.postprocessors;

import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FenceBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.SlabType;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraftforge.common.Tags;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.worldgen.features.BasicFeature;

public class BrickDungeonPostProcessor extends StructurePostProcessor {
    public BrickDungeonPostProcessor(BlockPos minPos, BlockPos farCorner) {
        super(minPos, farCorner);
    }

    @Override
    protected void postProcessBlock(WorldGenLevel world, ChunkGenerator generator, RandomSource rand, BlockState state, BlockPos blockPos) {
        var stoneBricks = List.of(Blocks.STONE_BRICKS, Blocks.MOSSY_STONE_BRICKS, Blocks.CRACKED_STONE_BRICKS);
        var deepslateBricks = List.of(Blocks.DEEPSLATE_BRICKS, Blocks.CRACKED_DEEPSLATE_BRICKS);
        var deepslateLevel = rand.nextInt(5);
        var biome = world.getBiome(blockPos);
        var wood = BasicFeature.getWood(rand, biome);

        if(state.is(Blocks.OAK_PLANKS))
            world.setBlock(blockPos, wood.get(5).defaultBlockState(), 32);

        if(state.is(Blocks.OAK_FENCE)) {
            var fence = wood.get(0);
            var north = state.getValue(FenceBlock.NORTH);
            var south = state.getValue(FenceBlock.SOUTH);
            var east = state.getValue(FenceBlock.EAST);
            var west = state.getValue(FenceBlock.WEST);

            world.setBlock(
                blockPos,
                fence.defaultBlockState()
                    .setValue(FenceBlock.NORTH, north)
                    .setValue(FenceBlock.SOUTH, south)
                    .setValue(FenceBlock.EAST, east)
                    .setValue(FenceBlock.WEST, west)
                    .setValue(FenceBlock.WATERLOGGED, false),
                32
            );
        }

        if(state.is(Blocks.BARRIER))
            world.setBlock(blockPos, Blocks.AIR.defaultBlockState(), 32);

        if(state.is(Blocks.COBBLESTONE) && rand.nextBoolean())
            world.setBlock(blockPos, Blocks.MOSSY_COBBLESTONE.defaultBlockState(), 32);

        else if(state.is(BlockTags.STONE_BRICKS) && biome.is(Biomes.DEEP_DARK)) 
            world.setBlock(blockPos, DeepDarkBlocks.POLISHED_DARKSTONE_BRICKS.get().defaultBlockState(), 32);
        else if(state.is(Blocks.BLACK_STAINED_GLASS)) {
            world.setBlock(blockPos, Blocks.GLASS.defaultBlockState(), 2);
            for(var direction : Direction.values()) {
                var adjBlock = world.getBlockState(blockPos.relative(direction));
                if(adjBlock.is(Blocks.DEEPSLATE) || adjBlock.is(Blocks.COBBLED_DEEPSLATE)) {
                    world.setBlock(blockPos, deepslateBricks.get(rand.nextInt(2)).defaultBlockState(), 2);
                    break;
                }
                else if(
                    adjBlock.is(Tags.Blocks.STONE) ||
                    adjBlock.is(Tags.Blocks.GRAVEL) ||
                    adjBlock.is(Tags.Blocks.ORES)
                ) {
                    world.setBlock(blockPos, stoneBricks.get(rand.nextInt(3)).defaultBlockState(), 2);
                    break;
                }
                else if(adjBlock.is(BlockTags.DIRT)) {
                    world.setBlock(blockPos, Blocks.MUD_BRICKS.defaultBlockState(), 2);
                    break;
                }
            }
        }
        else if(blockPos.getY() <= deepslateLevel) {
            if(state.is(BlockTags.STONE_BRICKS))
                world.setBlock(blockPos, deepslateBricks.get(rand.nextInt(2)).defaultBlockState(), 2);
            else if(state.is(Blocks.STONE_BRICK_SLAB))
                world.setBlock(blockPos, Blocks.DEEPSLATE_BRICK_SLAB.defaultBlockState().setValue(SlabBlock.TYPE, SlabType.TOP), 2);
        };
    }

}
