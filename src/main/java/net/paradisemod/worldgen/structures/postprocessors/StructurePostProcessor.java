package net.paradisemod.worldgen.structures.postprocessors;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraftforge.event.ForgeEventFactory;
import net.paradisemod.base.PMConfig;

public abstract class StructurePostProcessor {
    protected final BlockPos minPos;
    protected final BlockPos farCorner;
    private Registry<PlacedFeature> placedFeatureRegistry;
    private Registry<ConfiguredFeature<?, ?>> featureRegistry;

    protected StructurePostProcessor(BlockPos minPos, BlockPos farCorner) {
        this.minPos = minPos;
        this.farCorner = farCorner;
    }

    public final void run(WorldGenLevel world, ChunkGenerator generator, RandomSource rand) {
        placedFeatureRegistry = world.getLevel().registryAccess().registryOrThrow(Registries.PLACED_FEATURE);
        featureRegistry = world.getLevel().registryAccess().registryOrThrow(Registries.CONFIGURED_FEATURE);

        for(var blockPos : BlockPos.betweenClosed(minPos, farCorner)) {
            var state = world.getBlockState(blockPos);

            if(state.hasProperty(BlockStateProperties.WATERLOGGED))
                world.setBlock(blockPos, state.setValue(BlockStateProperties.WATERLOGGED, false), 16);

            postProcessBlock(world, generator, rand, world.getBlockState(blockPos), blockPos);

            if(state.isAir() && PMConfig.SETTINGS.structures.debugMode.get())
                world.setBlock(blockPos, Blocks.GLASS.defaultBlockState(), 16);
        }

        if(PMConfig.SETTINGS.structures.debugMode.get()) {
            world.setBlock(minPos, Blocks.LIME_STAINED_GLASS.defaultBlockState(), 16);
            world.setBlock(farCorner, Blocks.LIME_STAINED_GLASS.defaultBlockState(), 16);
        }
    }

    /** Override this to place additional blocks and generate features in the structure */
    protected abstract void postProcessBlock(WorldGenLevel world, ChunkGenerator generator, RandomSource rand, BlockState state, BlockPos blockPos);

    protected final Holder<PlacedFeature> getPlacedFeature(ResourceKey<PlacedFeature> key) {
        return placedFeatureRegistry.getHolderOrThrow(key);
    }

    protected final Holder<ConfiguredFeature<?, ?>> getFeature(ResourceKey<ConfiguredFeature<?, ?>> key) {
        return featureRegistry.getHolderOrThrow(key);
    }

    protected final <T extends Mob> void placeMobAtPos(WorldGenLevel world, EntityType<T> mobType, BlockPos mobPos) {
        var mob = mobType.create(world.getLevel());
        mob.moveTo(mobPos.getX() + 0.5D, mobPos.getY(), mobPos.getZ() + 0.5D, 0.0F, 0.0F);
        ForgeEventFactory.onFinalizeSpawn(mob, world, world.getCurrentDifficultyAt(minPos), MobSpawnType.STRUCTURE, null, null);
        world.addFreshEntityWithPassengers(mob);
    }

    protected final <T extends Mob> void placeMobAtPos(WorldGenLevel world, T mob, BlockPos mobPos) {
        mob.moveTo(mobPos.getX() + 0.5D, mobPos.getY(), mobPos.getZ() + 0.5D, 0.0F, 0.0F);
        ForgeEventFactory.onFinalizeSpawn(mob, world, world.getCurrentDifficultyAt(minPos), MobSpawnType.STRUCTURE, null, null);
        world.addFreshEntityWithPassengers(mob);
    }

    @FunctionalInterface
    public interface Constructor {
        StructurePostProcessor construct(BlockPos minPos, BlockPos farCorner);
    }
}
