package net.paradisemod.worldgen.structures;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FenceGateBlock;
import net.minecraft.world.level.block.LeverBlock;
import net.minecraft.world.level.block.RedstoneLampBlock;
import net.minecraft.world.level.block.TrapDoorBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.AttachFace;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.Half;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.paradisemod.base.PMConfig;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.building.FenceGates;
import net.paradisemod.building.Fences;
import net.paradisemod.building.Trapdoors;
import net.paradisemod.redstone.Redstone;
import net.paradisemod.world.Ores;
import net.paradisemod.world.PMWorld;
import net.paradisemod.worldgen.features.BasicFeature;

public class Buoy extends BasicFeature {
    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        if(PMConfig.SETTINGS.structures.buoys.shouldGenerate(rand)) {
            var newPos = pos.atY(62);
            var trapdoor = Blocks.OAK_TRAPDOOR;
            var fence = Blocks.OAK_FENCE;
            var fenceGate = Blocks.OAK_FENCE_GATE;

            switch(rand.nextInt(3)) {
                case 0:
                    trapdoor = Trapdoors.RUSTED_IRON_TRAPDOOR.get();
                    fence = Fences.RUSTED_IRON_FENCE.get();
                    fenceGate = FenceGates.RUSTED_IRON_FENCE_GATE.get();

                case 1:
                    var lanterns = Utils.getBlockTag(PMTags.Blocks.BUOY_LANTERNS);
                    if(lanterns.isEmpty()) return false;
                    for (var x = 0; x <= 2; x++)
                        for (var z = 0; z <= 2; z++)
                            world.setBlock(newPos.offset(x, 0, z), trapdoor.defaultBlockState().setValue(BlockStateProperties.WATERLOGGED, true).setValue(TrapDoorBlock.HALF, Half.TOP).setValue(TrapDoorBlock.FACING, Direction.Plane.HORIZONTAL.getRandomDirection(rand)), 8);

                    for (var x = 0; x <= 2; x++)
                        for (var z = 0; z <= 2; z++)
                            world.setBlock(newPos.offset(x, 1, z), fence.defaultBlockState(), 8);

                    world.setBlock(newPos.offset(1, 1, 1), lanterns.getRandomElement(rand).get().defaultBlockState(), 8);
                    world.setBlock(newPos.offset(1, 1, 0), fenceGate.defaultBlockState().setValue(FenceGateBlock.FACING, Direction.NORTH), 8);
                    PMWorld.updateBlockStates(world, newPos, 3, 2, 3);
                    return true;

                case 2: default:
                    var lights = Utils.getBlockTag(PMTags.Blocks.BUOY_LIGHTS);
                    if(lights.isEmpty()) return false;

                    for (var x = 0; x <= 5; x++)
                        for (var z = 0; z <= 5; z++)
                            world.setBlock(newPos.offset(x, 1, z), Fences.RUSTED_IRON_FENCE.get().defaultBlockState(), 8);

                    for (var x = 1; x <= 4; x++)
                        for (var z = 1; z <= 4; z++)
                            world.setBlock(newPos.offset(x, 1, z), Blocks.AIR.defaultBlockState(), 8);


                    for (var x = 0; x <= 5; x++)
                        for (var z = 0; z <= 5; z++)
                            world.setBlock(newPos.offset(x, 0, z), Trapdoors.RUSTED_IRON_TRAPDOOR.get().defaultBlockState().setValue(BlockStateProperties.WATERLOGGED, true).setValue(TrapDoorBlock.HALF, Half.TOP).setValue(TrapDoorBlock.FACING, Direction.Plane.HORIZONTAL.getRandomDirection(rand)), 8);

                    world.setBlock(newPos.offset(2, 1, 5), FenceGates.RUSTED_IRON_FENCE_GATE.get().defaultBlockState().setValue(FenceGateBlock.FACING, Direction.NORTH), 8);
                    var lamp = lights.getRandomElement(rand).get();

                    for(var x = 2; x <= 3; x++)
                        for (var z = 2; z <= 3; z++)
                            for (var by = 1; by <= 2; by++)
                                world.setBlock(newPos.offset(x, by, z), lamp.defaultBlockState().setValue(RedstoneLampBlock.LIT, true), 8);

                    for(var x = 2; x <= 3; x++)
                        for(var z = 2; z <= 3; z++)
                            world.setBlock(newPos.offset(x, 3, z), genLever(rand), 8);

                    for(var x = 2; x <= 3; x++)
                        for (var z = 2; z <= 3; z++)
                            for (var by = -1; by <= 0; by++)
                                world.setBlock(newPos.offset(x, by, z), Ores.RUSTED_IRON_BLOCK.get().defaultBlockState(), 8);

                    PMWorld.updateBlockStates(world, newPos, 6, 2, 6);
                    return true;
            }
        }

        return false;
    }

    private static BlockState genLever(RandomSource rand) {
        if(rand.nextBoolean())
            return Blocks.LEVER.defaultBlockState().setValue(LeverBlock.FACE, AttachFace.FLOOR).setValue(LeverBlock.POWERED, true).setValue(LeverBlock.FACING, Direction.Plane.HORIZONTAL.getRandomDirection(rand));
        return Redstone.MOSSY_COBBLESTONE_LEVER.get().defaultBlockState().setValue(LeverBlock.FACE, AttachFace.FLOOR).setValue(LeverBlock.POWERED, true).setValue(LeverBlock.FACING, Direction.Plane.HORIZONTAL.getRandomDirection(rand));
    }
}