package net.paradisemod.worldgen.structures;

import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.PMConfig;
import net.paradisemod.world.PMWorld;

public class CreatorHeads extends SmallStructure {
    private static final ResourceLocation CHAOS_HEAD = new ResourceLocation(ParadiseMod.ID, "chaos_head");
    private static final ResourceLocation NETHERNOAH_HEAD = new ResourceLocation(ParadiseMod.ID, "nethernoah_head");

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        if(PMConfig.SETTINGS.structures.creatorMonuments.shouldGenerate(rand)) {
            var location = rand.nextBoolean() ? CHAOS_HEAD : NETHERNOAH_HEAD;
            var template = world.getLevel().getStructureManager().get(location).get();
            var size = template.getSize();
            var level = PMWorld.getLowestY(world, pos, pos.offset(size)) + 1;
            if (level < 63) return false; // if we're generating on the ground, the y level should never be below sea level
            var newPos = new BlockPos(pos.getX(), level, pos.getZ());
            if(!checkArea(world, newPos, size)) return false;
            return genStructureFromTemplate(template, world, rand, newPos, generator, false);
        }

        return false;
    }
}