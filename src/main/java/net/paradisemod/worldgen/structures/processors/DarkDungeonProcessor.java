package net.paradisemod.worldgen.structures.processors;

import java.util.List;

import javax.annotation.Nullable;

import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.CrossCollisionBlock;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessorType;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
import net.paradisemod.building.Doors;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.decoration.Tables;
import net.paradisemod.worldgen.features.BasicFeature;
import net.paradisemod.worldgen.structures.PMStructures;

public class DarkDungeonProcessor extends StructureProcessor {
    public static final Codec<DarkDungeonProcessor> CODEC = Codec.unit(DarkDungeonProcessor::new);

    public static final List<Block> ALL_CONCRETE = List.of(
        Blocks.BLACK_CONCRETE,
        Blocks.BROWN_CONCRETE,
        Blocks.RED_CONCRETE,
        Blocks.ORANGE_CONCRETE,
        Blocks.YELLOW_CONCRETE,
        Blocks.LIME_CONCRETE,
        Blocks.GREEN_CONCRETE,
        Blocks.CYAN_CONCRETE,
        Blocks.LIGHT_BLUE_CONCRETE,
        Blocks.BLUE_CONCRETE,
        Blocks.PURPLE_CONCRETE,
        Blocks.MAGENTA_CONCRETE,
        Blocks.PINK_CONCRETE
    );

    private @Nullable ConcreteColors colors = null;

    @Nullable
    @Override
    public StructureTemplate.StructureBlockInfo process(
        LevelReader world,
        BlockPos structPos,
        BlockPos blockPos,
        StructureTemplate.StructureBlockInfo blockInfo,
        StructureTemplate.StructureBlockInfo relativeBlockInfo,
        StructurePlaceSettings settings,
        StructureTemplate template
    ) {
        var worldPos = relativeBlockInfo.pos();
        var rand = settings.getRandom(relativeBlockInfo.pos());

        if(colors == null) {
            var allSchemes = ConcreteColors.values();
            colors = allSchemes[rand.nextInt(allSchemes.length)];
        }

        var state = relativeBlockInfo.state();
        var nbt = relativeBlockInfo.nbt();
        var biome = world.getBiome(worldPos);
        var wood = BasicFeature.getWood(rand, biome);
        var table = wood.get(2);
        var door = wood.get(7);
        var bookShelf = wood.get(10);
        
        if(state.is(Blocks.WHITE_CONCRETE))
            state = colors.getConcrete(rand).defaultBlockState();
        else if(state.is(Blocks.IRON_BARS) && rand.nextBoolean()) {
            var east = state.getValue(CrossCollisionBlock.EAST);
            var west = state.getValue(CrossCollisionBlock.WEST);
            var north = state.getValue(CrossCollisionBlock.NORTH);
            var south = state.getValue(CrossCollisionBlock.SOUTH);
            state = Decoration.RUSTED_IRON_BARS.get()
                .defaultBlockState()
                .setValue(CrossCollisionBlock.EAST, east)
                .setValue(CrossCollisionBlock.WEST, west)
                .setValue(CrossCollisionBlock.NORTH, north)
                .setValue(CrossCollisionBlock.SOUTH, south);
        }
        else if(state.is(Doors.BLACKENED_OAK_DOOR.get())) {
            var facing = state.getValue(DoorBlock.FACING);
            var open = state.getValue(DoorBlock.OPEN);
            var hinge = state.getValue(DoorBlock.HINGE);
            var half = state.getValue(DoorBlock.HALF);
            state = door.defaultBlockState()
                .setValue(DoorBlock.FACING, facing)
                .setValue(DoorBlock.OPEN, open)
                .setValue(DoorBlock.HINGE, hinge)
                .setValue(DoorBlock.HALF, half);
        }
        else if(state.is(Tables.BLACKENED_OAK_CRAFTING_TABLE.get()))
            state = table.defaultBlockState();
        else if(state.is(Decoration.BLACKENED_OAK_BOOKSHELF.get()))
            state = bookShelf.defaultBlockState();

        return new StructureTemplate.StructureBlockInfo(worldPos, state, nbt);
    }

    @Override
    protected StructureProcessorType<DarkDungeonProcessor> getType() { return PMStructures.DARK_DUNGEON_PROCESSOR.get(); }

    private enum ConcreteColors {
        RED_WHITE_BLUE,
        ARIZONA_COLORS,
        ALL;

        private Block getConcrete(RandomSource rand) {
            return switch(this) {
                case RED_WHITE_BLUE -> {
                    Block[] concrete = {Blocks.RED_CONCRETE, Blocks.WHITE_CONCRETE, Blocks.BLUE_CONCRETE};
                    yield concrete[rand.nextInt(3)];
                }
                case ARIZONA_COLORS -> {
                    Block[] concrete = {Blocks.RED_CONCRETE, Blocks.BLUE_CONCRETE, Blocks.YELLOW_CONCRETE, Blocks.ORANGE_CONCRETE};
                    yield concrete[rand.nextInt(4)];
                }

                case ALL -> ALL_CONCRETE.get(rand.nextInt(ALL_CONCRETE.size()));
            };
        }
    }
}
