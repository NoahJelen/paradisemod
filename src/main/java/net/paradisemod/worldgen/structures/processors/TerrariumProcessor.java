package net.paradisemod.worldgen.structures.processors;

import java.util.List;

import org.jetbrains.annotations.Nullable;

import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.tags.BiomeTags;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.BedBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.RedstoneLampBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessorType;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.misc.Misc;
import net.paradisemod.worldgen.features.BasicFeature;
import net.paradisemod.worldgen.structures.PMStructures;

public class TerrariumProcessor extends StructureProcessor {
    public static final Codec<TerrariumProcessor> CODEC = Codec.unit(TerrariumProcessor::new);

    @Nullable
    @Override
    public StructureTemplate.StructureBlockInfo process(
        LevelReader world,
        BlockPos structPos,
        BlockPos blockPos,
        StructureTemplate.StructureBlockInfo blockInfo,
        StructureTemplate.StructureBlockInfo relativeBlockInfo,
        StructurePlaceSettings settings,
        StructureTemplate template
    ) {

        var state = relativeBlockInfo.state();
        var worldPos = relativeBlockInfo.pos();
        var biome = world.getBiome(worldPos);
        var rand = settings.getRandom(relativeBlockInfo.pos());
        var nbt = relativeBlockInfo.nbt();
        var stoneBricks = List.of(Blocks.STONE_BRICKS, Blocks.MOSSY_STONE_BRICKS, Blocks.CRACKED_STONE_BRICKS);
        var coloredBlocks = BasicFeature.getColoredBlocks(rand, biome);
        var lantern = coloredBlocks.get(1);
        var lamp = coloredBlocks.get(2);
        var bed = coloredBlocks.get(3);
        var wood = BasicFeature.getWood(rand, biome);
        var table = wood.get(2);
        var planks = wood.get(5);
        var door = wood.get(7);
        var strippedLog = wood.get(11);
        var slab = wood.get(12);

        if(state.is(Blocks.STONE_BRICKS)) state = stoneBricks.get(rand.nextInt(stoneBricks.size())).defaultBlockState();
        else if(state.is(Blocks.GRASS_BLOCK) && rand.nextInt(5) == 0)
            state = getLight(biome).defaultBlockState();
        else if(state.is(Blocks.CRAFTING_TABLE)) state = table.defaultBlockState();
        else if(state.is(Blocks.OAK_PLANKS)) state = planks.defaultBlockState();
        else if(state.is(Blocks.OAK_SLAB)) {
            var type = state.getValue(SlabBlock.TYPE);
            state = slab.defaultBlockState()
                .setValue(SlabBlock.TYPE, type)
                .setValue(BlockStateProperties.WATERLOGGED, false);
        }
        else if(state.is(Blocks.OAK_DOOR)) {
            var facing = state.getValue(DoorBlock.FACING);
            var open = state.getValue(DoorBlock.OPEN);
            var hinge = state.getValue(DoorBlock.HINGE);
            var half = state.getValue(DoorBlock.HALF);
            state = door.defaultBlockState()
                .setValue(DoorBlock.FACING, facing)
                .setValue(DoorBlock.OPEN, open)
                .setValue(DoorBlock.HINGE, hinge)
                .setValue(DoorBlock.HALF, half);
        }
        else if(state.is(Blocks.REDSTONE_LAMP))
            state = lamp.defaultBlockState().setValue(RedstoneLampBlock.LIT, true);
        else if(state.is(Blocks.LANTERN)) state = lantern.defaultBlockState();
        else if(state.is(Blocks.RED_BED)) {
            var facing = state.getValue(BedBlock.FACING);
            var part = state.getValue(BedBlock.PART);
            state = bed.defaultBlockState()
                .setValue(BedBlock.PART, part)
                .setValue(BedBlock.FACING, facing);
        }
        else if(state.is(Blocks.STRIPPED_OAK_LOG)) state = strippedLog.defaultBlockState();

        return new StructureTemplate.StructureBlockInfo(worldPos, state, nbt);
    }

    @Override
    protected StructureProcessorType<TerrariumProcessor> getType() { return PMStructures.TERRARIUM_PROCESSOR.get(); }

    private static Block getLight(Holder<Biome> biome) {
        if(biome.is(PMTags.Biomes.SALT)) return Misc.SALT_LAMP.get();
        else if(biome.is(BiomeTags.IS_OCEAN)) return Blocks.SEA_LANTERN;
        else return Blocks.GLOWSTONE;
    }
}