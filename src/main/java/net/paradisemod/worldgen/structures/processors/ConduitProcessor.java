package net.paradisemod.worldgen.structures.processors;

import javax.annotation.Nullable;

import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessorType;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate.StructureBlockInfo;
import net.paradisemod.worldgen.structures.PMStructures;

public class ConduitProcessor extends StructureProcessor {
    public static final Codec<ConduitProcessor> CODEC = Codec.unit(ConduitProcessor::new);

    @Override
    @Nullable
    public StructureBlockInfo processBlock(LevelReader world, BlockPos offset, BlockPos pos, StructureBlockInfo blockInfo, StructureBlockInfo relativeBlockInfo, StructurePlaceSettings settings) {
        var state = relativeBlockInfo.state();
        var worldPos = relativeBlockInfo.pos();
        var rand = settings.getRandom(relativeBlockInfo.pos());
        var nbt = relativeBlockInfo.nbt();

        if(state.is(Blocks.PRISMARINE_BRICKS) && rand.nextInt(10) == 0)
            state = (rand.nextBoolean() ? Blocks.PRISMARINE : Blocks.DARK_PRISMARINE).defaultBlockState();

        return new StructureTemplate.StructureBlockInfo(worldPos, state, nbt);
    }

    @Override
    protected StructureProcessorType<?> getType() {
        return PMStructures.CONDUIT_PROCESSOR.get();
    }
}