package net.paradisemod.worldgen.structures.processors;

import java.util.List;

import org.jetbrains.annotations.Nullable;

import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FurnaceBlock;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessorType;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
import net.paradisemod.automation.Automation;
import net.paradisemod.worldgen.structures.PMStructures;

public class BrickDungeonProcessor extends StructureProcessor {
    public static final Codec<BrickDungeonProcessor> CODEC = Codec.unit(BrickDungeonProcessor::new);

    @Nullable
    @Override
    public StructureTemplate.StructureBlockInfo process(
        LevelReader world,
        BlockPos structPos,
        BlockPos blockPos,
        StructureTemplate.StructureBlockInfo blockInfo,
        StructureTemplate.StructureBlockInfo relativeBlockInfo,
        StructurePlaceSettings settings,
        StructureTemplate template
    ) {
        var state = relativeBlockInfo.state();
        var worldPos = relativeBlockInfo.pos();
        var rand = settings.getRandom(relativeBlockInfo.pos());
        var nbt = relativeBlockInfo.nbt();
        var stoneBricks = List.of(Blocks.STONE_BRICKS, Blocks.MOSSY_STONE_BRICKS, Blocks.CRACKED_STONE_BRICKS);
        if(state.is(Blocks.STONE_BRICKS)) state = stoneBricks.get(rand.nextInt(3)).defaultBlockState();
        else if(state.is(Blocks.FURNACE) && rand.nextBoolean()) {
            var facing = state.getValue(FurnaceBlock.FACING);
            state = Automation.MOSSY_COBBLESTONE_FURNACE.get().defaultBlockState().setValue(FurnaceBlock.FACING, facing);
        }

        return new StructureTemplate.StructureBlockInfo(worldPos, state, nbt);
    }

    @Override
    protected StructureProcessorType<BrickDungeonProcessor> getType() { return PMStructures.BRICK_DUNGEON_PROCESSOR.get(); }
}