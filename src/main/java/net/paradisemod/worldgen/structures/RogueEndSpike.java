package net.paradisemod.worldgen.structures;

import net.minecraft.core.BlockPos;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.IronBarsBlock;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.paradisemod.base.PMConfig;
import net.paradisemod.worldgen.features.BasicFeature;

public class RogueEndSpike extends BasicFeature {
    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        if(
            (pos.getX() < 500 && pos.getZ() < 500 && pos.getX() > -500 && pos.getZ() > -500) ||
            !PMConfig.SETTINGS.betterEnd.rogueSpike.shouldGenerate(rand)
        )
            return false;

        var radius = 3 + rand.nextInt(4);
        var height = 80 + rand.nextInt(10);
        var isGuarded = rand.nextBoolean();

        // generate the pillar
        for(var blockpos : BlockPos.betweenClosed(new BlockPos(pos.getX() - radius, world.getMinBuildHeight(), pos.getZ() - radius), new BlockPos(pos.getX() + radius,height + 10, pos.getZ() + radius)))
            if(blockpos.distToLowCornerSqr(pos.getX(), blockpos.getY(), pos.getZ()) <= (radius * radius + 1) && blockpos.getY() < height)
                world.setBlock(blockpos, Blocks.OBSIDIAN.defaultBlockState(), 32);

        // generate the iron bar cage if there is one
        if(isGuarded) {
            var barsPos = new BlockPos.MutableBlockPos();
            for(var k = -2; k <= 2; k++)
                for(var l = -2; l <= 2; l++)
                    for(var i1 = 0; i1 <= 3; i1++) {
                        var flag = Mth.abs(k) == 2;
                        var flag1 = Mth.abs(l) == 2;
                        var flag2 = i1 == 3;
                        if (flag || flag1 || flag2) {
                            var flag3 = k == -2 || k == 2 || flag2;
                            var flag4 = l == -2 || l == 2 || flag2;
                            var bars = Blocks.IRON_BARS.defaultBlockState().setValue(IronBarsBlock.NORTH, flag3 && l != -2).setValue(IronBarsBlock.SOUTH, flag3 && l != 2).setValue(IronBarsBlock.WEST, flag4 && k != -2).setValue(IronBarsBlock.EAST, flag4 && k != 2);
                            world.setBlock(barsPos.set(pos.getX() + k, height + i1, pos.getZ() + l), bars, 32);
                        }
                    }
        }

        // spawn the ender crystal
        var endCrystal = EntityType.END_CRYSTAL.create(world.getLevel());
        endCrystal.moveTo(pos.getX() + .5D, height + 1, pos.getZ() + 0.5D, rand.nextFloat() * 360f, 0f);
        world.addFreshEntity(endCrystal);
        world.setBlock(new BlockPos(pos.getX(), height, pos.getZ()), Blocks.BEDROCK.defaultBlockState(), 32);
        return true;
    }
}