package net.paradisemod.worldgen.structures;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
import net.paradisemod.ParadiseMod;
import net.paradisemod.world.PMWorld;
import net.paradisemod.worldgen.features.BasicFeature;

public abstract class SmallStructure extends BasicFeature {
    protected void postProcessStructure(StructureTemplate template, WorldGenLevel world, RandomSource rand, BlockPos pos, StructurePlaceSettings settings, ChunkGenerator generator) { }

    protected final void postProcessByBlock(WorldGenLevel world, StructureTemplate template, BlockPos pos, BlockProcessor processor) {
        var size = template.getSize();
        postProcessByBlock(world, size.getX(), size.getY(), size.getZ(), pos, processor);
    }

    protected final void postProcessByBlock(WorldGenLevel world, int sizeX, int sizeY, int sizeZ, BlockPos pos, BlockProcessor processor) {
        for(var curPos : BlockPos.betweenClosed(pos, pos.offset(sizeX, sizeY, sizeZ)))
            processor.process(world.getBlockState(curPos), curPos);
    }

    protected final boolean genStructureFromTemplate(StructureTemplate template, WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator, boolean postProcess) {
        var settings = new StructurePlaceSettings();
        settings.setIgnoreEntities(false);

        // place the structure
        var success = template.placeInWorld(world, pos, pos, settings, rand, 2);

        if(postProcess && success)
            postProcessStructure(template, world, rand, pos, settings, generator);

        return success;
    }

    protected final boolean genStructureFromTemplate(ResourceLocation templateName, WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator, boolean postProcess) {
        var template = world.getLevel().getStructureManager().get(templateName).orElse(null);

        if(template == null) {
            ParadiseMod.LOG.error("Unable to generate " + templateName);
            ParadiseMod.LOG.error("Does the associated structure file exist?");
            return false;
        }

        return genStructureFromTemplate(template, world, rand, pos, generator, postProcess);
    }

    /** Checks if a structure has already generated */
    protected static boolean checkArea(WorldGenLevel world, BlockPos structurePos, Vec3i size) {
        for(var newPos : BlockPos.betweenClosed(structurePos, structurePos.offset(size)))
            if(PMWorld.doNotReplace(world, newPos))
                return false;

        return true;
    }

    @FunctionalInterface
    protected interface BlockProcessor {
        void process(BlockState curState, BlockPos curPos);
    }
}