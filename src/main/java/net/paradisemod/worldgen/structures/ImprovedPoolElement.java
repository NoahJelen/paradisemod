package net.paradisemod.worldgen.structures;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import com.mojang.datafixers.util.Either;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.DynamicOps;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.Vec3i;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.StructureManager;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.pools.StructurePoolElement;
import net.minecraft.world.level.levelgen.structure.pools.StructurePoolElementType;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;
import net.minecraft.world.level.levelgen.structure.templatesystem.BlockIgnoreProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.JigsawReplacementProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessorList;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessorType;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate.StructureBlockInfo;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplateManager;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.PMConfig;

public class ImprovedPoolElement extends StructurePoolElement {
    private static final Codec<Either<ResourceLocation, StructureTemplate>> TEMPLATE_CODEC = Codec.of(
        ImprovedPoolElement::encodeTemplate,
        ResourceLocation.CODEC.map(Either::left)
    );

    public static final Codec<ImprovedPoolElement> CODEC = RecordCodecBuilder.create(
        builder -> builder.group(
            templateCodec(),
            processorsCodec(),
            projectionCodec(),
            Codec.STRING.optionalFieldOf("post_processor")
                .forGetter(element -> element.postProcessorName)
        )
            .apply(builder, ImprovedPoolElement::new)
    );

    protected final Either<ResourceLocation, StructureTemplate> template;
    protected final Holder<StructureProcessorList> processors;
    private final Optional<String> postProcessorName;

    protected ImprovedPoolElement(Either<ResourceLocation, StructureTemplate> template, Holder<StructureProcessorList> processors, StructureTemplatePool.Projection projection, Optional<String> postProcessorName) {
        super(projection);
        this.template = template;
        this.processors = processors;
        this.postProcessorName = postProcessorName;
    }

    @Override
    public Vec3i getSize(StructureTemplateManager structureTemplateManager, Rotation rotation) {
        var template = getTemplate(structureTemplateManager);
        return template.getSize(rotation);
    }

    @Override
    public List<StructureBlockInfo> getShuffledJigsawBlocks(StructureTemplateManager structureTemplateManager, BlockPos pos, Rotation rotation, RandomSource random) {
        var structuretemplate = getTemplate(structureTemplateManager);
        var jigsaws = structuretemplate.filterBlocks(pos, new StructurePlaceSettings().setRotation(rotation), Blocks.JIGSAW, true);
        Util.shuffle(jigsaws, random);
        return jigsaws;
    }

    @Override
    public BoundingBox getBoundingBox(StructureTemplateManager structureTemplateManager, BlockPos pos, Rotation rotation) {
        var template = getTemplate(structureTemplateManager);
        return template.getBoundingBox(new StructurePlaceSettings().setRotation(rotation), pos);
    }

    @Override
    public boolean place(StructureTemplateManager structureTemplateManager, WorldGenLevel world, StructureManager structureManager, ChunkGenerator generator, BlockPos offset, BlockPos pos, Rotation rotation, BoundingBox chunkBox, RandomSource rand, boolean keepJigsaws) {
        var template = getTemplate(structureTemplateManager);
        var placeSettings = getSettings(rotation, chunkBox, keepJigsaws);
        var box = template.getBoundingBox(placeSettings, offset);

        if(template.placeInWorld(world, offset, pos, placeSettings, rand, 2)) {
            if(postProcessorName.isPresent()) {
                if(PMConfig.SETTINGS.structures.debugMode.get()) {
                    world.setBlock(pos, Blocks.RED_STAINED_GLASS.defaultBlockState(), 16);
                    world.setBlock(offset, Blocks.BLUE_STAINED_GLASS.defaultBlockState(), 16);
                }

                var postProcessor = PMStructures.STRUCT_POST_PROCESSORS.get(postProcessorName.get());

                if(postProcessor != null) {
                    postProcessor.construct(
                        new BlockPos(box.minX(), box.minY(), box.minZ()),
                        new BlockPos(box.maxX(), box.maxY(), box.maxZ())
                    )
                        .run(world, generator, rand);
                }
                else {
                    ParadiseMod.LOG.error("Structure post processor \"" + postProcessorName.get() + "\" doesn't seem to exist!");
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    @Override
    public StructurePoolElementType<ImprovedPoolElement> getType() {
        return PMStructures.IMPROVED_ELEMENT.get();
    }

    @Override
    public String toString() {
        return "ImprovedElement[" + this.template + "]";
    }

    private StructurePlaceSettings getSettings(Rotation rotation, BoundingBox boundingBox, boolean offset) {
        var placeSettings = new StructurePlaceSettings()
            .setBoundingBox(boundingBox)
            .setRotation(rotation)
            .setKnownShape(true)
            .setIgnoreEntities(false)
            .addProcessor(BlockIgnoreProcessor.STRUCTURE_BLOCK)
            .setFinalizeEntities(true);

        if (!offset)
            placeSettings.addProcessor(JigsawReplacementProcessor.INSTANCE);

        processors.value().list().forEach(placeSettings::addProcessor);
        getProjection().getProcessors().forEach(placeSettings::addProcessor);
        return placeSettings;
    }

    private StructureTemplate getTemplate(StructureTemplateManager structureTemplateManager) {
        return template.map(structureTemplateManager::getOrCreate, Function.identity());
    }

    private static <T> DataResult<T> encodeTemplate(Either<ResourceLocation, StructureTemplate> template, DynamicOps<T> options, T element) {
        Optional<ResourceLocation> optional = template.left();
        return !optional.isPresent() ? DataResult.error(() -> "Can not serialize a runtime pool element") : ResourceLocation.CODEC.encode(optional.get(), options, element);
    }

    private static <E extends ImprovedPoolElement> RecordCodecBuilder<E, Holder<StructureProcessorList>> processorsCodec() {
        return StructureProcessorType.LIST_CODEC.fieldOf("processors").forGetter(element -> element.processors);
    }

    private static <E extends ImprovedPoolElement> RecordCodecBuilder<E, Either<ResourceLocation, StructureTemplate>> templateCodec() {
        return TEMPLATE_CODEC.fieldOf("location").forGetter(element -> element.template);
    }

    public static Function<StructureTemplatePool.Projection, ImprovedPoolElement> create(String id) {
        return projection -> new ImprovedPoolElement(Either.left(new ResourceLocation(id)), new Holder.Direct<>(new StructureProcessorList(List.of())), projection, Optional.empty());
    }

    public static Function<StructureTemplatePool.Projection, ImprovedPoolElement> create(String id, String postProcessorName, Holder<StructureProcessorList> processors) {
        return projection -> new ImprovedPoolElement(Either.left(new ResourceLocation(id)), processors, projection, Optional.of(postProcessorName));
    }

    public static Function<StructureTemplatePool.Projection, ImprovedPoolElement> create(String id, Optional<String> postProcessorName, Holder<StructureProcessorList> processors) {
        return projection -> new ImprovedPoolElement(Either.left(new ResourceLocation(id)), processors, projection, postProcessorName);
    }

    public static Function<StructureTemplatePool.Projection, ImprovedPoolElement> create(String id, String postProcessorName) {
        return projection -> new ImprovedPoolElement(Either.left(new ResourceLocation(id)), new Holder.Direct<>(new StructureProcessorList(List.of())),  projection, Optional.of(postProcessorName));
    }
}