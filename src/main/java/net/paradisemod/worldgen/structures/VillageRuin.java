package net.paradisemod.worldgen.structures;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BiomeTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.FenceGateBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.VineBlock;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
import net.minecraftforge.common.Tags;
import net.paradisemod.base.PMConfig;
import net.paradisemod.world.PMWorld;

public class VillageRuin extends SmallStructure {
    private static final String[] RUINS = {
        "paradisemod:ruins/blacksmith",
        "paradisemod:ruins/house_1",
        "paradisemod:ruins/house_2",
        "paradisemod:ruins/large_house_1",
        "paradisemod:ruins/large_house_2",
        "paradisemod:ruins/library",
        "paradisemod:ruins/well"
    };

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        var settings = PMConfig.SETTINGS.structures.ruins;
        if(settings.shouldGenerate(rand)) {
            var name = RUINS[rand.nextInt(RUINS.length)];
            var location = new ResourceLocation(name);
            var template = world.getLevel().getStructureManager().get(location).get();
            var size = template.getSize();

            if (
                checkArea(world, pos, size) &&
                isFlat(world, pos, size.getX(), size.getZ(), false)
            ) {
                if (name.equals("paradisemod:ruins/well"))
                    return genStructureFromTemplate(template, world, rand, pos.below(12), generator, true);
                else return genStructureFromTemplate(template, world, rand, pos, generator, true);
            }
        }

        return false;
    }

    @Override
    protected void postProcessStructure(StructureTemplate template, WorldGenLevel world, RandomSource rand, BlockPos pos, StructurePlaceSettings settings, ChunkGenerator generator) {
        var sizeX = template.getSize().getX();
        var sizeY = template.getSize().getY();
        var sizeZ = template.getSize().getZ();
        var biome = world.getBiome(pos);
        var wood = getWood(rand, biome);
        var fence = wood.get(0);
        var fenceGate = wood.get(1);
        var table = wood.get(2);
        var log = wood.get(3);
        var plate = wood.get(4);
        var planks = wood.get(5);
        var stairs = wood.get(6);
        var door = wood.get(7);
        var bookShelf = wood.get(10);

        postProcessByBlock(
            world, template, pos,
            (curState, curPos) -> {
                var curStateAbove = world.getBlockState(curPos.above());

                if (curState.is(Blocks.WATER) && curStateAbove.is(Blocks.AIR) && rand.nextInt(7) == 0)
                    world.setBlock(curPos.above(), Blocks.LILY_PAD.defaultBlockState(), 32);

                if (curState.canOcclude() && curStateAbove.is(Blocks.WATER) && rand.nextInt(7) == 0)
                    world.setBlock(curPos.above(), Blocks.SEAGRASS.defaultBlockState(), 32);

                if(rand.nextInt(10) == 0 && (biome.is(BiomeTags.IS_JUNGLE) || biome.is(Tags.Biomes.IS_SWAMP))) {
                    if(curState.canOcclude()) {
                        for(var direction : Direction.Plane.HORIZONTAL) {
                            if (world.getBlockState(curPos.relative(direction)).isAir()) {
                                switch(direction) {
                                    case EAST ->
                                        world.setBlock(curPos.east(), Blocks.VINE.defaultBlockState().setValue(VineBlock.WEST, true), 32);

                                    case WEST ->
                                        world.setBlock(curPos.west(), Blocks.VINE.defaultBlockState().setValue(VineBlock.EAST, true), 32);

                                    case NORTH ->
                                        world.setBlock(curPos.north(), Blocks.VINE.defaultBlockState().setValue(VineBlock.SOUTH, true), 32);

                                    case SOUTH ->
                                        world.setBlock(curPos.south(), Blocks.VINE.defaultBlockState().setValue(VineBlock.NORTH, true), 32);
                                }
                            }
                        }
                    }
                }

                if(curState.is(Blocks.OAK_DOOR)) {
                    var facing = curState.getValue(DoorBlock.FACING);
                    var open = curState.getValue(DoorBlock.OPEN);
                    var hinge = curState.getValue(DoorBlock.HINGE);
                    var half = curState.getValue(DoorBlock.HALF);

                    if(half == DoubleBlockHalf.LOWER) {
                        world.setBlock(curPos, door.defaultBlockState().setValue(DoorBlock.FACING, facing).setValue(DoorBlock.OPEN, open).setValue(DoorBlock.HINGE, hinge).setValue(DoorBlock.HALF, DoubleBlockHalf.LOWER), 32);
                        world.setBlock(curPos.above(), door.defaultBlockState().setValue(DoorBlock.FACING, facing).setValue(DoorBlock.OPEN, open).setValue(DoorBlock.HINGE, hinge).setValue(DoorBlock.HALF, DoubleBlockHalf.UPPER), 32);
                    }
                }

                if(curState.is(Blocks.OAK_STAIRS)) {
                    var facing = curState.getValue(StairBlock.FACING);
                    var half = curState.getValue(StairBlock.HALF);
                    var shape = curState.getValue(StairBlock.SHAPE);
                    world.setBlock(curPos, stairs.defaultBlockState().setValue(StairBlock.FACING, facing).setValue(StairBlock.HALF, half).setValue(StairBlock.SHAPE, shape).setValue(BlockStateProperties.WATERLOGGED, false), 32);
                }

                if(curState.is(Blocks.OAK_PLANKS))
                    world.setBlock(curPos, planks.defaultBlockState(), 32);

                if(curState.is(Blocks.BOOKSHELF))
                    world.setBlock(curPos, bookShelf.defaultBlockState(), 32);

                if(curState.is(Blocks.OAK_PRESSURE_PLATE))
                    world.setBlock(curPos, plate.defaultBlockState(), 32);

                if(curState.is(Blocks.OAK_LOG))
                    world.setBlock(curPos, log.defaultBlockState(), 32);

                if(curState.is(Blocks.CRAFTING_TABLE))
                    world.setBlock(curPos, table.defaultBlockState(), 32);

                if(curState.is(Blocks.OAK_FENCE))
                    world.setBlock(curPos, fence.defaultBlockState(), 32);

                if(curState.is(Blocks.OAK_FENCE_GATE)) {
                    var facing = curState.getValue(FenceGateBlock.FACING);
                    world.setBlock(curPos, fenceGate.defaultBlockState().setValue(FenceGateBlock.FACING, facing), 32);
                }

                if(curState.is(Blocks.COBBLESTONE) && rand.nextInt(3) == 0)
                    world.setBlock(curPos, Blocks.MOSSY_COBBLESTONE.defaultBlockState(), 32);

                if(world.getBlockState(pos).isAir() && rand.nextInt(5) == 0)
                    world.setBlock(pos, Blocks.COBWEB.defaultBlockState(), 32);
            }
        );

        PMWorld.updateBlockStates(world, pos, sizeX, sizeY, sizeZ);
    }
}