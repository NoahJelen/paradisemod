package net.paradisemod.worldgen.structures;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SignBlock;
import net.minecraft.world.level.block.VineBlock;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.PMConfig;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.building.Building;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.world.PMWorld;
import net.paradisemod.worldgen.features.cave.PMCaveFeatures;

// structure that generates underground
public class UndergroundStructure extends SmallStructure {
    private final ResourceLocation structure;
    private final boolean overGrow;
    private final PMConfig.WorldGenOption settings;

    public UndergroundStructure(String name, boolean overGrow, PMConfig.WorldGenOption settings) {
        structure = new ResourceLocation(ParadiseMod.ID, name);
        this.settings = settings;
        this.overGrow = overGrow;
    }

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        if(settings.shouldGenerate(rand)) {
            var height = PMCaveFeatures.dimWorldgenLimit(world);
            var template = world.getLevel().getStructureManager().get(structure).get();
            var size = template.getSize();
            var newPos = pos.atY(rand.nextInt(-32, height - size.getY()));

            if(checkArea(world, newPos, size))
                return genStructureFromTemplate(template, world, rand, newPos, generator, overGrow);
        }

        return false;
    }

    @Override
    protected void postProcessStructure(StructureTemplate template, WorldGenLevel world, RandomSource rand, BlockPos pos, StructurePlaceSettings settings, ChunkGenerator generator) {
        var sizeX = template.getSize().getX();
        var sizeY = template.getSize().getY();
        var sizeZ = template.getSize().getZ();
        var carpet = rand.nextBoolean() ? new Block[]{Blocks.BLUE_CARPET, Blocks.WHITE_CARPET, Blocks.RED_CARPET} : new Block[] {Blocks.BLUE_CARPET, Blocks.YELLOW_CARPET, Blocks.ORANGE_CARPET, Blocks.RED_CARPET};

        postProcessByBlock(world, template, pos,
            (curState, curPos) -> {
                var curStateAbove = world.getBlockState(curPos.above());
                if(PMWorld.isFiller(world, curPos, false) || curState.getBlock() instanceof SignBlock) return;

                if(curState.is(Blocks.WATER) && curStateAbove.is(Blocks.AIR) && rand.nextInt(7) == 0)
                    world.setBlock(curPos.above(), Blocks.LILY_PAD.defaultBlockState(), 32);

                if(curState.canOcclude() && curStateAbove.is(Blocks.WATER) && rand.nextInt(7) == 0)
                    world.setBlock(curPos.above(), Blocks.SEAGRASS.defaultBlockState(), 32);

                if(curState.is(Blocks.COBBLESTONE) && rand.nextInt(3) == 0)
                    world.setBlock(curPos, Blocks.MOSSY_COBBLESTONE.defaultBlockState(), 32);

                if(curState.is(Blocks.STONE_BRICKS)) {
                    if(rand.nextInt(10) == 0) {
                        for(var direction : Direction.Plane.HORIZONTAL) {
                            if(world.getBlockState(curPos.relative(direction)).isAir()) {
                                switch (direction) {
                                    case EAST ->
                                        world.setBlock(curPos.east(), Blocks.VINE.defaultBlockState().setValue(VineBlock.WEST, true), 32);

                                    case WEST ->
                                        world.setBlock(curPos.west(), Blocks.VINE.defaultBlockState().setValue(VineBlock.EAST, true), 32);

                                    case NORTH ->
                                        world.setBlock(curPos.north(), Blocks.VINE.defaultBlockState().setValue(VineBlock.SOUTH, true), 32);

                                    case SOUTH ->
                                        world.setBlock(curPos.south(), Blocks.VINE.defaultBlockState().setValue(VineBlock.NORTH, true), 32);
                                }
                            }
                        }
                    }

                    switch (rand.nextInt(3)) {
                        case 0 -> world.setBlock(curPos, Blocks.MOSSY_STONE_BRICKS.defaultBlockState(), 32);
                        case 1 -> world.setBlock(curPos, Blocks.CRACKED_STONE_BRICKS.defaultBlockState(), 32);
                    }
                }

                if(curState.is(Blocks.WHITE_CARPET)) world.setBlock(curPos, carpet[rand.nextInt(carpet.length)].defaultBlockState(), 32);

                if(curState.is(Building.RED_GLOWING_OBSIDIAN.get())) {
                    var glowingObsidian = Utils.getBlockTag(PMTags.Blocks.GLOWING_OBSIDIAN);
                    if (glowingObsidian.isEmpty()) return;
                    world.setBlock(curPos, glowingObsidian.getRandomElement(rand).get().defaultBlockState(), 32);
                }

                if(curState.is(Blocks.IRON_BARS) && rand.nextBoolean())
                    world.setBlock(curPos, Decoration.RUSTED_IRON_BARS.get().defaultBlockState(), 32);
            }
        );

        PMWorld.updateBlockStates(world, pos, sizeX, sizeY, sizeZ);
    }
}