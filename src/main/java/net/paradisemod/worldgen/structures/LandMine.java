package net.paradisemod.worldgen.structures;

import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.paradisemod.base.PMConfig;
import net.paradisemod.redstone.Plates;
import net.paradisemod.world.PMWorld;
import net.paradisemod.worldgen.features.BasicFeature;

public class LandMine extends BasicFeature {
    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        if(PMConfig.SETTINGS.structures.landmines.shouldGenerate(rand)) {
            var y = PMWorld.getGroundLevel(world, -56, 320, pos);
            if(y.isPresent()) {
                var newPos = pos.atY(y.getAsInt() + 1);
                var validBlocks = List.of(Blocks.STONE, Blocks.DIORITE, Blocks.ANDESITE, Blocks.GRANITE, Blocks.GRASS_BLOCK, Blocks.DIRT);
                var plates = List.of(Blocks.STONE_PRESSURE_PLATE, Plates.DIORITE_PRESSURE_PLATE.get(), Plates.ANDESITE_PRESSURE_PLATE.get(), Plates.GRANITE_PRESSURE_PLATE.get(), Plates.GRASS_PRESSURE_PLATE.get(), Plates.DIRT_PRESSURE_PLATE.get());

                for(var x = -1; x <= 1; x++)
                    for(var z = -1; z <= 1; z++){
                        var ground = world.getBlockState(newPos.offset(x, -1, z)).getBlock();
                        if (validBlocks.contains(ground)) continue;
                        return false;
                    }

                int index = 0;

                for(var block : validBlocks) {
                    if(world.getBlockState(newPos.below()).is(block)) break;
                    index++;
                }

                world.setBlock(newPos, plates.get(index).defaultBlockState(), 1);

                for(var x = -1; x <= 1; x++)
                    for(var z = -1; z <= 1; z++)
                        world.setBlock(newPos.offset(x, -2, z), Blocks.TNT.defaultBlockState(), 1);

                return true;
            }
        }

        return false;
    }
}