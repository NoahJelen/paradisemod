package net.paradisemod.worldgen.structures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Holder;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BiomeTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.LadderBlock;
import net.minecraft.world.level.block.RedstoneLampBlock;
import net.minecraft.world.level.block.VineBlock;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
import net.minecraftforge.common.Tags;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.PMConfig;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.redstone.Lamps;
import net.paradisemod.world.PMWorld;
import net.paradisemod.world.biome.PMBiomes;

// structure that generates on the ground
public class GroundStructure extends SmallStructure {
    @Nullable
    private Holder<Biome> biome = null;
    private final ResourceLocation structure;
    private final boolean overGrow;
    private final PMConfig.WorldGenOption settings;
    private final ArrayList<Supplier<Block>> supportBlocks = new ArrayList<>();
    private final SupportType support;

    public GroundStructure(String name, boolean overGrow, PMConfig.WorldGenOption settings, SupportType support, Block... supportBlocks) {
        structure = new ResourceLocation("paradisemod:" + name);
        this.overGrow = overGrow;
        this.settings = settings;
        for(var block : supportBlocks) this.supportBlocks.add(() -> block);
        this.support = support;
    }

    @SafeVarargs
    public GroundStructure(String name, boolean overGrow, PMConfig.WorldGenOption settings, SupportType support, Supplier<Block>... supportBlocks) {
        structure = new ResourceLocation("paradisemod:" + name);
        this.overGrow = overGrow;
        this.settings = settings;
        this.supportBlocks.addAll(Arrays.asList(supportBlocks));
        this.support = support;
    }

    public GroundStructure(String name, boolean overGrow, PMConfig.WorldGenOption settings, SupportType support) {
        structure = new ResourceLocation(ParadiseMod.ID, name);
        this.overGrow = overGrow;
        this.settings = settings;
        this.support = support;
    }

    @Override
    protected boolean placeFeature(WorldGenLevel world, RandomSource rand, BlockPos pos, ChunkGenerator generator) {
        if(settings.shouldGenerate(rand)) {
            var template = world.getLevel().getStructureManager().get(structure).get();
            var size = template.getSize();
            int level;

            if(support == SupportType.SOLID || support == SupportType.STILTS)
                level = PMWorld.getHighestY(world, pos, pos.offset(size));
            else level = PMWorld.getLowestY(world, pos, pos.offset(size));

            if (level < 63) return false; // if we're generating on the ground, the y level should never be below sea level
            var newPos = pos.atY(level);
            if(structure.toString() == "paradisemod:rebels_1") newPos = pos.atY(level - 1);

            if(checkArea(world, newPos, size))
                return genStructureFromTemplate(template, world, rand, newPos, generator, support != SupportType.IN_GROUND || overGrow);
        }

        return false;
    }

    @Override
    protected void postProcessStructure(StructureTemplate template, WorldGenLevel world, RandomSource rand, BlockPos pos, StructurePlaceSettings settings, ChunkGenerator generator) {
        int sizeX = template.getSize().getX();
        int sizeY = template.getSize().getY();
        int sizeZ = template.getSize().getZ();
        biome = world.getBiome(pos);

        postProcessByBlock(
            world, template, pos,
            (curState, curPos) -> {
                var x = curPos.getX() - pos.getX();
                var y = curPos.getY() - pos.getY();
                var z = curPos.getZ() - pos.getZ();
                var curStateAbove = world.getBlockState(curPos.above());
                var curStateBelow = world.getBlockState(curPos.below());

                if (overGrow) {
                    if (curState.is(Blocks.WATER) && curStateAbove.is(Blocks.AIR) && rand.nextInt(3) == 0)
                        world.setBlock(curPos.above(), Blocks.LILY_PAD.defaultBlockState(), 32);

                    if (curState.canOcclude() && curStateAbove.is(Blocks.WATER) && rand.nextInt(7) == 0)
                        world.setBlock(curPos.above(), Blocks.SEAGRASS.defaultBlockState(), 32);

                    if(curState.is(Blocks.STONE_BRICKS)) {
                        switch (rand.nextInt(3)) {
                            case 0 ->
                                world.setBlock(curPos, Blocks.MOSSY_STONE_BRICKS.defaultBlockState(), 32);

                            case 1 ->
                                world.setBlock(curPos, Blocks.CRACKED_STONE_BRICKS.defaultBlockState(), 32);

                            default -> { }
                        }
                    }

                    if(rand.nextInt(10) == 0 && (biome.is(BiomeTags.IS_JUNGLE) || biome.is(Tags.Biomes.IS_SWAMP))) {
                        if(curState.canOcclude()) {
                            for(Direction direction : Direction.Plane.HORIZONTAL) {
                                if(world.getBlockState(curPos.relative(direction)).isAir()) {
                                    switch(direction) {
                                        case EAST ->
                                            world.setBlock(curPos.east(), Blocks.VINE.defaultBlockState().setValue(VineBlock.WEST, true), 32);

                                        case WEST ->
                                            world.setBlock(curPos.west(), Blocks.VINE.defaultBlockState().setValue(VineBlock.EAST, true), 32);

                                        case NORTH ->
                                            world.setBlock(curPos.north(), Blocks.VINE.defaultBlockState().setValue(VineBlock.SOUTH, true), 32);

                                        case SOUTH ->
                                            world.setBlock(curPos.south(), Blocks.VINE.defaultBlockState().setValue(VineBlock.NORTH, true), 32);
                                    }
                                }
                            }
                        }
                    }
                }

                if(y == 0 && support != SupportType.IN_GROUND && curState.canOcclude()) {
                    var newPos = curPos.below().mutable();
                    switch (support) {
                        case SOLID -> {
                            while(!world.getBlockState(newPos).canOcclude()) {
                                world.setBlock(newPos, supportBlocks.get(rand.nextInt(supportBlocks.size())).get().defaultBlockState(), 32);
                                newPos.move(Direction.DOWN);
                                if (newPos.getY() < 30) break;
                            }
                        }

                        case STILTS -> {
                            if((x == 0 && z == 0) || (x == 0 && z == sizeZ - 1) || (x == sizeX - 1 && z == 0) || (x == sizeX - 1 && z == sizeZ - 1)) {
                                while(!world.getBlockState(newPos).canOcclude()) {
                                    world.setBlock(newPos, supportBlocks.get(rand.nextInt(supportBlocks.size())).get().defaultBlockState(), 32);
                                    newPos.move(Direction.DOWN);
                                    if (newPos.getY() < 30) break;
                                }
                            }
                        }
                    }
                }

                if(curState.is(Blocks.COBBLESTONE) && rand.nextInt(3) == 0)
                    world.setBlock(curPos, Blocks.MOSSY_COBBLESTONE.defaultBlockState(), 32);

                if(curState.is(Blocks.BARRIER)) {
                    world.setBlock(curPos, Blocks.AIR.defaultBlockState(), 32);
                    world.setBlock(curPos.above(), Blocks.AIR.defaultBlockState(), 32);
                }

                if(curState.is(Blocks.REDSTONE_LAMP))
                    world.setBlock(curPos, getColoredLamp(rand).defaultBlockState().setValue(RedstoneLampBlock.LIT, true), 32);

                if(curState.is(Blocks.LADDER) && curStateBelow.isAir()) {
                    var newPos = curPos.below().mutable();
                    var facing = curState.getValue(LadderBlock.FACING);

                    while (!world.getBlockState(newPos).canOcclude()) {
                        world.setBlock(newPos, Blocks.LADDER.defaultBlockState().setValue(LadderBlock.FACING, facing), 32);
                        newPos.move(Direction.DOWN);
                        if (newPos.getY() < 30) break;
                    }
                }
            }
        );

        PMWorld.updateBlockStates(world, pos, sizeX, sizeY, sizeZ);
    }

    private Block getColoredLamp(RandomSource rand) {
        var key = biome.unwrapKey().get();

        for(var entry : PMBiomes.ROSE_FIELDS_BY_COLOR.entrySet()) {
            var color = entry.getKey();
            var biome = entry.getValue();

            if(key == biome) return Lamps.REDSTONE_LAMPS.get(color).get();
        }

        if(biome.is(Biomes.DARK_FOREST)) return Lamps.BLUE_REDSTONE_LAMP.get();

        if(biome.is(BiomeTags.IS_BADLANDS) || biome.is(PMTags.Biomes.ROCKY_DESERTS) || biome.is(PMTags.Biomes.COLD_ROCKY_DESERTS))
            return Lamps.BROWN_REDSTONE_LAMP.get();

        if(biome.is(Biomes.BIRCH_FOREST) || biome.is(Biomes.OLD_GROWTH_BIRCH_FOREST))
            return Lamps.CYAN_REDSTONE_LAMP.get();

        if(biome.is(Tags.Biomes.IS_SWAMP) || biome.is(PMBiomes.TEMPERATE_RAINFOREST))
            return Lamps.GREEN_REDSTONE_LAMP.get();

        if(biome.is(Tags.Biomes.IS_CONIFEROUS) || biome.is(Tags.Biomes.IS_COLD) || biome.is(Tags.Biomes.IS_SNOWY))
            return Lamps.LIGHT_BLUE_REDSTONE_LAMP.get();

        if(biome.is(BiomeTags.IS_JUNGLE))
            return Lamps.LIME_REDSTONE_LAMP.get();

        if(biome.is(BiomeTags.IS_SAVANNA)) return Lamps.ORANGE_REDSTONE_LAMP.get();

        if(biome.is(Tags.Biomes.IS_DRY) && !biome.is(BiomeTags.IS_BADLANDS) && !biome.is(PMTags.Biomes.ROCKY_DESERTS) && !biome.is(PMTags.Biomes.COLD_ROCKY_DESERTS))
            return Lamps.YELLOW_REDSTONE_LAMP.get();

        if(biome.is(PMBiomes.AUTUMN_FOREST)) {
            Block[] autumnLamps = {
                Lamps.YELLOW_REDSTONE_LAMP.get(),
                Lamps.ORANGE_REDSTONE_LAMP.get(),
                Lamps.RED_REDSTONE_LAMP.get(),
                Lamps.BLUE_REDSTONE_LAMP.get()
            };

            return autumnLamps[rand.nextInt(4)];
        }

        return Blocks.REDSTONE_LAMP;
    }

    public enum SupportType {
        IN_GROUND,
        SOLID,
        STILTS
    }
}