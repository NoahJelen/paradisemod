package net.paradisemod.worldgen.structures;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Supplier;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;

import net.minecraft.core.Holder;
import net.minecraft.core.HolderGetter;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.data.worldgen.Pools;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.BiomeTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.GenerationStep.Decoration;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.placement.CountPlacement;
import net.minecraft.world.level.levelgen.placement.HeightRangePlacement;
import net.minecraft.world.level.levelgen.placement.InSquarePlacement;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.placement.PlacementModifier;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraft.world.level.levelgen.structure.Structure.StructureSettings;
import net.minecraft.world.level.levelgen.structure.StructureSet;
import net.minecraft.world.level.levelgen.structure.StructureSet.StructureSelectionEntry;
import net.minecraft.world.level.levelgen.structure.StructureType;
import net.minecraft.world.level.levelgen.structure.TerrainAdjustment;
import net.minecraft.world.level.levelgen.structure.placement.RandomSpreadStructurePlacement;
import net.minecraft.world.level.levelgen.structure.placement.RandomSpreadType;
import net.minecraft.world.level.levelgen.structure.pools.StructurePoolElement;
import net.minecraft.world.level.levelgen.structure.pools.StructurePoolElementType;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool.Projection;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessorList;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessorType;
import net.minecraftforge.common.Tags;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.PMConfig;
import net.paradisemod.base.Utils;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.PMWorld.WorldgenFactory;
import net.paradisemod.worldgen.features.BasicFeature;
import net.paradisemod.worldgen.features.PMFeatures;
import net.paradisemod.worldgen.structures.GroundStructure.SupportType;
import net.paradisemod.worldgen.structures.postprocessors.BrickDungeonPostProcessor;
import net.paradisemod.worldgen.structures.postprocessors.DarkDungeonPostProcessor;
import net.paradisemod.worldgen.structures.postprocessors.MonasteryPostProcessor;
import net.paradisemod.worldgen.structures.postprocessors.StructurePostProcessor;
import net.paradisemod.worldgen.structures.postprocessors.TerrariumPostProcessor;
import net.paradisemod.worldgen.structures.postprocessors.TraderTentPostProcessor;
import net.paradisemod.worldgen.structures.processors.BrickDungeonProcessor;
import net.paradisemod.worldgen.structures.processors.ConduitProcessor;
import net.paradisemod.worldgen.structures.processors.DarkDungeonProcessor;
import net.paradisemod.worldgen.structures.processors.TerrariumProcessor;

@SuppressWarnings("unchecked")
public class PMStructures {
    public static final HashMap<ResourceKey<Structure>, StructureFactory> STRUCTURES = new HashMap<>();
    protected static final HashMap<String, StructurePostProcessor.Constructor> STRUCT_POST_PROCESSORS = new HashMap<>();
    private static int LAST_SALT = 0;
    private static final Random STRUCT_SET_SALT_RAND = new Random(420777);
    private static final DeferredRegister<StructureType<?>> STRUCT_TYPES = PMRegistries.createRegistry(Registries.STRUCTURE_TYPE);
    private static final DeferredRegister<StructureProcessorType<?>> STRUCT_PROCESSORS = PMRegistries.createRegistry(Registries.STRUCTURE_PROCESSOR);
    private static final DeferredRegister<StructurePoolElementType<?>> POOL_ELEMENT_TYPES = PMRegistries.createRegistry(Registries.STRUCTURE_POOL_ELEMENT);
    public static final RegistryObject<StructurePoolElementType<ImprovedPoolElement>> IMPROVED_ELEMENT = POOL_ELEMENT_TYPES.register("improved_element", () -> () -> ImprovedPoolElement.CODEC);

    static {
        STRUCT_POST_PROCESSORS.put("brick_dungeon", BrickDungeonPostProcessor::new);
        STRUCT_POST_PROCESSORS.put("terrarium", TerrariumPostProcessor::new);
        STRUCT_POST_PROCESSORS.put("dark_dungeon", DarkDungeonPostProcessor::new);
        STRUCT_POST_PROCESSORS.put("trader_tent", TraderTentPostProcessor::new);
        STRUCT_POST_PROCESSORS.put("monastery", MonasteryPostProcessor::new);
    }

    public static final RegistryObject<StructureType<BigStructure>> BIG_STRUCTURE = regStructType("big_structure", BigStructure.CODEC);

    // small structures (implemented as placed features)
    public static final ResourceKey<PlacedFeature> BUOY = getSmallStructure("buoy", Buoy::new);
    public static final ResourceKey<PlacedFeature> LANDMINE = getSmallStructure("landmine", LandMine::new);

    public static final ResourceKey<PlacedFeature> RUNWAY = getSmallStructure(
        "runway",
        () -> new GroundStructure("runway", false, PMConfig.SETTINGS.structures.runways, SupportType.SOLID, Blocks.STONE_BRICKS)
    );

    public static final ResourceKey<PlacedFeature> EASTER_EGG_1 = getSmallStructure(
        "easter_egg_1",
        () -> new UndergroundStructure("easter_egg_1", true, PMConfig.SETTINGS.structures.eastereggs)
    );

    public static final ResourceKey<PlacedFeature> EASTER_EGG_2 = getSmallStructure(
        "easter_egg_2",
        () -> new GroundStructure("easter_egg_2", true,  PMConfig.SETTINGS.structures.eastereggs, SupportType.STILTS, Blocks.STONE_BRICKS, Blocks.CRACKED_STONE_BRICKS, Blocks.MOSSY_STONE_BRICKS)
    );

    public static final ResourceKey<PlacedFeature> REBELS_1 = getSmallStructure(
        "rebels_1",
        () -> new GroundStructure("rebels_1", false,  PMConfig.SETTINGS.structures.blackAndGold, SupportType.IN_GROUND)
    );

    public static final ResourceKey<PlacedFeature> REBELS_2 = getSmallStructure(
        "rebels_2",
        () -> new UndergroundStructure("rebels_2", true, PMConfig.SETTINGS.structures.blackAndGold)
    );

    public static final ResourceKey<PlacedFeature> BLACK_CROSS = getSmallStructure(
        "black_cross",
        () -> new GroundStructure("black_cross", true, PMConfig.SETTINGS.structures.blackCross, SupportType.STILTS, Blocks.STONE_BRICKS, Blocks.CRACKED_STONE_BRICKS, Blocks.MOSSY_STONE_BRICKS)
    );

    public static final ResourceKey<PlacedFeature> DEEP_DARK_BLACK_CROSS = getSmallStructure(
        "deep_dark_black_cross",
        () -> new GroundStructure("black_cross_deep_dark", false, PMConfig.SETTINGS.structures.blackCross, SupportType.STILTS, DeepDarkBlocks.POLISHED_DARKSTONE_BRICKS)
    );

    public static final ResourceKey<PlacedFeature> GOLD_CROSS = getSmallStructure(
        "gold_cross",
        () -> new GroundStructure("gold_cross", true, PMConfig.SETTINGS.structures.goldCross, SupportType.STILTS, Blocks.STONE_BRICKS, Blocks.CRACKED_STONE_BRICKS, Blocks.MOSSY_STONE_BRICKS)
    );

    public static final ResourceKey<PlacedFeature> DEEP_DARK_GOLD_CROSS = getSmallStructure(
        "deep_dark_gold_cross",
        () -> new GroundStructure("gold_cross_deep_dark", false, PMConfig.SETTINGS.structures.goldCross, SupportType.STILTS, DeepDarkBlocks.POLISHED_DARKSTONE_BRICKS)
    );

    public static final ResourceKey<PlacedFeature> RUINS = getSmallStructure("ruins", VillageRuin::new, PlacementUtils.HEIGHTMAP);

    public static final ResourceKey<PlacedFeature> SMALL_STRONGHOLD = getSmallStructure("small_stronghold",
        () -> new UndergroundStructure("dungeons/small_stronghold", true, PMConfig.SETTINGS.structures.smallStronghold)
    );

    public static final ResourceKey<PlacedFeature> CREATOR_HEADS = getSmallStructure("creator_heads", CreatorHeads::new);
    public static final ResourceKey<PlacedFeature> RESEARCH_BASE = getSmallStructure("research_base", ResearchBase::new);
    public static final ResourceKey<PlacedFeature> HOME = getSmallStructure("home", Home::new);
    public static final ResourceKey<PlacedFeature> SMALL_DARK_DUNGEON = getSmallStructure("small_dark_dungeon", SmallDarkDungeon::new, CountPlacement.of(10), InSquarePlacement.spread(), HeightRangePlacement.uniform(VerticalAnchor.absolute(0), VerticalAnchor.top()));
    public static final ResourceKey<PlacedFeature> ROGUE_SPIKE = getSmallStructure("rogue_spike", RogueEndSpike::new);

    // big structure processors
    public static final RegistryObject<StructureProcessorType<BrickDungeonProcessor>> BRICK_DUNGEON_PROCESSOR = regStructProcessor("brick_dungeon", BrickDungeonProcessor.CODEC);
    public static final RegistryObject<StructureProcessorType<TerrariumProcessor>> TERRARIUM_PROCESSOR = regStructProcessor("terrarium", TerrariumProcessor.CODEC);
    public static final RegistryObject<StructureProcessorType<DarkDungeonProcessor>> DARK_DUNGEON_PROCESSOR = regStructProcessor("dark_dungeon", DarkDungeonProcessor.CODEC);
    public static final RegistryObject<StructureProcessorType<ConduitProcessor>> CONDUIT_PROCESSOR = regStructProcessor("conduit", ConduitProcessor.CODEC);

    // big structures
    public static final ResourceKey<Structure> BRICK_PYRAMID = createStructure(
        "brick_pyramid",
        "brickPyramids",
        "brick_pyramid/1",
        4,
        StructureGenType.GROUND,
        BiomeTags.IS_OVERWORLD,
        Decoration.SURFACE_STRUCTURES,
        TerrainAdjustment.BEARD_BOX
    );

    public static final ResourceKey<Structure> MEDIUM_DARK_DUNGEON = createStructure(
        "medium_dark_dungeon",
        "mediumDarkDungeon",
        "medium_dark_dungeon",
        1,
        StructureGenType.UNDERGROUND,
        PMTags.Biomes.DEEP_DARK,
        Decoration.UNDERGROUND_STRUCTURES,
        TerrainAdjustment.BURY
    );

    public static final ResourceKey<Structure> LARGE_DARK_DUNGEON = createStructure(
        "large_dark_dungeon",
        "largeDarkDungeon",
        "large_dark_dungeon/1",
        4,
        StructureGenType.GROUND,
        PMTags.Biomes.DEEP_DARK,
        Decoration.SURFACE_STRUCTURES,
        TerrainAdjustment.BEARD_BOX
    );

    public static final ResourceKey<Structure> DARK_TOWER = createStructure(
        "dark_tower",
        "darkTower",
        "dark_tower/bottom",
        12,
        StructureGenType.GROUND,
        PMTags.Biomes.DEEP_DARK,
        Decoration.SURFACE_STRUCTURES,
        TerrainAdjustment.BEARD_BOX
    );

    public static final ResourceKey<Structure> MINER_BASE = createStructure(
        "miner_base",
        "minerBases",
        "miner_base",
        1,
        StructureGenType.UNDERGROUND,
        PMTags.Biomes.MINER_BASE_BIOMES,
        Decoration.UNDERGROUND_STRUCTURES,
        TerrainAdjustment.BURY
    );

    public static final ResourceKey<Structure> TERRARIUM = createStructure(
        "terrarium",
        "terrariums",
        "terrarium",
        1,
        StructureGenType.UNDERGROUND,
        PMTags.Biomes.MINER_BASE_BIOMES,
        Decoration.UNDERGROUND_STRUCTURES,
        TerrainAdjustment.BURY
    );

    public static final ResourceKey<Structure> OCEAN_TERRARIUM = createStructure(
        "ocean_terrarium",
        "terrariums",
        "terrarium",
        1,
        StructureGenType.GROUND,
        BiomeTags.IS_OCEAN,
        Decoration.SURFACE_STRUCTURES,
        TerrainAdjustment.BEARD_BOX
    );

    public static final ResourceKey<Structure> BADLANDS_PYRAMID = createStructure(
        "badlands_pyramid",
        "badlandsPyramids",
        "badlands_pyramid/pyramid",
        2,
        StructureGenType.GROUND,
        BiomeTags.IS_BADLANDS,
        Decoration.SURFACE_STRUCTURES,
        TerrainAdjustment.BEARD_BOX
    );

    public static final ResourceKey<Structure> DARK_DESERT_PYRAMID = createStructure(
        "dark_desert_pyramid",
        "darkDesertPyramids",
        "dark_desert_pyramid/pyramid",
        2,
        StructureGenType.GROUND,
        PMTags.Biomes.DARK_DESERTS,
        Decoration.SURFACE_STRUCTURES,
        TerrainAdjustment.BEARD_BOX
    );

    public static final ResourceKey<Structure> WICKER_MAN = createStructure(
        "wicker_man",
        "wickerMan",
        "wicker_man/shrine",
        2,
        StructureGenType.DRY_GROUND,
        BiomeTags.HAS_STRONGHOLD,
        Decoration.SURFACE_STRUCTURES,
        TerrainAdjustment.BEARD_BOX
    );

    public static final ResourceKey<Structure> TRADER_TENT = createStructure(
        "trader_tent",
        "traderTent",
        "trader_tent",
        1,
        StructureGenType.DRY_GROUND,
        BiomeTags.HAS_STRONGHOLD,
        Decoration.SURFACE_STRUCTURES,
        TerrainAdjustment.BEARD_BOX
    );

    public static final ResourceKey<Structure> SKY_WHEEL = createStructure(
        "sky_wheel",
        "skyWheels",
        "sky_wheel",
        1,
        StructureGenType.SKY,
        PMTags.Biomes.MINER_BASE_BIOMES,
        Decoration.TOP_LAYER_MODIFICATION,
        TerrainAdjustment.NONE
    );

    public static final ResourceKey<Structure> ENDER_OUTPOST = createStructure(
        "ender_outpost",
        "enderOutpost",
        "ender_outpost/bottom",
        12,
        StructureGenType.GROUND,
        BiomeTags.IS_END,
        Decoration.SURFACE_STRUCTURES,
        TerrainAdjustment.BEARD_BOX
    );

    public static final ResourceKey<Structure> MONASTERY = createStructure(
        "monastery",
        "monastery",
        "monastery/base",
        2,
        StructureGenType.DRY_GROUND,
        PMTags.Biomes.MINER_BASE_BIOMES,
        Decoration.SURFACE_STRUCTURES,
        TerrainAdjustment.BEARD_BOX
    );

    public static final ResourceKey<Structure> CONDUIT = createStructure(
        "conduit",
        "conduit",
        "conduit",
        2,
        StructureGenType.GROUND,
        Tags.Biomes.IS_WATER,
        Decoration.SURFACE_STRUCTURES,
        TerrainAdjustment.BEARD_BOX
    );

    public static void init(IEventBus eventBus) {
        STRUCT_TYPES.register(eventBus);
        STRUCT_PROCESSORS.register(eventBus);
        POOL_ELEMENT_TYPES.register(eventBus);
    }

    public static void buildStructSets(BootstapContext<StructureSet> context) {
        ParadiseMod.LOG.debug("Generating structure sets...");
        var structGetter = context.lookup(Registries.STRUCTURE);
        buildStructSet(context, BADLANDS_PYRAMID, 32, 8);
        buildStructSet(context, DARK_DESERT_PYRAMID, 32, 8);
        buildStructSet(context, TRADER_TENT, 16, 8);
        buildStructSet(context, BRICK_PYRAMID, 64, 32);
        buildStructSet(context, WICKER_MAN, 64, 32);
        buildStructSet(context, MONASTERY, 64, 32);
        buildStructSet(context, CONDUIT, 32, 16);

        buildStructSet(
            context,
            "dark_dungeons",
            32, 8,
            createEntry(structGetter, LARGE_DARK_DUNGEON),
            createEntry(structGetter, MEDIUM_DARK_DUNGEON, 2),
            createEntry(structGetter, DARK_TOWER, 3)
        );

        buildStructSet(
            context,
            "terrariums",
            32, 16,
            createEntry(structGetter, TERRARIUM),
            createEntry(structGetter, OCEAN_TERRARIUM)
        );

        buildStructSet(context, ENDER_OUTPOST, 32, 8);
        buildStructSet(context, MINER_BASE, 32, 16);
        buildStructSet(context, SKY_WHEEL, 16, 8);
    }

    public static void buildStructPools(BootstapContext<StructureTemplatePool> context) {
        ParadiseMod.LOG.debug("Generating template pools...");
        var structPoolGetter = context.lookup(Registries.TEMPLATE_POOL);
        dungeonPool(context, "badlands_pyramid/bottom");
        dungeonPool(context, "badlands_pyramid/pyramid");
        dungeonPool(context, "dark_desert_pyramid/bottom");
        dungeonPool(context, "dark_desert_pyramid/pyramid");
        structPool(context, "wicker_man/floor");
        structPool(context, "wicker_man/shrine", "brick_dungeon");
        dungeonPool(context, "brick_pyramid/1");
        dungeonPool(context, "brick_pyramid/2");
        dungeonPool(context, "brick_pyramid/3");
        dungeonPool(context, "brick_pyramid/4");
        dungeonPool(context, "dark_tower/bottom");
        structPool(context, "monastery/base", "monastery");
        structPool(context, "monastery/shrine", "monastery");
        structPool(context, "conduit", new ConduitProcessor());
        var darkTowerTop = dungeonPool(context, "dark_tower/top");

        createMultiElemPool(
            context,
            "dark_tower/middle_or_top",
            darkTowerTop,
            Pair.of(
                ImprovedPoolElement.create("paradisemod:dungeons/dark_tower/middle"),
                2
            ),
            Pair.of(
                ImprovedPoolElement.create("paradisemod:dungeons/dark_tower/top"),
                1
            )
        );

        dungeonPool(context, "ender_outpost/bottom");
        var enderOutpostTop = dungeonPool(context, "ender_outpost/top");
        var enderOutpostTopInverted = dungeonPool(context, "ender_outpost/top_inverted");

        createMultiElemPool(
            context,
            "ender_outpost/middle_or_top",
            enderOutpostTop,
            Pair.of(
                ImprovedPoolElement.create("paradisemod:dungeons/ender_outpost/middle"),
                2
            ),
            Pair.of(
                ImprovedPoolElement.create("paradisemod:dungeons/ender_outpost/top"),
                1
            )
        );

        createMultiElemPool(
            context, "ender_outpost/middle_or_top_inverted",
            enderOutpostTopInverted,
            Pair.of(
                ImprovedPoolElement.create("paradisemod:dungeons/ender_outpost/middle_inverted"),
                2
            ),
            Pair.of(
                ImprovedPoolElement.create("paradisemod:dungeons/ender_outpost/top_inverted"),
                1
            )
        );

        dungeonPool(context, "large_dark_dungeon/1", "dark_dungeon", new DarkDungeonProcessor());
        dungeonPool(context, "large_dark_dungeon/2", "dark_dungeon", new DarkDungeonProcessor());
        dungeonPool(context, "large_dark_dungeon/3", "dark_dungeon", new DarkDungeonProcessor());
        dungeonPool(context, "large_dark_dungeon/4", "dark_dungeon", new DarkDungeonProcessor());
        dungeonPool(context, "medium_dark_dungeon", "dark_dungeon", new DarkDungeonProcessor());
        dungeonPool(context, "miner_base", "brick_dungeon", new BrickDungeonProcessor());
        dungeonPool(context, "sky_wheel");

        createMultiElemPool(
            context, "terrarium",
            structPoolGetter.getOrThrow(Pools.EMPTY),
            Pair.of(
                ImprovedPoolElement.create(
                    "paradisemod:terrarium",
                    "terrarium",
                    createProcessorList(new TerrariumProcessor())
                ),
                1
            ),
            Pair.of(
                ImprovedPoolElement.create(
                    "paradisemod:village_terrarium",
                    "terrarium",
                    createProcessorList(new TerrariumProcessor())
                ),
                1
            )
        );

        createMultiElemPool(
            context, "trader_tent",
            structPoolGetter.getOrThrow(Pools.EMPTY),
            Pair.of(
                ImprovedPoolElement.create("paradisemod:trader_tent/left", "trader_tent"),
                1
            ),
            Pair.of(
                ImprovedPoolElement.create("paradisemod:trader_tent/right", "trader_tent"),
                1
            )
        );
    }

    private static StructureSelectionEntry createEntry(HolderGetter<Structure> structGetter, ResourceKey<Structure> key, int weight) {
        return StructureSet.entry(structGetter.getOrThrow(key), weight);
    }

    private static StructureSelectionEntry createEntry(HolderGetter<Structure> structGetter, ResourceKey<Structure> key) {
        return StructureSet.entry(structGetter.getOrThrow(key));
    }

    private static void buildStructSet(BootstapContext<StructureSet> context, ResourceKey<Structure> key, int spacing, int separation) {
        var structGetter = context.lookup(Registries.STRUCTURE);

        buildStructSet(
            context,
            key.location().getPath(),
            spacing,
            separation,
            createEntry(structGetter, key)
        );
    }

    private static void buildStructSet(BootstapContext<StructureSet> context, String name, int spacing, int separation, StructureSelectionEntry... entries) {
        // ensure each salt is uniquely generated to avoid clashing
        int salt;

        do {
            salt = STRUCT_SET_SALT_RAND.nextInt(1, Integer.MAX_VALUE);
        }
        while(salt == LAST_SALT);

        LAST_SALT = salt;

        context.register(
            PMRegistries.createModResourceKey(Registries.STRUCTURE_SET, name),
            new StructureSet(
                List.of(entries),
                new RandomSpreadStructurePlacement(spacing, separation, RandomSpreadType.LINEAR, salt)
            )
        );
    }

    private static Holder<StructureProcessorList> createProcessorList(StructureProcessor... processors) {
        return new Holder.Direct<>(new StructureProcessorList(List.of(processors)));
    }

    private static Holder<StructureTemplatePool> dungeonPool(
        BootstapContext<StructureTemplatePool> context,
        String name,
        String postProcessorName,
        StructureProcessor... processors
    ) {
        return structPoolInternal(context, name, "dungeons/" + name, Optional.of(postProcessorName), processors);
    }

    private static Holder<StructureTemplatePool> structPool(
        BootstapContext<StructureTemplatePool> context,
        String name,
        String postProcessorName,
        StructureProcessor... processors
    ) {
        return structPoolInternal(context, name, name, Optional.of(postProcessorName), processors);
    }

    private static Holder<StructureTemplatePool> dungeonPool(
        BootstapContext<StructureTemplatePool> context,
        String name,
        StructureProcessor... processors
    ) {
        return structPoolInternal(context, name, "dungeons/" + name, Optional.empty(), processors);
    }

    private static Holder<StructureTemplatePool> structPool(
        BootstapContext<StructureTemplatePool> context,
        String name,
        StructureProcessor... processors
    ) {
        return structPoolInternal(context, name, name, Optional.empty(), processors);
    }

    private static Holder<StructureTemplatePool> structPoolInternal(
        BootstapContext<StructureTemplatePool> context,
        String name,
        String structName,
        Optional<String> postProcessorName,
        StructureProcessor... processors
    ) {
        var structPoolGetter = context.lookup(Registries.TEMPLATE_POOL);
        var element = ImprovedPoolElement.create("paradisemod:" + structName, postProcessorName, createProcessorList());

        if(processors.length > 0)
            element = ImprovedPoolElement.create(
                "paradisemod:" + structName,
                postProcessorName,
                createProcessorList(processors)
            );

        return createMultiElemPool(
            context, name,
            structPoolGetter.getOrThrow(Pools.EMPTY),
            Pair.of(
                element,
                1
            )
        );
    }

    private static Holder<StructureTemplatePool> createMultiElemPool(
        BootstapContext<StructureTemplatePool> context,
        String name,
        Holder<StructureTemplatePool> fallback,
        Pair<Function<Projection, ? extends StructurePoolElement>, Integer>... elements
    ) {
        var key = PMRegistries.createModResourceKey(Registries.TEMPLATE_POOL, name);

        context.register(
            key,
            new StructureTemplatePool(
                fallback,
                List.of(elements),
                Projection.RIGID
            )
        );

        var structPoolGetter = context.lookup(Registries.TEMPLATE_POOL);
        return structPoolGetter.getOrThrow(key);
    }

    private static <P extends StructureProcessor> RegistryObject<StructureProcessorType<P>> regStructProcessor(String name, Codec<P> processorCodec) {
        return Utils.handlePossibleException(
            () -> STRUCT_PROCESSORS.register(name, () -> () -> processorCodec)
        );
    }

    private static ResourceKey<Structure> createStructure(
        String name,
        String configKey,
        String startPoolName,
        int size,
        StructureGenType genType,
        TagKey<Biome> biomeTag,
        GenerationStep.Decoration step,
        TerrainAdjustment terrainAdaptation
    ) {
        var structKey = PMRegistries.createModResourceKey(Registries.STRUCTURE, name);

        STRUCTURES.put(
            structKey,
            biomeGetter -> {
                var biomeList = biomeGetter.getOrThrow(biomeTag);
                var structSettings = new StructureSettings(biomeList, new HashMap<>(), step, terrainAdaptation);

                return new BigStructure(
                    structSettings,
                    "paradisemod:" + startPoolName,
                    configKey,
                    size,
                    genType
                );
            }
        );

        return structKey;
    }

    // create and register a small structure
    private static ResourceKey<PlacedFeature> getSmallStructure(String name, Supplier<BasicFeature> structure, PlacementModifier... placements) {
        return PMFeatures.regPlacedFeature("small_structure/" + name, structure, (cfgFeatureGetter, featureGetter) -> FeatureConfiguration.NONE, placements);
    }

    private static <T extends Structure> RegistryObject<StructureType<T>> regStructType(String name, Codec<T> structureCodec) {
        return Utils.handlePossibleException(() -> STRUCT_TYPES.register(name, () -> () -> structureCodec));
    }

    @FunctionalInterface
    public interface StructureFactory extends WorldgenFactory<Structure> {
        Structure create(HolderGetter<Biome> biomeGetter);

        default Structure generate(BootstapContext<Structure> context) {
            var biomeGetter = context.lookup(Registries.BIOME);
            return create(biomeGetter);
        }
    }

    protected enum StructureGenType {
        UNDERGROUND(-32),
        GROUND(1),
        DRY_GROUND(1),
        SKY(256);

        private final int startHeight;

        private StructureGenType(int startHeight) {
            this.startHeight = startHeight;
        }

        protected int startHeight() { return startHeight; }

        protected boolean isGround() {
            return this == DRY_GROUND ||
                this == GROUND;
        }
    }
}