package net.paradisemod.worldgen.carver;

import java.util.HashMap;
import java.util.function.Supplier;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.util.valueproviders.ConstantFloat;
import net.minecraft.util.valueproviders.TrapezoidFloat;
import net.minecraft.util.valueproviders.UniformFloat;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.carver.CanyonCarverConfiguration;
import net.minecraft.world.level.levelgen.carver.CarverConfiguration;
import net.minecraft.world.level.levelgen.carver.CarverDebugSettings;
import net.minecraft.world.level.levelgen.carver.CaveCarverConfiguration;
import net.minecraft.world.level.levelgen.carver.ConfiguredWorldCarver;
import net.minecraft.world.level.levelgen.carver.WorldCarver;
import net.minecraft.world.level.levelgen.heightproviders.UniformHeight;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.paradisemod.base.Utils;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.PMWorld;
import net.paradisemod.world.PMWorld.WorldgenFactory;
import net.paradisemod.worldgen.features.PMFeatures;

@SuppressWarnings("unchecked")
public class PMCarvers {
    private static final DeferredRegister<WorldCarver<?>> CARVERS = PMRegistries.createRegistry(ForgeRegistries.WORLD_CARVERS);
    public static HashMap<ResourceKey<ConfiguredWorldCarver<?>>, CarverFactory> CFG_CARVERS = new HashMap<>();

    public static final ResourceKey<ConfiguredWorldCarver<?>> DEEP_DARK_CAVES = getCarver("deep_dark_caves", PMCave::new, () -> caveConfig(DeepDarkBlocks.DARKSTONE.get(), Blocks.DEEPSLATE, DeepDarkBlocks.GLOWING_NYLIUM.get(), DeepDarkBlocks.OVERGROWN_DARKSTONE.get(), DeepDarkBlocks.BLACKENED_SANDSTONE.get(), DeepDarkBlocks.BLACKENED_SAND.get()));
    public static final ResourceKey<ConfiguredWorldCarver<?>> DEEP_DARK_CANYONS = getCarver("deep_dark_canyons", PMCanyon::new, () -> canyonConfig(DeepDarkBlocks.DARKSTONE.get(), Blocks.DEEPSLATE, DeepDarkBlocks.GLOWING_NYLIUM.get(), DeepDarkBlocks.OVERGROWN_DARKSTONE.get(), DeepDarkBlocks.BLACKENED_SANDSTONE.get(), DeepDarkBlocks.BLACKENED_SAND.get()));
    public static final ResourceKey<ConfiguredWorldCarver<?>> ICE_CAVES = getCarver("ice_caves", PMCave::new, PMCarvers::iceCaveConfig);
    public static final ResourceKey<ConfiguredWorldCarver<?>> CREVASSE = getCarver("crevasse", PMCanyon::new, PMCarvers::crevasseConfig);
    public static final ResourceKey<ConfiguredWorldCarver<?>> END_CAVES = getCarver("end_caves", PMCave::new, () -> caveConfig(Blocks.END_STONE, PMWorld.OVERGROWN_END_STONE.get()));
    public static final ResourceKey<ConfiguredWorldCarver<?>> END_CANYONS = getCarver("end_canyons", PMCanyon::new, () -> canyonConfig(Blocks.END_STONE, PMWorld.OVERGROWN_END_STONE.get()));

    public static void init(IEventBus eventbus) { CARVERS.register(eventbus); }

    private static <WC extends CarverConfiguration> ResourceKey<ConfiguredWorldCarver<?>> getCarver(String name, Supplier<WorldCarver<WC>> carver, Supplier<WC> config) {
        RegistryObject<WorldCarver<WC>> regCarver = Utils.handlePossibleException(
            () -> CARVERS.register(name, carver)
        );

        var key = PMRegistries.createModResourceKey(Registries.CONFIGURED_CARVER, name);
        CFG_CARVERS.put(key, () -> (ConfiguredWorldCarver<CarverConfiguration>) regCarver.get().configured(config.get()));
        return key;
    }

    private static CaveCarverConfiguration iceCaveConfig() {
        return new CaveCarverConfiguration(
            0.75F,
            UniformHeight.of(
                VerticalAnchor.absolute(-50),
                VerticalAnchor.absolute(180)
            ),
            UniformFloat.of(
                0.1F,
                0.9F
            ),
            VerticalAnchor.aboveBottom(10),
            CarverDebugSettings.of(false, Blocks.RED_STAINED_GLASS.defaultBlockState()),
            PMFeatures.blockList(Blocks.AIR),
            UniformFloat.of(0.7F, 1.4F),
            UniformFloat.of(0.8F, 1.3F),
            UniformFloat.of(-1.0F, -0.4F)
        );
    }

    private static CanyonCarverConfiguration crevasseConfig() {
        return new CanyonCarverConfiguration(
            0.5F,
            UniformHeight.of(
                VerticalAnchor.absolute(-50),
                VerticalAnchor.absolute(180)
            ),
            ConstantFloat.of(3.0F),
            VerticalAnchor.aboveBottom(10),
            CarverDebugSettings.of(false, Blocks.RED_STAINED_GLASS.defaultBlockState()),
            PMFeatures.blockList(Blocks.AIR),
            UniformFloat.of(-0.125F, 0.125F),
            new CanyonCarverConfiguration.CanyonShapeConfiguration(
                UniformFloat.of(0.75F, 1.0F),
                TrapezoidFloat.of(0.0F, 6.0F, 2.0F),
                3,
                UniformFloat.of(0.75F, 1.0F),
                1.0F,
                0.0F
            )
        );
    }

    private static CaveCarverConfiguration caveConfig(Block... replaceables) {
        return new CaveCarverConfiguration(
            0.5F,
            UniformHeight.of(
                VerticalAnchor.aboveBottom(8),
                VerticalAnchor.absolute(180)
            ),
            UniformFloat.of(
                0.1F,
                0.9F
            ),
            VerticalAnchor.aboveBottom(10),
            CarverDebugSettings.of(false, Blocks.AIR.defaultBlockState()),
            PMFeatures.blockList(replaceables),
            UniformFloat.of(0.7F, 1.4F),
            UniformFloat.of(0.8F, 1.3F),
            UniformFloat.of(-1.0F, -0.4F)
        );
    }

    private static CanyonCarverConfiguration canyonConfig(Block... replaceables) {
        return new CanyonCarverConfiguration(
            0.25F,
            UniformHeight.of(
                VerticalAnchor.aboveBottom(8),
                VerticalAnchor.absolute(180)
            ),
            ConstantFloat.of(3.0F),
            VerticalAnchor.aboveBottom(10),
            CarverDebugSettings.of(false, Blocks.AIR.defaultBlockState()),
            PMFeatures.blockList(replaceables),
            UniformFloat.of(-0.125F, 0.125F),
            new CanyonCarverConfiguration.CanyonShapeConfiguration(
                UniformFloat.of(0.75F, 1.0F),
                TrapezoidFloat.of(0.0F, 6.0F, 2.0F),
                3,
                UniformFloat.of(0.75F, 1.0F),
                1.0F,
                0.0F
            )
        );
    }

    @FunctionalInterface
    public interface CarverFactory extends WorldgenFactory<ConfiguredWorldCarver<?>> {
        ConfiguredWorldCarver<?> get();

        default ConfiguredWorldCarver<?> generate(BootstapContext<ConfiguredWorldCarver<?>> context) {
            return get();
        }
    }
}