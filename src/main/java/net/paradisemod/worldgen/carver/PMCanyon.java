package net.paradisemod.worldgen.carver;

import java.util.function.Function;

import org.apache.commons.lang3.mutable.MutableBoolean;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.chunk.CarvingMask;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.levelgen.Aquifer;
import net.minecraft.world.level.levelgen.carver.CanyonCarverConfiguration;
import net.minecraft.world.level.levelgen.carver.CanyonWorldCarver;
import net.minecraft.world.level.levelgen.carver.CarvingContext;

public class PMCanyon extends CanyonWorldCarver {
    public PMCanyon() {
        super(CanyonCarverConfiguration.CODEC);
    }

    @Override
    protected boolean carveBlock(CarvingContext context, CanyonCarverConfiguration config, ChunkAccess chunk, Function<BlockPos, Holder<Biome>> biomeGetter, CarvingMask carvingMask, BlockPos.MutableBlockPos pos, BlockPos.MutableBlockPos checkPos, Aquifer aquifer, MutableBoolean reachedSurface) {
        var state = chunk.getBlockState(pos);
        if (canReplaceBlock(config, state)) {
            chunk.setBlockState(pos, Blocks.CAVE_AIR.defaultBlockState(), false);
            return true;
        }
        else return false;
    }
}