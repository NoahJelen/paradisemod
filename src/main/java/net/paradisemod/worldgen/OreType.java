package net.paradisemod.worldgen;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.structure.templatesystem.BlockMatchTest;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.Ores;

import java.util.function.Supplier;

public enum OreType {
    DARKSTONE_COAL(Ores.DARKSTONE_COAL_ORE, DeepDarkBlocks.DARKSTONE, 17, -48, 256, 2, "deepDark"),
    DARKSTONE_IRON(Ores.DARKSTONE_IRON_ORE, DeepDarkBlocks.DARKSTONE, 9, -48, 256, 4, "deepDark"),
    DARKSTONE_GOLD(Ores.DARKSTONE_GOLD_ORE, DeepDarkBlocks.DARKSTONE, 9, -48, 256, 8, "deepDark"),
    DARKSTONE_SILVER(Ores.DARKSTONE_SILVER_ORE, DeepDarkBlocks.DARKSTONE, 9, -48, 256, 8, "deepDark"),
    END_RUBY(Ores.END_RUBY_ORE, () -> Blocks.END_STONE, 8, 25, 70, 20, "end"),
    ENDERITE(Ores.ENDERITE_ORE, () -> Blocks.END_STONE, 4, 4, 30, 40, "end"),
    NETHER_SILVER(Ores.NETHER_SILVER_ORE, () -> Blocks.NETHERRACK, 9, 0, 128, 4, "netherSilver"),
    RUBY(Ores.RUBY_ORE, () -> Blocks.STONE, 8, 0, 16, 40, "overworld"),
    SALT(Ores.SALT_ORE, () -> Blocks.STONE, 8, 0, 320, 4, "overworld"),
    SILVER(Ores.SILVER_ORE, () -> Blocks.STONE, 9, 0, 128, 8, "overworld"),
    DEEPSLATE_RUBY(Ores.DEEPSLATE_RUBY_ORE, () -> Blocks.DEEPSLATE, 8, -63, 0, 30, "overworld"),
    DEEPSLATE_SALT(Ores.DEEPSLATE_SALT_ORE, () -> Blocks.DEEPSLATE, 8, -63, 0, 3, "overworld"),
    DEEPSLATE_SILVER(Ores.DEEPSLATE_SILVER_ORE, () -> Blocks.DEEPSLATE, 9, -63, 0, 4, "overworld");

    private final Supplier<Block> ore;
    private final Supplier<Block> fillBlock;
    private final int maxVeinSize;
    private final int maxHeight;
    private final int minHeight;
    private final int rarity;
    private final String configKey;

    OreType(Supplier<Block> ore, Supplier<Block> fillerBlock, int maxVeinSize, int minHeight, int maxHeight, int rarity, String configKey) {
        this.ore = ore;
        this.fillBlock = fillerBlock;
        this.maxVeinSize = maxVeinSize;
        this.minHeight = minHeight;
        this.maxHeight = maxHeight;
        this.rarity = rarity;
        this.configKey = configKey;
    }

    public Block getBlock() { return ore.get(); }
    public int getRarity() { return rarity; }
    public int getMaxVeinSize() { return maxVeinSize; }
    public int getMinHeight() { return minHeight; }
    public int getMaxHeight() { return maxHeight; }
    public BlockMatchTest getGenRule() { return new BlockMatchTest(fillBlock.get()); }
    public String configKey() { return configKey; }
}
