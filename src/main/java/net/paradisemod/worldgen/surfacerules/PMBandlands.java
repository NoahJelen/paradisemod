package net.paradisemod.worldgen.surfacerules;

import java.util.Arrays;
import java.util.List;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.util.KeyDispatchDataCodec;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.SurfaceRules;
import net.minecraft.world.level.levelgen.SurfaceRules.Context;
import net.minecraft.world.level.levelgen.SurfaceRules.RuleSource;
import net.paradisemod.base.mixin.SurfaceRulesContextAccessor;
import net.paradisemod.base.mixin.SurfaceSystemAccessor;

public enum PMBandlands implements RuleSource {
    CONCRETE(
        Blocks.WHITE_CONCRETE,
        Blocks.ORANGE_CONCRETE,
        Blocks.MAGENTA_CONCRETE,
        Blocks.LIGHT_BLUE_CONCRETE,
        Blocks.YELLOW_CONCRETE,
        Blocks.LIME_CONCRETE,
        Blocks.PINK_CONCRETE,
        Blocks.GRAY_CONCRETE,
        Blocks.LIGHT_GRAY_CONCRETE,
        Blocks.CYAN_CONCRETE,
        Blocks.PURPLE_CONCRETE,
        Blocks.BLUE_CONCRETE,
        Blocks.BROWN_CONCRETE,
        Blocks.GREEN_CONCRETE,
        Blocks.RED_CONCRETE,
        Blocks.BLACK_CONCRETE
    ),

    TERRACOTTA(
        Blocks.WHITE_TERRACOTTA,
        Blocks.ORANGE_TERRACOTTA,
        Blocks.MAGENTA_TERRACOTTA,
        Blocks.LIGHT_BLUE_TERRACOTTA,
        Blocks.YELLOW_TERRACOTTA,
        Blocks.LIME_TERRACOTTA,
        Blocks.PINK_TERRACOTTA,
        Blocks.GRAY_TERRACOTTA,
        Blocks.LIGHT_GRAY_TERRACOTTA,
        Blocks.CYAN_TERRACOTTA,
        Blocks.PURPLE_TERRACOTTA,
        Blocks.BLUE_TERRACOTTA,
        Blocks.BROWN_TERRACOTTA,
        Blocks.GREEN_TERRACOTTA,
        Blocks.RED_TERRACOTTA,
        Blocks.BLACK_TERRACOTTA
    );

    public static final KeyDispatchDataCodec<PMBandlands> CODEC = KeyDispatchDataCodec.of(
        RecordCodecBuilder.create(builder ->
            builder.group(
                Codec.STRING
                    .fieldOf("group")
                    .stable()
                    .forGetter(bl -> bl.name().toLowerCase())
            )
                .apply(builder, builder.stable(s -> PMBandlands.valueOf(s.toUpperCase())))
        )
    );
    /*public static final Codec<PMBandlands> CODEC = RecordCodecBuilder.create(builder ->
        builder.group(
            Codec.STRING
                .fieldOf("group")
                .stable()
                .forGetter(bl -> bl.name().toLowerCase())
        )
            .apply(builder, builder.stable(s -> PMBandlands.valueOf(s.toUpperCase())))
    );*/

    private final List<BlockState> bandBlocks;

    PMBandlands(Block... bandBlocks) {
        this.bandBlocks = Arrays.stream(bandBlocks)
            .map(Block::defaultBlockState)
            .toList();
    }

    @Override
    public KeyDispatchDataCodec<PMBandlands> codec() { return CODEC; }

    @Override
    public SurfaceRules.SurfaceRule apply(Context context) {
        var system = ((SurfaceRulesContextAccessor) (Object) context).surfaceSystem();
        var clayBandsOffsetNoise = ((SurfaceSystemAccessor) system).getGetBandsNoise();

        return (x, y, z) -> {
            var ofs = (int) Math.round(clayBandsOffsetNoise.getValue(x, 0, z) * 4.0D);
            var block = (this == CONCRETE ? Blocks.ORANGE_CONCRETE : Blocks.TERRACOTTA).defaultBlockState();

            for(int i = 0; i < 16; i++) {
                if((y + ofs) % (i + 1) == 0)
                    block = bandBlocks.get(i);
            }

            return block;
        };
    }
}