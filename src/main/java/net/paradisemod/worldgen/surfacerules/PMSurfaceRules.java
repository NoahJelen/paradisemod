package net.paradisemod.worldgen.surfacerules;

import java.util.function.Supplier;

import net.minecraft.data.worldgen.SurfaceRuleData;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.Noises;
import net.minecraft.world.level.levelgen.SurfaceRules;
import net.minecraft.world.level.levelgen.SurfaceRules.ConditionSource;
import net.minecraft.world.level.levelgen.SurfaceRules.RuleSource;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.paradisemod.world.DeepDarkBlocks;
import net.paradisemod.world.Ores;
import net.paradisemod.world.PMWorld;
import net.paradisemod.world.biome.PMBiomes;

public class PMSurfaceRules {
    private static final ConditionSource WATER_CHECK = SurfaceRules.waterBlockCheck(-1, 0);

    // blocks used for the surface rules
    private static final RuleSource MUD = buildStateRule(Blocks.MUD);
    private static final RuleSource GRASS = buildStateRule(Blocks.GRASS_BLOCK);
    private static final RuleSource MYCELIUM = buildStateRule(Blocks.MYCELIUM);
    private static final RuleSource DIRT = buildStateRule(Blocks.DIRT);
    private static final RuleSource COARSE_DIRT = buildStateRule(Blocks.COARSE_DIRT);
    private static final RuleSource SAND = buildStateRule(Blocks.SAND);
    private static final RuleSource GRAVEL = buildStateRule(Blocks.GRAVEL);
    private static final RuleSource SANDSTONE = buildStateRule(Blocks.SANDSTONE);
    private static final RuleSource RED_SAND = buildStateRule(Blocks.RED_SAND);
    private static final RuleSource RED_SANDSTONE = buildStateRule(Blocks.RED_SANDSTONE);
    private static final RuleSource STONE = buildStateRule(Blocks.STONE);
    private static final RuleSource SNOW = buildStateRule(Blocks.SNOW_BLOCK);
    private static final Supplier<RuleSource> DARKSTONE = () -> buildStateRule(DeepDarkBlocks.DARKSTONE.get());
    private static final Supplier<RuleSource> OVERGROWN_DARKSTONE = () -> buildStateRule(DeepDarkBlocks.OVERGROWN_DARKSTONE.get());
    private static final Supplier<RuleSource> GLOWING_NYLIUM = () -> buildStateRule(DeepDarkBlocks.GLOWING_NYLIUM.get());
    private static final Supplier<RuleSource> SALT = () -> buildStateRule(Ores.SALT_BLOCK.get());
    private static final Supplier<RuleSource> BLACKENED_SAND = () -> buildStateRule(DeepDarkBlocks.BLACKENED_SAND.get());
    private static final Supplier<RuleSource> BLACKENED_SANDSTONE = () -> buildStateRule(DeepDarkBlocks.BLACKENED_SANDSTONE.get());
    private static final Supplier<RuleSource> COMPACT_SALT = () -> buildStateRule(Ores.COMPACT_SALT_BLOCK.get());
    private static final RuleSource HONEYCOMB = buildStateRule(Blocks.HONEYCOMB_BLOCK);
    private static final RuleSource BEDROCK = buildStateRule(Blocks.BEDROCK);
    private static final RuleSource DEEPSLATE = buildStateRule(Blocks.DEEPSLATE);

    // the surface rules

    // the default is a grass covered surface
    private static final RuleSource DEFAULT = SurfaceRules.sequence(
        SurfaceRules.ifTrue(
            WATER_CHECK,
            SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, GRASS)
        ),

        SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, DIRT)
    );

    // overworld surface rules
    private static final RuleSource STONE_SURFACE = SurfaceRules.sequence(
        SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, STONE),
        SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, STONE)
    );

    private static final RuleSource COARSE_DIRT_SURFACE = SurfaceRules.sequence(
        SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, COARSE_DIRT),
        SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, COARSE_DIRT)
    );

    private static final Supplier<RuleSource> SALT_SURFACE = () -> SurfaceRules.sequence(
        SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, SALT.get()),
        SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, COMPACT_SALT.get())
    );

    private static final RuleSource CB_SURFACE = SurfaceRules.sequence(
        SurfaceRules.ifTrue(
            SurfaceRules.not(SurfaceRules.yStartCheck(VerticalAnchor.absolute(128), 2)),
            SurfaceRules.sequence(
                SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, buildStateRule(Blocks.ORANGE_CONCRETE_POWDER)),
                SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, buildStateRule(Blocks.ORANGE_CONCRETE))
            )
        )
    );

    // deep dark surface rules
    private static final Supplier<RuleSource> DARKSTONE_SURFACE = () -> SurfaceRules.sequence(
        SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, DARKSTONE.get()),
        SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, DARKSTONE.get())
    );

    private static final Supplier<RuleSource> OVERGROWN_DARKSTONE_SURFACE = () -> SurfaceRules.sequence(
        SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, OVERGROWN_DARKSTONE.get()),
        SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, DARKSTONE.get())
    );

    private static final Supplier<RuleSource> GLOWING_NYLIUM_SURFACE = () -> SurfaceRules.sequence(
        SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, GLOWING_NYLIUM.get()),
        SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, DARKSTONE.get())
    );

    // overworld core rules
    private static final RuleSource HONEY_SURFACE = SurfaceRules.sequence(
        SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, HONEYCOMB),
        SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, HONEYCOMB)
    );

    private static final RuleSource SAND_SURFACE = SurfaceRules.sequence(
        SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, SAND),
        SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, SANDSTONE)
    );

    private static final RuleSource SNOW_SURFACE = SurfaceRules.sequence(
        SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, SNOW),
        SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, DIRT)
    );

    private static final RuleSource MUD_SURFACE = SurfaceRules.sequence(
        SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, MUD),
        SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, MUD)
    );

    private static final RuleSource UNDERGROUND_OCEAN_FLOOR = SurfaceRules.sequence(
        SurfaceRules.ifTrue(SurfaceRules.waterBlockCheck(-1, 0), GRASS), GRAVEL
    );

    private static final RuleSource WARM_UNDERGROUND_OCEAN_FLOOR = SurfaceRules.sequence(
        SurfaceRules.ifTrue(SurfaceRules.waterBlockCheck(-1, 0), GRASS), SAND
    );

    private static final RuleSource MYCELIUM_SURFACE = SurfaceRules.sequence(
        SurfaceRules.ifTrue(
            WATER_CHECK,
            SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, MYCELIUM)
        ),

        SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, DIRT)
    );

    private static final RuleSource TC_SURFACE = SurfaceRules.sequence(
        SurfaceRules.ifTrue(
            SurfaceRules.not(SurfaceRules.yStartCheck(VerticalAnchor.absolute(128), 2)),
            SurfaceRules.sequence(
                SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, RED_SAND),
                SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, RED_SANDSTONE)
            )
        )
    );

    public static RuleSource buildOverworldRules() {
        SurfaceRules.sequence(
            SurfaceRules.ifTrue(
                SurfaceRules.abovePreliminarySurface(),

                SurfaceRules.sequence(
                    SurfaceRules.ifTrue(
                        SurfaceRules.isBiome(
                            PMBiomes.ROCKY_DESERT,
                            PMBiomes.HIGH_ROCKY_DESERT,
                            PMBiomes.SNOWY_ROCKY_DESERT,
                            PMBiomes.MESQUITE_FOREST,
                            PMBiomes.PALO_VERDE_FOREST
                        ),
                        COARSE_DIRT_SURFACE
                    ),

                    SurfaceRules.ifTrue(SurfaceRules.isBiome(PMBiomes.SALT_FLAT), SALT_SURFACE.get()),
                    SurfaceRules.ifTrue(SurfaceRules.isBiome(PMBiomes.GLACIER, PMBiomes.VOLCANIC_FIELD, PMBiomes.SUBGLACIAL_VOLCANIC_FIELD), STONE_SURFACE),

                    // the default surface is grass
                    SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, DEFAULT)
                )
            )
        );

        return SurfaceRules.ifTrue(
            SurfaceRules.abovePreliminarySurface(),

            SurfaceRules.sequence(
                SurfaceRules.ifTrue(
                    SurfaceRules.isBiome(
                        PMBiomes.ROCKY_DESERT,
                        PMBiomes.HIGH_ROCKY_DESERT,
                        PMBiomes.SNOWY_ROCKY_DESERT,
                        PMBiomes.MESQUITE_FOREST,
                        PMBiomes.PALO_VERDE_FOREST
                    ),
                    COARSE_DIRT_SURFACE
                ),

                SurfaceRules.ifTrue(SurfaceRules.isBiome(PMBiomes.SALT_FLAT), SALT_SURFACE.get()),
                SurfaceRules.ifTrue(SurfaceRules.isBiome(PMBiomes.GLACIER, PMBiomes.VOLCANIC_FIELD, PMBiomes.SUBGLACIAL_VOLCANIC_FIELD), STONE_SURFACE),

                // the default surface is grass
                SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, DEFAULT)
            )
        );
    }

    public static RuleSource buildEndRule() {
        var overgrownEndStone = buildStateRule(PMWorld.OVERGROWN_END_STONE.get());
        var endStone = buildStateRule(Blocks.END_STONE);

        return SurfaceRules.sequence(
            SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, overgrownEndStone),
            SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, endStone)
        );
    }

    public static RuleSource buildDeepDarkRules() {
        return SurfaceRules.sequence(
            SurfaceRules.ifTrue(
                SurfaceRules.isBiome(
                    PMBiomes.TAIGA_INSOMNIUM,
                    PMBiomes.SILVA_INSOMNIUM
                ),
                OVERGROWN_DARKSTONE_SURFACE.get()
            ),

            SurfaceRules.ifTrue(
                SurfaceRules.isBiome(
                    PMBiomes.GLOWING_FOREST,
                    PMBiomes.CRYSTAL_FOREST
                ),
                GLOWING_NYLIUM_SURFACE.get()
            ),
            SurfaceRules.ifTrue(
                SurfaceRules.isBiome(PMBiomes.DARK_DESERT),
                SurfaceRules.sequence(
                    SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, BLACKENED_SAND.get()),
                    SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, BLACKENED_SANDSTONE.get())
                )
            ),
            SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, DARKSTONE_SURFACE.get())
        );
    }

    public static RuleSource buildOverworldCoreRules() {
        return SurfaceRules.sequence(
            SurfaceRules.ifTrue(
                SurfaceRules.not(SurfaceRules.verticalGradient("bedrock_roof", VerticalAnchor.belowTop(5), VerticalAnchor.top())),
                BEDROCK
            ),

            SurfaceRules.ifTrue(
                SurfaceRules.verticalGradient("bedrock_floor", VerticalAnchor.bottom(), VerticalAnchor.aboveBottom(5)),
                BEDROCK
            ),

            SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR,
                SurfaceRules.sequence(
                    // terracotta cave
                    SurfaceRules.ifTrue(
                        SurfaceRules.isBiome(PMBiomes.TERRACOTTA_CAVE),
                        SurfaceRules.sequence(
                            SurfaceRules.ifTrue(
                                SurfaceRules.ON_FLOOR,
                                SurfaceRules.ifTrue(
                                    SurfaceRules.not(
                                        SurfaceRules.yStartCheck(VerticalAnchor.absolute(192), 1)
                                    ),
                                    SurfaceRules.sequence(
                                        SurfaceRules.ifTrue(
                                            SurfaceRules.noiseCondition(Noises.SURFACE, -0.909D, -0.5454D),
                                            buildStateRule(Blocks.RED_TERRACOTTA)
                                        ),
                                        SurfaceRules.ifTrue(
                                            SurfaceRules.noiseCondition(Noises.SURFACE, -0.1818D, 0.1818D),
                                            buildStateRule(Blocks.WHITE_TERRACOTTA)
                                        ),
                                        SurfaceRules.ifTrue(
                                            SurfaceRules.noiseCondition(Noises.SURFACE, 0.5454D, 0.909D),
                                            buildStateRule(Blocks.BLUE_TERRACOTTA)
                                        )
                                    )
                                )
                            ),

                            TC_SURFACE,
                            SurfaceRules.ifTrue(
                                SurfaceRules.yBlockCheck(VerticalAnchor.absolute(63), 2),
                                PMBandlands.TERRACOTTA
                            )
                        )
                    ),

                    // underground rocky deserts
                    SurfaceRules.ifTrue(
                        SurfaceRules.isBiome(
                            PMBiomes.UNDERGROUND_ROCKY_DESERT,
                            PMBiomes.COLD_UNDERGROUND_ROCKY_DESERT,
                            PMBiomes.SNOWY_UNDERGROUND_ROCKY_DESERT,
                            PMBiomes.UNDERGROUND_MESQUITE_FOREST,
                            PMBiomes.UNDERGROUND_PALO_VERDE_FOREST
                        ),
                        COARSE_DIRT_SURFACE
                    ),

                    // underground oceans
                    SurfaceRules.ifTrue(
                        SurfaceRules.isBiome(
                            PMBiomes.UNDERGROUND_OCEAN,
                            PMBiomes.COLD_UNDERGROUND_OCEAN,
                            PMBiomes.FROZEN_UNDERGROUND_OCEAN
                        ),
                        UNDERGROUND_OCEAN_FLOOR
                    ),

                    SurfaceRules.ifTrue(
                        SurfaceRules.isBiome(
                            PMBiomes.WARM_UNDERGROUND_OCEAN,
                            PMBiomes.LUKEWARM_UNDERGROUND_OCEAN
                        ),
                        WARM_UNDERGROUND_OCEAN_FLOOR
                    ),

                    SurfaceRules.ifTrue(SurfaceRules.isBiome(PMBiomes.SALT_CAVE), SALT_SURFACE.get()),
                    SurfaceRules.ifTrue(SurfaceRules.isBiome(PMBiomes.UNDERGROUND_DESERT), SAND_SURFACE),

                    // honey cave
                    SurfaceRules.ifTrue(SurfaceRules.isBiome(PMBiomes.HONEY_CAVE),
                        SurfaceRules.sequence(
                            SurfaceRules.ifTrue(
                                SurfaceRules.ON_FLOOR,
                                SurfaceRules.ifTrue(
                                    SurfaceRules.not(
                                        SurfaceRules.yStartCheck(VerticalAnchor.absolute(310), 1)
                                    ),
                                    SurfaceRules.ifTrue(
                                        SurfaceRules.noiseCondition(Noises.SURFACE, 0.5454D, 0.909D),
                                        buildStateRule(Blocks.HONEY_BLOCK)
                                    )
                                )
                            ),
                            HONEY_SURFACE
                        )
                    ),
                    SurfaceRules.ifTrue(
                        SurfaceRules.isBiome(
                            PMBiomes.UNDERGROUND_GLACIER,
                            PMBiomes.VOLCANIC_CAVE,
                            Biomes.DRIPSTONE_CAVES
                        ),
                        STONE_SURFACE
                    ),
                    SurfaceRules.ifTrue(SurfaceRules.isBiome(PMBiomes.ICE_SPIKES_CAVE), SNOW_SURFACE),
                    SurfaceRules.ifTrue(SurfaceRules.isBiome(PMBiomes.MUSHROOM_CAVE), MYCELIUM_SURFACE),
                    SurfaceRules.ifTrue(SurfaceRules.isBiome(PMBiomes.UNDERGROUND_MANGROVE_SWAMP), MUD_SURFACE),

                    // the default surface is grass
                    DEFAULT
                )
            ),

            SurfaceRules.ifTrue(SurfaceRules.verticalGradient("deepslate", VerticalAnchor.absolute(0), VerticalAnchor.absolute(8)), DEEPSLATE)
        );
    }

    public static RuleSource buildElysiumRules() {
        var overworldRules = SurfaceRuleData.overworldLike(false, false, false);

        return SurfaceRules.sequence(
            // concrete badlands
            SurfaceRules.ifTrue(
                SurfaceRules.isBiome(PMBiomes.CONCRETE_BADLANDS),
                SurfaceRules.sequence(
                    SurfaceRules.ifTrue(
                        SurfaceRules.ON_FLOOR,
                        SurfaceRules.ifTrue(
                            SurfaceRules.not(
                                SurfaceRules.yStartCheck(VerticalAnchor.absolute(192), 1)
                            ),
                            SurfaceRules.sequence(
                                SurfaceRules.ifTrue(
                                    SurfaceRules.noiseCondition(Noises.SURFACE, -0.909D, -0.5454D),
                                    buildStateRule(Blocks.RED_CONCRETE)
                                ),
                                SurfaceRules.ifTrue(
                                    SurfaceRules.noiseCondition(Noises.SURFACE, -0.1818D, 0.1818D),
                                    buildStateRule(Blocks.WHITE_CONCRETE)
                                ),
                                SurfaceRules.ifTrue(
                                    SurfaceRules.noiseCondition(Noises.SURFACE, 0.5454D, 0.909D),
                                    buildStateRule(Blocks.BLUE_CONCRETE)
                                )
                            )
                        )
                    ),

                    CB_SURFACE,
                    SurfaceRules.ifTrue(
                        SurfaceRules.yBlockCheck(VerticalAnchor.absolute(63), 2),
                        PMBandlands.CONCRETE
                    )
                )
            ),

            SurfaceRules.ifTrue(
                SurfaceRules.isBiome(
                    PMBiomes.ROCKY_DESERT,
                    PMBiomes.HIGH_ROCKY_DESERT,
                    PMBiomes.SNOWY_ROCKY_DESERT,
                    PMBiomes.MESQUITE_FOREST,
                    PMBiomes.PALO_VERDE_FOREST
                ),
                COARSE_DIRT_SURFACE
            ),

            SurfaceRules.ifTrue(
                SurfaceRules.isBiome(
                    PMBiomes.SALT_FLAT,
                    PMBiomes.SALT_DEPOSIT
                ),
                SALT_SURFACE.get()
            ),

            SurfaceRules.ifTrue(
                SurfaceRules.isBiome(
                    PMBiomes.GLACIER,
                    PMBiomes.VOLCANIC_FIELD,
                    PMBiomes.SUBGLACIAL_VOLCANIC_FIELD
                ),
                STONE_SURFACE
            ),

            overworldRules
        );
    }

    private static RuleSource buildStateRule(Block block) {
        return SurfaceRules.state(block.defaultBlockState());
    }
}