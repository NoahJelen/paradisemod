package net.paradisemod.worldgen;

import com.mojang.serialization.Codec;

import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.BiomeTags;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.carver.ConfiguredWorldCarver;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.world.BiomeGenerationSettingsBuilder;
import net.minecraftforge.common.world.BiomeModifier;
import net.minecraftforge.common.world.ModifiableBiomeInfo.BiomeInfo;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.server.ServerLifecycleHooks;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.PMConfig;
import net.paradisemod.base.data.tags.PMTags;
import net.paradisemod.base.registry.PMRegistries;
import net.paradisemod.world.biome.PMBiomes;
import net.paradisemod.worldgen.carver.PMCarvers;
import net.paradisemod.worldgen.features.PMFeatures;
import net.paradisemod.worldgen.features.cave.PMCaveFeatures;
import net.paradisemod.worldgen.features.foliage.PMFoliage;
import net.paradisemod.worldgen.structures.PMStructures;

public class PMBiomeModifier implements BiomeModifier {
    private static final DeferredRegister<Codec<? extends BiomeModifier>> BIOME_MODIFIER_SERIALIZERS = PMRegistries.createRegistry(ForgeRegistries.Keys.BIOME_MODIFIER_SERIALIZERS);

    public static final RegistryObject<Codec<PMBiomeModifier>> PM_BIOME_MOD_CODEC = BIOME_MODIFIER_SERIALIZERS.register("pm_biome_mod", () -> Codec.unit(new PMBiomeModifier()));

    private Registry<ConfiguredWorldCarver<?>> carverRegistry;
    private Registry<PlacedFeature> placedFeatureRegistry;
    private BiomeGenerationSettingsBuilder genSettings;

    @Override
    public void modify(Holder<Biome> biome, Phase phase, BiomeInfo.Builder builder) {
        var regAccess = ServerLifecycleHooks.getCurrentServer().registryAccess();
        placedFeatureRegistry = regAccess.registryOrThrow(Registries.PLACED_FEATURE);
        carverRegistry = regAccess.registryOrThrow(Registries.CONFIGURED_CARVER);

        genSettings = builder.getGenerationSettings();

        if (phase == Phase.ADD) {
            if(biome.is(BiomeTags.IS_END)) {
                if (PMConfig.SETTINGS.betterEnd.enderAcid.get()) {
                    addFeature(GenerationStep.Decoration.LAKES, PMFeatures.ENDER_ACID_LAKE);
                    addFeature(GenerationStep.Decoration.LAKES, PMFeatures.ENDER_ACID_LAKE_UNDERGROUND);
                    addFeature(GenerationStep.Decoration.FLUID_SPRINGS, PMFeatures.ENDER_ACID_SPRING);
                }

                // allow modded end biomes to do their own thing
                if(isVanilla(biome) && PMConfig.SETTINGS.betterEnd.endFoliage.get()) {
                    addFeature(PMFoliage.END_FOLIAGE);
                    addFeature(PMStructures.ROGUE_SPIKE);

                    if(biome.is(Biomes.END_HIGHLANDS))
                        addFeature(PMFoliage.PM_CHORUS_PLANT);
                }

                addFeature(PMFeatures.RANDOM_END_CRYSTALS);
                addFeature(PMFeatures.MIXED_END_CRYSTALS);

                if(PMConfig.SETTINGS.betterEnd.enderCaves.get()) {
                    addCarver(GenerationStep.Carving.AIR, PMCarvers.END_CAVES);
                    addCarver(GenerationStep.Carving.AIR, PMCarvers.END_CANYONS);
                    addFeature(PMFeatures.ENDERITE_GEODE);
                }
            }

            if(biome.is(PMBiomes.MESQUITE_FOREST))
                addFeature(PMFoliage.EXTRA_MESQUITE);

            if(biome.is(PMBiomes.PALO_VERDE_FOREST))
                addFeature(PMFoliage.EXTRA_PALO_VERDE);

            if(biome.is(Biomes.ICE_SPIKES))
                addFeature(PMFeatures.ICE_SPIRE);

            if(biome.is(PMBiomes.ICE_SPIKES_CAVE))
                addFeature(PMFeatures.ICE_SPIRE_OWC);

            if(biome.is(Biomes.RIVER))
                addFeature(PMFoliage.GALLERY_FOREST);

            if(biome.is(BiomeTags.IS_NETHER)) {
                addFeature(PMFoliage.SOUL_PUMPKIN_PATCH);
                addFeature(PMFeatures.QUARTZ_CRYSTALS);
                addFeature(PMFeatures.QUARTZ_GEODE_NETHER);
            }
            else if(!biome.is(Biomes.DEEP_DARK)) {
                if(!biome.is(PMTags.Biomes.ROSE_FIELDS) && !biome.is(Tags.Biomes.IS_DRY) && biome.is(BiomeTags.IS_OVERWORLD))
                    addFeature(PMFoliage.ROSES);

                if(biome.is(BiomeTags.IS_OVERWORLD) || biome.is(PMTags.Biomes.OVERWORLD_CORE) || biome.is(PMTags.Biomes.ELYSIUM)) {
                    if(biome.is(BiomeTags.IS_MOUNTAIN))
                        addFeature(PMFeatures.EMERALD_GEODE);

                    addFeature(PMStructures.HOME);
                    addFeature(PMStructures.RESEARCH_BASE);
                    addFeature(PMStructures.SMALL_STRONGHOLD);
                    addFeature(PMFeatures.SALT_CRYSTALS);
                    addFeature(PMFeatures.MIXED_CRYSTALS);
                    addFeature(PMFeatures.RANDOM_CRYSTALS);
                    addFeature(PMFeatures.TAR_SPRING);
                    addFeature(PMFeatures.DIAMOND_GEODE);
                    addFeature(PMFeatures.LAPIS_GEODE);
                    addFeature(PMFeatures.QUARTZ_GEODE);
                    addFeature(PMFeatures.REDSTONE_GEODE);
                    addFeature(PMFeatures.RUBY_GEODE);
                    addFeature(PMFeatures.SALT_GEODE);
                }

                if(biome.is(BiomeTags.IS_OVERWORLD) || biome.is(PMTags.Biomes.ELYSIUM)) {
                    addFeature(PMFeatures.FALLEN_TREE);
                    addFeature(PMStructures.LANDMINE);
                    addFeature(PMStructures.EASTER_EGG_1);
                    addFeature(PMStructures.EASTER_EGG_2);
                    addFeature(PMStructures.REBELS_1);
                    addFeature(PMStructures.REBELS_2);
                    addFeature(PMStructures.RUNWAY);
                    addFeature(PMStructures.BLACK_CROSS);
                    addFeature(PMStructures.GOLD_CROSS);
                    addFeature(PMStructures.CREATOR_HEADS);
                    addFeature(GenerationStep.Decoration.LAKES, PMFeatures.TAR_PIT);

                    if(!biome.is(Tags.Biomes.IS_WATER)) {
                        addFeature(GenerationStep.Decoration.LAKES, PMFeatures.WATER_LAKE);
                        addFeature(GenerationStep.Decoration.LAKES, PMFeatures.WATER_LAKE_UNDERGROUND);
                    }

                    if(PMBiomes.isSandyDesert(biome) || biome.is(BiomeTags.IS_BADLANDS))
                        addFeature(PMFoliage.PRICKLY_PEAR);
                }

                if(!biome.is(PMTags.Biomes.ROCKY_DESERTS) && !biome.is(BiomeTags.IS_END))
                    addFeature(PMStructures.RUINS);

                if(biome.is(BiomeTags.IS_OCEAN))
                    addFeature(PMStructures.BUOY);
            }

            for(var oreFeature : PMFeatures.ORES)
                addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, oreFeature);

            addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMCaveFeatures.CAVE_FORMATIONS);
            addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMCaveFeatures.CAVE_BLOCKS);
            if(!biome.is(BiomeTags.IS_NETHER))
                addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMCaveFeatures.CAVE_FOLIAGE);

            if(biome.is(BiomeTags.IS_JUNGLE) || biome.is(Tags.Biomes.IS_SWAMP))
                addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMCaveFeatures.CAVE_VINE);

            addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PMFeatures.RAW_CHUNK_PROCESSOR);
            addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, PMFeatures.VEGETAL_CHUNK_PROCESSOR);
        }
    }

    @Override
    public Codec<? extends BiomeModifier> codec() {
        return PM_BIOME_MOD_CODEC.get();
    }

    private void addFeature(GenerationStep.Decoration step, ResourceKey<PlacedFeature> key) {
        var feature = placedFeatureRegistry.getHolderOrThrow(key);
        genSettings.addFeature(step, feature);
    }

    private void addFeature(ResourceKey<PlacedFeature> key) {
        addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, key);
    }

    private void addCarver(GenerationStep.Carving step, ResourceKey<ConfiguredWorldCarver<?>> key) {
        var carver = carverRegistry.getHolderOrThrow(key);
        genSettings.addCarver(step, carver);
    }

    private boolean isVanilla(Holder<Biome> biome) {
        var key = biome.unwrapKey().get();
        var resourceName = key.location();
        var namespace = resourceName.getNamespace();
        return namespace == "minecraft" || namespace == "";
    }

    public static void init(IEventBus eventBus) {
        BIOME_MODIFIER_SERIALIZERS.register(eventBus);
    }

    public static void buildBiomeModifier(BootstapContext<BiomeModifier> context) {
        ParadiseMod.LOG.debug("Generating biome modifiers...");
        context.register(
            PMRegistries.createModResourceKey(ForgeRegistries.Keys.BIOME_MODIFIERS, "pm_biome_mod"),
            new PMBiomeModifier()
        );
    }
}