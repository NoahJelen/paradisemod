// Made with Blockbench 4.12.1
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports


public class Gargoyle<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "gargoyle"), "main");
	private final ModelPart gargoyle;
	private final ModelPart leg1;
	private final ModelPart leg2;
	private final ModelPart body;
	private final ModelPart head;
	private final ModelPart arms;
	private final ModelPart wing1;
	private final ModelPart wing2;

	public Gargoyle(ModelPart root) {
		this.gargoyle = root.getChild("gargoyle");
		this.leg1 = this.gargoyle.getChild("leg1");
		this.leg2 = this.gargoyle.getChild("leg2");
		this.body = this.gargoyle.getChild("body");
		this.head = this.gargoyle.getChild("head");
		this.arms = this.gargoyle.getChild("arms");
		this.wing1 = this.gargoyle.getChild("wing1");
		this.wing2 = this.gargoyle.getChild("wing2");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition gargoyle = partdefinition.addOrReplaceChild("gargoyle", CubeListBuilder.create(), PartPose.offset(-3.0F, 16.0F, 0.0F));

		PartDefinition leg1 = gargoyle.addOrReplaceChild("leg1", CubeListBuilder.create().texOffs(16, 24).addBox(-1.0F, 0.0F, -2.0F, 3.0F, 8.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition leg2 = gargoyle.addOrReplaceChild("leg2", CubeListBuilder.create().texOffs(16, 24).addBox(-2.0F, 0.0F, -2.0F, 3.0F, 8.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(6.0F, 0.0F, 0.0F));

		PartDefinition body = gargoyle.addOrReplaceChild("body", CubeListBuilder.create().texOffs(26, 12).addBox(-3.0F, 0.0F, -2.0F, 6.0F, 2.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-5.0F, -7.0F, -3.0F, 10.0F, 7.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offset(3.0F, -2.0F, 0.0F));

		PartDefinition head = gargoyle.addOrReplaceChild("head", CubeListBuilder.create().texOffs(26, 17).addBox(0.0F, 2.0F, -3.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 24).addBox(-1.5F, -1.0F, -2.5F, 4.0F, 4.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(30, 0).addBox(1.0F, -3.0F, -1.0F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(30, 0).addBox(-1.0F, -3.0F, -1.0F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(2.5F, -12.0F, -0.5F));

		PartDefinition arms = gargoyle.addOrReplaceChild("arms", CubeListBuilder.create().texOffs(0, 32).addBox(11.0F, -2.0F, -1.0F, 2.0F, 12.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(0, 32).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 12.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-3.0F, -7.0F, 0.0F));

		PartDefinition wing1 = gargoyle.addOrReplaceChild("wing1", CubeListBuilder.create().texOffs(0, 12).addBox(-11.0F, -2.0F, 0.0F, 11.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(0, 20).addBox(-11.0F, 0.0F, 1.0F, 11.0F, 4.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(2.0F, -5.0F, 2.0F));

		PartDefinition wing2 = gargoyle.addOrReplaceChild("wing2", CubeListBuilder.create().texOffs(0, 12).addBox(0.0F, -2.0F, 0.0F, 11.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(0, 20).addBox(0.0F, 0.0F, 1.0F, 11.0F, 4.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(4.0F, -5.0F, 2.0F));

		return LayerDefinition.create(meshdefinition, 64, 64);
	}

	@Override
	public void setupAnim(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		gargoyle.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}
}